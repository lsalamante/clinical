# Clinical Trial Something
TODO: Fix title
* * *
#Table of contents
1. [Project description](#markdown-header-project-description)
1. [Installation](#markdown-header-installation)
1. [Developers](#markdown-header-developers)
1. [Technical specifications](#markdown-header-technical-specifications)
    + [Overview](#markdown-header-overview)
    + [Dependencies](#markdown-header-dependencies)
1. [Notes](#markdown-header-notes)
1. [Commit conventions](#markdown-header-commit-conventions)


* * *
# Project description
TODO: Add project description
* * *
# Installation  
1. Clone from `https://bitbucket.org/lsalamante/clinical`
1. Create database `clinical`
1. Import sql from `sql` folder
1. Configure `/jp_library/conf.php`
1. Configure `/jp_library/con-define.php`
1. Rock and roll!
* * *
# Developers
* Lorenzo Salamante ---- `@jjjjcccjjf`  
* Sir Vanleor Boquiren ---- TODO: Add contact info
* * *
# Technical specifications
## Overview
* Front-end: HTML5 and CSS3 using [Bootstrap FlatLab Theme](http://thevectorlab.net/flatlab/)
* Back-end: PHP, JavaScript
* Database: MySQL
* Design pattern: TODO
* Methodology: Agile / Extreme Programming
* VCS: SourceTree, Git
* Repository: Bitbucket
## Dependencies
1. JP library
1. [PHP Mailer](https://github.com/PHPMailer/PHPMailer) for mail functions
* * *
#Notes
## jp_library/conf.php  

* This is where you activate and deactivate certain modules native to FlatLab theme  
* You should **NEVER** set the `$LOCAL_MODE` variable to `true` on production / test site!!!  
* You should always initialize module variables such as `$PICKERS` to `true` if you want to use them  
* Change your site name here
* Change your default site URL here
* You can find most of the constants here

## jp_library/con-define.php  
* Configure database connections here

## Random notes
`footer.php` literal footer stuff without javascript  
`scripts.php` generic javascript stuffs before `</body>` tag that are omnipresent in all pages!!!  
`left-sidebar` and `right-sidebar` pretty self explanatory
`header.php` mostly `<head>` stuff
`top-nav.php` top navigation of flatlab theme (logout, logo position, etc)  
`logo.php` logo   
There is a `session_start()` at `jp_lib.php` line `345` you might wanna check that out (or put it somewhere else) if you want to explicitly define sessions

## Commit conventions
* Small letters all
* First word must be a verb or feature name **(pagusapan natin sir haha)**

verb: commit head  
commit body (if necessary)

**Example:**  
fix: email notification bug  
style: add style for left-sidebar

or

cms: add ability to add admins

**More examples**  
login: add forms for logging in  
login: add login validation