<?php
include('../../jp_library/jp_lib.php');
include('../../php-functions/fncCommon.php'); 


####### NOT USED
?>
                        <div class="form-group">
                          <label>Name</label><span class="required">*</span>
                          <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" required >
                          <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" style="margin-top:4px;" required> 
                          <!--<input type="text" class="form-control" id="middle_initial" name="middle_initial" placeholder="Middle Initial">-->
                        </div>
                        <div class="form-group">
                          <label>Mobile Number</label><span class="required">*</span>
                          <input type="text" class="form-control" id="mobile_num" name="mobile_num" required>
                          <span class="required" id="mobile_required" style="display:none">Mobile number is already registered.</span>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label><span class="required">*</span>
                            <input type="email" class="form-control" id="email" name="email">
                            <span class="required" id="email_required" style="display:none">Email is already registered.</span>
                        </div>
                        <div class="form-group">
                            <label for="username">Birthdate</label>
                            <input type="text" class="form-control" id="birthdate" name="birthdate">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label><span class="required">*</span>
                            <input type="password" class="form-control" id="password" name="password" required>
                            <!--<input type="checkbox" id="sp" name="sp[]" value="true"><label for="sp">Show Password</label>-->
                        </div>
                        <div class="form-group">
                            <label for="con_password">Confirm Password</label>
                            <input type="password" class="form-control" id="con_password" name="" required>
                            <span class="required" id="password_required" style="display:none">Password does not match.</span>
                            <!--<input type="checkbox" id="sp" name="sp[]" value="true"><label for="sp">Show Password</label>-->
                        </div>
                        <div class="form-group">
                          <label for="admin">Is Admin?</label>
                          <input type="checkbox" name="admin[]" id="admin[]" value="1" onclick="is_admin()">
                        </div>
                        <div class="form-group" id="pos_div">
                          <label for="position_id">Position</label><span class="required">*</span>
                          <select name="position_id" id="position_id" class="form-control" onchange="check_prof();" required>
                            <option value="" selected>Select Position</option>
                          <?php foreach (getPositions() as $position_arr) { ?>
                            <option value="<?php echo $position_arr['position_id']; ?>" data-profgroup="<?php echo $position_arr['is_professional_group']; ?>"><?php echo $position_arr['description']; ?></option>
                          <?php
                          } ?>
                          </select>
                        </div>
                        <div class="form-group last" id="prof_group_div" style="display:none">
                          <label class="control-label col-md-3">Professional Group</label><span class="required">*</span>
                          <div class="col-md-9">
                            <select name="pgroup_id[]" class="multi-select" multiple="multiple" id="my_multi_select3" >
                              <?php foreach (getProfGroup() as $pgroup_arr) 
                              { ?>
                              <option value="<?php echo $pgroup_arr['pgroup_id']; ?>"><?php echo $pgroup_arr['pgroup_name']; ?></option>
                              <?php
                              } ?>
                            </select>
                          </div>
                        </div>
                        <!--<button type="button" class="btn btn-info" name="admin" id="add-without-image">Try Message</button>-->
                        <!--<button type="button" class="btn btn-info" name="admin" onclick="alertme()">Try</button-->
                        <button type="button" class="btn btn-info" name="admin" onclick="save_user()" id="final_submit" disabled='disabled' style="margin-top:10px">Submit</button>
                        <button type="button" class="btn btn-warning" onclick="$('#add_user').slideToggle();" style="margin-top:10px">Cancel</button>