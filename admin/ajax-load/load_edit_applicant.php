<?php
include('../../jp_library/jp_lib.php');
include('../../php-functions/fncCommon.php');
$user = getUserById(false, $_GET['user_id'], true, $_GET['role_id']);
//print_r($user_info);
?>
                  <div class="modal-header nobg nopad" style="background:#64aaf9">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="alert-modal-title"><?php echo $phrases['edit']; ?> <?php echo $user['fname']." ".$user['lname']; ?></h4>
                  </div>
                  <div class="modal-body nopadR nopadL">
                    <div class="form-group">
                      	<label class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['name']; ?></label>
                      	<div class="col-sm-4">
                      		<input type="text" class="form-control" name="fname" id="fname" value="<?php echo $user['fname']; ?>">
                      	</div>
                      	<!-- <label class="col-sm-2 col-sm-2 control-label"><?php echo $phrases['name']; ?></label>
                      	<div class="col-sm-4">
                      		<input type="text" class="form-control" name="lname" id="lname" value="<?php echo $user['lname']; ?>">
                      	</div> -->
	                </div>
	                <div class="form-group">
                      	<label class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['gender']; ?></label>
                      	<div class="col-sm-9">
                      		<div class="radio">
                      			<label>
                      				<input type="radio" name="gender" id="radio_male" value="0"  <?php echo $user['gender'] == 0 ? "checked" : ""; ?>>
                      				<?php echo $phrases['male']; ?>
                      			</label>
                      		</div>
                      		<div class="radio">
                      			<label>
                      				<input type="radio" name="gender" id="radio_female" value="1" <?php echo $user['gender'] == 1 ? "checked" : ""; ?>>
                      				<?php echo $phrases['female']; ?>
                      			</label>
                      		</div>
                      	</div>
	                </div>
	                <div class="form-group">
                      	<label class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['company']; ?></label>
                      	<div class="col-sm-9">
                      		<input type="text" class="form-control" name="company" id="company" value="<?php echo $user['company'] ; ?>">
                      	</div>
	                </div>
                  <?php if($_GET['ajax_page'] == 'm'): ?>
                  <div class="form-group">
                        <label class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['department']; ?></label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" name="department" id="department" value="<?php echo $user['department'] ; ?>">
                        </div>
                  </div>
                <?php endif; ?>
	                <div class="form-group">
                      	<label class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['mobile_num']; ?><span class="required">*</span></label>
                      	<div class="col-sm-9">
                      		<input type="text" class="form-control" name="mobile_num" id="mobile_num" value="<?php echo $user['mobile_num'] ; ?>">
                      		<span class="required" id="mobile_required" style="display:none"><?php echo $phrases['mobile_registered']; ?>.</span>
                      	</div>
	                </div>
	                <div class="form-group">
                      	<label class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['email']; ?><span class="required">*</span></label>
                      	<div class="col-sm-9">
                      		<input type="text" class="form-control" name="email" id="email" value="<?php echo $user['email'] ; ?>">
                      		<span class="required" id="email_required" style="display:none"><?php echo $phrases['email_registered']; ?>.</span>
                      	</div>
	                </div>
	                <div class="form-group">
                      <label for="password" class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['password']; ?> (<?php echo $phrases['blank_update']; ?>)</label>
                      <div class="col-sm-9">
                      	<input type="password" class="form-control" id="password" name="password" required>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="con_password" class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['con_pass']; ?></label>
                      <div class="col-sm-9">
                      	<input type="password" class="form-control" id="con_password" name="" required>
                      	<span class="required" id="password_required" style="display:none"><?php echo $phrases['password_not_match']; ?>.</span>
                      </div>
                  </div>

                  <!-- Uploads START -->
                  <div class="form-group">
                    <label class="col-sm-3 col-sm-3 control-label"><?= $phrases['id_card_admin']?><span class="required">*</span></label>
                    <div class="col-sm-7">
                      <input type="file" class="form-control" name="doc_id_card" id="doc_id_card">
                    </div>
                    <?php if($user['doc_id_card'] != ''): ?>
                    <div class="col-sm-2">
                      <a href="<?php echo $user['doc_id_card'] ?>" class="btn btn-sm btn-primary" target="_blank" title="view file"><i class="fa fa-eye"></i></a>
                    </div>
                  <?php endif; ?>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 col-sm-3 control-label"><?= $phrases['gcp_card']?><span class="required">*</span></label>
                    <div class="col-sm-7">
                      <input type="file" class="form-control" name="doc_gcp_card" id="doc_gcp_card">
                    </div>
                    <?php if($user['doc_gcp_card'] != ''): ?>
                    <div class="col-sm-2">
                      <a href="<?php echo $user['doc_gcp_card'] ?>" class="btn btn-sm btn-primary" target="_blank" title="view file"><i class="fa fa-eye"></i></a>
                    </div>
                  <?php endif; ?>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 col-sm-3 control-label"><?= $phrases['diploma']?><span class="required">*</span></label>
                    <div class="col-sm-7">
                      <input type="file" class="form-control" name="doc_diploma" id="doc_diploma">
                    </div>
                    <?php if($user['doc_diploma'] != ''): ?>
                    <div class="col-sm-2">
                      <a href="<?php echo $user['doc_diploma'] ?>" class="btn btn-sm btn-primary" target="_blank" title="view file"><i class="fa fa-eye"></i></a>
                    </div>
                  <?php endif; ?>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 col-sm-3 control-label"><?= $phrases['degree_certificate']?><span class="required">*</span></label>
                    <div class="col-sm-7">
                      <input type="file" class="form-control" name="doc_degree" id="doc_degree">
                    </div>
                    <?php if($user['doc_degree'] != ''): ?>
                    <div class="col-sm-2">
                      <a href="<?php echo $user['doc_degree'] ?>" class="btn btn-sm btn-primary" target="_blank" title="view file"><i class="fa fa-eye"></i></a>
                    </div>
                  <?php endif; ?>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-3 col-sm-3 control-label"><?= $phrases['other']?><span class="required">*</span></label>
                    <div class="col-sm-7">
                      <input type="file" class="form-control" name="doc_other" id="doc_other">
                    </div>
                    <?php if($user['doc_other'] != ''): ?>
                    <div class="col-sm-2">
                      <a href="<?php echo $user['doc_other'] ?>" class="btn btn-sm btn-primary" target="_blank" title="view file"><i class="fa fa-eye"></i></a>
                    </div>
                  <?php endif; ?>
                  </div>
                  <!-- Uploads END -->

                  <?php if($_GET['ajax_page'] == 'm'): ?>
                  <div class="form-group">
                      <label for="" class="col-sm-3 control-label"><?php echo $phrases['position']; ?> <br><h6>(uncheck positions you want to remove for this user)</h6></label>
                      <div class="col-sm-9">
                        <?php foreach (getAllRoles(false, $_GET['user_id'] ) as $role) { ?>
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="<?php echo $role['role_id']; ?>" name="role_id[]" checked><?php echo $role['description']; ?>
                        </div>
                        <?php } ?>
                      </div>
                  </div>
                  <?php endif; ?>
                  </div> <!-- Closing div for ajax parent -->
                  <div class="modal-footer txtcenter nopad">
                  	<input type="hidden" name="user_id"  value="<?php echo $_GET['user_id']; ?>">
                    <input type="hidden" name="ajax_page" value="<?php echo $_GET['ajax_page'] ?>">
                    <button type="button" class="btn btn-warning" data-dismiss="modal"><?php echo $phrases['cancel']; ?></button>
                    <button class="btn btn-info" id="final_submit" type="button" onclick="update_user('<?php echo $_GET['ajax_page']; ?>', '<?php echo $_GET['pg']; ?>')"><?php echo $phrases['update']; ?></button>
                  </div>
