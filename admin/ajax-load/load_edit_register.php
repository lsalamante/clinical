<?php
include('../../jp_library/jp_lib.php');
include('../../php-functions/fncCommon.php'); 
$user_arr = getUserById(false, $_GET['user_id']);
?>

                        <div class="form-group">
                      		<label>Name</label><span class="required">*</span>
                      		<input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" required value="<?php echo $user_arr['fname']; ?>">
                      		<input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" style="margin-top:4px;" required value="<?php echo $user_arr['lname']; ?>"> 
                      		<!--<input type="text" class="form-control" id="middle_initial" name="middle_initial" placeholder="Middle Initial">-->
                      	</div>
                      	<div class="form-group">
                      		<label>Mobile Number</label><span class="required">*</span>
                      		<input type="text" class="form-control" id="mobile_num" name="mobile_num" required value="<?php echo $user_arr['mobile_num']; ?>">
                          <span class="required" id="mobile_required" style="display:none">Mobile number is already registered.</span>
                      	</div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label><span class="required">*</span>
                            <input type="email" class="form-control" id="email" name="email" value="<?php echo $user_arr['email']; ?>">
                            <span class="required" id="email_required" style="display:none">Email is already registered.</span>
                        </div>
                        <div class="form-group">
                            <label for="username">Birthdate</label>
                            <input type="text" class="form-control" id="birthdate" name="birthdate" value="<?php echo date("m/d/Y", strtotime($user_arr['birthdate'])); ?>">
                        </div>
                        <div class="form-group">
                            <label for="password">New Password (leave blank if not updating password)</label>
                            <input type="password" class="form-control" id="password" name="password">
                            <!--<input type="checkbox" id="sp" name="sp[]" value="true"><label for="sp">Show Password</label>-->
                        </div>
                        <div class="form-group">
                            <label for="con_password">Confirm Password</label>
                            <input type="password" class="form-control" id="con_password" name="">
                            <!--<input type="checkbox" id="sp" name="sp[]" value="true"><label for="sp">Show Password</label>-->
                        </div>
                        <div class="form-group">
                        	<label for="admin">Is Admin?</label>
                        	<input type="checkbox" name="admin[]" id="admin[]" value="1" onclick="is_admin()" <?php echo $user_arr['is_admin'] == '1' ? "checked" : ""; ?>>
                        </div>
                        <div class="form-group" id="pos_div">
                        	<label for="position_id">Position</label><span class="required">*</span>
                        	<select name="position_id" id="position_id" class="form-control" onchange="check_prof();">
                        		<option value="" selected>Select Position</option>
                        	<?php foreach (getPositions() as $position_arr) { ?>
                        		<option value="<?php echo $position_arr['position_id']; ?>" data-profgroup="<?php echo $position_arr['is_professional_group']; ?>" <?php echo $user_arr['position_id'] == $position_arr['position_id'] ? 'selected' : ''; ?>><?php echo $position_arr['description']; ?></option>
                        	<?php
                        	} ?>
                        	</select>
                        </div>
                        <div class="form-group last" id="prof_group_div" style="display:none">
                          <label class="control-label col-md-3">Professional Group</label>
                          <div class="col-md-9">
                            <select name="pgroup_id[]" class="multi-select" multiple="multiple" id="my_multi_select3" >
                              <?php 
                              $saved_pgroup = explode(",", $user_arr['pgroup_id']);
                              foreach (getProfGroup() as $pgroup_arr) 
                              { ?>
                              <option value="<?php echo $pgroup_arr['pgroup_id']; ?>" <?php echo in_array($pgroup_arr['pgroup_id'], $saved_pgroup) ? 'selected' : ''; ?>><?php echo $pgroup_arr['pgroup_name']; ?></option>
                              <?php
                              } ?>
                            </select>
                          </div>
                        </div>
                        <!--<button type="button" class="btn btn-info" name="admin" id="add-without-image">Try Message</button>-->
                        <!--<button type="button" class="btn btn-info" name="admin" onclick="testModal()">Try Modal</button>-->
                      <input type="hidden" name="user_id" value="<?php echo $_GET['user_id']; ?>">
                      <button type="button" class="btn btn-info" name="admin" onclick="update_user()" id="final_submit" style="margin-top:10px">Update</button>
                      <button type="button" class="btn btn-warning" onclick="cancel_edit();" style="margin-top:10px">Cancel</button>