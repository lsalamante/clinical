<?php
include('../../jp_library/jp_lib.php');
include('../../php-functions/fncCommon.php');
?>

<div class="col-sm-12">
	<section class="panel">
		<header class="panel-heading" style="padding-bottom: 15px;">
			<?php
			$user_types = array(
				"a" => $phrases['applicant'],
				"c" => "CRO",
				"m" => $phrases['member'],
				"t" => $phrases['tfo'],
				"p" => $phrases['pre'],
				"e" => $phrases['ethics_committee']
			);
			$user_list = array(
				"a" => $phrases['applicant_list'],
				"c" => "CRO List",
				"m" => $phrases['member_list'],
				"t" => $phrases['tfo_list'],
				"p" => $phrases['member_list'],
				"e" => $phrases['member_list']
			);
			?>
			<?php echo isset($_GET['m']) ? $user_list[$_GET['m']] : ""; ?>
			<span class="tools pull-right">
				<a href="#addApplicant" data-toggle="modal" class="btn btn-info pull-right" style="margin-bottom:5px; color:white;"><i class="fa fa-plus"></i>&nbsp;<?php echo $phrases['add']; ?> <?php echo isset($_GET['m']) ? $user_types[$_GET['m']] : "Applicant"; ?></a>
			</span>
		</header>
		<section class="panel">
		<style>
		span.required
		{
			margin-left: 3px;
			color: red;
		}
		</style>
		<div class="panel-body" id="table_inside">
			<div class="adv-table">
				<table class="display table table-bordered" id="hidden-table-info">
					<thead>
						<tr>
							<th><?php echo $phrases['name']; ?></th>
							<th><?php echo $phrases['gender']; ?></th>
							<th><?php echo $phrases['company']; ?></th>
							<?php if($_GET['m'] == 'm'): ?>
								<th><?php echo $phrases['department']; ?></th>
							<?php endif; ?>
							<th><?php echo $phrases['mobile_num']; ?></th>
							<!-- <th>Birthdate<br>mm/dd/yyyy</th> -->
							<!-- <th>Position</th> -->
							<!-- <th>Professional Group</th> -->
							<?php if(isset($_GET['m']) && ($_GET['m'] == "m" || $_GET['m'] == "e")): ?>
								<th><?php echo $phrases['position']; ?></th>
							<?php endif; ?>
							<th style="width:100px;"><?php echo $phrases['status']; ?></th>
							<th style="width:15%"><?php echo $phrases['action']; ?></th>
						</tr>
					</thead>
					<tbody>
						<?php
						$get_acronym = array(
							"a" => "A",
							"c" => "A-C",
							"m" => false,
							"t" => "TFO",
							"p" => "PRE",
							"e" => "EC"
						);
						$usertype = $get_acronym[$_GET['m']];



						foreach (getAllUsersByUserType(0, $usertype) as $user) { ?>
							<tr class="">
								<td><?php echo $user['fname']." ".$user['lname'];  ?></td>
								<td><?php echo $user['gender'] == 0 ? $phrases['male'] : $phrases['female']; ?></td>
								<td><?php echo $user['company'] != null ? $user['company'] : $phrases['not_applicable']; ?></td>
								<?php if($_GET['m'] == 'm'): ?>
									<td><?php echo $user['department'] != "" ? $user['department'] : $phrases['not_applicable']; ?></td>
								<?php endif; ?>
								<td><?php echo $user['mobile_num']; ?></td>
								<?php if(isset($_GET['m']) && ($_GET['m'] == 'm' || $_GET['m'] == "e")): ?>
									<td>
										<?php
										if($_GET['m'] == 'e')
										{
											echo $user['description'];
										}
										else
										{
											$ctr = 0;
											foreach (getAllRoles(false, $user['user_id']) as $role)
											{
												$ctr++;
												echo $phrases[$role['lang_phrase']]."<br>";
											}
											if($ctr == 0)
											{
												echo $phrases['not_applicable'];
											}
										}
										?>
									</td>
								<?php endif; ?>
								<td><?php echo $user['is_active'] == 1 ? $phrases['activated'] : $phrases['frozen']; ?></td>
								<td>
									<a href="#editApplicant" data-toggle="modal" class="btn btn-primary btn-xs edit_user_details" onclick="ready_edit('<?php echo $user['user_id']; ?>', '<?php echo $user['role_id']; ?>', '<?php echo $_GET['m']; ?>', '0');"><?= $phrases['edit'] ?></a>
									<button class="btn btn-primary btn-xs" onclick="show_status_modal(<?php echo $user['is_active']; ?>, <?php echo $user['user_id']; ?>, '<?php echo $_GET['m']; ?>');"><?php echo $user['is_active'] == 1 ? $phrases['freeze'] : $phrases['activation']; ?></button>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>

				</table>
			</div>
		</div>
	</section>
</div>



<?php if($_GET['m'] == 'a' || $_GET['m'] == 'm' || $_GET['m'] == 'c'): ?>
	<div class="modal fade" id="addApplicant" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content pad30">
				<form role="form" class="form-horizontal tasi-form" enctype="multipart/form-data" method="POST" autocomplete="off" id="form_add_user" onsubmit="javascript:;" onchange="form_validate();" data-edit="">
					<div class="modal-header nobg nopad" style="background:#64aaf9">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h5 class="alert-modal-title"><?php echo $phrases['add']; ?> <?php echo $user_types[$_GET['m']]; ?></h5>
					</div>
					<div class="modal-body nopadR nopadL">
						<div class="form-group">
							<label class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['name']; ?></label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="fname" id="fname">
							</div>
							<!-- <label class="col-sm-2 col-sm-2 control-label"><?php echo $phrases['name']; ?></label>
							<div class="col-sm-4">
							<input type="text" class="form-control" name="lname" id="lname">
						</div> -->
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['gender']; ?></label>
						<div class="col-sm-9">
							<div class="radio">
								<label>
									<input type="radio" name="gender" id="radio_male" value="0" checked>
									<?php echo $phrases['male']; ?>
								</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="gender" id="radio_female" value="1">
									<?php echo $phrases['female']; ?>
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['company']; ?></label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="company" id="company">
						</div>
					</div>
					<?php if($_GET['m'] != 'a'): ?>
						<div class="form-group">
							<label class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['department']; ?></label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="department" id="department">
							</div>
						</div>
					<?php endif; ?>
					<div class="form-group">
						<label class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['mobile_num']; ?><span class="required">*</span></label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="mobile_num" id="mobile_num">
							<span class="required" id="mobile_required" style="display:none"><?php echo $phrases['mobile_registered']; ?>.</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['email']; ?><span class="required">*</span></label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="email" id="email">
							<span class="required" id="email_required" style="display:none"><?php echo $phrases['email_registered']; ?>.</span>
						</div>
					</div>

					<!-- Uploads START -->
					<div class="form-group">
						<label class="col-sm-3 col-sm-3 control-label"><?= $phrases['id_card_admin']?><span class="required">*</span></label>
						<div class="col-sm-9">
							<input type="file" class="form-control" name="doc_id_card" id="doc_id_card" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-sm-3 control-label"><?= $phrases['gcp_card']?><span class="required">*</span></label>
						<div class="col-sm-9">
							<input type="file" class="form-control" name="doc_gcp_card" id="doc_gcp_card" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-sm-3 control-label"><?= $phrases['diploma']?><span class="required">*</span></label>
						<div class="col-sm-9">
							<input type="file" class="form-control" name="doc_diploma" id="doc_diploma" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-sm-3 control-label"><?= $phrases['degree_certificate']?><span class="required">*</span></label>
						<div class="col-sm-9">
							<input type="file" class="form-control" name="doc_degree" id="doc_degree" >
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 col-sm-3 control-label"><?= $phrases['other']?><span class="required">*</span></label>
						<div class="col-sm-9">
							<input type="file" class="form-control" name="doc_other" id="doc_other" >
						</div>
					</div>
					<!-- Uploads END -->

					<div class="form-group">
						<label for="password" class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['password']; ?><span class="required">*</span></label>
						<div class="col-sm-9">
							<input type="password" class="form-control" id="password" name="password" required>
						</div>
					</div>
					<div class="form-group">
						<label for="con_password" class="col-sm-3 col-sm-3 control-label"><?php echo $phrases['con_pass']; ?><span class="required">*</span></label>
						<div class="col-sm-9">
							<input type="password" class="form-control" id="con_password" name="" required>
							<span class="required" id="password_required" style="display:none"><?php echo $phrases['password_not_match']; ?>.</span>
						</div>
					</div>
				</div>
				<div class="modal-footer txtcenter nopad">
					<?php if($_GET['m'] == "a"): ?>
						<input type="hidden" value="A" name="position_type">
					<?php elseif($_GET['m'] == "c"): ?>
						<input type="hidden" value="A-CRO" name="position_type">
					<?php endif; ?>
					<button type="button" class="btn btn-warning" data-dismiss="modal"><?php echo $phrases['cancel']; ?></button>
					<button class="btn btn-info" id="final_submit" type="button" onclick="save_user('<?php echo $_GET['m']; ?>')" disabled><?php echo $phrases['submit']; ?></button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="editApplicant" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content pad30">
			<form role="form" class="form-horizontal tasi-form" method="POST" autocomplete="off" id="form_edit_user" onsubmit="javascript:;" onchange="" data-edit="">
				<!-- AJAX LOAD -->
			</form>
		</div>
	</div>
</div>
<?php else: ?>
	<div class="modal fade" id="addApplicant" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content pad30">
				<form role="form" class="form-horizontal tasi-form" method="POST" autocomplete="off" id="form_add_role" onsubmit="javascript:;" data-edit="">
					<div class="modal-header nobg nopad" style="background:#64aaf9">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h5 class="alert-modal-title"><?php echo $phrases['add_member']; ?></h5>
					</div>
					<div class="modal-body nopadR nopadL">
						<div class="form-group" id="prof_group_div">
							<label class="control-label col-md-3"><?php echo $phrases['add']; ?> <?php echo $user_types[$_GET['m']]; ?><span class="required">*</span></label>
							<div class="col-md-9">
								<select name="user_id[]" class="multi-select" multiple="multiple" id="my_multi_select3" >
									<option value="" selected></option>
									<?php foreach (getAllUsersByUserType(0, false, $get_acronym[$_GET['m']]) as $user_arr)
									{ ?>
										<option value="<?php echo $user_arr['user_id']; ?>"><?php echo $user_arr['fname']." ".$user_arr['lname']; ?></option>
										<?php
									} ?>
								</select>
							</div>
						</div>
						<?php if($_GET['m'] == 'e'): ?>
							<div class="form-group">
								<label class="control-label col-md-2"><?php echo $phrases['job_title']; ?><span class="required">*</span></label>
								<div class="col-md-7">
									<select class="form-control" name="position_type">
										<option value="ECM"><?php echo $phrases['ecm']; ?></option>
										<option value="ECC"><?php echo $phrases['ecc']; ?></option>
										<option value="ECS"><?php echo $phrases['ethics_committee_secretary']; ?></option>
									</select>
								</div>
							</div>
						<?php else: ?>
							<input  type="hidden" value="<?php echo $get_acronym[$_GET['m']]; ?>" name="position_type">
						<?php endif; ?>
					</div>
					<div class="modal-footer txtcenter nopad">
						<button type="button" class="btn btn-warning" data-dismiss="modal"><?php echo $phrases['cancel']; ?></button>
						<button class="btn btn-info" id="final_submit" type="button" onclick="save_role('<?php echo $_GET['m']; ?>', false)"><?php echo $phrases['submit']; ?></button>
					</div>
				</form>
			</div>
		</div>
	</div>

<?php endif; ?>
<div class="modal fade" id="editApplicant" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content pad30">
			<form role="form" class="form-horizontal tasi-form" method="POST" autocomplete="off" id="form_edit_user" onsubmit="javascript:;" onchange="" data-edit="">
				<!-- AJAX LOAD -->
			</form>
		</div>
	</div>
</div>
