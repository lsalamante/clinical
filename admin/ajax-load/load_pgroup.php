<?php
include('../../jp_library/jp_lib.php');
include('../../php-functions/fncCommon.php');
?>
						    <style>
			                span.required
			                {
			                	margin-left: 3px;
			                	color: red;
			                }
			                .list-group-item.active
			                {
			                	background-color: #83bdff;
			                	border-color: #83bdff;
			                }
			             	.list-group-item.active:hover
			             	{
			             		background-color: #83bdff;
			                	border-color: #83bdff;
			             	}
			                </style>
		                	<div class="col-lg-3">
		                      <section class="panel">
		                          <header class="panel-heading" style="font-size:17px;">
		                              <b><?php echo $phrases['professional_group'] ?></b>
		                          </header>
		                          <div class="list-group" style="font-size:16px">
		                          <?php foreach (getProfGroup() as $value) { ?>
		                          	<a class="list-group-item" href="javascript:loadpgroup('<?php echo $value['pgroup_id']; ?>', '<?php echo $value['pgroup_name']; ?>');";><?php echo $value['pgroup_name']; ?></a>
		                          <?php } ?>
		                          	<div class="list-group-item">
		                          		<a class="btn btn-block btn-info" href="#addPGroup" data-toggle="modal"><i class="fa fa-plus"></i>&nbsp;<?php echo $phrases['add_group']; ?></a>
		                          	</div>
		                          </div>
		                      </section>
		                  	</div>
		                 <div class="col-sm-9">
		                  <section class="panel">
		                    <header class="panel-heading" style="padding-bottom: 15px;" id="pgroup_header">
		                   		<?php echo $phrases['professional_group']; ?>
		                        <span class="tools pull-right">
		                          <a id="add_btn_member" href="#addMember" data-toggle="modal" class="btn btn-info pull-right" style="margin-bottom:5px; color:white;display:none;" onclick="$('.ms-elem-selectable').attr('class', 'ms-elem-selectable').attr('style', '');$('.ms-elem-selection').attr('class', 'ms-elem-selection').attr('style', 'display:none');"><i class="fa fa-plus"></i>&nbsp;<?php echo $phrases['add_applicant']; ?></a>
		                       	</span>
		                    </header>
		                  <section class="panel">
			                <div class="panel-body">
			                  <div class="adv-table" id="pgroup_inside">
			                  <table class="display table table-bordered" id="hidden-table-info">
								<thead>
								  <tr>
								    <th><?php echo $phrases['name']; ?></th>
								    <th><?php echo $phrases['gender']; ?></th>
								    <th><?php echo $phrases['department']; ?></th>
								    <th><?php echo $phrases['mobile_num']; ?></th>
								    <th><?php echo $phrases['position']; ?></th>
								    <th style="width:100px;"><?php echo $phrases['status']; ?></th>
								    <th style="width:15%"><?php echo $phrases['action']; ?></th>
								  </tr>
								</thead>
								<tbody>
								</tbody>
							   </table>
			                  </div>
		                	</div>
		                  </section>
		                </section>
		               </div>
		               <div class="modal fade" id="editApplicant" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				          <div class="modal-dialog modal-lg">
				              <div class="modal-content pad30">
				              	<form role="form" class="form-horizontal tasi-form" method="POST" autocomplete="off" id="form_edit_user" onsubmit="javascript:;" onchange="" data-edit="">
				                  <!-- AJAX LOAD -->
				                </form>
				              </div>
				          </div>
				        </div>

				       <div class="modal fade" id="addMember" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				          <div class="modal-dialog modal-lg">
				              <div class="modal-content pad30">
				              <form role="form" class="form-horizontal tasi-form" method="POST" autocomplete="off" id="form_add_role" onsubmit="javascript:;" data-edit="">
				                  <div class="modal-header nobg nopad" style="background:#64aaf9">
				                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                      <h5 class="alert-modal-title">Add Professional Group Member</h5>
				                  </div>
				                  <div class="modal-body nopadR nopadL">
				                    <div class="form-group last" id="prof_group_div">
				                      <label class="control-label col-md-3">Add Group Member<span class="required">*</span></label>
				                      <div class="col-md-9">
				                        <select name="user_id[]" class="multi-select" multiple="multiple" id="my_multi_select3" >
				                        <option value="" selected></option>
				                          <?php foreach (getAllUsersByUserType(0, false, "pg") as $user_arr)
				                          { ?>
				                          <option value="<?php echo $user_arr['user_id']; ?>"><?php echo $user_arr['fname']." ".$user_arr['lname']; ?></option>
				                          <?php
				                          } ?>
				                        </select>
				                      </div>
				                    </div>

				                    <div class="form-group">
				                    	<label class="control-label col-md-2">Job Title<span class="required">*</span></label>
				                    	<div class="col-md-7">
					                    	<select class="form-control" name="position_type">
					                    	<?php
					                    	$allowed_positions = array("PI", "I", "QC");
					                    	?>
					                    		<?php foreach (getPositions(false, true) as $value) { ?>
					                    		   <?php if(in_array($value['acronym'], $allowed_positions)) { ?>
					                    			<option value="<?php echo $value['acronym']; ?>"><?php echo $value['description']; ?></option>
					                    			<?php } ?>
					                    		<?php } ?>
					                    	</select>
				                    	</div>
				                    </div>

				                  </div>
				                  <div class="modal-footer txtcenter nopad">
				                  	<input name="pgroup_id" id="pgroup_id_add" hidden>
				                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
				                    <button class="btn btn-info" id="final_submit_pgroup" type="button" onclick="">Submit</button>
				                  </div>
				                </form>
				              </div>
				          </div>
				        </div>

				        <div class="modal fade" id="addPGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				          <div class="modal-dialog">
				              <div class="modal-content pad30">
				              <form role="form" class="form-horizontal tasi-form" method="POST" autocomplete="off" id="form_add_pgroup" onsubmit="javascript:;" data-edit="">
				                  <div class="modal-header nobg nopad" style="background:#64aaf9">
				                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                      <h4 class="alert-modal-title">Add Professional Group</h4>
				                  </div>
				                  <div class="modal-body nopadR nopadL">
				                    <div class="form-group" id="prof_group_div">
				                      <label class="control-label col-md-3">Group Name<span class="required">*</span></label>
				                      <div class="col-md-9">
				                        <input type="text" class="form-control" name="pgroup_name" id="pgroup_name">
				                      </div>
				                    </div>
				                  <div class="modal-footer txtcenter">
				                    <button type="button" class="btn btn-warning" data-dismiss="modal"><?php echo $phrases['cancel']; ?></button>
				                    <button class="btn btn-info" id="final_submit" type="button" onclick="save_pgroup();"><?php echo $phrases['submit']; ?></button>
				                  </div>
				                </form>
				              </div>
				          </div>
				       </div>
