<?php
include('../../jp_library/jp_lib.php');
include('../../php-functions/fncCommon.php');
?>
<table class="display table table-bordered" id="hidden-table-info">
<thead>
  <tr>
    <th><?php echo $phrases['name']; ?></th>
    <th><?php echo $phrases['gender']; ?></th>
    <th><?php echo $phrases['department']; ?></th>
    <th><?php echo $phrases['mobile_num']; ?></th>
    <th><?php echo $phrases['position']; ?></th>
    <th style="width:100px;"><?php echo $phrases['status']; ?></th>
    <th style="width:15%"><?php echo $phrases['action']; ?></th>
  </tr>
</thead>
<tbody id="pgroup_inside">
	<?php
	$temp_count = getProfGroupMembers(false,$_GET['id']);
	if(count($temp_count)>0):
		foreach($temp_count as $member){ ?>
			<tr>
				<td><?php echo $member['fname']." ".$member['lname']; ?></td>
				<td><?php echo $member['gender'] == "0" ? $phrases['male'] : $phrases['female']; ?></td>
				<td><?php echo $member['department']; ?></td>
				<td><?php echo $member['mobile_num']; ?></td>
				<td><?php echo $phrases[$member['lang_phrase']]; ?></td>
				<td><?php echo $member['is_active'] == 1 ? $phrases['activated'] : $phrases['frozen']; ?></td>
				<td>
					<a href="#editApplicant" data-toggle="modal" class="btn btn-primary btn-xs edit_user_details" onclick="ready_edit('<?php echo $member['user_id']; ?>', '<?php echo $member['role_id']; ?>', '<?php echo $_GET['id']; ?>', '1');"><i class="fa fa-pencil"></i></a>
					<button class="btn btn-primary btn-xs" onclick="show_status_modal(<?php echo $member['is_active']; ?>, <?php echo $member['user_id']; ?>, '<?php echo $_GET['id']; ?>', true);"><i class="fa fa-<?php echo $member['is_active'] == 1 ? "lock" : "unlock-alt"; ?>"></i></button>
				</td>
			</tr>
	<?php }
	else: ?>
	<tr>
		<td colspan = "7">
			<center>No data available in table</center>
		</td>
	</tr>

	<?php endif; ?>

	</tbody>
</table>
