<?php
include('../../jp_library/jp_lib.php');
include('../../php-functions/fncCommon.php'); ?>
                          <thead>
                              <tr>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Birthdate</th>
                                <th>Position</th>
                                <th style="width:15px">Professional Group</th>
                                <th style="width:100px;">Status</th>
                                <th style="width:15px">Action</th>
                              </tr>
                          </thead>
                          <tbody id="table_content">
                            <?php
                            foreach (getAllUsers() as $users_arr) { ?>
                            <tr id="row_user_id_<?php echo $users_arr['user_id']; ?>">
                              <td><?php echo $users_arr['fname']." ".$users_arr['lname']; ?></td>
                              <td><?php echo $users_arr['mobile_num']; ?></td>
                              <td><?php echo $users_arr['email']; ?></td>
                              <td><?php echo $users_arr['birthdate']; ?></td>
                              <?php
                              if($users_arr['is_admin'] == 1)
                              {
                                $pos = "Admin";
                              }
                              else
                              {
                                $pos = $users_arr['description'];
                              }
                              ?>
                              <td><?php echo $pos; ?></td>
                              <td><?php echo $users_arr['pgroup_name'] != '' ? $users_arr['pgroup_name'] : $phrases['not_applicable']; ?></td>
                              <td><?php echo $users_arr['is_active'] == 1 ? $phrases['activated'] : $phrases['frozen']; ?></td>
                              <td>None</td>

                            </tr>
                            <?php
                            }
                            ?>
                          </tbody>
