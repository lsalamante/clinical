<?php
/*************************************************************************************************/
/*
Description: User Maintenance
Ajax Files Dependents (update also these files if you update this PHP file): 
    * load_add_register.php
    * load_edit_register.php
Created by: Vanananana
NOTES: ajax load id="table_inside" --> refer to left-sidebar.php and ajax-load/load_general.php
*/

include('../jp_library/jp_lib.php');
$REGISTER_PAGE = true;
$PICKERS = true;
$DYNAMIC_TABLE = true;
include('header.php');
include('../php-functions/fncCommon.php');
?>
<!--main content start-->
    <section id="main-content" style="background:#f9f9f9">
       <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row" id="table_inside">
        </div>
      </section>
    </section>
    <div class="modal fade" id="accout_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-sm">
              <div class="modal-content pad30">
                  <div class="modal-header nobg nopad" style="background:#64aaf9">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h5 class="alert-modal-title">System Message</h5>
                  </div>
                  <div class="modal-body nopadR nopadL">
                    <ul class="pop-ul">
                      <center id="status_msg" style="font-size:15px"></center>
                    </ul>
                  </div>
                  <div class="modal-footer txtcenter nopad">
                    <button class="btn btn-info" data-dismiss="modal" id="change_status_btn" type="button">Ok</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                  </div>
              </div>
          </div>
        </div>
<?php include('footer.php'); ?>
<?php include('user_management_scripts.php'); ?>