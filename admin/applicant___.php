<?php
/*************************************************************************************************/
/*
Description: User Maintenance
Ajax Files Dependents (update also these files if you update this PHP file):
    * load_add_register.php
    * load_edit_register.php
Created by: Vanananana
*/

include('../jp_library/jp_lib.php');
$REGISTER_PAGE = true;
$PICKERS = true;
$DYNAMIC_TABLE = true;
include('header.php');
include('../php-functions/fncCommon.php');
?>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
          <!-- page start-->
          <div class="row">
                <div class="col-sm-12">
                  <section class="panel">
                    <header class="panel-heading">
                        Admin List
                        <span class="tools pull-right">
                          <button class="btn btn-info btn-xs pull-right" onclick="$('#add_user').slideToggle();" id="toggle_button"><i class="fa fa-plus"></i>&nbsp;Add User</button>
                       </span>
                    </header>
                    <div class="col-sm-5" style="display:none" id="add_user" data-show='0'>
         		<section class="panel">
                <div class="panel-body">
                <style>
                span.required
                {
                	margin-left: 3px;
                	color: red;
                }
                </style>
                    <form role="form" method="POST" autocomplete="off" id="form_add_user" onsubmit="javascript:;" data-edit="0" onchange="form_validate();">
                      	<div class="form-group">
                      		<label>Name</label><span class="required">*</span>
                      		<input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" required >
                      		<input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" style="margin-top:4px;" required>
                      		<!--<input type="text" class="form-control" id="middle_initial" name="middle_initial" placeholder="Middle Initial">-->
                      	</div>
                      	<div class="form-group">
                      		<label>Mobile Number</label><span class="required">*</span>
                      		<input type="text" class="form-control" id="mobile_num" name="mobile_num" required>
                          <span class="required" id="mobile_required" style="display:none">Mobile number is already registered.</span>
                      	</div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label><span class="required">*</span>
                            <input type="email" class="form-control" id="email" name="email">
                            <span class="required" id="email_required" style="display:none">Email is already registered.</span>
                        </div>
                        <div class="form-group">
                            <label for="username">Birthdate</label>
                            <input type="text" class="form-control" id="birthdate" name="birthdate">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label><span class="required">*</span>
                            <input type="password" class="form-control" id="password" name="password" required>
                            <!--<input type="checkbox" id="sp" name="sp[]" value="true"><label for="sp">Show Password</label>-->
                        </div>
                        <div class="form-group">
                            <label for="con_password">Confirm Password</label>
                            <input type="password" class="form-control" id="con_password" name="" required>
                            <span class="required" id="password_required" style="display:none">Password does not match.</span>
                            <!--<input type="checkbox" id="sp" name="sp[]" value="true"><label for="sp">Show Password</label>-->
                        </div>
                        <div class="form-group">
                        	<label for="admin">Is Admin?</label>
                        	<input type="checkbox" name="admin[]" id="admin[]" value="1" onclick="is_admin()">
                        </div>
                        <div class="form-group" id="pos_div">
                        	<label for="position_id">Position</label><span class="required">*</span>
                        	<select name="position_id" id="position_id" class="form-control" onchange="check_prof();" required>
                        		<option value="" selected>Select Position</option>
                        	<?php foreach (getPositions() as $position_arr) { ?>
                        		<option value="<?php echo $position_arr['position_id']; ?>" data-profgroup="<?php echo $position_arr['is_professional_group']; ?>"><?php echo $position_arr['description']; ?></option>
                        	<?php
                        	} ?>
                        	</select>
                        </div>
                        <div class="form-group last" id="prof_group_div" style="display:none">
                          <label class="control-label col-md-3">Professional Group</label><span class="required">*</span>
                          <div class="col-md-9">
                            <select name="pgroup_id[]" class="multi-select" multiple="multiple" id="my_multi_select3" >
                              <?php foreach (getProfGroup() as $pgroup_arr)
                              { ?>
                              <option value="<?php echo $pgroup_arr['pgroup_id']; ?>"><?php echo $pgroup_arr['pgroup_name']; ?></option>
                              <?php
                              } ?>
                            </select>
                          </div>
                        </div>
                        <!--<button type="button" class="btn btn-info" name="admin" id="add-without-image">Try Message</button>-->
                        <!--<button type="button" class="btn btn-info" name="admin" onclick="alertme()">Try</button-->
                        <button type="button" class="btn btn-info" name="admin" onclick="save_user()" id="final_submit" disabled='disabled' style="margin-top:10px">Submit</button>
                        <button type="button" class="btn btn-warning" onclick="$('#add_user').slideToggle();" style="margin-top:10px">Cancel</button>
                    </form>
                </div>
                </section>
              </div>
              <div class="panel-body">
                  <div class="adv-table">
                      <table class="display table table-bordered" id="hidden-table-info">
                          <thead>
                              <tr>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Birthdate<br>mm/dd/yyyy</th>
                                <th>Position</th>
                                <th>Professional Group</th>
                                <th style="width:15px;">Status</th>
                                <th style="width:15px">Action</th>
                              </tr>
                          </thead>
                          <tbody id="table_content">
                            <?php
                            foreach (getAllUsers() as $users_arr) { ?>
                            <tr id="row_user_id_<?php echo $users_arr['user_id']; ?>">
                              <td><?php echo $users_arr['fname']." ".$users_arr['lname']; ?></td>
                              <td><?php echo $users_arr['mobile_num']; ?></td>
                              <td><?php echo $users_arr['email']; ?></td>
                              <td><?php echo date("m/d/Y", strtotime($users_arr['birthdate'])); ?></td>
                              <?php
                              if($users_arr['is_admin'] == 1)
                              {
                                $pos = "Admin";
                              }
                              else
                              {
                                $pos = $users_arr['description'];
                              }
                              ?>
                              <td><?php echo $pos; ?></td>
                              <td><?php echo $users_arr['pgroup_name'] != '' ? $users_arr['pgroup_name'] : $phrases['not_applicable']; ?></td>
                              <td><?php echo $users_arr['is_active'] == 1 ? "Active" : "Disabled"; ?></td>
                              <td>
                                <button class="btn btn-primary btn-xs edit_user_details" onclick="ready_edit('<?php echo $users_arr['user_id']; ?>');"><i class="fa fa-pencil"></i></button>
                                <button class="btn btn-primary btn-xs" onclick="show_status_modal(<?php echo $users_arr['is_active']; ?>, <?php echo $users_arr['user_id']; ?>);"><i class="fa fa-<?php echo $users_arr['is_active'] == 1 ? "lock" : "unlock-alt"; ?>"></i></button>
                              </td>
                            </tr>
                            <?php
                            }
                            ?>
                          </tbody>
                      </table>
                  </div>
                  </div>
                </div>
                </section>
                </div>
        <!-- page end-->

        <div class="modal fade" id="accout_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-sm">
              <div class="modal-content pad30">
                  <div class="modal-header nobg nopad">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h5 class="alert-modal-title">System Message</h5>
                  </div>
                  <div class="modal-body nopadR nopadL">
                    <ul class="pop-ul">
                      <center id="status_msg" style="font-size:15px"></center>
                    </ul>
                  </div>
                  <div class="modal-footer txtcenter nopad">
                    <button class="btn btn-info" data-dismiss="modal" id="change_status_btn" type="button" onclick="change_user_status();">Ok</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                  </div>
              </div>
          </div>
        </div>


        </section>
    </section>
    <!--main content end-->
<!--footer start-->
<?php include('footer.php'); ?>
<!--footer end-->
<script>
$('#birthdate').datepicker();   // for date picker in addin users
var FORM_VALID = false;         // for form validation
/*************************************************************************************************/
/*
Description: onchange attr of <form>
Purpose: -- Form Validation
         -- Check avaialability of unique data (i.e.: email) in TABLE: `users`
Parameters: none
Created by Developer: Vanananana
*/


/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/

function form_validate()
{
    // Check Mobile Num (used for logged in) && Email
    FORM_VALID = true;
    var form_data={
          mobile_num:$('#mobile_num').val(),
          email:$('#email').val(),
          user_id:$('#form_add_user').data('edit')
        };

    $.ajax({
      url:"../php-functions/fncUserManagement.php?action=checkUserAvailability",
      type:"POST",
      data:form_data,
      success:function(data)
      {
        var res = JSON.parse(data);
        if(res["mobile"] == 1)
        {
          $('#mobile_required').attr('style', 'display:block');
          $('#final_submit').attr('disabled', 'disabled');
          FORM_VALID = false;
        }
        else
          $('#mobile_required').attr('style', 'display:none');

        if(res["email"] == 1)
        {
          $('#email_required').attr('style', 'display:block');
          $('#final_submit').attr('disabled', 'disabled');
          FORM_VALID = false;
        }
        else
          $('#email_required').attr('style', 'display:none');
      }
    });

    // check if passwords match
    if($('#password').val() != $('#con_password').val() && $('#password').val() != '' && $('#con_password').val() != '')
    {
      $('#password_required').attr('style', 'display:block');
      FORM_VALID = false;
    }
    else
      $('#password_required').attr('style', 'display:none');

    // check if all required fields are not empty
    if(FORM_VALID)
    {
      $('#form_add_user').find(':required').each(function(){
        //alert($(this).val());
        if($(this).val() == '')
        {
          FORM_VALID = false;
          $('#final_submit').attr('disabled', 'disabled');
        }
      });
      if(FORM_VALID)
      {
          $('#final_submit').removeAttr('disabled');
      }
    }
}
/*************************************************************************************************/
/*
Description: Saving of new user details in TABLE: `users`
Purpose: -- save user registration in form
         -- check php-functions/fncUserManagement.php --> function addUser() for PHP details
Parameters: none
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/

function save_user()
{
  if(!FORM_VALID)
  {
    show_alert('Please make sure the details are correct.', '', 0);
  }
  else
  {
    var form_data={
    data:$('#form_add_user').serialize()
    };
    $.ajax({
      url:"../php-functions/fncUserManagement.php?action=addUser",
      type:"POST",
      data:form_data,
      success:function(msg)
      {
        if(msg != 0)
        {
          show_alert("Successfully registered new user.", 'register.php', 1);
          /** PS: function show_alert(msg, redirect, type) --> footer.php **/
        }
        else
        {
          show_alert("Registration failed. Please try again.", '', 0);
        }
      }
    });
  }
}
/*************************************************************************************************/
/*
Description: Updating of users in TABLE: `users`
Purpose: -- check php-functions/fncUserManagement.php --> function updateUser() for PHP details
Parameters: none
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/

function update_user()
{
 if(!FORM_VALID)
  {
    show_alert('Please make sure the details are correct.', '', 0);
  }
  else
  {
    var form_data={
    data:$('#form_add_user').serialize(), // NOTE: user_id for update is in hidden input, check --> ajax-load/load_edit_register.php
    };
    $.ajax({
      url:"../php-functions/fncUserManagement.php?action=updateUser",
      type:"POST",
      data:form_data,
      success:function(msg)
      {
        if(msg != 0)
        {
          show_alert("Successfully updated user.", 'register.php', 1);
          /** PS: function show_alert(msg, redirect, type) --> footer.php **/
        }
        else
        {
          show_alert("Update failed. Please try again.", '', 0);
        }
      }
    });
  }
}

/*************************************************************************************************/
/*
Description: Show hide professional group, based on position selected
Purpose: for Form validation
Parameters: none
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/

function check_prof()
{
	$('#prof_group_div').attr('style', 'display:none');
	if($('#position_id').find(":selected").data('profgroup') == 1)
	{
		$('#prof_group_div').attr('style', 'display:block');
	}
}
/*************************************************************************************************/
/*
Description: check if registered user is_admin(), used by is admin checkbox in form
Purpose: for Form validation
Parameters: none
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/

function is_admin()
{
	var checked = $('input[name="admin[]"]:checked').length;
	if(checked == 1){
	  $('#pos_div').attr("style", "display:none");
    $('#prof_group_div').attr("style", "display:none");
	}else{
	  $('#pos_div').attr("style", "display:block");
    check_prof();
	}
}
/*************************************************************************************************/
/*
Description: ready <form> details for editing before div.slidetoggle
Purpose: editing users in table w/o loading the page
Parameters:
  - userId => `user_id` in `users`, passed via the edit button
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/

function ready_edit(userId)
{
  $('#form_add_user').attr('data-edit', userId);
  var data={
    user_id:userId
  };
  ajax_load(data, 'load_edit_register.php', 'form_add_user');
  $('#toggle_button').html('<i class="fa fa-plus"></i>&nbsp;Edit User');
  $('#toggle_button').pulsate({
            color: "#00b5f9",
            reach: 300,
            repeat: 1,
            speed: 15,
            glow: true
        });
  $('#toggle_button').focus();
  $('#add_user').slideDown();
  /** PS: function function ajax_load(form_data, file_url, element_id) --> footer.php **/
}
/*************************************************************************************************/
/*
Description: reset <form> details for adding user
Purpose: change back to add functionality for form
Parameters: none
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/

function cancel_edit()
{
  $('#form_add_user').attr('data-edit', '0');
  var data={
    none:'none'
  };
  ajax_load(data, 'load_add_register.php', 'form_add_user');
  $('#toggle_button').html('<i class="fa fa-plus"></i>&nbsp;Add User');
  $('#toggle_button').pulsate({
            color: "#00b5f9",
            reach: 300,
            repeat: 1,
            speed: 10,
            glow: true
        });
  $('#toggle_button').focus();
  $('#add_user').slideToggle();
}
/*************************************************************************************************/
/*
Description: for disabling/enabling accounts
Purpose: change is_active to 0/1 in TABLE: `users`
Parameters: -- current_status => current value of is_active in TABLE: `users`
            -- user_id => user_id in TABLE: `users`
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/

function show_status_modal(current_status, user_id)
{
  $(".modal").modal("hide");
  if(current_status == 1)
  {
    $('#status_msg').html("Are you sure you want to disable account?");
    $('#change_status_btn').attr("onclick", "change_user_status('0', '"+user_id+"')");
  }
  else
  {
    $('#status_msg').html("Are you sure you want to activate account?");
    $('#change_status_btn').attr("onclick", "change_user_status('1', '"+user_id+"')");
  }
  $("#accout_status").modal('show');

}
/*************************************************************************************************/
/*
Description: Activate/Disable Account
Purpose: change is_active value to 0/1
Parameters: -- status_value => (passed via the modal button Ok) active to be set
            -- userId => user_id in TABLE: `users`
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/

function change_user_status(status_value, userId)
{

  var form_data={
    is_active:status_value,
    user_id:userId
  };
    $.ajax({
      url:"../php-functions/fncUserManagement.php?action=changeStatus",
      type:"POST",
      data:form_data,
      success:function(msg)
      {
        if(msg != 0)
        {
          show_alert("Successfully change status of user.", 'register.php', 1);
          /** PS: function show_alert(msg, redirect, type) --> footer.php **/
        }
        else
        {
          show_alert("Change of status failed. Please try again.", '', 0);
        }
      }
    });
}
/*************************************************************************************************/
/** BETA TESTING WORTHLESS SCRIPT **/
/*var Gritter = function () {
  $('#add-without-image').click(function(){

        $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: 'This is a notice without an image!',
            // (string | mandatory) the text inside the notification
            text: '<ul>
                <li>Hellow</li>
                <li>Hi</li>
              </ul>'
        });

        return false;
    });

}();*/
/*$('#toggle_button').click(function(){
  if($('#form_add_user').data('edit') == '0')
  {
    $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Instructions',
        // (string | mandatory) the text inside the notification
        text: '<ul>
                <li>Hellow</li>
                <li>Hi</li>
              </ul>'
    });

    return false;
  }

});*/

/*function loadTable(id){
  //$('#hidden-table-info').html("载入中...");
  var form_data={
    user_id:id
  };
  var final_url = "ajax-load/load_table_register.php?";
  $.each(form_data, function( key, value ){
    final_url += key+"="+value+"&";
  });
  var xmlhttp;
    if (window.XMLHttpRequest){
      // code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
    }else{
      // code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function(){
      if (xmlhttp.readyState==4 && xmlhttp.status==200){
        document.getElementById("hidden-table-info").innerHTML=xmlhttp.responseText;
        $('#row_user_id_'+id).attr('style', 'border-style:solid; border-color:aqua;');
        //$('#searchParent').html("Search");
      }
    }
    xmlhttp.open("GET",final_url,true);
    xmlhttp.send();
}*/




</script>
