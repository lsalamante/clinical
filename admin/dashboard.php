<?php
//if (!isset($_SESSION['is_logged_in'])) {
//    header("Location: " . "login.php");
//    die();
//}

include('../jp_library/jp_lib.php');
$REGISTER_PAGE = true;
$PICKERS = true;
$DYNAMIC_TABLE = true;
include('../php-functions/fncCommon.php');
include('header.php');

?>
<!--main content start-->
    <section id="main-content">
       <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row" id="table_inside">
        <!-- AJAX LOAD -->
        </div>
      </section>
    </section>
    <div class="modal fade" id="accout_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-sm">
              <div class="modal-content pad30">
                  <div class="modal-header nobg nopad" style="background:#64aaf9">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h5 class="alert-modal-title"><?php echo $phrases['system_message']; ?></h5>
                  </div>
                  <div class="modal-body nopadR nopadL">
                    <ul class="pop-ul">
                      <center id="status_msg" style="font-size:15px"></center>
                    </ul>
                  </div>
                  <div class="modal-footer txtcenter nopad">
                    <button type="button" class="btn btn-warning" data-dismiss="modal"><?php echo $phrases['cancel']; ?></button>
                    <button class="btn btn-info" data-dismiss="modal" id="change_status_btn" type="button"><?php echo $phrases['ok']; ?></button>
                  </div>
              </div>
          </div>
        </div>
<?php include('footer.php'); ?>
<?php include('user_management_scripts.php'); ?>