<div class="modal fade" id="alert_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content pad30">
          <div class="modal-header nobg nopad" style="background:#64aaf9">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="alert-modal-title"><?= $phrases['system_message'] ?></h4>
          </div>
          <div class="modal-body nopadR nopadL">
            <ul class="pop-ul">
              <h4><center id="alert_message"></center></h4>
              <input id="alert_redirect_value" hidden>
            </ul>
          </div>
          <!--<div class="result" style=" padding: 10px 0 10px 0; text-align: center; font-weight: bold;"></div>-->
          <div class="modal-footer txtcenter nopad">
            <button class="btn btn-success" data-dismiss="modal" id="declare_btn" type="button" onclick="reload_page();"><?= $phrases['ok'] ?></button>
          </div>
      </div>
  </div>
</div>
<footer class="site-footer">
    <div class="text-center"> 2016 &copy;
        <?php echo $SITE_NAME ?>
        <a href="#" class="go-top"> <i class="fa fa-angle-up"></i> </a>
    </div>
</footer>
</section> <!-- close section in opening section #container -->
<?php include('scripts.php'); ?>
<script>

/*************************************************************************************************/
/*
Description: dynamically reloading the page, uses by show_alert()
Purpose: for alerts
Parameters: none
Created by: Vanananana
*/

/* If Updated by Others than the Creator
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s) :
*/

function reload_page(){
	var redirect = $('#alert_redirect_value').val();
	if(redirect != ''){
	  location.href = redirect;
	}
}
/*************************************************************************************************/
/*
Description: #NoMoreDefaultJavaAlertPlease
Purpose: for modal alerts/system messages
Parameters:
	- msg => System Message
	- redirect => .php file link to redirect
	- type => 1 (positive look, green text) and 0 (negative look, red look)
Created by: Vanananana
*/

/* If Updated by Others than the Creator
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s) :
*/

function show_alert(msg, redirect, type){

    $(".modal").modal("hide");
    //type = 1 -> success 0 -> error
    if(redirect != ''){
      $('#alert_redirect_value').val(redirect);
    }
    $('#alert_message').html(msg);
    $('#alert_message').css({color:'#64aaf9'});
    if(type == 0){
      $('#alert_message').css({color:'red '});
    }
    $("#alert_modal").modal('show');

}
/******************************************/
function loadusers(utype)
{
	var data = {
		m:utype
	}
	var url = "load_general.php";
	if(utype=='pg')
	{
		url = "load_pgroup.php";
	}
	ajax_load(data, url, 'table_inside');
}

function loadpgroup(pgroup_id, pgroup_value)
{
	var data = {
		id:pgroup_id
	}
	// for adding members
	$('#pgroup_id_add').val(pgroup_id);
	//$('#pgroup_header').html(pgroup_value);
	$('#final_submit_pgroup').attr("onclick", "save_role("+pgroup_id+", true)");
	$('#add_btn_member').attr('style', 'margin-bottom:5px;color:white;display:block;');
	ajax_load(data, 'load_pgroup_table.php', 'pgroup_inside');
}
/*************************************************************************************************/
/*
Description: uses ajax, compatible with all browsers
Purpose: loads html elements contents using ajax
Parameters:
	- form_data => converted to $_GET variables for php handling
	- file_url => path to ajax-load default, includes php handled data of element contents
	- element_id => element id contents that is handled/loaded
Created by: Vanananana
*/

/* If Updated by Others than the Creator
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s) :
*/

function ajax_load(form_data, file_url, element_id)
{

	var final_url = "ajax-load/"+file_url+"?";
	$.each(form_data, function( key, value ){
	final_url += key+"="+value+"&";
	});
	var xmlhttp;
	if (window.XMLHttpRequest){
	  // code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}else{
	  // code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200){
	    document.getElementById(element_id).innerHTML=xmlhttp.responseText;
	    if(element_id == 'table_inside')
	    {
	    	/** START specific script needed for edit users in register.php **/

		    $('#dynamic-table').dataTable( {
		        "aaSorting": [[ 0, "desc" ]]
		    } );
		    var oTable = $('#hidden-table-info').dataTable( {
		        "aoColumnDefs": [
		            { "bSortable": false, "aTargets": [  ] }
		        ],
		        "aaSorting": [[0, 'asc']]
		    });

		    /* Add event listener for opening and closing details
		     * Note that the indicator for showing which row is open is not controlled by DataTables,
		     * rather it is done here
		     */
		    $(document).on('click','#hidden-table-info tbody td img',function () {
		        var nTr = $(this).parents('tr')[0];
		        if ( oTable.fnIsOpen(nTr) )
		        {
		            /* This row is already open - close it */
		            this.src = "img/details_open.png";
		            oTable.fnClose( nTr );
		        }
		        else
		        {
		            /* Open this row */
		            this.src = "img/details_close.png";
		            oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
		        }
		    } );

		    $('#my_multi_select3').multiSelect({
		        selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='搜索'>",
		        selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='搜索'>",
		        afterInit: function (ms) {
		            var that = this,
		                $selectableSearch = that.$selectableUl.prev(),
		                $selectionSearch = that.$selectionUl.prev(),
		                selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
		                selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

		            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
		                .on('keydown', function (e) {
		                    if (e.which === 40) {
		                        that.$selectableUl.focus();
		                        return false;
		                    }
		                });

		            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
		                .on('keydown', function (e) {
		                    if (e.which == 40) {
		                        that.$selectionUl.focus();
		                        return false;
		                    }
		                });
		        },
		        afterSelect: function () {
		            this.qs1.cache();
		            this.qs2.cache();
		        },
		        afterDeselect: function () {
		            this.qs1.cache();
		            this.qs2.cache();
		        }
		    });
	    }
	    if(element_id == "pgroup_inside")
	    {
	    	/** START specific script needed for edit users in register.php **/

		    $('#dynamic-table').dataTable( {
		        "aaSorting": [[ 0, "desc" ]]
		    } );
		    var oTable = $('#hidden-table-info').dataTable( {
		        "aoColumnDefs": [
		            { "bSortable": false, "aTargets": [  ] }
		        ],
		        "aaSorting": [[0, 'asc']]
		    });

		    /* Add event listener for opening and closing details
		     * Note that the indicator for showing which row is open is not controlled by DataTables,
		     * rather it is done here
		     */
		    $(document).on('click','#hidden-table-info tbody td img',function () {
		        var nTr = $(this).parents('tr')[0];
		        if ( oTable.fnIsOpen(nTr) )
		        {
		            /* This row is already open - close it */
		            this.src = "img/details_open.png";
		            oTable.fnClose( nTr );
		        }
		        else
		        {
		            /* Open this row */
		            this.src = "img/details_close.png";
		            oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
		        }
		    } );
	    }
	    /** END specific script needed for edit users in register.php **/
	  }
	}
	xmlhttp.open("GET",final_url,true);
	xmlhttp.send();
}

$(document).ready(function(){

    $('#lang_cn').on('click', function(){
      $.ajax({
        url:"../php-functions/fncCommon.php?action=langToCN",
        success:function(msg)
        {
          show_alert("语言已更改", "<?php echo $PAGE_NAME; echo (isset($_GET['project_id'])) ? '?project_id='.$_GET['project_id'] : '' ; ?>", 1);
        }
      });
    });

    $('#lang_en').on('click', function(){
      $.ajax({
        url:"../php-functions/fncCommon.php?action=langToEN",
        success:function(msg)
        {
          show_alert("Language changed", "<?php echo $PAGE_NAME; echo (isset($_GET['project_id'])) ? '?project_id='.$_GET['project_id'] : '' ; ?>", 1);
        }
      });
    });

  });

</script>
</body>
</html>
