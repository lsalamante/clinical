<?php
#INCLUDES
include('../jp_library/jp_lib.php');

if (isset($_SESSION['admin'])) {
    //header("Location: " . "dashboard.php");
    //die();
}

?>
<!DOCTYPE html>
<html lang="en">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="../img/favicon.png">
    <title><?php echo $SITE_NAME ?></title>
    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/custom.css" rel="stylesheet">
    <link href="../css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="../assets/font-awesome/css/font-awesome.css" rel="stylesheet"/>
    <link href="../assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css"
          media="screen"/>
    <link rel="stylesheet" href="../css/owl.carousel.css" type="text/css">

    <?php if ($DYNAMIC_TABLE) { ?>
        <!--dynamic table-->
        <link href="../assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet"/>
        <link href="../assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet"/>
        <link rel="stylesheet" href="../assets/data-tables/DT_bootstrap.css"/>
    <?php } ?>

    <?php if ($PICKERS) { ?>
        <link rel="stylesheet" type="text/css" href="../assets/bootstrap-fileupload/bootstrap-fileupload.css"/>
        <link rel="stylesheet" type="text/css" href="../assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
        <link rel="stylesheet" type="text/css" href="../assets/bootstrap-datepicker/css/datepicker.css"/>
        <link rel="stylesheet" type="text/css" href="../assets/bootstrap-timepicker/compiled/timepicker.css"/>
        <link rel="stylesheet" type="text/css" href="../assets/bootstrap-colorpicker/css/colorpicker.css"/>
        <link rel="stylesheet" type="text/css" href="../assets/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
        <link rel="stylesheet" type="text/css" href="../assets/bootstrap-datetimepicker/css/datetimepicker.css"/>
        <link rel="stylesheet" type="text/css" href="../assets/jquery-multi-select/css/multi-select.css"/>
    <?php } ?>

    <?php if (isset($REGISTER_PAGE)) { ?>
        <link rel="stylesheet" type="text/css" href="css/jquery.steps.css"/>
    <?php } ?>

    <!--right slidebar-->
    <link href="../css/slidebars.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/style-responsive.css" rel="stylesheet"/>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltips and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">
<div class="container">
    <form class="form-signin" method="post" id="form_login" action="javascript:admin_login();">
        <h2 class="form-signin-heading">
            <span><img src="../images/loginlogo.jpg"></span>
            科群迈谱(CTRIMAP)<br>临床试验研究管理平台</h2>
        </h2>
        <div class="login-wrap">
            <div class="login-err-msg">
                <span id="err_msg"></span>
            </div>
            <br>
            <input type="text" style="font-size:12px" class="form-control" placeholder="mobile" autofocus required
                   name="mm_num">
            <input style="margin-top:10px" type="password" class="form-control" placeholder="Password" required
                   name="pp_password">
            <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>
        </div>
    </form>
</div>
<!-- js placed at the end of the document so the pages load faster -->
<script>
    function admin_login() {
        var form_data = {
            data: $('#form_login').serialize()
        };
        //alert(form_data);
        $.ajax({
            url: "../php-functions/fncLogin.php?action=adminLogin",
            type: "POST",
            data: form_data,
            success: function (msg) {
                if (msg == 1)
                    window.location.href = "dashboard.php";
                else
                    $('#err_msg').html("Log-in credentials doesn't exist.");
            }

        });

    }
</script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>

</html>