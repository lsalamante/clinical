var Gritter = function () {
    $('#toggle_button').click(function(){
        if($('#form_add_user').data('edit') == '0')
        {
            $.gritter.add({
                // (string | mandatory) the heading of the notification
                title: 'Instructions for Adding Users:',
                // (string | mandatory) the text inside the notification
                text: '* Make sure all required fields are filled.<br> * Click cancel or click again the "Add Users" button to hide the user form details.',
                time: '6000',
                class_name: 'gritter-light'
            });
            return false;
        }
        else
        {
            $.gritter.add({
                // (string | mandatory) the heading of the notification
                title: 'Instructions for Updating Users:',
                // (string | mandatory) the text inside the notification
                text: '* Makes sure all required fields are filled.<br> * Click "Cancel" if you want to cancel updating the user details.',
                time: '6000',
                class_name: 'gritter-light'
            });
            return false;
        }     
    });
    $('.edit_user_details').click(function(){
        $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: 'Instructions for Updating Users:',
            // (string | mandatory) the text inside the notification
            text: '* Makes sure all required fields are filled.<br> * Click "Cancel" if you want to cancel updating the user details.',
            time: '6000',
            class_name: 'gritter-light'
        });
        return false;
    });

    
}();