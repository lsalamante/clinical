<aside>
    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <?php 
            $current_page = basename($_SERVER['PHP_SELF']); // get active page
            if($_GET)
            {
                $getv = '';
                foreach ($_GET as $key => $value) 
                {
                    $getv .= $key."=".$value;    
                }
                $current_page .= "?".$getv;
            }

                
            // "dashboard.php" => 
            //         array(
            //             "label" => "Dashboard",
            //             "iclass" => "fa fa-tachometer"
            //             ),
            $pages = array(
                "loadusers('a')" => 
                    array(
                        "label" => $phrases['applicant'],
                        "iclass" => "fa fa-user"
                        ),
                "loadusers('c')" => 
                    array(
                        "label" => "CRO",
                        "iclass" => "fa fa-user"
                        ),
                "loadusers('m')" =>
                    array(
                        "label" => $phrases['add_member'],
                        "iclass" => "fa fa-group"
                        ),
                "loadusers('t')" =>
                    array(
                        "label" => $phrases['tfo'],
                        "iclass" => "fa fa-book"
                        ),
                "loadusers('pg')" =>
                    array(
                        "label" => $phrases['professional_group'],
                        "iclass" => "fa fa-briefcase"
                        ),
                "loadusers('p')" =>
                    array(
                        "label" => $phrases['pre'],
                        "iclass" => "fa fa-comments"
                        ),
                "loadusers('e')" =>
                    array(
                        "label" => $phrases['ethics_committee'],
                        "iclass" => "fa fa-suitcase"
                        )
            );

            foreach ($pages as $page_link => $array_values) { ?>
            <li class="sub-menu">
                <a href="javascript:<?php echo $page_link; ?>;" class="<?php echo $current_page == $page_link ? "active" : ""; ?>"><i class="<?php echo $array_values['iclass']; ?>"></i><span style="font-size:17px"><?php echo $array_values['label']; ?></span> </a>

            </li>
            <?php } ?>
            <!--multi level menu end-->
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
