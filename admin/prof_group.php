<?php
/*************************************************************************************************/
/*
Description: Professional Group 
Ajax Files Dependents (update also these files if you update this PHP file): 
    
    
Created by: Vanananana
*/

include('../jp_library/jp_lib.php');
$REGISTER_PAGE = true;
$PICKERS = true;
$DYNAMIC_TABLE = true;
include('header.php');
include('../php-functions/fncCommon.php');
?>
	<!--main content start-->
    <section id="main-content">
	    <section class="wrapper site-min-height">
	    <!-- page start-->
	    <div class="row">
	    	<div class="col-sm-9">
				<section class="panel">
	    			<div class="panel-body">
		    			<header class="panel-heading">
	                        Professional Group List
	                    </header>
		                <div class="adv-table">
		                      <table class="display table table-bordered" id="hidden-table-info">
		                          <thead>
		                              <tr>
		                                <th>Group Name</th>
		                                <th>Group Admin</th>
		                              </tr>
		                          </thead>
		                          <tbody id="table_content">
		                          	
		                          </tbody>
		                      </table>
		                </div>
            		</div>
            	</section>
            </div>
        </div>

	    <!-- page end-->
	    </section>
    </section>
    <!--main content end-->
<!--footer start-->
<?php include('footer.php'); ?>
<!--footer end-->
<script>
</script>