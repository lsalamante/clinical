<!-- js placed at the end of the document so the pages load faster -->
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="../js/jquery.dcjqaccordion.2.7.js"></script>
<script src="../js/jquery.scrollTo.min.js"></script>
<script src="../js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="../js/jquery.sparkline.js" type="text/javascript"></script>
<script src="../assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="../js/owl.carousel.js"></script>
<script src="../js/jquery.customSelect.min.js"></script>
<script src="../js/respond.min.js"></script>
<!--right slidebar-->
<script src="../js/slidebars.min.js"></script>
<script src="../js/common-scripts.js"></script>

<?php 
if($DYNAMIC_TABLE){ ?>
<!--dynamic table initialization -->
<script type="text/javascript" language="javascript" src="../assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../assets/data-tables/DT_bootstrap.js"></script>
<script src="js/dynamic_table_init.js"></script>
<?php } ?>

<?php
if($PICKERS){ ?>
<script type="text/javascript" src="../assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="../assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="../assets/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="../assets/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="../assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
  <script type="text/javascript" src="../assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <!--<script src="../js/advanced-form-components.js"></script>-->
<?php
} ?>

<?php if(isset($PROF_GROUP_PAGE)){ ?>
<script src="../js/bootstrap-validator.min.js" type="text/javascript"></script>
<script src="../js/jquery.validate.min.js" type="text/javascript"></script>
<script src="../js/advanced-form-components.js"></script>
<?php } ?>

<?php if(isset($REGISTER_PAGE)){ ?>
<script src="../js/bootstrap-validator.min.js" type="text/javascript"></script>
<script src="../js/jquery.validate.min.js" type="text/javascript"></script>
<script src="../assets/jquery-multi-select/js/jquery.multi-select.js"></script>
<script src="../assets/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script src="../js/advanced-form-components.js"></script>
<script type="text/javascript" src="../js/jquery.pulsate.min.js"></script>
<script type="text/javascript" src="../assets/gritter/js/jquery.gritter.js"></script>
<!--<script src="../js/gritter.js" type="text/javascript"></script>-->
<script src="js/gritter.js" type="text/javascript"></script>

<script>
      //step wizard

      /*$(function() {
          $('#').stepy({ // #id of steppy
              backLabel: 'Previous',
              block: true,
              nextLabel: 'Next',
              titleClick: true,
              titleTarget: '.stepy-tab'
          });
      });*/
  </script>
<?php } ?>

<!--common script for all pages-->
<!--script for this page-->
<!--
<script src="../js/sparkline-chart.js"></script>
<script src="../js/easy-pie-chart.js"></script>
<script src="../js/count.js"></script>
-->
<!--
<script>
    //owl carousel
    $(document).ready(function () {
        $("#owl-demo").owlCarousel({
            navigation: true
            , slideSpeed: 300
            , paginationSpeed: 400
            , singleItem: true
            , autoPlay: true
        });
    });
    //custom select box
    $(function () {
        $('select.styled').customSelect();
    });
    $(window).on("resize", function () {
        var owl = $("#owl-demo").data("owlCarousel");
        owl.reinit();
    });
</script>-->
