<div class="top-nav">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
        <li>
            <?php if($SEARCH) { ?> <input type="text" class="form-control search" placeholder="Search"> </li>
        <?php } ?>
        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <?php if($AVATAR) echo '<img alt="" src="../img/admin.png">'; ?>
                                <span class="username">
<!--                               Welcome back, -->
                                <?php
                                    if (isset($_SESSION['full_name']))
                                        echo $_SESSION['full_name'];
                                ?>
                                </span> <b class="caret"></b> </a>
            <ul class="dropdown-menu extended logout">
                <div class="log-arrow-up"></div>
<!--                <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>-->
<!--                <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>-->
<!--                <li><a href="#"><i class="fa fa-bell-o"></i> Notification</a></li>-->
                <li><a href="logout.php"><i class="fa fa-key"></i> Log Out</a></li>
            </ul>
        </li>
        <?php if($RIGHT_SIDEBAR)
                    {
                       echo '<li class="sb-toggle-right"> <i class="fa  fa-align-right"></i> </li>';
                    }
 ?>
 <?php if($LANGUAGE){ ?>
        <li class="dropdown language">
          <a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="#">
              <?php if($_SESSION['lang'] == 'chinese'){?>
                <img src="../img/flags/ch.png" alt="">
                <span class="username">中文</span>
              <?php }elseif($_SESSION['lang'] == 'english'){ ?>
                <img src="../img/flags/us.png" alt="">
                <span class="username">English</span>
              <?php } ?>
              <b class="caret"></b>
          </a>
          <ul class="dropdown-menu">
              <li><a href="#" id="lang_cn"><img src="../img/flags/ch.png" alt=""> 中文</a></li>
              <li><a href="#" id="lang_en"><img src="../img/flags/us.png" alt=""> English</a></li>
          </ul>
        </li>

<?php } ?>
        <!-- user login dropdown end -->
    </ul>
    <!--search & user info end-->
</div>
