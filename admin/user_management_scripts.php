<script type="text/javascript">
	/*************************************************************************************************/
/*
Description: onchange attr of <form>
Purpose: -- Form Validation
         -- Check avaialability of unique data (i.e.: email) in TABLE: `users`
Parameters: none
Created by Developer: Vanananana
*/


/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/

function form_validate()
{
    // Check Mobile Num (used for logged in) && Email
    FORM_VALID = true;
    var form_data={
          mobile_num:$('#mobile_num').val(),
          email:$('#email').val(),
          user_id:$('#form_add_user').data('edit')
        };

    $.ajax({
      url:"../php-functions/fncUserManagement.php?action=checkUserAvailability",
      type:"POST",
      data:form_data,
      success:function(data)
      {
        var res = JSON.parse(data);
        if(res["mobile"] == 1)
        {
          $('#mobile_required').attr('style', 'display:block');
          $('#final_submit').attr('disabled', 'disabled');
          FORM_VALID = false;
        }
        else
          $('#mobile_required').attr('style', 'display:none');

        if(res["email"] == 1)
        {
          $('#email_required').attr('style', 'display:block');
          $('#final_submit').attr('disabled', 'disabled');
          FORM_VALID = false;
        }
        else
          $('#email_required').attr('style', 'display:none');
      }
    });

    // check if passwords match
    if($('#password').val() != $('#con_password').val() && $('#password').val() != '' && $('#con_password').val() != '')
    {
      $('#password_required').attr('style', 'display:block');
      FORM_VALID = false;
    }
    else
      $('#password_required').attr('style', 'display:none');

    // check if all required fields are not empty
    if(FORM_VALID)
    {
      $('#form_add_user').find(':required').each(function(){
        //alert($(this).val());
        if($(this).val() == '')
        {
          FORM_VALID = false;
          $('#final_submit').attr('disabled', 'disabled');
        }
      });
      if(FORM_VALID)
      {
          $('#final_submit').removeAttr('disabled');
      }
    }
}
/*************************************************************************************************/
/*
Description: Saving of new user details in TABLE: `users`
Purpose: -- save user registration in form
         -- check php-functions/fncUserManagement.php --> function addUser() for PHP details
Parameters: none
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/

function save_user(reload_m)
{
  if(!FORM_VALID)
  {
    show_alert('<?= $phrases['make_sure'] ?>', '', 0);
  }
  else
  {
    var form_data=new FormData($('#form_add_user')[0]);
    $.ajax({
      url:"../php-functions/fncUserManagement.php?action=addUser",
      type:"POST",
      data:form_data,
      processData: false,
      contentType: false,
      success:function(msg)
      {
        if(msg != 0)
        {
          show_alert("<?= $phrases['new_user_success']?>", '', 1);
          loadusers(reload_m);
          /** PS: function show_alert(msg, redirect, type) --> footer.php **/
        }
        else
        {
          show_alert("<?= $phrases['operation_failed'] ?>", '', 0);
        }
      }
    });
  }
}
/*************************************************************************************************/
/*
Description: Save roles of selected members
Purpose: -- add roles of specific user
Parameters: none
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/

function save_role(reload_m, pg)
{
    var form_data={
    data:$('#form_add_role').serialize()
    };
    $.ajax({
      url:"../php-functions/fncUserManagement.php?action=addRole",
      type:"POST",
      data:form_data,
      success:function(msg)
      {
        if(msg != 0)
        {
          show_alert("<?= $phrases['operation_successful'] ?>", '', 1);
          if(pg == false)
            loadusers(reload_m);
          else
            loadpgroup(reload_m);
          /** PS: function show_alert(msg, redirect, type) --> footer.php **/
        }
        else
        {
          show_alert("<?= $phrases['operation_failed']?>", '', 0);
        }
      }
    });
}
/*************************************************************************************************/
/*
Description: for disabling/enabling accounts
Purpose: change is_active to 0/1 in TABLE: `users`
Parameters: -- current_status => current value of is_active in TABLE: `users`
            -- user_id => user_id in TABLE: `users`
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/

function show_status_modal(current_status, user_id, reload_m, pgroup)
{
  $(".modal").modal("hide");
  if(current_status == 1)
  {
    $('#status_msg').html("<?php echo $phrases['disable_account']; ?>?");
    $('#change_status_btn').attr("onclick", "change_user_status('0', '"+user_id+"', '"+reload_m+"', "+pgroup+")");
  }
  else
  {
    $('#status_msg').html("Are you sure you want to activate account?");
    $('#change_status_btn').attr("onclick", "change_user_status('1', '"+user_id+"', '"+reload_m+"', "+pgroup+")");
  }
  $("#accout_status").modal('show');

}
/*************************************************************************************************/
/*
Description: Activate/Disable Account
Purpose: change is_active value to 0/1
Parameters: -- status_value => (passed via the modal button Ok) active to be set
            -- userId => user_id in TABLE: `users`
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/

function change_user_status(status_value, userId, reload_m, pgroup)
{

  var form_data={
    is_active:status_value,
    user_id:userId
  };
    $.ajax({
      url:"../php-functions/fncUserManagement.php?action=changeStatus",
      type:"POST",
      data:form_data,
      success:function(msg)
      {
        if(msg != 0)
        {
          show_alert("<?= $phrases['operation_successful'] ?>", '', 1);
          if(!pgroup)
            loadusers(reload_m);
          else
            loadpgroup(reload_m);
          /** PS: function show_alert(msg, redirect, type) --> footer.php **/
        }
        else
        {
          show_alert("<?= $phrases['operation_failed']?>", '', 0);
        }
      }
    });
}



function save_pgroup()
{
    var form_data={
    data:$('#form_add_pgroup').serialize()
    };
    $.ajax({
      url:"../php-functions/fncUserManagement.php?action=addPgroup",
      type:"POST",
      data:form_data,
      success:function(msg)
      {
        if(msg != 0)
        {
          show_alert("<?= $phrases['operation_successful'] ?>", '', 1);
          loadusers('pg');
          /** PS: function show_alert(msg, redirect, type) --> footer.php **/
        }
        else
        {
          show_alert("<?= $phrases['operation_failed']?>", '', 0);
        }
      }
    });
}


function ready_edit(userId, roleId, page, pg_id)
{
  var data={
    user_id:userId,
    role_id:roleId,
    ajax_page:page,
    pg: pg_id
  };
  ajax_load(data, 'load_edit_applicant.php', 'form_edit_user');
}

function update_user(reload_m, pg)
{
  var form_data=new FormData($('#form_edit_user')[0]);
    $.ajax({
      url:"../php-functions/fncUserManagement.php?action=updateUser",
      type:"POST",
      data:form_data,
      processData: false,
      contentType: false,
      success:function(msg)
      {
        if(msg != 0)
        {
          //show_alert("Successfully updated user.", '', 1);
          show_alert("<?= $phrases['operation_successful'] ?>", '', 1);
          if(pg == 0)
          {
            loadusers(reload_m);
          }
          else
          {
            loadpgroup(reload_m);
          }

          /** PS: function show_alert(msg, redirect, type) --> footer.php **/
        }
        else
        {
          show_alert("<?= $phrases['operation_failed']?>", '', 0);
        }
      }
    });
}

</script>
