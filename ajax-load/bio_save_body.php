<?php
include('../jp_library/jp_lib.php');
include('../php-functions/fncBioSave.php');
include('../php-functions/fncCommon.php');

#GENERIC VARIABLE NAME FOR REUSE!!!
$res = getAllBioSave(0,$_GET['project_id']);

$counter = 1;
?>
    <?php
    if($res->num_rows > 0){
        foreach($res as $row){ ?>
        <tr onclick="getBioSaveDetails('<?php echo $row['bio_save_id'] ?>', event); return false;" style="cursor:pointer;">
          <!-- <td> -->
                <!-- <input type="checkbox" id="bgc_chk-<?php# echo $row['up_id'] ?>" /> -->
            <!-- </td> -->
            <td>
                <?php echo sprintf('%02d', $counter++);?>
            </td>
            <td>
                <?php echo $phrases[$row['name']] ?>
            </td>
            <td>
                <?php echo $row['quantity'] ?>
            </td>
            <td>
                <?php echo $row['unit'] ?>
            </td>
            <td>
                <?php echo $row['date_created'] ?>
            </td>
            <td>
                <?php echo $row['bio_desc'] ?>
            </td>
            <td>
                <a href="#bio_save_remark_modal" data-toggle="modal" class="btn btn-xs btn-primary" title="add remarks" onclick="changeBioRemarkId(<?php echo $row['bio_save_id']?>); "><?php echo $phrases['remarks']; ?></a>
            </td>
        </tr>
      <?php } #end foreach ?>

      <?php
    }else{
        echo "<tr><td colspan='7' class='text-center'>" . $phrases['no_data'] . "</td></tr>";
    }
?>
