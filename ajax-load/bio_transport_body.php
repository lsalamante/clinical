<?php
include('../jp_library/jp_lib.php');
include('../php-functions/fncCommon.php');

#GENERIC VARIABLE NAME FOR REUSE!!!
$res = getAllTransportbyProjectId(0,$_GET['project_id']);

$counter = 1;
?>
    <?php
    if($res->num_rows > 0){
        foreach($res as $row){ ?>
        <tr onclick="getTransportDetails('<?php echo $row['transport_id'] ?>', event); return false;" style="cursor:pointer;">
          <!-- <td> -->
                <!-- <input type="checkbox" id="determine_chk-<?php# echo $row['up_id'] ?>" /> -->
            <!-- </td> -->
            <td>
                <?php echo sprintf('%02d', $counter++);?>
            </td>
            <td>
              <?php echo $phrases[$row['name']] ?>
            </td>
            <td>
                <?php echo $row['quantity'] ?>
            </td>
            <td>
                <?php echo $row['unit'] ?>
            </td>
            <td>
                <?php echo $row['date_created'] ?>
            </td>
            <td>
                <?php echo $row['description'] ?>
            </td>
            <td>
                <a href="#transport_remark_modal" data-toggle="modal" class="btn btn-xs btn-primary" title="add remarks" onclick="changeTransportRemarkId(<?php echo $row['transport_id']?>); "><?php echo $phrases['remarks']; ?></a>
            </td>
        </tr>
      <?php } #end foreach ?>

      <?php
    }else{
        echo "<tr><td colspan='7' class='text-center'>" . $phrases['no_data'] . "</td></tr>";
    }
?>
