<?php
include('../jp_library/jp_lib.php');
include('../php-functions/fncCommon.php'); 

#GENERIC VARIABLE NAME FOR REUSE!!!
$res = getAllPD2ByProjectId(0,$_GET['project_id']);

?>
    <?php 
    if($res->num_rows > 0){
        foreach($res as $row){ ?>
        <tr class="i_row">
            <td><input type="checkbox" id="i_chk-<?php echo $row['up_id'] ?>" /></td>
            <td>
                <?php echo $row['up_name'] ?>
            </td>
            <td>
                <?php echo $row['up_ver'] ?>
            </td>
            <td>
                <?php echo $row['up_date'] ?>
            </td>
            <td>
                <?php echo $row['up_desc'] ?>
            </td>
            <td>
                <a href="<?php echo $row['up_path'] ?>" class="btn btn-sm btn-primary" target="_blank" title="view file"><?php echo $phrases['view_attachments']; ?></a>
            </td>
        </tr>
        <?php 
        }
    }else{
        echo "<tr><td colspan='6' class='text-center'>No data available.</td></tr>";
    }

?>
