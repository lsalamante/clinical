<?php
include('../jp_library/jp_lib.php');
include('../php-functions/fncCommon.php');

#GENERIC VARIABLE NAME FOR REUSE!!!
$res = getAllJobByProjectId(0,$_GET['project_id']);

$counter = 1;
?>
    <?php
    if($res->num_rows > 0){
        foreach($res as $row){ ?>
        <tr>
            <td>
              <?php if ($_SESSION['role_id'] == $row['role_id']){?>
                <input type="checkbox" id="job_chk-<?php echo $row['up_id'] ?>" />
              <?php } ?>
            </td>
            <td>
                <?php echo sprintf('%02d', $counter++);?>
            </td>
            <td>
                <?php echo $row['up_name'] ?>
            </td>
            <td>
                <?php echo $row['up_ver'] ?>
            </td>
            <td>
                <?php echo $row['up_date'] ?>
            </td>
            <td>
                <?php echo $row['up_desc'] ?>
            </td>
            <td>
                <a href="<?php echo $row['up_path'] ?>" class="btn btn-sm btn-primary" target="_blank" title="view file"><?php echo $phrases['view_attachments']; ?></a>
            </td>
        </tr>
        <?php
        }
    }else{
        echo "<tr><td colspan='7' class='text-center'>" . $phrases['no_data'] . "</td></tr>";
    }

?>
