<?php
include('../jp_library/jp_lib.php');
include('../php-functions/fncCommon.php');

#GENERIC VARIABLE NAME FOR REUSE!!!
$res = getAllMCbyProjectId(0,$_GET['project_id']);

$counter = 1;
?>
    <?php
    if($res->num_rows > 0){
        foreach($res as $row){ ?>
        <tr>
            <td><input type="checkbox" id="mc_chk-<?php echo $row['mc_id'] ?>" /></td>
            <td>
                <?php echo sprintf('%02d', $counter++);?>
            </td>
            <td>
                <?php echo $row['mc_org'] ?>
            </td>
            <td>
                <?php echo $row['mc_person'] ?>
            </td>
            <td>
                <?php echo $row['mc_lead'] ?>
            </td>
            <td>
                <?php echo $row['mc_remarks'] ?>
            </td>
        </tr>
        <?php
        }
    }else{
        echo "<tr><td colspan='6' class='text-center'>" . $phrases['no_data'] . "</td></tr>";
    }

?>
