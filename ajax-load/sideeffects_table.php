<?php
include('../jp_library/jp_lib.php');
include('../php-functions/fncCommon.php');
include("../php-functions/fncSideEffect.php");  
?>

<?php
$report_t = array(
  "0" => $phrases['first_report'],
  "1" => $phrases['follow_up_report'],
  "2" => $phrases['sum_up_report']
  );
$sae_s = array(
  "0" => $phrases['die'],
  "1" => $phrases['be_in_hospital'],
  "2" => $phrases['disability'],
  "3" => $phrases['dysfunction'],
  "4" => $phrases['deformity'],
  "5" => $phrases['life_threatening'],
  "6" => $phrases['other'],
  "7" => $phrases['severe']
  );
                $count = 0;
                $project_id = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? $_GET["project_id"] : 0;
                $sideEffects = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? getSideEffects(false, $project_id) : getSideEffects(false, $project_id, $projects);//make way for side effect list
                foreach ($sideEffects as $effect) {

                  $count++;
            ?>

                <tr onclick="getSideEffectDetails(<?php echo $effect['side_effect_id']; ?>, event, '<?php echo $_SESSION["user_type"]; ?>'); return false;" style="cursor:pointer;">
                    <td><?php echo $count; ?></td>
                    <td><?php echo $effect["project_name"]; ?></td>
                    <td><?php echo $effect["sub_initials"]; ?></td>
                    <td><?php echo $effect["reporter_position"]; ?></td>
                    <td><?php echo $effect["report_time"] != "0000-00-00 00:00:00.000000" && $effect["report_time"] != "0000-00-00 00:00:00" ? date("Y-m-d", strtotime($effect["report_time"])) : "N/A" ; ?></td>
                    <td><?php
                    $all_reports = explode(",", $effect['report_type']);
                    foreach ($all_reports as $value) {
                      if($value != '')
                      {
                        echo $report_t[$value]."<br>";
                      }
                      else
                      {
                        echo $phrases['not_applicable'];
                      }
                    }
                    
                    ?></td>
                    <td><?php echo $sae_s[$effect['sae_situation']] ?></td>
                    <td onclick="event.cancelBubble = true;">
                      <a  class="btn btn-xs btn-primary" title="add remarks" onclick="$('#side_effect_remark_modal').modal('toggle');
                      $('#side_effect_remark_modal').modal('show'); changeSideEffectRemarkId(<?php echo $effect['side_effect_id']; ?>)" ><?php echo $phrases['remarks']; ?></a>
                    </td>
                </tr>

            <?php } ?>