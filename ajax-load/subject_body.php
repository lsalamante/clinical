<?php
namespace Clinical\Helpers;
include('../jp_library/jp_lib.php');
$t = new Translation();

$_SESSION['lang'] = 'chinese';

include('../php-functions/fncCommon.php');
  $project = getProjectById(0, $_GET['project_id']);
require("../php-functions/fncSubjects.php");

if((isset($_GET['module']) && $_GET['module'] != '') || (isset($_GET['keyword']) && $_GET['keyword'] != ''))
  $subjects = getSubjects($_GET['project_id'], array(), "","","","", $_GET['keyword']);
else
  $subjects = getSubjects($_GET['project_id'], array(), $_GET['ssearch_random_no'],$_GET['ssearch_status'],$_GET['ssearch_birthday'],$_GET['ssearch_gender']);

          $state = array(
              "C" => $phrases['chosen'],
              "B" => $phrases['begin'],
              "FU" => $phrases['follow_up'],
              "STP" => $phrases['stop'],
              "O" => $phrases['out'],
              "D" => $phrases['drop'],
              "NS" => $phrases['next_select'],
              "F" => $phrases['finish']);

          $count = 0;

          foreach ($subjects as $subject) {

            $pi_id = (getProjectById(0,$subject["project_id"])['pi_id']);
            $pi = (getUserById(0,$pi_id));
            $pi = $pi['lname'] ." ". $pi['fname']; #Get the PI name

            $count++;
            ?>
            <!-- set_subject_id is on the bottom of this page -->
            <tr href="#update_subject_modal" onclick="set_subject_id(<?php echo $subject['volunteer_id'] ?>, <?php echo $subject['subject_id'] ?>, '<?php echo $pi ?>')" data-toggle="modal" style="cursor:pointer;">
              <td><?php echo $count; ?></td>
              <td><?php echo $subject["project_name"]; ?></td>
              <td><?php echo $subject["project_num"]; ?></td>
              <td><?php echo $subject["subjects_num"]; ?></td>
              <td><?php echo strtoupper($subject["v_name"]); ?></td>
              <td><?php echo $subject["abbrv"]; ?></td>
              <td><?php echo date('m-d-Y', strtotime($subject["bday"])); ?></td>
              <td><?php echo $subject["id_card_num"]; ?></td>
              <!-- <td><?php echo $subject["random_date"]; ?></td> -->
              <td><?php echo $subject["random_num"]; ?></td>
              <td><?php echo $state[$subject["subject_stat"]]; ?> </td>
              <td><?php echo date('m-d-Y', strtotime($subject["status_date"])); ?> </td>

              <td onclick="event.cancelBubble = true;">

                <?php if($_SESSION["user_type"] == 'C' || $_SESSION['user_type'] == 'SC'){ ?>

                    <!-- Start of buttons for CLinician @Rai-->
                    <?php if($subject['subject_stat'] == "C"){ ?>

                      <a class="btn btn-sm btn-primary" title="Begin" onclick="statusChange(<?php echo $subject["subject_id"] ?>, 'B', '', '<?php echo $_SESSION['lang']?>'); return false;"><?php echo $phrases['subject_begin']; ?></a>

                    <?php }else if($subject['subject_stat'] == 'B'){ ?>

                      <a class="btn btn-sm btn-primary" title="Follow Up" onclick="statusChange(<?php echo $subject["subject_id"] ?>, 'FU', '', '<?php echo $_SESSION['lang']?>'); return false;"><!-- <i class="fa   fa-map-marker"></i> --><?php echo $phrases['subject_follow_up']; ?></a>
                      <a class="btn btn-sm btn-primary" title="Stop" onclick="statusWithRemarks(<?php echo $subject["subject_id"] ?>, 'STP'); return false;"><!-- <i class="fa   fa-exclamation-triangle"></i> --><?php echo $phrases['subject_stop']; ?></a>
                      <a class="btn btn-sm btn-primary" title="Out" onclick="statusWithRemarks(<?php echo $subject["subject_id"] ?>, 'O'); return false;"><!-- <i class="fa  fa-ban"></i> --><?php echo $phrases['subject_out']; ?></a>
                      <a class="btn btn-sm btn-primary" title="Drop" onclick="statusWithRemarks(<?php echo $subject["subject_id"] ?>, 'D'); return false;"><!-- <i class="fa   fa-minus-circle"></i> --><?php echo $phrases['subject_drop']; ?></a>
                      <a class="btn btn-sm btn-primary" title="SAE" onclick="show_sub_sae('<?php echo $subject['subject_id']; ?>')">SAE</a>

                    <?php }else if($subject['subject_stat'] == "FU"){ ?>

                      <a class="btn btn-sm btn-primary" title="Finish" onclick="statusChange(<?php echo $subject["subject_id"] ?>, 'F', '', '<?php echo $_SESSION['lang']?>'); return false;"><?php echo $phrases['finish']?></a>

                    <?php } ?>
                    <!-- End of buttons for CLinician @Rai-->

                <?php }else if($_SESSION["user_type"] == 'I' && in_array($subject['subject_stat'], array('NS', 'STP', 'O', 'D') ) ){ ?>

                    <!-- Start of button for Inspector @Rai-->
                      <a class="btn btn-sm btn-primary" title="Return to Volunteer" onclick="statusInspectorChange(<?php echo $subject["volunteer_id"] ?>); return false;"><?= $phrases['return_to_volunteer'] ?></a>
                    <!-- End of button for Inspector @Rai-->

                <?php } ?>

              </td>

            </tr>

    <?php } ?>