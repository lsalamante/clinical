<?php
include('../jp_library/jp_lib.php');
include("../php-functions/fncCommon.php");
include("../php-functions/fncTraining.php");
                $count = 0;
                // $project_id = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? $_GET["project_id"] : 0; 
				$materials = getMaterials(false, $_GET['training_id']);
                foreach ($materials as $material) { 

                  $count++;
            ?>

                <tr onclick="getTrainingDetails(<?php echo $material['']; ?>); return false;" style="cursor:pointer;">
                    <td><?php echo $count; ?></td>
                    <td><?php echo $material['name'] == '' ? "Untitled Meeting" : $material["name"]; ?></td>
                    <td><?php echo $material['version'] == null ? $phrases['not_applicable'] : $material["version"]; ?></td>
                    <td><?php echo date("Y-m-d H:i:s", strtotime($material['version_date'])); ?></td>
                    <td><?php echo $material['description']; ?></td>
                    <td><a href="<?php echo $material['up_path']; ?>" target="_blank" class="btn btn-sm btn-primary"><?php echo $phrases['view_attachments']; ?></a></td>
                    
                </tr>

            <?php } ?>
