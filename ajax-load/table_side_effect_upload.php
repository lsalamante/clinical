<?php
include('../jp_library/jp_lib.php');
include("../php-functions/fncCommon.php"); 
include("../php-functions/fncSideEffect.php");
                $count = 0;
                // $project_id = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? $_GET["project_id"] : 0; 
				$documents_se = getDocuments(false, $_GET['side_effect_id']);
                foreach ($documents_se as $doc_se) { 

                  $count++;
            ?>

                <tr style="cursor:pointer;">
                    <td><?php echo $count; ?></td>
                    <td><?php echo $doc_se['name'] == '' ? "Untitled Document" : $doc_se["name"]; ?></td>
                    <td><?php echo $doc_se['version'] == null ? $phrases['not_applicable'] : $doc_se["version"]; ?></td>
                    <td><?php echo date("Y-m-d H:i:s", strtotime($doc_se['version_date'])); ?></td>
                    <td><?php echo $doc_se['description']; ?></td>
                    <td><a href="<?php echo $doc_se['up_path']; ?>" target="_blank" class="btn btn-sm btn-primary"><?php echo $phrases['view_attachments']; ?></a></td>
                </tr>

            <?php } ?>
