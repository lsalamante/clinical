<?php
include('../jp_library/jp_lib.php');
require("../php-functions/fncCommon.php");
include("../php-functions/fncTraining.php");
$t_type = array(
    "0" => $phrases['will_start'],
    "1" => $phrases['training'],
    "2" => $phrases['other']
    );
                $count = 0;
                $project_id = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? $_GET["project_id"] : 0;
				$trainings = getTraining(false, $project_id);
                foreach ($trainings as $training) {

                  $count++;
                  $training_proj = getProjectById(0, $training['project_id']);
									$training_proj_name = $training_proj['project_name'];
            ?>

                <tr onclick="getTrainingDetails(<?php echo $training['training_id']; ?>); return false;" style="cursor:pointer;">
                    <td><?php echo $count; ?></td>
                    <td><?php echo $training_proj_name; ?></td>
                    <td><?php echo $training['title'] == '' ? $phrases['untitled_meeting'] : $training["title"]; ?></td>
                    <td><?php echo $training['type'] == null ? $phrases["not_applicable"] : $t_type[$training["type"]]; ?></td>
                    <td><?php echo date("Y-m-d H:i:s", strtotime($training['date_created'])); ?></td>
                    <td><?php echo $training['is_drafted'] == 1 ? $phrases['drafted'] : $phrases['submitted']; ?></td>
                    <!-- <td onclick="event.cancelBubble = true;"></td> -->
                </tr>

            <?php } ?>
