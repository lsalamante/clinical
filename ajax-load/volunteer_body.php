<?php
namespace Clinical\Helpers;
include('../jp_library/jp_lib.php');
$t = new Translation($_SESSION['lang']);

// $_SESSION['lang'] = 'chinese';

include('../php-functions/fncCommon.php');
  $project = getProjectById(0, $_GET['project_id']);
require("../php-functions/fncVolunteers.php");

if((isset($_GET['module']) && $_GET['module'] != '') || (isset($_GET['keyword']) && $_GET['keyword'] != ''))
  $volunteers = search($_GET['keyword'],$_GET['project_id'],$_GET['module']);
else  
  $volunteers = searchFilters($_GET['vsearch_type'],$_GET['vsearch_status'],$_GET['vsearch_birthday'],$_GET['vsearch_gender'],$_GET['project_id']);
            
            $count = 0;

            foreach ($volunteers as $vol) {

              $count++;
              ?>

              <tr onclick="getVolunteerDetails(<?php echo $vol['volunteer_id']; ?>, '<?php echo $vol['status']; ?>', '<?php echo $_SESSION['user']; ?>'); return false;" style="cursor:pointer;">
                <td onclick="event.cancelBubble = true;">
                  <?php if (($_SESSION['user_type'] == 'C' || $_SESSION['user_type'] == 'SC') && in_array($vol['status_letter'], ['P','NP'])){?>
                    <input type="checkbox" id="vol_chk-<?php echo $vol['volunteer_id'] ?>" />
                    <?php } ?>
                  </td>
                  <td><?php echo $count; ?></td>
                  <td><?php echo strtoupper($vol["v_name"]); ?></td>
                  <td><?php echo $vol["gender"] == "Male" ? $phrases['male'] : $phrases['female']; ?></td>
                  <td><?php echo $vol["bday"]; ?></td>
                  <td><?php echo $vol["native"]; ?></td>
                  <td><?php echo $vol["married"] == "Yes" ? $phrases['yes'] : $phrases['no']; ?></td>
                  <td><?php echo $vol["id_card_num"]; ?></td>
                  <td><?php echo $vol["mobile"]; ?></td>
                  <td>
                    <?php

                    if($vol['status_letter'] == 'P') #NOTE: this is important!!! `status_letter` is used instead of `status` for using the subject_status status column!
                    $status = $phrases['pass'];
                    else if ($vol['status_letter'] == 'NP')
                    $status = $phrases['no_pass'];
                    else if ($vol['status_letter'] == 'S')
                    $status = $phrases['clinician_selecting'];
                    else if ($vol['status_letter'] == 'C')
                    $status = $phrases['chosen_subject'];
                    else if ($vol['status_letter'] == 'B')
                    $status = $phrases['begin'];
                    else if ($vol['status_letter'] == 'FU')
                    $status = $phrases['follow_up'];
                    else if ($vol['status_letter'] == 'STP')
                    $status = $phrases['stop'];
                    else if ($vol['status_letter'] == 'O')
                    $status = $phrases['out'];
                    else if ($vol['status_letter'] == 'F')
                    $status = $phrases['finish'];
                    else if ($vol['status'] == 0)
                    $status = $phrases['before_selecting'];

                    // if($vol["status"] == 0)
                    // $status = "Before selecting"; #PHRASE: add before_selecting phrase
                    // else if($vol["status"] == 1)
                    // $status = "Clinician selecting";

                    echo $status;

                    ?>
                  </td>
                  <td>
                    <?php 
                    if($vol["volunteer_type"] == 1)
                      echo $t->tryTranslate("patient");
                    else if($vol["volunteer_type"] == 2)
                      echo $t->tryTranslate("healthy_people");
                    ?>
                 </td>
                  <td onclick="event.cancelBubble = true;">

                    <?php if($project['status_num'] == 14 && ($_SESSION['user_type'] == 'RN' || $_SESSION['user_type'] == 'SC') && $vol['status_letter'] == ''){ ?>

                      <a href="#" onclick="volunteerStatus(<?php echo $vol['volunteer_id']; ?>, <?php echo $_GET['project_id']; ?>, 'S', '<?php echo $_SESSION['lang']; ?>'); return false;">
                        <button class="btn btn-primary" title="Inform clinician">
                          <?php echo $phrases['inform_clinician']; ?>
                        </button>
                      </a>

                      <?php } ?>

                      <?php if($project['status_num'] == 14 && ($_SESSION['user_type'] == 'C' || $_SESSION['user_type'] == 'SC') && $vol['status_letter'] == 'S'){ ?>

                        <a href="#" onclick="volunteerStatus(<?php echo $vol['volunteer_id']; ?>, <?php echo $_GET['project_id']; ?>, 'P', '<?php echo $_SESSION['lang']; ?>'); return false;">
                          <button class="btn btn-primary" title="Pass">
                            <!-- <i class="fa fa-check" aria-hidden="true"></i> -->
                            <?php echo $phrases['volunteer_pass']; ?>
                          </button>
                        </a>

                        <?php } ?>

                        <?php if($project['status_num'] == 14 && ($_SESSION['user_type'] == 'C' || $_SESSION['user_type'] == 'SC') && $vol['status_letter'] == 'S'){ ?>

                          <a href="#" onclick="volunteerStatus(<?php echo $vol['volunteer_id']; ?>, <?php echo $_GET['project_id']; ?>, 'NP', '<?php echo $_SESSION['lang']; ?>'); return false;">
                            <button class="btn btn-primary" title="No pass">
                              <!-- <i class="fa fa-times" aria-hidden="true"></i> -->
                              <?php echo $phrases['volunteer_no_pass']; ?>
                            </button>
                          </a>

                          <?php } ?>


                          <!-- Show only when pending -->
                          <?php if(false){ ?>

                            <a href="#" onclick="volunteerStatus(<?php echo $vol['volunteer_id']; ?>, <?php echo $_GET['project_id']; ?>, 2); return false;">
                              <button class="btn btn-primary" title="Reject">
                                <i class="fa fa-times" aria-hidden="true"></i>
                              </button>
                            </a>

                            <?php } ?>


                            <a href="#" onclick="remarks(<?php echo $vol['volunteer_id']; ?>); return false;">
                              <button class="btn btn-primary" title="Remarks">
                                <i class="fa fa-align-justify" aria-hidden="true"></i>
                              </button>
                            </a>


                            <!-- Show only when pending and rejected -->
                            <?php if(false){ ?>

                              <a href="#" onclick="volunteerStatus(<?php echo $vol['volunteer_id']; ?>, <?php echo $_GET['project_id']; ?>, 3); return false;">
                                <button class="btn btn-primary" title="Delete">
                                  <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                              </a>

                              <?php } ?>

                            </td>

                          </tr>

                          <?php } ?>