<?php
include "../../jp_library/jp_lib.php";

/*******************
* LOGS START HERE! *
*******************/
$t = new Clinical\Helpers\Translation($_SESSION['lang']);
$p = new Clinical\Helpers\Project($_POST['project_id']);
$u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

# notify DA here so he can double check
unset($p->notify_group);
$p->fetch(1);
$p->notify_group = array_diff($p->notify_group, array_diff($p->positions, [$_POST['user_type']]));

Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang'], $t->tryTranslate('da_has_checked'), $t->tryTranslate('da_please_check') . ": " . $t->tryTranslate($_POST['omodule']));
# / notify DA here so he can double check

$l = new Clinical\Helpers\Log($_SESSION['role_id'], 'double_check',
array(
  $t->tryTranslate('project_name') => "$p->project_name",
  $t->tryTranslate('module') => $t->tryTranslate($_POST['omodule']),
  $t->tryTranslate('number') => "$p->project_num",
  $t->tryTranslate('submitter') => "$u->fname"
  )
);
$l->save();

/*******************
* LOGS END HERE!   *
*******************/
echo 1;
