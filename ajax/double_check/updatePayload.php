<?php
include "../../jp_library/jp_lib.php";

#instantiate our normal stuffs right here
$dc = new Clinical\Helpers\DoubleCheck($_POST['table'], $_POST['pk_col'], $_POST['bio_id'], $_SESSION['lang']);

#fetch payload data. json and array
$dc->fetch($_POST['table'], $_POST['pk_col'], $_POST['bio_id']);

#set new values from our ajax call
$dc->payload[$_POST['payload_key']]['is_checked'] = $_POST['is_checked'];
$dc->payload[$_POST['payload_key']]['comments'] = $_POST['comments'];

#if databse update was successful, echo id
if(
$dc->updatePayload($_POST['table'], $_POST['pk_col'], $_POST['bio_id'])
){
  echo $dc->id;
}else{
  echo false;
}
