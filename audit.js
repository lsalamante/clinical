var status_radio = $(".status-radio");
var agree_radio = $("#agree_radio");
var disagree_radio = $("#disagree_radio");
var status_submit = $("#status_submit");
var add_documents = $('#add_documents');

agree_radio.on('click', function (e) {
  $('#comments').val('');
  $('#disagree_remarks').attr('style', 'display:none');
  status_submit.removeAttr('disabled');
});
disagree_radio.on('click', function (e) {
  $("#disagree_remarks").attr('style', 'display:block; margin-left:250px;');
  status_submit.attr('disabled', 'disabled');

  if ($('#status_submit2').length) {
    $('#status_submit2').attr('disabled', 'disabled');
  }

});

$('#comments').on('keyup', function (e) {
  if($('#comments').val() != '')
  {
    status_submit.removeAttr('disabled');
    if ($('#status_submit2').length) {
      $('#status_submit2').removeAttr('disabled');
    }
  }
  else
  {
    status_submit.attr('disabled', 'disabled');
    if ($('#status_submit2').length) {
      $('#status_submit2').attr('disabled', 'disabled');
    }
  }
});

// add_documents.on('click', function (e) {
//   $('#add_comments').val("");
//   $('#docs_comments_div').attr('style', 'display:none;');
//   $('#add_documents:checked').each(function(){
//     $('#docs_comments_div').attr('style', 'display:block; margin-left:70px;');
//   });
// });

add_documents.change(function(){
  if(this.checked){
    $('#add_comments').val("");
    $('#docs_comments_div').attr('style', 'display:none;');
    $('#docs_comments_div').attr('style', 'display:block; margin-left:70px;margin-top:15px');
  }else{
    $('#add_comments').val("");
    $('#docs_comments_div').attr('style', 'display:none;');
  }
});

status_submit.on('click', function (e) {
  var bValid = true;
  var fields = new Array();

  // BUG. I think js conflict. it is not saving -- VAN
  if($("#project_status").val() == 13){ // required following fields once status is 12

    fields['clinician'] = {'type' : 'dropdown', 'label' : 'Clinician'};
    fields['nurse'] = {'type' : 'dropdown', 'label' : 'Nurse'};
    fields['bio_controller'] = {'type' : 'dropdown', 'label' : 'Biological Sample Controller'};
    fields['med_controller'] = {'type' : 'dropdown', 'label' : 'Medicine Controller'};

    bValid = bValid && checkErrors(fields); //  used checker.js in js/ folder

  }

  if(bValid){

    status_submit.attr('disabled', 'disabled');
    status_submit.html("载入中...");
    var form_data = $('#audit_form').serialize();
    $.ajax({
      url:"php-functions/fncProjects.php?action=changeStatus",
      type:"POST",
      data:form_data,
      success:function(msg)
      {
        if(msg != 0)
        {
          show_alert("<?php echo $phrases['operation_successful']; ?>", 'dashboard.php', 1);
          status_submit.html("<?php echo $phrases['submit']; ?>");
          /** PS: function show_alert(msg, redirect, type) --> footer.php **/
        }
        else
        {
          //show_alert("Unsuccessful Submission. Please Try Again. ", '', 0);
          show_alert(msg, '', 0);
          status_submit.removeAttr('disabled');
          status_submit.html("<?php echo $phrases['submit']; ?>");
        }
      }
    });

  }

  e.preventDefault();
});
