var bgc_form = $('#bgc_form');
var bgc_add = $("#bgc_add");
var bgc_del = $("#bgc_del");
var bgc_chk_all = $("#bgc_chk_all");
/* for remarks */
var bgc_remark_form = $("#bgc_remark_form");
var bgc_remark_id = $("#bgc_remark_id");
var bgc_remarks = $("#bgc_remarks");
var bgc_remark_submit = $("#bgc_remark_submit");

$("#bio_collect_start").datetimepicker();
$("#bio_collect_end").datetimepicker();

ajax_load(data, 'bgc_body.php', 'bgc_body');

/* this is for resetting the form everytime add is clicked*/
bgc_add.on('click', function() {
  $("#bio_id_update").val("");
  bgc_form[0].reset();
  $('#bgc_submit').html('<?php echo $phrases['submit']?>');
  $('#update_collect_record').attr('style', 'display:none');
  $('#update_process_record').attr('style', 'display:none');
});

/* this is the listener for our submit button from the modal*/
bgc_form.on('submit', function (e){
  e.preventDefault();
  var res = true;
  // here I am checking for textFields, password fields, and any
  // drop down you may have in the form
  $("input[type='text'],select,input[type='password']",this).each(function() {
    if($(this).val().trim() == "") {
      res = false;
    }
  })
  if(res){
    $('#bgc_err').text('');
    add_bgc_info();
  }else{
    $('#bgc_err').text('请填写所有必填字段');
  }
});

/* this is for our check all/ uncheck all function */
/* check_all and uncheck_all are found in initialize.js */
bgc_chk_all.change(function(){
  this.checked ? check_all(bgc_body) :  uncheck_all(bgc_body);
});

/* this is our function for inserting and uploading data */
function add_bgc_info(){
  var form_data = new FormData(bgc_form[0]);
  form_data.append('project_id', "<?php echo $_GET['project_id'] ?>");
  form_data.append('sub_dir', 'bio_samples');
  form_data.append('role_id', '<?php echo $_SESSION['role_id'] ?>');

  $.ajax({
    url: "php-functions/fncBioSamples.php?action=uploadBioSample",
    type:'POST',
    processData: false,
    contentType: false,
    data:form_data,
    success:function(msg){
      if(msg == 1){
        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
        //                                  #We reload the table
        console.log(msg);
        ajax_load(data, 'bgc_body.php', 'bgc_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
        console.log(msg);
      }
    }
  });
}

/* this is for deleting data */
bgc_del.on('click', function(){
  $("#bgc_body input:checkbox:checked").each(function() {
    var _id = $(this).attr('id');
    _id = _id.split('-');
    /* push the number only */
    del_arr.push(_id[1]);
  });
  var form_data = {
    data: "del_arr=" + del_arr.join(",")
  };
  $.ajax({
    url: "php-functions/fncCommon.php?action=delUpInArray",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        /* Clear our array */
        del_arr = [];
        show_alert("<?php echo $phrases['deleted']?>", "", 1);
        /* #We reload the table */
        ajax_load(data, 'bgc_body.php', 'bgc_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
      bgc_chk_all.prop('checked', false);
    }
  });
});

changeRemarkId = function (bio_id) {
  bgc_remark_id.val(bio_id);

  $.ajax({
    url:"php-functions/fncBioSamples.php?action=getBioDetails",
    type: "POST",
    dataType: "json",
    data: { "bio_id" : bio_id, "type" : "ajax" },
    success:function(data){
      $('#bgc_remarks').val(data[0].remarks);
    }
  });
}


bgc_remark_form.on('submit', function(e) {
  e.preventDefault();
  var form_data = bgc_remark_form.serialize();

  $.ajax({
    url: "php-functions/fncBioSamples.php?action=addRemarks",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
        ajax_load(data, 'bgc_body.php', 'bgc_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });

});

//Payload goes here

$("#bgc_dbl_chk_form").on('submit', function(e){ //modify payload data using ajax
  e.preventDefault();

  $.ajax({
    url:"ajax/double_check/updatePayload.php",
    type: "POST",
    dataType: "json",
    data: $("#bgc_dbl_chk_form").serialize(),
    success:function(data){ // data is the id of the row
      $("#bgc_dbl_chk_modal ").modal("hide");
      getBioDetails(data, event);
    }
  });
});

bgcSetPayload = function(caller){ //Set the payload key to be edited
  $("#bgc_dbl_chk_form")[0].reset();

  $("#bgc_payload_key").val($(caller).data('key'));

  //for setting default comments
  $("#bgc_comment").text($(caller).data('comments'));

  //for setting default value of checkbox
  if($(caller).data('is_checked') == 1){
    $("#bgc_dbl_chk_pass").attr('checked', true);
    $("#bgc_dbl_chk_nopass").attr('checked', false);
  }else if($(caller).data('is_checked') == -1)  {
    $("#bgc_dbl_chk_nopass").attr('checked', true);
    $("#bgc_dbl_chk_pass").attr('checked', false);
  }else{
    $("#bgc_dbl_chk_nopass").attr('checked', false);
    $("#bgc_dbl_chk_pass").attr('checked', false);
  }

}
// Payload
