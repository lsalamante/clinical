
<div class="panel-body">
  <section class="panel negative-panel" style="margin-top:0px!important">
    <header class="panel-heading" style="padding-left:0px;">
      <?php if($_SESSION['user_type'] == 'BGC' && $project['status_num'] >= 14):?>
      <a href="#bgc_modal" data-toggle="modal" class="btn btn-xs btn-primary"
      id="bgc_add" onclick="rmDoubleCheck();"><?php echo $phrases['add']?></a>
    <?php endif; ?>
    </header>
    <table class="table table-hover">
      <thead>
        <tr>
          <!-- <th> TODO: can be uncommented later if deletion is required
          <input type="checkbox" id="bgc_chk_all"/>
        </th> -->
        <th><?php echo $phrases['number'] ?></th>
        <th><?php echo $phrases['name'] ?></th>
        <th><?php echo $phrases['sample_volume'] ?></th>
        <th><?php echo $phrases['unit'] ?></th>
        <th><?php echo $phrases['date'] ?></th>
        <th><?php echo $phrases['description'] ?></th>
        <th><?php echo $phrases['operation'] ?></th>
      </tr>
    </thead>
    <tbody id="bgc_body">
    <!-- AJAX LOAD bgc_body.php -->
    </tbody>
  </table>
</section>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bgc_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" >×</button>
        <h4 class="modal-title"><?php echo $phrases['add_bio_record'] ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="bgc_form" enctype="multipart/form-data" novalidate>
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo $phrases['name'] ?></label>
            <select class="form-control" name="name" id="bgc_name" required>
              <option value="" ><?php echo $phrases['choose']?></option>
              <option value="whole_blood"><?php echo $phrases['whole_blood']?></option>
              <option value="blood_plasma"><?php echo $phrases['blood_plasma']?></option>
              <option value="blood_serum"><?php echo $phrases['blood_serum']?></option>
              <option value="urine"><?php echo $phrases['urine']?></option>
              <option value="dung"><?php echo $phrases['dung']?></option>
              <option value="saliva"><?php echo $phrases['saliva']?></option>
              <option value="milk"><?php echo $phrases['milk']?></option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['quantity_bgc'] ?></label>
            <input type="text" class="form-control" id="bgc_quantity" name="quantity" placeholder="" required>
          </div>
          <div class="form-group">
            <label ><?php echo $t->tryTranslate('unit'); ?></label>
            <input type="text" class="form-control" id="bgc_unit" name="unit" placeholder="" required>
          </div>
          <div class="form-group">
            <label class="control-label "><?php echo $phrases['collect_time_bgc']?></label>
            <div class="">
              <input class="form-control form-control-inline"
              required name="collect_start" id="bio_collect_start" type="text" value="" required>

            </div>
            <div class="">
              to
            </div>
            <div class="">
              <input class="form-control form-control-inline"
              required name="collect_end" id="bio_collect_end" type="text" value="" required>

            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputFile"><?php echo $phrases['collect_record_bgc']?></label>
            <input type="file" name="collect_record" id="collect_record" class="form-control">
          </div>
          <div class="form-group">
            <a href="" id="update_collect_record" class="btn btn-sm btn-danger" target="_blank" title="view file" style="display:none"><?php echo $phrases['view_attachments']; ?></a>
          </div>
          <div class="form-group">
            <input type="checkbox" name="databank" value="1" id="bio_collect_databank">
            <label for="bio_collect_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
          </div>

          <!-- TODO: Add loading animation -->
          <!-- <div class="form-group">
            <label for="exampleInputFile"><?php #echo $phrases['process_record']?></label>
            <input type="file" id="process_record" name="process_record[]" multiple="" required>
            <a href="" id="update_process_record" class="btn btn-sm btn-danger" target="_blank" title="view file" style="display:none"><i class="fa fa-eye"></i></a>
          </div> -->
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['description'] ?></label>
            <textarea class="form-control v-resize-only" id="bgc_description" name="description" placeholder="" ></textarea>
          </div>
          <input type="hidden" name="bio_id_update" id="bio_id_update" value="">
          <p style="color:red; font-size:15px" id="bgc_err"></p>
          <?php if($_SESSION['user_type'] == 'BGC'):?>
          <button type="" class="btn btn-primary" id="bgc_submit"><?php echo $phrases['submit'] ?></button>
        <?php endif; ?>

        <!-- BUTTON FOR NOTIFY FOR DA -->
        <?php if($_SESSION["user_type"] == "DA" ): ?>
          <button class="btn btn-primary" type="button" onclick="notifyOwner('<?php echo $_GET['project_id']?>', 'BGC', 'collect')">
            <?php echo $t->tryTranslate('notify')?>
          </button>
        <?php endif; ?>
        <!-- / BUTTON FOR NOTIFY FOR DA -->

        </form>
      </div>
    </div>
  </div>
</div>

<!--  REMARK MODAL  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bgc_remark_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $phrases['add_remarks'] ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="bgc_remark_form">
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo $phrases['remarks'] ?></label>
            <textarea class="form-control v-resize-only" id="bgc_remarks" name="remarks" placeholder="" required></textarea>
            <input type='hidden' name="bio_id" id="bgc_remark_id" value="">
          </div>
          <?php $bio_remarks_users = array("BGC", "DA"); ?>
          <button type="" class="btn btn-primary make-loading" id="bgc_remark_submit" <?php if(!in_array($_SESSION['user_type'], $bio_remarks_users)) echo 'disabled'; ?>><?php echo $phrases['submit'] ?></button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- / REMARK MODAL  -->

<!--  DOUBLE CHECK MODAL  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bgc_dbl_chk_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $t->tryTranslate('add_double_check_opinion') ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="bgc_dbl_chk_form">
          <div class="form-group">
            <div class="form-group <?php echo ($_SESSION['user_type'] == 'DA') ? '' : 'hidden' ; ?>">
              <div class="radio">
                <input value="1" name="is_checked" id="bgc_dbl_chk_pass" type="radio" onClick="clearDblCheck('bgc_comment', this.value)"> <?php echo $t->tryTranslate('no_problem') ?> &nbsp;&nbsp;
                <input value="-1" name="is_checked" id="bgc_dbl_chk_nopass" type="radio" onClick="clearDblCheck('bgc_comment', this.value);"> <?php echo $t->tryTranslate('have_problem') ?> </div>
            </div>
            <label for="exampleInputEmail1" class="dbl_chk_label"><?php echo $t->tryTranslate('comments') ?></label>
            <textarea class="form-control v-resize-only" id="bgc_comment" name="comments" <?php echo ($_SESSION['user_type'] != 'DA') ? 'disabled' : '' ;?> placeholder=""></textarea>
            <input type='hidden' name="bio_id" id="bgc_dbl_chk_id" value="">  <!-- unique id of PK-->
            <input type='hidden' name="payload_key" id="bgc_payload_key" value="">  <!-- key of payload to modify -->
            <input type='hidden' name="table" id="bgc_dbl_chk_tbl" value="bio_samples"> <!-- table name to modify -->
            <input type='hidden' name="pk_col" id="bgc_pk_col" value="bio_id">  <!-- primary key column name of table-->
          </div>
          <?php if($_SESSION['user_type'] == 'DA'):?>
          <button type="" class="btn btn-primary make-loading" id="bgc_dbl_chk_submit"><?php echo $phrases['submit'] ?></button>
        <?php endif; ?>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- / DOUBLE CHECK MODAL  -->
