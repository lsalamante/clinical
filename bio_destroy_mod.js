var bio_destroy_form = $('#bio_destroy_form');
var bio_destroy_submit = $('#bio_destroy_submit');
var add_bio_destroy = $('#add_bio_destroy');

$('#bio_destroy_date').datetimepicker();

ajax_load(data, "bio_destroy_body.php", "bio_destroy_body");

add_bio_destroy.on('click', function (e){
	bio_destroy_form[0].reset();
	$('#bio_destroy_id_update').val("");
	$('#bio_destroy_submit').html('<?php echo $phrases['submit']?>');
	$('#bio_destroy_record_a').attr('href', "");
	$('#bio_destroy_record_a').attr('style', 'display:none');
});


bio_destroy_form.on('submit', function (e){
  e.preventDefault();
	var res = true;
	// here I am checking for textFields, password fields, and any
	// drop down you may have in the form
	$("input[type='text'],select,input[type='password']",this).each(function() {
			if($(this).val().trim() == "") {
					res = false;
			}
	})
	if(res){
		$('#destroy_err').text('');
		add_bio_destroy_info();
	}else{
		$('#destroy_err').text('请填写所有必填字段');
	}
});


/* for adding new bio destroy info */
function add_bio_destroy_info()
{
	var form_data = new FormData(bio_destroy_form[0]);
	form_data.append('project_id', "<?php echo $_GET['project_id'] ?>");
	form_data.append('role_id', "<?php echo $_SESSION['role_id'] ?>");
	// form_data.append('sub_dir', 'bio_samples');
	$.ajax({
	    url: "php-functions/fncBioDestroy.php?action=addDestroyInfo",
	    type:'POST',
	    processData: false,
	    contentType: false,
	    data:form_data,
	    success:function(msg){
	      if(msg == 1){
	        show_alert("<?php echo $phrases['operation_successful'] ?>", "", 1);
	        //                                  #We reload the table
	        ajax_load(data, "bio_destroy_body.php", "bio_destroy_body");
	      }else{
	        show_alert(msg, "<?php echo $phrases['operation_failed'] ?>", 0);
	      }
	    }
	  });
}

var bio_destroy_remark_form = $('#bio_destroy_remark_form');
changeBioRemarkId = function (bio_destroy_id) {
  $('#bio_destroy_id').val(bio_destroy_id);

  $.ajax({
    url:"php-functions/fncBiodestroy.php?action=getBioRemarkDetails",
    type: "POST",
    dataType: "json",
    data: { "bio_destroy_id" : bio_destroy_id, "type" : "ajax" },
    success:function(data){
      $('#bio_destroy_remarks').val(data[0].remarks);
    }
  });
}

bio_destroy_remark_form.on('submit', function(e) {
  e.preventDefault();
  var form_data = bio_destroy_remark_form.serialize();

  $.ajax({
    url: "php-functions/fncBiodestroy.php?action=addRemarks",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
        ajax_load(data, 'bio_destroy_body.php', 'bio_destroy_body');
      }else{
        console.log(msg);
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });

});


//Payload goes here

$("#bio_destroy_dbl_chk_form").on('submit', function(e){ //modify payload data using ajax
  e.preventDefault();

  $.ajax({
    url:"ajax/double_check/updatePayload.php",
    type: "POST",
    dataType: "json",
    data: $("#bio_destroy_dbl_chk_form").serialize(),
    success:function(data){ // data is the id of the row
			$("#bio_destroy_dbl_chk_modal ").modal("hide");
      getBioDestroyDetails(data, event);
    }
  });
});

destroySetPayload = function(caller){ //Set the payload key to be edited
	$("#bio_destroy_dbl_chk_form")[0].reset();
	
  $("#bio_destroy_payload_key").val($(caller).data('key'));

  //for setting default comments
  $("#bio_destroy_comment").text($(caller).data('comments'));

  //for setting default value of checkbox
  if($(caller).data('is_checked') == 1){
    $("#bio_destroy_dbl_chk_pass").attr('checked', true);
    $("#bio_destroy_dbl_chk_nopass").attr('checked', false);
  }else if($(caller).data('is_checked') == -1)  {
    $("#bio_destroy_dbl_chk_nopass").attr('checked', true);
    $("#bio_destroy_dbl_chk_pass").attr('checked', false);
  }else{
    $("#bio_destroy_dbl_chk_nopass").attr('checked', false);
    $("#bio_destroy_dbl_chk_pass").attr('checked', false);
  }

}
// Payload
