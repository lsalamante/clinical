
<div class="panel-body">
  <section class="panel negative-panel" style="margin-top:0px!important">
    <header class="panel-heading" style="padding-left:0px;">
      <?php if($_SESSION['user_type'] == 'BGC' && $project['status_num'] >= 14):?>
      <a href="#determine_modal" data-toggle="modal" class="btn btn-xs btn-primary"
      id="determine_add" onclick="rmDoubleCheck();"><?php echo $phrases['add']?></a>
    <?php endif; ?>
    </header>
    <table class="table table-hover">
      <thead>
        <tr>
          <!-- <th> TODO: can be uncommented later if deletion is required
          <input type="checkbox" id="determine_chk_all"/>
        </th> -->
        <th><?php echo $phrases['determine'] ?></th>
        <th><?php echo $phrases['name'] ?></th>
        <th><?php echo $phrases['sample_volume'] ?></th>
        <th><?php echo $phrases['unit'] ?></th>
        <th><?php echo $phrases['date'] ?></th>
        <th><?php echo $phrases['description'] ?></th>
        <th><?php echo $phrases['operation'] ?></th>
      </tr>
    </thead>
    <tbody id="determine_body">
    <!-- AJAX LOAD determine_body.php -->
    </tbody>
  </table>
</section>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="determine_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" >×</button>
        <h4 class="modal-title"><?php echo $phrases['add_bio_record'] ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="determine_form" enctype="multipart/form-data" novalidate>
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo $phrases['name'] ?></label>
            <select class="form-control" name="name" id="determine_name" required>
              <option value="" ><?php echo $phrases['choose']?></option>
              <option value="whole_blood"><?php echo $phrases['whole_blood']?></option>
              <option value="blood_plasma"><?php echo $phrases['blood_plasma']?></option>
              <option value="blood_serum"><?php echo $phrases['blood_serum']?></option>
              <option value="urine"><?php echo $phrases['urine']?></option>
              <option value="dung"><?php echo $phrases['dung']?></option>
              <option value="saliva"><?php echo $phrases['saliva']?></option>
              <option value="milk"><?php echo $phrases['milk']?></option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['quantity_bgc'] ?></label>
            <input type="text" class="form-control" id="determine_quantity" name="quantity" placeholder="">
          </div>
          <div class="form-group">
            <label ><?php echo $t->tryTranslate('unit'); ?></label>
            <input type="text" class="form-control" id="determine_unit" name="unit" placeholder="" required>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['determine_company'] ?></label>
            <input type="text" class="form-control" id="determine_company" name="determine_company" placeholder="">
          </div>
          <div class="form-group">
            <label class="control-label col-lg-3"><?php echo $phrases['date_determine']?></label>
            <div class="col-lg-4">
              <input class="form-control form-control-inline"
              required name="determine_start" id="determine_start" type="text" value="">

            </div>
            <div class="col-lg-1">
              to
            </div>
            <div class="col-lg-4">
              <input class="form-control form-control-inline"
              required name="determine_end" id="determine_end" type="text" value="">

            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputFile"><?php echo $phrases['determine_record']?></label>
            <input type="file" id="determine_record" name="determine_record" required>
            <!-- <input type="file" id="determine_record" name="determine_record[]" multiple="" required> -->
            <a href="" id="update_determine_record" class="btn btn-sm btn-danger" target="_blank" title="view file" style="display:none"><?php echo $phrases['view_attachments']; ?></a>
          </div>


          <div class="form-group">
            <input type="checkbox" name="databank_record" value="1" id="bio_determine_record_databank">
            <label for="bio_determine_record_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
          </div>

          <!-- TODO: Add loading animation -->
          <div class="form-group">
            <label for="exampleInputFile"><?php echo $phrases['determine_report']?></label>
            <input type="file" id="determine_report" name="determine_report" required>
            <!-- <input type="file" id="determine_report" name="determine_report[]" multiple="" required> -->
            <a href="" id="update_determine_report" class="btn btn-sm btn-danger" target="_blank" title="view file" style="display:none"><?php echo $phrases['view_attachments']; ?></a>
          </div>


          <div class="form-group">
            <input type="checkbox" name="databank_report" value="1" id="bio_determine_report_databank">
            <label for="bio_determine_report_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
          </div>

          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['description'] ?></label>
            <textarea class="form-control v-resize-only" id="determine_description" name="description" placeholder="" ></textarea>
          </div>
          <input type="hidden" name="determine_id_update" id="determine_id_update" value="">
          <p style="color:red; font-size:15px" id="determine_err"></p>
          <?php if($_SESSION['user_type'] == 'BGC'):?>
          <button type="" class="btn btn-primary" id="determine_submit"><?php echo $phrases['submit'] ?></button>
        <?php endif; ?>

        <!-- BUTTON FOR NOTIFY FOR DA -->
        <?php if($_SESSION["user_type"] == "DA" ): ?>
          <button class="btn btn-primary" type="button" onclick="notifyOwner('<?php echo $_GET['project_id']?>', 'BGC', 'determine')">
            <?php echo $t->tryTranslate('notify')?>
          </button>
        <?php endif; ?>
        <!-- / BUTTON FOR NOTIFY FOR DA -->

        </form>
      </div>
    </div>
  </div>
</div>

<!--  REMARK MODAL  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="determine_remark_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $phrases['add_remarks'] ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="determine_remark_form">
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo $phrases['remarks'] ?></label>
            <textarea class="form-control v-resize-only" id="determine_remarks" name="remarks" placeholder="" required></textarea>
            <input type='hidden' name="determine_id" id="determine_remark_id" value="">
          </div>
          <?php $bio_remarks_users = array("BGC", "DA"); ?>
          <button type="" class="btn btn-primary make-loading" id="determine_remark_submit" <?php if(!in_array($_SESSION['user_type'], $bio_remarks_users)) echo 'disabled'; ?>><?php echo $phrases['submit'] ?></button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- / REMARK MODAL  -->

<!--  DOUBLE CHECK MODAL  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="determine_dbl_chk_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $t->tryTranslate('add_double_check_opinion') ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="determine_dbl_chk_form">
          <div class="form-group">
            <div class="form-group <?php echo ($_SESSION['user_type'] == 'DA') ? '' : 'hidden' ; ?>">
              <div class="radio">
                <input value="1" name="is_checked" id="determine_dbl_chk_pass" type="radio" onClick="clearDblCheck('determine_comment', this.value);"> <?php echo $t->tryTranslate('no_problem') ?> &nbsp;&nbsp;
                <input value="-1" name="is_checked" id="determine_dbl_chk_nopass" type="radio" onClick="clearDblCheck('determine_comment', this.value);"> <?php echo $t->tryTranslate('have_problem') ?> </div>
            </div>
            <label for="exampleInputEmail1" class="dbl_chk_label"><?php echo $t->tryTranslate('comments') ?></label>
            <textarea class="form-control v-resize-only" id="determine_comment" name="comments" <?php echo ($_SESSION['user_type'] != 'DA') ? 'disabled' : '' ;?> placeholder=""></textarea>
            <input type='hidden' name="bio_id" id="determine_dbl_chk_id" value="">  <!-- unique id of PK-->
            <input type='hidden' name="payload_key" id="determine_payload_key" value="">  <!-- key of payload to modify -->
            <input type='hidden' name="table" id="determine_dbl_chk_tbl" value="bio_determine"> <!-- table name to modify -->
            <input type='hidden' name="pk_col" id="determine_pk_col" value="determine_id">  <!-- primary key column name of table-->
          </div>
          <?php if($_SESSION['user_type'] == 'DA'):?>
          <button type="" class="btn btn-primary make-loading" id="determine_dbl_chk_submit"><?php echo $phrases['submit'] ?></button>
        <?php endif; ?>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- / DOUBLE CHECK MODAL  -->
