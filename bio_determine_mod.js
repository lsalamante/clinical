var determine_form = $('#determine_form');
var determine_add = $("#determine_add");
var determine_del = $("#determine_del");
var determine_chk_all = $("#determine_chk_all");
/* for remarks */
var determine_remark_form = $("#determine_remark_form");
var determine_remark_id = $("#determine_remark_id");
var determine_remarks = $("#determine_remarks");
var determine_remark_submit = $("#determine_remark_submit");

ajax_load(data, 'bio_determine_body.php', 'determine_body');

$("#determine_start").datetimepicker();
$("#determine_end").datetimepicker();

/* this is for resetting the form everytime add is clicked*/
determine_add.on('click', function() {
  $("#determine_id_update").val("");
  determine_form[0].reset();
  $('#determine_submit').html('<?php echo $phrases['submit']?>');
  $('#update_determine_record').attr('style', 'display:none');
  $('#update_determine_report').attr('style', 'display:none');
  $('#determine_record').attr('style', 'display:block');
  $('#determine_report').attr('style', 'display:block');

});

/* this is the listener for our submit button from the modal*/
determine_form.on('submit', function (e){
  e.preventDefault();
  var res = true;
  // here I am checking for textFields, password fields, and any
  // drop down you may have in the form
  $("input[type='text'],select,input[type='password']",this).each(function() {
      if($(this).val().trim() == "") {
          res = false;
      }
  })
  if(res){
    $('#determine_err').text('');
    add_determine_info();
  }else{
    $('#determine_err').text('请填写所有必填字段');
  }
});

/* this is for our check all/ uncheck all function */
/* check_all and uncheck_all are found in initialize.js */
determine_chk_all.change(function(){
  this.checked ? check_all(determine_body) :  uncheck_all(determine_body);
});

/* this is our function for inserting and uploading data */
function add_determine_info(){
  var form_data = new FormData(determine_form[0]);
  form_data.append('project_id', "<?php echo $_GET['project_id'] ?>");
  form_data.append('role_id', '<?php echo $_SESSION['role_id'] ?>');

  $.ajax({
    url: "php-functions/fncBioDetermine.php?action=uploadBioSample",
    type:'POST',
    processData: false,
    contentType: false,
    data:form_data,
    success:function(msg){
      if(msg == 1){
        show_alert("<?php echo $phrases['submit']?>", "", 1);
        //                                  #We reload the table
        ajax_load(data, 'bio_determine_body.php', 'determine_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });
}

/* this is for deleting data */
determine_del.on('click', function(){
  $("#determine_body input:checkbox:checked").each(function() {
    var _id = $(this).attr('id');
    _id = _id.split('-');
    /* push the number only */
    del_arr.push(_id[1]);
  });
  var form_data = {
    data: "del_arr=" + del_arr.join(",")
  };
  $.ajax({
    url: "php-functions/fncCommon.php?action=delUpInArray",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        /* Clear our array */
        del_arr = [];
        show_alert("<?php echo $phrases['deleted']?>", "", 1);
        /* #We reload the table */
        ajax_load(data, 'bio_determine_body.php', 'determine_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
      determine_chk_all.prop('checked', false);
    }
  });
});

changeDetermineRemarkId = function (determine_id) {
  determine_remark_id.val(determine_id);

  $.ajax({
    url:"php-functions/fncBioDetermine.php?action=getBioDetails",
    type: "POST",
    dataType: "json",
    data: { "determine_id" : determine_id, "type" : "ajax" },
    success:function(data){
      $('#determine_remarks').val(data[0].remarks);
    }
  });
}


determine_remark_form.on('submit', function(e) {
  e.preventDefault();
  var form_data = determine_remark_form.serialize();

  $.ajax({
    url: "php-functions/fncBioDetermine.php?action=addRemarks",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        show_alert("<?php echo $phrases['submit']?>", "", 1);
        ajax_load(data, 'bio_determine_body.php', 'determine_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });

});

//Payload goes here

$("#determine_dbl_chk_form").on('submit', function(e){ //modify payload data using ajax
  e.preventDefault();

  $.ajax({
    url:"ajax/double_check/updatePayload.php",
    type: "POST",
    dataType: "json",
    data: $("#determine_dbl_chk_form").serialize(),
    success:function(data){ // data is the id of the row
      $("#determine_dbl_chk_modal ").modal("hide");
      getDetermineDetails(data, event);
    }
  });
});

determineSetPayload = function(caller){ //Set the payload key to be edited
  $("#determine_dbl_chk_form")[0].reset();
  $("#determine_payload_key").val($(caller).data('key'));

  //for setting default comments
  $("#determine_comment").text($(caller).data('comments'));

  //for setting default value of checkbox
  if($(caller).data('is_checked') == 1){
    $("#determine_dbl_chk_pass").attr('checked', true);
    $("#determine_dbl_chk_nopass").attr('checked', false);
  }else if($(caller).data('is_checked') == -1)  {
    $("#determine_dbl_chk_nopass").attr('checked', true);
    $("#determine_dbl_chk_pass").attr('checked', false);
  }else{
    $("#determine_dbl_chk_nopass").attr('checked', false);
    $("#determine_dbl_chk_pass").attr('checked', false);
  }


}
// Payload
