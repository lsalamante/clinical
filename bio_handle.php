
<div class="panel-body">
  <section class="panel negative-panel" style="margin-top:0px!important">
    <header class="panel-heading" style="padding-left:0px;">
      <?php if($_SESSION['user_type'] == 'BGC' && $project['status_num'] >= 14):?>
      <a href="#bio_handle_modal" data-toggle="modal" class="btn btn-xs btn-primary"
      id="bio_handle_add" onclick="rmDoubleCheck();"><?php echo $phrases['add']?></a>
    <?php endif; ?>
    </header>
    <table class="table table-hover">
      <thead>
        <tr>
          <!-- <th> TODO: can be uncommented later if deletion is required
          <input type="checkbox" id="bgc_chk_all"/>
        </th> -->
        <th><?php echo $phrases['number'] ?></th>
        <th><?php echo $phrases['name'] ?></th>
        <th><?php echo $phrases['sample_volume'] ?></th>
        <th><?php echo $phrases['unit'] ?></th>
        <th><?php echo $phrases['date'] ?></th>
        <th><?php echo $phrases['description'] ?></th>
        <th><?php echo $phrases['operation'] ?></th>
      </tr>
    </thead>
    <tbody id="bio_handle_body">
    <!-- AJAX LOAD bgc_body.php -->
    </tbody>
  </table>
</section>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bio_handle_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" >×</button>
        <h4 class="modal-title"><?php echo $phrases['add_bio_record'] ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="bio_handle_form" enctype="multipart/form-data" novalidate>
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo $phrases['name'] ?></label>
            <select class="form-control" name="name" id="bio_handle_name" required>
              <option value="" ><?php echo $phrases['choose']?></option>
              <option value="whole_blood"><?php echo $phrases['whole_blood']?></option>
              <option value="blood_plasma"><?php echo $phrases['blood_plasma']?></option>
              <option value="blood_serum"><?php echo $phrases['blood_serum']?></option>
              <option value="urine"><?php echo $phrases['urine']?></option>
              <option value="dung"><?php echo $phrases['dung']?></option>
              <option value="saliva"><?php echo $phrases['saliva']?></option>
              <option value="milk"><?php echo $phrases['milk']?></option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['quantity_bgc'] ?></label>
            <input type="text" class="form-control" id="bio_handle_quantity" name="quantity" placeholder="">
          </div>
          <div class="form-group">
            <label ><?php echo $t->tryTranslate('unit'); ?></label>
            <input type="text" class="form-control" id="bio_handle_unit" name="unit" placeholder="" required>
          </div>
          <div class="form-group">
            <label class="control-label " style="margin-top: 5px;"><?php echo $phrases['handle_time'] ?></label>
            <div class="">
              <input class="form-control form-control-inline"
              required name="handle_start" id="handle_start" type="text" value="">

            </div>
            <div class="">
              to
            </div>
            <div class="">
              <input class="form-control form-control-inline"
              required name="handle_end" id="handle_end" type="text" value="">

            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputFile"><?php echo $phrases['handle_record'] ?></label> <?php // TODO: add translation ?>
            <!-- <input type="file" id="handle_record" name="handle_record[]" multiple="" required> -->
            <input type="file" name="handle_record" id="handle_record" class="form-control">
            <a href="" id="update_handle_record" class="btn btn-sm btn-danger" target="_blank" title="view file" style="display:none"><?php echo $phrases['view_attachments']; ?></a>
          </div>

          <div class="form-group">
            <input type="checkbox" name="databank" value="1" id="bio_handle_databank">
            <label for="bio_handle_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
          </div>

          <!-- TODO: Add loading animation -->
          <!-- <div class="form-group">
            <label for="exampleInputFile"><?php # echo $phrases['process_record']?></label>
            <input type="file" id="process_record" name="process_record[]" multiple="" required>
            <a href="" id="update_process_record" class="btn btn-sm btn-danger" target="_blank" title="view file" style="display:none"><i class="fa fa-eye"></i></a>
          </div> -->
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['description'] ?></label>
            <textarea class="form-control v-resize-only" id="bio_handle_description" name="description" placeholder="" ></textarea>
          </div>
          <input type="hidden" name="bio_handle_id_update" id="bio_handle_id_update" value="">
          <p style="color:red; font-size:15px" id="handle_err"></p>
          <?php if($_SESSION['user_type'] == 'BGC'):?>
          <button type="" class="btn btn-primary" id="bio_handle_submit"><?php echo $phrases['submit'] ?></button>
        <?php endif; ?>

        <!-- BUTTON FOR NOTIFY FOR DA -->
        <?php if($_SESSION["user_type"] == "DA" ): ?>
          <button class="btn btn-primary" type="button" onclick="notifyOwner('<?php echo $_GET['project_id']?>', 'BGC', 'handle')">
            <?php echo $t->tryTranslate('notify')?>
          </button>
        <?php endif; ?>
        <!-- / BUTTON FOR NOTIFY FOR DA -->
        </form>
      </div>
    </div>
  </div>
</div>

<!--  REMARK MODAL  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bio_handle_remark_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $phrases['add_remarks'] ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="bio_handle_remark_form">
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo $phrases['remarks'] ?></label>
            <textarea class="form-control v-resize-only" id="bio_handle_remarks" name="remarks" placeholder="" required></textarea>
            <input type='hidden' name="bio_handle_id" id="bio_handle_remark_id" value="">
          </div>
          <?php $bio_remarks_users = array("BGC", "DA"); ?>
          <button type="" class="btn btn-primary make-loading" id="bio_handle_remark_submit" <?php if(!in_array($_SESSION['user_type'], $bio_remarks_users)) echo 'disabled'; ?>><?php echo $phrases['submit'] ?></button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- / REMARK MODAL  -->


<!--  DOUBLE CHECK MODAL  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bio_handle_dbl_chk_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $t->tryTranslate('add_double_check_opinion') ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="bio_handle_dbl_chk_form">
          <div class="form-group">
            <div class="form-group <?php echo ($_SESSION['user_type'] == 'DA') ? '' : 'hidden' ; ?>">
              <div class="radio">
                <input value="1" name="is_checked" id="bio_handle_dbl_chk_pass" type="radio" onClick="clearDblCheck('bio_handle_comment', this.value);"> <?php echo $t->tryTranslate('no_problem') ?> &nbsp;&nbsp;
                <input value="-1" name="is_checked" id="bio_handle_dbl_chk_nopass" type="radio" onClick="clearDblCheck('bio_handle_comment', this.value);"> <?php echo $t->tryTranslate('have_problem') ?> </div>
            </div>
            <label for="exampleInputEmail1" class="dbl_chk_label"><?php echo $t->tryTranslate('comments') ?></label>
            <textarea class="form-control v-resize-only" id="bio_handle_comment" name="comments" <?php echo ($_SESSION['user_type'] != 'DA') ? 'disabled' : '' ;?> placeholder=""></textarea>
            <input type='hidden' name="bio_id" id="bio_handle_dbl_chk_id" value="">  <!-- unique id of PK-->
            <input type='hidden' name="payload_key" id="bio_handle_payload_key" value="">  <!-- key of payload to modify -->
            <input type='hidden' name="table" id="bio_handle_dbl_chk_tbl" value="bio_handles"> <!-- table name to modify -->
            <input type='hidden' name="pk_col" id="bio_handle_pk_col" value="bio_handle_id">  <!-- primary key column name of table-->
          </div>
          <?php if($_SESSION['user_type'] == 'DA'):?>
          <button type="" class="btn btn-primary make-loading" id="bio_handle_dbl_chk_submit"><?php echo $phrases['submit'] ?></button>
        <?php endif; ?>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- / DOUBLE CHECK MODAL  -->
