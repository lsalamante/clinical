var bio_handle_form = $('#bio_handle_form');
var bio_handle_add = $("#bio_handle_add");
var bio_handle_del = $("#bio_handle_del");
var bio_handle_chk_all = $("#bio_handle_chk_all");
/* for remarks */
var bio_handle_remark_form = $("#bio_handle_remark_form");
var bio_handle_remark_id = $("#bio_handle_remark_id");
var bio_handle_remarks = $("#bio_handle_remarks");
var bio_handle_remark_submit = $("#bio_handle_remark_submit");

ajax_load(data, 'bio_handle_body.php', 'bio_handle_body');

$("#handle_start").datetimepicker();
$("#handle_end").datetimepicker();

/* this is for resetting the form everytime add is clicked*/
bio_handle_add.on('click', function() {
  $("#bio_handle_id_update").val("");
  bio_handle_form[0].reset();
  $('#bio_handle_submit').html('<?php echo $phrases['submit']?>');
  $('#update_handle_record').attr('style', 'display:none');
});

/* this is the listener for our submit button from the modal*/
bio_handle_form.on('submit', function (e){
  e.preventDefault();
  var res = true;
  // here I am checking for textFields, password fields, and any
  // drop down you may have in the form
  $("input[type='text'],select,input[type='password']",this).each(function() {
      if($(this).val().trim() == "") {
          res = false;
      }
  })
  if(res){
    $('#handle_err').text('');
    add_bio_handle_info();
  }else{
    $('#handle_err').text('请填写所有必填字段');
  }
});

/* this is for our check all/ uncheck all function */
/* check_all and uncheck_all are found in initialize.js */
bio_handle_chk_all.change(function(){
  this.checked ? check_all(bio_handle_body) :  uncheck_all(bio_handle_body);
});

/* this is our function for inserting and uploading data */
function add_bio_handle_info(){
  var form_data = new FormData(bio_handle_form[0]);
  form_data.append('project_id', "<?php echo $_GET['project_id'] ?>");
  form_data.append('role_id', '<?php echo $_SESSION['role_id'] ?>');

  $.ajax({
    url: "php-functions/fncBioHandles.php?action=uploadBioSample",
    type:'POST',
    processData: false,
    contentType: false,
    data:form_data,
    success:function(msg){
      if(msg == 1){
        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
        //                                  #We reload the table
        ajax_load(data, 'bio_handle_body.php', 'bio_handle_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });
}

/* this is for deleting data */
bio_handle_del.on('click', function(){
  $("#bio_handle_body input:checkbox:checked").each(function() {
    var _id = $(this).attr('id');
    _id = _id.split('-');
    /* push the number only */
    del_arr.push(_id[1]);
  });
  var form_data = {
    data: "del_arr=" + del_arr.join(",")
  };
  $.ajax({
    url: "php-functions/fncCommon.php?action=delUpInArray",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        /* Clear our array */
        del_arr = [];
        show_alert("<?php echo $phrases['deleted']?>", "", 1);
        /* #We reload the table */
        ajax_load(data, 'bio_handle_body.php', 'bio_handle_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
      bio_handle_chk_all.prop('checked', false);
    }
  });
});

changeBioRemarkId = function (bio_handle_id) {
  bio_handle_remark_id.val(bio_handle_id);

  $.ajax({
    url:"php-functions/fncBioHandles.php?action=getBioDetails",
    type: "POST",
    dataType: "json",
    data: { "bio_handle_id" : bio_handle_id, "type" : "ajax" },
    success:function(data){
      $('#bio_handle_remarks').val(data[0].remarks);
    }
  });
}

bio_handle_remark_form.on('submit', function(e) {
  e.preventDefault();
  var form_data = bio_handle_remark_form.serialize();

  $.ajax({
    url: "php-functions/fncBioHandles.php?action=addRemarks",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
        ajax_load(data, 'bio_handle_body.php', 'bio_handle_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });

});

//Payload goes here

$("#bio_handle_dbl_chk_form").on('submit', function(e){ //modify payload data using ajax
  e.preventDefault();

  $.ajax({
    url:"ajax/double_check/updatePayload.php",
    type: "POST",
    dataType: "json",
    data: $("#bio_handle_dbl_chk_form").serialize(),
    success:function(data){ // data is the id of the row
      $("#bio_handle_dbl_chk_modal ").modal("hide");
      getBioHandleDetails(data, event);
    }
  });
});

handleSetPayload = function(caller){ //Set the payload key to be edited
  $("#bio_handle_dbl_chk_form")[0].reset();

  $("#bio_handle_payload_key").val($(caller).data('key'));

  //for setting default comments
  $("#bio_handle_comment").text($(caller).data('comments'));

  //for setting default value of checkbox
  if($(caller).data('is_checked') == 1){
    $("#bio_handle_dbl_chk_pass").attr('checked', true);
    $("#bio_handle_dbl_chk_nopass").attr('checked', false);
  }else if($(caller).data('is_checked') == -1)  {
    $("#bio_handle_dbl_chk_nopass").attr('checked', true);
    $("#bio_handle_dbl_chk_pass").attr('checked', false);
  }else{
    $("#bio_handle_dbl_chk_nopass").attr('checked', false);
    $("#bio_handle_dbl_chk_pass").attr('checked', false);
  }

}
// Payload
