
<div class="row">
	<div class="col-lg-2">
		<section class="panel">
			<div class="panel-body">
				<center style="font-size:18px;">
					<a class="<?php echo (!isset($_GET['bio_detail'])) ? 'bio-shadow' : '' ;?>" href="<?php echo $PAGE_NAME."?project_id=".$_GET['project_id']."&tab=bio-management"; ?>"><?php echo $phrases['collect']; ?></a>
				</center>
			</div>
		</section>
	</div>
	<div class="col-lg-2">
		<section class="panel">
			<div class="panel-body">
				<center style="font-size:18px;">
					<a class="<?php echo ($_GET['bio_detail'] == 'handle') ? 'bio-shadow' : '' ;?>" href="<?php echo $PAGE_NAME."?project_id=".$_GET['project_id']."&tab=bio-management&bio_detail=handle"; ?>"><?php echo $phrases['handle']; ?></a>
				</center>
			</div>
		</section>
	</div>
	<div class="col-lg-2">
		<section class="panel">
			<div class="panel-body">
				<center style="font-size:18px;">
					<a class="<?php echo ($_GET['bio_detail'] == 'save') ? 'bio-shadow' : '' ;?>" href="<?php echo $PAGE_NAME."?project_id=".$_GET['project_id']."&tab=bio-management&bio_detail=save"; ?>"><?php echo $phrases['save']; ?></a>
				</center>
			</div>
		</section>
	</div>
	<div class="col-lg-2">
		<section class="panel">
			<div class="panel-body">
				<center style="font-size:18px;">
					<a class="<?php echo ($_GET['bio_detail'] == 'transport') ? 'bio-shadow' : '' ;?>" href="<?php echo $PAGE_NAME."?project_id=".$_GET['project_id']."&tab=bio-management&bio_detail=transport"; ?>"><?php echo $phrases['transport']; ?></a>
				</center>
			</div>
		</section>
	</div>
	<div class="col-lg-2">
		<section class="panel">
			<div class="panel-body">
				<center style="font-size:18px;">
					<a class="<?php echo ($_GET['bio_detail'] == 'determine') ? 'bio-shadow' : '' ;?>" href="<?php echo $PAGE_NAME."?project_id=".$_GET['project_id']."&tab=bio-management&bio_detail=determine"; ?>"><?php echo $phrases['determine']; ?></a>
				</center>
			</div>
		</section>
	</div>
	<div class="col-lg-2">
		<section class="panel">
			<div class="panel-body">
				<center style="font-size:18px;">
					<a class="<?php echo ($_GET['bio_detail'] == 'recycle') ? 'bio-shadow' : '' ;?>" href="<?php echo $PAGE_NAME."?project_id=".$_GET['project_id']."&tab=bio-management&bio_detail=recycle"; ?>"><?php echo $phrases['recycle']; ?></a>
				</center>
			</div>
		</section>
	</div>
	<div class="col-lg-2">
		<section class="panel">
			<div class="panel-body">
				<center style="font-size:18px;">
					<a class="<?php echo ($_GET['bio_detail'] == 'destroy') ? 'bio-shadow' : '' ;?>" href="<?php echo $PAGE_NAME."?project_id=".$_GET['project_id']."&tab=bio-management&bio_detail=destroy"; ?>"><?php echo $phrases['destroy']; ?></a>
				</center>
			</div>
		</section>
	</div>
</div>

<?php
$name_dropdown = array(
	"whole_blood" => $phrases['whole_blood'],
	"blood_plasma" => $phrases['blood_plasma'],
	"blood_serum" => $phrases['blood_serum'],
	"urine" => $phrases['urine'],
	"dung" => $phrases['dung'],
	"saliva" => $phrases['saliva'],
	"milk" => $phrases['milk']
	);

$_GET['bio_detail'] = (empty($_GET['bio_detail'])) ? '' : $_GET['bio_detail'];
switch($_GET['bio_detail']){
case 'handle': include('bio_handle.php');
break;
case 'save': include("bio_save.php");
break;
case 'transport': include("bio_transport.php");
break;
case 'determine': include('bio_determine.php');
break;
case 'recycle': include('bio_recycle.php');
break;
case 'destroy': include('bio_destroy.php');
break;
default: include('bio_collect.php');
break;
} ?>
