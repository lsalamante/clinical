<div class="panel-body">
  <section class="panel negative-panel" style="margin-top:0px!important">
    <header class="panel-heading" style="padding-left:0px;">
      <?php if($_SESSION['user_type'] == 'BGC' && $project['status_num'] >= 14):?>
      <a href="#bio_recycle_modal" data-toggle="modal" class="btn btn-xs btn-primary"
       id="add_bio_recycle"><?php echo $phrases['add']?></a>
     <?php endif; ?>
    </header>
    <table class="table table-hover">
      <thead>
        <tr>
          <!-- <th> TODO: can be uncommented later if deletion is required
          <input type="checkbox" id="recycle_chk_all"/>
        </th> -->
        <th><?php echo $phrases['number'] ?></th>
        <th><?php echo $phrases['name'] ?></th>
        <th><?php echo $phrases['sample_volume'] ?></th>
        <th><?php echo $phrases['unit'] ?></th>
        <th><?php echo $phrases['date'] ?></th>
        <th><?php echo $phrases['description'] ?></th>
        <th><?php echo $phrases['operation'] ?></th>
      </tr>
    </thead>
    <tbody id="bio_recycle_body">
    <!-- AJAX LOAD recycle_body.php -->
    </tbody>
  </table>
</section>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bio_recycle_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" >×</button>
        <h4 class="modal-title"><?php echo $phrases['add_bio_record'] ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="bio_recycle_form" enctype="multipart/form-data" novalidate>
          <div class="form-group">
            <label><?php echo $phrases['name'] ?></label>
            <select class="form-control" name="name" id="bio_recycle_name" required>
              <option value="" ><?php echo $phrases['choose']?></option>
              <?php foreach ($name_dropdown as $value => $text) { ?>
                <option value="<?php echo $value; ?>"><?php echo $text; ?></option>
              <?php } ?>
            </select>
          </div>

          <div class="form-group">
            <label><?php echo $phrases['quantity_bgc'] ?></label>
            <input type="text" class="form-control" name="quantity" id="bio_recycle_quantity" placeholder="">
          </div>
          <div class="form-group">
            <label ><?php echo $t->tryTranslate('unit'); ?></label>
            <input type="text" class="form-control" id="bio_recycle_unit" name="unit" placeholder="" required>
          </div>

          <!-- Sending Party -->
          <div class="form-group">
            <label><?php echo $phrases['sending_party'] ?></label>
            <input type="text" class="form-control" name="sending_party" id="bio_recycle_sending_party" placeholder="">
          </div>
          <div class="form-group">
            <label><?php echo $phrases['operator'] ?></label>
            <input type="text" class="form-control" name="sending_operator" id="bio_recycle_sending_operator" placeholder="">
          </div>
          <div class="form-group">
            <label><?php echo $phrases['date'] ?></label>
            <input type="text" class="form-control form-control-inline" name="sending_date" id="bio_recycle_sending_date" placeholder="">
          </div>
          <!-- Sending Party -->

          <!-- Shipper -->
          <div class="form-group">
            <label><?php echo $phrases['shipper'] ?></label>
            <input type="text" class="form-control" name="shipper" id="bio_recycle_shipper" placeholder="">
          </div>
          <div class="form-group">
            <label><?php echo $phrases['operator'] ?></label>
            <input type="text" class="form-control" name="shipper_operator" id="bio_recycle_shipper_operator" placeholder="">
          </div>
          <div class="form-group">
            <label><?php echo $phrases['date'] ?></label>
            <input type="text" class="form-control form-control-inline" name="shipper_date" id="bio_recycle_shipper_date" placeholder="">
          </div>
          <!-- Shipper -->

          <!-- Recycler -->
          <div class="form-group">
            <label><?php echo $phrases['recycler'] ?></label>
            <input type="text" class="form-control" name="recycler" id="bio_recycle_recycler" placeholder="">
          </div>
          <div class="form-group">
            <label><?php echo $phrases['operator'] ?></label>
            <input type="text" class="form-control" name="recycler_operator" id="bio_recycle_recycler_operator" placeholder="">
          </div>
          <div class="form-group">
            <label><?php echo $phrases['date'] ?></label>
            <input type="text" class="form-control form-control-inline" name="recycler_date" id="bio_recycle_recycler_date" placeholder="">
          </div>
          <!-- Recycler -->

          <div class="form-group">
            <label><?php echo $phrases['recycle_report'] ?></label>
            <input type="file" class="form-control" name="recycle_record" id="recycle_record" placeholder="">
            <a href="" id="bio_recycle_record_a" style="display:none" class="btn btn-sm btn-danger" target="_blank"><?php echo $phrases['view_attachments']; ?></a>
          </div>

          <div class="form-group">
            <input type="checkbox" name="databank" value="1" id="bio_recycle_databank">
            <label for="bio_recycle_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
          </div>

          <div class="form-group">
            <label><?php echo $phrases['description'] ?></label>
            <textarea name="bio_desc" id="bio_recycle_desc" class="form-control"></textarea>
          </div>

          <input type="hidden" name="bio_recycle_id_update" id="bio_recycle_id_update" value="">
          <p style="color:red; font-size:15px" id="recycle_err"></p>
          <?php if($_SESSION['user_type'] == 'BGC'):?>
          <button type="" class="btn btn-primary" id="bio_recycle_submit"><?php echo $phrases['submit'] ?></button>
        <?php endif; ?>
        <!-- BUTTON FOR NOTIFY FOR DA -->
        <?php if($_SESSION["user_type"] == "DA" ): ?>
          <button class="btn btn-primary" type="button" onclick="notifyOwner('<?php echo $_GET['project_id']?>', 'BGC', 'recycle')">
            <?php echo $t->tryTranslate('notify')?>
          </button>
        <?php endif; ?>
        <!-- / BUTTON FOR NOTIFY FOR DA -->
        </form>
      </div>
    </div>
  </div>
</div>

<!--  REMARK MODAL  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bio_recycle_remark_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $phrases['add_remarks'] ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="bio_recycle_remark_form">
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo $phrases['remarks'] ?></label>
            <textarea class="form-control v-resize-only" id="bio_recycle_remarks" name="remarks" placeholder="" required></textarea>
            <input type='hidden' name="bio_recycle_id" id="bio_recycle_id" value="">
          </div>
          <?php $bio_remarks_users = array("BGC", "DA"); ?>
          <button type="" class="btn btn-primary make-loading" id="recycle_remark_submit" <?php if(!in_array($_SESSION['user_type'], $bio_remarks_users)) echo 'disabled'; ?>><?php echo $phrases['submit'] ?></button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- / REMARK MODAL  -->

<!--  DOUBLE CHECK MODAL  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="recycle_dbl_chk_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $t->tryTranslate('add_double_check_opinion') ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="recycle_dbl_chk_form">
          <div class="form-group">
            <div class="form-group <?php echo ($_SESSION['user_type'] == 'DA') ? '' : 'hidden' ; ?>">
              <div class="radio">
                <input value="1" name="is_checked" id="recycle_dbl_chk_pass" type="radio" onClick="clearDblCheck('recycle_comment', this.value);"> <?php echo $t->tryTranslate('no_problem') ?> &nbsp;&nbsp;
                <input value="-1" name="is_checked" id="recycle_dbl_chk_nopass" type="radio" onClick="clearDblCheck('recycle_comment', this.value);"> <?php echo $t->tryTranslate('have_problem') ?> </div>
            </div>
            <label for="exampleInputEmail1" class="dbl_chk_label"><?php echo $t->tryTranslate('comments') ?></label>
            <textarea class="form-control v-resize-only" id="recycle_comment" name="comments" <?php echo ($_SESSION['user_type'] != 'DA') ? 'disabled' : '' ;?> placeholder=""></textarea>
            <input type='hidden' name="bio_id" id="recycle_dbl_chk_id" value="">  <!-- unique id of PK-->
            <input type='hidden' name="payload_key" id="recycle_payload_key" value="">  <!-- key of payload to modify -->
            <input type='hidden' name="table" id="recycle_dbl_chk_tbl" value="bio_recycle"> <!-- table name to modify -->
            <input type='hidden' name="pk_col" id="recycle_pk_col" value="bio_recycle_id">  <!-- primary key column name of table-->
          </div>
          <?php if($_SESSION['user_type'] == 'DA'):?>
          <button type="" class="btn btn-primary make-loading" id="recycle_dbl_chk_submit"><?php echo $phrases['submit'] ?></button>
        <?php endif; ?>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- / DOUBLE CHECK MODAL  -->
