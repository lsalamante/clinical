var bio_recycle_form = $('#bio_recycle_form');
var bio_recycle_submit = $('#bio_recycle_submit');
var add_bio_recycle = $('#add_bio_recycle');
$('#bio_recycle_sending_date').datetimepicker();
$('#bio_recycle_shipper_date').datetimepicker();
$('#bio_recycle_recycler_date').datetimepicker();

ajax_load(data, "bio_recycle_body.php", "bio_recycle_body");

add_bio_recycle.on('click', function (e){
	bio_recycle_form[0].reset();
	$('#bio_recycle_id_update').val("");
	bio_recycle_submit.html('<?php echo $phrases['submit']?>');
	$('#bio_recycle_record_a').attr('href', "");
	$('#bio_recycle_record_a').attr('style', 'display:none');
});


bio_recycle_form.on('submit', function (e){
  e.preventDefault();
	var res = true;
	// here I am checking for textFields, password fields, and any
	// drop down you may have in the form
	$("input[type='text'],select,input[type='password']",this).each(function() {
			if($(this).val().trim() == "") {
					res = false;
			}
	})
	if(res){
		$('#recycle_err').text('');
		add_bio_recycle_info();
	}else{
		$('#recycle_err').text('请填写所有必填字段');
	}
});
/* for adding new bio recycle info */
function add_bio_recycle_info()
{
	var form_data = new FormData(bio_recycle_form[0]);
	form_data.append('project_id', "<?php echo $_GET['project_id'] ?>");
	form_data.append('role_id', "<?php echo $_SESSION['role_id'] ?>");
	// form_data.append('sub_dir', 'bio_samples');
	$.ajax({
	    url: "php-functions/fncBioRecycle.php?action=addRecycleInfo",
	    type:'POST',
	    processData: false,
	    contentType: false,
	    data:form_data,
	    success:function(msg){
	      if(msg == 1){
	        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
	        //                                  #We reload the table
	        ajax_load(data, "bio_recycle_body.php", "bio_recycle_body");
	      }else{
	        show_alert(msg, "", 0);
	      }
	    }
	  });
}

var bio_recycle_remark_form = $('#bio_recycle_remark_form');
changeBioRemarkId = function (bio_recycle_id) {
  $('#bio_recycle_id').val(bio_recycle_id);

  $.ajax({
    url:"php-functions/fncBiorecycle.php?action=getBioRemarkDetails",
    type: "POST",
    dataType: "json",
    data: { "bio_recycle_id" : bio_recycle_id, "type" : "ajax" },
    success:function(data){
      $('#bio_recycle_remarks').val(data[0].remarks);
    }
  });
}

bio_recycle_remark_form.on('submit', function(e) {
  e.preventDefault();
  var form_data = bio_recycle_remark_form.serialize();

  $.ajax({
    url: "php-functions/fncBiorecycle.php?action=addRemarks",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
        ajax_load(data, 'bio_recycle_body.php', 'bio_recycle_body');
      }else{
        console.log(msg);
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });

});

//Payload goes here

$("#recycle_dbl_chk_form").on('submit', function(e){ //modify payload data using ajax
  e.preventDefault();

  $.ajax({
    url:"ajax/double_check/updatePayload.php",
    type: "POST",
    dataType: "json",
    data: $("#recycle_dbl_chk_form").serialize(),
    success:function(data){ // data is the id of the row
			$("#recycle_dbl_chk_modal ").modal("hide");
      getBioRecycleDetails(data, event);
    }
  });
});

recycleSetPayload = function(caller){ //Set the payload key to be edited
	$("#recycle_dbl_chk_form")[0].reset();
	
  $("#recycle_payload_key").val($(caller).data('key'));

  //for setting default comments
  $("#recycle_comment").text($(caller).data('comments'));

  //for setting default value of checkbox
  if($(caller).data('is_checked') == 1){
    $("#recycle_dbl_chk_pass").attr('checked', true);
    $("#recycle_dbl_chk_nopass").attr('checked', false);
  }else if($(caller).data('is_checked') == -1)  {
    $("#recycle_dbl_chk_nopass").attr('checked', true);
    $("#recycle_dbl_chk_pass").attr('checked', false);
  }else{
    $("#recycle_dbl_chk_nopass").attr('checked', false);
    $("#recycle_dbl_chk_pass").attr('checked', false);
  }

}
// Payload
