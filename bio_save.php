<div class="panel-body">
  <section class="panel negative-panel" style="margin-top:0px!important">
    <header class="panel-heading" style="padding-left:0px;">
      <?php if($_SESSION['user_type'] == 'BGC' && $project['status_num'] >= 14):?>
      <a href="#bio_save_modal" data-toggle="modal" class="btn btn-xs btn-primary"
       id="add_bio_save" onclick="rmDoubleCheck();"><?php echo $phrases['add']?></a>
     <?php endif; ?>
    </header>
    <table class="table table-hover">
      <thead>
        <tr>
          <!-- <th> TODO: can be uncommented later if deletion is required
          <input type="checkbox" id="bio_save_chk_all"/>
        </th> -->
        <th><?php echo $phrases['number']; ?></th>
        <th><?php echo $phrases['name'] ?></th>
        <th><?php echo $phrases['sample_volume'] ?></th>
        <th><?php echo $phrases['unit'] ?></th>
        <th><?php echo $phrases['date'] ?></th>
        <th><?php echo $phrases['description'] ?></th>
        <th><?php echo $phrases['operation'] ?></th>
      </tr>
    </thead>
    <tbody id="bio_save_body">
    <!-- AJAX LOAD bio_save_body.php -->
    </tbody>
  </table>
</section>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bio_save_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button" >×</button>
        <h4 class="modal-title"><?php echo $phrases['add_bio_record'] ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="bio_save_form" enctype="multipart/form-data" novalidate>
          <div class="form-group">
            <label><?php echo $phrases['name'] ?></label>
            <select class="form-control" name="name" id="bio_save_name" required>
              <option value="" ><?php echo $phrases['choose']?></option>
              <?php foreach ($name_dropdown as $value => $text) { ?>
                <option value="<?php echo $value; ?>"><?php echo $text; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label><?php echo $phrases['quantity_bgc'] ?></label>
            <input type="text" class="form-control" name="quantity" id="bio_save_quantity" placeholder="">
          </div>
          <div class="form-group">
            <label ><?php echo $t->tryTranslate('unit'); ?></label>
            <input type="text" class="form-control" id="bio_save_unit" name="unit" placeholder="" required>
          </div>
          <div class="form-group">
            <label class="control-label col-lg-3" style="margin-top:5px;"><?php echo $phrases['collect_time_save']?></label>
            <div class="col-lg-4">
              <input class="form-control form-control-inline"
              required name="collect_start" id="bio_save_collect_start" value="">
            </div>
            <div class="col-lg-1">
              to
            </div>
            <div class="col-lg-4">
              <input class="form-control form-control-inline"
              required name="collect_end" id="bio_save_collect_end" value="">

            </div>
          </div>
          <div class="form-group">
            <label><?php echo $phrases['save_address']?></label>
            <input type="text" name="address" id="bio_save_address" class="form-control" required>
          </div>

          <div class="form-group">
            <label><?php echo $phrases['save_temp'] ?></label>
            <input type="text" name="temp" id="bio_save_temp" class="form-control" required>
          </div>

          <div class="form-group">
            <label><?php echo $phrases['save_humidity'] ?></label>
            <input type="text" name="humidity" id="bio_save_humidity" class="form-control" required>
          </div>

          <div class="form-group">
            <label><?php echo $phrases['process_record_save'] ?></label>
            <input type="file" name="record" id="bio_save_record" class="form-control">
            <a href="" id="bio_save_record_a" style="display:none" class="btn btn-sm btn-danger" target="_blank"><?php echo $phrases['view_attachments']; ?></a>
          </div>

          <div class="form-group">
            <input type="checkbox" name="databank" value="1" id="bio_save_databank">
            <label for="bio_save_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
          </div>

          <div class="form-group">
            <label><?php echo $phrases['description'] ?></label>
            <textarea name="bio_desc" id="bio_save_desc" class="form-control"></textarea>
          </div>
          <input type="hidden" name="bio_save_id_update" id="bio_save_id_update" value="">
          <p style="color:red; font-size:15px" id="save_err"></p>
          <?php if($_SESSION['user_type'] == 'BGC'):?>
          <button type="" class="btn btn-primary" id="bio_save_submit"><?php echo $phrases['submit'] ?></button>
        <?php endif; ?>

        <!-- BUTTON FOR NOTIFY FOR DA -->
        <?php if($_SESSION["user_type"] == "DA" ): ?>
          <button class="btn btn-primary" type="button" onclick="notifyOwner('<?php echo $_GET['project_id']?>', 'BGC', 'save')">
            <?php echo $t->tryTranslate('notify')?>
          </button>
        <?php endif; ?>
        <!-- / BUTTON FOR NOTIFY FOR DA -->
        </form>
      </div>
    </div>
  </div>
</div>

<!--  REMARK MODAL  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bio_save_remark_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $phrases['add_remarks'] ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="bio_save_remark_form">
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo $phrases['remarks'] ?></label>
            <textarea class="form-control v-resize-only" id="bio_save_remarks" name="remarks" placeholder="" required></textarea>
            <input type='hidden' name="bio_save_id" id="bio_save_id" value="">
          </div>
          <?php $bio_remarks_users = array("BGC", "DA"); ?>
          <button type="" class="btn btn-primary make-loading" id="bio_save_remark_submit" <?php if(!in_array($_SESSION['user_type'], $bio_remarks_users)) echo 'disabled'; ?>><?php echo $phrases['submit'] ?></button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- / REMARK MODAL  -->

<!--  DOUBLE CHECK MODAL  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bio_save_dbl_chk_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $t->tryTranslate('add_double_check_opinion') ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="bio_save_dbl_chk_form">
          <div class="form-group">
            <div class="form-group <?php echo ($_SESSION['user_type'] == 'DA') ? '' : 'hidden' ; ?>">
              <div class="radio">
                <input value="1" name="is_checked" id="bio_save_dbl_chk_pass" type="radio" onClick="clearDblCheck('bio_save_comment', this.value);"> <?php echo $t->tryTranslate('no_problem') ?> &nbsp;&nbsp;
                <input value="-1" name="is_checked" id="bio_save_dbl_chk_nopass" type="radio" onClick="clearDblCheck('bio_save_comment', this.value);"> <?php echo $t->tryTranslate('have_problem') ?> </div>
            </div>
            <label for="exampleInputEmail1" class="dbl_chk_label"><?php echo $t->tryTranslate('comments') ?></label>
            <textarea class="form-control v-resize-only" id="bio_save_comment" name="comments" <?php echo ($_SESSION['user_type'] != 'DA') ? 'disabled' : '' ;?> placeholder=""></textarea>
            <input type='hidden' name="bio_id" id="bio_save_dbl_chk_id" value="">  <!-- unique id of PK-->
            <input type='hidden' name="payload_key" id="bio_save_payload_key" value="">  <!-- key of payload to modify -->
            <input type='hidden' name="table" id="bio_save_dbl_chk_tbl" value="bio_save"> <!-- table name to modify -->
            <input type='hidden' name="pk_col" id="bio_save_pk_col" value="bio_save_id">  <!-- primary key column name of table-->
          </div>
          <?php if($_SESSION['user_type'] == 'DA'):?>
          <button type="" class="btn btn-primary make-loading" id="bio_save_dbl_chk_submit"><?php echo $phrases['submit'] ?></button>
        <?php endif; ?>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- / DOUBLE CHECK MODAL  -->
