var bio_save_form = $('#bio_save_form');
var bio_save_submit = $('#bio_save_submit');
var add_bio_save = $('#add_bio_save');
$('#bio_save_collect_start').datetimepicker();
$('#bio_save_collect_end').datetimepicker();
ajax_load(data, "bio_save_body.php", "bio_save_body");

add_bio_save.on('click', function (e){
	bio_save_form[0].reset();
	$('#bio_save_id_update').val("");
	$('#bio_save_submit').html('<?php echo $phrases['submit'] ?>');
	$('#bio_save_record_a').attr('href', "");
	$('#bio_save_record_a').attr('style', 'display:none');
});


bio_save_form.on('submit', function (e){
  e.preventDefault();
	var res = true;

	$("input[type='text'],select,input[type='password']",this).each(function() {
      if($(this).val().trim() == "") {
          res = false;
      }
  })
  if(res){
    $('#save_err').text('');
		add_bio_save_info();
  }else{
    $('#save_err').text('请填写所有必填字段');
  }
});


/* for adding new bio save info */
function add_bio_save_info()
{
	var form_data = new FormData(bio_save_form[0]);
	form_data.append('project_id', "<?php echo $_GET['project_id'] ?>");
	form_data.append('role_id', "<?php echo $_SESSION['role_id'] ?>");
	// form_data.append('sub_dir', 'bio_samples');
	$.ajax({
	    url: "php-functions/fncBioSave.php?action=addInfo",
	    type:'POST',
	    processData: false,
	    contentType: false,
	    data:form_data,
	    success:function(msg){
	      if(msg == 1){
	        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
	        //                                  #We reload the table
	        ajax_load(data, "bio_save_body.php", "bio_save_body");
	      }else{
	        show_alert(msg, "", 0);
	      }
	    }
	  });
}
// remarks here
var bio_save_remark_form = $('#bio_save_remark_form');

changeBioRemarkId = function (bio_save_id) {
  $('#bio_save_id').val(bio_save_id);

  $.ajax({
    url:"php-functions/fncBioSave.php?action=getBioRemarkDetails",
    type: "POST",
    dataType: "json",
    data: { "bio_save_id" : bio_save_id, "type" : "ajax" },
    success:function(data){
      $('#bio_save_remarks').val(data[0].remarks);
    }
  });
}

bio_save_remark_form.on('submit', function(e) {
  e.preventDefault();
  var form_data = bio_save_remark_form.serialize();

  $.ajax({
    url: "php-functions/fncBioSave.php?action=addRemarks",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
        ajax_load(data, 'bio_save_body.php', 'bio_save_body');
      }else{
        console.log(msg);
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });

});

//Payload goes here

$("#bio_save_dbl_chk_form").on('submit', function(e){ //modify payload data using ajax
  e.preventDefault();

  $.ajax({
    url:"ajax/double_check/updatePayload.php",
    type: "POST",
    dataType: "json",
    data: $("#bio_save_dbl_chk_form").serialize(),
    success:function(data){ // data is the id of the row
			$("#bio_save_dbl_chk_modal ").modal("hide");
      getBioSaveDetails(data, event);
    }
  });
});

saveSetPayload = function(caller){ //Set the payload key to be edited
	$("#bio_save_dbl_chk_form")[0].reset();
	
  $("#bio_save_payload_key").val($(caller).data('key'));

  //for setting default comments
  $("#bio_save_comment").text($(caller).data('comments'));

  //for setting default value of checkbox
  if($(caller).data('is_checked') == 1){
    $("#bio_save_dbl_chk_pass").attr('checked', true);
    $("#bio_save_dbl_chk_nopass").attr('checked', false);
  }else if($(caller).data('is_checked') == -1)  {
    $("#bio_save_dbl_chk_nopass").attr('checked', true);
    $("#bio_save_dbl_chk_pass").attr('checked', false);
  }else{
    $("#bio_save_dbl_chk_nopass").attr('checked', false);
    $("#bio_save_dbl_chk_pass").attr('checked', false);
  }

}
// Payload
