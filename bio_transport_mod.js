var transport_form = $('#transport_form');
var transport_add = $("#transport_add");
var transport_del = $("#transport_del");
var transport_chk_all = $("#transport_chk_all");
/* for remarks */
var transport_remark_form = $("#transport_remark_form");
var transport_remark_id = $("#transport_remark_id");
var transport_remarks = $("#transport_remarks");
var transport_remark_submit = $("#transport_remark_submit");

ajax_load(data, 'bio_transport_body.php', 'transport_body');

$("#transport_start").datetimepicker();
$("#transport_end").datetimepicker();

/* this is for resetting the form everytime add is clicked*/
transport_add.on('click', function() {
  $("#transport_id_update").val("");
  transport_form[0].reset();
  $('#process_record').attr('style', 'display:block');
  $('#transport_submit').html('<?php echo $phrases['submit']?>');
  $('#update_transport_process_record').attr('style', 'display:none');
});

/* this is the listener for our submit button from the modal*/
transport_form.on('submit', function (e){
  e.preventDefault();
  var res = true;

	$("input[type='text'],select,input[type='password']",this).each(function() {
      if($(this).val().trim() == "") {
          res = false;
      }
  })
  if(res){
    $('#transport_err').text('');
    add_transport_info();
  }else{
    $('#transport_err').text('请填写所有必填字段');
  }
});

/* this is for our check all/ uncheck all function */
/* check_all and uncheck_all are found in initialize.js */
transport_chk_all.change(function(){
  this.checked ? check_all(transport_body) :  uncheck_all(transport_body);
});

/* this is our function for inserting and uploading data */
function add_transport_info(){
  var form_data = new FormData(transport_form[0]);
  form_data.append('project_id', "<?php echo $_GET['project_id'] ?>");
  form_data.append('role_id', '<?php echo $_SESSION['role_id'] ?>');

  $.ajax({
    url: "php-functions/fncBioTransport.php?action=uploadBioSample",
    type:'POST',
    processData: false,
    contentType: false,
    data:form_data,
    success:function(msg){
      if(msg == 1){
        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
        //                                  #We reload the table
        ajax_load(data, 'bio_transport_body.php', 'transport_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });
}

/* this is for deleting data */
transport_del.on('click', function(){
  $("#transport_body input:checkbox:checked").each(function() {
    var _id = $(this).attr('id');
    _id = _id.split('-');
    /* push the number only */
    del_arr.push(_id[1]);
  });
  var form_data = {
    data: "del_arr=" + del_arr.join(",")
  };
  $.ajax({
    url: "php-functions/fncCommon.php?action=delUpInArray",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        /* Clear our array */
        del_arr = [];
        show_alert("'<?php echo $phrases['deleted']?>'", "", 1);
        /* #We reload the table */
        ajax_load(data, 'bio_transport_body.php', 'transport_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
      transport_chk_all.prop('checked', false);
    }
  });
});

changeTransportRemarkId = function (transport_id) {
  transport_remark_id.val(transport_id);

  $.ajax({
    url:"php-functions/fncBioTransport.php?action=getBioDetails",
    type: "POST",
    dataType: "json",
    data: { "transport_id" : transport_id, "type" : "ajax" },
    success:function(data){
      $('#transport_remarks').val(data[0].remarks);
    }
  });
}


transport_remark_form.on('submit', function(e) {
  e.preventDefault();
  var form_data = transport_remark_form.serialize();

  $.ajax({
    url: "php-functions/fncBioTransport.php?action=addRemarks",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
        ajax_load(data, 'bio_transport_body.php', 'transport_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });

});

//Payload goes here

$("#transport_dbl_chk_form").on('submit', function(e){ //modify payload data using ajax
  e.preventDefault();

  $.ajax({
    url:"ajax/double_check/updatePayload.php",
    type: "POST",
    dataType: "json",
    data: $("#transport_dbl_chk_form").serialize(),
    success:function(data){ // data is the id of the row
      $("#transport_dbl_chk_modal ").modal("hide");
      getTransportDetails(data, event);
    }
  });
});

transportSetPayload = function(caller){ //Set the payload key to be edited
  $("#transport_dbl_chk_form")[0].reset();

  $("#transport_payload_key").val($(caller).data('key'));

  //for setting default comments
  $("#transport_comment").text($(caller).data('comments'));

  //for setting default value of checkbox
  if($(caller).data('is_checked') == 1){
    $("#transport_dbl_chk_pass").attr('checked', true);
    $("#transport_dbl_chk_nopass").attr('checked', false);
  }else if($(caller).data('is_checked') == -1)  {
    $("#transport_dbl_chk_nopass").attr('checked', true);
    $("#transport_dbl_chk_pass").attr('checked', false);
  }else{
    $("#transport_dbl_chk_nopass").attr('checked', false);
    $("#transport_dbl_chk_pass").attr('checked', false);
  }

}
// Payload
