var cmt_form = $('#cmt_form');
var cmt_add = $("#cmt_add");
var cmt_del = $("#cmt_del");
var cmt_chk_all = $("#cmt_chk_all");

/* this is for resetting the form everytime add is clicked*/
cmt_add.on('click', function() {
  cmt_form[0].reset();
});

/* this is the listener for our submit button from the modal*/
cmt_form.on('submit', function (e){
  e.preventDefault();
  add_cmt_info();
});

/* this is for our check all/ uncheck all function */
/* check_all and uncheck_all are found in initialize.js */
cmt_chk_all.change(function(){
  this.checked ? check_all(cmt_body) :  uncheck_all(cmt_body);
});

/* this is our function for inserting and uploading data */
function add_cmt_info(){
  var form_data = new FormData(cmt_form[0]);
  form_data.append('project_id', "<?php echo $_GET['project_id'] ?>");
  form_data.append('sub_dir', 'pre_comments');
  form_data.append('role_id', '<?php echo $_SESSION['role_id'] ?>');
  form_data.append('up_type', '3'); /* up_type 3 for P.R.E. comment */

  $.ajax({
    url: "php-functions/fncCommon.php?action=uploadFile",
    type:'POST',
    processData: false,
    contentType: false,
    data:form_data,
    success:function(msg){
      if(msg == 1){
        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
        //                                  #We reload the table
        ajax_load(data, 'cmt_body.php', 'cmt_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });
}

/* this is for deleting data */
cmt_del.on('click', function(){
  $("#cmt_body input:checkbox:checked").each(function() {
    var _id = $(this).attr('id');
    _id = _id.split('-');
    /* push the number only */
    del_arr.push(_id[1]);
  });
  var form_data = {
    data: "del_arr=" + del_arr.join(",")
  };
  $.ajax({
    url: "php-functions/fncCommon.php?action=delUpInArray",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        /* Clear our array */
        del_arr = [];
        show_alert("<?php echo $phrases['deleted']?>", "", 1);
        /* #We reload the table */
        ajax_load(data, 'cmt_body.php', 'cmt_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
      cmt_chk_all.prop('checked', false);
    }
  });
});
