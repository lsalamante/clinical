<?php
/*************************************************************************************************/
/*
Description: Dashboard for ALL
Ajax Files Dependents (update also these files if you update this PHP file):
*


Created by: Dante
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer: Vananana
Reason: Make dynamic for all use types
Date Updated (mm-dd-yyyy H:i:s): 11/19/2016 - 11/21/2016 OT
=======
Created by: Dante and Van

NOTES: backup : "4" => Comments offered by Peer Review Expert done, Viewable by Investigator
- Set TABLE: status COLUMN: is_rejected to "1" if rejected and return to designated status -
- STATUS LEGEND -
"status #" => What happened before the status, where to next
"0" => Stand by, used when rejected by TFO/PI, viewable by APPLICANT -- NOTE: not submitted, is_rejected = 1
"1" => Submitted by Applicant, Viewable by TFO
"2" => Approved by TFO, Viewable by Principal Investigator
"3" => Approved by Principal Investigator, Viewable by Peer Review Expert && investigator
"4" => Upload of documents version 2x done by Investigator, Viewable by Applicant
"5" => Approved by Applicant, Viewable by Investigator again
"6" => Upload consent Agreement done by Investgator, Viewable by TFO
"7" => Submit Project/Document to Ethics Committee SECRETARY (simple submit button) by TFO, Viewable by Ethics Committee SECRETARY
"8" => Viewable by Ethics Committee Member and Independent evaluation experts and can be submitted by either Ethics Committee Member or Independent evaluation experts
"9" => Ethics Committee Member just finished adding comments. Ethics Committee SECRETARY can now upload conference content AND consent
"10" => DONE with Uploading Conference Content & Consent by Chairman. Viewable by Investigator and CAN UPLOAD NEW VERSION
"11" => DONE with Uploading of new version by Investigator, Viewable by Ethics Committee Chairman. Pass or no pass the new version ?
"12" => PASSED by Ethics Committee Chairman , Passed to Ethics committee SECRETARY. Ethics committee secretary must upload approval to move to the next status
"13" => Investigator add project members and upload job spec, conference content, training consent,
also, volunteer reqruitment. drug management, bio sample management, stage summary, inspection???
*/

# BELOW needed for CURRENT PROGRESS COLUMN IN DASHBOARD, THIS HAS DUPLICATE IN ajax-load/timeline_load.php, in case of update in phrases, please update both
# Wat??? -Enzo

include('jp_library/jp_lib.php');
require("php-functions/fncApplicant.php");
require("php-functions/fncCommon.php");
require("php-functions/fncProjects.php");
require("php-functions/fncStatusLabel.php"); # must always be below fncCommon.php and fncProjects.php

if (!isset($_SESSION['user_id'])) {
  header("Location: " . "index.php");
  die();
}

if($_SESSION['user_type'] == 'A')
{
  $projects = getAllApplicantProjectsFilters(0);

  $get_trial_id = "trial_id=";
  $_trial_id = 1;
  $get_status_id = 'status_num=';
  $_status_id = 1;
}
else if(in_array($_SESSION['user_type'], ['BGC', 'RN', 'C', 'MC', 'SC'])  ){ #HACK: LOL
  header("Location: " . "project_information.php");
  die();
}
else
{
  if(!isset($_GET['f']))
  $_GET['f'] = '';

  switch($_GET['f']){
    case 'pending':
    $projects = getFilteredProjects(0)['pending'];
    break;
    case 'pass':
    $projects = getFilteredProjects(0)['pass'];
    break;
    case 'nopass':
    $projects = getFilteredProjects(0)['rejected'];
    break;
    case 'ethics':
    $projects = getFilteredProjects(0)['ethics'];
    break;
    default:
    $projects = getAllProjects(0);  # found in php-functions/fncProjects.php
    break;
  }
}
$tdash = new Clinical\Helpers\Translation($_SESSION['lang']);
?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>

<body class="bggray">
  <section id="container">
    <!--header start-->
    <header class="header white-bg">
      <?php
      if ($LEFT_SIDEBAR) {
        //echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
      }
      ?>
      <!--logo start-->
      <?php if ($LOGO) {
        include('logo.php');
      }
      ?>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <?php if ($NOTIFICATION) {
          include('notification.php');
        } ?>
        <!--  notification end -->
      </div>
      <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
      include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->
        <!-- bread crumbs start -->
        <!--<div class="row">
        <div class="col-lg-12">

        <ul class="breadcrumb">
        <li><i class="fa fa-home"></i> My projects</li>
      </ul>

    </div>
  </div> -->
  <!-- breadcrumbs end -->
  <div class="row">
    <?php if($_SESSION['user_type'] == 'A'): ?>
      <div class="col-lg-12 floating-btn">
        <div class="col-sm-1 fright">
          <span class="input-group-btn">
            <a id="_search" href="project_information.php?
            <?php
            echo (isset($_GET['squery'])) ? $_GET['squery'] . "&": '' ;
            echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ;
            echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ;
            ?>
            ">  <button class="btn btn-white" type="button"><i class="fa fa-search"></i></button> </a>
          </span>
        </div>
        <div class="col-sm-3 fright">
          <input type="text" id="_squery" class="form-control" placeholder="<?php echo $phrases['search'] ?>" value="<?php echo (isset($_GET['squery'])) ? $_GET['squery'] : '' ;?>">

          <!--<div class="row">
          <div class="col-lg-12">
          <div class="form-group" style="margin-top:15px">
          <label class="col-sm-2 control-label">Search</label>

        </div>
      </div>
    </div>-->
  </div>
</div>

<?php endif; ?>
<div class="col-lg-12">
  <section class="panel">
    <!--<header class="panel-heading">
    My projects
  </header>-->
  <?php if($_SESSION['user_type'] == 'A' ): ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="form-group project-filters-row">
          <label class="col-sm-2 control-label lightblue" style="width:7.6%"><?php echo $phrases['research_category'] ?></label>
          <div class="col-sm-10" style="width:90%">
            <div class="btn-row">
              <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-primary <?php echo (isset($_GET['trial_id'])) ? '' : 'btn-active' ; ?>" onclick="window.location.href='dashboard.php?<?php echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox" > <?php echo $phrases['all'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 1) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox" > <?php echo $phrases['stage_1'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 2) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['stage_2'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 3) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['stage_3'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 4) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['stage_4'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 5) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['be_trial'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 6) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['clinical_verifications'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 7) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['medical_equipment'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 8) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['food'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 9) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['other'] ?>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="form-group project-filters-row">
          <label class="col-sm-2 control-label lightblue" style="width:7.6%"><?php echo $phrases['state']; ?></label>
          <div class="col-sm-10" style="width:90%">
            <div class="btn-row">
              <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-primary <?php echo (isset($_GET['status_num'])) ? '' : 'btn-active' ; ?>" onclick="window.location.href='dashboard.php?<?php echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['all'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 1) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_status_id . 1 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['apply'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 2) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_status_id . 2 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['initiation'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 10) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_status_id . 10 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['ethics_pass'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 14) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_status_id . 14 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['trial_begin'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 14) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_status_id . 14 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['process'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == -1) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_status_id . -1 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['stop'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 15) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_status_id . 15 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['trial_finish'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 16) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_status_id . 16 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['inspection'] ?>
                </label>
                <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 17) echo 'btn-active'?>" onclick="window.location.href='dashboard.php?<?php echo $get_status_id . 17 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                  <input type="checkbox"> <?php echo $phrases['project_complete'] ?>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-12 floating-btn">
      <div class="col-sm-1 fright" style="margin-right:32px">
        <button type="button" class="btn btn-primary txtlrg" id="new_project"><?php echo $phrases['new_project'] ?></button>
      </div>
    </div>
  <?php elseif($_SESSION['is_professonial_group'] == '1' || $_SESSION['user_type'] != 'PRE'): ?>
    <br>
    <?php if($_SESSION['user_type'] != "RN" && $_SESSION['user_type'] != "C" && $_SESSION['user_type'] != "SC" && $_SESSION['user_type'] != 'DA'): ?>
    <div class="row project-status-row">
      <?php
      $count = getProjectStatusCount();
      # found in php-functions/fncProjects.php
      ?>
      <div class="col-lg-12">
        <div class="col-lg-2">
          <center>
            <div class="status-row-highlight" style="width: 100px;">
            <?php echo $phrases['audit_status']; ?>
            </div>
          </center>
        </div>
        <div class="col-lg-2">
          <a href="<?php echo $PAGE_NAME . "?f=pending"?>" >
          <center>
            <div class="<?php echo ($_GET['f'] == 'pending') ? 'status-row-highlight' : '' ;?>" style="width: 100px;"><?php echo $phrases['check_pending']; ?>&nbsp;&nbsp;<?php echo $count['pending']['total']; ?></div>
          </center>
        </a>
        </div>
        <?php if($_SESSION['user_type'] == 'TFO'): ?>
        <div class="col-lg-2">
          <a href="<?php echo $PAGE_NAME . "?f=ethics"?>" >
          <center>
            <div class="<?php echo ($_GET['f'] == 'ethics') ? 'status-row-highlight' : '' ;?>" style="width: 100px;"><?php echo $phrases['ethics_pending']; ?>&nbsp;&nbsp;<?php echo $count['ethics']['total']; ?></div>
          </center>
        </a>
        </div>
        <?php endif; ?>
        <div class="col-lg-2">
          <a href="<?php echo $PAGE_NAME . "?f=pass"?>" >
          <center>
            <div class="<?php echo ($_GET['f'] == 'pass') ? 'status-row-highlight' : '' ;?>" style="width: 100px;"><?php echo $phrases['pass']; ?>&nbsp;&nbsp;<?php echo $count['pass']['total']; ?></div>
          </center>
        </a>
        </div>
        <?php if($_SESSION['user_type'] != 'I' || $_SESSION['user_type'] != 'ECC'): ?>
        <div class="col-lg-2">
          <a href="<?php echo $PAGE_NAME . "?f=nopass"?>" >
          <center>
            <div class="<?php echo ($_GET['f'] == 'nopass') ? 'status-row-highlight' : '' ;?>" style="width: 100px;"><?php echo $phrases['no_pass']; ?>&nbsp;&nbsp;<?php echo $count['rejected']['total']; ?></div>
          </center>
        </a>
        </div>
        <?php endif; ?>
      </div>
    </div>
  <?php endif; ?>
          <?php else: # Peer Review Expert User ?>
            <br>
            <?php $count = getProjectStatusCount(); # found in php-functions/fncProjects.php ?>
            <div class="row">
              <div class="col-lg-6">
                <?php if(count($projects) > 0):   #if there are no projects  ?>
                <div class="col-lg-2">
                  <center>
                    <?php echo $phrases['to_do']; ?>&nbsp;&nbsp;<?php echo $count['pending']['total']; ?></center>
                  </div>
                  <div class="col-lg-2">
                    <center>
                      <?php echo $phrases['done']; ?>&nbsp;&nbsp;<?php echo $count['pass']['total']; ?></center>
                    </div>
                  <?php endif; ?>
                  </div>
                </div>
              <?php endif; # ENDIF for $_SESSION['user_type'] == 'A' ?>
              <?php if($_SESSION['user_type'] == 'A'): ?>
                <div class="row" class="bordtopbot">
                  <div class="col-lg-12 tblpad">
                    <table class="table table-hover <?php echo ($projects->num_rows > 0) ? " " : "hidden "; ?>">
                      <thead>
                        <tr>
                          <th><?php echo $phrases['number'] ?></th>
                          <th><?php echo $phrases['project_number'] ?></th>
                          <th><?php echo $phrases['project_name'] ?></th>
                          <th><?php echo $phrases['applicant'] ?></th>
                          <th><?php echo $phrases['pi'] ?></th>
                          <th>CRO</th>
                          <th colspan="2"><?php echo $phrases['current_progress'] ?></th>
                          <th><?php echo $phrases['operation'] ?></th>
                        </tr>
                      </thead>
                      <tbody>

                        <?php foreach($projects as $project){ ?>
                          <tr class="clickable-row" onclick="ClickableRow('<?php echo $project['status_num'] < 13 ? "new_project_draft.php" : "pg_audit.php"; ?>?project_id=<?=$project['project_id']?>', event);" >
                            <td>
                              <?php echo sprintf('%04d', $project['project_id']); ?>
                            </td>
                            <td>
                              <?=$project['project_num']?>
                            </td>
                            <td>
                              <?=$project['project_name']?>
                            </td>
                            <td>
                              <?php
                              $dproj = new Clinical\Helpers\Project($project['project_id']);
                              echo $dproj->project_owner;
                               ?>
                            </td>
                            <td>
                              <?php
                              $pi = getUserById(0,$project['pi_id']);
                              echo $pi['fname'] . " " . $pi['lname'];
                              ?>
                            </td>
                            <td>
                              <?=$project['cro_name']?>
                            </td>
                            <td colspan="2">
                              <?php
                              if($project['status_id'] != null)
                              {
                                if($project['status_num'] != 8 && $project['status_num'] != 4)
                                {
                                  echo $project['is_rejected'] != "1" ? fncApproved($project['status_num']) : "<span style='color:red'>".fncRejected($project['status_num'], $project['reviewer_id'])."</span>";
                                }
                                elseif($project['status_num'] == 4)
                                {
                                  echo $project['is_rejected'] != "1" ? "<span style='color:#64aaf9'>".fncApproved($project['status_num'])."</span>" : "<span style='color:red'>".fncRejected($project['status_num'], $project['reviewer_id'])."</span>";
                                }
                                else
                                {
                                  $forStatus8 = getUserById(false, $project['reviewer_id'], true);
                                  if($forStatus8['acronym'] == 'A')
                                  {
                                    echo fncApproved("7.1");
                                  }
                                  else
                                  {
                                    echo $project['is_rejected'] != "1" ? fncApproved($project['status_num']) : "<span style='color:red'>".fncRejected($project['status_num'], $project['reviewer_id'])."</span>";
                                  }
                                }
                              }
                              else
                              {
                                if($_SESSION['lang'] != "english")
                                {
                                  echo "草稿";
                                }
                                else
                                {
                                  echo "Saved as Draft";
                                }
                              }
                              ?>
                              <?= " " . $project['date_created'] ?>
                            </td>
                            <td style="width:15%">
                              <!--  <a href="">
                              <button class="btn btn-primary" title="details"><i class="fa fa-book"></i></button>
                            </a> -->
                            <!--<button class="btn btn-danger del-btn" title="details" id="del-<?=$project['project_id']?>"><i class="fa fa-trash-o"></i></button>-->
                            <?php if($project['status_id'] != null): ?>
                              <button class="btn btn-success timeline-btn dashboard-operation" title="Timeline" id="timeline-<?php echo $project['project_id']; ?>"><?php echo $phrases['progress_icon']; ?></button>
                            <?php endif; ?>
                            <?php if (!in_array($project['status_num'], [-1,null,17])): ?>
                              <button class="btn btn-success stop-btn dashboard-operation" title="Stop" id="stop-<?php echo $project['project_id']; ?>"><?php echo $phrases['stop_button']; ?></button>
                            <?php endif; ?>
                          </td>
                        </tr>

                        <?php } ?>

                      </tbody>
                    </table>
                  </div>

                  <div class="col-lg-12 <?php echo ($projects->num_rows <= 0) ? " " : "hidden "; ?>">
                    <section class="panel">
                      <div class="row">

                        <div class="col-lg-12 text-center">
                          <img src="img/sad.png" class="sad">
                          <h1 class="sad"><?php echo $phrases['empty_table_data']; ?></h1>
                        </div>
                      </div>
                    </div>
                  </section>
                <?php   else: ?>
                  <div class="row" class="bordtopbot">
                    <div class="col-lg-12 tblpad">
                      <table class="table table-hover <?php echo (count($projects) > 0) ? " " : "hidden "; ?>">
                        <thead>
                          <tr>
                            <th><?php echo $phrases['number'] ?></th>
                            <th><?php echo $phrases['project_name'] ?></th>
                            <th><?php echo $phrases['date_created'] ?></th>
                            <th><?php echo $phrases['applicant'] ?></th>
                            <th><?php echo $phrases['status'] ?></th>
                            <th><?php echo $phrases['operation'] ?></th>
                          </tr>
                        </thead>
                        <tbody>

                          <?php foreach($projects as $project){  ?>
                            <?php if($project['status_num'] >= 13 && $_SESSION['is_professonial_group']) {?>
                              <!-- this is the page where multiple users such as the professional group should go to sa tamang panahon-->
                              <tr class="clickable-row" onclick="ClickableRow('pg_audit.php?project_id=<?=$project['project_id']?>', event);" >
                                <?php }else{ ?>
                                  <tr class="clickable-row" onclick="ClickableRow('<?php echo strtolower($_SESSION['user_type']); ?>_audit.php?project_id=<?=$project['project_id']?>', event);" >
                                    <?php } ?>
                                    <td>
                                      <?php echo sprintf('%04d', $project['project_id']); ?>
                                    </td>
                                    <td>
                                      <?php echo $project['project_name']; ?>
                                    </td>
                                    <td>
                                      <?php echo $project['date_created']; ?>
                                    </td>
                                    <td>
                                      <?php
                                      $dproj = new Clinical\Helpers\Project($project['project_id']);
                                      echo $dproj->project_owner;
                                       ?>
                                    </td>
                                    <td>
                                      <b>
                                        <?php
                                        echo $tdash->tryTranslate("status_" . $project['status_num'] . "_" . $project['is_rejected']);
                                        ?>
                                      </b>
                                    </td>
                                    <td>
                                      <!--<button class="btn btn-danger del-btn" title="details" id="del-<?=$project['project_id']?>"><i class="fa fa-trash-o"></i></button>-->
                                      <button class="btn btn-success timeline-btn" title="Timeline" id="timeline-<?php echo $project['project_id']; ?>"><?php echo $phrases['progress_icon']; ?></button>
                                    </td>
                                  </tr>

                                  <?php } ?>

                                </tbody>
                              </table>
                            </div>
                          </div>

                          <div class="row <?php echo (count($projects) <= 0) ? " " : "hidden "; ?>">
                            <div class="col-lg-12">
                              <section class="panel">
                                <div class="row">

                                  <div class="col-lg-12 text-center">
                                    <img src="img/sad.png" class="sad">
                                    <h1 class="sad"><?php echo $phrases['empty_table_data']; ?></h1>
                                  </div>
                                </div>
                              </section>
                            </div>
                          </div>
                        <?php   endif; #ENDIF for $_SESSION['user_type'] == 'A' in table  ?>
                        <!--timeline start-->

                        <div class="row" id="timeline_row" style="display:none">
                        </div>
                        <!--timeline end-->
                      </section>
                    </div>



                  </section>
                </div>

              </div>

            </div>

            <!-- page end-->
          </section>
        </section>
        <div id="focus_me"></div>
        <!--main content end-->
        <!-- Right Slidebar start -->
        <?php
        if ($RIGHT_SIDEBAR) {
          include('right-sidebar.php');
        }
        ?>
        <!-- Right Slidebar end -->
        <!--footer start-->
        <?php include('footer.php'); ?>
        <!--footer end-->
      </section>
      <?php include('scripts.php'); ?>


      <script>
      function ClickableRow(strlink,event)
      {
        // alert(event.target.nodeName);
        if(event.target.nodeName == 'BUTTON' || event.target.nodeName == "I")
        {
          return;
        }
        else
        {
          location.assign(strlink);
        }
      }
      $(document).ready(function () {

        var new_project = $("#new_project");
        var del_btn = $(".del-btn");
        var timeline_btn = $(".timeline-btn");
        var stop_btn = $(".stop-btn");

        /* stuff for our search function */
        var _trial_id = "<?php  echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; ?>";
        var _status_num = "<?php echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; ?>";
        var _squery = $("#_squery");
        var _search = $("#_search");
        /* underscore prefix for non-clashing variables */

        _search.on('click', function(e){
          e.preventDefault();
          if(_squery.val() == ''){
            _squery = '';
          }else{
            _squery = "squery=" + _squery.val() + "&";
          }

          window.location.href="dashboard.php?" + _squery + _status_num + _trial_id;
        });

        stop_btn.on('click', function(e){
          e.preventDefault();
            var _id = $(this).attr('id');
            _id = _id.split('-');
            var project_id = _id[1];

          confirmationModal("<?php echo $phrases['stop_project']?>", "", "",
              function(){
                $.ajax({
                  url:"php-functions/fncProjects.php?action=stopProject",
                  type: "POST",
                  dataType: "json",
                  data: { "project_id" : project_id, 'type' : 'ajax' },
                  success:function(data){

                    if(data == 1){
                      show_alert("<?php echo $phrases['operation_successful']; ?>", 'dashboard.php', 1);
                    }
                    else{
                      show_alert("<?php echo $phrases['operation_failed']?>", '', 0);
                    }

                  }
                });
              },
              function(){
                // do stuff for Cancel btn
              }
          );
        });


        del_btn.on('click', function (e) {
          var del_id = this.id;
          var del_arr = del_id.split("-");

          var form_data = {
            project_id: del_arr[1]
          };

          $.ajax({
            url: "php-functions/fncApplicant.php?action=deleteProjectById",
            type: "POST",
            data: form_data,
            success: function (msg) {

              show_alert("Project deleted", "dashboard.php", 1);

            }
          });
        });
        var last_timeline = "";
        timeline_btn.on('click', function (e){
          var timeline_id = this.id;
          if(last_timeline == timeline_id)
          {
            $('#timeline_row').fadeOut(1000);
            last_timeline = "none";
          }
          else
          {
            $('#timeline_row').fadeOut(100);
            var timeline_arr = timeline_id.split("-");
            $('#'+timeline_id).attr("data-active", "1");

            var form_data = {
              project_id: timeline_arr[1]
            };
            ajax_load(form_data, "timeline_load.php", "timeline_row"); // PS. ajax_load(form_data, file_url, element_id) in footer.php
            last_timeline = timeline_id;
          }
        });


        new_project.on('click', function (e) {
          e.preventDefault();

          var form_data = {
            user_id: <?= $_SESSION["user_id"]; ?>
          };

          $.ajax({
            url: "php-functions/fncCommon.php?action=addNewProject",
            type: "POST",
            data: form_data,
            success: function (msg) {

              window.location.replace("new_project_draft.php?project_id=" + msg);

            }
          });

        });

      });
      </script>
      </body>

      </html>
