<?php
namespace Clinical\Helpers;
$t = new Translation($_SESSION['lang']);
$d = new Databank($_GET['project_id']);
$d->fetch_databank();
?>

<table class="table table-hover">
  <thead>
    <tr>
      <th><?php echo $t->tryTranslate("number"); ?></th>
      <th><?php echo $t->tryTranslate("module"); ?></th>
      <th><?php echo $t->tryTranslate("title"); ?></th>
      <th><?php echo $t->tryTranslate("version"); ?></th>
      <th><?php echo $t->tryTranslate("upload_time"); ?></th>
      <th><?php echo $t->tryTranslate("uploader"); ?></th>
      <th><?php echo $t->tryTranslate("description"); ?></th>
      <th><?php echo $t->tryTranslate("action"); ?></th>
      <!-- <th><?php echo $t->tryTranslate("role"); ?></th>
      <th><?php echo $t->tryTranslate("action"); ?></th> -->
    </tr>
  </thead>
  <tbody>
    <?php
    $module_uploads = array(
     "1" => $t->tryTranslate('project_documents'),
     "2" => $t->tryTranslate('project_plan'),
     "3" => $t->tryTranslate('pre_opinion'),
     "5" => $t->tryTranslate('job_spec'),
     "7" => $t->tryTranslate('ecm_opinion')
   );
    $ctr_data = 1;
    foreach ($d->data as $databank) { ?>
    <tr>
      <td><?php echo $ctr_data; ?></td>
      <td><?php echo ctype_digit(strval($databank['module'])) == true ? $module_uploads[$databank['module']] : $databank['module']; ?>
      <td><?php echo $databank['title']; ?></td>
      <td><?php echo $databank['version']; ?></td>
      <td><?php echo $databank['date_uploaded']; ?></td>
      <td><?php echo $d->fetch_uploader($databank['uploader']); ?></td>
      <td><?php echo $databank['description']; ?>
      <td><a href="<?php echo $databank['up_path']; ?>" target="_blank"><?php echo $t->tryTranslate("view_attachment"); ?></a></td>
    </tr>
    <?php
    $ctr_data++;
    }
     ?>
  </tbody>
</table>
