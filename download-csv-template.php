<?php
namespace Clinical\Helpers;
include('jp_library/jp_lib.php');
$t = new Translation($_SESSION['lang']);

$columns = [
  // $t->tryTranslate("number"),
  // $t->tryTranslate("project_name"),
  $t->tryTranslate("name"),
  $t->tryTranslate('volunteer_type')." (1 = ".$t->tryTranslate('patient_volunteer').", 2 = ".$t->tryTranslate('healthy_volunteer').")",
  $t->tryTranslate("gender")." (Male/Female only)",
  $t->tryTranslate("birth_date")." (mm/dd/yyyy)",
  $t->tryTranslate("nation"),
  $t->tryTranslate("native_place"),
  $t->tryTranslate("married")." (Yes/No Only)",
  $t->tryTranslate("blood_type"),
  $t->tryTranslate("had_surgery")." (Yes/No Only)",
  $t->tryTranslate("family_history"),
  $t->tryTranslate("heavy_smoking")." (Yes/No Only)",
  $t->tryTranslate("heavy_drinking")." (Yes/No Only)",
  $t->tryTranslate("allergies"),
  $t->tryTranslate("height"),
  $t->tryTranslate("weight"),
  $t->tryTranslate("bmi"),
  $t->tryTranslate("medical_history"),
  $t->tryTranslate("mobile"),
  // $t->tryTranslate("id_card_num"),
  // $t->tryTranslate("id_card_copy")
  // $t->tryTranslate("case_num"),
  // $t->tryTranslate("subjects_num"),
  // $t->tryTranslate("abbrv"),
  // $t->tryTranslate("country"),
  // $t->tryTranslate("address"),
  // $t->tryTranslate("principal_investigator"),
  // $t->tryTranslate("random_num"),
  // $t->tryTranslate("random_date"),
  // $t->tryTranslate("sign_informed_consent"),
  // $t->tryTranslate("sign_date")
];
$csv = new CSV($columns);
$csv->downloadVolunteerTemplate();

exit;


 ?>
