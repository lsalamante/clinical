/* for remarks */
var drug_remark_form = $("#drug_remark_form");
var drug_remark_id = $("#drug_remark_id");
var drug_remarks = $("#drug_remarks");
var drug_remark_submit = $("#drug_remark_submit");

changeDrugRemarkId = function (drug_record_id) {
  drug_remark_id.val(drug_record_id);
  $.ajax({
    url:"php-functions/fncDrugRecord.php?action=getDrugRemarks",
    type: "POST",
    data: { "drug_record_id" : drug_record_id, "type" : "ajax" },
    success:function(data){
      var new_data = JSON.parse(data, true);
      $('#drug_remarks').val(new_data[0].remarks);
    }
  });

}

drug_remark_form.on('submit', function(e) {
  e.preventDefault();
  var form_data = drug_remark_form.serialize();

  $.ajax({
    url: "php-functions/fncDrugRecord.php?action=addDrugRemarks",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });

});
