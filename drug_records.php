   <!-- Add Side Effect per Drug per Project @Rai-->

<?php require("php-functions/fncDrugs.php"); ?>
<?php require("php-functions/fncDrugRecord.php"); ?>


<?php if(isset($_GET["project_id"]) && $_GET["project_id"] != "" && $_SESSION["user_type"] == "MC"){ #ALLOW DA ?>

  <a href="#" data-toggle="modal" class="btn btn-primary adder" id="addDrugRecord"><?php echo $phrases['add_record']?></a>

  <?php } ?>

<?php if(isset($_GET["project_id"]) && $_GET["project_id"] != "" && $_SESSION["user_type"] == "MC" || $_SESSION["user_type"] == "DA"){ #ALLOW DA ?>

<a href="#" data-toggle="modal" class="btn btn-primary adder" id="drugListAdd"><?php echo $phrases['drug_list']?></a>
<?php } ?>

  <!-- </style> -->

  <?php

  $project_id = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? $_GET["project_id"] : 0;
  $drugRecord = getDrugRecords('list', $project_id);

  $count = 0;

  if(count($drugRecord) > 0 && $project_id != 0){ // check if found record/s and if id is provided.

    ?>

    <table class="table table-hover">
      <thead>
        <tr>
          <th><?php echo $phrases['number']?></th>
          <th><?php echo $phrases['date_created']?></th>
          <th><?php echo $phrases['the_drug_name']?></th>
          <th><?php echo $phrases['record_type']?></th>
          <th><?php echo $t->tryTranslate('reason_drug'); ?></th>
          <th><?php echo $phrases['quantity']?></th>
          <!-- <th><?php echo $t->tryTranslate("storage"); ?></th> -->
          <!-- <th><?php //echo $t->tryTranslate('unit'); ?></th> -->
          <th><?php echo $phrases['operation']?></th>
        </tr>
      </thead>
      <tbody id="pp_body">

        <?php

        $count = 0;

        foreach ($drugRecord as $record) {

          $count++;
          ?>

          <tr onclick="getDrugRecordDetails(<?php echo $record['drug_record_id']; ?>); return false;" style="cursor:pointer;">
            <td><?php echo $count; ?></td>
            <td><?php echo $record["date"]; ?></td>
            <td><?php echo $record["trade_name"]; ?></td>
            <td><?php echo $phrases[$record["record_type"]]; ?></td>
            <td><?php echo $record["description"]; ?></td>
            <td><?php echo $record["quantity"]; ?></td>
            <!-- <td><?php echo $record["storage"]; ?></td> -->
            <!-- <td><?php// echo $record["unit"]; ?></td> -->
            <td onclick="event.cancelBubble = true;">
              <!-- FIXME -->
              <a  class="btn btn-xs btn-primary" title="add remarks" onclick="$('#drug_remark_modal').modal('toggle');
              $('#drug_remark_modal').modal('show'); changeDrugRemarkId(<?php echo $record['drug_record_id']; ?>)" ><?php echo $phrases['remarks']; ?></a>
            </td>
          </tr>

          <?php } ?>

        </tbody>
      </table>


      <?php }else{ // no record/s found ?>

        <div class="col-lg-12 text-center">
          <img src="img/sad.png" class="sad">
          <h1 class="sad"><?php echo $phrases['empty_table_data']; ?></h1>
        </div>

        <?php } ?>


        <!-- Start of Drug List Modal  @Rai-->

        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="drugListModal" class="modal fade " data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title" id="modalTitleList"><?php echo $phrases['drug_list'] ?></h4>
              </div>
              <div class="modal-body">
                <a href="#" data-toggle="modal" class="btn btn-primary adder <?= ($_SESSION['user_type'] == 'MC') ? '' : 'hidden' ; ?>" id="addDrug" onclick="rmDoubleCheck();" ><?php echo $phrases['add']?></a>
                <h4><center id="modalMsg"></center></h4><br>
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th><?php echo $phrases['number']?></th>
                      <th><?php echo $phrases['name']?></th>
                      <th><?php echo $phrases['type']?></th>
                      <th><?php echo $phrases['storage']?></th>
                      <th><?php echo $phrases['specification']?></th>
                      <th><?php echo $phrases['term_validity']?></th>
                    </tr>
                  </thead>
                  <tbody id="drugList"></tbody>
                </table>

              </div>
            </div>
          </div>
        </div>

        <!-- End of Drug List Modal  @Rai-->

        <!-- Start of Add Drug Modal  @Rai-->

        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addDrugModal" class="modal fade " data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title" id="modalTitleAddDrug"><?php echo $phrases['add_drug']?></h4>
              </div>
              <div class="modal-body">

                <form action="#" id="addDrugForm" enctype="multipart/form-data">

                  <p><?php echo $phrases['drug_name']?>:</p>
                  <div class="form-group" id="trade_name_div">
                    <label for="exampleInputEmail1"><?php echo $phrases['trade_name']?></label>
                    <input type="text" class="form-control" id="trade_name" name="trade_name" placeholder="">
                    <p class="help-block" style="display:none;" id="trade_name_msg"></p>
                  </div>

                  <div class="form-group" id="chemical_name_div">
                    <label for="exampleInputEmail1"><?php echo $phrases['chemical_name']?></label>
                    <input type="text" class="form-control" id="chemical_name" name="chemical_name" placeholder="">
                    <p class="help-block" style="display:none;" id="chemical_name_msg"></p>
                  </div>

                  <div class="form-group" id="english_name_div">
                    <label for="exampleInputEmail1"><?php echo $phrases['english_name']?></label>
                    <input type="text" class="form-control" id="english_name" name="english_name" placeholder="">
                    <p class="help-block" style="display:none;" id="english_name_msg"></p>
                  </div>
                  <hr>


                  <div class="form-group" id="drug_type_div">
                    <label for="exampleInputEmail1"><?php echo $phrases['type']?></label>
                    <select name="drug_type" id="drug_type" class="form-control">
                      <!--  REVIEW/TODO: should this be translated too? -->
                      <option value=""><?php echo $phrases['select']?></option>
                      <option value="tested_drug"><?php echo $phrases['tested_drug']?></option>
                      <option value="drug_combination"><?php echo $phrases['drug_combination']?></option>
                      <!-- <option value="Placebo"><?php echo $phrases['placebo']?></option> -->
                      <option value="control_drug"><?php echo $phrases['control_drug']?></option>
                      <option value="placebo_drug_record"><?php echo $phrases['placebo_drug_record']?></option>
                      <option value="adjuvant_drug"><?php echo $phrases['adjuvant_drug']?></option>
                      <option value="other"><?php echo $phrases['other']?></option>
                    </select>
                    <p class="help-block" style="display:none;" id="drug_type_msg"></p>
                  </div>

                  <div class="form-group" id="lot_number_div">
                    <label for="exampleInputEmail1"><?php echo $phrases['drug_production_batch_number']?></label>
                    <input type="text" class="form-control" id="lot_number" name="lot_number" placeholder="">
                    <p class="help-block" style="display:none;" id="lot_number_msg"></p>
                  </div>

                  <div class="form-group" id="specification_div">
                    <label for="exampleInputEmail1"><?php echo $phrases['specification']?></label>
                    <input type="text" class="form-control" id="specification" name="specification" placeholder="">
                    <p class="help-block" style="display:none;" id="specification_msg"></p>
                  </div>

                  <div class="form-group" id="validity_div">
                    <label for="exampleInputEmail1"><?php echo $phrases['term_validity']?></label>
                    <div class="row">
                      <div class="col-lg-5">
                        <div class="iconic-input right">
                          <i class="fa fa-calendar"></i>
                          <input type="text" class="form-control" id="validity_from" name="validity_from" placeholder="">
                        </div>
                      </div>
                      <div class="col-lg-1">
                        <center><?php echo $phrases['to_date']; ?></center>
                      </div>
                      <div class="col-lg-5">
                        <div class="iconic-input right">
                          <i class="fa fa-calendar"></i>
                          <input type="text" class="form-control" id="validity_to" name="validity_to" placeholder="">
                        </div>
                      </div>
                    </div>

                    <p class="help-block" style="display:none;" id="validity_msg"></p>
                  </div>

                  <div class="form-group" id="inspection_div">
                    <label for="exampleInputEmail1"><?php echo $phrases['inspection_report']?></label>
                    <input type="file" class="form-control" id="inspection" name="inspection" placeholder="" style="margin-bottom:20px">
                    <a href="" class="btn btn-sm" style="display:none;" id="inspection_upload" target="_blank"><?php echo $phrases['view_attachments']; ?></a>
                    <p class="help-block" style="display:none;" id="inspection_msg"></p>
                  </div>

                  <!-- Databank -->
                  <div class="form-group">
                    <input type="checkbox" name="databank" value="1" id="drug_databank">
                    <label for="drug_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
                  </div>
                  <!-- Databank -->

                  <div class="form-group" id="factory_div">
                    <label for="exampleInputEmail1"><?php echo $phrases['pharmaceutical_factory']?></label>
                    <input type="text" class="form-control" id="factory" name="factory" placeholder="">
                    <p class="help-block" style="display:none;" id="factory_msg"></p>
                  </div>

                  <div class="form-group" id="provider_div">
                    <label for="exampleInputEmail1"><?php echo $phrases['provider']?></label>
                    <input type="text" class="form-control" id="provider" name="provider" placeholder="">
                    <p class="help-block" style="display:none;" id="provider_msg"></p>
                  </div>

                  <!-- Start hidden fields @Rai-->

                  <input type="hidden" name="drug_id" id="drug_id" value="">
                  <input type="hidden" name="project_idR" id="project_idR" value="<?php echo $_GET['project_id']; ?>">

                  <!-- End hidden fields @Rai-->

                  <?php if(isset($_GET["project_id"]) && $_GET["project_id"] != "" && $_SESSION["user_type"] == "MC"){ ?>
                    <button type="submit" class="btn btn-primary" id="submitDrug"><?php echo $phrases['submit']?></button>
                    <?php } ?>

                    <!-- BUTTON FOR NOTIFY FROM DA -->
                    <?php if($_SESSION["user_type"] == "DA" ): ?>
                      <button class="btn btn-primary" type="button" onclick="notifyOwner('<?php echo $_GET['project_id']?>', 'MC', 'drugs')">
                        <?php echo $t->tryTranslate('notify')?>
                      </button>
                    <?php endif; ?>
                    <!-- / BUTTON FOR NOTIFY FROM DA -->

                  </form>

                </div>
              </div>
            </div>
          </div>

          <!-- End of Add Drug Modal  @Rai-->


          <!-- Start of Add Record Modal  @Rai-->

          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addRecordModal" class="modal fade " data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title" id="modalTitleAddRecord"><?php echo $phrases['add_record']?></h4>
                </div>
                <div class="modal-body">

                  <form action="#" id="addDrugRecordForm" enctype="multipart/form-data" method="POST">

                    <div class="form-group" id="drug_idR_div">
                      <label  for="inputSuccess"><?php echo $phrases['drug_name']?></label>

                      <div class="row">
                        <div class="col-lg-10">
                          <select name="drug_idR" id="drug_idR" class="form-control" >
                            <option value=""><?php echo $phrases['select']?></option>
                            <?php foreach (getDrugs("list", $_GET["project_id"]) as $drug) { ?>
                              <option value="<?php echo $drug["drug_id"]; ?>"><?php echo $drug["trade_name"]; ?></option>
                              <?php } ?>
                            </select>
                          </div>

                          <?php if(isset($_GET["project_id"]) && $_GET["project_id"] != "" && $_SESSION["user_type"] == "MC" ){ ?>
                            <div class="col-lg-2">
                              <a href="#" onclick="showAddNewDrug(); return false;" id="" ><?php echo $phrases['add_drug']?></a>
                            </div>
                            <?php } ?>

                          </div>

                          <p class="help-block" style="display:none;" id="drug_id_msg"></p>

                        </div>

                        <div class="form-group" id="quantity_div">
                          <label for="exampleInputEmail1"><?php echo $phrases['quantity']?></label>

                          <div class="row">
                            <div class="col-lg-6">
                              <input type="text" class="form-control" id="quantityR" name="quantity" placeholder="">
                            </div>
                            <div class="col-lg-6" style="margin-top: -23px;"> <!-- REVIEW -->
                              <label for="exampleInputEmail1"><?php echo $phrases['unit']?></label>
                                <input type="text" class="form-control" name="unit" id="unitR" placeholder="">
                            </div>
                          </div>
                          <p class="help-block" style="display:none;" id="quantity_msg"></p>
                        </div>

                        <div class="form-group" id="record_type_div">
                          <label for="exampleInputEmail1"><?php echo $phrases['record_type']?></label>
                          <select name="record_type" id="record_type" class="form-control">
                            <option value=""><?php echo $phrases['select']?></option>
                            <!-- <option value="choose"><?php echo $phrases['choose']?></option> # same as select??? - VAN -->
                            <option value="put_in_storage"><?php echo $phrases['put_in_storage']?></option>
                            <option value="put_out_storage"><?php echo $phrases['put_out_storage']?></option>
                            <option value="waste"><?php echo $phrases['waste']?></option>
                          </select>
                          <p class="help-block" style="display:none;" id="record_type_msg"></p>
                        </div>

                        <div class="form-group" id="date_div">
                          <label for="exampleInputEmail1"><?php echo $phrases['date']?></label>
                          <div class="iconic-input right">
                            <i class="fa fa-calendar"></i>
                            <input type="text" class="form-control" id="dateR" name="date" placeholder="">
                          </div>
                          <p class="help-block" style="display:none;" id="date_msg"></p>
                        </div>

                        <div class="form-group" id="actor1_div">
                          <label for="exampleInputEmail1"><?php echo $phrases['actor_user_one']?></label>
                          <input type="text" class="form-control" id="actor1" name="actor1" placeholder="">
                          <p class="help-block" style="display:none;" id="actor1_msg"></p>
                        </div>

                        <div class="form-group" id="position1_div">
                          <label for="exampleInputEmail1"><?php echo $phrases['identity']?></label> <!-- NOTE: before this was `position` but the translation provided is `identity`-->
                          <select name="position1" id="position1" class="form-control">
                            <option value=""><?php echo $phrases['select']?></option>
                            <option value="Applicant"><?php echo $phrases['applicant']?></option>
                            <option value="CRO">CRO</option>
                            <option value="Medicine controller"><?php echo $phrases['medicine_controller']?></option>
                            <option value="Research nurse"><?php echo $phrases['research_nurse']?></option>
                          </select>
                          <p class="help-block" style="display:none;" id="position1_msg"></p>
                        </div>

                        <div class="form-group" id="actor2_div">
                          <label for="exampleInputEmail1"><?php echo $phrases['actor_user_two']?></label>
                          <input type="text" class="form-control" id="actor2" name="actor2" placeholder="">
                          <p class="help-block" style="display:none;" id="actor2_msg"></p>
                        </div>

                        <div class="form-group" id="position2_div">
                          <label for="exampleInputEmail1"><?php echo $phrases['identity']?></label> <!-- NOTE: before this was `position` but the translation provided is `identity`-->
                          <select name="position2" id="position2" class="form-control">
                            <option value=""><?php echo $phrases['select']?></option>
                            <option value="Applicant"><?php echo $phrases['applicant']?></option>
                            <option value="CRO">CRO</option>
                            <option value="Medicine controller"><?php echo $phrases['medicine_controller']?></option>
                            <option value="Research nurse"><?php echo $phrases['research_nurse']?></option>
                          </select>
                          <p class="help-block" style="display:none;" id="position2_msg"></p>
                        </div>

                        <div class="form-group" id="receipt_div">
                          <label for="exampleInputEmail1"><?php echo $phrases['delivery_receipt']?></label>
                          <input type="file" class="form-control" id="receiptR" name="receipt[]" placeholder="" multiple  accept=".doc, .docx, *pdf">
                          <p class="help-block" style="display:none;" id="receipt_msg"></p>
                          <br>
                          <ul style="display:none;" id="receiptList"> </ul>

                        </div>

                        <div class="form-group" id="description_div">
                          <label for="exampleInputEmail1"><?php echo $phrases['content']?></label>
                          <textarea name="description" id="descriptionR" class="form-control"></textarea>
                          <p class="help-block" style="display:none;" id="description_msg"></p>
                        </div>

                        <!-- Start hidden fields @Rai-->

                        <input type="hidden" name="drug_record_id" id="drug_record_id" value="">

                        <!-- End hidden fields @Rai-->
                        <?php if(isset($_GET["project_id"]) && $_GET["project_id"] != "" && $_SESSION["user_type"] == "MC" ){ ?>
                          <button type="submit" class="btn btn-primary" id="submitRecord"><?php echo $phrases['submit']?></button>
                          <?php } ?>

                          <!-- BUTTON FOR NOTIFY FROM DA -->
                          <?php if($_SESSION["user_type"] == "DA" ): ?>
                            <button class="btn btn-primary" type="button" onclick="notifyOwner('<?php echo $_GET['project_id']?>', 'MC', 'drug_record')">
                              <?php echo $t->tryTranslate('notify')?>
                            </button>
                          <?php endif; ?>
                          <!-- / BUTTON FOR NOTIFY FROM DA -->

                        </form>

                      </div>
                    </div>
                  </div>

                  <!-- End of Add Record Modal  @Rai-->


                  <!-- Start of holds what modal opened  @Rai-->

                  <input type="hidden" name="modalId" id="modalId" value="">
                  <input type="hidden" name="project_idR" id="project_idR" value="<?php echo $_GET["project_id"]; ?>">

                  <!-- End of holds what modal opened  @Rai-->

                </div>

                <!--  REMARK MODAL  -->
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="drug_remark_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                        <h4 class="modal-title"><?php echo $phrases['add_remarks'] ?></h4>
                      </div>
                      <div class="modal-body">
                        <form role="form" id="drug_remark_form">
                          <div class="form-group">
                            <label for="exampleInputEmail1"><?php echo $phrases['remarks'] ?></label>
                            <textarea class="form-control v-resize-only" id="drug_remarks" name="remarks" placeholder="" required></textarea>
                            <input type='hidden' name="drug_record_id" id="drug_remark_id" value="">
                          </div>
                          <button type="" class="btn btn-primary make-loading" id="drug_remark_submit" <?php if($_SESSION['user_type'] != 'MC') echo 'disabled'; ?>><?php echo $phrases['submit'] ?></button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- / REMARK MODAL  -->


                <!--  DOUBLE CHECK DRUGS  -->
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="drugs_dbl_chk_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                        <h4 class="modal-title"><?php echo $t->tryTranslate('add_double_check_opinion') ?></h4>
                      </div>
                      <div class="modal-body">
                        <form role="form" id="drugs_dbl_chk_form">
                          <div class="form-group">
                            <div class="form-group <?php echo ($_SESSION['user_type'] == 'DA') ? '' : 'hidden' ; ?>">
                              <div class="radio">
                                <input value="1" name="is_checked" id="drugs_dbl_chk_pass" type="radio" onClick="clearDblCheck('drugs_comment', this.value);"> <?php echo $t->tryTranslate('no_problem') ?> &nbsp;&nbsp;
                                <input value="-1" name="is_checked" id="drugs_dbl_chk_nopass" type="radio" onClick="clearDblCheck('drugs_comment', this.value);"> <?php echo $t->tryTranslate('have_problem') ?> </div>
                            </div>
                            <label for="exampleInputEmail1" class="dbl_chk_label"><?php echo $t->tryTranslate('comments') ?></label>
                            <textarea class="form-control v-resize-only" id="drugs_comment" name="comments" <?php echo ($_SESSION['user_type'] != 'DA') ? 'disabled' : '' ;?> placeholder=""></textarea>
                            <input type='hidden' name="bio_id" id="drugs_dbl_chk_id" value="">  <!-- unique id of PK-->
                            <input type='hidden' name="payload_key" id="drugs_payload_key" value="">  <!-- key of payload to modify -->
                            <input type='hidden' name="table" id="drugs_dbl_chk_tbl" value="drugs"> <!-- table name to modify -->
                            <input type='hidden' name="pk_col" id="drugs_pk_col" value="drug_id">  <!-- primary key column name of table-->
                          </div>
                          <?php if($_SESSION['user_type'] == 'DA'): ?>
                          <button type="" class="btn btn-primary make-loading" id="drugs_dbl_chk_submit"><?php echo $phrases['submit'] ?></button>
                        <?php endif; ?>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- / DOUBLE CHECK DRUGS  -->

  <!--  DOUBLE CHECK DRUG RECORDS -->
                 <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="drugrec_dbl_chk_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
                   <div class="modal-dialog">
                     <div class="modal-content">
                       <div class="modal-header">
                         <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                         <h4 class="modal-title"><?php echo $t->tryTranslate('add_double_check_opinion') ?></h4>
                       </div>
                       <div class="modal-body">
                         <form role="form" id="drugrec_dbl_chk_form">
                           <div class="form-group">
                             <div class="form-group <?php echo ($_SESSION['user_type'] == 'DA') ? '' : 'hidden' ; ?>">
                               <div class="radio">
                                 <input value="1" name="is_checked" id="drugrec_dbl_chk_pass" type="radio" onClick="clearDblCheck('drugrec_comment', this.value);"> <?php echo $t->tryTranslate('no_problem') ?> &nbsp;&nbsp;
                                 <input value="-1" name="is_checked" id="drugrec_dbl_chk_nopass" type="radio" onClick="clearDblCheck('drugrec_comment', this.value);"> <?php echo $t->tryTranslate('have_problem') ?> </div>
                             </div>
                             <label for="exampleInputEmail1" class="dbl_chk_label"><?php echo $t->tryTranslate('comments') ?></label>
                             <textarea class="form-control v-resize-only" id="drugrec_comment" name="comments" <?php echo ($_SESSION['user_type'] != 'DA') ? 'disabled' : '' ;?> placeholder=""></textarea>
                             <input type='hidden' name="bio_id" id="drugrec_dbl_chk_id" value="">  <!-- unique id of PK-->
                             <input type='hidden' name="payload_key" id="drugrec_payload_key" value="">  <!-- key of payload to modify -->
                             <input type='hidden' name="table" id="drugrec_dbl_chk_tbl" value="drug_records"> <!-- table name to modify -->
                             <input type='hidden' name="pk_col" id="drugrec_pk_col" value="drug_record_id">  <!-- primary key column name of table-->
                           </div>
                           <?php if($_SESSION['user_type'] == 'DA'):?>
                           <button type="" class="btn btn-primary make-loading" id="drugrec_dbl_chk_submit"><?php echo $phrases['submit'] ?></button>
                         <?php endif; ?>
                         </form>
                       </div>
                     </div>
                   </div>
                 </div>
                 <!-- / DOUBLE CHECK DRUG RECORDS  -->
