var ecm_form = $('#ecm_form');
var ecm_add = $("#ecm_add");
var ecm_del = $("#ecm_del");
var ecm_chk_all = $("#ecm_chk_all");

/* this is for resetting the form everytime add is clicked*/
ecm_add.on('click', function() {
  ecm_form[0].reset();
});

/* this is the listener for our submit button from the modal*/
ecm_form.on('submit', function (e){
  e.preventDefault();
  add_ecm_info();
});

/* this is for our check all/ uncheck all function */
/* check_all and uncheck_all are found in initialize.js */
ecm_chk_all.change(function(){
  this.checked ? check_all(ecm_body) :  uncheck_all(ecm_body);
});

/* this is our function for inserting and uploading data */
function add_ecm_info(){
  var form_data = new FormData(ecm_form[0]);
  form_data.append('project_id', "<?php echo $_GET['project_id'] ?>");
  form_data.append('sub_dir', 'ecm_comments');
  form_data.append('role_id', '<?php echo $_SESSION['role_id'] ?>');
  form_data.append('up_type', '7'); /* up_type 7 for E.C.M comment */

  $.ajax({
    url: "php-functions/fncCommon.php?action=uploadFile",
    type:'POST',
    processData: false,
    contentType: false,
    data:form_data,
    success:function(msg){
      if(msg == 1){
        if(status_num == 8){
            $('#status_submit').attr('disabled', false);
        }

        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
        //                                  #We reload the table
        ajax_load(data, 'ecm_body.php', 'ecm_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });
}

/* this is for deleting data */
ecm_del.on('click', function(){
  $("#ecm_body input:checkbox:checked").each(function() {
    var _id = $(this).attr('id');
    _id = _id.split('-');
    /* push the number only */
    del_arr.push(_id[1]);
  });
  var form_data = {
    data: "del_arr=" + del_arr.join(",")
  };
  $.ajax({
    url: "php-functions/fncCommon.php?action=delUpInArray",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        /* Clear our array */
        del_arr = [];
        show_alert("<?php echo $phrases['deleted']?>", "", 1);
        /* #We reload the table */
        ajax_load(data, 'ecm_body.php', 'ecm_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
      ecm_chk_all.prop('checked', false);
    }
  });
});
