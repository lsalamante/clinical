var eligible_form = $("#eligible_form");
var eligible_add = $("#eligible_add");
var eligible_chk_all = $("#eligible_chk_all");
var eligible_del = $("#eligible_del");
var eligible_file = $("#eligible_file");

ajax_load(data, 'eligible_body.php', 'eligible_body');

eligible_add.on('click', function() {
  eligible_form[0].reset();
});

eligible_chk_all.change(function(){
  this.checked ? check_all(eligible_body) :  uncheck_all(eligible_body);
});

eligible_form.on('submit', function (e){
  e.preventDefault();
  add_eligible_info();
});

eligible_del.on('click', function(){
  $("#eligible_body input:checkbox:checked").each(function() {
    var _id = $(this).attr('id');
    _id = _id.split('-');
    //                      #push the number only
    del_arr.push(_id[1]);
  });

  var form_data = {
    data: "del_arr=" + del_arr.join(",")
  };

  $.ajax({
    url: "php-functions/fncCommon.php?action=delUpInArray",
    type: "POST",
    data: form_data,
    success: function (msg) {

      if(msg == 1){
        //                                    # Clear our array
        del_arr = [];

        show_alert("Operation successful.", "", 1);

        //                                  #We reload the table
        ajax_load(data, 'eligible_body.php', 'eligible_body');
      }else{
        show_alert("Operation failed.", "", 0);

      }
      eligible_chk_all.prop('checked', false);
    }
  });
});

function add_eligible_info(){

  var form_data = new FormData(eligible_form[0]);
  form_data.append('project_id', "<?php echo $_GET['project_id'] ?>");
  form_data.append('sub_dir', 'eligible');
  form_data.append('role_id', '<?php echo $_SESSION['role_id'] ?>');
  form_data.append('up_type', '6'); /* NOTE: up_type 5 for eligible */

  $.ajax({
    url: "php-functions/fncCommon.php?action=uploadFile",
    type:'POST',
    processData: false,
    contentType: false,
    data:form_data,
    success:function(msg){
      if(msg == 1){
        show_alert("Operation successful.", "", 1);
        //                                  #We reload the table
        ajax_load(data, 'eligible_body.php', 'eligible_body');
      }else{
        show_alert("Operation failed.", "", 0);
      }
    }
  });
}
