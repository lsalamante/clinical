<?php
namespace Clinical\Helpers;
include('jp_library/jp_lib.php');
include('php-functions/fncSubjects.php');
$t = new Translation($_SESSION['lang']);
$p = new Project($_GET['project_id']);

$columns = [
  $t->tryTranslate("number"),
  $t->tryTranslate("project_name"),
  $t->tryTranslate("name"),
  $t->tryTranslate("gender"),
  $t->tryTranslate("birth_date"),
  $t->tryTranslate("nation"),
  $t->tryTranslate("native_place"),
  $t->tryTranslate("married"),
  $t->tryTranslate("blood_type"),
  $t->tryTranslate("had_surgery"),
  $t->tryTranslate("family_history"),
  $t->tryTranslate("heavy_smoking"),
  $t->tryTranslate("heavy_drinking"),
  $t->tryTranslate("allergies"),
  $t->tryTranslate("height"),
  $t->tryTranslate("weight"),
  $t->tryTranslate("bmi"),
  $t->tryTranslate("medical_history"),
  $t->tryTranslate("mobile"),
  $t->tryTranslate("id_card_num"),
  $t->tryTranslate("id_card_copy"),
  $t->tryTranslate("case_num"),
  $t->tryTranslate("subjects_num"),
  $t->tryTranslate("abbrv"),
  $t->tryTranslate("country"),
  $t->tryTranslate("address"),
  $t->tryTranslate("principal_investigator"),
  $t->tryTranslate("random_num"),
  $t->tryTranslate("random_date"),
  $t->tryTranslate("sign_informed_consent"),
  $t->tryTranslate("sign_date")
];
$csv = new CSV($columns);

$subjects = getSubjects($_GET['project_id'], null);

// $params['table'] = "volunteers, subjects";
// $params['where'] = "volunteers.volunteer_id = subjects.volunteer_id AND subjects.project_id = " . $_GET['project_id'];
// $result = jp_get($params);
$count = 1;

foreach ($subjects as $row) 
{
  $arr_data = array(
    $count++,
    $p->project_name,
    $row['v_name'],
    $row['gender'],
    $row['bday'],
    $row['nation'],
    $row['native'],
    $row['married'],
    $row['blood_type'],
    $row['surgery'],
    $row['fam_history'],
    $row['smoker'],
    $row['drinker'],
    $row['allergies'],
    $row['height'],
    $row['weight'],
    $row['bmi'],
    $row['med_history'],
    $row['mobile'],
    $row['id_card_num'],
    $row['id_card_path'],
    $row['case_num'],
    $row['subjects_num'],
    $row['abbrv'],
    $row['country'],
    $row['address'],
    $row['pi'],
    $row['random_num'],
    $row['random_date'],
    $row['is_informed_consent'],
    $row['sign_date']
  );

  $csv->addRow($arr_data);
}


// while ($row = mysqli_fetch_array($result)) {

  

// }

$csv->export();

exit;
