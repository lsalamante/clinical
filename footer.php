
<div class="modal fade" id="alert_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
      <div class="modal-content pad30">
          <div class="modal-header nobg nopad">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="alert-modal-title"><?php echo $phrases['system_message']; ?></h4>
          </div>
          <div class="modal-body nopadR nopadL">
            <ul class="pop-ul">
              <h4><center id="alert_message"></center></h4>
              <input id="alert_redirect_value" hidden>
            </ul>
          </div>
          <!--<div class="result" style=" padding: 10px 0 10px 0; text-align: center; font-weight: bold;"></div>-->
          <div class="modal-footer txtcenter nopad">
            <button class="btn btn-success" data-dismiss="modal" id="declare_btn" type="button" onclick="reload_page();"><?php echo $phrases['ok']; ?></button>
          </div>
      </div>
  </div>
</div>

<!-- Start of confirmation modal  -->

<div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
      <div class="modal-content pad30">
          <div class="modal-header nobg nopad">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="alert-modal-title"><?php echo $phrases['confirmation']; ?></h4>
          </div>
          <div class="modal-body nopadR nopadL">
            <ul class="pop-ul">
              <h4><center id="confirmationMessage"></center></h4>
              <input id="redirectValue" hidden>
            </ul>
          </div>
          <!--<div class="result" style=" padding: 10px 0 10px 0; text-align: center; font-weight: bold;"></div>-->
          <div class="modal-footer txtcenter nopad">
            <button class="btn btn-success" data-dismiss="modal" id="cancelBtn" type="button"><?php echo $phrases['cancel']; ?></button>
            <button class="btn btn-success" data-dismiss="modal" id="agreedBtn" type="button" style="margin-bottom: 0px;"><?php echo $phrases['ok']; ?></button>
          </div>
      </div>
  </div>
</div>

<!-- End of confirmation modal  -->


<footer class="site-footer">
    <div class="text-center"> <!--2016 &copy;
        <?php echo $SITE_NAME ?>
        <a href="#" class="go-top"> <i class="fa fa-angle-up"></i> </a>-->
        <img src="images/footerlogo.png"> 广州华启医信互联网公司提供技术支持
    </div>
</footer>
</section> <!-- close section in opening section #container -->
<script>


/*************************************************************************************************/
/*
Description: dynamically reloading the page, uses by show_alert()
Purpose: for alerts
Parameters: none
Created by: Vanananana
*/

/* If Updated by Others than the Creator
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s) :
*/

function reload_page(){
	var redirect = $('#alert_redirect_value').val();
	if(redirect != ''){
	  location.href = redirect;
	}
}
/*************************************************************************************************/
/*
Description: #NoMoreDefaultJavaAlertPlease
Purpose: for modal alerts/system messages
Parameters:
	- msg => System Message
	- redirect => .php file link to redirect
	- type => 1 (positive look, green text) and 0 (negative look, red look)
Created by: Vanananana
*/

/* If Updated by Others than the Creator
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s) :
*/

function show_alert(msg, redirect, type){

    $(".modal").modal("hide");
    //type = 1 -> success 0 -> error
    if(redirect != ''){
      $('#alert_redirect_value').val(redirect);
    }
    $('#alert_message').html(msg);
    $('#alert_message').css({color:'rgba(41,160,50,1) '});
    if(type == 0){
      $('#alert_message').css({color:'red '});
    }
    $("#alert_modal").modal('show');

}
/*************************************************************************************************/
/*
Description: uses ajax, compatible with all browsers
Purpose: loads html elements contents using ajax
Parameters:
	- form_data => converted to $_GET variables for php handling
	- file_url => path to ajax-load default, includes php handled data of element contents
	- element_id => element id contents that is handled/loaded
Created by: Vanananana
*/

/* If Updated by Others than the Creator
Updated by Developer: Enzowwwwww
Reason: Make ajax_load generic for end-users, not only for admin
Date Updated" (11-17-2016 21:26:58)
*/

function ajax_load(form_data, file_url, element_id)
{
    var final_url = "ajax-load/"+file_url+"?";

    if(!form_data){
//       #if form data is empty?!!?!
    }else{
        $.each(form_data, function( key, value ){
            final_url += key+"="+value+"&";
        });
    }

	var xmlhttp;
	if (window.XMLHttpRequest){
	  // code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}else{
	  // code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState==4 && xmlhttp.status==200){
	    document.getElementById(element_id).innerHTML=xmlhttp.responseText;
//      CALLBACKS
        if(element_id == 'timeline_row')
        {
          $('#timeline_row').fadeIn(400);
          //$("html, body").animate({ scrollTop: $(document).height() }, 1000);
          //$("html, body").animate({ scrollTop: $("#timeline_row").scrollTop() }, 1000);
          $("html, body").animate({ scrollTop: 750 }, 1000);
        }

       if(element_id == 'cmt_body'){
           cmt_count = $("#cmt_body .cmt_row").length;

           if(cmt_count > 0){
               $("#status_submit").prop('disabled', false);
           }
       }
        if(element_id == 'i_body'){
           i_count = $("#i_body .i_row").length;

           if(i_count > 0){
               $("#status_submit").prop('disabled', false);
           }
       }

       if(element_id == 'multi_select_div'){
          $('#my_multi_select3').multiSelect({
          selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='搜索'>",
          selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='搜索'>",
          afterInit: function (ms) {
              var that = this,
                  $selectableSearch = that.$selectableUl.prev(),
                  $selectionSearch = that.$selectionUl.prev(),
                  selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                  selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

              that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                  .on('keydown', function (e) {
                      if (e.which === 40) {
                          that.$selectableUl.focus();
                          return false;
                      }
                  });

              that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                  .on('keydown', function (e) {
                      if (e.which == 40) {
                          that.$selectionUl.focus();
                          return false;
                      }
                  });
          },
          afterSelect: function () {
              this.qs1.cache();
              this.qs2.cache();
          },
          afterDeselect: function () {
              this.qs1.cache();
              this.qs2.cache();
          }
      });
       }
	  }
	}
	xmlhttp.open("GET",final_url,true);
	xmlhttp.send();
}


function make_loading(element){
    element.attr('disabled', 'disabled');
    element.html("载入中...");
}

/*************************************************************************************************/
/*
Description: #NoMoreDefaultJavaAlertPlease
Purpose: Confirmation Modal
Parameters:
  - action => verb ex: delete, approve, remove
  - subject => noun ex: user, volunteer, project
  - additionalMsg => for additional msg
  - trueCallBack => function for Ok btn
  - falseCallBack => function for Cancel btn

How to use:

  confirmationModal("approve", "volunteer", "",
      function(){
        // do stuff for Ok btn
      },
      function(){
        // do stuff for Cancel btn
      }
  );

Created by: @Rai
*/

/* If Updated by Others than the Creator
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s) :
*/


function confirmationModal(action, subject, additionalMsg , trueCallBack , falseCallBack ){

    var dialog = $('#confirmationModal').modal();

    dialog.removeData('bs.modal');
    dialog.modal("hide");

    // "<?php echo $phrases['are_you_sure']?> "+
    $('#confirmationMessage').html(action+""+subject+"? " +additionalMsg).css({color:'rgba(41,160,50,1) '});
    $("#confirmationModal").modal('show');

    if(trueCallBack != ""){

        $('#agreedBtn').unbind('click');
        $("#agreedBtn").on('click', function (event) {
            trueCallBack();
        });

    }//else simply closes modal

    if(falseCallBack != ""){

        $('#cancelBtn').unbind('click');
        $("#cancelBtn").on('click', function (event) {
            falseCallBack();
        });

    }//else simply closes modal

}
/*************************************************************************************************/
function topRoleChange(role_id)
{
  var form_data = {
    role_id: role_id
  };
  $.ajax({
    url: "ajax/changeRole.php",
    type: "POST",
    data: form_data,
    success: function(msg){
      show_alert("<?php echo $t->tryTranslate('role_changed'); ?>", 'dashboard.php', 1);
    }
  });
}
/*************************************************************************************************/
/*
Description: modal manual within a manual dynamics
Purpose: Allowing modal within a modal
Parameters:
  - close_modal : Modal to close
  - open_modal : Modal to open

How to use:

  confirmationModal("approve", "volunteer", "",
      function(){
        // do stuff for Ok btn
      },
      function(){
        // do stuff for Cancel btn
      }
  );

Created by: @Rai
*/

/* If Updated by Others than the Creator
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s) :
*/

/*************************************************************************************************/
//Created By: Kitcathe
//Note: module ex: volunteers, subjects, etc.
//element_id => the name of the id of the element
function keywordSearch(keyword,project_id,module,file_name,element_id){
  var form_data = {
  keyword : keyword,
  project_id: project_id,
  module: module
  };
  ajax_load(form_data, file_name, element_id);
}

</script>
