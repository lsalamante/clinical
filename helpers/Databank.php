<?php
namespace Clinical\Helpers;

use PDO;


/**
* @author: @jjjjcccjjf
*/
class Databank
{
  /**
  * @var string
  */
  public $data; # used on databank data
  public $role_id;

  public $project_id;
  public $project_name;


  /**
  * you can skip the last 2 params if only getting the notifications
  * @param int $project_id
  * @param string $language
  */
  public function __construct($project_id, $language = 'chinese')
  {

    $this->language = $language;
    $this->project_id = $project_id;
    date_default_timezone_set("Asia/Singapore");
    // $this->created_at = date('Y-m-d h:i:s');

    $this->fetch_databank();
  }


  /**
  * get all databanks based on project_id
  * @return array [description]
  */
  public function fetch_uploader($role_id)
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');
    # Get name of uploader
    $stmt = $conn->prepare("SELECT `fname` FROM `roles` r LEFT JOIN `users` u ON r.user_id = u.user_id WHERE r.role_id = $role_id");
    $result = $stmt->execute();
    if($result)
    {
      $user_arr = $stmt->fetchAll();
      foreach($user_arr as $name)
      {
        $user_name = $name['fname'];
      }
      return $user_name;
    }
    else
    {
      return $result;
    }
  }

  /**
  * get all databanks based on project_id
  * @return array [description]
  */
  public function fetch_databank()
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');
    # Get all uploads from all tables and re-structure using UNION to make everything compatible
    # SELECT FILEPATH as up_path, UPLOADMODULE as module, DEFAULTTITLE as title, UPVERSION as version From table WERE ...
    $stmt = $conn->prepare(
        "SELECT
          `destroy_record` as up_path,
          'Bio Management' as module,
          'Destroy Record' as title,
          'N/A' as version,
          `date_created` as date_uploaded,
          `role_id` as uploader,
          `bio_desc` as description
        FROM `bio_destroy`
        WHERE project_id = $this->project_id AND databank = 1
      UNION ALL
        SELECT
          `determine_record` as up_path,
          'Bio Management' as module,
          'Determine Record' as title,
          'N/A' as version,
          `date_created` as date_uploaded,
          `role_id` as uploader,
          `description` as description
        FROM `bio_determine`
        WHERE project_id = $this->project_id AND databank_record = 1
      UNION ALL
        SELECT
          `determine_report` as up_path,
          'Bio Management' as module,
          'Determine Report' as title,
           'N/A' as version,
          `date_created` as date_uploaded,
          `role_id` as uploader,
          `description` as description
        FROM `bio_determine`
        WHERE project_id = $this->project_id AND databank_report = 1
      UNION ALL
        SELECT
          `handle_record` as up_path,
          'Bio Management' as module,
          'Handle Record' as title,
          'N/A' as version,
          `date_created` as date_uploaded,
          `role_id` as uploader,
          `description` as description
        FROM `bio_handles`
        WHERE project_id = $this->project_id AND databank = 1
      UNION ALL
        SELECT
          `recycle_record` as up_path,
          'Bio Management' as module,
          'Recycle Record' as title,
          'N/A' as version,
          `date_created` as date_uploaded,
          `role_id` as uploader,
          `bio_desc` as description
        FROM `bio_recycle`
        WHERE project_id = $this->project_id AND databank = 1
      UNION ALL
        SELECT
          `collect_record` as up_path,
          'Bio Management' as module,
          'Samples Collect Record' as title,
          'N/A' as version,
          `date_created` as date_uploaded,
          `role_id` as uploader,
          `description` as description
        FROM `bio_samples`
        WHERE project_id = $this->project_id AND databank = 1
      UNION ALL
        SELECT
          `process_record` as up_path,
          'Bio Management' as module,
          'Samples Process Record' as title,
          'N/A' as version,
          `date_created` as date_uploaded,
          `role_id` as uploader,
          `description` as description
        FROM `bio_samples`
        WHERE project_id = $this->project_id AND databank = 1
      UNION ALL
        SELECT
          `record` as up_path,
          'Bio Management' as module,
          'Save Record' as title,
          'N/A' as version,
          `date_created` as date_uploaded,
          `role_id` as uploader,
          `bio_desc` as description
        FROM `bio_save`
        WHERE project_id = $this->project_id AND databank = 1
      UNION ALL
        SELECT
          `process_record` as up_path,
          'Bio Management' as module,
          'Transport Process Record' as title,
          'N/A' as version,
          `date_created` as date_uploaded,
          `role_id` as uploader,
          `description` as description
        FROM `bio_transport`
        WHERE project_id = $this->project_id AND databank = 1
      UNION ALL
        SELECT
          tm.`up_path`,
          'Training Material' as module,
          tm.`name` as title,
          tm.`version`,
          tm.`version_date` as date_uploaded,
          t.`created_by` as uploader,
          tm.`description`
        FROM trainings t LEFT JOIN training_material tm ON t.training_id = tm.training_id
        WHERE t.project_id = $this->project_id AND tm.databank = 1
      UNION ALL
        SELECT
          `inspection` as up_path,
          'Drugs List' as module,
          `trade_name` as title,
          'N/A' as version,
          `date_added` as date_uploaded,
          `added_by` as uploader,
          `specification` as description
        FROM drugs
        WHERE project_id = $this->project_id AND databank = 1
      UNION ALL
        SELECT
          sed.`up_path`,
          'Side Effects Documents' as module,
          sed.`name` as title,
          sed.`version`,
          se.`date_created` as date_uploaded,
          se.`created_by` as uploader,
          sed.`description`
        FROM side_effects_docs sed LEFT JOIN side_effect se ON sed.side_effect_id = se.side_effect_id
        WHERE se.project_id = $this->project_id AND sed.databank = 1
      UNION ALL
        SELECT
          sf.`file` as up_path,
          'Summary Documents' as module,
          sm.`name` as title,
          sm.`version`,
          sm.`version_date` as date_uploaded,
          ss.`added_by` as uploader,
          sm.`description`
        FROM 
          stage_summary ss LEFT JOIN summary_materials sm ON ss.stage_summary_id = sm.stage_summary_id
                           LEFT JOIN summary_files sf ON sm.material_id = sf.material_id
        WHERE ss.project_id = $this->project_id AND sm.databank = 1
      UNION ALL
        SELECT
          inspu.`inspection_file` as up_path,
          'Inspection Documents' as module,
          'Inspection' as title,
          'N/A' as version,
          insp.`date_added` as date_uploaded,
          insp.`added_by` as uploader,
          insp.`report_description` as description
        FROM inspections insp LEFT JOIN inspection_uploads inspu ON insp.inspection_id = inspu.inspection_id
        WHERE insp.project_id = $this->project_id AND inspu.databank = 1
      UNION ALL
        SELECT
          `up_path`,
          `up_type` as module,
          `up_name` as title,
          `up_ver` as version,
          up_date as date_uploaded,
          `role_id` as uploader,
          `up_desc` as description
        FROM uploads
        WHERE project_id = $this->project_id AND databank = 1
      ORDER BY date_uploaded DESC
      ");
    $result = $stmt->execute();

    if($result){
      $this->data = $stmt->fetchAll();
      // foreach ($stmt->fetchAll() as $row) {
      //   $this->up_path[] .= $row['up_path'];
      //   $this->title[] .= $row['title'];
      // } #end foreach
    }
    return $result;
  }
}
