<?php
namespace Clinical\Helpers;

use PDO;

/**
* @author: @jjjjcccjjf
*/

class DoubleCheck {

  // public $project_id; #unused

  /**
   * json version of $payload
   * @var string
   */
  public $json_payload;

  /**
   * array of payload data
   * @var array
   */
  public $payload;

  public $language;

  public $table;
  public $pk_col;
  public $id;


  public function __construct($table = null, $pk_col = null, $id = null, $language = 'chinese')
  {
    $this->table = $table;
    $this->pk_col = $pk_col;
    $this->id = $id;

    $this->language = $language;
  }

  /**
   * fetch payload from DB, also json_encode
   * @param  string $table  [description]
   * @param  string $pk_col [description]
   * @param  int $id     [description]
   * @return void         [description]
   */
  public function fetch($table = null, $pk_col = null, $id = null){
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT dbl_chk_payload FROM $table WHERE $pk_col = $id");
    $result = $stmt->execute();

    if($result){
      $this->json_payload = $stmt->fetch(PDO::FETCH_ASSOC)['dbl_chk_payload'];
    }

    $this->fetchPayloadArr();
  }

  /**
   * update payload in database for specific table
   * @param  string $table  [description]
   * @param  string $pk_col [description]
   * @param  int $id     [description]
   * @return bool         [description]
   */
  public function updatePayload($table = null, $pk_col = null, $id = null){

    $this->json_payload = json_encode($this->payload, JSON_UNESCAPED_UNICODE);

    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');
    $stmt = $conn->prepare("UPDATE $table SET dbl_chk_payload = '" . $this->json_payload . "' WHERE $pk_col = $id");

    return $stmt->execute();
  }


  public function fetchPayloadArr()
  {
    $this->payload = json_decode($this->json_payload, true);
    return true; #todo? ?/
  }

  public function resetPayload()
  {
    foreach($this->payload as &$row){
      if($row['is_checked'] == -1){
        $row['is_checked'] = 0;
        $row['comments'] = '';
      }
    }
  }

}
