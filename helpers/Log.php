<?php
namespace Clinical\Helpers;

use PDO;

/**
 * @author: @jjjjcccjjf
 */

class Log {
  /**
  * Logger class for logging user actions
  * if you just want to get the logs array
  * @example:
  * $o = new Log($_SESSION['role_id']);
  * $o->fetchLogs();
  * var_dump($o->logs);
  *
  * otherwise, if you want to save a log record
  * @example:
  * $o->(Integer $role_id, String $action, Array $payload);
  * $o->save();
  *
  */

  public $role_id;

  public $created_at;

  /**
   * variable for user action
   * must be parameter safe
   * no spaces, just underscores
   * @var string
   * @example:
   * "login", "logout", "modify_record", "modify", "submit"
   */
  public $action; # action key

  /**
   * will be displayed as key => value pairs on the log message
   * @var array
   * @example
   * array("project_name" => "hello world", "submitter" => "john doe")
   */
  public $payload;

  public $log_name;
  public $log_role;

  /**
   * json string of the $payload var
   * @var string
   */
  public $json_payload;

  /**
   * array of logs
   * @var array
   */
  public $logs;

 /**
  * you can omit the last 2 varaibles if you are only getting the logs property
  * @param int $role_id pass in the $_SESSION['role_id'] here (mostly)
  * @param string $action same with action property
  * @param array $payload same payload property
  */
  public function __construct($role_id = null, $action = null, $payload = null)
  {
    $this->payload = $payload;
    $this->action = $action;
    $this->role_id = $role_id;
    $this->created_at = date('Y-m-d h:i:s');

    $this->json_payload = $this->jsonifyPayload($payload);

    $u = new User('role_id', $role_id);
    $this->log_name = $u->fname;
    $this->log_role = $u->role;

  }

  /**
   * returns json encoded string of the $payload array
   * @param  array $payload the payload to encode to json
   * @return string
   */
  public function jsonifyPayload($payload)
  {
    if($payload == null)
    return '';
    else
    return json_encode($payload);
  }

  /**
   * inserts a record to the logs table
   * using the object's properties
   * @return bool
   */
  public function save()
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');
    $stmt = $conn->prepare("INSERT INTO logs (created_at, role_id, action, payload)
    VALUES (:created_at, :role_id, :action, :payload)");

    $stmt->bindValue(':role_id', $this->role_id);
    $stmt->bindValue(':created_at', $this->created_at);
    $stmt->bindValue(':action', $this->action);
    $stmt->bindValue(':payload', $this->json_payload);

    return $stmt->execute();
  }

  /**
   * get all logs based on role_id property
   * @return array
   */
  public function fetchLogs()
  {
    $u = new User('role_id', $this->role_id);
      $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');
      $stmt = $conn->prepare("SELECT * FROM logs WHERE 1");
      $result = $stmt->execute();

      if($result){
        $this->logs = $stmt->fetchAll(PDO::FETCH_ASSOC);
      }
      return $result;
  }

}
