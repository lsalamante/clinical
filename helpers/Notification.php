<?php
namespace Clinical\Helpers;
//
// require('PHPMailer-master/PHPMailerAutoload.php');
// use \PHPMailer;

use PDO;


/**
* @author: @jjjjcccjjf
*/
class Notification {
  /**
  * @link: http://ja6iae.axshare.com/#g=1&p=__message
  *
  */

  /**
  * a translation-safe generic title of a status
  * also, the subject header of the notification / email
  * @example: actual output: "Submitted project" $t->tryTranslate('submitted_project')
  * @var string
  */
  public $title;

  /**
  * @var string
  */
  public $todo;
  public $created_at;

  public $role_id;
  public $email;
  public $fname;
  public $status_num;
  public $acronym;
  public $url;

  public $project_id;
  public $project_name;
  public $project_owner;

  public $language;
  public $unread_notifs;

  /**
  * notifications array for the particular user(role_id)
  * @var array
  */
  public $notifications;

  /**
  * you can skip the last 2 params if only getting the notifications
  * @param int $role_id
  * @param int $project_id
  * @param string $language
  */
  public function __construct($role_id, $project_id, $language = 'chinese'){

    $this->language = $language;
    $this->role_id = $role_id;
    $this->project_id = $project_id;

    $u = new User('role_id', $this->role_id, $this->language);
    $this->fname = $u->fname;
    $this->email = $u->email;
    $this->acronym = $u->acronym;
    $this->is_professional_group = $u->is_professional_group;

    $p = new Project($this->project_id);
    $this->project_name = $p->project_name;
    $this->project_owner = $p->project_owner;
    $this->status_num = $p->status_num;

    $s = new Status($this->role_id, $this->project_id, $this->language);
    $this->title = $s->title;
    $this->todo = $s->todo;

    $this->fetchUrl();

    date_default_timezone_set("Asia/Singapore");
    $this->created_at = date('Y-m-d h:i:s');

    $this->fetch();
  }

  /**
  * main function for notification
  * @return void
  */
  public function notify(){
    $this->newNotification();
    $this->notifyEmail();
  }

  /**
  * get all notifications
  * @return array [description]
  */
  public function getAll(){
    return $this->notifications;
  }

  /**
  * send an email to the user to be notified
  * @return bool [description]
  */
  public function notifyEmail()
  {
    $t = new Translation($this->language);

    $email_from = 'noreply@betaprojex.com';
    $email_from_name = '科群迈谱';

    $reply_to = 'reply@example.com';
    $reply_to_name = 'Clinical Notification Test Reply';

    $subject = $this->title;

    $message = "
    <h1>$this->title</h1><br>
    <h4>" . $t->tryTranslate('hello') . " $this->fname,</h4><b4>
    <p>" . $t->tryTranslate('project_name') . ": $this->project_name</p><br>
    <p>" . $t->tryTranslate('project_owner') . ": $this->project_owner</p><br>
    <p>" . $t->tryTranslate('todo') . ": $this->todo</p><br>
    <p>" . $t->tryTranslate('timestamp') . ": $this->created_at</p><br>
    ";

    $mail = new \PHPMailer;
    $mail->isSMTP();                                                                       // Set mailer to use SMTP
    $mail->Host = 'mail.smtp2go.com';                                                      // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                                                                // Enable SMTP authentication
    $mail->Username = 'betamail@optimindsolutions.com';                                    // SMTP username
    $mail->Password = 'MDFldDJ5a3lkbWk3';                                                  // SMTP password
    $mail->SMTPSecure = 'tls';                                                             // Enable TLS encryption, `ssl` also accepted
    $mail->Port = "80";                                                                    // TCP port to connect to
    $mail->setFrom($email_from, $email_from_name);
    $mail->addAddress($this->email, $this->fname); #TODO
    $mail->addReplyTo($reply_to, $reply_to_name);
    $mail->isHTML(true);
    $mail->Subject = $subject;
    // $mail->AddEmbeddedImage('../img/email-img/image3.png', 'my-photo', 'image3.jpg ');
    $mail->Body = $message;

    return $mail->send();

  }

  /**
  * inserts a new notificaiton row to the db
  * @return bool [description]
  */
  public function newNotification()
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');
    $stmt = $conn->prepare("INSERT INTO notifications (role_id, project_id, title, email, project_name, project_owner, todo, url, created_at)
    VALUES (:role_id, :project_id, :title, :email, :project_name, :project_owner, :todo, :url, :created_at)");

    $stmt->bindValue(':role_id', $this->role_id);
    $stmt->bindValue(':project_id', $this->project_id);
    $stmt->bindValue(':title', $this->title);
    $stmt->bindValue(':email', $this->email);
    $stmt->bindValue(':project_name', $this->project_name);
    $stmt->bindValue(':project_owner', $this->project_owner);
    $stmt->bindValue(':todo', $this->todo);
    $stmt->bindValue(':url', $this->url);
    $stmt->bindValue(':created_at', $this->created_at);

    return $stmt->execute();
  }

  /**
  * gets all notifications from db
  * @return array [description]
  */
  public function fetch(){
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT * FROM notifications WHERE role_id = $this->role_id ORDER BY notification_id DESC");
    $result = $stmt->execute();

    if($result){
      $this->notifications = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    return $result;
  }

  public function countNotif() {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT COUNT(*) AS unread_notifs FROM notifications WHERE role_id = $this->role_id AND is_read = 0 ORDER BY notification_id DESC");
    $result = $stmt->execute();

    if($result){
      $this->unread_notifs = $stmt->fetch(PDO::FETCH_ASSOC)['unread_notifs'];
    }

    return $result;
  }


  public function fetchUrl()
  {
    if($this->acronym == 'A'){
      # IF APPLICANT
      $page = ($this->status_num < 13) ? "new_project_draft.php" : "pg_audit.php";
      $this->url = $GLOBALS['base_url'] . $page . "?project_id=" . $this->project_id;

    }else{

      if($this->status_num >= 13 && $this->is_professional_group){
        $this->url = $GLOBALS['base_url'] . "pg_audit.php?project_id=" . $this->project_id;
      }else{
        $this->url = $GLOBALS['base_url'] . strtolower($this->acronym) . "_audit.php?project_id=" . $this->project_id;
      }

    }
  }

}
