<?php
namespace Clinical\Helpers;

/**
* a factory for s
* @author: @jjjjcccjjf
*/
class NotificationFactory {

  /**
  * use this static class to mass produce notifications
  * @param  array $role_id_array associative array of the role_id's
  * @example:
  * array('0' => 'A',  # for applicant
  *       '1' => 'TFO') this is the structure
  *       so we're going to use the keys instead of the values
  *       we are doing this because we need it for filtering the users per status_num
  *       since keys can't duplicate
  * @param  int $project_id    [description]
  * @param  string $language      [description]
  * @return void                [description]
  */
  public static function create($role_id_array, $project_id, $language = 'chinese', $title = null, $todo = null)
  {
    foreach($role_id_array as $role_id => $acronym){
      $n = new Notification($role_id, $project_id, $language);
      // $n->newNotification(); # save a notif on DB

      # Optional manual todo and title
      if(!is_null($todo) && !is_null($title)){
        $n->todo = $todo;
        $n->title = $title;
      }

      // $n->notifyEmail(); # send an email
      $n->notify(); # uncomment if also want to send email
    }
  }

}
