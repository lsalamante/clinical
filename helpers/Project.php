<?php
namespace Clinical\Helpers;

use PDO;

class Project
{
  public $project_name;
  public $project_num;
  public $project_owner;
  public $project_owner_role_id;
  public $project_owner_user_id;

  /**
  * id from status table
  * @var int
  */
  public $status_id;

  /**
  * status number of the current project_id
  * @var int
  */
  public $status_num;

  /**
  * array of role id's involved with the project
  * @var array
  */
  public $notify_group;

  /**
  * all position acronyms
  * @var array
  */
  public $positions;
  public $pgroup_id;
  public $project_id;

  /**
  * @var tinyint
  */
  public $is_rejected;

  /**
  * initializes the object using fetch method
  * @param int $project_id
  * @return return void
  */
  public function __construct($project_id = null)
  {
    $this->project_id = $project_id;

    $this->fetch();
  }

  /**
  * just setting all of the objects properties
  * @return void
  */
  public function fetch($no_refine = false)
  {
    $this->fetchProject();
    $this->fetchStatus();

    $this->fetchPositions();

    $this->fetchProfessionalGroup();
    $this->fetchPI();
    $this->fetchPRE();
    $this->fetchOfficers();
    $this->fetchOwnerRoleId();
    $this->fetchResearchers();
    $this->fetchGlobalUsers();

    if(!$no_refine)
    $this->refine(); # refine to_notify based on status_num
    # remove index 0
    # just cleanup for the array
    unset($this->notify_group[0]);
  }

  /**
  * fetch project related stuffs from the DB and set assigns them to the object's
  * properties
  * @return bool [description]
  */
  public function fetchProject()
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT * FROM projects WHERE project_id = $this->project_id");
    $result = $stmt->execute();

    if($result){
      foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
        $this->project_name = $row['project_name'];
        $this->project_num = $row['project_num'];
        $this->project_owner_user_id = $row['user_id'];
        $this->status_id = $row['status_id'];
        $this->pgroup_id = $row['pgroup_id'];
      } #end foreach


      $u = new User('user_id', $this->project_owner_user_id);
      $this->project_owner = $u->fname;
    }

    return $result;
  }

  /**
  * fetch stuff from status table
  * @return bool [description]
  */
  public function fetchStatus()
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT * FROM status WHERE status_id = $this->status_id");
    $result = $stmt->execute();

    if($result){
      foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){
        $this->status_num = $row['status_num'];
        $this->is_rejected = $row['is_rejected'];
      }

    }

    return $result;
  }

  public function fetchProfessionalGroup()
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT * FROM roles, positions WHERE pgroup_id = $this->pgroup_id AND roles.position_id = positions.position_id");
    $result = $stmt->execute();

    if($result){
      foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){

        if(in_array($row['acronym'], ['I']))
        $this->notify_group[$row['role_id']] = $row['acronym'];

      }

    }

    return $result;
  }

  /**
  * todo
  * @return bool [description]
  */
  public function fetchPI()
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT role_id FROM roles WHERE position_id = 3 AND user_id = (SELECT pi_id FROM projects WHERE project_id = $this->project_id)");
    $result = $stmt->execute();

    if($result){
      foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){

        $this->notify_group[$row['role_id']] = 'PI';

      }

    }

    return $result;
  }

  public function fetchGlobalUsers()
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT * FROM roles, positions WHERE roles.position_id = positions.position_id");
    $result = $stmt->execute();

    if($result){
      foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){

        if(in_array($row['acronym'], ['TFO', 'ECS', 'ECM', 'ECC']))
        $this->notify_group[$row['role_id']] = $row['acronym'];
      }

    }

    return $result;
  }

  /**
  * fetch PRE related stuff
  * @return bool [description]
  */
  public function fetchPRE()
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT DISTINCT role_id, project_id FROM `projects_pre` WHERE project_id = $this->project_id");
    $result = $stmt->execute();

    if($result){
      foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){

        $this->notify_group[$row['role_id']] = 'PRE';
      }

    }

    return $result;
  }

  /**
  * fetch ethics committee + TFO
  * @return bool [description]
  */
  public function fetchOfficers()
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT role_id, acronym FROM `positions`, `roles` WHERE acronym IN ('TFO', 'ECS', 'ECM', 'ECC') AND positions.position_id = roles.position_id ORDER BY acronym DESC");
    $result = $stmt->execute();

    if($result){
      foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){
        $this->notify_group[$row['role_id']] = $row['acronym'];
      }

    }

    return $result;
  }

  /**
  * fetch trial begin people
  * @return bool [description]
  */
  public function fetchResearchers()
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT * FROM `projects` WHERE project_id = $this->project_id");
    $result = $stmt->execute();



    if($result){
      foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){

        /**
        * Explode array then set values to notify_group
        */
        foreach($this->explodeStar($row['subj_controller_id']) as $row_id){
          $this->notify_group[$row_id] = 'SC';
        }
        foreach($this->explodeStar($row['med_controller_id']) as $row_id){
          $this->notify_group[$row_id] = 'MC';
        }
        foreach($this->explodeStar($row['bio_controller_id']) as $row_id){
          $this->notify_group[$row_id] = 'BGC';
        }
        foreach($this->explodeStar($row['data_administrator_id']) as $row_id){
          $this->notify_group[$row_id] = 'DA';
        }
      }
    }

    return $result;
  }

  /**
  * helper function for fetching Researchers (BGC,MC,SC,DA)
  * @param  [string] $str [imploded array of role id's wrapped in stars]
  * @return [array]      [description]
  */
  public function explodeStar($str){
    $str = trim($str, ","); # clean the array first to make sure no trailing and leading commas
    $str = explode(',', $str); # *78*,*79* -> ['*78*', '*79*']
    $clean_arr = []; # Clean array of role_id's
    foreach($str as $starred_item){
      $clean_arr[] = trim($starred_item, "*"); # trim starred item then push it to $clean_arr
    }
    return $clean_arr; # return clean array of numbers
  }


  /**
  * pushes all acronyms to $positions property
  * @return [type] [description]
  */
  public function fetchPositions()
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT acronym FROM `positions`");
    $result = $stmt->execute();

    if($result){
      foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){
        $this->positions[] = $row['acronym'];
      }

    }

    return $result;
  }

  /**
  * fetches and sets the role id of the project owner
  * @return boo [description]
  */
  public function fetchOwnerRoleId(){
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT role_id FROM `roles` WHERE user_id = $this->project_owner_user_id AND position_id = 1"); #applicant only
    $result = $stmt->execute();

    if($result){
      foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){
        $this->project_owner_role_id = $row['role_id'];
      }

    }

    return $result;
  }

  /**
  * narrow down the $notify_group array property based on the status_num
  * @return bool [description]
  */
  public function refine()
  {
    foreach($this->notify_group as $key => $val){

      switch($this->status_num){

        case 0: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['A'])); break;
        case 1: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['TFO'])); break;
        case 2: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['PI'])); break;
        case 3:

        if($this->is_rejected == 1)
        $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['I']));
        else
        $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['I', 'PRE']));

        break;
        case 4: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['A'])); break;
        case 5: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['I'])); break;
        case 6: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['TFO'])); break;
        case 7: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['ECS'])); break;
        case 8: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['ECM'])); break;
        case 9: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['ECS'])); break;
        case 10: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['I'])); break;
        case 11: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['ECC'])); break;
        case 12: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['ECS'])); break;
        case 13: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['PI'])); break;
        case 14: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['A', 'TFO', 'PI', 'I', 'ECS', 'ECM', 'ECC', 'MC', 'SC', 'BGC', 'DA'])); break;
        case 15: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['I'])); break;
        case 17: $this->notify_group = array_diff($this->notify_group, array_diff($this->positions, ['A'])); break;
        # 14 and above
        # TODO: add cases for status 14 above
        default:
        break;

      }

    }

  }

}
