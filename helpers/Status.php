<?php
namespace Clinical\Helpers;

use PDO;

/**
* Helper class for status related shits
* @author: @jjjjcccjjf
*
*/
class Status {
  public $role_id;
  public $language;

  public $project_id;
  public $status_num;

  /**
  * @var tinyint
  */
  public $is_rejected;

  /**
  * translation safe phrase for the todo
  * @var string
  */
  public $todo;

  /**
  * translation safe generic title
  * @var string
  */
  public $title;

  public function __construct($role_id, $project_id, $language = 'chinese')
  {
    $this->role_id = $role_id;
    $this->project_id = $project_id;
    $this->language = $language;

    $p = new Project($this->project_id);
    $this->status_num = $p->status_num;
    $this->is_rejected = $p->is_rejected;

    $this->fetchTitle();
    $this->fetchTodo();
  }

  public function fetchTitle(){
    $t = new Translation($this->language);

    $title = [];

    for($i = 1; $i < 18; $i++){
      $title[$i] = $t->tryTranslate("status_$i" . "_" . "0");
    }

    # For rejected statuses
    if($this->status_num == 0 && $this->is_rejected){
      $this->title = $t->tryTranslate("status_0_1"); # Rejected Audit By Principal Investigator or Testing Facility Office
      return;
    }elseif($this->status_num == 3 && $this->is_rejected){
      $this->title = $t->tryTranslate("status_3_1"); # Returned to Investigator with comments By Applicant
      return;
    }elseif($this->status_num == 7 && $this->is_rejected){
      $this->title = $t->tryTranslate("status_7_1"); # Returned to Applicant and Investigator for modification by Ethics Committee Secretary
      return;
    }elseif($this->status_num == 9 && $this->is_rejected){
      $this->title = $t->tryTranslate("status_9_1"); # Rejected new version from Investigator by Ethics Committee Chairman
      return;
    }

    if(array_key_exists($this->status_num, $title))
    $this->title = $title[$this->status_num];
    else
    $this->title = $t->tryTranslate('not_applicable');
  }

  /**
  * todo dispatcher for every role
  * cooooool
  * @return void [description]
  */
  public function fetchTodo(){
    $t = new Translation($_SESSION['lang']);
    $u = new User('role_id', $this->role_id);

    switch($u->acronym){
      case 'A': $this->fetchTodoA(); break;
      case 'TFO': $this->fetchTodoTFO(); break;
      case 'PI': $this->fetchTodoPI(); break;
      case 'I': $this->fetchTodoI(); break;
      case 'PRE': $this->fetchTodoPRE(); break;
      case 'ECS': $this->fetchTodoECS(); break;
      case 'ECM': $this->fetchTodoECM(); break;
      case 'ECC': $this->fetchTodoECC(); break;
      case 'BGC': $this->fetchTodoBGC(); break;
      case 'MC': $this->fetchTodoMC(); break;
      case 'SC': $this->fetchTodoSC(); break;
      case 'DA': $this->fetchTodoDA(); break;

      default: $t->tryTranslate('not_applicable'); break;
    }

  }

  #TODO: docblocks
  public function checkTodo($todo){
    $t = new Translation($this->language);

    if(isset($todo[$this->status_num][$this->is_rejected]))
    return $todo[$this->status_num][$this->is_rejected];
    else
    return $t->tryTranslate('not_applicable');
  }

  public function fetchTodoA()
  {
    $t = new Translation($this->language);

    $todo = [];
    $todo[1][1] = $t->tryTranslate('applicant_todo_1_1'); #Please check the comments from the Testing Facility Officer and revise blahblah
    $todo[2][1] = $t->tryTranslate('applicant_todo_2_1'); #Please check the comments from the Principal Investigator blahblah
    $todo[4][0] = $t->tryTranslate('applicant_todo_4_0'); #Version 2 uploaded by Investigator
    $todo[7][1] = $t->tryTranslate('applicant_todo_9_1'); # Rejected by ethics secretary
    $todo[17][0] = $t->tryTranslate('applicant_todo_17_0'); # Please review Inspection report

    $this->todo = $this->checkTodo($todo);

  }
  public function fetchTodoPI()
  {
    $t = new Translation($this->language);

    $todo = [];
    $todo[2][0] = $t->tryTranslate('PI_todo_2_0'); # Audit project accepted by TFO
    $todo[13][0] = $t->tryTranslate('PI_todo_13_0'); # Please add project member

    $this->todo = $this->checkTodo($todo);

  }
  public function fetchTodoPRE()
  {
    $t = new Translation($this->language);

    $todo = [];
    $todo[3][0] = $t->tryTranslate('PRE_todo_3_0'); # Offer comments to applicant's project

    $this->todo = $this->checkTodo($todo);

  }
  public function fetchTodoI()
  {
    $t = new Translation($this->language);

    $todo = [];
    $todo[3][0] = $t->tryTranslate('I_todo_3_0'); # Please Upload version 2
    $todo[3][1] = $t->tryTranslate('I_todo_3_1'); # Please Revise version 2
    $todo[5][0] = $t->tryTranslate('I_todo_5_0'); # Please Uplaod consent agreement
    $todo[10][0] = $t->tryTranslate('I_todo_10_0'); # Please Uplaod new version
    $todo[15][0] = $t->tryTranslate('I_todo_15_0'); # Please upload report material / Please review applicant report material

    $this->todo = $this->checkTodo($todo);

  }
  public function fetchTodoECS()
  {
    $t = new Translation($this->language);

    $todo = [];
    $todo[7][0] = $t->tryTranslate('ECS_todo_7_0'); # Please audit the project
    $todo[9][0] = $t->tryTranslate('ECS_todo_9_0');  # Please upload conference consent and conference content
    $todo[9][1] = $t->tryTranslate('ECS_todo_9_1');  # Please RE-upload conference consent and conference content
    $todo[12][0] = $t->tryTranslate('ECS_todo_12_0');  # Please approve for trial to begin

    $this->todo = $this->checkTodo($todo);

  }
  public function fetchTodoECM()
  {
    $t = new Translation($this->language);

    $todo = [];
    $todo[8][0] = $t->tryTranslate('ECM_todo_8_0'); # Offer Comment

    $this->todo = $this->checkTodo($todo);

  }
  public function fetchTodoECC()
  {
    $t = new Translation($this->language);

    $todo = [];
    $todo[11][0] = $t->tryTranslate('ECC_todo_11_0'); # Audit version 2

    $this->todo = $this->checkTodo($todo);

  }

  public function fetchTodoTFO(){
    $t = new Translation($this->language);

    $todo = [];

    $todo[1][0] = $t->tryTranslate('tfo_todo_1_0'); # Please review applicant submission
    $todo[6][0] = $t->tryTranslate('tfo_todo_6_0'); # Please submit materials

    $this->todo = $this->checkTodo($todo);
  }

  public function fetchTodoBGC(){
    $t = new Translation($this->language);

    $todo = [];

    $todo[14][0] = $t->tryTranslate('bgc_todo_14_0'); # You can now upload biological samples

    $this->todo = $this->checkTodo($todo);
  }

  public function fetchTodoMC(){
    $t = new Translation($this->language);

    $todo = [];

    $todo[14][0] = $t->tryTranslate('mc_todo_14_0'); # You can now add drugs and drug records

    $this->todo = $this->checkTodo($todo);
  }

  public function fetchTodoSC(){
    $t = new Translation($this->language);

    $todo = [];

    $todo[14][0] = $t->tryTranslate('sc_todo_14_0'); # You can now add volunteers

    $this->todo = $this->checkTodo($todo);
  }

  public function fetchTodoDA(){
    $t = new Translation($this->language);

    $todo = [];

    $todo[14][0] = $t->tryTranslate('da_todo_14_0'); # You can now perform double checking

    $this->todo = $this->checkTodo($todo);
  }


}
