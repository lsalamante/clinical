<?php
namespace Clinical\Helpers;

use PDO;

/**
* translation helper
* @author: @jjjjcccjjf
*/
class Translation {

  public $phrases; #holds the phrases array
  public $language;

  /**
  * just a normal constructor
  * @param string $language. Pass in the $_SESSION['lang'] to this
  * @return void
  */
  public function __construct($language = 'chinese'){ #english as default if no parameter was passed #TODO: env vars
    $this->language = $language;
    $this->phrases = $this->getPhrases();
  }

  /**
  * converts chinese to ascii
  * stolen code from vanleor hehehehhe
  * @param  [type] $ords     [description]
  * @param  string $encoding [description]
  * @return [type]           [description]
  */
  public function ords_to_unistr($ords, $encoding = 'UTF-8'){
    $str = '';
    for($i = 0; $i < sizeof($ords); $i++){
      $v = $ords[$i];
      $str .= pack("N",$v);
    }
    $str = mb_convert_encoding($str,$encoding,"UCS-4BE");
    return($str);
  }

  /**
  * returns langauge table data from the database
  * @return array [description]
  */
  public function getLanguage(){
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT * FROM language");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  /**
  * returns the phrases array based on the current language
  * @return array [description]
  */
  public function getPhrases(){

    $result = $this->getLanguage();

    $data = [];

    if($this->language == 'english'){
      foreach($result as $row){
        $data[$row['phrase']] = $row['english'];
      }
    }else if ($this->language == 'chinese'){
      foreach($result as $row){
        $data[$row['phrase']] = $this->ords_to_unistr(explode(',', $row['chinese']));
      }
    }

    return $data;
  }

  # returns the phrases array
  public function getAll(){
    return $this->phrases;
  }

  /**
  * returns the translated word based on key
  * @param  string $key [description]
  * @return string      [description]
  */
  public function tryTranslate($key){
    $this->addKey($key);
    return $this->phrases[$key];
  }

  # accepts array key
  # returns bool
  #

  /**
  * check if key exists in phrases array
  * @param  string $key [description]
  * @return bool      [description]
  */
  public function checkKey($key){
    if(array_key_exists($key, $this->phrases))
    return true;
    else
    return false;
  }

  /**
  * adds the key + value to the phrases array if key is nonexistent
  * @param string $key [description]
  */
  public function addKey($key){
    if($this->checkKey($key))
    $this->phrases[$key];
    elseif($key == null)
    $this->phrases[$key] = null;
    else
    $this->phrases[$key] = "<span style='color:magenta'>$key</span>";
  }

}
