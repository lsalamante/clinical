<?php
namespace Clinical\Helpers;

use PDO;

class User
{
  public $fname;
  public $email;
  public $acronym;
  public $lang_phrase;
  public $role;
  public $is_professional_group;

  protected $role_id;
  protected $user_id;
  protected $position_id;

  /**
  * Just a normal constructor
  * Setups the default values for the class. calls the fetch method to initialize
  *
  * @author @jjjjcccjjf
  * @param string $key. can either be 'role_id' OR 'user_id' only
  * @param integer $id of user depending on the key
  * @return return void
  */
  public function __construct($key = null, $id = null, $language = 'chinese')
  {
    if($key == 'role_id'){
      $this->role_id = $id;
      $this->fetchUserId($id);
    }elseif($key == 'user_id'){
      $this->user_id = $id;
    }

    $this->language = $language;
    $this->fetch();
  }


  /**
  * fetches the user_id from the database via role_id
  * sets the user_id property
  * @author @jjjjcccjjf
  * @param int $role_id
  * @return return bool
  */
  public function fetchUserId($role_id)
  {

    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT user_id FROM roles WHERE role_id = $role_id");
    $result = $stmt->execute();

    if($result){
      $this->user_id = $stmt->fetch(PDO::FETCH_ASSOC)['user_id'];
    }

    return $result;
  }

  public function fetch()
  {
    $this->fetchUser();
    $this->fetchPositionId();
    $this->fetchAcronym();
    $this->fetchRole();
  }

  public function fetchUser()
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT * FROM users WHERE user_id = $this->user_id");
    $result = $stmt->execute();

    if($result){
      foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
        $this->fname = $row['fname'];
        $this->email = $row['email'];
      }
    }

    return $result;
  }

  public function fetchPositionId()
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT position_id FROM roles WHERE role_id = $this->role_id");
    $result = $stmt->execute();

    $this->position_id = $stmt->fetch(PDO::FETCH_ASSOC)['position_id'];

    return $result;
  }

  public function fetchAcronym()
  {
    $conn = new PDO('mysql:host=localhost;dbname=clinical', 'root', '');

    $stmt = $conn->prepare("SELECT * FROM positions WHERE position_id = $this->position_id");
    $result = $stmt->execute();

    foreach($stmt->fetchAll(PDO::FETCH_ASSOC) as $row){
      $this->acronym = $row['acronym'];
      $this->lang_phrase = $row['lang_phrase'];
      $this->is_professional_group = $row['is_professional_group'];
    }

    return $result;
  }

  public function fetchRole()
  {
    $t = new Translation($this->language);
    $this->role = $t->tryTranslate($this->lang_phrase);
  }

}
