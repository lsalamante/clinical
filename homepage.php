<?php
namespace Clinical\Helpers;
error_reporting(0);
include('jp_library/jp_lib.php');
require("php-functions/fncApplicant.php");
require("php-functions/fncCommon.php");
require("php-functions/fncProjects.php");
require("php-functions/fncStatusLabel.php"); # must always be below fncCommon.php and fncProjects.php

$n = new Notification($_SESSION['role_id'], NULL, $_SESSION['lang']);
$t = new Translation($_SESSION['lang']);

if (!isset($_SESSION['user_id'])) {
  header("Location: " . "index.php");
  die();
}

?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>

<body class="bggray">
  <section id="container">
    <!--header start-->
    <header class="header white-bg">
      <?php
      if ($LEFT_SIDEBAR) {
        //echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
      }
      ?>
      <!--logo start-->
      <?php if ($LOGO) {
        include('logo.php');
      }
      ?>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <?php if ($NOTIFICATION) {
          include('notification.php');
        } ?>
        <!--  notification end -->
      </div>
      <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
      include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->

<?php
$n->fetch();
 ?>
<section class="panel">
  <div class="panel-body">
    <div class="form-group home-page-header">
      <?php echo $t->tryTranslate('to_do_list'); ?> <?php echo "(" . count($n->notifications) . ")"; ?>
    </div>
    <?php if (count($n->notifications) <= 0): ?>
      <div class="form-group home-page-body">
          <section class="panel">
            <div class="row">
              <div class="col-lg-12 text-center" style="top: 40px;">
                <img src="img/sad.png" class="sad">
                <h1 class="sad"><?php echo $phrases['empty_table_data']; ?></h1>
              </div>
            </div>
          </section>
      </div>
    <?php endif; ?>

    <?php
    foreach ($n->notifications as $notification) {
     ?>
     <a href="<?php echo $notification['url']; ?>">
       <div class="form-group home-page-body">
        <div class="pull-right"><?php echo $notification['created_at']; ?></div>
        <div><?php echo $notification['title']; ?></div><br>
        <div><?php echo $t->tryTranslate('project_name'); ?> : <?php echo $notification['project_name']; ?></div>
        <div><?php echo $t->tryTranslate('applicant'); ?> : <?php echo $notification['project_owner']; ?></div>
        <div><?php echo $t->tryTranslate('to_do'); ?> : <?php echo $notification['todo']; ?></div>
      </div>
    </a>
    <?php } ?>
  </div>

</section>


            <!-- page end-->
          </section>
        </section>
        <div id="focus_me"></div>
        <!--main content end-->
        <!-- Right Slidebar start -->
        <?php
        if ($RIGHT_SIDEBAR) {
          include('right-sidebar.php');
        }
        ?>
        <!-- Right Slidebar end -->
        <!--footer start-->
        <?php include('footer.php'); ?>
        <!--footer end-->
      </section>
      <?php include('scripts.php'); ?>

      </body>

      </html>
