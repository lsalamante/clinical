<?php
include('jp_library/jp_lib.php');
require("php-functions/fncCommon.php");
require("php-functions/fncProjects.php");

if (!isset($_SESSION['user_id'])) {
  header("Location: " . "index.php");
  die();
}

#initial page data
//$user_array = getUserById(0, $_SESSION['user_id']);
$profgroup_array = getProfGroup(0);
$departments = getAllDepartments(0);
$trials = getAllTrials(0);

if(!isset($_GET['project_id'])){
  $_GET['project_id'] = false;
}

$project = getProjectById(0, $_GET['project_id']);

$prof_group_users = getPIByProfGroupId(0, $project['pgroup_id']); #get PI for principal investigator field

$pgroup_admin_id = getProfGroupAdminIdByProfGroupId(0, $project['pgroup_id']);

$pgroup_admin = getUserById(0, $pgroup_admin_id);
$pi = getUserById(0, $project['pi_id']);

$allowed_arr = [3,5,10,13];

if(in_array($project['status_num'], $allowed_arr)){
  $disabled = '';
}else{
  $disabled = 'disabled';
}

?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<?php include('header.php'); ?>

<body>
  <section id="container">
    <!--header start-->
    <header class="header white-bg">
      <?php
      if ($LEFT_SIDEBAR) {
        #echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
      }
      ?>
      <!--logo start-->
      <?php if ($LOGO) {
        include('logo.php');
      }
      ?>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <?php if ($NOTIFICATION) {
          include('notification.php');
        } ?>
        <!--  notification end -->
      </div>
      <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
      include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <style>
    span.required
    {
      margin-left: 3px;
      color: red;
    }
    </style>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
          <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
              <li><a href="dashboard.php"><i class="fa fa-home"></i> <?php echo $phrases['project_audit']; ?></a></li>
              <li><?php echo $phrases['project_detail']?></li>
            </ul>
            <!--breadcrumbs end -->
          </div>
        </div>
        <?php include('project.php'); ?>
        <section class="panel negative-panel">
          <div class="panel-body">
            <?php include('remarks.php'); ?>
          </div>
        </section>
        <?php if(!($disabled == 'disabled')){ ?>
        <form id="audit_form">
          <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
          <input hidden id="status" name="status" value="1">
          <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
          <section class="panel negative-panel">
            <div class="panel-body">

            <!-- Start of Add Member upon status 12 @Rai-->
               <!--
              Created by: Rai
              Started: 11-28-2016
              Desc: Add Member upon status 12
            -->

            <?php if($project['status_num'] == 12){ ?>
              <section class="panel">
                 <header class="panel-heading">
                  <?php echo $phrases['add_project_members']; ?>
                </header>

                <div class="panel-body">
                  <div class="tab-content">

                      <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $phrases['clinician']; ?><span class="required">*</span></label>
                        <div id="clinician_div">
                            <select name="clinician" id="clinician" class="form-control" required>
                                <option value=""><?php echo $phrases['select']; ?></option>
                                <?php foreach (getProfGroupMembers(false, $_SESSION["pgroup_id"], 7) as $user) { ?>
                                    <option value="<?php echo $user["role_id"] ?>"><?php echo $user["fname"]." ".$user["lname"]; ?></option>
                                <?php } ?>
                            </select>
                            <p class="help-block" style="display:none;" id="clinician_msg"></p>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $phrases['research_nurse']; ?><span class="required">*</span></label>
                        <div id="nurse_div">
                            <select name="nurse" id="nurse" class="form-control" required>
                                <option value=""><?php echo $phrases['select']; ?></option>
                                <?php foreach (getProfGroupMembers(false, $_SESSION["pgroup_id"], 9) as $user) { ?>
                                    <option value="<?php echo $user["role_id"] ?>"><?php echo $user["fname"]." ".$user["lname"]; ?></option>
                                <?php } ?>
                            </select>
                            <p class="help-block" style="display:none;" id="nurse_msg"></p>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3"><?php echo $phrases['biological_sample_controller'] ?><span class="required">*</span></label>
                        <div id="bio_controller_div">
                            <select name="bio_controller" id="bio_controller" class="form-control" required>
                                <option value=""><?php echo $phrases['select']; ?></option>
                                <?php foreach (getProfGroupMembers(false, $_SESSION["pgroup_id"], 5) as $user) { ?>
                                    <option value="<?php echo $user["role_id"] ?>"><?php echo $user["fname"]." ".$user["lname"]; ?></option>
                                <?php } ?>
                            </select>
                            <p class="help-block" style="display:none;" id="bio_controller_msg"></p>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $phrases['medicine_controller']; ?><span class="required">*</span></label>
                        <div id="med_controller_div">
                            <select name="med_controller" id="med_controller" class="form-control" required>
                                <option value=""><?php echo $phrases['select']; ?></option>
                                <?php foreach (getProfGroupMembers(false, $_SESSION["pgroup_id"], 6) as $user) { ?>
                                    <option value="<?php echo $user["role_id"] ?>"><?php echo $user["fname"]." ".$user["lname"]; ?></option>
                                <?php } ?>
                            </select>
                            <p class="help-block" style="display:none;" id="med_controller_msg"></p>
                        </div>
                      </div>

                  </div>
                </div>

                <!-- End of Add Member upon status 12 @Rai-->

              </section>

            <?php } ?>
              <button type="" class="btn btn-primary" id="status_submit" disabled ><?php echo $phrases['submit'] ?></button>
            </div>
          </section>
        </form>
        <?php } ?>
        <!-- page end-->
      </section>
    </section>
    <!--main content end-->
    <!-- Right Slidebar start -->
    <?php
    if ($RIGHT_SIDEBAR) {
      include('right-sidebar.php');
    }
    ?>
    <!-- Right Slidebar end -->
    <!--footer start-->
    <?php include('footer.php'); ?>
    <!--footer end-->
  </section>
  <?php include('scripts.php'); ?>


    <!-- Custom checker @Rai-->
    <script type="text/javascript" src="js/checker.js"></script>

  <script>
  $(document).ready(function () {

    <?php include 'initialize.js' ?>
    <?php include 'audit.js' ?>
    <?php include 'pd_mod.js' ?> /* project document modules */
    <?php include 'pp_mod.js' ?> /* project plan modules */
    <?php include 'disable_project.js' ?>

    switch(status_num){
      case 5: //fallthrough
      $('#pd_add').attr("href", "#pd_modal");
      $('#pd_add').show();
      $('#pd_del').show();
      $('#pd_add').attr("disabled", false);
      $('#pd_del').attr("disabled", false);
      break;
      case 3: //fallthrough
      case 10:
      $('#pp_add').attr("href", "#pp_modal");
      $('#pp_add').attr("disabled", false);
      $('#pp_del').attr("disabled", false);
      $('#pp_add').show();
      $('#pp_del').show();
      break;
      case 12:
      $('#status_submit').attr("disabled", false);
      break;
    }
    /* END DCUMENT RDY*/
  });
  </script>
  </body>
  </html>
