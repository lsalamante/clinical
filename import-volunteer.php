<?php
require("jp_library/jp_lib.php");
$fh = fopen($_FILES['file']['tmp_name'], 'r+');
$lines = array();
while( ($row = fgetcsv($fh, 8192)) !== FALSE ) {
	$lines[] = $row;
}

$params['table'] = "volunteers";
for ($i=1; $i < count($lines); $i++) {
  $data['project_id'] = $_GET['project_id'];
  $data['v_name'] = $lines[$i][0];
  $data['volunteer_type'] = $lines[$i][1];
  $data['gender'] = $lines[$i][2];
  $data['bday'] = date('Y-m-d',strtotime($lines[$i][3]));
  $data['nation'] = $lines[$i][4];
  $data['native'] = $lines[$i][5];
  $data['married'] = $lines[$i][6];
  $data['blood_type'] = $lines[$i][7];
  $data['surgery'] = $lines[$i][8];
  $data['fam_history'] = $lines[$i][9];
  $data['smoker'] = $lines[$i][10];
  $data['drinker'] = $lines[$i][11];
  $data['allergies'] = $lines[$i][12];
  $data['height'] = $lines[$i][13];
  $data['weight'] = $lines[$i][14];
  $data['bmi'] = $lines[$i][15];
  $data['med_history'] = $lines[$i][16];
  $data['mobile'] = $lines[$i][17];
  $params['data'] = $data;
  jp_add($params);
}

header("Location: pg_audit.php?tab=volunteer&project_id=" . $_GET['project_id']);
