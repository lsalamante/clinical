<?php
#INCLUDES
include('jp_library/jp_lib.php');

if (isset($_SESSION['user_id']) && $_SESSION['admin'] == false) {
    header("Location: " . "dashboard.php");
    die();
}

?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php') ?>

<body class="login-body">
<div class="container">
    <form class="form-signin" method="post" id="form_login" action="javascript:applicant_login();">
        <h2 class="form-signin-heading">
        <span>
          <img src="images/loginlogo.jpg">
          <div style="float: right;max-width: 90px;width: 100%;">
              <select name="language" id="language" class="form-control" required style="font-size:12px">
                  <option value="chinese"><i src="img/flags/ch.png" alt="" selected>中文</option>
                  <option value="english"><i src="img/flags/us.png" alt="">English</option>
              </select>
          </div>
        </span>
        科群迈谱(CTRIMAP)<br>临床试验研究管理平台</h2>
        <div class="login-wrap">
            <div class="login-err-msg">
                <span id="err_msg"></span>
            </div>
            <br>
            <input type="text" style="font-size:12px" class="form-control" placeholder="帐号" autofocus required
                   name="mm_num" id="mm_num" onchange="javascript:checkroles();">
            <input style="margin-top:10px" type="password" class="form-control" placeholder="密码" required
                   name="pp_password">
            <div id="rr_role_id_div">
                <select name="rr_role_id" id="rr_role_id" class="form-control" required style="font-size:12px" disabled>
                    <option value="">选择身份</option>
                </select>
            </div>

            <button class="btn btn-lg btn-login btn-block" type="submit">登录</button>
            <br>
            <span>如需申请帐号，请您联系：4008503119，谢谢！</span>
        </div>
    </form>
</div>
<!-- js placed at the end of the document so the pages load faster -->
<script>
    function applicant_login() {
        var form_data = {
            data: $('#form_login').serialize()
        };
        //alert(form_data);
        $.ajax({
            url: "php-functions/fncLogin.php?action=applicantLogin",
            type: "POST",
            data: form_data,
            success: function (msg) {
                if (msg == 1)
                    window.location.href = "homepage.php";
                else
                $('#err_msg').html("登录信息有误"); //Log-in credentials doesn't exist.
            }

        });
    }

    function checkroles()
    {
        if($('#mm_num').val() == '')
        {
            $('#rr_role_id').attr('disabled', 'disabled');
        }
        else
        {
            $('#rr_role_id').removeAttr('disabled');
        }
        var form_data = {
            mm_num: $('#mm_num').val()
        }
        ajax_load(form_data, "load_login_select.php", "rr_role_id_div");
    }

    function ajax_load(form_data, file_url, element_id)
    {
        var final_url = "ajax-load/"+file_url+"?";

        if(!form_data){
        // #if form data is empty?!!?!
        }else{
            $.each(form_data, function( key, value ){
                final_url += key+"="+value+"&";
            });
        }

        var xmlhttp;
        if (window.XMLHttpRequest){
          // code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
        }else{
          // code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
          if (xmlhttp.readyState==4 && xmlhttp.status==200){
            document.getElementById(element_id).innerHTML=xmlhttp.responseText;
          }
        }
        xmlhttp.open("GET",final_url,true);
        xmlhttp.send();
    }
</script>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>

</html>
