var status_num = <?php echo (isset($project['status_num'])) ? $project['status_num'] : 'null'?>;
var proj_pi = <?php echo ($project['pi_id']) ? $project['pi_id'] : 'null'?>;
var proj_pg_contact = <?php echo ($project['pgroup_contact_id']) ? $project['pgroup_contact_id'] : 'null'?>;
var pi_id_selected = <?php echo ($project['pi_id']) ? $project['pi_id'] : 'null' ?>;

/* We'll be reusing this array for multiple id's to delete */
/* So we'll clear it every time after we use it */
var del_arr = [];

/* Initialize pgroup tab contents */
set_pgroup_admin_data();
set_pgroup_data();

/* intialize ajax loaded tables */

var data = {
  project_id:<?php echo $_GET['project_id'] ?>,
};
ajax_load(data, 'mc_body.php', 'mc_body');
ajax_load(data, 'pd_body.php', 'pd_body');
ajax_load(data, 'pp_body.php', 'pp_body');
ajax_load(data, 'cmt_body.php', 'cmt_body');
ajax_load(data, 'ecm_body.php', 'ecm_body');

/* these are our functions for handling check/uncheck behaviours */
function check_all(form_body){
  $(form_body).find(':checkbox').each(function(){
    $(this).prop('checked',true);
  });
}
function uncheck_all(form_body){
  $(form_body).find(':checkbox').each(function(){
    $(this).prop('checked',false);
  });
}

function set_pgroup_admin_data(){
  var form_data = {
    data: $("#professional_group").serialize()
  };

  $.ajax({
    url: "php-functions/fncCommon.php?action=getProfGroupById",
    type: "POST",
    data: form_data,
    success: function (msg) {
      var pgroup = JSON.parse(msg);

      form_data = {
        data: "user_id=" + pgroup["pgroup_admin_id"]
      };

      $.ajax({
        url: "php-functions/fncCommon.php?action=getUserById",
        type: "POST",
        data: form_data,
        success: function (msg) {
          var user = JSON.parse(msg);

          $("#pgroup_contact").val(user["fname"] + " " + user["lname"]);
          $("#pgroup_mobile").val(user["mobile_num"]);
          $("#pgroup_email").val(user["email"]);
        }
      });
    }
  });
}


function set_pgroup_data() {
  $("#pi_id").empty();
  $("#pgroup_contact_id").empty();
  var form_data = {
    data: $("#professional_group").serialize()
  };

  $.ajax({
    url: "php-functions/fncCommon.php?action=getPIByProfGroupId",
    type: "POST",
    data: form_data,
    success: function (msg) {
      var p_investigators = JSON.parse(msg);

      for (var i = 0; i < p_investigators.length; i++) {

        var selected = '';
        if(pi_id_selected == p_investigators[i]['user_id'])
          selected = 'selected';

        $("#pi_id").append("<option value='" + p_investigators[i]['user_id'] + "' "+ selected +">" + p_investigators[i]['fname'] + " " + p_investigators[i]['lname'] + "</option>");
      }
    }
  });
  set_pgroup_admin_data();

  $.ajax({
    url: "php-functions/fncCommon.php?action=getProfGroupMembers",
    type: "POST",
    data: form_data,
    success: function (msg) {
      var json_arr = JSON.parse(msg);


      for (var i = 0; i < json_arr.length; i++) {

        var selected = '';

        if(proj_pg_contact == json_arr[i]['user_id'])
        selected = 'selected';

        $("#pgroup_contact_id").append("<option value='" + json_arr[i]['user_id'] + "'  "+ selected +">" + json_arr[i]['fname'] + " " + json_arr[i]['lname'] + "</option>");
      }

    }
  });

}
