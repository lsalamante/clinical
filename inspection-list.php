<?php
include('jp_library/jp_lib.php');
require("php-functions/fncCommon.php");
require("php-functions/fncProjects.php");
require("php-functions/fncVolunteers.php");
require("php-functions/fncStatusLabel.php");

if (!isset($_SESSION['user_id'])) {
  header("Location: " . "index.php");
  die();
}

// ------------- Start get all projects assignment (fncCommon.php, fncProjects.php, fncStatusLabel.php) ------------- //
$projects =array();
foreach (getAllProjects(0) as $proj) {

	// array_push($projects, $proj["project_id"]);
  $projects['project_id'] = $proj['project_id'];
  $projects['status_num'] = $proj['status_num'];

}
// ------------- End get all projects assignment ------------- //

?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<?php include('header.php'); ?>

<body>
  <section id="container">
    <!--header start-->
    <header class="header white-bg">
      <?php
      if ($LEFT_SIDEBAR) {
        #echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
      }
      ?>
      <!--logo start-->
      <?php if ($LOGO) {
        include('logo.php');
      }
      ?>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <?php if ($NOTIFICATION) {
          include('notification.php');
        } ?>
        <!--  notification end -->
      </div>
      <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
      include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <style>
    span.required
    {
      margin-left: 3px;
      color: red;
    }
    </style>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
          <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
              <li><a href="training-list"><i class="fa fa-home"></i> <?php echo $phrases['inspection']?></a></li>
            </ul>
            <!--breadcrumbs end -->
          </div>
        </div>


        <section class="panel">
          <?php include("inspection.php"); ?>
      	</section>
      <!-- page end-->

  </section>
  <!--main content end-->
  <!-- Right Slidebar start -->
  <?php
  if ($RIGHT_SIDEBAR) {
    include('right-sidebar.php');
  }
  ?>
  <!-- Right Slidebar end -->
  <!--footer start-->
  <?php include('footer.php'); echo "Rai"; ?>
  <!--footer end-->
</section>
<?php $PICKERS = TRUE; ?>
<?php include('scripts.php'); ?>





<!-- Start of Add Volunteer Scripts  @Rai-->

<script type="text/javascript" src="js-functions/inspection.js"></script>

<!-- End of Add Volunteer Scripts  @Rai-->


</body>
</html>
