<!-- Add Inspection per Project @Rai-->
<?php
if($PAGE_NAME == 'pg_audit.php'){
	$projects =array();
	foreach (getAllProjects(0) as $proj) {

		// array_push($projects, $proj["project_id"]);
		$projects['project_id'] = $proj['project_id'];
		$projects['status_num'] = $proj['status_num'];

	}

	$_project_ = getProjectById(false, false);
}

?>
<?php require("php-functions/fncInspection.php"); ?>

<!--

Show add btn if there's at least 1 drug record, bio management (see $bio from above), side effect (side-effect.php), stage summary

-->
<?php if( $projects['status_num'] <= 15 && isset($_GET["project_id"]) && $_GET["project_id"] != "" && ($_SESSION["user_type"] == "QC" || $_SESSION["user_type"] == "TFO" || $_SESSION["user_type"] == "A" || $_SESSION["user_type"] == "I")){ ?>

	<a href="#" data-toggle="modal" class="btn btn-primary adder" id="addInspection"><?php echo $phrases['add'] ?></a>

	<?php } ?>


	<?php

	$project_id = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? $_GET["project_id"] : 0;
	//$projects - were from inspection-list.php
	$stageInspection = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? getInspection("list", $project_id) : getInspection("listAll", $project_id, $projects); //make way for inspection list


	$count = 0;

	if(count($stageInspection) > 0){ // check if found record/s and make way for inpection list.
		?>

		<table class="table table-hover">
			<thead>
				<tr>
					<th><?php echo $phrases['number']?></th>
					<th><?php echo $phrases['project_name']?></th>
					<th><?php echo $phrases['execution_date']?></th>
					<th><?php echo $t->tryTranslate('executor'); ?></th>
					<th><?php echo $phrases['uploader']?></th>
					<th><?php echo $phrases['type']?></th>
					<th><?php echo $phrases['description']?></th>
				</tr>
			</thead>

			<tbody id="">
				<?php

				$count = 0;

				foreach ($stageInspection as $inspection) {

					$count++;

					$inspection_proj = getProjectById(0, $inspection['project_id']);
					$inspection_proj_name = $inspection_proj['project_name'];

					?>
					<tr onclick="getInspection(<?php echo $inspection['inspection_id']; ?>); return false;" style="cursor:pointer;">
						<td><?php echo $count ?></td>
						<td><?php echo $inspection_proj_name ?></td> <!--add project name -->
						<td><?php echo $inspection['report_date']; ?></td>
						<td><?php echo $inspection['executor']; ?></td>
						<td><?php echo $inspection['uploaded_by']; ?></td>
						<td><?php echo $phrases[$inspection['type']]; ?></td>
						<td><?php echo $inspection['report_description']; ?></td>

					</tr>

					<?php } ?>
				</tbody>
			</table>
			<?php if(isset($_project_))if($_project_['status_num'] == 15){ ?>
				<form id="audit_form">
					<div class="row">
						<input hidden id="status" name="status" value="1">
						<input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
						<input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
					</div>
					<section class="panel negative-panel">
						<div class="panel-body">
							<button type="" class="btn btn-primary" id="status_submit" ><?php echo $phrases['submit']?></button>
						</div>
					</section>
				</form>
				<?php }?>

				<?php }else{ // no record/s found ?>

					<div class="col-lg-12 text-center">
						<img src="img/sad.png" class="sad">
						<h1 class="sad"><?php echo $phrases['empty_table_data']?></h1>
					</div>

					<?php } ?>

					<!-- Start of Add Stage Inspection Modal  @Rai-->

					<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addInspectionModal" class="modal fade " data-backdrop="static" data-keyboard="false">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
									<h4 class="modal-title" id="modalTitleAddInspection"><?php echo $phrases['inspection']; ?></h4>

								</div>
								<div class="modal-body">

									<form action="#" id="addInspectionForm">

										<div class="form-group" id="type_div">
											<label for="exampleInputEmail1"><?php echo $phrases['type']?></label>
											<select name="type" id="type" class="form-control">
												<option value="" disabled><?php echo $phrases['select']?></option>
												<option value="inspector"><?php echo $phrases['inspector']?></option>
												<option value="on_site_inspector"><?php echo $phrases['on_site_inspector']?></option>
												<?php //if($_SESSION['user_type'] == 'I' && $project['status_num'] == 15){?>
												<?php //} ?>
											</select>
											<p class="help-block" style="display:none;" id="type_msg"></p>
										</div>

										<div class="form-group" id="executor_div">
											<label><?php echo $t->tryTranslate("executor"); ?></label>
											<input type="text" class="form-control" id="executor" name="executor" placeholder="">
											<p class="help-block" style="display:none;" id="executor_msg"></p>
										</div>

										<div class="form-group" id="report_date_div">
											<label for="exampleInputEmail1"><?php echo $phrases['execution_date']?></label>
											<input type="text" class="form-control" id="report_date" name="report_date" placeholder="">
											<p class="help-block" style="display:none;" id="report_date_msg"></p>
										</div>

											<!-- comment this out because this should be automatic -->
											<!-- <div class="form-group" id="role_div">
											<label for="exampleInputEmail1">Role</label>
											<select name="role" id="role" class="form-control">
											<option value="">Select</option>
											<option value="Inspector">Inspector</option>
											<option value="On-site inspector">On-site inspector</option>
										</select>
										<p class="help-block" style="display:none;" id="role_msg"></p>
									</div> -->

									<div class="form-group" id="report_description_div">
										<label for="exampleInputEmail1"><?php echo $phrases['description']?></label>
										<textarea name="report_description" id="report_description" class="form-control"></textarea>
										<p class="help-block" style="display:none;" id="report_description_msg"></p>
									</div>

									<div class="form-group" id="report_div">
										<label for="exampleInputEmail1"><?php echo $phrases['upload_report']?></label>
										<input type="file" class="form-control" id="report" name="report[]" placeholder="" multiple>
										<p class="help-block" style="display:none;" id="report_msg"></p>
										<br>
										<ul style="display:none;" id="reportList"> </ul>

									</div>

									<div class="form-group">
						            <input type="checkbox" name="databank" value="1" id="ins_databank">
						            <label for="ins_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
						          </div>

									<!-- Start hidden fields @Rai-->

									<input type="hidden" name="inspection_id" id="inspection_id" value="">
									<input type="hidden" name="project_idI" id="project_idI" value="<?php echo isset($_GET["project_id"]) && $_GET["project_id"] ? $_GET["project_id"] : ''; ?>">

									<!-- End hidden fields @Rai-->

									<?php if( isset($_GET["project_id"]) && $_GET["project_id"] != "" && ($_SESSION["user_type"] == "QC" || $_SESSION["user_type"] == "TFO" || $_SESSION["user_type"] == "A"|| $_SESSION["user_type"] == "I" )){ ?>
										<button type="submit" class="btn btn-primary" id="submitInspection"><?php echo $phrases['submit']?></button>
										<?php } ?>

									</form>

								</div>
							</div>
						</div>

						<!-- End of Add Stage Inspection Modal  @Rai-->
