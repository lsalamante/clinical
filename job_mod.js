var job_form = $("#job_form");
var job_add = $("#job_add");
var job_chk_all = $("#job_chk_all");
var job_del = $("#job_del");
var job_file = $("#job_file");

ajax_load(data, 'job_body.php', 'job_body');

job_add.on('click', function() {
  job_form[0].reset();
});

job_chk_all.change(function(){
  this.checked ? check_all(job_body) :  uncheck_all(job_body);
});

job_form.on('submit', function (e){
  e.preventDefault();
  add_job_info();
});

job_del.on('click', function(){
  $("#job_body input:checkbox:checked").each(function() {
    var _id = $(this).attr('id');
    _id = _id.split('-');
    //                      #push the number only
    del_arr.push(_id[1]);
  });

  var form_data = {
    data: "del_arr=" + del_arr.join(",")
  };

  $.ajax({
    url: "php-functions/fncCommon.php?action=delUpInArray",
    type: "POST",
    data: form_data,
    success: function (msg) {

      if(msg == 1){
        //                                    # Clear our array
        del_arr = [];

        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);

        //                                  #We reload the table
        ajax_load(data, 'job_body.php', 'job_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);

      }
      job_chk_all.prop('checked', false);
    }
  });
});

function add_job_info(){

  var form_data = new FormData(job_form[0]);
  form_data.append('project_id', "<?php echo $_GET['project_id'] ?>");
  form_data.append('sub_dir', 'job_specifications');
  form_data.append('role_id', '<?php echo $_SESSION['role_id'] ?>');
  form_data.append('up_type', '5'); /* NOTE: up_type 5 for job spec */

  $.ajax({
    url: "php-functions/fncCommon.php?action=uploadFile",
    type:'POST',
    processData: false,
    contentType: false,
    data:form_data,
    success:function(msg){
      if(msg == 1){
        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
        //                                  #We reload the table
        ajax_load(data, 'job_body.php', 'job_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });
}
