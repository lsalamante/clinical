<section class="panel negative-panel">
  <header class="panel-heading">
    <br>
    <?php if($project['status_num'] >= 13 && $_SESSION['user_type'] == 'I'){
      // disable for other users
    ?>
    <a href="#job_modal" data-toggle="modal" class="btn btn-xs btn-primary" id="job_add"><?php echo $phrases['add']?></a>
    <button type="submit" class="btn btn-danger btn-xs" id="job_del"><?php echo $phrases['delete']?></button>
    <?php }  ?>
  </header>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>
          <input type="checkbox" id="job_chk_all" />
        </th>
        <th><?php echo $phrases['number'] ?></th>
        <th><?php echo $phrases['title'] ?></th>
        <th><?php echo $phrases['version'] ?></th>
        <th><?php echo $phrases['version_date'] ?></th>
        <th><?php echo $phrases['description'] ?></th>
        <th><?php echo $phrases['file'] ?></th>
      </tr>
    </thead>
    <tbody id="job_body">

    </tbody>
  </table>
</section>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="job_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $phrases['add_job_spec'] ?></h4>
      </div>
      <div class="modal-body">

        <form role="form" id="job_form" enctype="multipart/form-data">
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo $phrases['title'] ?></label>
            <input type="text" class="form-control" id="job_name" name="up_name" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['version'] ?></label>
            <input type="number" min="0" step="0.1" class="form-control" id="job_ver" name="up_ver" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['description'] ?></label>
            <textarea class="form-control v-resize-only" id="job_desc" name="up_desc" placeholder=""></textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputFile"><?php echo $phrases['select_file'] ?></label>
            <input type="file" id="job_file" name="up_file[]" multiple="" required>
          </div>
          <div class="form-group">
            <input type="checkbox" name="databank" value="1" id="job_databank">
            <label for="job_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
          </div>
          <button type="" class="btn btn-primary make-loading" id="job_submit"><?php echo $phrases['submit'] ?></button>
        </form>
      </div>
    </div>
  </div>
</div>
