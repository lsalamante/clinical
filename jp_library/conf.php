<?php
$LOCAL_MODE = true; //SET TO TRUE ONLY IN LOCAL COMPUTER

#MODULE CONTROLS
$AVATAR = true;
$LEFT_SIDEBAR = true;
$RIGHT_SIDEBAR = false;
$SEARCH = false;
$LANGUAGE = true;
$NOTIFICATION = true;
$LOGO = true;

# ENCRYPTION FOR INTEGER $_GET VARIABLES
$ENCRYPT_GET = 2312454123;

#DATETIME SETTINGS
date_default_timezone_set('Asia/Manila');

#DEFAULT MODULE INITIALIZATIONS
#OVERRIDE ON YOUR PAGE IF NECESSARY
$DYNAMIC_TABLE = false;
$PICKERS = false; #FOR TIMEPICKERS, DATEPICKERS, ETC

#SITE CONFIG
#$SITE_NAME = '临床研究管理系统';
$SITE_NAME = '科群迈谱 [CTRIMAP]';
$LOGO_NAME = '<b>科群迈谱</b>[CTRIMAP] <span style="font-size:20px">临床试验研究管理平台</span>';

#URL CONTROLS
$PAGE_NAME = basename($_SERVER['PHP_SELF']);

if ($LOCAL_MODE){
    $BASE_URL = "http://localhost/clinical/";
    $GLOBALS['base_url'] = "http://localhost/clinical/";
}
else{
    $BASE_URL = "http://betaprojex.com/clinical/";
    $GLOBALS['base_url'] = "http://betaprojex.com/clinical/";
}

#DEPENDENCIES
require __DIR__ . '/../PHPMailer-master/PHPMailerAutoload.php';
require __DIR__ . "/../helpers/Translation.php";
require __DIR__ . "/../helpers/Notification.php";
require __DIR__ . "/../helpers/DoubleCheck.php";
require __DIR__ . "/../helpers/NotificationFactory.php";
require __DIR__ . "/../helpers/User.php";
require __DIR__ . "/../helpers/Log.php";
require __DIR__ . "/../helpers/Project.php";
require __DIR__ . "/../helpers/Status.php";
require __DIR__ . "/../helpers/CSV.php";
require __DIR__ . "/../helpers/Databank.php";


use \PHPMailer as PHPMailer;

use Clinical\Helpers\Notification;
use Clinical\Helpers\DoubleCheck;
use Clinical\Helpers\NotificationFactory;
use Clinical\Helpers\Translation;
use Clinical\Helpers\User;
use Clinical\Helpers\Log;
use Clinical\Helpers\Project;
use Clinical\Helpers\Status;
use Clinical\Helpers\CSV;
use Clinical\Helpers\Databank;
