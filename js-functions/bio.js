// function getBioDetails(bio_id, event)
// {
//   if(event.target.nodeName != "A" && event.target.nodeName != "I")
//   {
//     $('#bio_id_update').val(bio_id);
//     $.ajax({
//       url:"php-functions/fncBioSamples.php?action=getBioDetails",
//       type: "POST",
//       dataType: "json",
//       data: { "bio_id" : bio_id, "type" : "ajax" },
//       success:function(data){
//
//
//         //DOUBLE CHECK STUFF
//         var payload = JSON.parse(data[0].dbl_chk_payload);
//
//         var fields_arr = {
//           "name" : "#bgc_name",
//           'quantity' : '#bgc_quantity',
//           'collect_start' : '#bio_collect_start',
//           'collect_end' : '#bio_collect_end',
//           'description' : '#bgc_description',
//           'collect_record' : '#update_collect_record',
//         }
//
//         $(".dbl_chk").remove();
//         $("#bgc_dbl_chk_id").val(data[0].bio_id);
//
//  //todo: disable comments when pass is clicked
//         for(var key in fields_arr){
//           if(payload[key]['is_checked'] == 0){
//             $(fields_arr[key]).after('<a href="#bgc_dbl_chk_modal" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="bgcSetPayload(this)" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-question"></i> 复核</a>');
//           }else if(payload[key]['is_checked'] == -1){
//             $(fields_arr[key]).after('<a href="#bgc_dbl_chk_modal" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="bgcSetPayload(this)" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-times"></i> 复核不通过</a>');
//           }else if(payload[key]['is_checked'] == 1){
//             $(fields_arr[key]).after('<a href="#bgc_dbl_chk_modal" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="bgcSetPayload(this)" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-check"></i> 复核通过</a>');
//           }
//         }
//
//         //DOUBLE CHECK STUFF END
//
//         $('#bgc_submit').html('更新');
//         // $('#collect_record').attr('style', 'display:none');
//         $('#process_record').attr('style', 'display:none');
//         $('#bgc_name').val(data[0].name);
//         $('#bgc_quantity').val(data[0].quantity);
//         $('#bio_collect_start').val(data[0].collect_start.substring(0,10));
//         $('#bio_collect_end').val(data[0].collect_end.substring(0,10));
//         $('#bgc_description').val(data[0].description);
//         $('#update_collect_record').attr('href', data[0].collect_record);
//         $('#update_collect_record').attr('style', "display:inline");
//         // $('#update_process_record').attr('href', data[0].process_record);
//         // $('#update_process_record').attr('style', "display:inline");
//         $('#bgc_modal').modal('show');
//       }
//     });
//   }
// }
//
// function rmDoubleCheck(){
//   $(".dbl_chk").remove();
// }
//
// function getTransportDetails(transport_id, event)
// {
//   if(event.target.nodeName != "A" && event.target.nodeName != "I")
//   {
//     $('#transport_id_update').val(transport_id);
//     $.ajax({
//       url:"php-functions/fncBioTransport.php?action=getBioDetails",
//       type: "POST",
//       dataType: "json",
//       data: { "transport_id" : transport_id, "type" : "ajax" },
//       success:function(data){
//
//         $('#transport_submit').html('更新');
//         $('#process_record').attr('style', 'display:none');
//         $('#transport_name').val(data[0].name);
//         $('#transport_quantity').val(data[0].quantity);
//         $('#transport_company').val(data[0].transport_company);
//         $('#transport_temp').val(data[0].transport_temp);
//         $('#transport_humidity').val(data[0].transport_humidity);
//         $('#transport_start').val(data[0].transport_start.substring(0,10));
//         $('#transport_end').val(data[0].transport_end.substring(0,10));
//         $('#transport_description').val(data[0].description);
//         $('#update_transport_process_record').attr('href', data[0].process_record);
//         $('#update_transport_process_record').attr('style', "display:inline");
//         $('#transport_modal').modal('show');
//       }
//     });
//   }
// }
//
// getDetermineDetails = function (determine_id, event)
// {
//   if(event.target.nodeName != "A" && event.target.nodeName != "I")
//   {
//     $('#determine_id_update').val(determine_id);
//     $.ajax({
//       url:"php-functions/fncBioDetermine.php?action=getBioDetails",
//       type: "POST",
//       dataType: "json",
//       data: { "determine_id" : determine_id, "type" : "ajax" },
//       success:function(data){
//         //FIXME: bug about clicking the row then adding new bio
//         $('#determine_submit').html('更新');
//         $('#determine_record').attr('style', 'display:none');
//         $('#determine_report').attr('style', 'display:none');
//         $('#determine_name').val(data[0].name);
//         $('#determine_quantity').val(data[0].quantity);
//         $('#determine_company').val(data[0].determine_company);
//         $('#determine_start').val(data[0].determine_start.substring(0,10));
//         $('#determine_end').val(data[0].determine_end.substring(0,10));
//         $('#determine_description').val(data[0].description);
//         $('#update_determine_record').attr('href', data[0].determine_record);
//         $('#update_determine_record').attr('style', "display:inline");
//         $('#update_determine_report').attr('href', data[0].determine_report);
//         $('#update_determine_report').attr('style', "display:inline");
//         $('#determine_modal').modal('show');
//       }
//     });
//   }
// }
//
// getBioHandleDetails = function (bio_handle_id, event)
// {
//   if(event.target.nodeName != "A" && event.target.nodeName != "I")
//   {
//     $('#bio_handle_id_update').val(bio_handle_id);
//     $.ajax({
//       url:"php-functions/fncBioHandles.php?action=getBioDetails",
//       type: "POST",
//       dataType: "json",
//       data: { "bio_handle_id" : bio_handle_id, "type" : "ajax" },
//       success:function(data){
//         $('#bio_handle_submit').html('更新');
//         $('#handle_record').attr('style', 'display:none');
//         $('#process_record').attr('style', 'display:none');
//         $('#bio_handle_name').val(data[0].name);
//         $('#bio_handle_quantity').val(data[0].quantity);
//         $('#handle_start').val(data[0].handle_start.substring(0,10));
//         $('#handle_end').val(data[0].handle_end.substring(0,10));
//         $('#bio_handle_description').val(data[0].description);
//         $('#update_handle_record').attr('href', data[0].handle_record);
//         $('#update_handle_record').attr('style', "display:inline");
//         $('#bio_handle_modal').modal('show');
//       }
//     });
//   }
// }
//
// getBioSaveDetails = function (bio_save_id, event)
// {
//   if(event.target.nodeName != "A" && event.target.nodeName != "I")
//   {
//     $('#bio_save_id_update').val(bio_save_id);
//     $.ajax({
//       url:"php-functions/fncBioSave.php?action=getBioSaveDetails",
//       type: "POST",
//       dataType: "json",
//       data: { "bio_save_id" : bio_save_id, "type" : "ajax" },
//       success:function(data){
//         $('#bio_save_form')[0].reset();
//         $('#bio_save_submit').html('更新');
//         $('#bio_save_name').val(data[0].name);
//         $('#bio_save_quantity').val(data[0].quantity);
//         $('#bio_save_collect_start').val(data[0].collect_start.substring(0,10));
//         $('#bio_save_collect_end').val(data[0].collect_end.substring(0,10));
//         $('#bio_save_address').val(data[0].address);
//         $('#bio_save_temp').val(data[0].temp);
//         $('#bio_save_humidity').val(data[0].humidity);
//         $('#bio_save_record_a').attr('href', data[0].record);
//         $('#bio_save_record_a').attr('style', 'display:inline');
//         $('#bio_save_desc').val(data[0].bio_desc);
//         $('#bio_save_id_update').val(bio_save_id);
//         $('#bio_save_modal').modal('show');
//       }
//     });
//   }
// }
//
// getBioRecycleDetails = function (bio_recycle_id, event)
// {
//   if(event.target.nodeName != "A" && event.target.nodeName != "I")
//   {
//     $('#bio_recycle_id_update').val(bio_recycle_id);
//     $.ajax({
//       url:"php-functions/fncBioRecycle.php?action=getBioRecycleDetails",
//       type: "POST",
//       dataType: "json",
//       data: { "bio_recycle_id" : bio_recycle_id, "type" : "ajax" },
//       success:function(data){
//         $('#bio_recycle_form')[0].reset();
//         $('#bio_recycle_submit').html('更新');
//         $('#bio_recycle_name').val(data[0].name);
//         $('#bio_recycle_quantity').val(data[0].quantity);
//         $('#bio_recycle_sending_party').val(data[0].sending_party);
//         $('#bio_recycle_sending_operator').val(data[0].sending_operator);
//         $('#bio_recycle_sending_date').val(data[0].sending_date);
//         $('#bio_recycle_shipper').val(data[0].shipper);
//         $('#bio_recycle_shipper_operator').val(data[0].shipper_operator);
//         $('#bio_recycle_shipper_date').val(data[0].shipper_date);
//         $('#bio_recycle_recycler').val(data[0].recycler);
//         $('#bio_recycle_recycler_operator').val(data[0].recycler_operator);
//         $('#bio_recycle_recycler_date').val(data[0].recycler_date);
//         $('#bio_recycle_record_a').attr("href", data[0].recycle_record);
//         $('#bio_recycle_record_a').attr("style", "display:inline");
//         $('#bio_recycle_desc').val(data[0].bio_desc);
//         $('#bio_recycle_modal').modal('show');
//       }
//     });
//   }
// }
//
// getBioDestroyDetails = function (bio_destroy_id, event)
// {
//   if(event.target.nodeName != "A" && event.target.nodeName != "I")
//   {
//     $('#bio_destroy_id_update').val(bio_destroy_id);
//     $.ajax({
//       url:"php-functions/fncBioDestroy.php?action=getBioDestroyDetails",
//       type: "POST",
//       dataType: "json",
//       data: { "bio_destroy_id" : bio_destroy_id, "type" : "ajax" },
//       success:function(data){
//         $('#bio_destroy_form')[0].reset();
//         $('#bio_destroy_submit').html('更新');
//         $('#bio_destroy_name').val(data[0].name);
//         $('#bio_destroy_quantity').val(data[0].quantity);
//         $('#bio_destroy_date').val(data[0].bio_date);
//         $('#bio_destroy_address').val(data[0].destroy_address);
//         $('#bio_destroy_operator').val(data[0].destroy_operator);
//         $('#bio_destroy_record_a').attr("href", data[0].destroy_record);
//         $('#bio_destroy_record_a').attr("style", "display:inline");
//         $('#bio_destroy_desc').val(data[0].bio_desc);
//         $('#bio_destroy_modal').modal('show');
//       }
//     });
//   }
// }
