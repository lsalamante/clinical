window.onload = function() {


	$("#addDrugRecord").on("click", function (event){
		$("#modalTitleAddRecord").html($('#for_translate_add_record').val());
		$("#submitRecord").html($('#for_translate_submit').val());
		$('#addRecordModal').modal('show');
		$('#addDrugRecordForm')[0].reset();
		$('#receiptList').hide().html('');

		$('#drug_record_id').val('');




	});

	$("#addDrugRecordForm").on('submit', function (event) { // Submit Drug Form

		var bValid = true;
		var fields = new Array();

		fields['drug_idR'] = {'type' : 'dropdown', 'label' : 'Drug'}; /*
		fields['quantity'] = {'type' : 'numeric', 'label' : 'Quantity'};
		fields['unit'] = {'type' : 'dropdown', 'label' : 'Unit'}; */
		fields['record_type'] = {'type' : 'dropdown', 'label' : 'Record Type'};
		fields['date'] = {'type' : 'input', 'label' : 'Date'};
		fields['actor1'] = {'type' : 'input', 'label' : 'Actor User One'};
		fields['position1'] = {'type' : 'dropdown', 'label' : 'Position for Actor User One'};
		fields['actor2'] = {'type' : 'input', 'label' : 'Actor User Two'};
		fields['position2'] = {'type' : 'dropdown', 'label' : 'Position for Actor User Two'};
		fields['description'] = {'type' : 'input', 'label' : 'Description'};

		bValid = bValid && checkErrors(fields);

		if($("#quantity").val() == "" || $("#quantity").val() < 1 || $("#unit").val() == ""){

			$("#quantity_div").addClass('has-error');
	        $("#quantity_msg").show().html('您输入的日期范围无效。');
	        $('body, html, #quantity_div').scrollTop($("#quantity_div").offset().top - 100);

	        bValid = bValid && false;

		}

	    if(bValid){

			var formData = new FormData();

	        var other_data = $('#addDrugRecordForm').serializeArray();
	        $.each(other_data,function(key,input){
	            formData.append(input.name, input.value);
	        });

	        $.each($("input[type=file]"), function(i, obj) {
		        $.each(obj.files,function(j, file){
		            formData.append('receipt['+j+']', file);
		        })
			});

			$("#submitRecord").prop('disabled', true).html('载入中...');
			$.ajax({
				url:"php-functions/fncDrugRecord.php?action=addDrugRecord",
				type: "POST",
	    		dataType: "json",
	        	data: formData,
	        	processData: false,
            	contentType: false,
				success:function(data){

					$("#submitRecord").prop('disabled', false).html($('#for_translate_submit').val());

					if(data.type == "success")
						show_alert(data.msg, 'pg_audit.php?tab=drug-records&project_id='+$("#project_idR").val(), 1);
					else
						show_alert(data.msg, '', 0);

				}
			});

		}

		event.preventDefault();

	});




}

function getDrugRecordDetails(drug_record_id){

	$.ajax({
		url:"php-functions/fncDrugRecord.php?action=getDrugRecords",
		type: "POST",
		dataType: "json",
		data: { "type" : "ajax", "drug_record_id" : drug_record_id, "project_id" : $("#project_idR").val() },
		success:function(data){

			var dialog = $('#addRecordModal').modal();
			    dialog.removeData('bs.modal');
			    dialog.modal("show");

			$("#modalTitleAddRecord").html('药品记录');
			$("#submitRecord").html($('#for_translate_update').val());

			$('#drug_record_id').val(data[0].drug_record_id);
			$('#drug_idR').val(data[0].drug_id);
			$('#quantity').val(data[0].quantity);
			$('#unit').val(data[0].unit);
			$('#record_type').val(data[0].record_type);
			$('#date').val(data[0].date);
			$('#actor1').val(data[0].actor1);
			$('#position1').val(data[0].position1);
			$('#actor2').val(data[0].actor2);
			$('#position2').val(data[0].position2);
			$('#description').val(data[0].description);

			// --------- Start of display receipts -----------//
			var receiptList = "";

			if(data[0].receipt_file.length > 0){

				for(var i = 0; i < data[0].receipt_file.length; i++){

					receiptList += "<li><a href='drug-records/"+data[0].receipt_file[i]+"'>"+data[0].receipt[i]+"</a></li>";

				}
			}

			$("#receiptList").show().html(receiptList);
			// --------- End of display receipts -----------//

		}
	});

}
