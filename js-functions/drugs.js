// $(document).ready(function () {
//
// 	$("#bday").datepicker();
//
// 	$("#validity_from").datepicker();
// 	$("#validity_to").datepicker();
//
// 	$('#drugListAdd').on('click', function () { // retrieved drug list
//
// 		$("#modalId").val('drug');
//
// 		$(".modal-backdrop").remove();
// 	   	var dialog = $('#drugListModal').modal();
// 		    dialog.removeData('bs.modal');
// 		    dialog.modal("show");
//
// 	    getDrugs('list', $("#project_idR").val());
//
// 	});
//
// 	$('#addDrug').on('click', function () { // when add drug modal shows
// 
// 		$("#modalId").val('drug');
//
// 	   	$(".modal-backdrop").remove();
// 	   	var drugListModal = $('#drugListModal').modal();
// 	    	drugListModal.hide();
//
//
// 	   	var drugModal = $('#addDrugModal').modal();
// 		    drugModal.removeData('bs.modal');
// 		    drugModal.modal("show");
//
// 		    $("#modalTitleAddDrug").html($('#for_translate_add_drug').val());
// 		   	$('#drug_id').val('');
// 		   	$("#submitDrug").html($('#for_translate_submit').val());
//
// 		   	$("#addDrugForm")[0].reset();
//
//
// 	});
//
//
// 	$('#addDrugModal').on('hidden.bs.modal', function () { // Show drug list modal when add drug modal closed
//
// 		$(".modal-backdrop").remove();
//
// 		if($("#modalId").val() != "record"){
//
// 		   	var dialog = $('#drugListModal').modal();
// 		   	getDrugs('list', $("#project_idR").val());
//
// 		}else{
//
// 			var dialog = $('#addRecordModal').modal();
// 			getDrugs('option', $("#project_idR").val());
//
// 		}
//
//
// 			dialog.removeData('bs.modal');
// 			dialog.modal("show");
// 	});
//
//
// 	$("#addDrugForm").on('submit', function (event) { // Submit Drug Form
//
// 		var bValid = true;
// 		var fields = new Array();
//
// 		fields['trade_name'] = {'type' : 'input', 'label' : 'Trade Name'};
// 		fields['chemical_name'] = {'type' : 'dropdown', 'label' : 'Chemical Name'};
// 		fields['english_name'] = {'type' : 'input', 'label' : 'English Name'};
// 		fields['drug_type'] = {'type' : 'dropdown', 'label' : 'Drug Type'};
// 		fields['lot_number'] = {'type' : 'input', 'label' : 'Lot Type'};
// 		fields['specification'] = {'type' : 'input', 'label' : 'Specification'};
// 		// fields['inspection'] = {'type' : 'input', 'label' : 'Inspection Report'};
// 		fields['factory'] = {'type' : 'input', 'label' : 'Pharmaceutical Factory'};
// 		fields['provider'] = {'type' : 'input', 'label' : 'Provider'};
//
// 		bValid = bValid && checkErrors(fields);
//
// 		var startDate = $("#validity_from").val();
// 	    var endDate = $("#validity_to").val();
//
// 	    var startD = new Date(startDate + " 00:00:00");
// 	    var endD = new Date(endDate + " 00:00:00");
//
// 	    if(endD < startD || startDate == '' || endDate == ''){
//
// 	        $("#validity_div").addClass('has-error');
// 	        $("#validity_msg").show().html('您输入的日期范围无效。');
// 	        $('body, html, #validity_div').scrollTop($("#validity_div").offset().top - 100);
//
// 	        bValid = bValid && false;
// 	    }
//
// 	    //bValid = true;
// 		if(bValid){
//
// 			$("#submitDrug").prop('disabled', true).html('载入中...');
// 			var form_data = new FormData($('#addDrugForm')[0]);
// 			$.ajax({
// 				url:"php-functions/fncDrugs.php?action=addDrugs",
// 				type: "POST",
// 	    		dataType: "json",
// 	    		processData: false,
// 				contentType: false,
// 	        	// data: $("#addDrugForm").serialize(),
// 	        	data: form_data,
// 				success:function(data){
//
// 					$("#validity_div").removeClass('has-error');
//     				$("#validity_msg").empty().hide();
//
// 					$("#submitDrug").prop('disabled', false).html($('#for_translate_submit').val());
//
// 					$('#modalMsg').html(data.msg);
// 					setTimeout(function(){
// 						$('#modalMsg').empty();
// 					}, 5000);
//
// 					if(data.type == "success")
// 						$('#modalMsg').css({color:'rgba(41,160,50,1) '});
// 					else
// 						$('#modalMsg').css({color:'red'});
//
// 					$('#drugListModal').show();
// 		   			$("#addDrugModal").modal('hide');
//
// 				}
// 			});
//
// 		}
//
// 		event.preventDefault();
//
// 	});
//
// });
//
//
//
// function getDrugs(action, project_id){
// 	$("#drugList").html("正在加载列表...");
// 	$.ajax({
// 		url:"php-functions/fncDrugs.php?action=getDrugs",
// 		type: "POST",
// 		data:{ "type" : "ajax", "project_id" : project_id},
// 		dataType: "json",
// 		success:function(data){
//
// 			var drugList = action == "list" ? "" : "<option value=''>"+$('#for_translate_select').val()+"</option>";
// 			var count = 1;
// 			for(var i = 0; i < data.length; i++){
//
// 				if(action == "list"){
//
// 					drugList +=	"<tr onclick='getDrugDetails("+data[i].drug_id+", "+project_id+");' style='cursor:pointer;'>";
// 						drugList += "<td>"+count+"</td>";
// 						drugList += "<td>"+data[i].trade_name+"</td>";
// 						switch(data[i].drug_type){
// 							case 'tested_drug': data[i].drug_type = '受试药'; break;
// 							case 'drug_combination': data[i].drug_type = '合并用药'; break;
// 							case 'control_drug': data[i].drug_type = '对照药'; break;
// 							case 'placebo_drug_record': data[i].drug_type = '安慰剂'; break;
// 							case 'adjuvant_drug': data[i].drug_type = '辅助用药'; break;
// 							case 'other': data[i].drug_type = '其他'; break;
// 						}
// 						drugList += "<td>"+data[i].drug_type+"</td>";
// 						drugList += "<td>"+data[i].storage+"</td>";
// 						drugList += "<td>"+data[i].specification+"</td>";
// 						drugList += "<td>"+data[i].validity_from+" - "+data[i].validity_to+"</td>";
// 		            drugList +=	"</tr>";
//
// 		            count++;
//
// 		        }else
// 		        	drugList += "<option value='"+data[i].drug_id+"'>"+data[i].trade_name+"</option>";
//
//
// 			}
//
// 			if(action == "list")
// 				$("#drugList").html(drugList);
// 			else
// 				$("#drug_idR").html(drugList);
//
//
// 		}
// 	});
//
// }
//
// function getDrugDetails(drug_id, project_id){
//
// 	$.ajax({
// 		url:"php-functions/fncDrugs.php?action=getDrugs",
// 		type: "POST",
// 		dataType: "json",
// 		data: { "type" : "ajax", "drug_id" : drug_id, "project_id" : project_id },
// 		success:function(data){
//
// 			$('#addDrug').click();
// 			$("#modalTitleAddDrug").html('药品注册分类及剂型');
// 			$("#submitDrug").html('更新');
//
// 			$('#drug_id').val(data[0].drug_id);
// 			$('#trade_name').val(data[0].trade_name);
// 			$('#chemical_name').val(data[0].chemical_name);
// 			$('#english_name').val(data[0].english_name);
// 			$('#drug_type').val(data[0].drug_type);
// 			$('#lot_number').val(data[0].lot_number);
// 			$('#specification').val(data[0].specification);
// 			$('#inspection_upload').attr('style','display:inline;');
// 			$('#inspection_upload').attr('href', data[0].inspection);
// 			$('#factory').val(data[0].factory);
// 			$('#provider').val(data[0].provider);
// 			$("#validity_from").val(data[0].validity_from);
// 	    	$("#validity_to").val(data[0].validity_to);
//
// 		}
// 	});
//
// }
//
//
// function showAddNewDrug(){
//
// 	$("#modalId").val('record');
//
// 	$(".modal-backdrop").remove();
//    	var drugListModal = $('#addRecordModal').modal();
//     	drugListModal.hide();
//
//
//    	var drugModal = $('#addDrugModal').modal();
// 	    drugModal.removeData('bs.modal');
// 	    drugModal.modal("show");
//
// 	    $("#modalTitleAddDrug").html($('#for_translate_add_drug').val());
// 	   	$('#drug_id').val('');
// 	   	$("#submitDrug").html($('#for_translate_submit').val());
//
// 	   	$("#addDrugForm")[0].reset();
//
// }
