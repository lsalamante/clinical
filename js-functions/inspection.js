$(document).ready(function () {

	$("#report_date").datepicker();

	$("#addInspection").on("click", function (event){

		$("#modalTitleAddInspection").html('添加阶段检验报告');
		$("#submitInspection").html('提交');
		$('#addInspectionForm')[0].reset();
		$('#inspection_id').val('');

		$('#addInspectionModal').modal('show');

	});


	$("#addInspectionForm").on('submit', function (event) { // Submit Drug Form

		var bValid = true;
		var fields = new Array();

		fields['report_date'] = {'type' : 'input', 'label' : 'Date'};
		fields['role'] = {'type' : 'dropdown', 'label' : 'Role'};
		fields['type'] = {'type' : 'dropdown', 'label' : 'Type'};
		// fields['report_description'] = {'type' : 'input', 'label' : 'Description'};
		fields['report'] = {'type' : 'input', 'label' : 'Report Upload'};

		bValid = bValid && checkErrors(fields);


	    //bValid = true;
		if(bValid){

			var formData = new FormData();

	        var other_data = $('#addInspectionForm').serializeArray();
	        $.each(other_data,function(key,input){
	            formData.append(input.name, input.value);
	        });

	        $.each($("input[type=file]"), function(i, obj) {
		        $.each(obj.files,function(j, file){
		            formData.append('report['+j+']', file);
		        })
			});

			$("#submitInspection").prop('disabled', true).html('载入中...');
			$.ajax({
				url:"php-functions/fncInspection.php?action=addInspection",
				type: "POST",
	    		dataType: "json",
	        	data: formData,
	        	processData: false,
            	contentType: false,
				success:function(data){

					$("#submitInspection").prop('disabled', false).html('提交');

					if(data.type == "success")
						show_alert(data.msg, 'pg_audit.php?tab=inspection&project_id='+$("#project_idI").val(), 1);
					else
						show_alert(data.msg, '', 0);

				}
			});

		}

		event.preventDefault();

	});

});


function getInspection(inspection_id){

	$.ajax({
		url:"php-functions/fncInspection.php?action=getInspection",
		type: "POST",
		dataType: "json",
		data: { "type" : "ajax", "inspection_id" : inspection_id, "project_id" : $("#project_idI").val() },
		success:function(data){

			$("#modalTitleAddInspection").html('添加阶段检验报告');
			$("#submitInspection").html('提交');
			$('#addInspectionForm')[0].reset();
			$('#inspection_id').val('');

			$('#addInspectionModal').modal('show');

			$("#modalTitleAddInspection").html('检查报告');
			$("#submitInspection").html('更新');

			$('#inspection_id').val(data[0].inspection_id);
			$('#report_date').val(data[0].report_date);
			$('#role').val(data[0].role);
			$('#type').val(data[0].type);
			$('#executor').val(data[0].executor);
			$('#report_description').val(data[0].report_description);

			// --------- Start of display uploaded reports -----------//
			var reportList = "";

			if(data[0].inspection_file.length > 0){

				for(var i = 0; i < data[0].inspection_file.length; i++){

					reportList += "<li><a href='"+data[0].inspection_file[i]+"' target='_blank'>"+data[0].inspection[i]+"</a></li>";

				}
			}

			$("#reportList").show().html(reportList);
			// --------- End of display uploaded reports -----------//

		}
	});

}
