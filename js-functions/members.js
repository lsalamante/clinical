$(document).ready(function () {
	$("#form_add_member").on('submit', function (event) {
		$("#mem_submit").prop('disabled', true).html('载入中...');
		var form_data=new FormData($('#form_add_member')[0]);
	    $.ajax({
	      url:"php-functions/fncMembers.php?action=addProjectMembers",
	      type:"POST",
	      data:form_data,
	      processData: false,
	      contentType: false,
	      success:function(msg)
	      {
	        if(msg != 0)
	        {
	          // show_alert(msg, '', 1);
	          show_alert("您已成功添加成员", 'pg_audit.php?tab=member&project_id='+$("#project_idI").val(), 1);
	          /** PS: function show_alert(msg, redirect, type) --> footer.php 您已成功添加新帐号 **/
	        }
	        else
	        {
	          // show_alert(msg, '', 0);
	          show_alert("操作失败", '', 0);
	        }
	      }
	    });
		event.preventDefault();
	});
});
