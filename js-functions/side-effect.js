$(document).ready(function () {

	$("#sub_birthdate").datepicker();
	$("#report_time").datepicker();
	$('#addSideEffect').on('click', function (event) {
	var sae = $('#is_sae').val();
	var data_sae = "side";
	if(sae == 1)
	{
		data_sae = "sae";
	}
		$.ajax({
			url:"php-functions/fncSideEffect.php?action=addSideEffectBlank",
			type: "POST",
			dataType: "json",
			data: $('#addNewSE').serialize(),
			success:function(id){
				$('#side_effect_id_update').val(id);
				var data = {
					project_id:$('#project_idE').val(),
					side_effect:data_sae
				};
				ajax_load(data, "sideeffects_table_tab.php", "se_tbody");
				$('#addSideEffectModal').modal('show');
			}
		});

		$('#addSideEffectForm')[0].reset();
		$('#is_sae').val(sae);
		$('#submitSideEffects').html("提交");
		// $('#addSideEffectModal').modal('show');
	});

});

function add_side_effect(btn_clicked)
{
	var sae = $('#is_sae').val();
	var tab_get = "sideeffect";
	if(sae == 1)
	{
		tab_get = "sae";
	}
		$("#"+btn_clicked).prop('disabled', true).html('载入中...');
		$.ajax({
				url:"php-functions/fncSideEffect.php?action=addSideEffect",
				type: "POST",
	    		dataType: "json",
	        	data: $("#addSideEffectForm").serialize(),
				success:function(data){

					$("#"+btn_clicked).prop('disabled', false).html('提交');
					if(data == "1")
						show_alert("您已成功添加不良事件/反应记录", '', 1);
						// You have successfully added a Side Effect
						// show_alert("您已成功添加不良事件/反应记录", 'pg_audit.php?tab='+tab_get+'&project_id='+$("#project_idSE").val(), 1);
					else if(data == "2")
						show_alert("您已成功添加不良反应记录", '', 1);
						// You have successfully udpated a Side Effect
						// show_alert("您已成功添加不良反应记录", 'pg_audit.php?tab='+tab_get+'&project_id='+$("#project_idSE").val(), 1);
					else
						show_alert(data, '', 0);

					var sae = $('#is_sae').val();
					var data_sae = "side";
					if(sae == 1)
					{
						data_sae = "sae";
					}
					var data = {
						project_id:$('#project_idE').val(),
						side_effect:data_sae
					};
					ajax_load(data, "sideeffects_table_tab.php", "se_tbody");

				}
			});
		event.preventDefault();

}

$('#se_docs_modal').on('hidden.bs.modal', function () { // Show side effects main modal when add document is closed
	$('#addSideEffectModal').modal('show');
});

$('#se_docs_form').on('submit', function (e){
  e.preventDefault();
  add_se_document();
});

function add_se_document()
{
  var form_data = new FormData($('#se_docs_form')[0]);
  form_data.append('side_effect_id', $('#side_effect_id_update').val());
  form_data.append('sub_dir', 'side_effect_documents');
  // form_data.append('role_id', '<?php echo $_SESSION['role_id'] ?>');
  // form_data.append('up_type', '1'); /* up_type 1 for PD */

  $.ajax({
    url: "php-functions/fncCommon.php?action=uploadFile",
    type:'POST',
    processData: false,
    contentType: false,
    data:form_data,
    success:function(msg){
      if(msg == 1){
        // show_alert("Operation successful.", "", 1);
        // #We reload the table
        var side_effect_id_ajax = {
        	"side_effect_id":$('#side_effect_id_update').val()
        }
        $('#se_docs_modal').modal('hide');
        ajax_load(side_effect_id_ajax, 'table_side_effect_upload.php', 'table_side_effect_upload');
      }else{
        show_alert(msg, "", 0);
      }
    }
  });
}
