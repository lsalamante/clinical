// $(document).ready(function () {
//
//
//
// 	$("#time_slot_from").datepicker();
// 	$("#time_slot_to").datepicker();
//
// 	$("#addSummary").on("click", function (event){
//
// 		$("#modalTitleAddSummary").html('阶段小结');
// 		$("#submitSummary").html('提交');
// 		$('#addSummaryForm')[0].reset();
// 		$('#stage_summary_id').val('');
//
// 		$('#addSummaryModal').modal('show');
//
// 	});
//
// 	$("#addUploadS").on("click", function (event){
//
// 		$(".modal-backdrop").remove();
// 	   	var drugListModal = $('#addSummaryModal').modal();
// 	    	drugListModal.hide();
//
// 	    $("#summaryUploadForm")[0].reset();
//
// 	   	var dialog = $('#summaryUploadModal').modal();
// 		    dialog.removeData('bs.modal');
// 		    dialog.modal("show");
//
// 	});
//
//
//
// 	$("#submitMaterialS").on("click", function (event){
//
// 		$("#up_fileS_div").removeClass('has-error');
// 		$("#up_fileS_msg").hide().html('');
//
// 		bValid = true;
// 		var fields = new Array();
//
// 		fields['nameS'] = {'type' : 'input', 'label' : 'Name'};
// 		fields['versionS'] = {'type' : 'numeric', 'label' : 'Version'};
// 		fields['descriptionS'] = {'type' : 'input', 'label' : 'Description'};
// 		fields['up_fileS'] = {'type' : 'input', 'label' : 'Materials'};
// 		bValid = bValid && checkErrors(fields);
//
// 		if(bValid){
//
// 			var count = parseInt($("#countS").val()) + 1;
// 			var databank = $('#stage_databank').val();
// 			var uploadFormData = new FormData();
//
// 			uploadFormData.append("count", count);
// 			if(document.getElementById('stage_databank').checked)
// 			{
// 				uploadFormData.append("databank", databank);
// 			}
// 			$.each($("#up_fileS"), function(i, obj) {
// 		        $.each(obj.files,function(j, file){
// 		            uploadFormData.append('materials['+j+']', file);
// 		        })
// 			});
//
// 			$("#submitMaterialS").prop('disabled', true).html('载入中...');
// 			$.ajax({
// 				url:"php-functions/fncStageSummary.php?action=uploadMaterial",
// 				type: "POST",
// 	    		dataType: "json",
// 	        	data: uploadFormData,
// 	        	processData: false,
// 	        	contentType: false,
// 				success:function(data){
//
// 					$("#submitMaterialS").prop('disabled', false).html('Submit');
//
// 		        	$("#up_fileS_msg").show().html(data.msg);
//
// 					if(data.type == "success"){
//
// 						var uploadList = '<tr>';
// 								uploadList += '<td>'+count+'</td>';
// 								uploadList += '<td>'+$('#nameS').val()+'</td>';
// 								uploadList += '<td>'+$('#versionS').val()+'</td>';
// 								uploadList += '<td>'+data.version_date+'</td>';
// 								uploadList += '<td>'+$('#descriptionS').val()+'</td>';
//
// 								var file = data.files.split(",");
// 								var filename = data.filename.split(",");
// 								uploadList += '<td><ul>';
//
// 									$.each(file, function(i, obj) {
// 								        uploadList += '<li><i><a href="temp-uploads/'+obj+'">'+filename[i]+'</a></i></li>';
// 									});
//
// 								uploadList += '</ul></td>';
//
// 								uploadList += '<td id="hiddenFields" style="display:none;"></td>';
// 									uploadList += '<input type="hidden" name="name['+count+']" value="'+$('#nameS').val()+'">';
// 									uploadList += '<input type="hidden" name="version['+count+']" value="'+$('#versionS').val()+'">';
// 									uploadList += '<input type="hidden" name="description['+count+']" value="'+$('#descriptionS').val()+'">';
// 									uploadList += '<input type="hidden" name="file['+count+']" value="'+data.files+'">';
// 									uploadList += '<input type="hidden" name="filename['+count+']" value="'+data.filename+'">';
// 									uploadList += '<input type="hidden" name="databank['+count+']" value="'+data.databank+'">';
// 								uploadList += '</td>';
//
// 							uploadList += '</tr>';
//
// 						$("#stageSummaryUpload").append(uploadList);
//
// 						$("#countS").val(count);
//
// 						$(".modal-backdrop").remove();
// 					   	var drugListModal = $('#summaryUploadModal').modal();
// 					    	drugListModal.hide();
//
// 					   	var dialog = $('#addSummaryModal').modal();
// 						    dialog.removeData('bs.modal');
// 						    dialog.modal("show");
//
// 					}else{
//
//
//
// 					}
//
// 				}
// 			});
//
// 		}
//
// 	});
//
//
// 	$("#submitSummary").on('click', function (event) { // Submit Form
//
// 		var bValid = true;
// 		var fields = new Array();
//
// 		fields['enroll_amount'] = {'type' : 'input', 'label' : 'Enroll Amount'};
// 		fields['quit_amount'] = {'type' : 'input', 'label' : 'Quit Amount'};
// 		fields['finish_amount'] = {'type' : 'input', 'label' : 'Finish Amount'};
// 		fields['light_effect'] = {'type' : 'notzero', 'label' : 'Light Effect'};
// 		fields['light_amount'] = {'type' : 'input', 'label' : 'Light Amount'};
// 		fields['mid_effect'] = {'type' : 'notzero', 'label' : 'Mid Effect'};
// 		fields['mid_amount'] = {'type' : 'input', 'label' : 'Mid Amount'};
// 		fields['hard_effect'] = {'type' : 'notzero', 'label' : 'Hard Effect'};
// 		fields['hard_amount'] = {'type' : 'input', 'label' : 'Hard Amount'};
// 		fields['important_effect'] = {'type' : 'notzero', 'label' : 'Important Effect'};
// 		fields['important_amount'] = {'type' : 'input', 'label' : 'Important Amount'};
// 		fields['degree'] = {'type' : 'input', 'label' : 'Degree'};
// 		fields['sae'] = {'type' : 'input', 'label' : 'SAE'};
// 		fields['amount'] = {'type' : 'input', 'label' : 'Amount'};
// 		fields['handling_info'] = {'type' : 'input', 'label' : 'Other Situation'};
//
// 		bValid = bValid && checkErrors(fields);
//
// 		var startDate = $("#time_slot_from").val();
// 	    var endDate = $("#time_slot_to").val();
//
// 	    var startD = new Date(startDate + " 00:00:00");
// 	    var endD = new Date(endDate + " 00:00:00");
//
// 	    if(endD < startD || startDate == '' || endDate == ''){
//
// 	        $("#time_slot_div").addClass('has-error');
// 	        $("#time_slot_msg").show().html('您输入的日期范围无效。');
// 	        $('body, html, #time_slot_div').scrollTop($("#time_slot_div").offset().top - 100);
//
// 	        bValid = bValid && false;
// 	    }
//
// 	    //bValid = true;
// 		if(bValid){
//
// 			$("#submitSummary").prop('disabled', true).html('载入中...');
// 			$.ajax({
// 				url:"php-functions/fncStageSummary.php?action=addStageSummary",
// 				type: "POST",
// 	    		dataType: "json",
// 	        	data: $("#addSummaryForm").serialize(),
// 				success:function(data){
//
// 					$("#submitSummary").prop('disabled', false).html('提交');
//
// 					if(data.type == "success")
// 						show_alert(data.msg, 'pg_audit.php?tab=stage-summary&project_id='+$("#project_idS").val(), 1);
// 					else
// 						show_alert(data.msg, '', 0);
//
// 					event.preventDefault();
//
// 				}
// 			});
//
// 		}
//
// 		event.preventDefault();
//
// 	});
//
// });
//
//
// function getStageSummary(stage_summary_id){
//
// 	$.ajax({
// 		url:"php-functions/fncStageSummary.php?action=getStageSummary",
// 		type: "POST",
// 		dataType: "json",
// 		data: { "type" : "ajax", "stage_summary_id" : stage_summary_id, "project_id" : $("#project_idS").val() },
// 		success:function(data){
//
// 			$("#addSummary").click();
//
// 			$("#modalTitleAddSummary").html('阶段摘要详细信息');
// 			$("#submitSummary").html('更新');
//
// 			$('#stage_summary_id').val(data[0].stage_summary_id);
// 			$('#time_slot_from').val(data[0].time_slot_from);
// 			$('#time_slot_to').val(data[0].time_slot_to);
// 			$('#enroll_amount').val(data[0].enroll_amount);
// 			$('#quit_amount').val(data[0].quit_amount);
// 			$('#finish_amount').val(data[0].finish_amount);
// 			$('#light_effect').val(data[0].light_effect);
// 			$('#light_amount').val(data[0].light_amount);
// 			$('#mid_effect').val(data[0].mid_effect);
// 			$('#mid_amount').val(data[0].mid_amount);
// 			$('#hard_effect').val(data[0].hard_effect);
// 			$('#hard_amount').val(data[0].hard_amount);
// 			$('#important_effect').val(data[0].important_effect);
// 			$('#important_amount').val(data[0].important_amount);
// 			$('#sae').val(data[0].sae);
// 			$('#amount').val(data[0].amount);
// 			$('#handling_info').val(data[0].handling_info);
// 			$('#degree').val(data[0].degree);
//
//
// 			// ------------- Start of get Materials ------------- @Rai//
// 			var uploadList = "";
// 			var count = 1;
// 			$.each(data[0].materials, function(i, obj) {
//
// 				//console.log(obj.material_id);
//
// 				uploadList += '<tr>';
// 					uploadList += '<td>'+count+'</td>';
// 					uploadList += '<td>'+obj.name+'</td>';
// 					uploadList += '<td>'+obj.version+'</td>';
// 					uploadList += '<td>'+obj.version_date+'</td>';
// 					uploadList += '<td>'+obj.description+'</td>';
//
// 					// ------------- Start of get Material Files ------------- @Rai//
// 					uploadList += '<td><ul>';
//
// 						$.each(obj.files, function(i, obj_) {
// 					        uploadList += '<li><i><a href="'+obj_.file+'" target="_blank">'+obj_.filename+'</a></i></li>';
// 						});
//
// 					uploadList += '</ul></td>';
// 					// ------------- End of get Material Files ------------- @Rai//
//
// 				uploadList += '</tr>';
//
// 				$("#stageSummaryUpload").html(uploadList);
//
// 				count++;
//
// 			});
// 			// ------------- End of get Materials ------------- @Rai//
//
// 			$("#countS").val(count - 1);
//
// 		}
//
// 	});
//
// }
