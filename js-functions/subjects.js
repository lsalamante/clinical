$(document).ready(function(){
  $('#ssearch_birthday').datepicker();
});

function statusChange(subject_id, status, type, lang){

  if(type == "remarks"){

    subject_id = $("#subject_id").val();
    status = $("#statusRemark").val();
    remarks = $("#remarkS").val();

  }else
    remarks = "";

  var state = "";
  switch(status) {
      case "B":
          state = (lang == 'chinese') ? "确认开始试验" : "Begin subject";
          break;
      case "FU":
          state = (lang == 'chinese') ? "确认完成随访" : "Follow up subject";
          break;
      case "STP":
          state = (lang == 'chinese') ? "停止" : "Stop subject";
          break;
      case "O":
          state = (lang == 'chinese') ? "出来" : "Out subject";
          break;
      case "D":
          state = (lang == 'chinese') ? "下降" : "Drop subject";
          break;
      case "F":
          state = (lang == 'chinese') ? "确认完成" : "Finish subject";
          break;
  }

  var bValid = true;
  if($("#remarkS").val() == "" && type == "remarks"){

      bValid = false;

      $("#remarkS_div").addClass('has-error');
      // This is a required field
      $("#remarkS_msg").show().html('这是一个必填字段');
      $('body, html, #remarkS_div').scrollTop($("#remarkS_div").offset().top - 100);

  }

  if(bValid){

    confirmationModal(state, "", "",
        function(){
          $.ajax({
            url:"php-functions/fncSubjects.php?actionS=statusChange",
            type: "POST",
            dataType: "json",
            data: { "subject_id" : subject_id, "status" : status, "remarks" : remarks, 'type' : 'ajax' },
            success:function(data){

              if(data.type == "success")
                show_alert(data.msg, 'pg_audit.php?tab=subjects&project_id='+$("#project_idS").val(), 1);
              else
                show_alert(data.msg, '', 0);

            }
          });
        },
        function(){
          // do stuff for Cancel btn
        }
    );

  }

}

function statusWithRemarks(subject_id, status, type){

  $("#subject_id").val('');
  $("#statusRemark").val('');

  $("#addRemarkSModal").modal('show');
  $("#subject_id").val(subject_id);
  $("#statusRemark").val(status);

}

function statusInspectorChange(volunteer_id){

  confirmationModal("确认转为志愿者", "", "",
      function(){
        $.ajax({
          url:"php-functions/fncSubjects.php?actionS=updateVolunteerStatus",
          type: "POST",
          dataType: "json",
          data: { "volunteer_id" : volunteer_id },
          success:function(data){

            if(data.type == "success")
              show_alert(data.msg, 'subject-library.php', 1);
            else
              show_alert(data.msg, '', 0);

          }
        });
      },
      function(){
        // do stuff for Cancel btn
      }
  );

}
function show_sub_sae(sub_id)
{
  $('#div_SAE').attr('style', 'display:block');
  $('#se_subj_select').val(sub_id);
  $('#submitSideEffects').attr('style', 'display:block');
  // $('#se_subj_id_'+sub_id).attr('selected', 'selected');
  $('#addSideEffectModal').modal('show');
}

//Desc: Search by type, status, bday and gender
//Developer: Kitcathe
$('#vfilter_subj').click(function(){
  var form_data = {
  ssearch_random_no : $('#ssearch_random_no').val(),
  ssearch_status : $('#ssearch_status').val(),
  ssearch_birthday : $('#ssearch_birthday').val(),
  ssearch_gender : $('#ssearch_gender').val(),
  project_id: $("#project_id").val()
  };
  ajax_load(form_data, 'subject_body.php', 'subj_body');
});