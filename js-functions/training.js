$(document).ready(function () {
	$('#training_date').datetimepicker();
	$("#addTrainingButton").on('click', function (event) {
		// $('#addTraining').modal('show');
		$('#table_material_upload').html('');

		$('#addTrainingForm')[0].reset();
		$.ajax({
			url:"php-functions/fncTraining.php?action=addTemp",
			type: "POST",
			dataType: "json",
			data: $('#addNewTraining').serialize(),
			success:function(id){
				$('#training_id').val(id);
				var data = {
					project_id:$('#project_idT').val()
				};
				ajax_load(data, "training_table.php", "training_table_body");
				$('#addTraining').modal('show');
			}
		});

		$("#modalTitleTrainingModal").html('添加培训/会议记录');
		$("#submitTraining").html('提交');

		$('#training_id').val('');

	});
	$("#submitTraining").on('click', function (event) {
		submitTraining("0");
	});
	$("#draftTraining").on('click', function (event) {
		submitTraining("1");
	});
	$('#t_material_modal').on('hidden.bs.modal', function () { // Show drug list modal when add drug modal closed
		$('#addTraining').modal('show');
	});

	$('#t_material_form').on('submit', function (e){
	  e.preventDefault();
	  add_material();
	});


});

function submitTraining(is_drafted)
{
	$('#is_drafted').val(is_drafted);
	$("#submitTraining").prop('disabled', true).html('提交');
	$.ajax({
		url:"php-functions/fncTraining.php?action=addTraining",
		type: "POST",
		dataType: "json",
    	data: $("#addTrainingForm").serialize() + "&project_id=" + $('#project_idT').val(),
		success:function(data){

			$("#submitTraining").prop('disabled', false).html('提交');

			if(data == "1")
				show_alert("您已成功添加会议记录", 'pg_audit.php?tab=training&project_id='+$("#project_idT").val(), 1);
			else
				show_alert(data, '', 0);

		},
		error:function(data)
		{
			console.log(data);
		}
	});
	event.preventDefault();
}

function add_material(){

  var form_data = new FormData($('#t_material_form')[0]);
  form_data.append('training_id', $('#training_id').val());
  form_data.append('sub_dir', 'training_material');
  // form_data.append('role_id', '<?php echo $_SESSION['role_id'] ?>');
  // form_data.append('up_type', '1'); /* up_type 1 for PD */

  $.ajax({
    url: "php-functions/fncCommon.php?action=uploadFile",
    type:'POST',
    processData: false,
    contentType: false,
    data:form_data,
    success:function(msg){
      if(msg == 1){
        // show_alert("Operation successful.", "", 1);
        // #We reload the table
        var training_id = {
        	"training_id":$('#training_id').val()
        }
        $('#t_material_modal').modal('hide');
        ajax_load(training_id, 'table_material_upload.php', 'table_material_upload');
      }else{
        show_alert(msg, "", 0);
      }
    }
  });
}


function getTrainingDetails(training_id){

	$.ajax({
		url:"php-functions/fncTraining.php?action=getTraining",
		type: "POST",
		dataType: "json",
		data: { "type" : "ajax", "training_id" : training_id, "project_id" : $("#project_idT").val() },
		success:function(data){

			$('#addTraining').modal('show');

			$("#modalTitleTrainingModal").html('培训/会议详情');
			$("#submitTraining").html('提交');

			for (var key in data) {

				$('#training_id').val(data[key].training_id);
				$('#title').val(data[key].title);
				$('#type').val(data[key].type);

				// Get date and set format -- Van
				// var t_date = new Date(data[key].training_date);
				// var m_date = {
				// 	"0": "",
				// 	"1": (t_date.getMonth())+1,
				// };
				// if(m_date["1"] < 10)
				// {
				// 	m_date["0"] = "0";
				// }
				// var sm_date = m_date["0"] + m_date["1"];
				// var d_date = {
				// 	"0": "",
				// 	"1":  (t_date.getDate()),
				// }
				// if(d_date["1"] < 10)
				// {
				// 	d_date["0"] = "0";
				// }
				// var sd_date = d_date["0"] + d_date["1"];
				// var f_date = sm_date + "/" + sd_date + "/" + t_date.getFullYear();
				// Get date and set format -- Van

				if(data[key].training_date != '0000-00-00 00:00:00'){
					// var newdate = new Date(data[key].training_date);
					// newdate.setHours(newdate.getHours() - 4);
					// ^ NOT SURE ABOUT THIS SHIT THIS SHIT IS DANGEROUS I TELL YOU! Date automatically adds 4 hours, so i subtract
					// All the same.
					// $('#training_date').val(newdate.toISOString().substring(0,19));
					$('#training_date').val(data[key].training_date);
				}else{
					$('#training_date').val('');
				}

				$('#host').val(data[key].host);
				$('#place').val(data[key].place);
				$('#meeting_content').val(data[key].meeting_content);
				$('#meeting_summary').val(data[key].meeting_summary);

				// --------- Start of get participants -----------//
				$("input:checkbox").removeAttr("checked");
				if(data[key].participants.length > 0){

					for(var i = 0; i < data[key].participants.length; i++){

						$("#participant_"+data[key].participants[i]).prop('checked', true);

					}
				}
				// --------- End of get participants -----------//

				// --------- Start of get materials -----------//
				var materialList = "";

				if(data[key].materials.length > 0){

					var count = 1;
					for(var i = 0; i < data[key].materials.length; i++){

						materialList += "<tr>";
							materialList += "<td>"+count+"</td>";
							materialList += "<td><a href='"+data[key].materials[i].up_path+"' target='_blank'>"+data[key].materials[i].name+"</a></td>";
							materialList += "<td>"+data[key].materials[i].version+"</td>";
							materialList += "<td>"+data[key].materials[i].version_date+"</td>";
							materialList += "<td>"+data[key].materials[i].description+"</td>";
							materialList += "<td><a class='btn' href='"+data[key].materials[i].up_path+"' target='_blank'>查看文件</a></td>";
						materialList += "</tr>";

						count++;

					}
				}

				$("#table_material_upload").html(materialList);
				// --------- End of get materials -----------//

			}

		},
		error:function(data)
		{
			var acc = []
			$.each(data, function(index, value) {
			    acc.push(index + ': ' + value);
			});
			alert(JSON.stringify(acc));

		}
	});

}
