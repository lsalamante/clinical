//  used checkErrors(fields) for field checker in js/checker.js
// returns TRUE when all fields were provided. otherwise FALSE

$(document).ready(function () {
	$('#vsearch_birthday').datepicker();
	$("#addVolunteer").on('click', function(){ // Initialize Add Volunteer Form
		$("#modalTitle").html("添加志愿者信息");
		$("#volunteerForm")[0].reset();
		$("#submitVolunteer").html('提交');
		$('#volunteer_id').val('');
		$('#volunteerModal').modal('show');

	});

	$("#volunteerForm").on('submit', function (event) { //  Volunteer Form

		if($("#v_status").val() != 1){ // add and update function is only for pending and rejected

			var bValid = true;
			var fields = new Array();

			fields['v_name'] = {'type' : 'input', 'label' : 'Name'};
			fields['gender'] = {'type' : 'dropdown', 'label' : 'Gender'};
			fields['bday'] = {'type' : 'input', 'label' : 'Birthdate'};
			fields['nation'] = {'type' : 'input', 'label' : 'Nation'};
			fields['native'] = {'type' : 'input', 'label' : 'Native Place'};
			fields['married'] = {'type' : 'radio', 'label' : 'Married'};
			fields['blood_type'] = {'type' : 'input', 'label' : 'Blood Type'};
			fields['surgery'] = {'type' : 'radio', 'label' : 'Surgery'};
			fields['fam_history'] = {'type' : 'input', 'label' : 'Family History'};
			fields['med_history'] = {'type' : 'input', 'label' : 'Medical History'};
			fields['smoker'] = {'type' : 'radio', 'label' : 'Heavy Smoker'};
			fields['drinker'] = {'type' : 'radio', 'label' : 'Heavy Drinker'};
			fields['allergies'] = {'type' : 'input', 'label' : 'Allergies'};
			fields['height'] = {'type' : 'input', 'label' : 'Height'};
			fields['weight'] = {'type' : 'input', 'label' : 'Weight'};
			fields['bmi'] = {'type' : 'input', 'label' : 'BMI'};
			fields['mobile'] = {'type' : 'input', 'label' : 'Mobile'};
			// TODO: ADD BVALID FOR ID CARD NUM AND ID CARD UPLOAD

			bValid = bValid && checkErrors(fields);

			if(bValid){

				$("#submitVolunteer").prop('disabled', true).html('载入中...');

				var form_data = new FormData($("#volunteerForm")[0]);

				$.ajax({
					url:"php-functions/fncVolunteers.php?action=addVolunteer",
					type:'POST',
					processData: false,
					contentType: false,
					data:form_data,
					success:function(msg){
						var msg = JSON.parse(msg);
						$("#submitVolunteer").prop('disabled', false).html('提交');

						if(msg["type"] == "success"){
							show_alert(msg["msg"], 'pg_audit.php?tab=volunteer&project_id='+$("#project_id").val(), 1);
						}
						else{
							// show_alert(data.msg, 'pg_audit.php?tab=volunteer&project_id='+$("#project_id").val(), 1);
							show_alert(msg["msg"], '', 0);
						}

					}
				});

			}

		}

		event.preventDefault();

	});

	$("#remarksForm").on('submit', function (event) { //  Volunteer Form
		event.preventDefault();

		$("#submitRemarks").prop('disabled', true).html('载入中...');
		$.ajax({
			url:"php-functions/fncVolunteers.php?action=addRemarks",
			type: "POST",
			dataType: "json",
			data: $("#remarksForm").serialize(),
			success:function(data){

				$("#submitRemarks").prop('disabled', false).html('提交');

				if(data.type == "success")
				show_alert(data.msg, 'pg_audit.php?tab=volunteer&project_id='+$("#project_idR").val(), 1);
				else
				show_alert(data.msg, '', 0);

			}
		});

	});

});

function getVolunteerDetails(volunteer_id, status, user_type){ // on click volunteer row

	// if(status == 1)
	// $("#submitVolunteer").hide(); // hide submit button when volunteer is already approved

	$("#v_status").val(status);
	$("#modalTitle").html("志愿者详细信息"); // initial Add Volunteer Modal Title

	$.ajax({
		url:"php-functions/fncVolunteers.php?action=getVolunteerById",
		type: "POST",
		dataType: "json",
		data: { "volunteer_id" : volunteer_id, "type" : "ajax" },
		success:function(data){

			if(user_type != "SC")
			{
				$('#volunteerForm :input').attr('disabled', 'disabled');
			}

			$("#submitVolunteer").html('更新');
			$("#volunteerModal").modal("show");

			$('#volunteer_id').val(data.volunteer_id);
			$('#v_name').val(data.v_name);
			$('#gender').val(data.gender);
			$('#bday').val(data.bday);
			$('#nation').val(data.nation);
			$('#native').val(data.native);
			$('#gender').val(data.gender);
			$('input[name="married"]').val([data.married]);
			$('#blood_type').val(data.blood_type);
			$('input[name=surgery]').val([data.surgery]);
			$('#fam_history').val(data.fam_history);
			$('#med_history').val(data.med_history);
			$('input[name=smoker]').val([data.smoker]);
			$('input[name=drinker]').val([data.drinker]);
			$('#allergies').val(data.allergies);
			$('#height').val(data.height);
			$('#weight').val(data.weight);
			$('#bmi').val(data.bmi);
			$('#mobile').val(data.mobile);
			$('#id_card_num').val(data.id_card_num);
			if(data.id_card_path != ''){
				$('#id_card_copy_div').removeClass('hidden');
				$('#id_card_copy_file').attr('href', data.id_card_path);
			}


		}
	});

}

function volunteerStatus(volunteer_id, project_id, status, lang){ // agree, reject, delete

	if(lang == 'chinese'){
		var action = status == 'S' ? "确认已通知该志愿者体检" : status == 'P' ? "确认体检合格 " : status == 'NP' ? ' 确认体检不通过 ' : ''; // TODO: fill last status
		var message = status == 'S' ? "" : status == 'P' ? "" : status == 'NP' ? '' : '';
	}else{
		var action = status == 'S' ? "enlist " : status == 'P' ? "pass " : status == 'NP' ? 'reject ' : ''; // TODO: fill last status
		var message = status == 'S' ? "volunteer for clinician\'s selection" : status == 'P' ? "volunteer" : status == 'NP' ? 'volunteer' : '';
	}

	confirmationModal(action, message, "",
	function(){

		$.ajax({
			url:"php-functions/fncVolunteers.php?action=volunteerStatus",
			type: "POST",
			dataType: "json",
			data: { "volunteer_id" : volunteer_id, "project_id" : project_id, "status" : status },
			success:function(data){

				if(data.type == "success")
				show_alert(data.msg, 'pg_audit.php?tab=volunteer&project_id='+project_id, 1);
				else
				show_alert(data.msg, '', 0);
				// TODO: fix success message on volunteer pass or no passs
			}
		});

	},
	function(){}
);

}

function remarks(volunteer_id){
	$("#remarks_").text(''); //reset remarks

	$('#addRemarks').click();
	$('#volunteer_idR').val(volunteer_id);

	$.ajax({
			url:"php-functions/fncVolunteers.php?action=getVolunteerRemarks",
		type: "POST",
		dataType: "json",
		data: { "volunteer_id" : volunteer_id, "type" : "ajax" },
		success:function(data){
			if(data['remarks'] != ""){

				$("#submitRemarks").html('更新');
				$("#remarks_").text(data['remarks']);

			}else{
				$("#submitRemarks").html('提交');
			}

		}
	});

}

function computeBMI()
{
	var h = $('#height').val();
	h = h / 100;
	var w = $('#weight').val();
	var bmi = 0;
	bmi = w / ( h * h );
	bmi = bmi.toFixed(2);
	$('#bmi').val(bmi);
}


//Desc: Search by type, status, bday and gender
//Developer: Kitcathe
$('#vfilter_submit').click(function(){

	var form_data = {
	vsearch_type : $('#vsearch_type').val(),
	vsearch_status : $('#vsearch_status').val(),
	vsearch_birthday : $('#vsearch_birthday').val(),
	vsearch_gender : $('#vsearch_gender').val(),
	project_id: $("#project_id").val()
	};
	ajax_load(form_data, 'volunteer_body.php', 'vol_body');
	
});