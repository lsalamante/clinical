// Check multiple fields
// using html element
function checkErrors(fields){

    var bValid = true;
    var codeCheck = true;
    var schedCheck = true;
    var result = Array();

   for (var key in fields) {

        var check = true;

        if(fields[key]['type'] == 'input' || fields[key]['type'] == 'dropdown'){



              if($("#"+key).val() == ''){

                result.push(key);
                check = false;
                bValid = bValid && false;

            }else
                bValid = bValid && true;

        }else if(fields[key]['type'] == 'alpha'){

            if( !isNaN($("#"+key).val()) || $("#"+key).val() == ''){

                result.push(key);
                check = false;
                bValid = bValid && false;

            }else
                bValid = bValid && true;

        }else if(fields[key]['type'] == 'numeric'){

            if( isNaN($("#"+key).val()) || $("#"+key).val() == ''){

                result.push(key);
                check = false;
                bValid = bValid && false;

            }else
                bValid = bValid && true;

        }else if(fields[key]['type'] == 'notzero'){

            if( parseFloat($("#"+key).val()) < 1 || $("#"+key).val() == ""){

                result.push(key);
                check = false;
                bValid = bValid && false;

            }else
                bValid = bValid && true;

        }else if(fields[key]['type'] == 'email'){

            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if( !regex.test( ( $("#"+key).val() ) ) ) {

                result.push(key);
                check = false;
                bValid = bValid && false;

            }else
                bValid = bValid && true;

        }else if(fields[key]['type'] == 'src'){

            if($("#"+key).attr('src') == '' || $("#"+key).attr('src') == "images/noimage.png"){

                result.push(key);
                check = false;
                bValid = bValid && false;

            }else
                bValid = bValid && true;

        }else if(fields[key]['type'] == 'checkbox'){

            if($("input:checkbox[name='"+key+"[]']").is( ":checked" ) == false){

                result.push(key);
                check = false;
                bValid = bValid && false;

            }else
                bValid = bValid && true;


        }else if(fields[key]['type'] == 'radio'){

            if($("input:radio[name='"+key+"']").is( ":checked" ) == false){

                result.push(key);
                check = false;
                bValid = bValid && false;

            }else
                bValid = bValid && true;

        }

        if(!check){

          $("#"+key+"_div").addClass('has-error');
          $("#"+key+"_msg").show();

          if(fields[key]['type'] == 'input'){

              // $("#"+key+"_msg").html(fields[key]['label']+' is a required field.');
              //Just made this general `This is a required field` because %^!&I$^*!^$*!!!!!!!!!!
              $("#"+key+"_msg").html('这是一个必填字段');

          }else if(fields[key]['type'] == 'dropdown'){

              $("#"+key+"_msg").html('这是一个必填字段');
              // $("#"+key+"_msg").html('You must have a selection for '+fields[key]['label']+'.');

          }else if(fields[key]['type'] == 'alpha' || fields[key]['type'] == 'numeric' || fields[key]['type'] == 'notzero'){

              $("#"+key+"_msg").html('这是一个必填字段');
              // $("#"+key+"_msg").html('You have inputed an invalid value for '+fields[key]['label']+'.');

          }else if(fields[key]['type'] == 'email'){

              $("#"+key+"_msg").html('You must provide a valid email address.');

          }else if(fields[key]['type'] == 'src'){

              // $("#"+key+"_msg").html(fields[key]['label']+' is a required field.');
              //Just made this general `This is a required field` because %^!&I$^*!^$*!!!!!!!!!!
              $("#"+key+"_msg").html('这是一个必填字段');

          }else if(fields[key]['type'] == 'checkbox'){

              $("#"+key+"_msg").html('You must provide at least a value for '  +fields[key]['label']+'.');

          }else if(fields[key]['type'] == 'radio'){

              $("#"+key+"_msg").html('You must select any of the '  +fields[key]['label']+' choices.');

          }

        }else{

          $("#"+key+"_div").removeClass('has-error');
          $("#"+key+"_msg").hide();

        }

    }

    if(result.length > 0){

      console.log(result);

      $('body, html, #'+result[0]+'_div').scrollTop($("#"+result[0]+'_div').offset().top - 100);

    }


    return bValid;

}

// remove has-error addClass upon onchange
function clearError(field){

    $("#"+field.id+"_div").removeClass('has-error');
    $("#"+field.id+"_msg").hide();

}

function clearAllError(fields){

   for (var key in fields) {

      $("#"+fields[key]+"_div").removeClass('has-error');
      $("#"+fields[key]+"_msg").hide();

   }

}

function formatAmount(el){  // FORMAT AMOUNT

  el.value = el.value.replace(/\D/g, "").replace(/([0-9])([0-9]{2})$/, '$1.$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
}
