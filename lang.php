<?php
function ordutf8($string, &$offset) {
    $code = ord(substr($string, $offset,1));
    if ($code >= 128) {        //otherwise 0xxxxxxx
        if ($code < 224) $bytesnumber = 2;                //110xxxxx
        else if ($code < 240) $bytesnumber = 3;        //1110xxxx
        else if ($code < 248) $bytesnumber = 4;    //11110xxx
        $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);
        for ($i = 2; $i <= $bytesnumber; $i++) {
            $offset ++;
            $code2 = ord(substr($string, $offset, 1)) - 128;        //10xxxxxx
            $codetemp = $codetemp*64 + $code2;
        }
        $code = $codetemp;
    }
    $offset += 1;
    if ($offset >= strlen($string)) $offset = -1;
    return $code;
}
function ords_to_unistr($ords, $encoding = 'UTF-8'){
    // Turns an array of ordinal values into a string of unicode characters
    $str = '';
    for($i = 0; $i < sizeof($ords); $i++){
        // Pack this number into a 4-byte string
        // (Or multiple one-byte strings, depending on context.)
        $v = $ords[$i];
        $str .= pack("N",$v);
    }
    $str = mb_convert_encoding($str,$encoding,"UCS-4BE");
    return($str);
}



$str = '请选择';
$offset = 0;
$chn_char = array();
$eng_char = array();
$ctr = 0;
while($offset >= 0)
{
	$chn_char[$ctr] = ordutf8($str, $offset);
	$ctr++;
}

$eng_char = ords_to_unistr($chn_char);

print_r(implode(",",$chn_char));
echo "<br>";
print_r($eng_char);


include('jp_library/jp_lib.php');
if($_POST)
{
	//print_r($_POST['chinese']);
	for($x = 0; $x<count($_POST['chinese']); $x++ )
	{
		$str = $_POST['chinese'][$x];
		$offset = 0;
		$chn_char = array();
		$eng_char = array();
		$ctr = 0;
		while($offset >= 0)
		{
			$chn_char[$ctr] = ordutf8($str, $offset);
			$ctr++;
		}

		$eng_char = ords_to_unistr($chn_char);
		/*echo "ASCII: ";
		print_r(implode(",",$chn_char));
		echo "<br>";
		echo "ENG CHAR: ";
		print_r($eng_char);
		echo "<br><br>";*/
		$data['chinese'] = implode(",",$chn_char);
		$data['english'] = $_POST['english'][$x];
		$data['phrase'] = $_POST['phrase'][$x];
		$insert['table'] = "language";
		$insert['data'] = $data;
		jp_add($insert);
	}
}
?>
<html>
<body>
<!--<select onchange="numberEntries()" id="num">
	<?php $i = 0;
	for($i = 0; $i<20; $i++){	 ?>
	<option value="<?php echo $i; ?>"><?php echo $i; ?><option>
	<?php } ?>
</select>-->
<form role="form" method="POST" accept-charset="UTF-8">
<?php if(isset($_GET['p'])){
		for($i = 0; $i < $_GET['p']; $i++) { ?>
ENGLISH HERE: <input type="text" name="english[]" id="english" autofocus><br>
CHINESE CHAR HERE: <input type="" name="chinese[]" id="chinese"><br>
PHRASE: <input type="" name="phrase[]" id="phrase"></br><br>
	<?php } ?>
<?php }else{ ?>
ENGLISH HERE: <input type="" name="english[]" id="english"><br>
CHINESE CHAR HERE: <input type="" name="chinese[]" id="chinese"><br>
PHRASE: <input type="" name="phrase[]" id="phrase"></br>
<?php } ?>
<button type="submit">Submit</button>
</form>
