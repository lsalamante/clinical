<?php
namespace Clinical\Helpers;
$t = new Translation($_SESSION['lang']); # instantiate t

?>
<aside class="sidebar2">
  <div id="sidebar" >

    <div class="role">
      <!-- Role -->
      <select style="font-size: 1.7em; border:none; width:13.6% !important;margin-top: 6px;" id="top_nav_role" onchange="topRoleChange(this.value);">
        <?php
        $getRolesUser['table'] = "users u LEFT JOIN
        roles r ON u.user_id = r.user_id LEFT JOIN
        positions p ON r.position_id = p.position_id";
        $getRolesUser['where'] = "u.user_id = ".$_SESSION['user_id'];
        $resRolesUser = jp_get($getRolesUser);
        while ($rowRolesUser = mysqli_fetch_assoc($resRolesUser)) {
          ?>
          <option value="<?php echo $rowRolesUser['role_id']; ?>" <?php echo ($_SESSION['role_id'] == $rowRolesUser['role_id']) ? "selected" : ""; ?>>
            <?php echo $t->tryTranslate($rowRolesUser['lang_phrase']); ?>
          </option>
          <?php
        }
        ?>
      </select>
      <!-- End of Role -->
    </div>

    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">

      <!-- VARIABLE TO SET FOR STYLE WHEN CHINESE -->
      <?php
      $style_add = ""; # blank if enlish language
      if($_SESSION['lang'] == 'chinese')
      {
        # will be appended inside class of <li>
        $projectaudit2 = "projectaudit2";
        $projectinfo2 = "projectinfo2";
        $subjects2 = "subjects2";
        $traininglist2 = "traininglist2";
        $sideeffects2 = "sideeffects2";
        $inspections2 = "inspections2";

      }
      ?>
      <!-- VARIABLE TO SET FOR STYLE WHEN CHINESE -->
      <li class="subjects <?php echo $subjects2; ?>">
        <a href="homepage.php" class="<?php if($PAGE_NAME == 'homepage.php') echo "active"; ?>"> <span><?php echo $t->tryTranslate("home"); ?></span> </a>
      </li>
      <?php if(!in_array($_SESSION['user_type'], ['RN', 'C', 'BGC', 'MC', 'SC', 'DA'])){ ?>
        <li class="projectaudit <?php echo $projectaudit2; ?>">
          <a class="<?php if ($PAGE_NAME == 'dashboard.php' || $PAGE_NAME == 'new_project_draft.php' || ($PAGE_NAME == 'pg_audit.php' && $_SESSION['user_type'] == 'A')) echo 'active' ?>"
            href="dashboard.php"> <!--<i class="fa fa-home"></i>--> <span><?php echo $_SESSION['user_type'] != "A" ? $phrases['my_projects_non_a'] : $phrases['my_projects']; ?></span></a>
          </li>
          <?php } ?>
          <?php //if(in_array($_SESSION['user_type'], ['TFO', 'RN', 'C', 'BGC', 'I', 'MC'])){?>
            <?php if($_SESSION['user_type'] != "A"): ?>
              <li class="projectinfo <?php echo $projectinfo2; ?>">
                <a class="<?php if ($PAGE_NAME == 'project_information.php' || $PAGE_NAME == 'pg_audit.php' ) echo 'active' ?>" href="project_information.php"> <!--<i class="fa fa-home"></i>--> <span style="<?php echo $style_add; ?>"><?php echo $phrases['project_information'] ?></span></a>
                </li>
              <?php endif; ?>
              <?php //}?>

              <!-- Start of user logs -->
              <?php if ($_SESSION['user_type'] == "PI"): ?>
                <li class="subjects <?php echo $subjects2; ?>">
                  <a class="<?php if ($PAGE_NAME == 'user_logs.php') echo 'active'; ?>" href="user_logs.php">
                    <span><?php echo $t->tryTranslate("user_logs"); ?></span>
                  </a>
                </li>
              <?php endif; ?>
              <!-- End of user logs -->

              <!-- Start of Subject Library @Rai-->
              <?php if($_SESSION['is_professonial_group'] == 1): ?>
                <li class="subjects <?php echo $subjects2; ?>">
                  <a class="<?php if ($PAGE_NAME == 'subject-library.php') echo 'active' ?>"
                    href="subject-library.php"> <span style="<?php echo $style_add; ?>"><?php echo $phrases['subject_library'] ?></span> </a>
                  </li>
                  <!-- End of Subject Library @Rai-->

                  <!-- Start of Training / Meeting @Rai-->
                  <li class="traininglist <?php echo $traininglist2; ?>">
                    <a class="<?php if ($PAGE_NAME == 'training-list.php') echo 'active' ?>"
                      href="training-list.php"> <span style="<?php echo $style_add; ?>"><?php echo $phrases['training_meeting'] ?></span> </a>
                    </li>
                    <!-- End of Training / Meeting @Rai-->
                  <?php endif; ?>

                  <?php if(!in_array($_SESSION['user_type'], ['PRE', 'A'])): ?>
                  <li class="sideeffects <?php echo $sideeffects2; ?>">
                    <a class="<?php if ($PAGE_NAME == 'side-effect-list.php') echo 'active' ?>"
                      href="side-effect-list.php"> <!--<i class="fa fa-flask"></i>--> <span style="<?php echo $style_add; ?>"><?php echo $phrases['side_effects'] ?></span> </a>
                    </li>
                  <?php endif; ?>

                    <li class="inspections <?php echo $inspections2; ?>">
                      <a class="<?php if ($PAGE_NAME == 'inspection-list.php') echo 'active' ?>"
                        href="inspection-list.php"> <!--<i class="fa fa-search-plus"></i>--> <span style="<?php echo $style_add; ?>"><?php echo $phrases['inspection'] ?></span> </a>
                      </li>
                    </ul>
                    <!-- sidebar menu end-->
                  </div>
                </aside>
