<?php
namespace Clinical\Helpers;

include('jp_library/jp_lib.php');

$l = new Log($_SESSION['role_id'], 'logout', null);
$l->save();

session_destroy();

header("Location: " . "index.php");
die();
