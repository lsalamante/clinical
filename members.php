<?php 
namespace Clinical\Helpers;
require("php-functions/fncMembers.php");
$t = new Translation($_SESSION['lang']);


$project_id = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? $_GET["project_id"] : 0;
$members_arr = getMembers(false, $project_id);
$count = 0;
?>
  <section class="panel negative-panel">
    <header class="panel-heading">
      <br>
      <?php if($project['status_num'] >= 13 && $_SESSION['user_type'] == 'PI'){
        // disable for other users
      ?>
      <a href="#add_member_modal" data-toggle="modal" class="btn btn-xs btn-primary" id="memeber_add"><?php echo $t->tryTranslate('manage_members')?></a>
      <!-- <button type="submit" class="btn btn-danger btn-xs" id="job_del"><?php echo $phrases['delete']?></button> -->
      <?php }  ?>
    </header>
    <?php  if(count($members_arr) > 0 && $project_id != 0){ // check if found record/s and if id is provided. ?>
		<table class="table table-hover">
      <thead>
        <tr>
          <th><?php echo $phrases['number']?></th>
          <th><?php echo $phrases['member_name']?></th>
          <th><?php echo $phrases['gender']?></th>
          <th><?php echo $phrases['professional_group']; ?></th>
          <th><?php echo $phrases['departments']?></th>
          <th><?php echo $phrases['job_title']?></th>
          <th><?php echo $phrases['member_position']?></th>
          <th><?php echo $phrases['mobile_num']?></th>
          <th><?php echo $phrases['id_card']; ?></th>
          <th><?php echo $phrases['gcp']; ?></th>
          <th><?php echo $phrases['diploma']; ?></th>
          <th><?php echo $phrases['deg_cert']; ?></th>
          <!-- <th>Action</th> -->
        </tr>
      </thead>
        <tbody id="pp_body">

          <?php

              $count = 0;

              foreach ($members_arr as $member) {

                $count++;
          ?>
              <tr onclick="getMemberDetails(<?php echo $member['user_id']; ?>); return false;" style="cursor:pointer;">
                  <td><?php echo $count; ?></td>
                  <td><?php echo $member["fname"]." ".$member['lname']; ?></td>
                  <td><?php echo $member["gender"] == "0" ? $phrases['male'] : $phrases['female']; ?></td>
                  <td><?php echo $member["pgroup_name"]; ?></td>
                  <td><?php echo $member["department"]; ?></td>
                  <td><?php echo $member["job_title"]; ?></td>
                  <td><?php echo $t->tryTranslate($member["lang_phrase"]); ?></td>
                  <td><?php echo $member['mobile_num'] ?></td>
                  <td><?php echo $member['doc_id_card'] != '' ? '<a href="'.$member["doc_id_card"].'" class="btn btn-sm btn-primary" target="_blank" title="view file">'.$phrases['view_attachments'].'</a>' : $phrases['not_applicable']; ?></td>
                  <td><?php echo $member['doc_gcp_card'] != '' ? '<a href="'.$member["doc_gcp_card"].'" class="btn btn-sm btn-primary" target="_blank" title="view file">'.$phrases['view_attachments'].'</a>' : $phrases['not_applicable']; ?></td>
                  <td><?php echo $member['doc_diploma'] != '' ? '<a href="'.$member["doc_diploma"].'" class="btn btn-sm btn-primary" target="_blank" title="view file">'.$phrases['view_attachments'].'</a>' : $phrases['not_applicable']; ?></td>
                  <td><?php echo $member['doc_degree'] != '' ? '<a href="'.$member["doc_degree"].'" class="btn btn-sm btn-primary" target="_blank" title="view file">'.$phrases['view_attachments'].'</a>' : $phrases['not_applicable']; ?></td>
                  <!-- <td onclick="event.cancelBubble = true;"></td> -->
              </tr>

          <?php } ?>
        </tbody>
    </table>

<?php }else{ // no record/s found ?>
		<div class="col-lg-12 text-center">
          <img src="img/sad.png" class="sad">
          <h1 class="sad"><?php echo $phrases['empty_table_data']?></h1>
        </div>
<?php } ?>
  </section>

