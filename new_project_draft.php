<?php
namespace Clinical\Helpers;
include('jp_library/jp_lib.php');
require("php-functions/fncCommon.php");
require("php-functions/fncProjects.php");
require("php-functions/fncSubjects.php");
//require("php-functions/fncProjects.php"); # check if really needed
$t = new Translation($_SESSION['lang']);

if (!isset($_SESSION['user_id'])) {
  header("Location: " . "index.php");
  die();
}
#initial page data
$user_array = getUserById(0, $_SESSION['user_id']);
$profgroup_array = getProfGroup(0);
$departments = getAllDepartments(0);
$trials = getAllTrials(0);
if(!isset($_GET['project_id'])){
  $_GET['project_id'] = false;
}
$project = getProjectById(0, $_GET['project_id']);

$prof_group_users = getPIByProfGroupId(0, $project['pgroup_id']); #get PI for principal investigator field
$pgroup_admin_id = getProfGroupAdminIdByProfGroupId(0, $project['pgroup_id']);
$pgroup_admin = getUserById(0, $pgroup_admin_id);
//$pi = getUserById(0, $project['pi_id']);
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<?php include('header.php'); ?>
<body>
  <section id="container">
    <!--header start-->
    <header class="header white-bg">
      <?php
      if ($LEFT_SIDEBAR) {
        // echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
      }
      ?>
      <!--logo start-->
      <?php if ($LOGO) {
        include('logo.php');
      }
      ?>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <?php if ($NOTIFICATION) {
          include('notification.php');
        } ?>
        <!--  notification end -->
      </div>
      <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
      include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->

    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
          <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
              <li><a href="dashboard.php"><i class="fa fa-home"></i> <?php echo $phrases['my_projects']?></a></li>
              <li><?php echo $phrases['project_detail'] ?></li>
            </ul>
            <!--breadcrumbs end -->
          </div>
        </div>
        <?php include('project.php'); ?>
        <section class="panel negative-panel">
            <div class="panel-body">
            <?php include("remarks.php"); ?>
          </div>
        </section>
        <?php include('submit_button.php'); ?>
        <!-- page end-->
      <?php include("side-effect-modal.php"); ?>
      </section>
    </section>

    <!--main content end-->
    <!-- Right Slidebar start -->
    <?php
    if ($RIGHT_SIDEBAR) {
      include('right-sidebar.php');
    }
    ?>
    <!-- Right Slidebar end -->
    <!--footer start-->
    <?php include('footer.php'); ?>
    <!--footer end-->
  </section>
  <?php include('scripts.php'); ?>
  <!--        #######################  PROJECT SPECIFIC SCRIPTS ##############-->
  <script>
  $(document).ready(function () {

    var professional_group = $("#professional_group");
    var pgroup_contact = $("#pgroup_contact");
    var pgroup_contact_id = $("#pgroup_contact_id");
    var pgroup_mobile = $("#pgroup_mobile");
    var pgroup_email = $("#pgroup_email");
    var pi_id = $("#pi_id");
    var ap_save = $("#ap_save");
    var ap_submit = $("#ap_submit");
    var project_form = $("#project_form");
    var mc_form = $("#mc_form");
    var mc_add = $("#mc_add");
    var mc_del = $("#mc_del");
    var mc_chk_all = $("#mc_chk_all");

    <?php include("initialize.js"); ?>
    <?php include("audit.js"); ?>
    <?php include("pd_mod.js"); ?> /* this is for activating pd modules */
    <?php include("pp_mod.js"); ?> /* this is for activating pp modules */

    //                      ##### EVENTS!!!

    //                      ###### Reset our fields every time `add` is clicked
    //                      ###### any other way to efficiently do this?
    //                      ###### TODO: Refactor if necessary
    mc_add.on('click', function() {
      mc_form[0].reset();
    });

    //                      ###### / Reset our fields every time `add` is clicked
    //                      ###### / INITIALIZE

    //                      ###### Deletion functions
    mc_del.on('click', function(){
      $("#mc_body input:checkbox:checked").each(function() {
        var _id = $(this).attr('id');
        _id = _id.split('-');
        //                      #push the number only
        del_arr.push(_id[1]);
      });

      var form_data = {
        data: "del_arr=" + del_arr.join(",")
      };

      $.ajax({
        url: "php-functions/fncApplicant.php?action=delMcInArray",
        type: "POST",
        data: form_data,
        success: function (msg) {

          if(msg == 1){
            //                                    # Clear our array
            del_arr = [];

            show_alert("<?php echo $phrases['deleted']; ?>", "", 1);

            //                                  #We reload the table
            ajax_load(data, 'mc_body.php', 'mc_body');
          }else{
            show_alert("<?php echo $phrases['operation_failed']; ?>", "", 0);

          }
          mc_chk_all.prop('checked', false);
        }
      });
    });

    //                      ###### / Deletion functions

    //                      #shet panget ng code huhuhu rush kasi eh huhuh
    //                      #violating DRY sad
    mc_chk_all.change(function(){
      this.checked ? check_all(mc_body) :  uncheck_all(mc_body);
    });

    professional_group.on('change', function () {
      set_pgroup_data();
    });

    ap_save.on('click', function (e) {
      e.preventDefault();
      save_project(false);
    });
    mc_form.on('submit', function (e){
      e.preventDefault();
      add_mc_info();
    });

    ap_submit.on('click', function (e){
      make_loading(ap_submit);
      save_project(true);
    });

    //                      ##### FUNCTIONS!!

    function add_mc_info(){
      var form_data = {
        data: mc_form.serialize() + "&project_id=<?php echo $_GET['project_id'] ?>"
      };
      $.ajax({
        url: "php-functions/fncApplicant.php?action=addMcInfo",
        type: "POST",
        data: form_data,
        success: function (msg) {
          show_alert("<?php echo $phrases['operation_successful']; ?>", "", 1);
          //                                  #We reload the table
          ajax_load(data, 'mc_body.php', 'mc_body');
        }
      });
    }

    function save_project(from_submit) {
      var form_data = {
        data: project_form.serialize()
      };

      $.ajax({
        url: "php-functions/fncApplicant.php?action=saveProject",
        type: "POST",
        data: form_data,
        success: function (msg) {
          if (msg == 1){
            if(from_submit == false)
            {
              show_alert("<?php echo $phrases['operation_successful']; ?>", "dashboard.php", 1);
            }
            else
            {
              submit_project();
            }
          }
          else{
            show_alert("<?php echo $phrases['error_updating_project']; ?>", '', 0);
          }
        }

      });
    }

    function submit_project() {
      var form_data = $('#ap_submit_form').serialize();
      $.ajax({
        url:"php-functions/fncProjects.php?action=changeStatus",
        type:"POST",
        data:form_data,
        success:function(msg)
        {
          if(msg != 0)
          {
            show_alert("<?php echo $phrases['operation_successful']; ?>", 'dashboard.php', 1);
            ap_submit.html("Submit");
            /** PS: function show_alert(msg, redirect, type) --> footer.php **/
          }
          else
          {
            //show_alert("Unsuccessful Submission. Please Try Again. ", '', 0);
            show_alert(msg, '', 0);
            ap_submit.removeAttr('disabled');
            ap_submit.html("<?php echo $phrases['submit']; ?>");
          }
        }
      });
    }

    var allowed_arr = [0,4,null]; //project status that the user is allowed to submit

    if(allowed_arr.indexOf(status_num) == -1){
      $('#project_div input').attr("disabled", true);
      $('#project_div select').attr("disabled", true);
      $('#project_div textarea').attr("disabled", true);
      $('#project_div button').attr("disabled", true);
      $('.adder-disable').hide();
      $('.adder-disable').attr("href", "#");
    }

    <?php if(($project['status_num'] == 7 && $project['is_rejected'])){ ?>
      $('#project_div input').attr("disabled", false);
      $('#project_div select').attr("disabled", false);
      $('#project_div textarea').attr("disabled", false);
      $('#project_div button').attr("disabled", false);
      $('.adder-disable').attr("disabled", false);
      $('#project_div input').show();
      $('#project_div select').show();
      $('#project_div textarea').show();
      $('#project_div button').show();
      $('.adder-disable').show();
      $('#mc_add').attr("href", "#mc_modal");
      $('#pd_add').attr("href", "#pd_modal");
      $('#pp_add').attr("href", "#pp_modal");
      <?php } ?>

    switch(status_num){
      case 14:
      case 15:
      $('#pd_add').attr("href", "#pd_modal");
      $('#pd_add').show();
      $('#pd_del').show();
      break;
    }
      /* END DOCUMENT READY */
    });
    </script>
    <script type="text/javascript" src="js-functions/side-effect.js"></script>
    </body>

    </html>
