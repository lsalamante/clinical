<?php
namespace Clinical\Helpers;
$t = new Translation($_SESSION['lang']);
$n = new Notification($_SESSION['role_id'], NULL, $_SESSION['lang']);
$n->fetch();
$n->countNotif();
?>
<ul class="nav top-menu">
  <!-- settings start -->
  <!-- <li class="dropdown">
  <a data-toggle="dropdown" class="dropdown-toggle" href="#">
  <i class="fa fa-tasks"></i>
  <span class="badge bg-success">6</span>
</a>
<ul class="dropdown-menu extended tasks-bar">
<div class="notify-arrow notify-arrow-green"></div>
<li>
<p class="green">You have 6 pending tasks</p>
</li>
<li>
<a href="#">
<div class="task-info">
<div class="desc">Dashboard v1.3</div>
<div class="percent">40%</div>
</div>
<div class="progress progress-striped">
<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
<span class="sr-only">40% Complete (success)</span>
</div>
</div>
</a>
</li>
<li>
<a href="#">
<div class="task-info">
<div class="desc">Database Update</div>
<div class="percent">60%</div>
</div>
<div class="progress progress-striped">
<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
<span class="sr-only">60% Complete (warning)</span>
</div>
</div>
</a>
</li>
<li>
<a href="#">
<div class="task-info">
<div class="desc">Iphone Development</div>
<div class="percent">87%</div>
</div>
<div class="progress progress-striped">
<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 87%">
<span class="sr-only">87% Complete</span>
</div>
</div>
</a>
</li>
<li>
<a href="#">
<div class="task-info">
<div class="desc">Mobile App</div>
<div class="percent">33%</div>
</div>
<div class="progress progress-striped">
<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 33%">
<span class="sr-only">33% Complete (danger)</span>
</div>
</div>
</a>
</li>
<li>
<a href="#">
<div class="task-info">
<div class="desc">Dashboard v1.3</div>
<div class="percent">45%</div>
</div>
<div class="progress progress-striped active">
<div class="progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
<span class="sr-only">45% Complete</span>
</div>
</div>

</a>
</li>
<li class="external">
<a href="#">See All Tasks</a>
</li>
</ul>
</li> -->
<!-- settings end -->
<!-- inbox dropdown start-->
<li id="header_inbox_bar" class="dropdown">
  <a data-toggle="dropdown" class="dropdown-toggle" href="#" id="to_do_notif">
    <i class="fa fa-envelope-o"></i>
    <?php if ($n->unread_notifs > 0): ?>
      <span class="badge bg-important"><?php echo $n->unread_notifs; ?></span>
    <?php endif; ?>
  </a>
  <ul class="dropdown-menu extended inbox">
    <div class="notify-arrow notify-arrow-red"></div>
    <li>
      <p><?php echo $t->tryTranslate("news"); ?></p>
    </li>
    <?php
    if(count($n->notifications) > 0):
      foreach ($n->notifications as $notification) {
        ?>
        <li>
          <a href="<?php echo $notification['url'] ?>">
            <span class="subject">
              <span class="from"><?php echo $notification['title']; ?></span>
              <span class="time"><?php echo $notification['created_at']; ?></span>
            </span>
            <span class="message">
              <div><?php echo $t->tryTranslate('project_name'); ?> : <?php echo $notification['project_name']; ?></div>
              <div><?php echo $t->tryTranslate('applicant'); ?> : <?php echo $notification['project_owner']; ?></div>
              <div><?php echo $t->tryTranslate('to_do'); ?> : <?php echo $notification['todo']; ?></div>
            </span>
          </a>
        </li>
        <?php }
      else:
?>
<li>
  <a href="#">
    <span class="subject">
    </span>
    <span class="message" >
      <center>
      <?php echo $t->tryTranslate('empty_table_data'); ?>
    </center>
    </span>
  </a>
</li>
<?php endif;
      ?>
      <!-- <li>
      <a href="#">See all messages</a>
    </li> -->
  </ul>
</li>
<!-- inbox dropdown end -->
<!-- notification dropdown start-->
<!-- <li id="header_notification_bar" class="dropdown">
<a data-toggle="dropdown" class="dropdown-toggle" href="#">

<i class="fa fa-bell-o"></i>
<span class="badge bg-warning">7</span>
</a>
<ul class="dropdown-menu extended notification">
<div class="notify-arrow notify-arrow-yellow"></div>
<li>
<p class="yellow">You have 7 new notifications</p>
</li>
<li>
<a href="#">
<span class="label label-danger"><i class="fa fa-bolt"></i></span> Server #3 overloaded.
<span class="small italic">34 mins</span>
</a>
</li>
<li>
<a href="#">
<span class="label label-warning"><i class="fa fa-bell"></i></span> Server #10 not respoding.
<span class="small italic">1 Hours</span>
</a>
</li>
<li>
<a href="#">
<span class="label label-danger"><i class="fa fa-bolt"></i></span> Database overloaded 24%.
<span class="small italic">4 hrs</span>
</a>
</li>
<li>
<a href="#">
<span class="label label-success"><i class="fa fa-plus"></i></span> New user registered.
<span class="small italic">Just now</span>
</a>
</li>
<li>
<a href="#">
<span class="label label-info"><i class="fa fa-bullhorn"></i></span> Application error.
<span class="small italic">10 mins</span>
</a>
</li>
<li>
<a href="#">See all notifications</a>
</li>
</ul>
</li> -->
<!-- notification dropdown end -->
</ul>
