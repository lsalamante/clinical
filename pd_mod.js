var pd_form = $("#pd_form");
var pd_add = $("#pd_add");
var pd_chk_all = $("#pd_chk_all");
var pd_del = $("#pd_del");
var pd_file = $("#pd_file");

pd_add.on('click', function() {
  pd_form[0].reset();
});

pd_chk_all.change(function(){
  this.checked ? check_all(pd_body) :  uncheck_all(pd_body);
});

pd_form.on('submit', function (e){
  e.preventDefault();
  add_pd_info();
});

pd_del.on('click', function(){
  $("#pd_body input:checkbox:checked").each(function() {
    var _id = $(this).attr('id');
    _id = _id.split('-');
    //                      #push the number only
    del_arr.push(_id[1]);
  });

  var form_data = {
    data: "del_arr=" + del_arr.join(",")
  };

  $.ajax({
    url: "php-functions/fncCommon.php?action=delUpInArray",
    type: "POST",
    data: form_data,
    success: function (msg) {

      if(msg == 1){
        //                                    # Clear our array
        del_arr = [];

        show_alert("<?php echo $phrases['deleted']?>", "", 1);

        //                                  #We reload the table
        ajax_load(data, 'pd_body.php', 'pd_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);

      }
      pd_chk_all.prop('checked', false);
    }
  });
});

function add_pd_info(){

  var form_data = new FormData(pd_form[0]);
  form_data.append('project_id', "<?php echo $_GET['project_id'] ?>");
  form_data.append('sub_dir', 'project_documents');
  form_data.append('role_id', '<?php echo $_SESSION['role_id'] ?>');
  form_data.append('up_type', '1'); /* up_type 1 for PD */

  $.ajax({
    url: "php-functions/fncCommon.php?action=uploadFile",
    type:'POST',
    processData: false,
    contentType: false,
    data:form_data,
    success:function(msg){
      if(msg == 1){
        if(status_num == 5 || status_num == 13 || status_num == 9 || status_num == 12){
            $('#status_submit').attr('disabled', false);
        }
        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);
        // <?php echo $phrases['operation_successful']?>
        //                                  #We reload the table
        ajax_load(data, 'pd_body.php', 'pd_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });
}
