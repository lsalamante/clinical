<?php
namespace Clinical\Helpers;
include('jp_library/jp_lib.php');
require("php-functions/fncCommon.php");
require("php-functions/fncProjects.php");
$t = new Translation($_SESSION['lang']);



if (!isset($_SESSION['user_id'])) {
  header("Location: " . "index.php");
  die();
}
#initial page data
//$user_array = getUserById(0, $_SESSION['user_id']);
$profgroup_array = getProfGroup(0);
$departments = getAllDepartments(0);
$trials = getAllTrials(0);

if(!isset($_GET['project_id'])){
  $_GET['project_id'] = false;
}

$project = getProjectById(0, $_GET['project_id']);

$prof_group_users = getPIByProfGroupId(0, $project['pgroup_id']); #get PI for principal investigator field

$pgroup_admin_id = getProfGroupAdminIdByProfGroupId(0, $project['pgroup_id']);

$pgroup_admin = getUserById(0, $pgroup_admin_id);
$pi = getUserById(0, $project['pi_id']);

?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<?php include('header.php'); ?>
<link rel="stylesheet" type="text/css" href="assets/jquery-multi-select/css/multi-select.css" />
<body>
  <section id="container">
    <!--header start-->
    <header class="header white-bg">
      <?php
      if ($LEFT_SIDEBAR) {
        #echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
      }
      ?>
      <!--logo start-->
      <?php if ($LOGO) {
        include('logo.php');
      }
      ?>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <?php if ($NOTIFICATION) {
          include('notification.php');
        } ?>
        <!--  notification end -->
      </div>
      <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
      include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <style>
    span.required
    {
      margin-left: 3px;
      color: red;
    }
    </style>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="panel dgraybg">
          <header class="panel-heading tab-bg-dark-navy-blue ">
            <ul class="nav nav-tabs">
              <li <?php echo !isset($_GET["tab"]) ? 'class="active"' : ''; ?>>
                <a data-toggle="tab" href="#material" class="material"><?php echo $phrases['material']?></a>
              </li>
              <li <?php echo isset($_GET["tab"]) && $_GET["tab"] == "progress" ? 'class="active"' : ''; ?>>
                <a data-toggle="tab" href="#progress" class="progress2"><?php echo $phrases['progress']?></a>
              </li>
              <li <?php echo isset($_GET["tab"]) && $_GET["tab"] == "member" ? 'class="active"' : ''; ?>>
                <a data-toggle="tab" href="#member" class="member2"><?php echo $phrases['member']?></a>
              </li>
              <?php if($_SESSION['user_type'] != 'A'): ?>
                <li>
                  <a data-toggle="tab" href="#job-specification" class="jobspecs"><?php echo $phrases['job_spec']?></a>
                </li>
                <li <?php echo isset($_GET["tab"]) && $_GET["tab"] == "drug-records" ? 'class="active"' : ''; ?>>
                  <a data-toggle="tab" href="#drug-management" class="drugmngt"><?php echo $phrases['drug_management']?></a>
                </li>
                <li <?php echo isset($_GET["tab"]) && $_GET["tab"] == "training" ? 'class="active"' : ''; ?>>
                  <a data-toggle="tab" href="#training-meeting" class="training2"><?php echo $phrases['training_meeting']?></a>
                </li>
                <li <?php echo isset($_GET["tab"]) && $_GET["tab"] == "volunteer" ? 'class="active"' : ''; ?>>
                  <a data-toggle="tab" href="#volunteer" class="volunteer2"><?php echo $phrases['volunteer']?></a>
                </li>
                <li <?php echo isset($_GET["tab"]) && $_GET["tab"] == "subjects" ? 'class="active"' : ''; ?>>
                  <a data-toggle="tab" href="#subjects" class="subjects2"><?php echo $phrases['subjects']?></a>
                </li>
              <?php endif; ?>
              <li <?php echo isset($_GET["tab"]) && $_GET["tab"] == "sideeffect" ? 'class="active"' : ''; ?>>
                <a data-toggle="tab" class="side-effects2" href="#side-effects" onclick="change_se_tab('side'); "><?php echo $phrases['side_effects']?></a>
              </li>
              <li <?php echo isset($_GET["tab"]) && $_GET["tab"] == "sae" ? 'class="active"' : ''; ?>>
                <a data-toggle="tab" class="side-effects2" href="#side-effects" onclick="change_se_tab('sae'); ">SAE</a>
              </li>
              <?php if($_SESSION['user_type'] != 'A'): ?>
                <li <?php echo isset($_GET["tab"]) && $_GET["tab"] == "bio-management" ? 'class="active"' : ''; ?>>
                  <a data-toggle="tab" class="biosample" href="#bio-sample-management"><?php echo $phrases['bio_samp_mgt']?></a>
                </li>
                <li <?php echo isset($_GET["tab"]) && $_GET["tab"] == "stage-summary" ? 'class="active"' : ''; ?>>
                  <a data-toggle="tab" class="summary2" href="#stage-summary"><?php echo $phrases['stage_summary']?></a>
                </li>
              <?php endif; ?>
              <li <?php echo isset($_GET["tab"]) && $_GET["tab"] == "inspection" ? 'class="active"' : ''; ?>>
                <a data-toggle="tab" class="inspection2" href="#inspection____">
                  <?php
                  if($project['status_num'] == 15 && $_SESSION['user_type'] == 'I'){
                    echo "<span style='color:red'>" . $phrases['inspection'] . " <i class='fa fa-exclamation-triangle'></i></span>";
                  }else{
                    echo $phrases['inspection'];
                  }
                  ?>
                </a>
              </li>

              <!-- new tabs without links: TODO temporary by client -->
              <li <?php echo isset($_GET["tab"]) && $_GET["tab"] == "databank" ? 'class="active"' : ''; ?>>
                <a data-toggle="tab" class="databank" href="#data-bank"><?php echo $phrases['data_bank']; ?></a>
              </li>
              <!-- new tabs without links: TODO temporary by client -->


            </ul>
          </header>
          <div class="panel-body contentarea">
            <div class="row">
              <div class="col-lg-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                  <li><a href="dashboard.php"><i class="fa fa-home"></i> <?php echo $phrases['project_information']?></a></li>
                  <li><?php echo $phrases['project_detail']?></li>
                </ul>
                <!--breadcrumbs end -->
              </div>
              <div style="margin-left: 2.5em;"><h3><?php echo $project['project_name']?></h3></div>
            </div>


            <div class="tab-content">
              <div id="material" class="tab-pane <?php echo !isset($_GET["tab"]) ? 'active' : ''; ?>">
                <!-- project material  -->
                <div class="panel-body">

                  <?php include('project.php'); ?>
                  <section class="panel negative-panel">
                    <div class="panel-body">
                      <?php include('remarks.php'); ?>
                    </div>
                    <?php if($_SESSION['user_type'] == 'I' && $project['status_num'] == 14){?>
                      <form id="audit_form">
                        <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
                        <input hidden id="status" name="status" value="1">
                        <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
                        <button type="" class="btn btn-primary" id="status_submit"><?php echo $phrases['submit']; ?></button>
                      </form>
                      <?php }elseif($_SESSION['user_type'] == 'A' && $project['status_num'] == 16){?>
                        <form id="audit_form">
                          <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
                          <input hidden id="status" name="status" value="1">
                          <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
                          <button type="" class="btn btn-primary" id="status_submit"><?php echo $phrases['project_complete']; ?></button>
                        </form>
                        <?php }?>
                      </section>

                      <!-- UPDATE 2/7/2017 VAN : not included anymore. change to another function -->
                      <!-- FOR INVESTIGATOR STUFFS  -->
                      <?php if($project['status_num'] == 13 && in_array($_SESSION['user_type'], ['I', 'CRC'])){ ?>
                        <!-- <form id="audit_form">
                        <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
                        <input hidden id="status" name="status" value="1">
                        <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
                        <section class="panel negative-panel">
                        <div class="panel-body"> -->

                        <!--  For adding project members -->

                        <!-- <section class="panel">
                        <header class="panel-heading">
                        <?php echo $phrases['add_project_members']; ?>
                      </header>

                      <div class="panel-body">
                      <div class="tab-content" style="margin-top: 20px;">

                      <div class="form-group">
                      <label class="control-label col-md-3"><?php echo $phrases['clinician']; ?><span class="required">*</span></label>
                      <div id="clinician_div">
                      <select name="clinician" id="clinician" class="form-control" required>
                      <option value=""><?php echo $phrases['select']; ?></option>
                      <?php foreach (getProfGroupMembers(false, $_SESSION["pgroup_id"], 7) as $user) { ?>
                      <option value="<?php echo $user["role_id"] ?>"><?php echo $user["fname"]." ".$user["lname"]; ?></option>
                      <?php } ?>
                    </select> -->
                    <!-- <p class="help-block" style="display:none;" id="clinician_msg"></p> -->
                    <!--  </div>
                  </div> -->

                  <!--  <div class="form-group">
                  <label class="control-label col-md-3"><?php echo $phrases['research_nurse']; ?><span class="required">*</span></label>
                  <div id="nurse_div">
                  <select name="nurse" id="nurse" class="form-control" required>
                  <option value=""><?php echo $phrases['select']; ?></option>
                  <?php foreach (getProfGroupMembers(false, $_SESSION["pgroup_id"], 9) as $user) { ?>
                  <option value="<?php echo $user["role_id"] ?>"><?php echo $user["fname"]." ".$user["lname"]; ?></option>
                  <?php } ?>
                </select> -->
                <!-- <p class="help-block" style="display:none;" id="nurse_msg"></p> -->
                <!-- </div>
              </div> -->

              <!-- <div class="form-group">
              <label class="control-label col-md-3"><?php echo $phrases['biological_sample_controller']; ?><span class="required">*</span></label>
              <div id="bio_controller_div">
              <select name="bio_controller" id="bio_controller" class="form-control" required>
              <option value=""><?php echo $phrases['select']; ?></option>
              <?php foreach (getProfGroupMembers(false, $_SESSION["pgroup_id"], 5) as $user) { ?>
              <option value="<?php echo $user["role_id"] ?>"><?php echo $user["fname"]." ".$user["lname"]; ?></option>
              <?php } ?>
            </select> -->
            <!-- <p class="help-block" style="display:none;" id="bio_controller_msg"></p> -->
            <!-- </div>
          </div> -->

          <!-- <div class="form-group">
          <label class="col-sm-3 control-label"><?php echo $phrases['medicine_controller']; ?><span class="required">*</span></label>
          <div id="med_controller_div">
          <select name="med_controller" id="med_controller" class="form-control" required>
          <option value=""><?php echo $phrases['select']; ?></option>
          <?php foreach (getProfGroupMembers(false, $_SESSION["pgroup_id"], 6) as $user) { ?>
          <option value="<?php echo $user["role_id"] ?>"><?php echo $user["fname"]." ".$user["lname"]; ?></option>
          <?php } ?>
        </select> -->
        <!-- <p class="help-block" style="display:none;" id="med_controller_msg"></p> -->
        <!--  </div>
      </div>

    </div>
  </div> -->

  <!-- End of Add Member upon status 13 @Rai-->

  <!-- </section> -->

  <!-- <button type="" class="btn btn-primary" id="status_submit"><?php echo $phrases['submit']; ?></button> -->
  <!-- FIXME: Add validations -->
  <!--   </div>
</section>
</form> -->
<?php } ?>
<!-- UPDATE 2/7/2017 VAN : not included anymore. change to another function -->


</div>

<!-- /project material  -->

</div>

<div id="progress" class="tab-pane <?php echo isset($_GET["tab"]) && $_GET["tab"] == "progress" ? 'active' : ''; ?>">
  <div class="panel-body">
    <?php $proj_info = true; include("progress.php"); ?>
  </div>
</div>


<div id="member" class="tab-pane <?php echo isset($_GET["tab"]) && $_GET["tab"] == "member" ? 'active' : ''; ?>">
  <div class="panel-body">
    <?php include("members.php"); ?>

    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="add_member_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg">
        <div class="modal-content pad30">
          <form role="form" class="form-horizontal tasi-form" method="POST" autocomplete="off" id="form_add_member" onsubmit="javascript:;" data-edit="">
            <div class="modal-header nobg nopad" style="background:#64aaf9">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h5 class="alert-modal-title"><?php echo $phrases['add_member']; ?></h5>
            </div>
            <div class="modal-body nopadR nopadL">

              <div class="form-group">
                <label class="control-label col-md-2"><?php echo $phrases['position']; ?><span class="required">*</span></label>
                <div class="col-md-7">
                  <select class="form-control" name="member_position_type" onchange="changePositionDropdown(this.value, <?php echo $_GET['project_id'] ?>);">
                   <option value="" selected><?php echo $t->tryTranslate('choose'); ?></option>
                  <?php
                  $mem_arr = array("BGC", "MC", "SC", "DA");
                  foreach (getPositions(false, true) as $value) {
                    if(in_array($value['acronym'], $mem_arr)){ ?>
                      <option value="<?php echo $value['position_id']; ?>"><?php echo $t->tryTranslate($value['lang_phrase']); ?></option>
                      <?php }
                    } ?>
                  </select>
                </div>
              </div>

              <div class="form-group last" id="prof_group_div">
                <label class="control-label col-md-3"><?php echo $phrases['add_member']; ?><span class="required">*<?php //echo getPIMembersAssign(false, $_GET['project_id'], "BGC"); ?></span></label>
                <label style="margin-left:70px;"><?php echo $t->tryTranslate('available_members'); ?></label>
                <label style="margin-left:150px;"><?php echo $t->tryTranslate('current_project_members'); ?></label>
                <div class="col-md-9" id="multi_select_div">
                  <select name="member_user_id[]" class="multi-select" multiple="multiple" id="my_multi_select3" disabled>
                    <?php foreach (getPIMembersAssign(false, $_GET['project_id'], "BGC") as $user_arr)
                    { ?>
                      <option value="<?php echo $user_arr['user_id']; ?>" <?php echo $user_arr['selected'] == 1 ? "selected" : ""; ?>><?php echo $user_arr['fname']." ".$user_arr['lname']; ?></option>
                      <?php
                    } ?>
                  </select>
                </div>
              </div>



              </div>
              <div class="modal-footer txtcenter nopad">
                <input name="pgroup_id_mem" value="<?php echo $_SESSION['pgroup_id']; ?>" hidden>
                <input name="project_id_mem" value="<?php echo $_GET['project_id']; ?>" hidden>
                <button type="button" class="btn btn-warning" data-dismiss="modal" style="margin-top:5.5px;"><?php echo $phrases['cancel']; ?></button>
                <button class="btn btn-info" id="mem_submit" type="submit" onclick=""><?php echo $phrases['submit']; ?></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div id="job-specification" class="tab-pane">
    <div class="panel-body">
      <?php include 'job_spec.php' ?>
    </div>
  </div>

  <!-- Start of drugs @Rai-->

  <div id="drug-management" class="tab-pane <?php echo isset($_GET["tab"]) && $_GET["tab"] == "drug-records" ? 'active' : ''; ?>">
    <div class="panel-body">
      <section class="panel negative-panel">
        <div class="panel-body">
          <?php include("drug_records.php"); ?>
        </div>
      </section>
    </div>
  </div>

  <!-- End of drugs @Rai-->


  <div id="training-meeting" class="tab-pane <?php echo isset($_GET["tab"]) && $_GET["tab"] == "training" ? 'active' : ''; ?>">
    <div class="panel-body">
      <section class="panel negative-panel">
        <div class="panel-body">
          <?php include("trainings.php"); ?>
        </div>
      </section>
    </div>
  </div>

  <!-- Start of volunteer @Rai-->
  <div id="volunteer" class="tab-pane <?php echo isset($_GET["tab"]) && $_GET["tab"] == "volunteer" ? 'active' : ''; ?>">

    <div class="panel-body">

      <section class="panel negative-panel">
        <div class="panel-body">
          <?php include("volunteer.php"); ?>
        </div>
      </section>
    </div>

  </div>
  <!-- End of volunteer @Rai-->

  <div id="subjects" class="tab-pane <?php echo isset($_GET["tab"]) && $_GET["tab"] == "subjects" ? 'active' : ''; ?>" >
    <div class="panel-body">
      <?php include('subjects.php') ?>
    </div>
  </div>

  <!-- Start of side-effect @Van-->

  <div id="side-effects" class="tab-pane <?php echo isset($_GET["tab"]) && $_GET["tab"] == "sideeffect" ? 'active' : ''; ?>">
    <div class="panel-body">

      <section class="panel negative-panel">
        <div class="panel-body">
          <?php include("side-effect.php"); ?>
        </div>
      </section>
    </div>
  </div>
  <!-- End of side-effect @Van-->


  <?php include("side-effect-modal.php"); ?>
  <div id="bio-sample-management" class="tab-pane <?php echo isset($_GET["tab"]) && $_GET["tab"] == "bio-management" ? 'active' : ''; ?>">
    <?php include("bio_management.php"); ?>
  </div>

  <!-- Start of stage-summary @Rai-->

  <div id="stage-summary" class="tab-pane <?php echo isset($_GET["tab"]) && $_GET["tab"] == "stage-summary" ? 'active' : ''; ?>">
    <div class="panel-body">
      <section class="panel negative-panel">
        <div class="panel-body">
          <?php include("stage-summary.php"); ?>
        </div>
      </section>
    </div>
  </div>

  <!-- Start of stage-summary @Rai-->

  <!-- Start of inspection____ @Rai-->

  <div id="inspection____" class="tab-pane <?php echo isset($_GET["tab"]) && $_GET["tab"] == "inspection" ? 'active' : ''; ?>">
    <div class="panel-body">
      <section class="panel negative-panel">
        <div class="panel-body">
          <?php include("inspection.php"); ?>
        </div>
      </section>
    </div>
  </div>
  <!-- Start of inspection____ @Rai-->

  <!-- Start of user-logs -->
  <div id="user-logs" class="tab-pane <?php echo isset($_GET["tabl"]) && $_GET["tab"] == "user-logs" ? 'active' : '' ?>">
    <div class="panel-body">
      <section class="panel negative-panel">
        <div class="panel-body">
          <?php include("user-logs.php"); ?>
        </div>
      </section>
    </div>
  </div>
  <!-- End of user-logs -->

  <!-- Start of Databank -->
  <div id="data-bank" class="tab-pane <?php echo isset($_GET["tabl"]) && $_GET["tab"] == "data-bank" ? 'active' : '' ?>">
    <div class="panel-body">
      <section class="panel negative-panel">
        <div class="panel-body">
          <?php include("databank.php"); ?>
        </div>
      </section>
    </div>
  </div>
  <!-- End of Databank -->

</div>
</div>
</section>
<!-- page end-->
</section>
</section>
<!--main content end-->
<!-- Right Slidebar start -->
<?php
if ($RIGHT_SIDEBAR) {
  include('right-sidebar.php');
}
?>
<!-- Right Slidebar end -->
<!--footer start-->
<?php include('footer.php'); ?>
<!--footer end-->
</section>
<?php $PICKERS = TRUE; ?>
<?php include('scripts.php'); ?>

<script type="text/javascript" src="assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="assets/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/jquery-multi-select/js/jquery.quicksearch.js"></script>
<!--common script for all pages-->
<script src="js/common-scripts.js"></script>
<!--this page  script only-->
<!-- <script src="js/advanced-form-components.js"></script> -->





<!-- Start of Add Volunteer Scripts  @Rai-->

<script type="text/javascript" src="js/checker.js"></script>
<script type="text/javascript" src="js-functions/volunteer.js"></script>
<!-- <script type="text/javascript" src="js-functions/drugs.js"></script> -->
<script type="text/javascript" src="js-functions/members.js"></script>
<script type="text/javascript" src="js-functions/drug-records.js"></script>
<script type="text/javascript" src="js-functions/side-effect.js"></script>
<script type="text/javascript" src="js-functions/training.js"></script>
<!-- <script type="text/javascript" src="js-functions/bio.js"></script> -->
<!-- <script type="text/javascript" src="js-functions/stage-summary.js"></script> -->
<script type="text/javascript" src="js-functions/inspection.js"></script>
<script type="text/javascript" src="js-functions/subjects.js"></script>
<script type="text/javascript" src="subj_mod.js"></script>

<!-- End of Add Volunteer Scripts  @Rai-->
<!--this page  script only-->
<script src="js/advanced-form-components.js"></script>


<script>
$(document).ready(function () {
  //FIXME: pg_audit.php?tab=volunteer&project_id=1:3157 Uncaught TypeError: Cannot set property 'innerHTML' of null(…)xmlhttp.onreadystatechange @ pg_audit.php?tab=volunteer&project_id=1:3157
  /*
  subject_status
  *status_id
  *subject_id
  *status
  *status_date
  *remarks
  wait
  subject_status
  S - selecting
  P - pass
  NP - no pass
  C - choose
  (update status of subject as 1 - means, not available for other projects)
  B - begin
  FU - follow up
  STP - stop
  O - out
  D - drop
  F - Finish (once finish, update volunteer status as 0 - make available to other projects) */

  <?php
  include 'initialize.js';
  include 'audit.js';
  include 'side_effects_mod.js';
  include 'disable_project.js';
  include 'job_mod.js';
  include 'drug_mod.js';
  include 'vol_mod.js';
  include 'ecm_mod.js';
  // include 'subj_mod.js';
  include 'pd_mod.js'; //NOTE: this is for uploading in status #13

  # NOTE: We are doing this to make sure the website doesn't load unecessary js files  which
  # can cause errors and shit
  $_GET['bio_detail'] = (empty($_GET['bio_detail'])) ? '' : $_GET['bio_detail'];
  switch($_GET['bio_detail']){
    case 'handle':  include 'bio_handle_mod.js';
    break;
    case 'save': include 'bio_save_mod.js';
    break;
    case 'transport': include "bio_transport_mod.js";
    break;
    case 'determine': include 'bio_determine_mod.js';
    break;
    case 'recycle': include 'bio_recycle_mod.js';
    break;
    case 'destroy': include 'bio_destroy_mod.js';
    break;
    default: include 'bgc_mod.js';
    break;
  }
  ?>

  //It's practically the same thing, but restricts user type to Investigator
  if("<?php echo $_SESSION['user_type']?>" == 'A' && status_num == 14 || "<?php echo $_SESSION['user_type']?>" == 'A' && status_num == 15 || "<?php echo $_SESSION['user_type']?>" == 'I' && status_num == 14){
    $('#pd_add').attr("href", "#pd_modal");
    $('#pd_add').show();
    $('#pd_del').show();
  }


  /* END DCUMENT RDY*/
});
</script>
<script>

notifyOwner = function(project_id, user_type, omodule){
  $.ajax({
    url:"ajax/double_check/notify.php",
    type: "POST",
    data: {"project_id": project_id, "user_type": user_type, "omodule": omodule},
    success:function(data){ // data is the id of the row
      console.log(data);
    },
    error: function(e){
      console.log(e);
    }
  });

  $(".modal").hide();
  $(".modal-backdrop").hide();
  show_alert('<?php echo $t->tryTranslate('operation_successful'); ?>', '', 1);

}

$("#submitSideEffects").on('click', function (event) {
      $('#is_drafted_se').val("0");
      add_side_effect("submitSideEffects");
    });

    $("#draftSideEffects").on('click', function (event) {
      $('#is_drafted_se').val("1");
      add_side_effect("draftSideEffects");
    });
function change_se_tab(side)
{
  $('#div_SAE').attr('style', 'display:block;');$('#is_sae').val(1);
  if(side != 'sae')
  {
    $('#div_SAE').attr('style', 'display:hidden;');$('#is_sae').val(0);
  }
  var data = {
    project_id:<?php echo $_GET['project_id']; ?>,
    side_effect: side
  };
  ajax_load(data, "sideeffects_table_tab.php", "se_tbody");
}
// CHANGE PI member position
function changePositionDropdown(position_id, project_id)
{
  var data={
    position_id:position_id,
    project_id:project_id
  };
  if(position_id != "")
  {
    $('#my_multi_select3').removeAttr('disabled');
    $('.ms-elem-selectable').removeClass('disabled');
    ajax_load(data, "change_pi_position.php", "multi_select_div");
  }
  else
  {
    ajax_load(data, "change_pi_position.php", "multi_select_div");
  }

}

// MIGRATE BIO.JS HERE BECAUSE WE NEED THE POWER OF PHP!!!!!

function clearDblCheck(textarea_id, check_value)
{
  if(check_value == "1")
  {
    $('#'+textarea_id).attr("readonly", "readonly");
    $('#'+textarea_id).addClass("hidden");
    $('.dbl_chk_label').addClass("hidden");
    $('#'+textarea_id).val("");
  }
  else
  {
    $('#'+textarea_id).removeAttr("readonly", "readonly");
    $('.dbl_chk_label').removeClass("hidden");
    $('#'+textarea_id).removeClass("hidden");

  }
}

$("#side_effects_dbl_chk_form").on('submit', function(e){ //modify payload data using ajax
  e.preventDefault();

  $.ajax({
    url:"ajax/double_check/updatePayload.php",
    type: "POST",
    dataType: "json",
    data: $("#side_effects_dbl_chk_form").serialize(),
    success:function(data){ // data is the id of the row
        $("#side_effects_dbl_chk_modal ").modal("hide");
        getSideEffectDetails(data, event, "<?php echo $_SESSION['user_type']; ?>", true); // rename this // Trigger ulit yung onclick event use other function
    }
  });
});

side_effectsSetPayload = function(caller){ //Set the payload key to be edited
  $("#side_effects_dbl_chk_form")[0].reset();

  $("#side_effects_payload_key").val($(caller).data('key'));
  //for setting default comments
  $("#side_effects_comment").text($(caller).data('comments'));

  //for setting default value of checkbox
  if($(caller).data('is_checked') == 1){
    $("#side_effects_dbl_chk_pass").attr('checked', true);
    $("#side_effects_dbl_chk_nopass").attr('checked', false);
  }else if($(caller).data('is_checked') == -1)  {
    $("#side_effects_dbl_chk_nopass").attr('checked', true);
    $("#side_effects_dbl_chk_pass").attr('checked', false);
  }else{
    $("#side_effects_dbl_chk_nopass").attr('checked', false);
    $("#side_effects_dbl_chk_pass").attr('checked', false);
  }

}
// Payload FOR side_effects

function getSideEffectDetails(sideeffect_id, event, user, from_dbl)
{
  // if(user != 'RN' )
  // {
  //   $('#submitSideEffects').attr('style', 'display:none');
  // }
  if(event.target.nodeName != "A" && event.target.nodeName != "I")
  {
    $('#side_effect_id_update').val(sideeffect_id);
      $.ajax({
        url:"php-functions/fncSideEffect.php?action=getSideEffectDetails",
        type: "POST",
        dataType: "json",
        data: { "side_effect_id" : sideeffect_id, "type" : "ajax" },
        success:function(data){
          // Put this dun sa ajax success ng onclick nung record
          <?php if (in_array($_SESSION['user_type'], ['DA', 'SC', 'I'])):?> // user types na pwedeng maka view
          // console.log(data);
          var payload = JSON.parse(data[0].dbl_chk_payload);

          var fields_arr = { // keyname sa db on the left, id nung input sa right
            "lot_num":"#lot_num_s",
            "num":"#num",
            "report_time":"#report_time",
            "hospital":"#hospital",
            "hospital_mobile": "#hospital_mobile",
            "dept":"#dept",
            "dept_mobile":"#dept_mobile",
            "drug_chinese_name":"#drug_chinese_name",
            "drug_english_name":"#drug_english_name",
            "drug_type":"#drug_type_dbl",
            "drug_form":"#drug_form",
            "drug_register":"#drug_register",
            "clinical_trial":"#clinical_trial_dbl",
            "clinical_indications":"#clinical_indications",
            "subject_id":"#se_subj_select",
            "sub_initials":"#sub_initials",
            "sub_gender":"#sub_gender_dbl",
            "sub_height":"#sub_height",
            "sub_weight":"#sub_weight",
            "sub_birthdate":"#sub_birthdate",
            "sub_complication":"#sub_complication_dbl",
            "sae_diagnosis":"#sae_diagnosis",
            "sae_drug_measure":"#sae_drug_measure_dbl",
            "sae_situation":"#sae_situation_dbl",
            "relation_sae_drug":"#relation_sae_drug_dbl",
            "sae_vest":"#sae_vest_dbl",
            "sae_unbinding":"#sae_unbinding_dbl",
            "sae_report_internal":"#sae_report_internal_dbl",
            "sae_report_external":"#sae_report_external_dbl",
            "sae_handle_detail":"#sae_handle_detail",
            "report_unit":"#report_unit",
            "reporter_position":"#reporter_position",
            "remark":"#side_remark"
          }

          $(".dbl_chk").remove(); $(".dbl_chk_readonly").remove();
          $("#side_effects_dbl_chk_id").val(data[0].side_effect_id);

          //todo: disable comments when pass is clicked
          // nothing to change here
          for(var key in fields_arr){
            <?php if($_SESSION['user_type'] == 'DA'): ?>
            if(payload[key]['is_checked'] == 0){
              $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'">复核</a>');
            }else
            <?php endif; ?>
             if(payload[key]['is_checked'] == -1){
              $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-times"></i> 复核不通过</a>');
            }else if(payload[key]['is_checked'] == 1){
              $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary <?php echo ($_SESSION['user_type'] == 'DA') ? 'dbl_chk' : 'dbl_chk_readonly'; ?>" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-check"></i> 复核通过</a>');
            }
          }

          $(".dbl_chk").on("click", function(){
            side_effectsSetPayload(this); // CHANGE THE NAME OF THIS FUNCTION for example, meetingSetPayload
            $(".modal-backdrop").remove();
            var drugListModal = $('#addSideEffectModal').modal(); // change the modal name according to the last model opened
            drugListModal.hide();
            var drugModal = $('#side_effects_dbl_chk_modal').modal();
            drugModal.removeData('bs.modal');
            drugModal.modal("show");
          });

          $('#side_effects_dbl_chk_modal').on('hidden.bs.modal', function () { // Show drug list modal when add drug modal closed
            $(".modal-backdrop").remove();
            var dialog = $('#addSideEffectModal').modal();
            dialog.removeData('bs.modal');
            dialog.modal("show");
          });

          <?php endif; ?>
          // inputs
          $('#lot_num_s').val(data[0].lot_num);
          $('#num').val(data[0].num);
          $('#report_time').val(data[0].report_time);
          $('#hospital').val(data[0].hospital);
          $('#hospital_mobile').val(data[0].hospital_mobile);
          $('#dept').val(data[0].dept);
          $('#dept_mobile').val(data[0].dept_mobile);
          $('#drug_chinese_name').val(data[0].drug_chinese_name);
          $('#drug_english_name').val(data[0].drug_english_name);
          $('#drug_form').val(data[0].drug_form);
          $('#drug_register').val(data[0].drug_register);
          $('#clinical_indications').val(data[0].clinical_indications);
          $('#sub_initials').val(data[0].sub_initials);
          $('#sub_height').val(data[0].sub_height);
          $('#sub_weight').val(data[0].sub_weight);
          $('#sub_birthdate').val(data[0].sub_birthdate);
          $('#sae_diagnosis').val(data[0].sae_diagnosis);
          $('#sae_handle_detail').val(data[0].sae_handle_detail);
          $('#report_unit').val(data[0].report_unit);
          $('#reporter_position').val(data[0].reporter_position);
          $('#remark').val(data[0].remark);

          // radio
          $('#drug_type_'+data[0].drug_type).prop("checked", true);
          $('#clinical_trial_'+data[0].clinical_trial).prop("checked", true);
          $('#sub_complications_'+data[0].sub_complication).prop("checked", true);
          $('#sae_drug_measure_'+data[0].sae_drug_measure).prop("checked", true);
          $('#sae_situation_'+data[0].sae_situation).prop("checked", true);
          $('#relation_sae_drug_'+data[0].relation_sae_drug).prop("checked", true);
          $('#sae_vest_'+data[0].sae_vest).prop("checked", true);
          $('#sae_unbinding_'+data[0].sae_unbinding).prop("checked", true);
          $('#sae_report_internal_'+data[0].sae_report_internal).prop("checked", true);
          $('#sae_report_external_'+data[0].sae_report_external).prop("checked", true);
          if(data[0].sub_gender == 0)
          {
            $('#sub_gender_m').prop("checked", true);
          }
          else
          {
            $('#sub_gender_f').prop("checked", true);
          }
          // checkbox
          var x = 0;
          for(x=0; x<data[0].report_type.length; x++)
          {
            $('#report_type_'+data[0].report_type[x]).prop('checked', true);
          }

          // select
          $('#se_subj_select').val(data[0].subject_id);
          // $('#se_subj_id_'+data[0].subject_id).attr('selected', 'selected');

          $('#side_effect_id_update').val(sideeffect_id);
          $('#submitSideEffects').html('更新');
          $('#addSideEffectModal').modal('show');
          var se_data = { "side_effect_id" : sideeffect_id };
          ajax_load(se_data, 'table_side_effect_upload.php', 'table_side_effect_upload');
        }
      });
  }
}


function getBioDetails(bio_id, event)
{
  if(event.target.nodeName != "A" && event.target.nodeName != "I")
  {
    $('#bio_id_update').val(bio_id);
    $.ajax({
      url:"php-functions/fncBioSamples.php?action=getBioDetails",
      type: "POST",
      dataType: "json",
      data: { "bio_id" : bio_id, "type" : "ajax" },
      success:function(data){

        //DOUBLE CHECK STUFF BGC
        <?php if (in_array($_SESSION['user_type'], ['DA', 'BGC'])):?>
        var payload = JSON.parse(data[0].dbl_chk_payload);
        // alert(JSON.stringify(payload));

        var fields_arr = {
          "name" : "#bgc_name",
          "unit" : "#bgc_unit",
          'quantity' : '#bgc_quantity',
          'collect_start' : '#bio_collect_start',
          'collect_end' : '#bio_collect_end',
          'description' : '#bgc_description',
          'collect_record' : '#update_collect_record',
        }

        $(".dbl_chk").remove(); $(".dbl_chk_readonly").remove();
        $("#bgc_dbl_chk_id").val(data[0].bio_id);

        //todo: disable comments when pass is clicked
        for(var key in fields_arr){
          <?php if($_SESSION['user_type'] == 'DA'): ?>
          if(payload[key]['is_checked'] == 0){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"> 复核</a>');
          }else
          <?php endif; ?>
           if(payload[key]['is_checked'] == -1){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-times"></i> 复核不通过</a>');
          }else if(payload[key]['is_checked'] == 1){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary <?php echo ($_SESSION['user_type'] == 'DA') ? 'dbl_chk' : 'dbl_chk_readonly'; ?>" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-check"></i> 复核通过</a>');
          }
        }

        $(".dbl_chk").on("click", function(){
          bgcSetPayload(this);
          $(".modal-backdrop").remove();
          var drugListModal = $('#bgc_modal').modal();
          drugListModal.hide();
          var drugModal = $('#bgc_dbl_chk_modal').modal();
          drugModal.removeData('bs.modal');
          drugModal.modal("show");
        });

        $('#bgc_dbl_chk_modal').on('hidden.bs.modal', function () { // Show drug list modal when add drug modal closed
          $(".modal-backdrop").remove();
          var dialog = $('#bgc_modal').modal();
          dialog.removeData('bs.modal');
          dialog.modal("show");
        });

        <?php endif; ?>
        //DOUBLE CHECK STUFF BGC END

        $('#bgc_submit').html('更新');
        // $('#collect_record').attr('style', 'display:none'); # deprecated
        $('#collect_record').val(""); // remove value of file
        $('#bgc_name').val(data[0].name);
        $('#bgc_quantity').val(data[0].quantity);
        $('#bgc_unit').val(data[0].unit);
        $('#bio_collect_start').val(data[0].collect_start);
        $('#bio_collect_end').val(data[0].collect_end);
        $('#bgc_description').val(data[0].description);
        $('#update_collect_record').attr('href', data[0].collect_record);
        $('#update_collect_record').attr("style", "display:block;width:19%;margin:10px 0px;");
        // $('#update_process_record').attr('href', data[0].process_record);
        // $('#update_process_record').attr('style', "display:inline");
        $('#bgc_modal').modal('show');
      },
      error:function(data)
      {
        var acc = []
        $.each(data, function(index, value) {
            acc.push(index + ': ' + value);
        });
        alert(JSON.stringify(acc));
      }
    });
  }
}

// REMOVE DOUBLE CHeCK FIELDS
function rmDoubleCheck(){
  $(".dbl_chk").remove(); $(".dbl_chk_readonly").remove();
}

function getTransportDetails(transport_id, event)
{
  if(event.target.nodeName != "A" && event.target.nodeName != "I")
  {
    $('#transport_id_update').val(transport_id);
    $.ajax({
      url:"php-functions/fncBioTransport.php?action=getBioDetails",
      type: "POST",
      dataType: "json",
      data: { "transport_id" : transport_id, "type" : "ajax" },
      success:function(data){

        //DOUBLE CHECK STUFF transport
        <?php if (in_array($_SESSION['user_type'], ['DA', 'BGC'])):?>
        var payload = JSON.parse(data[0].dbl_chk_payload);

        var fields_arr = {
          "name" : "#transport_name",
          "unit" : "#transport_unit",
          'quantity' : '#transport_quantity',
          'transport_start' : '#transport_start',
          'transport_end' : '#transport_end',
          'description' : '#transport_description',
          'process_record' : '#process_record',
          'transport_company' : '#transport_company',
          'transport_temp' : '#transport_temp',
          'transport_humidity' : '#transport_humidity',
        }

        $(".dbl_chk").remove(); $(".dbl_chk_readonly").remove();
        $("#transport_dbl_chk_id").val(data[0].transport_id);

        //todo: disable comments when pass is clicked
        for(var key in fields_arr){

          <?php if($_SESSION['user_type'] == 'DA'): ?>
          if(payload[key]['is_checked'] == 0){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"> 复核</a>');
          }else
          <?php endif; ?>
           if(payload[key]['is_checked'] == -1){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-times"></i> 复核不通过</a>');
          }else if(payload[key]['is_checked'] == 1){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary <?php echo ($_SESSION['user_type'] == 'DA') ? 'dbl_chk' : 'dbl_chk_readonly'; ?>" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-check"></i> 复核通过</a>');
          }
        }

        $(".dbl_chk").on("click", function(){
          transportSetPayload(this);
          $(".modal-backdrop").remove();
          var drugListModal = $('#transport_modal').modal();
          drugListModal.hide();
          var drugModal = $('#transport_dbl_chk_modal').modal();
          drugModal.removeData('bs.modal');
          drugModal.modal("show");
        });

        $('#transport_dbl_chk_modal').on('hidden.bs.modal', function () { // Show drug list modal when add drug modal closed
          $(".modal-backdrop").remove();
          var dialog = $('#transport_modal').modal();
          dialog.removeData('bs.modal');
          dialog.modal("show");
        });

        <?php endif; ?>
        //DOUBLE CHECK STUFF transport END


        $('#transport_submit').html('更新');
        // $('#process_record').attr('style', 'display:none');
        $('#process_record').val('');
        $('#transport_name').val(data[0].name);
        $('#transport_quantity').val(data[0].quantity);
        $('#transport_unit').val(data[0].unit);
        $('#transport_company').val(data[0].transport_company);
        $('#transport_temp').val(data[0].transport_temp);
        $('#transport_humidity').val(data[0].transport_humidity);
        $('#transport_start').val(data[0].transport_start);
        $('#transport_end').val(data[0].transport_end);
        $('#transport_description').val(data[0].description);
        $('#update_transport_process_record').attr('href', data[0].process_record);
        $('#update_transport_process_record').attr("style", "display:block;width:19%;margin:10px 0px;");
        $('#transport_modal').modal('show');
      }
    });
  }
}

getDetermineDetails = function (determine_id, event)
{
  if(event.target.nodeName != "A" && event.target.nodeName != "I")
  {
    $('#determine_id_update').val(determine_id);
    $.ajax({
      url:"php-functions/fncBioDetermine.php?action=getBioDetails",
      type: "POST",
      dataType: "json",
      data: { "determine_id" : determine_id, "type" : "ajax" },
      success:function(data){

        //DOUBLE CHECK STUFF determine
        <?php if (in_array($_SESSION['user_type'], ['DA', 'BGC'])):?>
        var payload = JSON.parse(data[0].dbl_chk_payload);

        var fields_arr = {
          "name" : "#determine_name",
          "unit" : "#determine_unit",
          'quantity' : '#determine_quantity',
          'determine_start' : '#determine_start',
          'determine_end' : '#determine_end',
          'description' : '#determine_description',
          'determine_record' : '#determine_record',
          'determine_report' : '#determine_report',
          'determine_company' : '#determine_company',
        }

        $(".dbl_chk").remove(); $(".dbl_chk_readonly").remove();
        $("#determine_dbl_chk_id").val(data[0].determine_id);

        //todo: disable comments when pass is clicked
        for(var key in fields_arr){
          <?php if($_SESSION['user_type'] == 'DA'): ?>
          if(payload[key]['is_checked'] == 0){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"> 复核</a>');
          }else
          <?php endif; ?>
           if(payload[key]['is_checked'] == -1){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-times"></i> 复核不通过</a>');
          }else if(payload[key]['is_checked'] == 1){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary <?php echo ($_SESSION['user_type'] == 'DA') ? 'dbl_chk' : 'dbl_chk_readonly'; ?>" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-check"></i> 复核通过</a>');
          }
        }

        $(".dbl_chk").on("click", function(){
          determineSetPayload(this);
          $(".modal-backdrop").remove();
          var drugListModal = $('#determine_modal').modal();
          drugListModal.hide();
          var drugModal = $('#determine_dbl_chk_modal').modal();
          drugModal.removeData('bs.modal');
          drugModal.modal("show");
        });

        $('#determine_dbl_chk_modal').on('hidden.bs.modal', function () { // Show drug list modal when add drug modal closed
          $(".modal-backdrop").remove();
          var dialog = $('#determine_modal').modal();
          dialog.removeData('bs.modal');
          dialog.modal("show");
        });

        <?php endif; ?>
        //DOUBLE CHECK STUFF determine

        $('#determine_submit').html('更新');
        // $('#determine_record').attr('style', 'display:none');
        // $('#determine_report').attr('style', 'display:none');
        $('#determine_record').val('');
        $('#determine_report').val('');
        $('#determine_name').val(data[0].name);
        $('#determine_quantity').val(data[0].quantity);
        $('#determine_unit').val(data[0].unit);
        $('#determine_company').val(data[0].determine_company);
        $('#determine_start').val(data[0].determine_start);
        $('#determine_end').val(data[0].determine_end);
        $('#determine_description').val(data[0].description);
        $('#update_determine_record').attr('href', data[0].determine_record);
        $('#update_determine_record').attr("style", "display:block;width:19%;margin:10px 0px;");
        $('#update_determine_report').attr('href', data[0].determine_report);
        $('#update_determine_report').attr("style", "display:block;width:19%;margin:10px 0px;");
        $('#determine_modal').modal('show');
      }
    });
  }
}

getBioHandleDetails = function (bio_handle_id, event)
{
  if(event.target.nodeName != "A" && event.target.nodeName != "I")
  {
    $('#bio_handle_id_update').val(bio_handle_id);
    $.ajax({
      url:"php-functions/fncBioHandles.php?action=getBioDetails",
      type: "POST",
      dataType: "json",
      data: { "bio_handle_id" : bio_handle_id, "type" : "ajax" },
      success:function(data){

        //DOUBLE CHECK STUFF Bio handle
        <?php if (in_array($_SESSION['user_type'], ['DA', 'BGC'])):?>
        var payload = JSON.parse(data[0].dbl_chk_payload);

        var fields_arr = {
          "name" : "#bio_handle_name",
          "unit" : "#bio_handle_unit",
          'quantity' : '#bio_handle_quantity',
          'handle_start' : '#handle_start',
          'handle_end' : '#handle_end',
          'description' : '#bio_handle_description',
          'handle_record' : '#update_handle_record',
        }

        $(".dbl_chk").remove(); $(".dbl_chk_readonly").remove();
        $("#bio_handle_dbl_chk_id").val(data[0].bio_handle_id);

        //todo: disable comments when pass is clicked
        for(var key in fields_arr){
          <?php if($_SESSION['user_type'] == 'DA'): ?>
          if(payload[key]['is_checked'] == 0){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"> 复核</a>');
          }else
          <?php endif; ?>
           if(payload[key]['is_checked'] == -1){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-times"></i> 复核不通过</a>');
          }else if(payload[key]['is_checked'] == 1){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary <?php echo ($_SESSION['user_type'] == 'DA') ? 'dbl_chk' : 'dbl_chk_readonly'; ?>" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-check"></i> 复核通过</a>');
          }
        }

        $(".dbl_chk").on("click", function(){
          handleSetPayload(this);
          $(".modal-backdrop").remove();
          var drugListModal = $('#bio_handle_modal').modal();
          drugListModal.hide();
          var drugModal = $('#bio_handle_dbl_chk_modal').modal();
          drugModal.removeData('bs.modal');
          drugModal.modal("show");
        });

        $('#bio_handle_dbl_chk_modal').on('hidden.bs.modal', function () { // Show drug list modal when add drug modal closed
          $(".modal-backdrop").remove();
          var dialog = $('#bio_handle_modal').modal();
          dialog.removeData('bs.modal');
          dialog.modal("show");
        });

        <?php endif; ?>
        //DOUBLE CHECK STUFF Bio handles END

        $('#bio_handle_submit').html('更新');
        // $('#handle_record').attr('style', 'display:none'); #deprecated
        $('#handle_record').val(""); // remove value of file
        $('#bio_handle_name').val(data[0].name);
        $('#bio_handle_quantity').val(data[0].quantity);
        $('#bio_handle_unit').val(data[0].quantity);
        $('#handle_start').val(data[0].handle_start);
        $('#handle_end').val(data[0].handle_end);
        $('#bio_handle_description').val(data[0].description);
        $('#update_handle_record').attr('href', data[0].handle_record);
        $('#update_handle_record').attr("style", "display:block;width:19%;margin:10px 0px;");
        $('#bio_handle_modal').modal('show');
      }
    });
  }
}

getBioSaveDetails = function (bio_save_id, event)
{
  if(event.target.nodeName != "A" && event.target.nodeName != "I")
  {
    $('#bio_save_id_update').val(bio_save_id);
    $.ajax({
      url:"php-functions/fncBioSave.php?action=getBioSaveDetails",
      type: "POST",
      dataType: "json",
      data: { "bio_save_id" : bio_save_id, "type" : "ajax" },
      success:function(data){

        //DOUBLE CHECK STUFF Bio save
        <?php if (in_array($_SESSION['user_type'], ['DA', 'BGC'])):?>
        var payload = JSON.parse(data[0].dbl_chk_payload);

        var fields_arr = {
          "name" : "#bio_save_name",
          "unit" : "#bio_save_unit",
          'quantity' : '#bio_save_quantity',
          'collect_start' : '#bio_save_collect_start',
          'collect_end' : '#bio_save_collect_end',
          'address' : '#bio_save_address',
          'temp' : '#bio_save_temp',
          'humidity' : '#bio_save_humidity',
          'record' : '#bio_save_record',
          'bio_desc' : '#bio_save_desc',
        }

        $(".dbl_chk").remove(); $(".dbl_chk_readonly").remove();
        $("#bio_save_dbl_chk_id").val(data[0].bio_save_id);

        //todo: disable comments when pass is clicked
        for(var key in fields_arr){
          <?php if($_SESSION['user_type'] == 'DA'): ?>
          if(payload[key]['is_checked'] == 0){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"> 复核</a>');
          }else
          <?php endif; ?>
           if(payload[key]['is_checked'] == -1){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-times"></i> 复核不通过</a>');
          }else if(payload[key]['is_checked'] == 1){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary <?php echo ($_SESSION['user_type'] == 'DA') ? 'dbl_chk' : 'dbl_chk_readonly'; ?>" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-check"></i> 复核通过</a>');
          }
        }

        $(".dbl_chk").on("click", function(){
          saveSetPayload(this);
          $(".modal-backdrop").remove();
          var drugListModal = $('#bio_save_modal').modal();
          drugListModal.hide();
          var drugModal = $('#bio_save_dbl_chk_modal').modal();
          drugModal.removeData('bs.modal');
          drugModal.modal("show");
        });

        $('#bio_save_dbl_chk_modal').on('hidden.bs.modal', function () { // Show drug list modal when add drug modal closed
          $(".modal-backdrop").remove();
          var dialog = $('#bio_save_modal').modal();
          dialog.removeData('bs.modal');
          dialog.modal("show");
        });

        <?php endif; ?>
        //DOUBLE CHECK STUFF Bio save END

        $('#bio_save_form')[0].reset();
        $('#bio_save_submit').html('更新');
        $('#bio_save_name').val(data[0].name);
        $('#bio_save_record').val('');
        $('#bio_save_quantity').val(data[0].quantity);
        $('#bio_save_unit').val(data[0].unit);
        $('#bio_save_collect_start').val(data[0].collect_start);
        $('#bio_save_collect_end').val(data[0].collect_end);
        $('#bio_save_address').val(data[0].address);
        $('#bio_save_temp').val(data[0].temp);
        $('#bio_save_humidity').val(data[0].humidity);
        $('#bio_save_record_a').attr('href', data[0].record);
        $('#bio_save_record_a').attr("style", "display:block;width:19%;margin:10px 0px;");
        $('#bio_save_desc').val(data[0].bio_desc);
        $('#bio_save_id_update').val(bio_save_id);
        $('#bio_save_modal').modal('show');
      }
    });
  }
}

getBioRecycleDetails = function (bio_recycle_id, event)
{
  if(event.target.nodeName != "A" && event.target.nodeName != "I")
  {
    $('#bio_recycle_id_update').val(bio_recycle_id);
    $.ajax({
      url:"php-functions/fncBioRecycle.php?action=getBioRecycleDetails",
      type: "POST",
      dataType: "json",
      data: { "bio_recycle_id" : bio_recycle_id, "type" : "ajax" },
      success:function(data){

        //DOUBLE CHECK STUFF recycle
        <?php if (in_array($_SESSION['user_type'], ['DA', 'BGC'])):?>
        var payload = JSON.parse(data[0].dbl_chk_payload);

        var fields_arr = {
          "name" : "#bio_recycle_name",
          "unit" : "#bio_recycle_unit",
          'quantity' : '#bio_recycle_quantity',
          'sending_party' : '#bio_recycle_sending_party',
          'sending_operator' : '#bio_recycle_sending_operator',
          'sending_date' : '#bio_recycle_sending_date',
          'shipper' : '#bio_recycle_shipper',
          'shipper_operator' : '#bio_recycle_shipper_operator',
          'shipper_date' : '#bio_recycle_shipper_date',
          'recycler' : '#bio_recycle_recycler',
          'recycler_operator' : '#bio_recycle_recycler_operator',
          'recycler_date' : '#bio_recycle_recycler_date',
          'recycle_record' : '#recycle_record',
          'bio_desc' : '#bio_recycle_desc',
        }

        $(".dbl_chk").remove(); $(".dbl_chk_readonly").remove();
        $("#recycle_dbl_chk_id").val(data[0].bio_recycle_id);

        //todo: disable comments when pass is clicked
        for(var key in fields_arr){
          <?php if($_SESSION['user_type'] == 'DA'): ?>
          if(payload[key]['is_checked'] == 0){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"> 复核</a>');
          }else
          <?php endif; ?>
           if(payload[key]['is_checked'] == -1){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-times"></i> 复核不通过</a>');
          }else if(payload[key]['is_checked'] == 1){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary <?php echo ($_SESSION['user_type'] == 'DA') ? 'dbl_chk' : 'dbl_chk_readonly'; ?>" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-check"></i> 复核通过</a>');
          }
        }

        $(".dbl_chk").on("click", function(){
          recycleSetPayload(this);
          $(".modal-backdrop").remove();
          var drugListModal = $('#bio_recycle_modal').modal();
          drugListModal.hide();
          var drugModal = $('#recycle_dbl_chk_modal').modal();
          drugModal.removeData('bs.modal');
          drugModal.modal("show");
        });

        $('#recycle_dbl_chk_modal').on('hidden.bs.modal', function () { // Show drug list modal when add drug modal closed
          $(".modal-backdrop").remove();
          var dialog = $('#bio_recycle_modal').modal();
          dialog.removeData('bs.modal');
          dialog.modal("show");
        });

        <?php endif; ?>
        //DOUBLE CHECK STUFF recycle END


        $('#bio_recycle_form')[0].reset();
        $('#bio_recycle_submit').html('更新');
        $('#bio_recycle_name').val(data[0].name);
        $('#recycle_record').val('');
        $('#bio_recycle_quantity').val(data[0].quantity);
        $('#bio_recycle_unit').val(data[0].unit);
        $('#bio_recycle_sending_party').val(data[0].sending_party);
        $('#bio_recycle_sending_operator').val(data[0].sending_operator);
        $('#bio_recycle_sending_date').val(data[0].sending_date);
        $('#bio_recycle_shipper').val(data[0].shipper);
        $('#bio_recycle_shipper_operator').val(data[0].shipper_operator);
        $('#bio_recycle_shipper_date').val(data[0].shipper_date);
        $('#bio_recycle_recycler').val(data[0].recycler);
        $('#bio_recycle_recycler_operator').val(data[0].recycler_operator);
        $('#bio_recycle_recycler_date').val(data[0].recycler_date);
        $('#bio_recycle_record_a').attr("href", data[0].recycle_record);
        $('#bio_recycle_record_a').attr("style", "display:block;width:19%;margin:10px 0px;");
        $('#bio_recycle_desc').val(data[0].bio_desc);
        $('#bio_recycle_modal').modal('show');
      }
    });
  }
}

getBioDestroyDetails = function (bio_destroy_id, event)
{
  if(event.target.nodeName != "A" && event.target.nodeName != "I")
  {
    $('#bio_destroy_id_update').val(bio_destroy_id);
    $.ajax({
      url:"php-functions/fncBioDestroy.php?action=getBioDestroyDetails",
      type: "POST",
      dataType: "json",
      data: { "bio_destroy_id" : bio_destroy_id, "type" : "ajax" },
      success:function(data){


        //DOUBLE CHECK STUFF destroy
        <?php if (in_array($_SESSION['user_type'], ['DA', 'BGC'])):?>
        var payload = JSON.parse(data[0].dbl_chk_payload);

        var fields_arr = {
          "name" : "#bio_destroy_name",
          "unit" : "#bio_destroy_unit",
          'quantity' : '#bio_destroy_quantity',
          'bio_date' : '#bio_destroy_date',
          'destroy_address' : '#bio_destroy_address',
          'destroy_operator' : '#bio_destroy_operator',
          'destroy_record' : '#bio_destroy_record',
          'bio_desc' : '#bio_destroy_desc',
        }

        $(".dbl_chk").remove(); $(".dbl_chk_readonly").remove();
        $("#bio_destroy_dbl_chk_id").val(data[0].bio_destroy_id);

        //todo: disable comments when pass is clicked
        for(var key in fields_arr){
          <?php if($_SESSION['user_type'] == 'DA'): ?>
          if(payload[key]['is_checked'] == 0){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"> 复核</a>');
          }else
          <?php endif; ?>
           if(payload[key]['is_checked'] == -1){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-times"></i> 复核不通过</a>');
          }else if(payload[key]['is_checked'] == 1){
            $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary <?php echo ($_SESSION['user_type'] == 'DA') ? 'dbl_chk' : 'dbl_chk_readonly'; ?>" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-check"></i> 复核通过</a>');
          }
        }

        $(".dbl_chk").on("click", function(){
          destroySetPayload(this);
          $(".modal-backdrop").remove();
          var drugListModal = $('#bio_destroy_modal').modal();
          drugListModal.hide();
          var drugModal = $('#bio_destroy_dbl_chk_modal').modal();
          drugModal.removeData('bs.modal');
          drugModal.modal("show");
        });

        $('#bio_destroy_dbl_chk_modal').on('hidden.bs.modal', function () { // Show drug list modal when add drug modal closed
          $(".modal-backdrop").remove();
          var dialog = $('#bio_destroy_modal').modal();
          dialog.removeData('bs.modal');
          dialog.modal("show");
        });

        <?php endif; ?>
        //DOUBLE CHECK STUFF destroy END

        $('#bio_destroy_form')[0].reset();
        $('#bio_destroy_submit').html('更新');
        $('#bio_destroy_name').val(data[0].name);
        $('#bio_destroy_record').val("");
        $('#bio_destroy_quantity').val(data[0].quantity);
        $('#bio_destroy_unit').val(data[0].unit);
        $('#bio_destroy_date').val(data[0].bio_date);
        $('#bio_destroy_address').val(data[0].destroy_address);
        $('#bio_destroy_operator').val(data[0].destroy_operator);
        $('#bio_destroy_record_a').attr("href", data[0].destroy_record);
        $('#bio_destroy_record_a').attr("style", "display:block;width:19%;margin:10px 0px;");
        $('#bio_destroy_desc').val(data[0].bio_desc);
        $('#bio_destroy_modal').modal('show');
      }
    });
  }
}
// -/ MIGRATE BIO.JS HERE BECAUSE WE NEED THE POWER OF PHP!!!!!

// MIGRATE DRUGS JS HERE
$(document).ready(function () {
  $("#dateR").datepicker();

	$("#bday").datepicker();

	$("#validity_from").datepicker();
	$("#validity_to").datepicker();

	$('#drugListAdd').on('click', function () { // retrieved drug list

		$("#modalId").val('drug');

		$(".modal-backdrop").remove();
	   	var dialog = $('#drugListModal').modal();
		    dialog.removeData('bs.modal');
		    dialog.modal("show");

	    getDrugs('list', $("#project_idR").val());

	});

	$('#addDrug').on('click', function () { // when add drug modal shows

		$("#modalId").val('drug');

	   	$(".modal-backdrop").remove();
	   	var drugListModal = $('#drugListModal').modal();
	    	drugListModal.hide();


	   	var drugModal = $('#addDrugModal').modal();
		    drugModal.removeData('bs.modal');
		    drugModal.modal("show");

		    $("#modalTitleAddDrug").html($('#for_translate_add_drug').val());
		   	$('#drug_id').val('');
		   	$("#submitDrug").html($('#for_translate_submit').val());

		   	$("#addDrugForm")[0].reset();


	});


	$('#addDrugModal').on('hidden.bs.modal', function () { // Show drug list modal when add drug modal closed

		$(".modal-backdrop").remove();

		if($("#modalId").val() != "record"){

		   	var dialog = $('#drugListModal').modal();
		   	getDrugs('list', $("#project_idR").val());

		}else{

			var dialog = $('#addRecordModal').modal();
			getDrugs('option', $("#project_idR").val());

		}


			dialog.removeData('bs.modal');
			dialog.modal("show");
	});


	$("#addDrugForm").on('submit', function (event) { // Submit Drug Form

		var bValid = true;
		var fields = new Array();

		fields['trade_name'] = {'type' : 'input', 'label' : 'Trade Name'};
		fields['chemical_name'] = {'type' : 'dropdown', 'label' : 'Chemical Name'};
		fields['english_name'] = {'type' : 'input', 'label' : 'English Name'};
		fields['drug_type'] = {'type' : 'dropdown', 'label' : 'Drug Type'};
		fields['lot_number'] = {'type' : 'input', 'label' : 'Lot Type'};
		fields['specification'] = {'type' : 'input', 'label' : 'Specification'};
		// fields['inspection'] = {'type' : 'input', 'label' : 'Inspection Report'};
		fields['factory'] = {'type' : 'input', 'label' : 'Pharmaceutical Factory'};
		fields['provider'] = {'type' : 'input', 'label' : 'Provider'};

		bValid = bValid && checkErrors(fields);

		var startDate = $("#validity_from").val();
	    var endDate = $("#validity_to").val();

	    var startD = new Date(startDate + " 00:00:00");
	    var endD = new Date(endDate + " 00:00:00");

	    if(endD < startD || startDate == '' || endDate == ''){

	        $("#validity_div").addClass('has-error');
	        $("#validity_msg").show().html('您输入的日期范围无效。');
	        $('body, html, #validity_div').scrollTop($("#validity_div").offset().top - 100);

	        bValid = bValid && false;
	    }

	    //bValid = true;
		if(bValid){

			$("#submitDrug").prop('disabled', true).html('载入中...');
			var form_data = new FormData($('#addDrugForm')[0]);
			$.ajax({
				url:"php-functions/fncDrugs.php?action=addDrugs",
				type: "POST",
	    		dataType: "json",
	    		processData: false,
				contentType: false,
	        	// data: $("#addDrugForm").serialize(),
	        	data: form_data,
				success:function(data){

					$("#validity_div").removeClass('has-error');
    				$("#validity_msg").empty().hide();

					$("#submitDrug").prop('disabled', false).html($('#for_translate_submit').val());

					$('#modalMsg').html(data.msg);
					setTimeout(function(){
						$('#modalMsg').empty();
					}, 5000);

					if(data.type == "success")
						$('#modalMsg').css({color:'rgba(41,160,50,1) '});
					else
						$('#modalMsg').css({color:'red'});

					$('#drugListModal').show();
		   			$("#addDrugModal").modal('hide');

				}
			});

		}

		event.preventDefault();

	});

});



function getDrugs(action, project_id){

	$("#drugList").html("正在加载列表...");
	$.ajax({
		url:"php-functions/fncDrugs.php?action=getDrugs",
		type: "POST",
		data:{ "type" : "ajax", "project_id" : project_id},
		dataType: "json",
		success:function(data){

			var drugList = action == "list" ? "" : "<option value=''>"+$('#for_translate_select').val()+"</option>";
			var count = 1;
			for(var i = 0; i < data.length; i++){

				if(action == "list"){

					drugList +=	"<tr onclick='getDrugDetails("+data[i].drug_id+", "+project_id+");' style='cursor:pointer;'>";
						drugList += "<td>"+count+"</td>";
						drugList += "<td>"+data[i].trade_name+"</td>";
						switch(data[i].drug_type){
							case 'tested_drug': data[i].drug_type = '受试药'; break;
							case 'drug_combination': data[i].drug_type = '合并用药'; break;
							case 'control_drug': data[i].drug_type = '对照药'; break;
							case 'placebo_drug_record': data[i].drug_type = '安慰剂'; break;
							case 'adjuvant_drug': data[i].drug_type = '辅助用药'; break;
							case 'other': data[i].drug_type = '其他'; break;
						}
						drugList += "<td>"+data[i].drug_type+"</td>";
						drugList += "<td>"+data[i].storage+"</td>";
						drugList += "<td>"+data[i].specification+"</td>";
						drugList += "<td>"+data[i].validity_from+" - "+data[i].validity_to+"</td>";
		            drugList +=	"</tr>";

		            count++;

		        }else
		        	drugList += "<option value='"+data[i].drug_id+"'>"+data[i].trade_name+"</option>";


			}

			if(action == "list")
				$("#drugList").html(drugList);
			else
				$("#drug_idR").html(drugList);


		}
	});

}

function getDrugDetails(drug_id, project_id, event){

	$.ajax({
		url:"php-functions/fncDrugs.php?action=getDrugs",
		type: "POST",
		dataType: "json",
		data: { "type" : "ajax", "drug_id" : drug_id, "project_id" : project_id },
		success:function(data){

      //DOUBLE CHECK STUFF DRUGS
      <?php if (in_array($_SESSION['user_type'], ['DA', 'MC'])):?>
      var payload = JSON.parse(data[0].dbl_chk_payload);

      var fields_arr = {
        "trade_name" : "#trade_name",
        'chemical_name' : '#chemical_name',
        'english_name' : '#english_name',
        'drug_type' : '#drug_type',
        'lot_number' : '#lot_number',
        'specification' : '#specification',
        'validity_from' : '#validity_from',
        'validity_to' : '#validity_to',
        'inspection' : '#inspection',
        'provider' : '#provider',
        'factory' : '#factory',
      }

      $(".dbl_chk").remove(); $(".dbl_chk_readonly").remove();
      $("#drugs_dbl_chk_id").val(data[0].drug_id);

      //todo: disable comments when pass is clicked
      for(var key in fields_arr){
        <?php if($_SESSION['user_type'] == 'DA'): ?>
        if(payload[key]['is_checked'] == 0){
          $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'">复核</a>');
        }else
        <?php endif; ?>
         if(payload[key]['is_checked'] == -1){
          $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-times"></i> 复核不通过</a>');
        }else if(payload[key]['is_checked'] == 1){
          $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary <?php echo ($_SESSION['user_type'] == 'DA') ? 'dbl_chk' : 'dbl_chk_readonly'; ?>" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-check"></i> 复核通过</a>');
        }
      }

      $(".dbl_chk").on("click", function(){
        drugsSetPayload(this);
        $(".modal-backdrop").remove();
        var drugListModal = $('#addDrugModal').modal();
        drugListModal.hide();
        var drugModal = $('#drugs_dbl_chk_modal').modal();
        drugModal.removeData('bs.modal');
        drugModal.modal("show");
      });

      $('#drugs_dbl_chk_modal').on('hidden.bs.modal', function () { // Show drug list modal when add drug modal closed
        $(".modal-backdrop").remove();
        var dialog = $('#addDrugModal').modal();
        dialog.removeData('bs.modal');
        dialog.modal("show");
      });

      <?php endif; ?>
      //DOUBLE CHECK STUFF DRUGS END

      $(".modal-backdrop").remove();
      var dialog = $('#addDrugModal').modal();
      dialog.removeData('bs.modal');
      dialog.modal("show");
      var drugListModal = $('#drugListModal').modal();
      drugListModal.hide();

			$("#modalTitleAddDrug").html('药品注册分类及剂型');
			$("#submitDrug").html('更新');

			$('#drug_id').val(data[0].drug_id);
			$('#trade_name').val(data[0].trade_name);
			$('#chemical_name').val(data[0].chemical_name);
			$('#english_name').val(data[0].english_name);
			$('#drug_type').val(data[0].drug_type);
			$('#lot_number').val(data[0].lot_number);
			$('#specification').val(data[0].specification);
			$('#inspection_upload').attr('style','display:inline;');
			$('#inspection_upload').attr('href', data[0].inspection);
			$('#factory').val(data[0].factory);
			$('#provider').val(data[0].provider);
			$("#validity_from").val(data[0].validity_from);
	    $("#validity_to").val(data[0].validity_to);

		}
	});

}

//Payload STUFFS FOR DRUGS goes here

$("#drugs_dbl_chk_form").on('submit', function(e){ //modify payload data using ajax
  e.preventDefault();

  $.ajax({
    url:"ajax/double_check/updatePayload.php",
    type: "POST",
    dataType: "json",
    data: $("#drugs_dbl_chk_form").serialize(),
    success:function(data){ // data is the id of the row
      $("#drugs_dbl_chk_modal ").modal("hide");
      getDrugDetails(data, "<?php echo $_GET['project_id'] ?>", event);
      // console.log(data);
    }
  });
});

drugsSetPayload = function(caller){ //Set the payload key to be edited
  $("#drugs_dbl_chk_form")[0].reset();

  $("#drugs_payload_key").val($(caller).data('key'));
  //for setting default comments
  $("#drugs_comment").text($(caller).data('comments'));

  //for setting default value of checkbox
  if($(caller).data('is_checked') == 1){
    $("#drugs_dbl_chk_pass").attr('checked', true);
    $("#drugs_dbl_chk_nopass").attr('checked', false);
  }else if($(caller).data('is_checked') == -1)  {
    $("#drugs_dbl_chk_nopass").attr('checked', true);
    $("#drugs_dbl_chk_pass").attr('checked', false);
  }else{
    $("#drugs_dbl_chk_nopass").attr('checked', false);
    $("#drugs_dbl_chk_pass").attr('checked', false);
  }

}
// Payload FOR DRUGS


function showAddNewDrug(){

	$("#modalId").val('record');

	$(".modal-backdrop").remove();
   	var drugListModal = $('#addRecordModal').modal();
    	drugListModal.hide();


   	var drugModal = $('#addDrugModal').modal();
	    drugModal.removeData('bs.modal');
	    drugModal.modal("show");

	    $("#modalTitleAddDrug").html($('#for_translate_add_drug').val());
	   	$('#drug_id').val('');
	   	$("#submitDrug").html($('#for_translate_submit').val());

	   	$("#addDrugForm")[0].reset();

}
// MIGRATE DRUG JS HERE END

//  MIGRATE DRUG RECORD JS HERE

window.onload = function() {

	$("#date").datepicker();

	$("#addDrugRecord").on("click", function (event){
		$("#modalTitleAddRecord").html($('#for_translate_add_record').val());
		$("#submitRecord").html($('#for_translate_submit').val());
		$('#addRecordModal').modal('show');
    rmDoubleCheck();
		$('#addDrugRecordForm')[0].reset();
		$('#receiptList').hide().html('');

		$('#drug_record_id').val('');




	});

	$("#addDrugRecordForm").on('submit', function (event) { // Submit Drug Form

		var bValid = true;
		var fields = new Array();

		fields['drug_idR'] = {'type' : 'dropdown', 'label' : 'Drug'}; /*
		fields['quantity'] = {'type' : 'numeric', 'label' : 'Quantity'};
		fields['unit'] = {'type' : 'dropdown', 'label' : 'Unit'}; */
		fields['record_type'] = {'type' : 'dropdown', 'label' : 'Record Type'};
		fields['date'] = {'type' : 'input', 'label' : 'Date'};
		fields['actor1'] = {'type' : 'input', 'label' : 'Actor User One'};
		fields['position1'] = {'type' : 'dropdown', 'label' : 'Position for Actor User One'};
		fields['actor2'] = {'type' : 'input', 'label' : 'Actor User Two'};
		fields['position2'] = {'type' : 'dropdown', 'label' : 'Position for Actor User Two'};
		fields['description'] = {'type' : 'input', 'label' : 'Description'};

		bValid = bValid && checkErrors(fields);

		if($("#quantity").val() == "" || $("#quantity").val() < 1 || $("#unit").val() == ""){

			$("#quantity_div").addClass('has-error');
	        $("#quantity_msg").show().html('您输入的日期范围无效。');
	        $('body, html, #quantity_div').scrollTop($("#quantity_div").offset().top - 100);

	        bValid = bValid && false;

		}

	    if(bValid){

			var formData = new FormData();

	        var other_data = $('#addDrugRecordForm').serializeArray();
	        $.each(other_data,function(key,input){
	            formData.append(input.name, input.value);
	        });

	        $.each($("input[type=file]"), function(i, obj) {
		        $.each(obj.files,function(j, file){
		            formData.append('receipt['+j+']', file);
		        })
			});
       formData.append("project_id", "<?php echo $_GET['project_id']?>");
			$("#submitRecord").prop('disabled', true).html('载入中...');
			$.ajax({
				url:"php-functions/fncDrugRecord.php?action=addDrugRecord",
				type: "POST",
	    		dataType: "json",
	        	data: formData,
	        	processData: false,
            	contentType: false,
				success:function(data){

					$("#submitRecord").prop('disabled', false).html($('#for_translate_submit').val());

					if(data.type == "success")
						show_alert(data.msg, 'pg_audit.php?tab=drug-records&project_id='+$("#project_idR").val(), 1);
					else
						show_alert(data.msg, '', 0);

				}
			});

		}

		event.preventDefault();

	});




}

//Payload STUFFS FOR DRUGS goes here

$("#drugrec_dbl_chk_form").on('submit', function(e){ //modify payload data using ajax
  e.preventDefault();
  $.ajax({
    url:"ajax/double_check/updatePayload.php",
    type: "POST",
    dataType: "json",
    data: $("#drugrec_dbl_chk_form").serialize(),
    success:function(data){ // data is the id of the row
      $("#drugrec_dbl_chk_modal").modal("hide");
      getDrugRecordDetails(data, event);
    }
  });
});

drugrecSetPayload = function(caller){ //Set the payload key to be edited
  $("#drugrec_dbl_chk_form")[0].reset();

  $("#drugrec_payload_key").val($(caller).data('key'));
  //for setting default comments
  $("#drugrec_comment").text($(caller).data('comments'));

  //for setting default value of checkbox
  if($(caller).data('is_checked') == 1){
    $("#drugrec_dbl_chk_pass").attr('checked', true);
    $("#drugrec_dbl_chk_nopass").attr('checked', false);
  }else if($(caller).data('is_checked') == -1)  {
    $("#drugrec_dbl_chk_nopass").attr('checked', true);
    $("#drugrec_dbl_chk_pass").attr('checked', false);
  }else{
    $("#drugrec_dbl_chk_nopass").attr('checked', false);
    $("#drugrec_dbl_chk_pass").attr('checked', false);
  }

}
// Payload FOR DRUGS
//
function getDrugRecordDetails(drug_record_id, event){

	$.ajax({
		url:"php-functions/fncDrugRecord.php?action=getDrugRecords",
		type: "POST",
		dataType: "json",
		data: { "type" : "ajax", "drug_record_id" : drug_record_id, "project_id" : $("#project_idR").val() },
		success:function(data){
      //DOUBLE CHECK STUFF DRUG RECORDS
      <?php if (in_array($_SESSION['user_type'], ['DA', 'MC'])):?> // user types na pwedeng maka view TODO: Remove MC...
      var payload = JSON.parse(data[0].dbl_chk_payload);

      var fields_arr = { // keyname sa db on the left, id nung input sa right
        "drug_id" : "#drug_idR",
        'quantity' : '#quantityR',
        'unit' : '#unitR',
        'record_type' : '#record_type',
        'date' : '#dateR',
        'actor1' : '#actor1',
        'position1' : '#position1',
        'actor2' : '#actor2',
        'position2' : '#position2',
        'receipt' : '#receiptList',
        'description' : '#descriptionR',
      }

      $(".dbl_chk").remove(); $(".dbl_chk_readonly").remove();
      $("#drugrec_dbl_chk_id").val(data[0].drug_record_id); // dont forget to replace drugs_id with the real id and also the drugs_ prefix

      //todo: disable comments when pass is clicked
      // nothing to change here
      for(var key in fields_arr){
        <?php if($_SESSION['user_type'] == 'DA'): ?>
        if(payload[key]['is_checked'] == 0){
          $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'">复核</a>');
        }else
        <?php endif; ?>
        if(payload[key]['is_checked'] == -1){
          $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-times"></i> 复核不通过</a>');
        }else if(payload[key]['is_checked'] == 1){
          $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary <?php echo ($_SESSION['user_type'] == 'DA') ? 'dbl_chk' : 'dbl_chk_readonly'; ?>" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-check"></i> 复核通过</a>');
        }
      }

      $(".dbl_chk").on("click", function(){
        drugrecSetPayload(this);
        $(".modal-backdrop").remove();
        var drugListModal = $('#addRecordModal').modal(); // change the modal name according to the last model opened
        drugListModal.hide();
        var drugModal = $('#drugrec_dbl_chk_modal').modal(); // modal name of the double check modal (replace drugs_ prefix)
        drugModal.removeData('bs.modal');
        drugModal.modal("show");
      });

      $('#drugrec_dbl_chk_modal').on('hidden.bs.modal', function () { // Show drug list modal when add drug modal closed
        $(".modal-backdrop").remove();
        var dialog = $('#addRecordModal').modal();
        dialog.removeData('bs.modal');
        dialog.modal("show");
      });

      <?php endif; ?>
      //DOUBLE CHECK STUFF DRUGS RECORDS END

			var dialog = $('#addRecordModal').modal();
			    dialog.removeData('bs.modal');
			    dialog.modal("show");

			$("#modalTitleAddRecord").html('添加记录');
			$("#submitRecord").html($('#for_translate_update').val());

			$('#drug_record_id').val(data[0].drug_record_id);
			$('#drug_idR').val(data[0].drug_id);
			$('#quantityR').val(data[0].quantity);
			$('#unitR').val(data[0].unit);
			$('#record_type').val(data[0].record_type);
			$('#dateR').val(data[0].date);
			$('#actor1').val(data[0].actor1);
			$('#position1').val(data[0].position1);
			$('#actor2').val(data[0].actor2);
			$('#position2').val(data[0].position2);
			$('#descriptionR').val(data[0].description);

			// --------- Start of display receipts -----------//
			var receiptList = "";

			if(data[0].receipt_file.length > 0){

				for(var i = 0; i < data[0].receipt_file.length; i++){

					receiptList += "<li><a href='drug-records/"+data[0].receipt_file[i]+"'>"+data[0].receipt[i]+"</a></li>";

				}
			}

			$("#receiptList").show().html(receiptList);
			// --------- End of display receipts -----------//

		}
	});

}
//  MIGRATE DRUG RECORD JS HERE END

//  MIGRATE STAGE SUMMARY!!!


 $("#time_slot_from").datepicker();
 $("#time_slot_to").datepicker();

 $("#addSummary").on("click", function (event){

   $("#modalTitleAddSummary").html('阶段小结');
   $("#submitSummary").html('提交');
   $('#addSummaryForm')[0].reset();
   $('#stage_summary_id').val('');
   rmDoubleCheck();
   $('#addSummaryModal').modal('show');

 });

 $("#addUploadS").on("click", function (event){

   $(".modal-backdrop").remove();
     var drugListModal = $('#addSummaryModal').modal();
       drugListModal.hide();

     $("#summaryUploadForm")[0].reset();

     var dialog = $('#summaryUploadModal').modal();
       dialog.removeData('bs.modal');
       dialog.modal("show");

 });



 $("#submitMaterialS").on("click", function (event){

   $("#up_fileS_div").removeClass('has-error');
   $("#up_fileS_msg").hide().html('');

   bValid = true;
   var fields = new Array();

   fields['nameS'] = {'type' : 'input', 'label' : 'Name'};
   fields['versionS'] = {'type' : 'numeric', 'label' : 'Version'};
   fields['descriptionS'] = {'type' : 'input', 'label' : 'Description'};
   fields['up_fileS'] = {'type' : 'input', 'label' : 'Materials'};
   bValid = bValid && checkErrors(fields);

   if(bValid){

     var count = parseInt($("#countS").val()) + 1;
     var databank = $('#stage_databank').val();
     var uploadFormData = new FormData();

     uploadFormData.append("count", count);
     if(document.getElementById('stage_databank').checked)
     {
       uploadFormData.append("databank", databank);
     }
     $.each($("#up_fileS"), function(i, obj) {
           $.each(obj.files,function(j, file){
               uploadFormData.append('materials['+j+']', file);
           })
     });

     $("#submitMaterialS").prop('disabled', true).html('载入中...');
     $.ajax({
       url:"php-functions/fncStageSummary.php?action=uploadMaterial",
       type: "POST",
         dataType: "json",
           data: uploadFormData,
           processData: false,
           contentType: false,
       success:function(data){

         $("#submitMaterialS").prop('disabled', false).html('Submit');

             $("#up_fileS_msg").show().html(data.msg);

         if(data.type == "success"){

           var uploadList = '<tr>';
               uploadList += '<td>'+count+'</td>';
               uploadList += '<td>'+$('#nameS').val()+'</td>';
               uploadList += '<td>'+$('#versionS').val()+'</td>';
               uploadList += '<td>'+data.version_date+'</td>';
               uploadList += '<td>'+$('#descriptionS').val()+'</td>';

               var file = data.files.split(",");
               var filename = data.filename.split(",");
               uploadList += '<td><ul>';

                 $.each(file, function(i, obj) {
                       uploadList += '<li><i><a href="temp-uploads/'+obj+'">'+filename[i]+'</a></i></li>';
                 });

               uploadList += '</ul></td>';

               uploadList += '<td id="hiddenFields" style="display:none;"></td>';
                 uploadList += '<input type="hidden" name="name['+count+']" value="'+$('#nameS').val()+'">';
                 uploadList += '<input type="hidden" name="version['+count+']" value="'+$('#versionS').val()+'">';
                 uploadList += '<input type="hidden" name="description['+count+']" value="'+$('#descriptionS').val()+'">';
                 uploadList += '<input type="hidden" name="file['+count+']" value="'+data.files+'">';
                 uploadList += '<input type="hidden" name="filename['+count+']" value="'+data.filename+'">';
                 uploadList += '<input type="hidden" name="databank['+count+']" value="'+data.databank+'">';
               uploadList += '</td>';

             uploadList += '</tr>';

           $("#stageSummaryUpload").append(uploadList);

           $("#countS").val(count);

           $(".modal-backdrop").remove();
             var drugListModal = $('#summaryUploadModal').modal();
               drugListModal.hide();

             var dialog = $('#addSummaryModal').modal();
               dialog.removeData('bs.modal');
               dialog.modal("show");

         }else{



         }

       }
     });

   }

 });


 $("#submitSummary").on('click', function (event) { // Submit Form

   var bValid = true;
   var fields = new Array();

   fields['enroll_amount'] = {'type' : 'input', 'label' : 'Enroll Amount'};
   fields['quit_amount'] = {'type' : 'input', 'label' : 'Quit Amount'};
   fields['finish_amount'] = {'type' : 'input', 'label' : 'Finish Amount'};
   fields['light_effect'] = {'type' : 'notzero', 'label' : 'Light Effect'};
   fields['light_amount'] = {'type' : 'input', 'label' : 'Light Amount'};
   fields['mid_effect'] = {'type' : 'notzero', 'label' : 'Mid Effect'};
   fields['mid_amount'] = {'type' : 'input', 'label' : 'Mid Amount'};
   fields['hard_effect'] = {'type' : 'notzero', 'label' : 'Hard Effect'};
   fields['hard_amount'] = {'type' : 'input', 'label' : 'Hard Amount'};
   fields['important_effect'] = {'type' : 'notzero', 'label' : 'Important Effect'};
   fields['important_amount'] = {'type' : 'input', 'label' : 'Important Amount'};
   fields['degree'] = {'type' : 'input', 'label' : 'Degree'};
   fields['sae'] = {'type' : 'input', 'label' : 'SAE'};
   fields['amount'] = {'type' : 'input', 'label' : 'Amount'};
   fields['handling_info'] = {'type' : 'input', 'label' : 'Other Situation'};

   bValid = bValid && checkErrors(fields);

   var startDate = $("#time_slot_from").val();
     var endDate = $("#time_slot_to").val();

     var startD = new Date(startDate + " 00:00:00");
     var endD = new Date(endDate + " 00:00:00");

     if(endD < startD || startDate == '' || endDate == ''){

         $("#time_slot_div").addClass('has-error');
         $("#time_slot_msg").show().html('您输入的日期范围无效。');
         $('body, html, #time_slot_div').scrollTop($("#time_slot_div").offset().top - 100);

         bValid = bValid && false;
     }

     //bValid = true;
   if(bValid){

     $("#submitSummary").prop('disabled', true).html('载入中...');
     $.ajax({
       url:"php-functions/fncStageSummary.php?action=addStageSummary",
       type: "POST",
         dataType: "json",
           data: $("#addSummaryForm").serialize() + "&project_id=" + "<?php echo $_GET['project_id']?>",
       success:function(data){

         $("#submitSummary").prop('disabled', false).html('提交');

         if(data.type == "success")
           show_alert(data.msg, 'pg_audit.php?tab=stage-summary&project_id='+$("#project_idS").val(), 1);
         else
           show_alert(data.msg, '', 0);

         event.preventDefault();

       }
     });

   }

   event.preventDefault();

 });

function getStageSummary(stage_summary_id, event){

 $.ajax({
   url:"php-functions/fncStageSummary.php?action=getStageSummary",
   type: "POST",
   dataType: "json",
   data: { "type" : "ajax", "stage_summary_id" : stage_summary_id, "project_id" : $("#project_idS").val() },
   success:function(data){

     //DOUBLE CHECK STUFF Stage summmary
    // Put this dun sa ajax success ng onclick nung record
    <?php if (in_array($_SESSION['user_type'], ['DA', 'I'])):?> // user types na pwedeng maka view
    var payload = JSON.parse(data[0].dbl_chk_payload);

    var fields_arr = { // keyname sa db on the left, id nung input sa right
      "time_slot_from" : "#time_slot_from",
      'time_slot_to' : '#time_slot_to',
      'enroll_amount' : '#enroll_amount',
      'quit_amount' : '#quit_amount',
      'finish_amount' : '#finish_amount',
      'light_effect' : '#light_effect',
      'light_amount' : '#light_amount',
      'mid_effect' : '#mid_effect',
      'mid_amount' : '#mid_amount',
      'hard_effect' : '#hard_effect',
      'hard_amount' : '#hard_amount',
      'important_effect' : '#important_effect',
      'important_amount' : '#important_amount',
      'degree' : '#degree',
      'sae' : '#sae',
      'amount' : '#amount',
      'stage_summary_upload' : '#stageSummaryUpload'
    }

    $(".dbl_chk").remove(); $(".dbl_chk_readonly").remove();
    $("#stagesummary_dbl_chk_id").val(data[0].stage_summary_id);

    for(var key in fields_arr){
      <?php if($_SESSION['user_type'] == 'DA'): ?>
      if(payload[key]['is_checked'] == 0){
        $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'">复核</a>');
      }else
      <?php endif; ?>
      if(payload[key]['is_checked'] == -1){
        $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary dbl_chk" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-times"></i> 复核不通过</a>');
      }else if(payload[key]['is_checked'] == 1){
        $(fields_arr[key]).after('<a href="#" data-toggle="modal" class="btn btn-xs btn-primary <?php echo ($_SESSION['user_type'] == 'DA') ? 'dbl_chk' : 'dbl_chk_readonly'; ?>" data-key="' + key + '" onclick="" data-is_checked="' + payload[key]["is_checked"] + '" data-comments="'+ payload[key]["comments"] +'"><i class="fa fa-check"></i> 复核通过</a>');
      }
    }

    $(".dbl_chk").on("click", function(){
      stagesummarySetPayload(this);
      $(".modal-backdrop").remove();
      var drugListModal = $('#addSummaryModal').modal();
      drugListModal.hide();
      var drugModal = $('#stagesummary_dbl_chk_modal').modal();
      drugModal.removeData('bs.modal');
      drugModal.modal("show");
    });

    $('#stagesummary_dbl_chk_modal').on('hidden.bs.modal', function () { // Show drug list modal when add drug modal closed
      $(".modal-backdrop").remove();
      var dialog = $('#addSummaryModal').modal();
      dialog.removeData('bs.modal');
      dialog.modal("show");
    });

    <?php endif; ?>
    //DOUBLE CHECK STUFF stage summary end

    //  $("#addSummary").click();

     $(".modal-backdrop").remove();
     var dialog = $('#addSummaryModal').modal();
     dialog.removeData('bs.modal');
     dialog.modal("show");

     $("#modalTitleAddSummary").html('阶段摘要详细信息');
     $("#submitSummary").html('更新');

     $('#stage_summary_id').val(data[0].stage_summary_id);
     $('#time_slot_from').val(data[0].time_slot_from);
     $('#time_slot_to').val(data[0].time_slot_to);
     $('#enroll_amount').val(data[0].enroll_amount);
     $('#quit_amount').val(data[0].quit_amount);
     $('#finish_amount').val(data[0].finish_amount);
     $('#light_effect').val(data[0].light_effect);
     $('#light_amount').val(data[0].light_amount);
     $('#mid_effect').val(data[0].mid_effect);
     $('#mid_amount').val(data[0].mid_amount);
     $('#hard_effect').val(data[0].hard_effect);
     $('#hard_amount').val(data[0].hard_amount);
     $('#important_effect').val(data[0].important_effect);
     $('#important_amount').val(data[0].important_amount);
     $('#sae').val(data[0].sae);
     $('#amount').val(data[0].amount);
     $('#handling_info').val(data[0].handling_info);
     $('#degree').val(data[0].degree);


     // ------------- Start of get Materials ------------- @Rai//
     var uploadList = "";
     var count = 1;
     $.each(data[0].materials, function(i, obj) {

       //console.log(obj.material_id);

       uploadList += '<tr>';
         uploadList += '<td>'+count+'</td>';
         uploadList += '<td>'+obj.name+'</td>';
         uploadList += '<td>'+obj.version+'</td>';
         uploadList += '<td>'+obj.version_date+'</td>';
         uploadList += '<td>'+obj.description+'</td>';

         // ------------- Start of get Material Files ------------- @Rai//
         uploadList += '<td><ul>';

           $.each(obj.files, function(i, obj_) {
                 uploadList += '<li><i><a href="'+obj_.file+'" target="_blank">'+obj_.filename+'</a></i></li>';
           });

         uploadList += '</ul></td>';
         // ------------- End of get Material Files ------------- @Rai//

       uploadList += '</tr>';

       $("#stageSummaryUpload").html(uploadList);

       count++;

     });
     // ------------- End of get Materials ------------- @Rai//

     $("#countS").val(count - 1);

   }

 });

}


$("#stagesummary_dbl_chk_form").on('submit', function(e){ //modify payload data using ajax
  e.preventDefault();

  $.ajax({
    url:"ajax/double_check/updatePayload.php",
    type: "POST",
    dataType: "json",
    data: $("#stagesummary_dbl_chk_form").serialize(),
    success:function(data){ // data is the id of the row
      $("#stagesummary_dbl_chk_modal ").modal("hide");
      getStageSummary(data, event); // rename this // Trigger ulit yung onclick event use other function
    }
  });
});

stagesummarySetPayload = function(caller){ //Set the payload key to be edited
  $("#stagesummary_dbl_chk_form")[0].reset();

  $("#stagesummary_payload_key").val($(caller).data('key'));
  //for setting default comments
  $("#stagesummary_comment").text($(caller).data('comments'));

  //for setting default value of checkbox
  if($(caller).data('is_checked') == 1){
    $("#stagesummary_dbl_chk_pass").attr('checked', true);
    $("#stagesummary_dbl_chk_nopass").attr('checked', false);
  }else if($(caller).data('is_checked') == -1)  {
    $("#stagesummary_dbl_chk_nopass").attr('checked', true);
    $("#stagesummary_dbl_chk_pass").attr('checked', false);
  }else{
    $("#stagesummary_dbl_chk_nopass").attr('checked', false);
    $("#stagesummary_dbl_chk_pass").attr('checked', false);
  }

}
// Payload FOR DRUGS

//  END MIGRATE STAGE SUMMARY END!!!

</script>
</body>
</html>
