<?php
/*
Includes: Common functions such as Getting all users, positions, departments, etc.
Available for ajax
*/
if (isset($_GET['action']) && $_GET['action'] != '') {
    require("../jp_library/jp_lib.php");
    if(isset($_POST['data']))
    {
        parse_str($_POST['data'], $_POST);
        $_POST['data'] = false;
    }
    $_GET['action'](true);
    if (!isset($_SESSION['user_id'])) {
        session_start();
    }
}

function addMcInfo($ajax = false){

    $params['table'] = "mc_info";
    $params['data'] = $_POST;

    $result = jp_add($params);

    /*******************
    * LOGS START HERE! *
    *******************/
    $t = new Clinical\Helpers\Translation($_SESSION['lang']);
    $p = new Clinical\Helpers\Project($_POST['project_id']);
    $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

    $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'add_record',
    array(
      $t->tryTranslate('project_name') => "$p->project_name",
      $t->tryTranslate('module') => $t->tryTranslate('mc_info'),
      $t->tryTranslate('number') => "$p->project_num",
      $t->tryTranslate('submitter') => "$u->fname"
      )
    );
    $l->save();

    /*******************
    * LOGS END HERE!   *
    *******************/

    echo $result;
}

########### DEPRECATED #############
/*function oldUploadFile($ajax = false){

    $dir_location = 'uploads/' . $_GET['subdir'] . "/";

    #TODO: add condition if necessary
    $traverser = __DIR__ . '/../'; #to go backwards in our file structure

    $storage = new \Upload\Storage\FileSystem($traverser . $dir_location);
    $file = new \Upload\File('up_file', $storage);

    #Optionally you can rename the file on upload
    $new_filename = uniqid();
    $file->setName($new_filename);

     #Validate file upload
     #MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
    $file->addValidations(array(
        # Ensure file is of type "image/png"
        // new \Upload\Validation\Mimetype('image/png'),

        #You can also add multi mimetype validation
        // new \Upload\Validation\Mimetype(array('image/png', 'image/jpg')),

        # Ensure file is no larger than 5M (use "B", "K", M", or "G")
//        new \Upload\Validation\Size('15M')
    ));

        #Access data about the file that has been uploaded
    $data = array(
        'name'       => $file->getNameWithExtension(),
        'extension'  => $file->getExtension(),
//        'mime'       => $file->getMimetype(),
//        'size'       => $file->getSize(),
//        'md5'        => $file->getMd5(),
//        'dimensions' => $file->getDimensions()
    );

    try {
        $file->upload();

        $result = 0; #initialize

        if($_GET['subdir'] == 'project_documents'){
            $params['table'] = "uploads";
            $_POST['up_type'] = "1"; #1 because this is PD
            $_POST['user_id'] = $_SESSION['user_id'];
            $_POST['project_id'] = $_POST['project_id'];
            $_POST['up_path'] = $GLOBALS['base_url'] . $dir_location . $data['name'];
            $params['data'] = $_POST;

            $result = jp_add($params);
        }

        echo $result;

    } catch (\Exception $e) {
        $errors = $file->getErrors();
        echo $errors;
    }

}*/
########### FOR REFERENCE ONLY ############
########### / DEPRECATED ############

function delMcInArray($ajax = false){
    $params['table'] = "mc_info";
    $params['where'] = "mc_id in (". $_POST['del_arr'] .")";
    $result = jp_delete($params);
    echo $result;
}
// function delUpInArray($ajax = false){
//     $params['table'] = "uploads";
//     $params['where'] = "up_id in (". $_POST['del_arr'] .")";
//     $result = jp_delete($params);
//     echo $result;
// }

function deleteProjectById($ajax = false, $project_id = false){

    $params['table'] = "projects";
    if($ajax == false){
        $params['where'] = "project_id = '". $project_id ."'";
    }else{
        $params['where'] = "project_id = '". $_POST['project_id'] ."'";
    }

    $result = jp_delete($params);


    if ($ajax == false) {
        return $result;
    } else {
        echo $result;
    }
}

function saveProject($ajax = false)
{
    # Save means it's just going to be drafted, not published

    $project_id = $_POST['project_id'];
    unset($_POST['project_id']); # We unset this because the $_POST array will contain it if we don't

    $data = "0";
    # required data manipulation before saving to DB

    $params['table'] = 'projects';
    $params['data'] = $_POST;
    $params['where'] = 'project_id = ' . $project_id . ' AND user_id = ' . $_SESSION['user_id'];
    if(jp_update($params))
    {
        $data = "1";
        if($ajax == false)
        {
            return $data;
        }
        else
        {
            echo $data;
        }
    }
}
