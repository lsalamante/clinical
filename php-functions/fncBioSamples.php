<?php
/*
Includes: Common functions such as Getting all users, positions, departments, etc.
Available for ajax
*/
if (isset($_GET['action']) && $_GET['action'] != '') {
  require("../jp_library/jp_lib.php");
  if(isset($_POST['data']))
  {
    parse_str($_POST['data'], $_POST);
    $_POST['data'] = false;
  }
  $_GET['action'](true);
  if (!isset($_SESSION['user_id'])) {
    session_start();
  }
}

function addRemarks($ajax = false)
{

  $bio_id = $_POST['bio_id'];
  unset($_POST['bio_id']); # We unset this because the $_POST array will contain it if we don't

  $data = "0";
  # required data manipulation before saving to DB

  $params['table'] = 'bio_samples';
  $params['data'] = $_POST;
  $params['where'] = "bio_id = '" . $bio_id . "'";

  if(jp_update($params))
  {
    $data = "1";
    if($ajax == false)
    {
      return $data;
    }
    else
    {
      echo $data;
    }
  }

}

function getBioDetails($ajax = false)
{

  $get_bio['table'] = 'bio_samples';
  $get_bio['where'] = 'bio_id = '.$_POST['bio_id'];
  $res_bio = jp_get($get_bio);
  $data = [];

  $key = 'bio_id';
  // $dc = new Clinical\Helpers\DoubleCheck;
  // $dc->fetch($get_bio['table'], $_POST[$key], $key);
  //
  // echo $dc->payload;

  while($row_bio = mysqli_fetch_assoc($res_bio))
  {
    $data[] = $row_bio;
  }

  if(isset($_POST["type"]) && $_POST["type"] == "ajax")
  {
    echo json_encode($data);
  }
  else
  {
    return $data;
  }
}

function uploadBioSample($ajax = false)
{
  $data = 0;
  $dir_location = "uploads/bio_samples/";
  $traverser = __DIR__ . '/../'; #to go backwards in our file structure

  $full_path = $traverser . $dir_location;

  #this crap right here is for creating a folder!!!
  if (!is_dir($full_path) && !mkdir($full_path, 0777, true)){
    mkdir($full_path, 0777, true);
  }
  if(isset($_FILES['collect_record']['tmp_name']) && $_FILES['collect_record']['tmp_name'] != "")
  {
    $id_card = "bio_collect_record_".time();
    $_POST['collect_record'] = jp_upload($_FILES['collect_record'],$id_card,"../uploads/bio_samples/");
    $_POST['collect_record'] = $GLOBALS['base_url'] . "uploads/bio_samples/" . $_POST['collect_record'];
  }

  $add_bio['table'] = "bio_samples";
  $add_bio['data'] = $_POST;
  // $add_bio['debug'] = 1;
  if(isset($_POST['bio_id_update']) && $_POST['bio_id_update'] != '')
  {
    $add_bio['where'] = 'bio_id = '.$_POST['bio_id_update'];
    if(jp_update($add_bio))
    {

      /*******************
      * LOGS START HERE! *
      *******************/
      $t = new Clinical\Helpers\Translation($_SESSION['lang']);
      $p = new Clinical\Helpers\Project($_POST['project_id']);
      $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

      # notify DA here so he can double check
      $p->notify_group = array_diff($p->notify_group, array_diff($p->positions, ['DA']));
      Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang'], $t->tryTranslate('da_todo_14_0'), $t->tryTranslate('da_dbl_chk_bgc') . " " . $t->tryTranslate('collect'));
      # / notify DA here so he can double check

      $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'modify',
      array(
        $t->tryTranslate('project_name') => "$p->project_name",
        $t->tryTranslate('module') => $t->tryTranslate('bio_sample_management') . ": " . $t->tryTranslate('collect'),
        $t->tryTranslate('number') => "$p->project_num",
        $t->tryTranslate('submitter') => "$u->fname"
        )
      );
      $l->save();

      /*******************
      * LOGS END HERE!   *
      *******************/

      /*************************
      * DOUBLE CHECK RESET!!   *
      **************************/
      $dc = new Clinical\Helpers\DoubleCheck("bio_samples", "bio_id", $_POST['bio_id_update'], $_SESSION['lang']);
      $dc->fetch("bio_samples", "bio_id", $_POST['bio_id_update']);
      $dc->resetPayload();
      $dc->updatePayload("bio_samples", "bio_id", $_POST['bio_id_update']);
      /*************************
      * / DOUBLE CHECK RESET!! *
      **************************/

      $data = "1";

    }
  }
  else
  {
    if(jp_add($add_bio))
    {

      /*******************
      * LOGS START HERE! *
      *******************/
      $t = new Clinical\Helpers\Translation($_SESSION['lang']);
      $p = new Clinical\Helpers\Project($_POST['project_id']);
      $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

      # notify DA here so he can double check
      $p->notify_group = array_diff($p->notify_group, array_diff($p->positions, ['DA']));
      Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang'], $t->tryTranslate('da_todo_14_0'), $t->tryTranslate('da_dbl_chk_bgc') . " " . $t->tryTranslate('collect'));
      # / notify DA here so he can double check

      $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'submit_the_record',
      array(
        $t->tryTranslate('project_name') => "$p->project_name",
        $t->tryTranslate('module') => $t->tryTranslate('bio_sample_management') . ": " . $t->tryTranslate('collect'),
        $t->tryTranslate('number') => "$p->project_num",
        $t->tryTranslate('submitter') => "$u->fname"
        )
      );
      $l->save();

      /*******************
      * LOGS END HERE!   *
      *******************/

      $data = "1";
    }
  }

  if($ajax == false)
  {
    return $data;
  }
  else
  {
    echo $data;
  }
}

// function uploadBioSample($ajax = false){
//   $uploadOK = 1;
//   #array for holding collect_record and process_record
//
//   if(!isset($_POST['bio_id_update']))
//   {
//     $dir_location = "uploads/" . $_POST['sub_dir'] . "/";
//     $traverser = __DIR__ . '/../'; #to go backwards in our file structure
//
//     $full_path = $traverser . $dir_location;
//
//     foreach ($_FILES['collect_record']['name'] as $up_fileKey => $up_fileVal )
//     {
//       $fileExt = explode(".", $up_fileVal);
//       $fileName = time() . "." . end($fileExt);
//
//       if (!is_dir($full_path) && !mkdir($full_path, 0777, true)){
//         mkdir($full_path, 0777, true);
//       }
//
//       if(in_array(end($fileExt), ['jpg', 'txt', 'png', 'xls', 'xlsx', 'jpeg', 'gif', 'doc', 'docx', 'pdf'])){ #these are the allowed files #TODO: add error message reject file type
//         if( move_uploaded_file($_FILES['collect_record']['tmp_name'][$up_fileKey], $full_path . $fileName) )
//         {
//           $_POST['collect_record'] = $GLOBALS['base_url'] . $dir_location . $fileName;;
//         }
//         else
//         {
//           $uploadOK = 0;
//         }
//       }else{
//         $uploadOK = 0;
//       }
//     }
//   }
//
//
//   if($uploadOK == 1){
//
//     $_POST['collect_start'] = date("Y-m-d", strtotime($_POST['collect_start']));
//     $_POST['collect_end'] = date("Y-m-d", strtotime($_POST['collect_end']));
//
//     $params['table'] = "bio_samples";
//     $params['data'] = $_POST;
//     if(isset($_POST['bio_id_update']) && $_POST['bio_id_update'] != '')
//     {
//       $params['where'] = "bio_id = ".$_POST['bio_id_update'];
//       echo $result = jp_update($params);
//     }
//     else
//     {
//       echo $result = jp_add($params);
//     }
//
//   }
//   else{
//     echo 0;
//   }
// }
