<?php
/*
Includes: Common functions such as Getting all users, positions, departments, etc.
Available for ajax
*/
if (isset($_GET['action']) && $_GET['action'] != '') {
    require("../jp_library/jp_lib.php");
    if(isset($_POST['data']))
    {
        parse_str($_POST['data'], $_POST);
        $_POST['data'] = false;
    }
    $_GET['action'](true);
    if (!isset($_SESSION['user_id'])) {
        session_start();
    }
}

function addInfo($ajax = false)
{
    $data = 0;
    $dir_location = "uploads/bio_save/";
    $traverser = __DIR__ . '/../'; #to go backwards in our file structure

    $full_path = $traverser . $dir_location;

    #this crap right here is for creating a folder!!!
    if (!is_dir($full_path) && !mkdir($full_path, 0777, true)){
      mkdir($full_path, 0777, true);
    }
    if(isset($_FILES['record']['tmp_name']) && $_FILES['record']['tmp_name'] != "")
    {
        $id_card = "bio_save_record_".time();
        $_POST['record'] = jp_upload($_FILES['record'],$id_card,"../uploads/bio_save/");
        $_POST['record'] = $GLOBALS['base_url'] . "uploads/bio_save/" . $_POST['record'];
    }

    $add_bio['table'] = "bio_save";
    $add_bio['data'] = $_POST;
    // $add_bio['debug'] = 1;
    if(isset($_POST['bio_save_id_update']) && $_POST['bio_save_id_update'] != '')
    {
        $add_bio['where'] = 'bio_save_id = '.$_POST['bio_save_id_update'];
        if(jp_update($add_bio))
        {
          /*******************
          * LOGS START HERE! *
          *******************/
          $t = new Clinical\Helpers\Translation($_SESSION['lang']);
          $p = new Clinical\Helpers\Project($_POST['project_id']);
          $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

          $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'modify',
          array(
            $t->tryTranslate('project_name') => "$p->project_name",
            $t->tryTranslate('module') => $t->tryTranslate('bio_sample_management') . ": " . $t->tryTranslate('save'),
            $t->tryTranslate('number') => "$p->project_num",
            $t->tryTranslate('submitter') => "$u->fname"
            )
          );
          $l->save();

          /*******************
          * LOGS END HERE!   *
          *******************/

          # notify DA here so he can double check
          $p->notify_group = array_diff($p->notify_group, array_diff($p->positions, ['DA']));
          Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang'], $t->tryTranslate('da_todo_14_0'), $t->tryTranslate('da_dbl_chk_bgc') . " " . $t->tryTranslate('save'));
          # / notify DA here so he can double check

          /*************************
          * DOUBLE CHECK RESET!!   *
          **************************/
          $dc = new Clinical\Helpers\DoubleCheck("bio_save", "bio_save_id", $_POST['bio_save_id_update'], $_SESSION['lang']);
          $dc->fetch("bio_save", "bio_save_id", $_POST['bio_save_id_update']);
          $dc->resetPayload();
          $dc->updatePayload("bio_save", "bio_save_id", $_POST['bio_save_id_update']);
          /*************************
          * / DOUBLE CHECK RESET!! *
          **************************/
            $data = "1";
        }
    }
    else
    {
        if(jp_add($add_bio))
        {

          /*******************
          * LOGS START HERE! *
          *******************/
          $t = new Clinical\Helpers\Translation($_SESSION['lang']);
          $p = new Clinical\Helpers\Project($_POST['project_id']);
          $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

          # notify DA here so he can double check
          $p->notify_group = array_diff($p->notify_group, array_diff($p->positions, ['DA']));
          Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang'], $t->tryTranslate('da_todo_14_0'), $t->tryTranslate('da_dbl_chk_bgc') . " " . $t->tryTranslate('save'));
          # / notify DA here so he can double check

          $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'submit_the_record',
          array(
            $t->tryTranslate('project_name') => "$p->project_name",
            $t->tryTranslate('module') => $t->tryTranslate('bio_sample_management') . ": " . $t->tryTranslate('save'),
            $t->tryTranslate('number') => "$p->project_num",
            $t->tryTranslate('submitter') => "$u->fname"
            )
          );
          $l->save();

          /*******************
          * LOGS END HERE!   *
          *******************/
            $data = "1";
        }
    }

    if($ajax == false)
    {
        return $data;
    }
    else
    {
        echo $data;
    }
}

function getAllBioSave($ajax = false, $project_id = false)
{
    $params['table'] = "bio_save";
    if($ajax == false){
        $params['where'] = "project_id = '$project_id'";
    }else{
        $params['where'] = "project_id = '".$_POST['project_id']."'" ;
    }
    $params['filters'] = "ORDER BY bio_save_id DESC";
    $result = jp_get($params);

    if($result){
        if ($ajax == false) {
          return $result;
        } else {
          echo json_encode($result);
        }
    }else{
        if ($ajax == false) {
          return false;
        } else {
          echo false;
        }
    }
}

function getBioSaveDetails($ajax = false)
{

    $get_bio['table'] = 'bio_save';
    $get_bio['where'] = 'bio_save_id = '.$_POST['bio_save_id'];
    $res_bio = jp_get($get_bio);
    $data = [];
    while($row_bio = mysqli_fetch_assoc($res_bio))
    {
        $data[] = $row_bio;
    }

    if(isset($_POST["type"]) && $_POST["type"] == "ajax")
    {
        echo json_encode($data);
    }
    else
    {
        return $data;
    }
}

function getBioRemarkDetails($ajax = false)
{
    $get_bio['table'] = 'bio_save';
    $get_bio['where'] = 'bio_save_id = '.$_POST['bio_save_id'];
    $res_bio = jp_get($get_bio);
    $data = [];
    while($row_bio = mysqli_fetch_assoc($res_bio))
    {
        $data[] = $row_bio;
    }

    if(isset($_POST["type"]) && $_POST["type"] == "ajax")
    {
        echo json_encode($data);
    }
    else
    {
        return $data;
    }
}

function addRemarks($ajax = false)
{
    $bio_save_id = $_POST['bio_save_id'];
    unset($_POST['bio_save_id']); # We unset this because the $_POST array will contain it if we don't

    $data = "0";
    # required data manipulation before saving to DB

    $params['table'] = 'bio_save';
    $params['data'] = $_POST;
    $params['where'] = "bio_save_id = '" . $bio_save_id . "'";
    // $params['debug'] = 1;
    if(jp_update($params))
    {
        $data = "1";
        if($ajax == false)
        {
            return $data;
        }
        else
        {
            echo $data;
        }
    }
}
