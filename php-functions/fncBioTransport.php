<?php
/*
Includes: Common functions such as Getting all users, positions, departments, etc.
Available for ajax
*/
if (isset($_GET['action']) && $_GET['action'] != '') {
    require("../jp_library/jp_lib.php");
    if(isset($_POST['data']))
    {
        parse_str($_POST['data'], $_POST);
        $_POST['data'] = false;
    }
    $_GET['action'](true);
    if (!isset($_SESSION['user_id'])) {
        session_start();
    }
}

function addRemarks($ajax = false)
{

    $transport_id = $_POST['transport_id'];
    unset($_POST['transport_id']); # We unset this because the $_POST array will contain it if we don't

    $data = "0";
    # required data manipulation before saving to DB

    $params['table'] = 'bio_transport';
    $params['data'] = $_POST;
    $params['where'] = "transport_id = '" . $transport_id . "'";

    if(jp_update($params))
    {
        $data = "1";
        if($ajax == false)
        {
            return $data;
        }
        else
        {
            echo $data;
        }
    }

}

function getBioDetails($ajax = false)
{

    $get_bio['table'] = 'bio_transport';
    $get_bio['where'] = 'transport_id = '.$_POST['transport_id'];
    $res_bio = jp_get($get_bio);
    $data = [];
    while($row_bio = mysqli_fetch_assoc($res_bio))
    {
        $data[] = $row_bio;
    }

    if(isset($_POST["type"]) && $_POST["type"] == "ajax")
    {
        echo json_encode($data);
    }
    else
    {
        return $data;
    }
}

function uploadBioSample($ajax = false){
  $uploadOK = 1;
  #array for holding collect_record and process_record
  #REVIEW this please
  if($_POST['transport_id_update'] == '' || $_POST['transport_id_update'] == 0)
  {
    $dir_location = "uploads/" . "bio_transport" . "/";
    $traverser = __DIR__ . '/../'; #to go backwards in our file structure

    $full_path = $traverser . $dir_location;

    foreach ($_FILES['process_record']['name'] as $up_fileKey => $up_fileVal )
    {
      $fileExt = explode(".", $up_fileVal);
      $fileName = time() . "." . end($fileExt);

      if (!is_dir($full_path) && !mkdir($full_path, 0777, true)){
        mkdir($full_path, 0777, true);
      }

      if(in_array(end($fileExt), ['jpg', 'txt', 'png', 'xls', 'xlsx', 'jpeg', 'gif', 'doc', 'docx', 'pdf'])){ #these are the allowed files #TODO: add error message reject file type
        if( move_uploaded_file($_FILES['process_record']['tmp_name'][$up_fileKey], $full_path . $fileName) )
        {
          $_POST['process_record'] = $GLOBALS['base_url'] . $dir_location . $fileName;;
        }
        else
        {
          $uploadOK = 0;
        }
      }else{
        $uploadOK = 0;
      }
    }
  }


  if($uploadOK == 1){

    $params['table'] = "bio_transport";
    $params['data'] = $_POST;
    if(isset($_POST['transport_id_update']) && $_POST['transport_id_update'] != '')
    {
      $params['where'] = "transport_id = ".$_POST['transport_id_update'];

      /*******************
      * LOGS START HERE! *
      *******************/
      $t = new Clinical\Helpers\Translation($_SESSION['lang']);
      $p = new Clinical\Helpers\Project($_POST['project_id']);
      $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

      # notify DA here so he can double check
      $p->notify_group = array_diff($p->notify_group, array_diff($p->positions, ['DA']));
      Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang'], $t->tryTranslate('da_todo_14_0'), $t->tryTranslate('da_dbl_chk_bgc') . " " . $t->tryTranslate('transport'));
      # / notify DA here so he can double check

      $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'modify',
      array(
        $t->tryTranslate('project_name') => "$p->project_name",
        $t->tryTranslate('module') => $t->tryTranslate('bio_sample_management') . ": " . $t->tryTranslate('transport'),
        $t->tryTranslate('number') => "$p->project_num",
        $t->tryTranslate('submitter') => "$u->fname"
        )
      );
      $l->save();

      /*******************
      * LOGS END HERE!   *
      *******************/

      /*************************
      * DOUBLE CHECK RESET!!   *
      **************************/
      $dc = new Clinical\Helpers\DoubleCheck("bio_transport", "transport_id", $_POST['transport_id_update'], $_SESSION['lang']);
      $dc->fetch("bio_transport", "transport_id", $_POST['transport_id_update']);
      $dc->resetPayload();
      $dc->updatePayload("bio_transport", "transport_id", $_POST['transport_id_update']);
      /*************************
      * / DOUBLE CHECK RESET!! *
      **************************/

      echo $result = jp_update($params);
    }
    else
    {
      /*******************
      * LOGS START HERE! *
      *******************/
      $t = new Clinical\Helpers\Translation($_SESSION['lang']);
      $p = new Clinical\Helpers\Project($_POST['project_id']);
      $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

      # notify DA here so he can double check
      $p->notify_group = array_diff($p->notify_group, array_diff($p->positions, ['DA']));
      Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang'], $t->tryTranslate('da_todo_14_0'), $t->tryTranslate('da_dbl_chk_bgc') . " " . $t->tryTranslate('transport'));
      # / notify DA here so he can double check

      $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'submit_the_record',
      array(
        $t->tryTranslate('project_name') => "$p->project_name",
        $t->tryTranslate('module') => $t->tryTranslate('bio_sample_management') . ": " . $t->tryTranslate('transport'),
        $t->tryTranslate('number') => "$p->project_num",
        $t->tryTranslate('submitter') => "$u->fname"
        )
      );
      $l->save();

      /*******************
      * LOGS END HERE!   *
      *******************/

      echo $result = jp_add($params);

    }

  }
  else{
    echo 0;
  }
}
