<?php
/*
Includes: Common functions such as Getting all users, positions, departments, etc.
Available for ajax
*/
if (isset($_GET['action']) && $_GET['action'] != '') {
  require("../jp_library/jp_lib.php");

  if(isset($_POST['data']))
  {
    parse_str($_POST['data'], $_POST);
    $_POST['data'] = false;
  }
  $_GET['action'](true);
  if (!isset($_SESSION['user_id'])) {
    session_start();
  }
}

function getPositions($ajax = false, $pgroup_only = false)
{
  $get_positions['table'] = 'positions';
  $get_positions['where'] = '1';
  if($pgroup_only != false)
  {
    $get_positions['where'] = 'is_professional_group = 1';
  }
  //$checkAdmin['debug'] = 1;
  $res_positions = jp_get($get_positions);
  $data = array();
  $i = 0;
  while ($row_positions = mysqli_fetch_assoc($res_positions)) {
    $data[$i] = $row_positions;
    $i++;
  }

  if ($ajax == false) {
    return $data;
  } else {
    echo $data;
  }
}


function getAllUsersByUserType($ajax = false, $usertype = false, $allexcept = false)
{
  $get_all_users['select'] = 'u.*, r.role_id, r.position_id, r.pgroup_id, p.*';
  $get_all_users['table'] = 'users u LEFT JOIN
  roles r ON u.user_id = r.user_id LEFT JOIN
  positions p ON r.position_id = p.position_id';
  $get_all_users['where'] = "(r.role_id IS NULL OR p.acronym != 'A') AND u.is_admin != 1";
  if($usertype != false)
  {
    $get_all_users['where'] = "p.acronym = '$usertype'";
    if($usertype == "EC")
    {
      $get_all_users['where'] = "(p.acronym = 'ECC' OR p.acronym = 'ECM')";
    }
    // elseif($usertype == "A-C")
    // {
    //   $get_all_users['where'] = "p.acronym_sub = 'CRO'";
    // }
  }
  if($allexcept != false)
  {
    $get_all_users['where'] = "(r.role_id IS NULL OR (u.user_id NOT IN (SELECT rr.user_id FROM roles rr LEFT JOIN positions pp ON rr.position_id = pp.position_id WHERE pp.acronym = '".$allexcept."' OR pp.acronym = 'A'))) AND u.is_admin != 1 AND u.is_active = 1";
    if($allexcept == "EC")
    {
      $get_all_users['where'] = "(r.role_id IS NULL OR (u.user_id NOT IN (SELECT rr.user_id FROM roles rr LEFT JOIN positions pp ON rr.position_id = pp.position_id WHERE pp.acronym = 'ECC' OR pp.acronym = 'ECM' OR pp.acronym = 'A'))) AND u.is_admin != 1 AND u.is_active = 1";
    }
    elseif($allexcept == "pg")
    {
      $get_all_users['where'] = "(r.role_id IS NULL OR (u.user_id NOT IN (SELECT rr.user_id FROM roles rr LEFT JOIN positions pp ON rr.position_id = pp.position_id WHERE pp.is_professional_group = 1 OR pp.acronym = 'A'))) AND u.is_admin != 1 AND u.is_active = 1";
    }
  }
  // $get_all_users['debug'] = 1;
  $res_all_users = jp_get($get_all_users);
  $data = array();
  $i = 0;
  while($row_all_users = mysqli_fetch_assoc($res_all_users))
  {
    $data[$row_all_users['user_id']] = $row_all_users;
    // $i++;
  }
  if ($ajax == false) {
    return $data;
  } else {
    echo $data;
  }

}


function getProfGroup($ajax = false)
{
  $get_profgroups['table'] = 'professional_group';
  $get_profgroups['where'] = '1';
  $res_profgroups = jp_get($get_profgroups);
  $i = 0;
  $data = array();
  while ($row_profgroups = mysqli_fetch_assoc($res_profgroups)) {
    $data[$i] = $row_profgroups;
    $i++;
  }

  if ($ajax == false) {
    return $data;
  } else {
    echo $data;
  }

}



function getProfGroupMembers($ajax = false, $pgroup_id = false, $position_id = '')
{
  $params['table'] = 'users u LEFT JOIN
  roles r ON u.user_id = r.user_id LEFT JOIN
  positions p ON r.position_id = p.position_id';

  if($ajax == false){
    $params['where'] = "pgroup_id = '$pgroup_id'";
  }else{
    $params['where'] = "pgroup_id = '".$_POST['pgroup_id']."'";
  }

  if($position_id != ''){
    $params['where'] .= " AND r.position_id = '".$position_id."'";
  }


  $result = jp_get($params);

  $i = 0;
  $data = array();
  while ($row = mysqli_fetch_assoc($result)) {
    $data[$i] = $row;
    $i++;
  }

  if ($ajax == false) {
    return $data;
  } else {
    echo json_encode($data);
  }
}

# PARAMS
# $user_id => user id in users table
# $get_position => tell the function that you want to get the position of the users (pass true)
# $role_id => tell the function that you want to get the details for a specific role only
function getUserById($ajax = false, $user_id = false, $get_position = false, $role_id = false)
{
  $params['table'] = 'users u';
  //$params['debug'] = 1;
  if($get_position != false)
  {
    $params['table'] .= ' LEFT JOIN roles r ON u.user_id = r.user_id LEFT JOIN
    positions p ON r.position_id = p.position_id';
  }

  if($ajax == false){
    $params['where'] = "u.user_id = '$user_id'";
  }else{
    $params['where'] = "u.user_id = '". $_POST["user_id"] ."'";
  }

  if($role_id != false)
  {
    $params['where'] .= " AND r.role_id = '$role_id'";
  }

  $result = mysqli_fetch_assoc(jp_get($params));

  if ($ajax == false) {
    return $result;
  } else {
    echo json_encode($result);
  }
}


function getUserByRoleId($ajax = false, $role_id = false)
{
  $params['table'] = 'users u LEFT JOIN
  roles r ON u.user_id = r.user_id LEFT JOIN
  positions p ON r.position_id = p.position_id';
  //$params['debug'] = 1;
  if($ajax == false){
    $params['where'] = "r.role_id = '$role_id'";
  }else{
    $params['where'] = "r.role_id = '". $_POST["role_id"] ."'";
  }

  $result = mysqli_fetch_assoc(jp_get($params));

  if ($ajax == false) {
    return $result;
  } else {
    echo json_encode($result);
  }
}

function getProfGroupById($ajax = false)
{
  $pgroup_id = $_POST['pgroup_id'];

  $params['table'] = 'professional_group';
  $params['where'] = "pgroup_id = '$pgroup_id'";
  $result = mysqli_fetch_assoc(jp_get($params));

  $data = array();
  foreach ($result as $key => $value) {
    $data[$key] = $value;
  }

  if ($ajax == false) {
    return $data;
  } else {
    echo json_encode($data);
  }
}

function getPIByProfGroupId($ajax = false, $pgroup_id = false)
{
  $position_id = getPositionIdByAcronym(0, 'PI');

  $params['table'] = "users u LEFT JOIN roles r ON u.user_id = r.user_id";

  if($ajax == false){
    $params['where'] = "r.pgroup_id = '$pgroup_id' AND r.position_id = '$position_id'";
  }else{
    $params['where'] = "r.pgroup_id = '". $_POST['pgroup_id'] ."' AND r.position_id = '$position_id'";
  }



  $result = jp_get($params);

  $i = 0;
  $data = array();
  while ($row = mysqli_fetch_assoc($result)) {
    $data[$i] = $row;
    $i++;
  }
  if ($ajax == false) {
    return $data;
  } else {
    echo json_encode($data);
  }
}

function getPositionIdByAcronym($ajax = false, $acronym = false)
{
  $params['select'] = 'position_id';
  $params['table'] = 'positions';
  if($ajax == false){
    $params['where'] = "acronym = '$acronym'";
  }else{
    $params['where'] = "acronym = '". $_POST["acronym"] ."'";
  }
  $result = mysqli_fetch_assoc(jp_get($params));

  if ($ajax == false) {
    return $result['position_id'];
  } else {
    echo $result['position_id'];
  }
}

function getProfGroupAdminIdByProfGroupId($ajax = false, $pgroup_id = false)
{
  $params['select'] = 'pgroup_admin_id';
  $params['table'] = 'professional_group';

  if($ajax == false){
    $params['where'] = "pgroup_id = '$pgroup_id'";
  }
  else{
    $params['where'] = "pgroup_id = '". $_POST["pgroup_id"] ."'";
  }

  $result = mysqli_fetch_assoc(jp_get($params));

  $result['pgroup_admin_id'];

  if ($ajax == false) {
    return $result['pgroup_admin_id'];
  } else {
    echo $result['pgroup_admin_id'];
  }
}

function getAllDepartments($ajax = false)
{
  $params['table'] = 'departments';

  $result = jp_get($params);

  if ($ajax == false) {
    return $result;
  } else {
    echo json_encode($result);
  }
}

function getAllTrials($ajax = false)
{
  if($_SESSION['lang'] == 'chinese')
  {
    $trials = array(
      "1" => "I期",
      "2" => "II期",
      "3" => "III期",
      "4" => "IV期",
      "5" => "BE试验",
      "6" => "临床验证",
      "7" => "医疗器械",
      "8" => "食品",
      "9" => "其他"
    );
  }
  else
  {
    $trials = array(
      "1" => "I Stage",
      "2" => "II Stage",
      "3" => "III Stage",
      "4" => "IV Stage",
      "5" => "BE trial",
      "6" => "Clinical verifications",
      "7" => "Medical Equipment",
      "8" => "Food",
      "9" => "Other"
    );
  }

  if ($ajax == false) {
    return $trials;
  } else {
    echo json_encode($trials);
  }
}

function addNewProject($ajax = false)
{
  $params['table'] = 'projects';
  $_POST['date_created'] = date('Y-m-d H:i:s');
  $params['data'] = $_POST;

  $result = jp_add($params);

  if ($ajax == false) {
    #TODO: Add return value
  } else {

    if($result){
      echo jp_last_added();
    }
    else{
      echo 0;
    }

  }
}

function getProjectById($ajax = false, $project_id = false)
{
  if($ajax == false && isset($_POST['project_id']))
  {
    $project_id = $_POST['project_id'];
  }
  $params['select'] = 'p.*, s.status_id, s.status_num, s.reviewer_id, s.is_rejected, s.status_date';
  $params['table'] = 'projects p LEFT JOIN
  status s ON p.status_id = s.status_id';

  if($_SESSION['user_type'] == "A")
  {
    $params['where'] = "p.user_id = '". $_SESSION['user_id'] ."' AND p.project_id = '$project_id'";
  }
  elseif($_SESSION['user_type'] == "PI")
  {
    $params['where'] = "p.pi_id = '".$_SESSION['user_id']."' AND p.project_id = '$project_id'";
  }
  else
  {
    $params['where'] = "p.project_id = '$project_id'";
  }
  //$params['debug'] = 1;
  $result = mysqli_fetch_assoc(jp_get($params));

  if($result){

    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }

}

function getAllProjectsByUserId($ajax = false)
{
  $params['select'] = 'p.*, s.status_id, s.status_num, s.reviewer_id, s.is_rejected, s.status_date';
  $params['table'] = 'projects p LEFT JOIN status s ON p.status_id = s.status_id';
  $params['where'] = "user_id = '". $_SESSION['user_id'] ."'";
  $params['filters'] = "ORDER BY p.date_created DESC";
  //$params['debug'] = "1";
  $result = jp_get($params);

  if($result){

    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }

}

function getAllProjectsByCategory($ajax = false)
{
  $params['select'] = 'p.*, s.status_id, s.status_num, s.reviewer_id, s.is_rejected, s.status_date';
  $params['table'] = 'projects p LEFT JOIN status s ON p.status_id = s.status_id';
  $params['where'] = "trial_id = '". $_GET['trial_id'] ."'";
  $params['filters'] = "ORDER BY p.date_created DESC";
  //$params['debug'] = "1";
  $result = jp_get($params);

  if($result){

    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }

}

function getAllProjectsByState($ajax = false)
{
  $params['select'] = 'p.*, s.status_id, s.status_num, s.reviewer_id, s.is_rejected, s.status_date';
  $params['table'] = 'projects p LEFT JOIN status s ON p.status_id = s.status_id';
  $params['where'] = "s.status_num >= '". $_GET['state'] ."'";
  $params['filters'] = "ORDER BY p.date_created DESC";
  //$params['debug'] = "1";
  $result = jp_get($params);

  if($result){

    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }

}

function getAllMCbyProjectId($ajax = false, $mc_id = false){
  #translators note: MC means `multi-center` (table) not main character!!!!

  $params['table'] = 'mc_info'; #this means `multi-center information` table
  if($ajax == false){
    $params['where'] = "project_id = '$mc_id'";
  }else{
    $params['where'] = "project_id = '".$_POST['mc_id']."'";
  }

  $params['filters'] = "ORDER BY mc_id DESC";
  $result = jp_get($params);

  if($result){
    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }

}


function getTimeline($ajax = false, $proj_id)
{
  $get_timeline['select'] = 's.*,p.project_name';
  $get_timeline['table'] = 'status s LEFT JOIN
  projects p ON s.project_id = p.project_id';  # TODO: delete this if not needed -> project_remarks pr ON s.status_id = pr.status_id
  $get_timeline['where'] = 'p.project_id = '.$proj_id;
  $get_timeline['filters'] = 'ORDER BY s.status_date ASC';
  //$get_timeline['debug'] = 1;
  $res_timeline = jp_get($get_timeline);
  $data = array();
  $i = 0;
  while($row_timeline = mysqli_fetch_assoc($res_timeline))
  {
    $get_remarks['table'] = 'project_remarks';
    $get_remarks['where'] = 'remark_type != 1 AND status_id = '.$row_timeline['status_id'];
    //$get_remarks['debug'] = 1;
    $res_remarks = jp_get($get_remarks);
    $row_timeline['remarks'] = "";
    $row_timeline['remarks_id'] = "";
    while($row_remarks = mysqli_fetch_assoc($res_remarks))
    {
      $row_timeline['remarks'] = $row_remarks['remarks'];
      $row_timeline['remarks_id'] = $row_remarks['remarks_id'];
    }
    $data['project_name'] = $row_timeline['project_name'];
    $data['project_status'][$i] = $row_timeline;
    $i++;
  }
  if($data){

    if ($ajax == false) {
      return $data;
    } else {
      echo json_encode($data);
    }
  }
}

function getAllPDbyProjectId($ajax = false, $project_id = false){

  $params['table'] = 'uploads';
  if($ajax == false){
    $params['where'] = "project_id = '$project_id' AND up_type in (1,4)";
  }else{
    $params['where'] = "project_id = '".$_POST['project_id']."' AND up_type = '1'";
  }

  $params['filters'] = "ORDER BY up_id DESC";
  $result = jp_get($params);

  if($result){
    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }
}
function getAllJobByProjectId($ajax = false, $project_id = false){

  $params['table'] = 'uploads';
  if($ajax == false){
    $params['where'] = "project_id = '$project_id' AND up_type in (5)"; #job specification
  }else{
    $params['where'] = "project_id = '".$_POST['project_id']."' AND up_type = '5'"; # 5 is for job spec
  }
  $params['filters'] = "ORDER BY up_id DESC";
  $result = jp_get($params);

  if($result){
    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }
}

function getAllEligibleByProjectId($ajax = false, $project_id = false){

  $params['table'] = 'uploads';
  if($ajax == false){
    $params['where'] = "project_id = '$project_id' AND up_type in (6)"; #NOTE: eligible 6
  }else{
    $params['where'] = "project_id = '".$_POST['project_id']."' AND up_type = '6'";
  }

  $result = jp_get($params);

  if($result){
    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }
}

function getAllPPbyProjectId($ajax = false, $project_id = false){

  $params['table'] = 'uploads';
  if($ajax == false){
    $params['where'] = "project_id = '$project_id' AND up_type = '2'"; #type 2 for PP
  }else{
    $params['where'] = "project_id = '".$_POST['project_id']."' AND up_type = '2'"; #type 2 for PP
  }
  $params['filters'] = "ORDER BY up_id DESC";
  $result = jp_get($params);

  if($result){
    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }
}

function getAllCmtByProjectId($ajax = false, $project_id = false){

  $params['table'] = 'uploads';
  if($ajax == false){
    $params['where'] = "project_id = '$project_id' AND up_type = '3'"; #type 3 for PRE comments
  }else{
    $params['where'] = "project_id = '".$_POST['project_id']."' AND up_type = '3'"; #type3 for PRE comments
  }

  $result = jp_get($params);

  if($result){
    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }
}

function getAllECMOpinion($ajax = false, $project_id = false){

  $params['table'] = 'uploads';
  if($ajax == false){
    $params['where'] = "project_id = '$project_id' AND up_type = '7'"; #type 7 for ECM comments
  }else{
    $params['where'] = "project_id = '".$_POST['project_id']."' AND up_type = '7'"; #type 7 for ECM comments
  }

  $result = jp_get($params);

  if($result){
    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }
}

function getAllBGCbyProjectId($ajax = false, $project_id = false){
  $params['table'] = 'bio_samples';
  if($ajax == false){
    $params['where'] = "project_id = '$project_id'";
  }else{
    $params['where'] = "project_id = '".$_POST['project_id']."'" ;
  }
  $params['filters'] = "ORDER BY bio_id DESC";
  $result = jp_get($params);

  if($result){
    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }
}

function getAllBioHandlesbyProjectId($ajax = false, $project_id = false){
  $params['table'] = 'bio_handles';
  if($ajax == false){
    $params['where'] = "project_id = '$project_id'";
  }else{
    $params['where'] = "project_id = '".$_POST['project_id']."'" ;
  }
  $params['filters'] = "ORDER BY bio_handle_id DESC";
  $result = jp_get($params);

  if($result){
    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }
}

function getAllTransportbyProjectId($ajax = false, $project_id = false){
  $params['table'] = 'bio_transport';
  if($ajax == false){
    $params['where'] = "project_id = '$project_id'";
  }else{
    $params['where'] = "project_id = '".$_POST['project_id']."'" ;
  }
  $params['filters'] = "ORDER BY transport_id DESC";
  $result = jp_get($params);

  if($result){
    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }
}

function getAllBioDeterminebyProjectId($ajax = false, $project_id = false){
  $params['table'] = 'bio_determine';
  if($ajax == false){
    $params['where'] = "project_id = '$project_id'";
  }else{
    $params['where'] = "project_id = '".$_POST['project_id']."'" ;
  }
  $params['filters'] = "ORDER BY determine_id DESC";
  $result = jp_get($params);

  if($result){
    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }
}

function getAllPD2byProjectId($ajax = false, $project_id = false){
  #PD2 means project document version 2 (or greater)

  $params['table'] = 'uploads';
  if($ajax == false){
    $params['where'] = "project_id = '$project_id' AND up_type = '4'"; #4 means the investigator's version 2.x
  }else{
    $params['where'] = "project_id = '".$_POST['project_id']."' AND up_type = '4'";
  }
  $params['filters'] = "ORDER BY up_id DESC";
  $result = jp_get($params);

  if($result){
    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }
}

function uploadFile($ajax = false){

  $uploadOK = 1;

  foreach ($_FILES['up_file']['name'] as $up_fileKey => $up_fileVal )
  {
    $fileExt = explode(".", $up_fileVal);
    $fileName = time() . "." . end($fileExt);

    $dir_location = "uploads/" . $_POST['sub_dir'] . "/";
    $traverser = __DIR__ . '/../'; #to go backwards in our file structure

    $full_path = $traverser . $dir_location;

    #this crap right here is for creating a folder!!!

    if (!is_dir($full_path) && !mkdir($full_path, 0777, true)){
      mkdir($full_path, 0777, true);
    }

    #NOTE: do not delete this scripts. might use this in the future
    #chmod($traverser . "uploads/", 0777);
    #chmod($full_path, 0777);

    if(in_array(end($fileExt), ['jpg', 'txt', 'png', 'xls', 'xlsx', 'jpeg', 'gif', 'doc', 'docx', 'pdf'])){ #these are the allowed files #TODO: add error message reject file type
      if( move_uploaded_file($_FILES['up_file']['tmp_name'][$up_fileKey], $full_path . $fileName) )
      {
        $_POST['up_date'] = date("Y-m-d H:i:s");
        $params['table'] = "uploads";

        $_POST['up_path'] = $GLOBALS['base_url'] . $dir_location . $fileName;


        $mod = 'upload'; #module key phrase
        if(isset($_POST['training_id']) && $_POST['training_id'] != "")
        {
          $params['table'] = "training_material";  # if upload came from training modal
          // $mod = 'training_meeting';
        }
        elseif(isset($_POST['side_effect_id']) && $_POST['side_effect_id'] != '')
        {
          $params['table'] = "side_effects_docs";  # if upload came from side effects modal
          $_POST['version_date'] = date("Y-m-d H:i:s");
          // $mod = 'side_effects';
        }

        $params['data'] = $_POST;

        /*******************
        * LOGS START HERE! *
        *******************/
        $t = new Clinical\Helpers\Translation($_SESSION['lang']);
        $p = new Clinical\Helpers\Project($_POST['project_id']);
        $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

        switch ($_POST['sub_dir']) {
          case 'side_effect_documents':
          $mod = 'side_effects';
          break;
          case 'bio_samples':
          $mod = 'bio_sample_management';
          break;
          case 'pre_comments':
          $mod = 'pre_opinion';
          /*******************************
          * Notification starts here!!! *
          *******************************/
          $p->notify_group = array_diff($p->notify_group, array_diff($p->positions, ['I']));
          # GET ONLY I

          Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang'], $t->tryTranslate('new_pre_opinion'), $t->tryTranslate('review_pre_opinion'));

          /*******************************
          * Notification ends here!!!    *
          *******************************/
          break;
          case 'eligible':
          $mod = 'eligible';
          break;
          case 'job_specifications':
            $mod = 'job_spec';
            break;
            case 'training_material':
            $mod = 'training_meeting';
            break;
            case 'project_documents':
            $mod = 'project_documents';
            break;
            case 'project_plan':
            $mod = 'project_plan';
            break;

            default:
            $mod = $mod;
            break;
          }


          $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'add_record',
          array(
            $t->tryTranslate('project_name') => "$p->project_name",
            $t->tryTranslate('module') => $t->tryTranslate($mod),
            $t->tryTranslate('number') => "$p->project_num",
            $t->tryTranslate('submitter') => "$u->fname"
            )
          );
          $l->save();
          $result = jp_add($params);
        }
        else
        {
          $uploadOK = 0;
        }
      }else{
        $uploadOK = 0;
      }
    }

    if($uploadOK == 1){
      echo 1;
    }
    else{
      echo 0;
    }
  }

  function getAllPRE($ajax = false)
  {
    $getAllPRE['table'] = 'users u LEFT JOIN
    roles r ON u.user_id = r.user_id LEFT JOIN
    positions p ON r.position_id = p.position_id';
    $getAllPRE['where'] = 'p.acronym = "PRE"';
    //$getAllPER['debug'] = 1;
    $result = jp_get($getAllPRE);
    $data = array();
    $i = 0;
    while($row = mysqli_fetch_assoc($result))
    {
      $data[$i] = $row;
      $i++;
    }

    if($data){
      if ($ajax == false) {
        return $data;
      } else {
        echo json_encode($data);
      }
    }else{
      if ($ajax == false) {
        return false;
      } else {
        echo false;
      }
    }
  }

  function getAllRoles($ajax = false, $user_id = null)
  {
    $get_roles['table'] = 'roles r LEFT JOIN positions p ON r.position_id = p.position_id';
    $get_roles['where'] = "1";
    if($user_id != null)
    {
      $get_roles['where'] = 'r.user_id = '.$user_id;
    }
    // $get_roles['debug'] = 1;
    $res_roles = jp_get($get_roles);
    $data = array();
    $i = 0;
    while($row_roles = mysqli_fetch_assoc($res_roles))
    {
      $data[$i] = $row_roles;
      $i++;
    }

    if($ajax == false)
    {
      return $data;
    }
    else
    {
      echo json_encode($data);
    }
  }

  # Get all members for adding by PI in Project (SC, BGC, MC)
  function getPIMembersAssign($ajax = false, $project_id = null, $acronym = null)
  {
    $acronym_v = array("BGC" => "bio_controller_id", "MC" => "med_controller_id", "SC" => "subj_controller_id", "DA" => "data_administrator_id");
    # Get Existing Users
    $getExisting['select'] = "bio_controller_id, med_controller_id, subj_controller_id, data_administrator_id";
    $getExisting['table'] = "projects";
    if($project_id != null)
    {
      $getExisting['where'] = "project_id=".$project_id;
    }
    $resExisting = jp_get($getExisting);
    $rowExisting = mysqli_fetch_assoc($resExisting);
    # str_replace * and explode 3 key positions
    $bio_controller_id = explode(",", str_replace("*", "", $rowExisting['bio_controller_id']));
    $med_controller_id = explode(",", str_replace("*", "", $rowExisting['med_controller_id']));
    $subj_controller_id = explode(",", str_replace("*", "", $rowExisting['subj_controller_id']));
    $data_administrator_id = explode(",", str_replace("*", "", $rowExisting['data_administrator_id']));
    $all_controllers = array_merge($bio_controller_id, $med_controller_id);
    $all_controllers = array_merge($all_controllers, $subj_controller_id);
    $all_controllers = array_merge($all_controllers, $data_administrator_id);
    $all_users_id = array();
    foreach ($all_controllers as $role_id)
    {
      if($role_id != 0)
      {
        $getUserId['select'] = 'user_id';
        $getUserId['table'] = 'roles';
        $getUserId['where'] = 'role_id='.$role_id;
        $resUserId = jp_get($getUserId);
        $rowUserId = mysqli_fetch_assoc($resUserId);
        $all_users_id[] =  $rowUserId['user_id'];
      }
    }
    # Get All users
    $getAllUsers['select'] = "r.*, u.*, p.*, u.user_id as user_id_PK";
    $getAllUsers['table'] = "users u LEFT JOIN
    roles r ON u.user_id = r.user_id  LEFT JOIN
    positions p ON r.position_id = p.position_id";
    if($acronym != null)
    {
      $all_controllers_filtered = array();
      if($acronym != "BGC")
      {
        $all_controllers_filtered = array_merge($all_controllers_filtered, $bio_controller_id);
      }
      if($acronym != "MC")
      {
        $all_controllers_filtered = array_merge($all_controllers_filtered, $med_controller_id);
      }
      if($acronym != "SC")
      {
        $all_controllers_filtered = array_merge($all_controllers_filtered, $subj_controller_id);
      }
      if($acronym != "DA")
      {
        $all_controllers_filtered = array_merge($all_controllers_filtered, $data_administrator_id);
      }

      $userIdFiltered = array();
      $getUserIdFiltered['select'] = "user_id";
      $getUserIdFiltered['table'] = "roles";
      $getUserIdFiltered['where'] = "role_id IN (".implode(",", $all_controllers_filtered).")";
      // $getUserIdFiltered['debug'] =1;
      $resUserIdFiltered = jp_get($getUserIdFiltered);
      while($rowUserIdFiltered = mysqli_fetch_assoc($resUserIdFiltered))
      {
        $userIdFiltered[] = $rowUserIdFiltered['user_id'];
      }
    }
    if($acronym != null && count($userIdFiltered) > 0)
    {
      $getAllUsers['where'] = "u.user_id NOT IN (".implode(",", $userIdFiltered).")";
    }
    // $getAllUsers['debug'] = 1;
    $resAllUsers = jp_get($getAllUsers);
    $data = array(); # set return variable
    $existing_id = array(); # so users won't double
    while($rowAllUsers = mysqli_fetch_assoc($resAllUsers))
    {
      $rowAllUsers['selected'] = 0;
      if(in_array($rowAllUsers['user_id'], $all_users_id) && $acronym != null)
      {
        $rowAllUsers['selected'] = 1;
      }
      if(!in_array($rowAllUsers['user_id'], $existing_id))
      {
        $data[] = $rowAllUsers;
        $existing_id[] = $rowAllUsers['user_id'];
      }

    }
    if ($ajax == false) {
      return $data;
    } else {
      echo $data;
    }
  }

  function getPhrases(){

    function ords_to_unistr($ords, $encoding = 'UTF-8'){
      // Turns an array of ordinal values into a string of unicode characters
      $str = '';
      for($i = 0; $i < sizeof($ords); $i++){
        // Pack this number into a 4-byte string
        // (Or multiple one-byte strings, depending on context.)
        $v = $ords[$i];
        $str .= pack("N",$v);
      }
      $str = mb_convert_encoding($str,$encoding,"UCS-4BE");
      return($str);
    }

    $params['table'] = "language";
    $result = jp_get($params);

    $data = [];

    if($_SESSION['lang'] == 'english'){
      foreach($result as $row){
        $data[$row['phrase']] = $row['english'];
      }
    }else if ($_SESSION['lang'] == 'chinese'){
      foreach($result as $row){
        $data[$row['phrase']] = ords_to_unistr(explode(',', $row['chinese']));
      }
    }

    return $data;
  }

  /* this is for deleting rows from `uploads table` - @enzo */
  function delUpInArray($ajax = false){
    $params['table'] = "uploads";
    $params['where'] = "up_id in (". $_POST['del_arr'] .")";
    $result = jp_delete($params);
    echo $result;
  }

  function langToCN(){
    $_SESSION['lang'] = 'chinese';
    return 1;
  }
  function langToEN(){
    $_SESSION['lang'] = 'english';
    return 1;
  }

  if(isset($_SESSION['lang']))
  {
    $phrases = getPhrases();
  }

  //Desc: Search by type, status, bday and gender
  //Developer: Kitcathe
  function search($keyword,$project_id,$module)
  {

    if($module == 'volunteers')
    {
      if($keyword != '')
      {
        $params['select'] = 'v.*, subj.status_id, stat.status as status_letter';
        $params['table'] = 'volunteers v, subject_status stat, subjects subj';
        $params['where'] = 'v.volunteer_id = subj.volunteer_id AND subj.status_id = stat.status_id AND v.project_id ='.$project_id.' AND
        ( v.gender LIKE "%'.$keyword.'%"
        OR v.bday LIKE "%'.$keyword.'%"
        OR stat.status LIKE "%'.$keyword.'%"
        OR v.volunteer_type LIKE "%'.$keyword.'%"
        OR v.v_name LIKE "%'.$keyword.'%"
        OR v.native LIKE "%'.$keyword.'%"
        OR v.married LIKE "%'.$keyword.'%"
        OR v.id_card_num LIKE "%'.$keyword.'%"
        OR v.mobile LIKE "%'.$keyword.'%" )
        ';
      }
      else
      {
        $params['select'] = 'v.*, subj.status_id, stat.status as status_letter';
        $params['table'] = 'volunteers v, subject_status stat, subjects subj';
        $params['where'] = 'v.volunteer_id = subj.volunteer_id AND subj.status_id = stat.status_id AND v.project_id ='.$project_id;
        $params['where'] .= " AND (stat.status != 'NS' OR v.status = 0)";
      }

    }
    elseif($module == 'subjects')
    {
      if($keyword != '')
      {
        $params['select'] = '*, subject_status.status AS subject_stat';
        $params['table'] = 'subjects
        LEFT JOIN volunteers v ON v.subject_id = subjects.subject_id
        LEFT JOIN projects ON projects.project_id = subjects.project_id
        LEFT JOIN subject_status ON subject_status.status_id = subjects.status_id';
        $params['where'] = " subjects.project_id = ".$project_id.' AND
        ( v.gender LIKE "%'.$keyword.'%"
        OR v.bday LIKE "%'.$keyword.'%"
        OR subject_status.status LIKE "%'.$keyword.'%"
        OR subjects.subjects_num LIKE "%'.$keyword.'%"
        OR v.v_name LIKE "%'.$keyword.'%"
        OR subjects.abbrv LIKE "%'.$keyword.'%"
        OR v.id_card_num LIKE "%'.$keyword.'%"
        OR projects.project_name LIKE "%'.$keyword.'%"
        OR projects.project_num LIKE "%'.$keyword.'%"
        OR subjects.random_num LIKE "%'.$keyword.'%"
        OR subject_status.status_date LIKE "%'.$keyword.'%"
        )
        ';
      }
      else
      {
        $params['select'] = '*, subject_status.status AS subject_stat';
        $params['table'] = 'subjects
        LEFT JOIN volunteers v ON v.subject_id = subjects.subject_id
        LEFT JOIN projects ON projects.project_id = subjects.project_id
        LEFT JOIN subject_status ON subject_status.status_id = subjects.status_id';
        $params['where'] = " subjects.project_id = ".$project_id;
      }

    }
    //$params['debug'] = 1;
    $result = jp_get($params);

    $data = array();
    while($row = mysqli_fetch_assoc($result)){

      $data[] = $row;

    }

    return $data;
  }
