<?php

if (isset($_GET['action']) && $_GET['action'] != '') {

  require("../jp_library/jp_lib.php");

  if(isset($_POST['data'])){

    parse_str($_POST['data'], $_POST);
    $_POST['data'] = false;

  }


  $_GET['action'](true);
  if (!isset($_SESSION['user_id']))
  session_start();

}


function addDrugRecord(){

  $params['table'] = "drug_records";

  $_POST["date"] = date("Y-m-d", strtotime($_POST["date"]));
  $_POST["drug_id"] = $_POST["drug_idR"];


  // ----------- Start of check file ext @Rai----------- //
  $bValid = true;
  if(isset($_FILES['receipt']['name'])){

    foreach($_FILES['receipt']['name'] as $imgKey => $imgVal ){

      $fileName = preg_replace('#[^a-z.0-9]#i', '', $imgVal);

      $file = explode(".", $fileName);
      $fileExt = end($file);

      // if( !in_array($fileExt, ['doc', 'docx', 'pdf']) )
      // $bValid = $bValid && false;

    }

  }
  // ----------- End of check file ext @Rai----------- //

  if($bValid){

    if(isset($_POST["drug_record_id"]) && $_POST["drug_record_id"] != ""){

      $success = "updated";
      $error = "updating";

      $_POST["updated_by"] = $_SESSION["role_id"];
      $_POST["date_update"] = date("Y-m-d");

      $drug_record_id = $_POST["drug_record_id"];
      unset($_POST["drug_record_id"]);

      $params['where'] = "drug_record_id = ".$drug_record_id;
      $params['data'] = $_POST;

      /*******************
      * LOGS START HERE! *
      *******************/
      $t = new Clinical\Helpers\Translation($_SESSION['lang']);
      $p = new Clinical\Helpers\Project($_POST['project_id']);
      $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

      # notify DA here so he can double check
      $p->notify_group = array_diff($p->notify_group, array_diff($p->positions, ['DA']));
      Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang'], $t->tryTranslate('da_todo_14_0'), $t->tryTranslate('da_dbl_chk_drug_records'));
      # / notify DA here so he can double check

      $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'modify',
      array(
        $t->tryTranslate('project_name') => "$p->project_name",
        $t->tryTranslate('module') => $t->tryTranslate('drug_management') . ": " . $t->tryTranslate('drug_record'),
        $t->tryTranslate('number') => "$p->project_num",
        $t->tryTranslate('submitter') => "$u->fname"
        )
      );
      $l->save();

      /*******************
      * LOGS END HERE!   *
      *******************/

      /*************************
      * DOUBLE CHECK RESET!!   *
      **************************/
      $dc = new Clinical\Helpers\DoubleCheck("drug_records", "drug_record_id", $drug_record_id, $_SESSION['lang']);
      $dc->fetch("drug_records", "drug_record_id", $drug_record_id);
      $dc->resetPayload();
      $dc->updatePayload("drug_records", "drug_record_id", $drug_record_id);
      /*************************
      * / DOUBLE CHECK RESET!! *
      **************************/

      $result = jp_update($params);

    }else{

      $success = "added";
      $error = "adding";

      $_POST["added_by"] = $_SESSION["role_id"];
      $_POST["date_added"] = date("Y-m-d");

      $params['data'] = $_POST;
      //$params['debug'] = 1;

      /*******************
      * LOGS START HERE! *
      *******************/
      $t = new Clinical\Helpers\Translation($_SESSION['lang']);
      $p = new Clinical\Helpers\Project($_POST['project_id']);
      $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

      # notify DA here so he can double check
      $p->notify_group = array_diff($p->notify_group, array_diff($p->positions, ['DA']));
      Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang'], $t->tryTranslate('da_todo_14_0'), $t->tryTranslate('da_dbl_chk_drug_records'));

      $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'submit_the_record',
      array(
        $t->tryTranslate('project_name') => "$p->project_name",
        $t->tryTranslate('module') => $t->tryTranslate('drug_management') . ": " . $t->tryTranslate('drug_record'),
        $t->tryTranslate('number') => "$p->project_num",
        $t->tryTranslate('submitter') => "$u->fname"
        )
      );
      $l->save();

      /*******************
      * LOGS END HERE!   *
      *******************/

      $result = jp_add($params);
      $drug_record_id = jp_last_added();

    }


    // ----------- Start of upload file @Rai----------- //
    if(isset($_FILES['receipt']['name'])){

      $i = 0;

      foreach($_FILES['receipt']['name'] as $imgKey => $imgVal ){

        if($_FILES['receipt']['size'][$i] > 0){

          $targetDir = "../drug-records/";

          $fileName = preg_replace('#[^a-z.0-9]#i', '', $imgVal);

          $file = explode(".", $fileName);
          $fileExt = end($file);

          $newFileName = $drug_record_id."--&--".time().rand().".".$fileExt;
          $newFile = $targetDir.basename($newFileName);
          $fileSize = getimagesize($_FILES['receipt']['tmp_name'][$i]);

          if( move_uploaded_file($_FILES['receipt']['tmp_name'][$i], $newFile) ){

            $receipt['drug_record_id'] = $drug_record_id;
            $receipt['receipt_file'] = $newFileName;
            $receipt['receipt'] = $fileName;

            $receipt___['table'] = "drug_record_receipts";
            $receipt___['data'] = $receipt;
            jp_add($receipt___);

          }

        }

        $i++;

      }

    }
    // ----------- End of upload file @Rai----------- //

  }


  $i = array();
  if( $result && $bValid){

    $i['type'] = "success";
    $i['msg'] = "操作成功";

  }else if(!$bValid){

    $i['type'] = "error";
    $i['msg'] = "您已上传无效的档案。";

  }else{

    $i['type'] = "error";
    $i['msg'] = "手术失败";

  }

  echo json_encode($i);

}


function getDrugRecords($type = "", $project_id = ""){

  $project_id = isset($_POST["drug_record_id"]) && $_POST["drug_record_id"] != "" ? $_POST["project_id"] : $project_id;

  $params['select'] = 'drug_records.*, drugs.trade_name, drugs.lot_number, drugs.drug_id';
  $params['table'] = 'drug_records
  LEFT JOIN drugs ON drugs.drug_id = drug_records.drug_id';

  $params['where'] = 'drugs.project_id = '.$project_id;

  if(isset($_POST["drug_record_id"]) && $_POST["drug_record_id"] != "")
  $params['where'] .= ' AND drug_record_id = '.$_POST["drug_record_id"];

  //$params['debug'] = 1;
  $params['filters'] = "ORDER BY drug_records.drug_record_id DESC";
  $result = jp_get($params);

  $data = array();
  $i = 0;
  while($row = mysqli_fetch_assoc($result)){

    $row["date"] = date("m/d/Y", strtotime($row["date"]));
    // $row["storage"] = $row["lot_number"] - $row["quantity"]; #not as simple as it seems
    # $storage = (drugs.lot_number + all*drug_records.put_in_storage) - all*drug_records.put_out_storage

    # all*drug_records.put_in_storage
    $getAllPutIn['select'] = "SUM(quantity) as total_put_in";
    $getAllPutIn['table'] = "drug_records";
    $getAllPutIn['where'] = "drug_id=".$row['drug_id']." AND record_type='put_in_storage'";
    $resAllPutIn = jp_get($getAllPutIn);
    $rowAllPutIn = mysqli_fetch_assoc($resAllPutIn);

    # all*drug_records.put_out_storage
    $getAllPutOut['select'] = "SUM(quantity) as total_put_out";
    $getAllPutOut['table'] = "drug_records";
    $getAllPutOut['where'] = "drug_id=".$row['drug_id']." AND (record_type='put_out_storage' OR record_type='waste')";
    $resAllPutOut = jp_get($getAllPutOut);
    $rowAllPutOut = mysqli_fetch_assoc($resAllPutOut);

    $row["storage"] = ($row['lot_number'] + $rowAllPutIn['total_put_in']) - $rowAllPutOut['total_put_out'];


    if(isset($_POST["type"])){ // include receipt file list

      $receiptParam['select'] = 'receipt_file, receipt';
      $receiptParam['table'] = "drug_record_receipts";
      $receiptParam['where'] = 'drug_record_id = '.$_POST["drug_record_id"];

      $receiptResult = jp_get($receiptParam);

      $receipt = array(
        "receipt_file" => "",
        "receipt" => ""
      );

      while($receiptRow = mysqli_fetch_assoc($receiptResult)){

        $receipt['receipt_file'][$i] = $receiptRow['receipt_file'];
        $receipt['receipt'][$i] = $receiptRow['receipt'];
        $i++;

      }

      $data[] = array_merge($row, $receipt);

    }else
    $data[] = $row;

  }

  if(isset($_POST["type"]))
  echo json_encode($data);
  else
  return $data;


}

function getDrugRemarks($ajax = false){

  $params['select'] = 'remarks';
  $params['table'] = 'drug_records';
  $params['where'] = 'drug_record_id = '.$_POST['drug_record_id'];
  $result = jp_get($params);
  $data = [];
  while($row = mysqli_fetch_assoc($result))
  {
    $data[] = $row;
  }

  if(isset($_POST["type"]) && $_POST["type"] == "ajax")
  {
    echo json_encode($data);
  }
  else
  {
    return $data;
  }
}
function addDrugRemarks($ajax = false){
  $drug_record_id = $_POST['drug_record_id'];
  unset($_POST['drug_record_id']); # We unset this because the $_POST array will contain it if we don't

  $data = "0";
  # required data manipulation before saving to DB

  $params['table'] = 'drug_records';
  $params['data'] = $_POST;
  $params['where'] = "drug_record_id = '" . $drug_record_id . "'";

  if(jp_update($params))
  {
    $data = "1";
    if($ajax == false)
    {
      return $data;
    }
    else
    {
      echo $data;
    }
  }
}
