<?php

    if (isset($_GET['action']) && $_GET['action'] != '') {

        require("../jp_library/jp_lib.php");

        if(isset($_POST['data'])){

            parse_str($_POST['data'], $_POST);
            $_POST['data'] = false;

        }


        $_GET['action'](true);
        if (!isset($_SESSION['user_id']))
            session_start();

    }


    function addDrugs(){


        $params['table'] = "drugs";

        $_POST["validity_from"] = date("Y-m-d", strtotime($_POST["validity_from"]));
        $_POST["validity_to"] = date("Y-m-d", strtotime($_POST["validity_to"]));

        // UPDATE FUNCTIONALITY: upload for inpsection input - START 12/27/2016
        $dir_location = "uploads/drug_records/";
        $traverser = __DIR__ . '/../'; #to go backwards in our file structure

        $full_path = $traverser . $dir_location;

        #this crap right here is for creating a folder!!!
        if (!is_dir($full_path) && !mkdir($full_path, 0777, true)){
          mkdir($full_path, 0777, true);
        }
        if(isset($_FILES['inspection']['tmp_name']) && $_FILES['inspection']['tmp_name'] != "")
        {
            $id_card = "drug_record_".time();
            $_POST['inspection'] = jp_upload($_FILES['inspection'],$id_card,"../uploads/drug_records/");
            $_POST['inspection'] = $GLOBALS['base_url'] . "uploads/drug_records/" . $_POST['inspection'];
        }
        // UPDATE FUNCTIONALITY: upload for inpsection input - END

        $_POST["project_id"] = $_POST["project_idR"];

        if(isset($_POST["drug_id"]) && $_POST["drug_id"] != ""){

            $success = "updated";
            $error = "updating";

            $_POST["updated_by"] = $_SESSION["role_id"];
            $_POST["date_update"] = date("Y-m-d");

            $drug_id = $_POST["drug_id"];
            unset($_POST["drug_id"]);

            $params['where'] = "drug_id = ".$drug_id;
            $params['data'] = $_POST;

              /*******************
              * LOGS START HERE! *
              *******************/
              $t = new Clinical\Helpers\Translation($_SESSION['lang']);
              $p = new Clinical\Helpers\Project($_POST['project_id']);
              $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

              # notify DA here so he can double check
              $p->notify_group = array_diff($p->notify_group, array_diff($p->positions, ['DA']));
              Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang'], $t->tryTranslate('da_todo_14_0'), $t->tryTranslate('da_dbl_chk_drugs'));
              # / notify DA here so he can double check

              $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'modify',
              array(
                $t->tryTranslate('project_name') => "$p->project_name",
                $t->tryTranslate('module') => $t->tryTranslate('drug_management') . ": " . $t->tryTranslate('drugs'),
                $t->tryTranslate('number') => "$p->project_num",
                $t->tryTranslate('submitter') => "$u->fname"
                )
              );
              $l->save();

              /*******************
              * LOGS END HERE!   *
              *******************/

              /*************************
              * DOUBLE CHECK RESET!!   *
              **************************/
              $dc = new Clinical\Helpers\DoubleCheck("drugs", "drug_id", $drug_id, $_SESSION['lang']);
              $dc->fetch("drugs", "drug_id", $drug_id);
              $dc->resetPayload();
              $dc->updatePayload("drugs", "drug_id", $drug_id);
              /*************************
              * / DOUBLE CHECK RESET!! *
              **************************/

            $result = jp_update($params);

        }else{

            $success = "added";
            $error = "adding";

            $_POST["added_by"] = $_SESSION["role_id"];
            $_POST["date_added"] = date("Y-m-d");

            $params['data'] = $_POST;

            /*******************
            * LOGS START HERE! *
            *******************/
            $t = new Clinical\Helpers\Translation($_SESSION['lang']);
            $p = new Clinical\Helpers\Project($_POST['project_id']);
            $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

            # notify DA here so he can double check
            $p->notify_group = array_diff($p->notify_group, array_diff($p->positions, ['DA']));
            Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang'], $t->tryTranslate('da_todo_14_0'), $t->tryTranslate('da_dbl_chk_drugs'));

            $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'submit_the_record',
            array(
              $t->tryTranslate('project_name') => "$p->project_name",
              $t->tryTranslate('module') => $t->tryTranslate('drug_management') . ": " . $t->tryTranslate('drugs'),
              $t->tryTranslate('number') => "$p->project_num",
              $t->tryTranslate('submitter') => "$u->fname"
              )
            );
            $l->save();

            /*******************
            * LOGS END HERE!   *
            *******************/

            $result = jp_add($params);

        }

        $i = array();
        if( $result ){

            $i['type'] = "success";
            $i['msg'] = "您已成功添加药品"; #successfully added a drug

        }else{

            $i['type'] = "error";
            $i['msg'] = "您未成功添加药物";

        }

        echo json_encode($i);

    }


    function getDrugs($type = "", $project_id = ""){

        $params['table'] = 'drugs';

        if(isset($_POST["type"]))
            $params['where'] = 'project_id = '.$_POST["project_id"];
        else
            $params['where'] = 'project_id = '.$project_id;

        if(isset($_POST["drug_id"]) && $_POST["drug_id"] != "")
            $params['where'] .= ' AND drug_id = '.$_POST["drug_id"];

        //$params['debug'] = 1;
        $params['filters'] = "ORDER BY drug_id DESC";
        $result = jp_get($params);

        $data = array();
        while($row = mysqli_fetch_assoc($result)){

            $row["validity_from"] = date("m/d/Y", strtotime($row["validity_from"]));
            $row["validity_to"] = date("m/d/Y", strtotime($row["validity_to"]));

            # all*drug_records.put_in_storage
            $getAllPutIn['select'] = "SUM(quantity) as total_put_in";
            $getAllPutIn['table'] = "drug_records";
            $getAllPutIn['where'] = "drug_id=".$row['drug_id']." AND record_type='put_in_storage'";
            $resAllPutIn = jp_get($getAllPutIn);
            $rowAllPutIn = mysqli_fetch_assoc($resAllPutIn);

            # all*drug_records.put_out_storage
            $getAllPutOut['select'] = "SUM(quantity) as total_put_out";
            $getAllPutOut['table'] = "drug_records";
            $getAllPutOut['where'] = "drug_id=".$row['drug_id']." AND (record_type='put_out_storage' OR record_type='waste')";
            $resAllPutOut = jp_get($getAllPutOut);
            $rowAllPutOut = mysqli_fetch_assoc($resAllPutOut);

            $row["storage"] = ($row['lot_number'] + $rowAllPutIn['total_put_in']) - $rowAllPutOut['total_put_out'];

            $data[] = $row;

        }

        if(isset($_POST["type"])){
            echo json_encode($data);
        }else
            return $data;


    }
?>
