<?php

    if (isset($_GET['action']) && $_GET['action'] != '') {

        require("../jp_library/jp_lib.php");

        if(isset($_POST['data'])){

            parse_str($_POST['data'], $_POST);
            $_POST['data'] = false;

        }


        $_GET['action'](true);
        if (!isset($_SESSION['user_id']))
            session_start();

    }


    function addInspection(){


        $params['table'] = "inspections";

        $_POST["report_date"] = date("Y-m-d", strtotime($_POST["report_date"]));

        $_POST["project_id"] = $_POST["project_idI"];

        // ----------- Start of check file ext @Rai----------- //
        $bValid = true;
        if(isset($_FILES['report']['name'])){

            foreach($_FILES['report']['name'] as $imgKey => $imgVal ){

                $fileName = preg_replace('#[^a-z.0-9]#i', '', $imgVal);

                $file = explode(".", $fileName);
                $fileExt = end($file);
                if( !in_array($fileExt, ['jpg', 'txt', 'png', 'xls', 'xlsx', 'jpeg', 'gif', 'doc', 'docx', 'pdf']) )
                    $bValid = $bValid && false;

            }

        }
        // ----------- End of check file ext @Rai----------- //

        if($bValid){

            if(isset($_POST["inspection_id"]) && $_POST["inspection_id"] != ""){

                $success = "You have successfuly updated an inspection report";
                if($_SESSION['lang'] != "english" )
                {
                    $success = "您已成功添加质量保证记录";
                }
                $error = "updating";

                $_POST["updated_by"] = $_SESSION["role_id"];
                $_POST["date_update"] = date("Y-m-d");
                $_POST["role"] = $_SESSION['role_id']; #set the role id here

                $inspection_id = $_POST["inspection_id"];
                unset($_POST["inspection_id"]);

                $params['where'] = "inspection_id = ".$inspection_id;
                $params['data'] = $_POST;
                $result = jp_update($params);

                /*******************
                * LOGS START HERE! *
                *******************/
                $t = new Clinical\Helpers\Translation($_SESSION['lang']);
                $p = new Clinical\Helpers\Project($_POST['project_id']);
                $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

                $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'modify',
                array(
                  $t->tryTranslate('project_name') => "$p->project_name",
                  $t->tryTranslate('module') => $t->tryTranslate('inspection'),
                  $t->tryTranslate('number') => "$p->project_num",
                  $t->tryTranslate('submitter') => "$u->fname"
                  )
                );
                $l->save();

                /*******************
                * LOGS END HERE!   *
                *******************/

            }else{

                $success = "You have successfuly added an inspection report";
                if($_SESSION['lang'] != 'english')
                {
                    $success = "您已成功添加质量保证记录";
                }
                $error = "adding";

                $_POST["role"] = $_SESSION['role_id'];
                $_POST["added_by"] = $_SESSION["role_id"];
                $_POST["date_added"] = date("Y-m-d");

                $params['data'] = $_POST;
                //$params['debug'] = 1;
                $result = jp_add($params);
                $inspection_id = jp_last_added();

                /*******************
                * LOGS START HERE! *
                *******************/
                $t = new Clinical\Helpers\Translation($_SESSION['lang']);
                $p = new Clinical\Helpers\Project($_POST['project_id']);
                $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

                $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'submit_the_record',
                array(
                  $t->tryTranslate('project_name') => "$p->project_name",
                  $t->tryTranslate('module') => $t->tryTranslate('inspection'),
                  $t->tryTranslate('number') => "$p->project_num",
                  $t->tryTranslate('submitter') => "$u->fname"
                  )
                );
                $l->save();

                /*******************
                * LOGS END HERE!   *
                *******************/

            }

            // ----------- Start of upload file @Rai----------- //
            if(isset($_FILES['report']['name'])){

                $i = 0;

                foreach($_FILES['report']['name'] as $imgKey => $imgVal ){

                    if($_FILES['report']['size'][$i] > 0){

                        $targetDir = "../inspection/";

                        $fileName = preg_replace('#[^a-z.0-9]#i', '', $imgVal);

                        $file = explode(".", $fileName);
                        $fileExt = end($file);

                        $newFileName = $inspection_id."--&--".time().rand().".".$fileExt;
                        $newFile = $targetDir.basename($newFileName);
                        $fileSize = getimagesize($_FILES['report']['tmp_name'][$i]);

                        if( move_uploaded_file($_FILES['report']['tmp_name'][$i], $newFile) ){

                            $report['inspection_id'] = $inspection_id;
                            $report['inspection_file'] = $GLOBALS['base_url'] . "inspection/" . $newFileName;
                            $report['inspection'] = $fileName;
                            if(!isset($_POST['databank']))
                            {
                                $_POST['databank'] = "0";
                            }
                            $report['databank'] = $_POST['databank'];

                            $report___['table'] = "inspection_uploads";
                            $report___['data'] = $report;
                            jp_add($report___);

                        }

                    }

                    $i++;

                }

            }
            // ----------- End of upload file @Rai----------- //

        }

        $i = array();
        if( $result ){

            $i['type'] = "success";
            $i['msg'] = $success;

        }else{

            $i['type'] = "error";
            $i['msg'] = "An error occurred upon ".$error." a inspection report.";

        }

        echo json_encode($i);

    }

                                                        // // $projects = all projects assignment - only for subject-library @Rai
    function getInspection($type = "", $project_id = "", $projects = array()){

        $params['select'] = 'inspections.*, CONCAT( users.fname, " ", users.lname) AS uploaded_by';
        $params['table'] = 'inspections
                            LEFT JOIN roles ON roles.role_id = inspections.added_by
                            LEFT JOIN users ON users.user_id = roles.user_id';

        if($type == "list")
            $params['where'] = 'project_id = '.$project_id;
        else if($type == "listAll"){

            // ------------------- Start of filter all inpections per project assignment  @Rai-------------------- //
            $projectFilter = "";

            if(count($projects) > 0){

                $count = 0;
                foreach ($projects as $proj) {

                    if($count == 0)
                        $projectFilter .= "project_id = ".$proj;
                    else
                        $projectFilter .= " OR project_id = ".$proj;

                    $count++;

                }

                $projectFilter = "(".$projectFilter.")";

            }

            $params['where'] = $projectFilter;

            // ------------------- End of filter all inpections per project assignment  @Rai-------------------- //

        }



        if(isset($_POST["inspection_id"]) && $_POST["inspection_id"] != "")
            $params['where'] = ' inspection_id = '.$_POST["inspection_id"];

        //$params['debug'] = 1;
        $params['filters'] = "ORDER BY inspections.inspection_id DESC";
        $result = jp_get($params);

        $data = array();
        $i = 0;
        while($row = mysqli_fetch_assoc($result)){

            $row["report_date"] = date("m/d/Y", strtotime($row["report_date"]));

            if(isset($_POST["type"])){ // include report uploads list

                $reportParam['select'] = 'inspection_file, inspection';
                $reportParam['table'] = "inspection_uploads";
                $reportParam['where'] = 'inspection_id = '.$_POST["inspection_id"];

                $reportResult = jp_get($reportParam);

                $report = array(
                        "inspection_file" => "",
                        "inspection" => ""
                    );

                while($reportRow = mysqli_fetch_assoc($reportResult)){

                    $report['inspection_file'][$i] = $reportRow['inspection_file'];
                    $report['inspection'][$i] = $reportRow['inspection'];
                    $i++;

                }

                $data[] = array_merge($row, $report);

            }else
                $data[] = $row;

        }

        if(isset($_POST["type"])){
            echo json_encode($data);
        }else
            return $data;


    }
?>
