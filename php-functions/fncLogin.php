<?php
require("../jp_library/jp_lib.php");
require("../php-functions/fncSubjects.php");

if (session_status() == PHP_SESSION_NONE) {
    //session_start();
}


if (isset($_GET['action']) && $_GET['action'] != '') {
    parse_str($_POST['data'], $_POST);
    $_POST['data'] = false;
    $_GET['action']();
}

function adminLogin()
{
    $echo_data = 0; // for ajax echo

    $checkAdmin['table'] = 'users';
    $checkAdmin['where'] = 'mobile_num = "' . $_POST['mm_num'] . '" AND ' .
        'password = "' . sha1($_POST['pp_password']) . '" AND ' .
        'is_active = 1 AND ' .
        'is_admin = 1';
    //$checkAdmin['debug'] = 1;
    $res_admin = jp_get($checkAdmin);

    while ($row_admin = mysqli_fetch_array($res_admin)) {

        // --------- Start of check and update volunteer and subject statuses  @Rai--------- //
            updateStatuses('week'); // function call from fncSubjects.php
            updateStatuses('month'); // function call from fncSubjects.php
        // --------- End of check and update volunteer and subject statuses  @Rai--------- //

        $_SESSION['admin'] = true;
        $_SESSION['user_id'] = $row_admin['user_id'];
        $_SESSION['full_name'] = $row_admin['fname'] . " " . $row_admin['lname'];
        $_SESSION['lang'] = 'chinese'; #set default language to chinese because why not
        $echo_data = 1;
    }

    echo $echo_data;
}

function applicantLogin()
{
    $echo_data = 0; // for ajax echo

    $params['table'] = 'users u LEFT JOIN
                        roles r ON u.user_id = r.user_id LEFT JOIN
                        positions p ON r.position_id = p.position_id';
    $params['where'] = 'u.mobile_num = "' . $_POST['mm_num'] . '" AND ' .
        'u.password = "' . sha1($_POST['pp_password']) . '" AND ' .
        'u.is_active = 1 AND ' . #active user
        'u.is_admin = 0 AND '. #not an admin
        'r.role_id = '.$_POST['rr_role_id']; #specific role
        //$checkAdmin['debug'] = 1;
    $result = jp_get($params);

    while ($row = mysqli_fetch_array($result)) {

         // --------- Start of check and update volunteer and subject statuses  @Rai--------- //
            updateStatuses('week'); // function call from fncSubjects.php
            updateStatuses('month'); // function call from fncSubjects.php
        // --------- End of check and update volunteer and subject statuses  @Rai--------- //

        $_SESSION['admin'] = false;
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['full_name'] = $row['fname'] . " " . $row['lname'];
        $_SESSION['lang'] = 'chinese'; #set default language to chinese because why not
        $_SESSION['user_type'] = $row['acronym']; # Position type of user
        $_SESSION['is_professonial_group'] = $row['is_professional_group']; #boolean-int if professional group
        $_SESSION['role_id'] = $row['role_id']; # to check what role_id is currently logged in by the user
        $_SESSION['pgroup_id'] = $row['pgroup_id'];
        $_SESSION['pos_desc'] = $row['description'];
        $_SESSION['lang_phrase'] = $row['lang_phrase'];
        $_SESSION['lang'] = $_POST['language'];
        $echo_data = 1;

        $l = new Clinical\Helpers\Log($row['role_id'], 'login');
        $res = $l->save();
    }

    echo $echo_data;
}
