<?php

   if (isset($_GET['action']) && $_GET['action'] != '') {
      require("../jp_library/jp_lib.php");

      if(isset($_POST['data']))
      {
        parse_str($_POST['data'], $_POST);
        $_POST['data'] = false;
      }
      $_GET['action'](true);
      if (!isset($_SESSION['user_id'])) {
        session_start();
      }
    }

    function getMembers($ajax = false, $project_id = false)
    {
        $get_members['select'] = "clinician_id, bio_controller_id, nurse_id, med_controller_id, subj_controller_id, data_administrator_id";
        $get_members['table'] = 'projects';
        $get_members['where'] = 1;
        if($project_id != false)
        {
          $get_members['where'] = 'project_id = '.$project_id;
        }
        $res_members = jp_get($get_members);
        $data = array();
        while($row_members = mysqli_fetch_assoc($res_members))
        {
          $row_members['clinician_id'] = $row_members['clinician_id'] == "" ? 0: $row_members['clinician_id'];
          $row_members['bio_controller_id'] = $row_members['bio_controller_id'] == "" ? 0: $row_members['bio_controller_id'];
          $row_members['nurse_id'] = $row_members['nurse_id'] == "" ? 0: $row_members['nurse_id'];
          $row_members['med_controller_id'] = $row_members['med_controller_id'] == "" ? 0: $row_members['med_controller_id'];
          $row_members['subj_controller_id'] = $row_members['subj_controller_id'] == "" ? 0: $row_members['subj_controller_id'];
          $row_members['data_administrator_id'] = $row_members['data_administrator_id'] == "" ? 0: $row_members['data_administrator_id'];
          $get_user['table'] = 'users u LEFT JOIN
                                roles r ON u.user_id = r.user_id LEFT JOIN
                                positions p ON r.position_id = p.position_id LEFT JOIN
                                professional_group pg ON r.pgroup_id = pg.pgroup_id';
          $get_user['where'] = 'p.acronym IN ("C", "RN", "BGC", "MC", "SC", "DA") AND ';
          $get_user['where'] .= "r.role_id IN 
                                (".str_replace("*", "", $row_members['clinician_id']).", 
                                 ".str_replace("*", "", $row_members['bio_controller_id']).", 
                                 ".str_replace("*", "", $row_members['nurse_id']).", 
                                 ".str_replace("*", "", $row_members['med_controller_id']).", 
                                 ".str_replace("*", "", $row_members['subj_controller_id']).", 
                                 ".str_replace("*", "", $row_members['data_administrator_id']).")";
          // $get_user['debug'] = 1;
          $res_user = jp_get($get_user);
          $ctr = 0;
          while($row_user = mysqli_fetch_assoc($res_user))
          {
            $data[$ctr] = $row_user;
            $ctr++;
          }
        }
        if ($ajax == false) {
            return $data;
        } else {
            echo $data;
        }
    }

    function getPositionDetails($ajax = false, $acronym)
    {
      $get_lang_phrase['select'] = "lang_phrase, position_id";
      $get_lang_phrase['table'] = "positions";
      $get_lang_phrase['where'] = "acronym = '".$acronym."'";
      $res_lang_phrase = jp_get($get_lang_phrase);
      while($row=mysqli_fetch_assoc($res_lang_phrase))
      {
        $data = $row;
      }
      if ($ajax == false) {
          return $data;
      } else {
          echo $data;
      }
    }

    function addProjectMembers($ajax = false)
    {
      # Check if they already exist in roles table
      $checkUser['select'] = "u.user_id, r.role_id";
      $checkUser['table'] = "users u LEFT JOIN
                             roles r ON u.user_id = r.user_id LEFT JOIN
                             positions p ON r.position_id = p.position_id";
      $checkUser['where'] = "p.position_id = ".$_POST['member_position_type'];
      $_POST["pgroup_id"] = $_POST['pgroup_id_mem'];
      $ctr_ok = 1;
      $set_role_id = array();
      $user_id_exsting = array();
      $checkUser['where'] .= " AND r.user_id IN (".implode(",", $_POST['member_user_id']).")";
      // $checkUser['debug'] = 1;
      $resUser = jp_get($checkUser);
      while($rowUser = mysqli_fetch_assoc($resUser))
      {
        $set_role_id[] .= "*".$rowUser['role_id']."*";
        $user_id_exsting[] = $rowUser['user_id'];
      }
      $new_user_id = array_diff($_POST['member_user_id'], $user_id_exsting);
      // 
      $add_member['table'] = "roles";
      $_POST['position_id'] = $_POST["member_position_type"];
      $_POST['pgroup_id'] = $_POST["pgroup_id_mem"];
      $ctr_ok = 1;
      foreach ($new_user_id as $user_id)
      {
        if($ctr_ok == 1 && $user_id != 0 && $user_id != "")
        {
          $_POST['user_id'] = $user_id;
          $add_member['data'] = $_POST;
          if(!jp_add($add_member))
          {
            $ctr_ok = 0;
          }
          else
          {
            $set_role_id[] .= "*".jp_last_added()."*";
          }
        }
      }
      if($ctr_ok == 1)
      {
        # check if first time saving members and get current status
        $get_current_project_details['select'] = "p.clinician_id, p.bio_controller_id, p.nurse_id, p.med_controller_id, p.subj_controller_id, p.data_administrator_id, s.status_num";
        $get_current_project_details['table'] = "projects p LEFT JOIN status s ON p.status_id = s.status_id";
        $get_current_project_details['where'] = "p.project_id = ".$_POST['project_id_mem'];
        $res_current_project_details = jp_get($get_current_project_details);
        $row_current_project_details = mysqli_fetch_assoc($res_current_project_details);
        if(($row_current_project_details['clinician_id'] == "0"
           || $row_current_project_details['bio_controller_id'] == "0"
           || $row_current_project_details['nurse_id'] == "0"
           || $row_current_project_details['med_controller_id'] == "0"
           || $row_current_project_details['subj_controller_id'] == "0"
           || $row_current_project_details['data_administrator_id'] == "0")
           && $row_current_project_details['status_num'] < "14")
        {
            $status_data = array
                            (
                              "status_num" => "14",
                              "reviewer_id" => $_SESSION['role_id'],
                              "is_rejected" => "0",
                              "project_id" => $_POST['project_id_mem']
                            );
            $change_status['table'] = "status";
            $change_status['data'] = $status_data;
            if(jp_add($change_status))
            {
              $update_project['table'] = "projects";
              $update_project['where'] = "project_id=".$_POST['project_id_mem'];
              $update_project['data'] = array("status_id" => jp_last_added());
              jp_update($update_project);
            }
        }

        if(isset($_POST['project_id_mem']))
        {
          $check_position['select'] = "acronym";
          $check_position['table'] = "positions";
          $check_position['where'] = "position_id=".$_POST['position_id'];
          $res_position = jp_get($check_position);
          $row_position = mysqli_fetch_assoc($res_position);

          // $proj_members = array(tbl_positions[acronym] => tbl_projects[equivalent to acronym]);
          $proj_members = array(
            "BGC" => "bio_controller_id",
            "MC" => "med_controller_id",
            "SC" => "subj_controller_id",
            "DA" => "data_administrator_id"
            );
          $user_concat = implode(",", $set_role_id);
          $members_data = array($proj_members[$row_position['acronym']] => $user_concat);

          $set_members['table'] = "projects";
          $set_members['where'] = "project_id=".$_POST['project_id_mem'];
          $set_members['data'] = $members_data;
          // print_r($members_data);
          if(jp_update($set_members))
          {
            if ($ajax == false)
            {
              return "1";
            } else {
                echo "1";
            }
          }
        }
      }
    }
?>
