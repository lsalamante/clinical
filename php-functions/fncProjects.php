<?php
/*
Includes: Functions concerning Projects such as geting status, count of pending/passed/rejected, etc
Available for ajax
*/
if (isset($_GET['action']) && $_GET['action'] != '') {
  require("../jp_library/jp_lib.php");

  if(isset($_POST['data']))
  {
    parse_str($_POST['data'], $_POST);
    $_POST['data'] = false;
  }
  $_GET['action'](true);
  if (!isset($_SESSION['user_id'])) {
    session_start();
  }
}
/*************************************************************************************************/
/*
Description: Count all projects by logged in user categorized by needs (i.e. Check pending, Pass, No pass, etc.)
Purpose: Output for dashboard for users reference
Parameters: NONE
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/
function getProjectStatusCount($ajax = false)
{
  $data = array();
  $status_pending = "";
  $status_pass = "";
  $status_rejected = "";

  if($_SESSION['user_type'] == 'PI') #PRINCIPAL INVESTIGATOR
  {
    # Variables will be attached to where after "s.status_num IN "
    $status_pending = "(2) AND p.pi_id = ".$_SESSION['user_id']; #status for projects viewable by PI, with no actions done yet
    $status_pass = "(3) AND p.pi_id = ".$_SESSION['user_id']; #status for projects that was approved by PI
    $status_rejected = "(0) AND p.project_status = 2 AND p.pi_id = ".$_SESSION['user_id']; #status for projects that was rejected by PI
  }
  elseif($_SESSION['user_type'] == 'ECS') # ETHICS COMMITTEE SECRETARY
  {
    $status_pending = "(7,9,12)";
    $status_pass = "(8,10,13)";
    $status_rejected = "(7)";
  }
  elseif($_SESSION['user_type'] == 'ECC') # ETHICS COMMITTEE CHAIRMAN
  {
    # Variables will be attached to where after "s.status_num IN "
    $status_pending = "(11)";
    $status_pass = "(12)";
    $status_rejected = "(9)";
  }
  elseif($_SESSION['user_type'] == 'ECM') # ETHICS COMMITTEE MEMBER
  {
    # Variables will be attached to where after "s.status_num IN "
    $status_pending = "(8)";
    $status_pass = "(9)";
    $status_rejected = "(0)";
  }
  elseif($_SESSION['user_type'] == 'PRE') # Peer Review Expert
  {
    # Variables will be attached to where after "s.status_num IN "
    $status_pending = "(3)";
    $status_pass = "(5)";
  }
  elseif($_SESSION['user_type'] == 'TFO') # Testing Facility Office
  {
    # Variables will be attached to where after "s.status_num IN "
    $status_pending = "(1,6)";
    $status_pass = "(2)";
    $status_rejected = "(0)";
  }
  elseif($_SESSION['user_type'] == "I")
  {
    # Variables will be attached to where after "s.status_num IN "
    $status_pending = "(3,5,10,13)";
    $status_pass = "(4, 14)";
    $status_rejected = "(3, 7, 10)";
  }
  elseif($_SESSION['user_type'] == "BGC")
  {
    $status_pending = "(14,15,16)";
    $status_pass = "(null)";
    $status_rejected = "(null)";

  }elseif($_SESSION['user_type'] == "C"){// for Clinician

    $status_pending = "(14)";
    $status_pass = "(0)";
    $status_rejected = "(0)";

  }  elseif($_SESSION['user_type'] == "RN" || $_SESSION['user_type'] == "MC"){// for Clinician

    $status_pending = "(12,13,14)";
    $status_pass = "(0)";
    $status_rejected = "(0)";

  }


  $get_projects_count['select'] = 'COUNT(p.project_id) as total';
  $get_projects_count['table'] = 'projects p LEFT JOIN
  status s ON p.status_id = s.status_id';
  $get_projects_count['where'] = 's.is_rejected = 0 AND s.status_num IN '.$status_pending; #condition to get pending status
  if($_SESSION['user_type'] == "I" || $_SESSION['user_type'] == "ECS")
  {
    $get_projects_count['where'] = "s.status_num IN ".$status_pending;
  }
  #all projects pending
  $res_projects_pending = jp_get($get_projects_count);
  $data['pending'] = mysqli_fetch_assoc($res_projects_pending);
  #all projects pass
  $get_projects_count['where'] = 's.reviewer_id = '.$_SESSION['role_id'].' AND s.is_rejected = 0 AND s.status_num IN '.$status_pass; #change where to get passed status
  $res_projects_pass = jp_get($get_projects_count);
  $data['pass'] = mysqli_fetch_assoc($res_projects_pass);
  if($status_rejected != '') # for users who cannot reject
  {
    #all projects rejected
    $get_projects_count['where'] = 's.reviewer_id = '.$_SESSION['role_id'].' AND s.is_rejected = 1 AND p.project_status = 2 AND s.status_num IN '.$status_rejected;
    // if($_SESSION['user_type'] == 'ECC' ) # &#@*!@&$(&!@%! SPECIAL CONDITION FOR STATUS 10
    // {
    //   $get_projects_count['select'] = "p.*, s.*";
    //   $get_projects_count['debug'] = 1;
    //   $res_projects_rejected = jp_get($get_projects_count);
    //   $ECC_total_rejected = 0;
    //   while($row_ECC = mysqli_fetch_assoc($res_projects_rejected))
    //   {
    //     // if($row_ECC['status_num'] != "9")
    //     // {
    //     //   $ECC_total_rejected++;
    //     // }
    //     // else
    //     // {
    //     //   $data['pending']['total'] = $data['pending']['total'] + 1;
    //     // }
    //   }
    //   $data['rejected']['total'] = $ECC_total_rejected;
    // }
    // else
    // { # Update by Van, this is commented out because of *%@&^#&@# changes in the flow and status_num values was !@$R#$#@ changed :)
      $res_projects_rejected = jp_get($get_projects_count);
      $data['rejected'] = mysqli_fetch_assoc($res_projects_rejected);
    // } # Update by Van, this is commented out because of *%@&^#&@# changes in the flow and status_num values was !@$R#$#@ changed :)
  }
  if($_SESSION['user_type'] == 'TFO')
  {
    $get_projects_count['table'] .= ' LEFT JOIN roles r ON s.reviewer_id = r.role_id LEFT JOIN
    positions ps ON r.position_id = ps.position_id';
    $get_projects_count['where'] = 's.is_rejected = 0 AND s.status_num IN (7, 8, 9, 11) AND ps.acronym != "A"'; #change where to get passed status
    //$get_projects_count['debug'] = 1;
    $res_projects_pass = jp_get($get_projects_count);
    $data['ethics'] = mysqli_fetch_assoc($res_projects_pass);
  }
  if ($ajax == false) {
    return $data;
  } else {
    echo $data;
  }

}
/*************************************************************************************************/
/*
Description: Get All projects if user type is not Applicant
Purpose: Dynamic based on logged in user
Parameters: NONE
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/

function getAllProjects($ajax = false)
{
  #TODO check if array is needed for $pending, $pass, $no_pass
  $data = array(); $pending = array(); $pass = array(); $no_pass = array();
  if($_SESSION['lang'] == 'english')
  {
    $status_label_pending = "Check Pending";
    $status_label_rejected = "No Pass";
    $status_label_done = "Pass";
    $return_app_rej_pi = "Returned to Applicant Rejected by PI";
    $return_I_comments_App = 'Returned to Investigator with comments By Applicant';
    $return_app_I_mod_ecc = "Returned to Applicant and Investigator for modification by Ethics Committee Chairman";
    $rejected_new_version_i_ecc = "Rejected new version from Investigator by Ethics Committee Chairman";
  }
  else
  {
    $status_label_pending = "待审核";
    $status_label_rejected = "审核不通过";
    $status_label_done = "审核通过";
    $return_app_rej_pi = "返回申请人被PI拒绝";
    $return_I_comments_App = "由申请人返回研究者";
    $return_app_I_mod_ecc = "返回申请人和调查员由道德委员会主席修改";
    $rejected_new_version_i_ecc = "拒绝新版本从调查员由伦理委员会主席";
  }


  if($_SESSION['user_type'] == 'PI')  #PRINCIPAL INVESTIGATOR
  {
    # Variables will be attached to where in sql where "s.status_num >= "
    $where = ""; # TODO : delete this line ? SPECIAL AT THE BOTTOM
    $pending[] .= "2";      #for status name return for pending projects
    $pass[] .= "3";         #for status name return for pass projects
    $no_pass[] .= "0";      #for status name return for no pass projects
  }
  elseif($_SESSION['user_type'] == 'ECS')
  {
    # Variables will be attached to where in sql where "s.status_num >= "
    $where = "7";
    $pending = array("7", "9", "12");      #for status name return for pending projects
    $pass = array("8", "10", "13");         #for status name return for pass projects
    $no_pass = array("7");            #for status name return for no pass projects
  }
  elseif($_SESSION['user_type'] == 'ECC')  #ETHICS COMMITTEE CHAIRMAN
  {
    # Variables will be attached to where in sql where "s.status_num >= "
    $where = "11";
    $pending = array("11");      #for status name return for pending projects
    $pass = array("12");         #for status name return for pass projects
    $no_pass = array("9");            #for status name return for no pass projects
  }
  elseif($_SESSION['user_type'] == 'ECM')  #ETHICS COMMITTEE CHAIRMAN
  {
    # Variables will be attached to where in sql where "s.status_num >= "
    $where = "8";
    $pending = array("8");      #for status name return for pending projects
    $pass = array("9");         #for status name return for pass projects
  }
  elseif($_SESSION['user_type'] == 'PRE')
  {
    # Variables will be attached to where in sql where "s.status_num >= "
    $where = "3 AND s.status_num <= 5";
    $pending = array("3");      #for status name return for pending projects
    $pass = array("5");         #for status name return for pass projects
    if($_SESSION['lang'] == 'english')
    {
      $status_label_done = "Done";
      $status_label_pending = "Available for comments";
    }
    else
    {
      $status_label_done = "完成";
      $status_label_pending = "待评议";
    }

    $checkpre['select'] = 'project_id';
    $checkpre['table'] = 'projects_pre';
    $checkpre['where'] = 'role_id = '.$_SESSION['role_id'];
    //$checkpre['debug'] = 1;
    $getPre = jp_get($checkpre);
    $AllPre = array();
    while($rowPre = mysqli_fetch_assoc($getPre))
    {
      $AllPre[] .= $rowPre['project_id'];
      //print_r($AllPre);
    }
    //
  }
  elseif($_SESSION['user_type'] == 'TFO')
  {
    # Variables will be attached to where in sql where "s.status_num >= "
    $where = "1 OR s.status_num = 0";
    $pending = array("1", "6");
    $pass = array("2");
    $no_pass = array("0");
    $ethics_pending = array("7", "8", "9", "11");
  }
  elseif($_SESSION['user_type'] == 'I')
  {
    # Variables will be attached to where in sql where "s.status_num >= "
    //$where = "(4, 5, 6, 8, 11, 12, 13)";
    $where = "3";
    $pending = array("3", "5", "10", "13");
    $pass = array("4", "11", "14");  # TODO status 6 should be passed? but doesn't need any approve Rejected

  }else if($_SESSION['user_type'] == 'RN' || $_SESSION['user_type'] == 'MC' || $_SESSION['user_type'] == 'BGC' || $_SESSION['user_type'] == 'SC'){ // get all projects assigned to Nurse or Medical Controller under status 13 and above

    $where = "12";

  }else if($_SESSION['user_type'] == 'C'){ // get all projects assigned to clinician under status 14 and above

    $where = "14";

  }else if($_SESSION['user_type'] == 'A'){ // get all projects assigned to clinician under status 14 and above

    $where = "1";

  }
  elseif($_SESSION['user_type'] == "DA")
  {
    $where = "13";
  }


  $get_projects['table'] = 'projects p LEFT JOIN
  status s ON p.status_id = s.status_id';
  $get_projects['where'] = 's.status_num >= '.$where; #condition to get pending status
  if($_SESSION['is_professonial_group'] == 1)
  {
    $get_projects['where'] .= ' AND p.pgroup_id = '.$_SESSION['pgroup_id'];
  }
  if($_SESSION['user_type'] == 'PI')
  { #PRINCIPAL INVESTIGATOR
    $get_projects['where'] = "(s.status_num >= 2 OR s.status_num = 0) AND p.pi_id = ".$_SESSION['user_id'];
  }
  if($_SESSION['user_type'] == 'RN')
  { #get all projects assigned to Research nurse
    $get_projects['where'] = "(s.status_num >= 13) AND p.nurse_id = ".$_SESSION['role_id'];
  }
  if($_SESSION['user_type'] == 'C')
  { #get all projects assigned to Clinician
    $get_projects['where'] = "(s.status_num >= 14) AND p.clinician_id = ".$_SESSION['role_id'];
  }
  if($_SESSION['user_type'] == 'MC' || $_SESSION['user_type'] == 'BGC' || $_SESSION['user_type'] == 'SC')
  { #get all projects assigned to medicine controller and biological sample controller
    #get all projects with status number 14. this is the last part in the flow
    $get_projects['where'] = "(s.status_num >= 14)";
    if($_SESSION['user_type'] == 'MC')
      $get_projects['where'] .= " AND p.med_controller_id LIKE '%*".$_SESSION['role_id']."*%'";
    elseif($_SESSION['user_type'] == 'SC')
      $get_projects['where'] .= " AND p.subj_controller_id LIKE '%*".$_SESSION['role_id']."*%'";
    else
      $get_projects['where'] .= " AND p.bio_controller_id LIKE '%*".$_SESSION['role_id']."*%'";
  }

  if(isset($_GET['trial_id'])){
    $get_projects['where'] .= " AND p.trial_id = '" . $_GET['trial_id']  . "'";
  }
  $get_projects['filters'] = 'ORDER BY p.date_created DESC';
  // $get_projects['debug'] = 1;
  $res_projects = jp_get($get_projects);
  $i = 0;
  while($row = mysqli_fetch_assoc($res_projects))
  {
    $cant_add = true;
    if($_SESSION['user_type'] == "PRE")
    {
      if(!in_array($row['project_id'], $AllPre))
      {
        $cant_add = false;
      }
    }

    if($row['is_rejected'] == 1)
    {
      if($row['reviewer_id'] == $_SESSION['role_id'])
      {
        $row['status_name'] = $status_label_rejected;
        if($row['status_num'] == 9 && $_SESSION['user_type'] == "ECS")
        $row['status_name'] = $status_label_pending;
      }
      else
      {
        if($row['status_num'] == 0)
        {
          $row['status_name'] = $return_app_rej_pi;
          if($_SESSION['user_type'] == "PI")
          {
            $cant_add = false;
          }
        }
        elseif($row['status_num'] == 3)
        {
          $row['status_name'] = $return_I_comments_App;
          if($_SESSION['user_type'] == "I")
          {
            $row['status_name'] = $status_label_pending;
          }
        }
        elseif($row['status_num'] == 7)
        {
          $row['status_name'] = $return_app_I_mod_ecc;
        }
        elseif($row['status_num'] == 9)
        {
          $row['status_name'] = $rejected_new_version_i_ecc;
        }
      }
    }
    else
    {
      if(isset($ethics_pending) && in_array($row['status_num'], $ethics_pending))
      {
        if($_SESSION['lang'] == 'english')
        {
          $row['status_name'] = 'Ethics Pending';
        }
        else
        {
          $row['status_name'] = "伦理审核中";
        }

      }
      else
      {
        if(in_array($row['status_num'], $pending))
        $row['status_name'] = $status_label_pending;
        elseif(in_array($row['status_num'], $pass))
        $row['status_name'] = $status_label_done;
        else
        {
          $row['status_name'] = fncApproved($row['status_num']);
        }

      }
    }

    if($cant_add)
    {
      $data[$i] = $row;
    }

    $i++;
  }
  if ($ajax == false) {
    return $data;
  } else {
    echo json_encode($data);
  }

}

function getProjectInformation($ajax = false){
  $params['select'] = 'p.*, s.status_id, s.status_num, s.reviewer_id, s.is_rejected, s.status_date, u.fname, u.lname';
  $params['table'] = 'projects p LEFT JOIN status s ON p.status_id = s.status_id LEFT JOIN users u ON p.user_id = u.user_id';
  $params['where'] = '';
  $params['filters'] = "ORDER BY p.date_created DESC";

  if(isset($_GET['trial_id']) == false && isset($_GET['status_num']) == false){
    $params['where'] = '1';
  }else{
    foreach($_GET as $key => $value){
      $operator = '=';
      if($key == 'status_num'){
        $operator = '=';
      }
      if($key == 'squery'){
        continue;
      }
      $params['where'] .= "$key $operator '$value' AND ";
    }
    $params['where'] = rtrim($params['where'], ' AND ');
  }

  if(isset($_GET['squery'])){
    $q = $_GET['squery'];

    $params['where'] .= " AND
    (p.project_id LIKE '%$q%' OR
    p.status_id LIKE '%$q%' OR
    p.lot_num LIKE '%$q%' OR
    p.project_num LIKE '%$q%' OR
    p.project_name LIKE '%$q%' OR
    p.trial_id LIKE '%$q%' OR
    p.indications LIKE '%$q%' OR
    p.risk_category LIKE '%$q%' OR
    p.dept_name LIKE '%$q%' OR
    p.pgroup_id LIKE '%$q%' OR
    p.user_id LIKE '%$q%' OR
    p.emergency_contact LIKE '%$q%' OR
    p.emergency_mobile LIKE '%$q%' OR
    p.emergency_email LIKE '%$q%' OR
    p.cro_name LIKE '%$q%' OR
    p.cro_mobile LIKE '%$q%' OR
    p.cro_email LIKE '%$q%' OR
    p.research_objectives LIKE '%$q%' OR
    p.subsidize_type LIKE '%$q%' OR
    p.remarks LIKE '%$q%' OR
    p.date_created LIKE '%$q%' OR
    p.date_modified LIKE '%$q%')
    ";
  }

  if($_SESSION['is_professonial_group'] == 1)
  {
    $params['where'] .= ' AND p.pgroup_id = '.$_SESSION['pgroup_id'];
  }
  if($_SESSION['user_type'] == 'PI')
  { #PRINCIPAL INVESTIGATOR
    $params['where'] .= " AND (s.status_num >= 2 OR s.status_num = 0) AND p.pi_id = ".$_SESSION['user_id'];
  }
  if($_SESSION['user_type'] == 'RN')
  { #get all projects assigned to Research nurse
    $params['where'] .= " AND (s.status_num >= 13) AND p.nurse_id = ".$_SESSION['role_id'];
  }
  if($_SESSION['user_type'] == 'C')
  { #get all projects assigned to Clinician
    $params['where'] .= " AND (s.status_num >= 14) AND p.clinician_id = ".$_SESSION['role_id'];
  }
  if($_SESSION['user_type'] == 'MC' || $_SESSION['user_type'] == 'BGC' || $_SESSION['user_type'] == 'SC' || $_SESSION['user_type'] == 'DA')
  { #get all projects assigned to medicine controller and biological sample controller
    #get all projects with status number 14. this is the last part in the flow
    $params['where'] .= " AND (s.status_num >= 14)";
    if($_SESSION['user_type'] == 'MC')
      $params['where'] .= " AND p.med_controller_id LIKE '%*".$_SESSION['role_id']."*%'";
    elseif($_SESSION['user_type'] == 'SC')
      $params['where'] .= " AND p.subj_controller_id LIKE '%*".$_SESSION['role_id']."*%'";
    elseif($_SESSION['user_type'] == 'DA')
      $params['where'] .= " AND p.data_administrator_id LIKE '%*".$_SESSION['role_id']."*%'";
    else
      $params['where'] .= " AND p.bio_controller_id LIKE '%*".$_SESSION['role_id']."*%'";
  }

  // $params['debug'] = 1;
  $params['where'] .= " AND status_num != -1";
  $result = jp_get($params);

  if($result){

    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }

}

function getAllApplicantProjectsFilters($ajax = false)
{
  $params['select'] = 'p.*, s.status_id, s.status_num, s.reviewer_id, s.is_rejected, s.status_date';
  $params['table'] = 'projects p LEFT JOIN status s ON p.status_id = s.status_id';
  $params['where'] = "user_id = '". $_SESSION['user_id'] ."'";
  $params['filters'] = "ORDER BY p.date_created DESC";

  if(isset($_GET['trial_id']) == false && isset($_GET['status_num']) == false){
    $params['where'] = "user_id = '". $_SESSION['user_id'] ."'";
  }else{
    foreach($_GET as $key => $value){
      $operator = '=';
      if($key == 'status_num'){
        $operator = '=';
      }
      if($key == 'squery'){
        continue;
      }
      $params['where'] .= " AND $key $operator '$value' ";
    }
    $params['where'] = rtrim($params['where'], ' AND ');
  }

  if(isset($_GET['squery'])){
    $q = $_GET['squery'];

    $params['where'] .= " AND
    (p.project_name LIKE '%$q%')
    ";
  }
  // $params['debug'] = 1;

  $result = jp_get($params);

  if($result){

    if ($ajax == false) {
      return $result;
    } else {
      echo json_encode($result);
    }
  }else{
    if ($ajax == false) {
      return false;
    } else {
      echo false;
    }
  }

}

/*************************************************************************************************/
/*
Description: get all remarks of specific project
Purpose: get all remarks to be outputed on specific project when viewing
Parameters: project_id => PK of project to get remarks
position_acronym => what user type entered the comment
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/
function getAllRemarks($ajax = false, $project_id)
{
  $get_remarks['table'] = "project_remarks pr LEFT JOIN
  status s ON pr.status_id = s.status_id LEFT JOIN
  roles r ON s.reviewer_id = r.role_id LEFT JOIN
  positions p ON r.position_id = p.position_id";
  $get_remarks['where'] = "pr.project_id = '$project_id'"; #" AND p.acronym = '$position_acronym'";
  $get_remarks['filters'] = "ORDER BY s.status_date DESC";
  //$get_remarks['debug'] = 1;
  $res_remarks = jp_get($get_remarks);
  $i = 0;
  $data = array();
  while($row_remarks = mysqli_fetch_assoc($res_remarks))
  {
    $data[$row_remarks['description']][] = $row_remarks;
    $i++;
  }
  if ($ajax == false) {
    return $data;
  } else {
    echo json_encode($data);
  }
}
/*************************************************************************************************/
/*
Description: Change Status of Project (Log status in TABLE: status)
Purpose: Log status in TABLE: `status`, Change status_id in TABLE: `projects`, Log Comments in comments (if any) in TABLE: project_remarks
Parameters: NONE
Created by: Vanananana
*/

/* If Updated by Others than the Creator, LOG here Please
Updated by Developer:
Reason:
Date Updated (mm-dd-yyyy H:i:s):
*/
function changeStatus($ajax = false)
{
  $t = new Clinical\Helpers\Translation($_SESSION['lang']);
  $p_before = new Clinical\Helpers\Project($_POST['project_id']);
  # project state before updating

  #ALL reject Status that can happen in current flow: "current status" => "reject status"
  $reject_status_num =
  array(
    "1" => "0",
    "2" => "0",
    "4" => "3",
    "7" => "7",
    "11" => "9"
  );

  $status_data = array(); # status data to be saved in TABLE: `status`
  $project_data = array(); # project to be UPDATED in TABLE: `projects`
  $status_data['reviewer_id'] = $_SESSION['role_id'];
  $status_data['project_id'] = $_POST['project_id'];
  $status_data['status_num'] = ($_POST['project_status'] + 1);
  $status_data['status_date'] = date("Y-m-d H:i:s");
  $project_data['project_status'] = "1";
  $change_status['table'] = 'status';
  if($_POST['status'] != 1) # if rejected
  {
    $status_data['status_num'] = $reject_status_num[$_POST['project_status']]; # get status for reject based on current status
    $status_data['is_rejected'] = "1";
    $project_data['project_status'] = "2"; # 2 = project rejected, to be used in updating TABLE: `project`
  }
  $change_status['data'] = $status_data; # set values to be saved in TABLE: `status`
  if(jp_add($change_status))
  {
    $new_status_id = jp_last_added(); # get saved status_id

    $project_data['status_id'] = $new_status_id;

    $project_data['status_id'] = $new_status_id;

    # Update 2/7/2017 - not needed any more, change in function
    //
    // if(isset($_POST['clinician']))
    // {
    //   $project_data['clinician_id'] = $_POST['clinician'];
    // }
    // if(isset($_POST['nurse']))
    // {
    //   $project_data['nurse_id'] = $_POST['nurse'];
    // }
    // if(isset($_POST['bio_controller']))
    // {
    //   $project_data['bio_controller_id'] = $_POST['bio_controller'];
    // }
    // if(isset($_POST['med_controller']))
    // {
    //   $project_data['med_controller_id'] = $_POST['med_controller'];
    // }
    //
    # Update 2/7/2017 - not needed any more, change in function

    $update_project['table'] = 'projects';
    $update_project['where'] = 'project_id = '.$_POST['project_id'];
    $update_project['data'] = $project_data;
    //$update_project['debug'] = 1;
    if(jp_update($update_project))
    {
      $return = 1;
    }
    if(isset($_POST['comments']) && $_POST['comments'] != "") # check if there are comments
    {
      $return = 0;
      $remarks_data = array();
      $remarks_data['remarks'] = $_POST['comments'];
      $remarks_data['project_id'] = $_POST['project_id']; # correlate with project for easy access, not normalized since related with status, but efficient for getting comments only without status history
      $remarks_data['status_id'] = $new_status_id; # correlate with specific status to keep track where remarks was inserted
      $save_remarks['table'] = 'project_remarks';
      $save_remarks['data'] = $remarks_data;
      if(jp_add($save_remarks))
      {
        $return = 1;
      }
    }
    if(isset($_POST['add_comments']) && $_POST['add_comments'] != "")
    {
      $return = 0;
      $docs_remarks_data = array();
      $docs_remarks_data['remarks'] = $_POST['add_comments'];
      $docs_remarks_data['project_id'] = $_POST['project_id']; # correlate with project for easy access, not normalized since related with status, but efficient for getting comments only without status history
      $docs_remarks_data['status_id'] = $new_status_id; # correlate with specific status to keep track where remarks was inserted
      $docs_remarks_data['remark_type'] = 1;
      $save_remarks['table'] = 'project_remarks';
      $save_remarks['data'] = $docs_remarks_data;
      if(jp_add($save_remarks))
      {
        $return = 1;
      }
    }
    if(isset($_POST['ecm_comments']) && $_POST['ecm_comments'] != "")
    {
      $return = 0;
      $docs_remarks_data = array();
      $docs_remarks_data['remarks'] = $_POST['ecm_comments'];
      $docs_remarks_data['project_id'] = $_POST['project_id']; # correlate with project for easy access, not normalized since related with status, but efficient for getting comments only without status history
      $docs_remarks_data['status_id'] = $new_status_id; # correlate with specific status to keep track where remarks was inserted
      $docs_remarks_data['remark_type'] = 2;
      $save_remarks['table'] = 'project_remarks';
      $save_remarks['data'] = $docs_remarks_data;
      if(jp_add($save_remarks))
      {
        $return = 1;
      }
    }
    if(isset($_POST['pre_experts']) && count($_POST['pre_experts'])>0 )
    {
      $return = 0;
      $savePRE['table'] = 'projects_pre';
      $data['project_id'] = $_POST['project_id'];
      $data['reviewer_id'] = $_SESSION['role_id'];
      foreach ($_POST['pre_experts'] as $key => $PRE_role_id) {
        $return = 0;
        $data['role_id'] = "";
        $data['role_id'] = $PRE_role_id;
        $savePRE['data'] = $data;
        if(jp_add($savePRE))
        {
          $return = 1;
        }

      }
    }
  }

  #notifications
  $p = new Clinical\Helpers\Project($_POST['project_id']);
  if(($p->is_rejected && $p->status_num == 7 ) || $p->status_num == 4 || $p->status_num == 17 || ($p->is_rejected && $p->status_num == 0 ))
  $p->notify_group[$p->project_owner_role_id] = 'A';

  if($p->is_rejected && $p->status_num == 7 ){
    $p->fetchGlobalUsers(); # function to get I
    # Notify only I and A
    $p->notify_group = array_diff($p->notify_group, array_diff($p->positions, ['I', 'A']));
  }

  $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

  Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang']);

  /**  This block is for setting the module name */
  switch($p_before->status_num){
    case 3:
    $module_name = $t->tryTranslate('project_plan');
    break;
    case 5:
    $module_name = $t->tryTranslate('project_documents');
    break;
    case 7:
    if($_SESSION['user_type'] == 'A' && $p_before->is_rejected)
    $module_name = $t->tryTranslate('project_plan'); #
    break;
    case 8:
    $module_name = $t->tryTranslate('ecm_opinion');
    break;
    case 9:
    $module_name = $t->tryTranslate('project_documents');
    break;
    case 10:
    $module_name = $t->tryTranslate('project_plan');
    break;
    case 12:
    $module_name = $t->tryTranslate('project_documents');
    break;

    default:
    $module_name = $t->tryTranslate('not_applicable');
    break;
  }
  /** / This block is for setting the module name   */

  #logs
  if($p_before->is_rejected){
    #if current project has been rejected, set action as `modify`
    # $p_before is instantiated at the first line of this function
    $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'modify',
    array(
      $t->tryTranslate('project_name') => "$p->project_name",
      $t->tryTranslate('module') => "$module_name",
      $t->tryTranslate('number') => "$p->project_num",
      $t->tryTranslate('submitter') => "$u->fname"
      )
    );
  }
  elseif($_POST['status'] != 1){ # if rejected
    $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'reject',
    array(
      $t->tryTranslate('project_name') => "$p->project_name",
      $t->tryTranslate('module') => "$module_name",
      $t->tryTranslate('number') => "$p->project_num",
      $t->tryTranslate('submitter') => "$u->fname"
      )
    );
  }
  else  {
    $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'submit_the_record',
    array(
      $t->tryTranslate('project_name') => "$p->project_name",
      $t->tryTranslate('module') => "$module_name",
      $t->tryTranslate('number') => "$p->project_num",
      $t->tryTranslate('submitter') => "$u->fname"
      )
    );
  }
  $l->save();

  if ($ajax == false)
  {
    return $return;
  } else {
    echo($return);
  }

}

function stopProject ($ajax = false){

  $result = 0;

  $params['table'] = 'status';
  $params['data'] = array(
    "status_num" => -1,
    "reviewer_id" => $_SESSION['role_id'],
    "project_id" => $_POST['project_id']
  );
  if(jp_add($params)){
    $last_id = jp_last_added();

    $params['table'] = 'projects';
    $params['where'] = "project_id = " . $_POST['project_id'];
    $params['data'] = array(
      "status_id" => $last_id,
    );
    $result = jp_update($params);
  }

  if($ajax)
  echo $result;
  else
  return $result;
}

/**
 * gets all projects in an associative multidimensional array $arr['pending'][0] and so on
 * @param  boolean $ajax [description]
 * @return array        [description]
 */
function getFilteredProjects($ajax = false)
{
  $data = array();
  $status_pending = "";
  $status_pass = "";
  $status_rejected = "";

  if($_SESSION['user_type'] == 'PI') #PRINCIPAL INVESTIGATOR
  {
    # Variables will be attached to where after "s.status_num IN "
    $status_pending = "(2) AND p.pi_id = ".$_SESSION['user_id']; #status for projects viewable by PI, with no actions done yet
    $status_pass = "(3) AND p.pi_id = ".$_SESSION['user_id']; #status for projects that was approved by PI
    $status_rejected = "(0) AND p.project_status = 2 AND p.pi_id = ".$_SESSION['user_id']; #status for projects that was rejected by PI
  }
  elseif($_SESSION['user_type'] == 'ECS') # ETHICS COMMITTEE SECRETARY
  {
    $status_pending = "(7,9,12)";
    $status_pass = "(8,10,13)";
    $status_rejected = "(7)";
  }
  elseif($_SESSION['user_type'] == 'ECC') # ETHICS COMMITTEE CHAIRMAN
  {
    # Variables will be attached to where after "s.status_num IN "
    $status_pending = "(11)";
    $status_pass = "(12)";
    $status_rejected = "(9)";
  }
  elseif($_SESSION['user_type'] == 'ECM') # ETHICS COMMITTEE MEMBER
  {
    # Variables will be attached to where after "s.status_num IN "
    $status_pending = "(8)";
    $status_pass = "(9)";
    $status_rejected = "(0)";
  }
  elseif($_SESSION['user_type'] == 'PRE') # Peer Review Expert
  {
    # Variables will be attached to where after "s.status_num IN "
    $status_pending = "(3)";
    $status_pass = "(5)";
  }
  elseif($_SESSION['user_type'] == 'TFO') # Testing Facility Office
  {
    # Variables will be attached to where after "s.status_num IN "
    $status_pending = "(1,6)";
    $status_pass = "(2)";
    $status_rejected = "(0)";
  }
  elseif($_SESSION['user_type'] == "I")
  {
    # Variables will be attached to where after "s.status_num IN "
    $status_pending = "(3,5,10,13)";
    $status_pass = "(4, 14)";
    $status_rejected = "(3, 7, 10)";
  }
  elseif($_SESSION['user_type'] == "BGC")
  {
    $status_pending = "(14,15,16)";
    $status_pass = "(null)";
    $status_rejected = "(null)";

  }elseif($_SESSION['user_type'] == "C"){// for Clinician

    $status_pending = "(14)";
    $status_pass = "(0)";
    $status_rejected = "(0)";

  }  elseif($_SESSION['user_type'] == "RN" || $_SESSION['user_type'] == "MC"){// for Clinician

    $status_pending = "(12,13,14)";
    $status_pass = "(0)";
    $status_rejected = "(0)";

  }

  // $get_projects_count['select'] = 'COUNT(p.project_id) as total';
  $get_projects_count['table'] = 'projects p LEFT JOIN
  status s ON p.status_id = s.status_id';
  $get_projects_count['where'] = 's.is_rejected = 0 AND s.status_num IN '.$status_pending; #condition to get pending status
  if($_SESSION['user_type'] == "I" || $_SESSION['user_type'] == "ECS")
  {
    $get_projects_count['where'] = "s.status_num IN ".$status_pending;
  }
  #### FILTERS ####
  $get_projects_count['filters'] = 'ORDER BY p.project_id DESC';
  #### /FILTERS ####
  $res_projects_pending = jp_get($get_projects_count);
  while($row = mysqli_fetch_assoc($res_projects_pending))
  $data['pending'][] = $row;

  $get_projects_count['where'] = 's.reviewer_id = '.$_SESSION['role_id'].' AND s.is_rejected = 0 AND s.status_num IN '.$status_pass; #change where to get passed status
  $res_projects_pass = jp_get($get_projects_count);
  while($row = mysqli_fetch_assoc($res_projects_pass))
  $data['pass'][] = $row;

  $get_projects_count['where'] = 's.reviewer_id = '.$_SESSION['role_id'].' AND s.is_rejected = 1 AND p.project_status = 2 AND s.status_num IN '.$status_rejected;
  $res_projects_rejected = jp_get($get_projects_count);
  while($row = mysqli_fetch_assoc($res_projects_rejected))
  $data['rejected'][] = $row;

  $get_projects_count['table'] .= ' LEFT JOIN roles r ON s.reviewer_id = r.role_id LEFT JOIN
  positions ps ON r.position_id = ps.position_id';
  $get_projects_count['where'] = 's.is_rejected = 0 AND s.status_num IN (7, 8, 9, 11) AND ps.acronym != "A"'; #change where to get passed status
  $res_projects_pass = jp_get($get_projects_count);
  while($row = mysqli_fetch_assoc($res_projects_pass))
  $data['ethics'][] = $row;


  if(empty($data['pending']))
  $data['pending'] = array();
  if(empty($data['pass']))
  $data['pass'] = array();
  if(empty($data['rejected']))
  $data['rejected'] = array();

  if ($ajax == false) {
    return $data;
  } else {
    echo $data;
  }

}
