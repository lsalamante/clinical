<?php

if (isset($_GET['action']) && $_GET['action'] != '') {
  require("../jp_library/jp_lib.php");

  if(isset($_POST['data']))
  {
    parse_str($_POST['data'], $_POST);
    $_POST['data'] = false;
  }
  $_GET['action'](true);
  if (!isset($_SESSION['user_id'])) {
    session_start();
  }
}

// $projects = all projects assignment - only for subject-library @Rai
function getSideEffects($ajax = false, $project_id = false, $projects = array(), $sae = null)
{
  $all_sideeffects['table'] = "side_effect se LEFT JOIN projects p ON se.project_id = p.project_id";
  $all_sideeffects['where'] = "se.is_drafted != 2";
  if($project_id != false)
  {
    $all_sideeffects['where'] .= " AND se.project_id = ".$project_id;
  }

  // ------------------- Start of filter all side effect per project assignment  @Rai-------------------- //

  if(count($projects) > 0 && $project_id == false){

    $projectFilter = "";

    $count = 0;
    foreach ($projects as $proj) {

      if($count == 0)
      $projectFilter .= "se.project_id = ".$proj;
      else
      $projectFilter .= " OR se.project_id = ".$proj;

      $count++;

    }

    $projectFilter = "(".$projectFilter.")";

    $all_sideeffects['where'] = $projectFilter;

  }



  // ------------------- End of filter all side effect per project assignment  @Rai-------------------- //

  // $all_sideeffects['debug'] = 1;
  if($sae != null && $sae == 'sae')
  {
    $all_sideeffects['where'] .= " AND se.is_sae = 1";
  }
  else
  {
    $all_sideeffects['where'] .= " AND se.is_sae != 1";
  }
  if($_SESSION['user_type'] == 'DA')
  {
    $all_sideeffects['where'] .= " AND se.is_drafted = 0";
  }
  $all_sideeffects['filters'] = "ORDER BY side_effect_id DESC";
  $res_sideeffects = jp_get($all_sideeffects);
  $data = array(); $i = 0;
  while($row_sideeffects = mysqli_fetch_assoc($res_sideeffects))
  {
    $data[$row_sideeffects['side_effect_id']] = $row_sideeffects;
  }

  if ($ajax == false) {
    return $data;
  } else {
    echo $data;
  }
}

function addSideEffect($ajax = false)
{
  $data = 0;
  $_POST['report_type'] = implode(",", $_POST['report_type']);
  $_POST['report_time'] = date("Y-m-d H:i:s", strtotime($_POST['report_time']));
  $_POST['sub_birthdate'] = date("Y-m-d H:i:s", strtotime($_POST['sub_birthdate']));
  $_POST['project_id'] = $_POST['project_idSE'];
  $_POST['created_by'] = $_SESSION["role_id"];
  $_POST["date_created"] = date("Y-m-d H:i:s");
  $add_side['table'] = "side_effect";
  $add_side['data'] = $_POST;
  if(isset($_POST['side_effect_id_update']) && $_POST['side_effect_id_update'] != '')
  {
    $add_side['where'] = 'side_effect_id = '.$_POST['side_effect_id_update'];
    if(jp_update($add_side))
    {
      $data = 2;
      /*******************
      * LOGS START HERE! *
      *******************/
      $t = new Clinical\Helpers\Translation($_SESSION['lang']);
      $p = new Clinical\Helpers\Project($_POST['project_id']);
      $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

      if($_POST['is_drafted'] == 0){
        # notify DA here so he can double check
        $p->notify_group = array_diff($p->notify_group, array_diff($p->positions, ['DA']));
        Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang'], $t->tryTranslate('da_todo_14_0'), $t->tryTranslate('da_dbl_chk_side_effect'));
        # / notify DA here so he can double check
      }

      $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'modify',
      array(
        $t->tryTranslate('project_name') => "$p->project_name",
        $t->tryTranslate('module') => $t->tryTranslate('side_effects'),
        $t->tryTranslate('number') => "$p->project_num",
        $t->tryTranslate('submitter') => "$u->fname"
        )
      );
      $l->save();

      /*******************
      * LOGS END HERE!   *
      *******************/

      /*************************
      * DOUBLE CHECK RESET!!   *
      **************************/
      $dc = new Clinical\Helpers\DoubleCheck("side_effect", "side_effect_id", $_POST['side_effect_id_update'], $_SESSION['lang']);
      $dc->fetch("side_effect", "side_effect_id", $_POST['side_effect_id_update']);
      $dc->resetPayload();
      $dc->updatePayload("side_effect", "side_effect_id", $_POST['side_effect_id_update']);
      /*************************
      * / DOUBLE CHECK RESET!! *
      **************************/

    }

  }
  else
  {
    if(jp_add($add_side))
    {
      $data = 1;

    }
  }
  if ($ajax == false) {
    return $data;
  } else {
    echo $data;
  }
}

function addSideEffectBlank($ajax = false)
{
  $data = 0;
  $add_temp['table'] = "side_effect";
  $add_temp['data'] = $_POST;
  if(jp_add($add_temp))
  {
    /*******************
    * LOGS START HERE! *
    *******************/
    $t = new Clinical\Helpers\Translation($_SESSION['lang']);
    $p = new Clinical\Helpers\Project($_POST['project_id']);
    $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

    $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'submit_the_record',
    array(
      $t->tryTranslate('project_name') => "$p->project_name",
      $t->tryTranslate('module') => $t->tryTranslate('side_effects'),
      $t->tryTranslate('number') => "$p->project_num",
      $t->tryTranslate('submitter') => "$u->fname"
      )
    );
    $l->save();

    /*******************
    * LOGS END HERE!   *
    *******************/
    $data = jp_last_added();
  }
  if ($ajax == false) {
    return $data;
  } else {
    echo $data;
  }
}


function getSideEffectDetails($ajax = false)
{
  $get_side_effect['table'] = 'side_effect';
  $get_side_effect['where'] = 'side_effect_id = '.$_POST['side_effect_id'];
  $res_side_effect = jp_get($get_side_effect);
  $data = array();
  while($row_side_effect = mysqli_fetch_assoc($res_side_effect))
  {
    $row_side_effect['report_type'] = explode(",", $row_side_effect['report_type']);
    $data[] = $row_side_effect;
  }
  if(isset($_POST["type"]) && $_POST["type"] == "ajax")
  {
    echo json_encode($data);
  }
  else
  {
    return $data;
  }
}
function getSideEffectRemarks($ajax = false){

  $params['select'] = 'clinician_remarks';
  $params['table'] = 'side_effect';
  $params['where'] = 'side_effect_id = '.$_POST['side_effect_id'];
  $result = jp_get($params);
  $data = [];
  while($row = mysqli_fetch_assoc($result))
  {
    $data[] = $row;
  }

  if(isset($_POST["type"]) && $_POST["type"] == "ajax")
  {
    echo json_encode($data);
  }
  else
  {
    return $data;
  }
}

function addSideEffectRemarks($ajax = false){
  $side_effect_id = $_POST['side_effect_id'];
  unset($_POST['side_effect_id']); # We unset this because the $_POST array will contain it if we don't

  $data = "0";
  # required data manipulation before saving to DB

  $params['table'] = 'side_effect';
  $params['data'] = $_POST;
  $params['where'] = "side_effect_id = '" . $side_effect_id . "'";

  if(jp_update($params))
  {
    $data = "1";
    if($ajax == false)
    {
      return $data;
    }
    else
    {
      echo $data;
    }
  }
}

function getDocuments($ajax = false, $side_effect_id = false)
{
  $all_docs['table'] = "side_effects_docs";
  $all_docs['where'] = 1;
  if($side_effect_id != false)
  {
    $all_docs['where'] = "side_effect_id = ".$side_effect_id;
  }
  $res_docs = jp_get($all_docs);
  $data = array(); $i = 0;
  while($row_docs = mysqli_fetch_assoc($res_docs))
  {
    $data[$row_docs['side_effect_doc_id']] = $row_docs;
  }
  if ($ajax == false) {
    return $data;
  } else {
    echo $data;
  }
}

function getSubjectData($ajax = false)
{
  $get_subject['table'] = "volunteers";
  $get_subject['where'] = "subject_id = ".$_POST['subject_id'];
  $res_subject = jp_get($get_subject);
  $data = [];
  while($row_subject = mysqli_fetch_assoc($res_subject))
  {
    $row_subject['bday'] = date("m/d/Y", strtotime($row_subject['bday']));
    $data[] = $row_subject;
  }
  if(isset($_POST["type"]) && $_POST["type"] == "ajax")
  {
    echo json_encode($data);
  }
  else
  {
    return $data;
  }

}
