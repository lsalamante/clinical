<?php

    if (isset($_GET['action']) && $_GET['action'] != '') {

        require("../jp_library/jp_lib.php");

        if(isset($_POST['data'])){

            parse_str($_POST['data'], $_POST);
            $_POST['data'] = false;

        }


        $_GET['action'](true);
        if (!isset($_SESSION['user_id']))
            session_start();

    }


    function addStageSummary(){


        $params['table'] = "stage_summary";

        $_POST["time_slot_from"] = date("Y-m-d", strtotime($_POST["time_slot_from"]));
        $_POST["time_slot_to"] = date("Y-m-d", strtotime($_POST["time_slot_to"]));

        $_POST["enroll_amount"] = str_replace(",", "", $_POST["enroll_amount"]);
        $_POST["quit_amount"] = str_replace(",", "", $_POST["quit_amount"]);
        $_POST["finish_amount"] = str_replace(",", "", $_POST["finish_amount"]);
        $_POST["light_amount"] = str_replace(",", "", $_POST["light_amount"]);
        $_POST["mid_amount"] = str_replace(",", "", $_POST["mid_amount"]);
        $_POST["hard_amount"] = str_replace(",", "", $_POST["hard_amount"]);
        $_POST["important_amount"] = str_replace(",", "", $_POST["important_amount"]);
        $_POST["amount"] = str_replace(",", "", $_POST["amount"]);

        $_POST["project_id"] = $_POST["project_idS"];

        if(isset($_POST["stage_summary_id"]) && $_POST["stage_summary_id"] != ""){

            $success = "updated";
            $error = "updating";

            $_POST["updated_by"] = $_SESSION["role_id"];
            $_POST["date_update"] = date("Y-m-d");

            $stage_summary_id = $_POST["stage_summary_id"];
            unset($_POST["stage_summary_id"]);

            $params['where'] = "stage_summary_id = ".$stage_summary_id;
            $params['data'] = $_POST;
            $result = jp_update($params);

            /*******************
            * LOGS START HERE! *
            *******************/
            $t = new Clinical\Helpers\Translation($_SESSION['lang']);
            $p = new Clinical\Helpers\Project($_POST['project_id']);
            $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

            # notify DA here so he can double check
            $p->notify_group = array_diff($p->notify_group, array_diff($p->positions, ['DA']));
            Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang'], $t->tryTranslate('da_todo_14_0'), $t->tryTranslate('da_dbl_chk_stage_summary'));
            # / notify DA here so he can double check
            
            $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'modify',
            array(
              $t->tryTranslate('project_name') => "$p->project_name",
              $t->tryTranslate('module') => $t->tryTranslate('stage_summary'),
              $t->tryTranslate('number') => "$p->project_num",
              $t->tryTranslate('submitter') => "$u->fname"
              )
            );
            $l->save();

            /*******************
            * LOGS END HERE!   *
            *******************/

            /*************************
            * DOUBLE CHECK RESET!!   *
            **************************/
            $dc = new Clinical\Helpers\DoubleCheck("stage_summary", "stage_summary_id", $stage_summary_id, $_SESSION['lang']);
            $dc->fetch("stage_summary", "stage_summary_id", $stage_summary_id);
            $dc->resetPayload();
            $dc->updatePayload("stage_summary", "stage_summary_id", $stage_summary_id);
            /*************************
            * / DOUBLE CHECK RESET!! *
            **************************/

        }else{

            $success = "added";
            $error = "adding";

            $_POST["added_by"] = $_SESSION["role_id"];
            $_POST["date_added"] = date("Y-m-d");

            $params['data'] = $_POST;
            //$params['debug'] = 1;
            $result = jp_add($params);
            $stage_summary_id = jp_last_added();

            /*******************
            * LOGS START HERE! *
            *******************/
            $t = new Clinical\Helpers\Translation($_SESSION['lang']);
            $p = new Clinical\Helpers\Project($_POST['project_id']);
            $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

            # notify DA here so he can double check
            $p->notify_group = array_diff($p->notify_group, array_diff($p->positions, ['DA']));
            Clinical\Helpers\NotificationFactory::create($p->notify_group, $_POST['project_id'], $_SESSION['lang'], $t->tryTranslate('da_todo_14_0'), $t->tryTranslate('da_dbl_chk_stage_summary'));
            # / notify DA here so he can double check

            $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'submit_the_record',
            array(
              $t->tryTranslate('project_name') => "$p->project_name",
              $t->tryTranslate('module') => $t->tryTranslate('stage_summary'),
              $t->tryTranslate('number') => "$p->project_num",
              $t->tryTranslate('submitter') => "$u->fname"
              )
            );
            $l->save();

            /*******************
            * LOGS END HERE!   *
            *******************/

        }

        // ----------------- Start of saving materials ----------------- //
        if(isset($_POST['name']))
        {
            if($result && count($_POST["name"]) > 0){

                $i = 1;
                foreach ($_POST["name"] as $key => $value) {


                //for($i = 1; $i <= count($_POST["name"]); $i++) {

                    $materails["stage_summary_id"] = $stage_summary_id;
                    $materails["name"] = $_POST["name"][$key];
                    $materails["version"] = $_POST["version"][$key];
                    //$materails["version_date"] = date("Y-m-d", strtotime($_POST["version_date"][$key]));
                    $materails["description"] = $_POST["description"][$key];
                    $materails["databank"] = $_POST["databank"][$key];

                    $materailsParams['table'] = "summary_materials";
                    $materailsParams['data'] = $materails;

                    //$materailsParams['debug'] = 1;
                    $materialResult = jp_add($materailsParams);
                    $material_id = jp_last_added();

                    // ----------------- Start of saving temp material files ----------------- //
                    if($material_id){

                        $filename = explode(",", $_POST["filename"][$key]);
                        $file = explode(",", $_POST["file"][$key]);

                        for ($count = 0; $count < count($filename); $count++) {

                            $materialFile['material_id'] = $material_id;
                            $materialFile['filename'] = $filename[$count];
                            $materialFile['file'] = $GLOBALS['base_url'] . "stage-summary/" . $file[$count];

                            $materailFilesParams['table'] = "summary_files";
                            $materailFilesParams['data'] = $materialFile;

                            $materialFilesResult = jp_add($materailFilesParams);

                            if($materialFilesResult){

                                copy('../temp-uploads/'.$file[$count], '../stage-summary/'.$file[$count]);
                                unlink('../temp-uploads/'.$file[$count]);

                           }

                        }

                    }
                    // ----------------- End of saving temp material files ----------------- //

                    $i++;
                }

            }

        }
        // ----------------- End of saving materials ----------------- //

        $i = array();
        if( $result ){

            $i['type'] = "success";
            $i['msg'] = "操作成功";

        }else{

            $i['type'] = "error";
            $i['msg'] = "手术失败";

        }

        echo json_encode($i);

    }


    function getStageSummary($type = "", $project_id = ""){

        $params['table'] = 'stage_summary';

        if(isset($_POST["type"]))
            $params['where'] = 'project_id = '.$_POST["project_id"];
        else
            $params['where'] = 'project_id = '.$project_id;


        if(isset($_POST["stage_summary_id"]) && $_POST["stage_summary_id"] != "")
            $params['where'] .= ' AND stage_summary_id = '.$_POST["stage_summary_id"];

        //$params['debug'] = 1;
        $params['filters'] = "ORDER BY stage_summary_id DESC";
        $result = jp_get($params);

        $data = array();
        $i = 0;

        // ------------- Start of get Summary ------------- @Rai//
        while($summaryRow = mysqli_fetch_assoc($result)){

            $summaryRow["enroll_amount"] = $summaryRow["enroll_amount"];
            $summaryRow["quit_amount"] = $summaryRow["quit_amount"];
            $summaryRow["finish_amount"] = $summaryRow["finish_amount"];
            $summaryRow["light_amount"] = $summaryRow["light_amount"];
            $summaryRow["mid_amount"] = $summaryRow["mid_amount"];
            $summaryRow["hard_amount"] = $summaryRow["hard_amount"];
            $summaryRow["important_amount"] = $summaryRow["important_amount"];
            $summaryRow["amount"] = $summaryRow["amount"];
            $summaryRow["time_slot_from"] = date("m-d-Y", strtotime($summaryRow["time_slot_from"]));
            $summaryRow["time_slot_to"] = date("m-d-Y", strtotime($summaryRow["time_slot_to"]));

            $data[$i] = $summaryRow;

            // ------------- Start of get Materials ------------- @Rai//
            if(isset($_POST["stage_summary_id"]) && $_POST["stage_summary_id"] != ""){

                $data[$i]["materials"] = array();


                $materialsParams['table'] = 'summary_materials';
                $materialsParams['where'] = 'stage_summary_id = '.$summaryRow["stage_summary_id"];
                //$materialsParams['debug'] = 1;
                $materialsResult = jp_get($materialsParams);

                while($materialsRow = mysqli_fetch_assoc($materialsResult)){

                    $materialsRow["version_date"] = date("m-d-Y", strtotime($materialsRow["version_date"]));
                    $data[$i]["materials"][$materialsRow["material_id"]] = $materialsRow;

                    // ------------- Start of get Material Files ------------- @Rai//
                    $filesParams['table'] = 'summary_files';
                    $filesParams['where'] = 'material_id = '.$materialsRow["material_id"];
                    //$filesParams['debug'] = 1;
                    $filesResult = jp_get($filesParams);

                    while($filesRow = mysqli_fetch_assoc($filesResult)){

                        $data[$i]["materials"][$filesRow['material_id']]["files"][] = $filesRow;

                    }
                    // ------------- Start of get Material Files ------------- @Rai//

                }

            }
            // ------------- End of get Materials ------------- @Rai//

            $i++;
        }

        // ------------- End of get Summary ------------- @Rai//

        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/

        if(isset($_POST["type"])){
            echo json_encode($data);
        }else
            return $data;


    }

    function uploadMaterial(){

        $bValid = true;
        $bValidUpload = true;
        $return = array();

        if(isset($_FILES['materials']['name'])){

            foreach($_FILES['materials']['name'] as $imgKey => $imgVal ){

                $fileName = preg_replace('#[^a-z.0-9]#i', '', $imgVal);

                $file = explode(".", $fileName);
                $fileExt = end($file);

                if( !in_array($fileExt, array('doc', 'docx', 'pdf')) )
                    $bValid = $bValid && false;

            }

            if($bValid){

                $i = 0;

                foreach($_FILES['materials']['name'] as $imgKey => $imgVal ){

                    if($_FILES['materials']['size'][$i] > 0){

                        $targetDir = "../temp-uploads/";

                        $fileName = preg_replace('#[^a-z.0-9]#i', '', $imgVal);

                        $file = explode(".", $fileName);
                        $fileExt = end($file);

                        $newFileName = $_POST["count"]."--&--".time().rand().".".$fileExt;
                        $newFile = $targetDir.basename($newFileName);
                        $fileSize = getimagesize($_FILES['materials']['tmp_name'][$i]);

                        if( move_uploaded_file($_FILES['materials']['tmp_name'][$i], $newFile) ){

                            $return['type'] = "success";
                            $filename[] = $_FILES['materials']['name'][$i];
                            $files[] = $newFileName;

                        }
                    }

                    $i++;

                }

                if(count($files) > 0){

                    $return['filename'] = implode(",", $filename);
                    $return['files'] = implode(",", $files);

                }
                if(!isset($_POST['databank']))
                {
                    $_POST['databank'] = 0;
                }
                $return['databank'] = $_POST['databank'];
                $return['version_date'] = date('m-d-Y');

            }else{

                $return['type'] = "error";
                $return['msg'] = "您已上传无效的档案。";

            }

            echo json_encode($return);

        }

    }
?>
