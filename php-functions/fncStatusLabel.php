<?php

function fncApproved($status_num)
{
    if($_SESSION['lang'] == 'english')
    {
        $d1 = "Submitted Project";
        $d2 = "Approved By Testing Facility Office";
        $d3 = "Approved By Principal Investigator";
        $d4 = "Project Version 2 uplvoaded By Investigator";
        $d5 = "Applicant Approved version 2 By Investigator";
        $d6 = "Consent Agreement Uploaded By Investigator";
        $d7 = "Submitted to Ethics Committee Secretary for Auditing";
        $d8 = "Submitted to Ethics Committee Members By Ethics Committee Secretary";
        $d9 = "Comments Offered By Ethics Committee Members";
        $d10 = "Conference Content & Consent Uploaded By Ethics Committee Secretary";
        $d11 = "New Version Uploaded By Investigator";
        $d12 = "New Version Approved By Ethics Committee Chairman";
        $d13 = "Investigator or Clinical Research Coordinator adding project members";
        $d14 = "Trial begin";
        $d15 = "Trial finish";
        $d16 = "Inspection";
        $d17 = "Project complete";
        $d18 = "Project stopped";
    }
    else
    {
        $d1 = "提交项目申请";
        $d2 = "试验机构审核通过";
        $d3 = "主要研究者审核通过";
        $d4 = "研究者上传新方案";
        $d5 = "申办者同意新方案";
        $d6 = "研究者上传双方签字盖章方案及其他资料扫描件";
        $d7 = "提交到伦理委员会审核";
        $d8 = "伦理委员会秘书审核通过";
        $d9 = "伦理委员会成员提供建议";
        $d10 = "已上传伦理委员会会议记录和意见";
        $d11 = "研究者上传新方案";
        $d12 = "伦理委员会同意新方案";
        $d13 = "添加项目成员";
        $d14 = "试验启动";
        $d15 = "试验结束";
        $d16 = "现场检查";
        $d17 = "项目完成";
        $d18 = "项目停止";
    }
    $status_approved_desc = array(
    "1" => $d1,
    "2" => $d2,
    "3" => $d3,
    "4" => $d4,
    "5" => $d5,
    "6" => $d6,
    "7" => $d7,
    "7.1" => $d7, //"Modified and Re-Submitted to Ethics Committee Secretary By Applicant",
    "8" => $d8,
    "9" => $d9,
    "10" => $d10,
    "11" => $d11,
    "12" => $d12,
    "13" => $d13,
    "14" => $d14,
    "15" => $d15,
    "16" => $d16,
    "17" => $d17,
    "-1" => $d18
    );

    return $status_approved_desc[$status_num];

}
function fncRejected($rejected_status, $reviewer_id)
{
    if($_SESSION['lang'] == 'english')  
    {
        $rd1 = "Rejected Audit By Testing Facility Office";
        $rd2 = "Rejected Audit By Principal Investigator";
        $rd3 = "Returned to Investigator with comments By Applicant";
        $rd4 = "Returned to Applicant and Investigator for modification by Ethics Committee Secretary";
        $rd5 = "Rejected new version from Investigator by Ethics Committee Chairman";
    }
    else
    {
        $rd1 = "试验机构审核不通过";
        $rd2 = "主要研究者审核不通过";
        $rd3 = "申办者提交修改建议";
        $rd4 = "伦理委员会秘书审核不通过";
        $rd5 = "伦理委员会不同意新方案";
    }
    $return = "";
    //print_r($user);
    if($rejected_status == 0)
    {
      $user = getUserByRoleId(false, $reviewer_id);
      //print_r($user);
      if($user['acronym'] == "PI")
        $return = $rd2;
      else
        $return = $rd1;
    }
    elseif($rejected_status == 3)
    {
      $return = $rd3;
    }
    elseif($rejected_status == 7)
    {
      $return = $rd4;
    }
    elseif($rejected_status == 9)
    {
      $return = $rd5;
    }
    return $return;

}
?>
