<?php

if (isset($_GET['actionS']) && $_GET['actionS'] != '') {

  require("../jp_library/jp_lib.php");

  if(isset($_POST['data'])){

    parse_str($_POST['data'], $_POST);
    $_POST['data'] = false;

  }

  $_GET['actionS'](true);
  if (!isset($_SESSION['user_id']))
  session_start();

}


function getSubjects($project_id, $projects, $random_num = '',$status = '',$birthday = '',$gender = '', $keyword = ''){

  $data = array();

  // if($_SESSION["user_type"] != "I"){ // if not Investigator
  if($_SESSION['is_professonial_group'] == '1'){

    // get subject where chosen within two weeks
    $twoWeekParams['select'] = "subjects.subject_id";
    /*$twoWeekParams['table'] = "subjects LEFT JOIN subject_status ON subject_status.subject_id = subjects.subject_id
    LEFT JOIN volunteers ON volunteers.volunteer_id = subjects.volunteer_id ";*/
    $twoWeekParams['table'] = "subjects LEFT JOIN subject_status ON subject_status.subject_id = subjects.subject_id
    LEFT JOIN volunteers ON volunteers.subject_id = subjects.subject_id ";

    $twoWeekParams['where'] = "subject_status.status_date BETWEEN '".date("Y-m-d H:i:s", strtotime("-3 weeks"))."' AND '".date('Y-m-d H:i:s')."' AND subject_status.status = 'C'
    AND volunteers.status = 1";
    // DATE_SUB(NOW(), INTERVAL 2 WEEK) AND NOW()

    // $twoWeekParams['debug'] = 1;
    $getTwoWeeks = jp_get($twoWeekParams);

    $i = 0;
    while($twoWeekRow = mysqli_fetch_assoc($getTwoWeeks)){

      // get subject information and latest status that were chosen
      $subjectParams['select'] = '*, subject_status.status AS subject_stat';
      $subjectParams['table'] = 'subjects
      LEFT JOIN volunteers ON volunteers.subject_id = subjects.subject_id
      LEFT JOIN projects ON projects.project_id = subjects.project_id
      LEFT JOIN subject_status ON subject_status.status_id = subjects.status_id';

      if($project_id != 0){
        $subjectParams['where'] = "subjects.project_id = ".$project_id." AND subjects.subject_id = ".$twoWeekRow['subject_id']."
        AND (subject_status.status = 'C' OR subject_status.status = 'B' OR subject_status.status = 'FU' OR subject_status.status = 'F' OR subject_status.status = 'NS') ";
      }else{

        $projectFilter = "";
        if(count($projects) > 0){

          $count = 0;
          foreach ($projects as $proj) {

            if($count == 0)
            $projectFilter .= "subjects.project_id = ".$proj;
            else
            $projectFilter .= " OR subjects.project_id = ".$proj;

            $count++;

          }

          $projectFilter = "AND (".$projectFilter.")";

        }

        $subjectParams['where'] = "subjects.subject_id = ".$twoWeekRow['subject_id']." ".$projectFilter;

      }

      //$subjectParams['debug'] = 1;
      if($keyword == '')
      {
        if($gender != '')
          $subjectParams['where'].= ' AND volunteers.gender LIKE "'.$gender.'"';
        
        if($birthday != '' && $birthday != '0000-00-00')
        {
          $birthdate = date('Y-m-d',strtotime($birthday));
          $subjectParams['where'].= " AND volunteers.bday = '".$birthdate."'";
        }

        if($status != '')
          $subjectParams['where'].= ' AND subject_status.status LIKE "'.$status.'"';

        if($random_num != '')
          $subjectParams['where'].= ' AND subjects.random_num = "'.$random_num.'"';
      }
      else
      {
        $subjectParams['where'] .= 'AND
        ( volunteers.gender LIKE "%'.$keyword.'%" 
        OR volunteers.bday LIKE "%'.$keyword.'%" 
        OR subject_status.status LIKE "%'.$keyword.'%" 
        OR subjects.subjects_num LIKE "%'.$keyword.'%"
        OR volunteers.v_name LIKE "%'.$keyword.'%"
        OR subjects.abbrv LIKE "%'.$keyword.'%"
        OR volunteers.id_card_num LIKE "%'.$keyword.'%"
        OR projects.project_name LIKE "%'.$keyword.'%"
        OR projects.project_num LIKE "%'.$keyword.'%"
        OR subjects.random_num LIKE "%'.$keyword.'%"
        OR subject_status.status_date LIKE "%'.$keyword.'%"
        )';
      }
      
      
      $getSubjects = jp_get($subjectParams);
      while($subjectRow = mysqli_fetch_assoc($getSubjects)){

        $data[] = $subjectRow;

      }

    }

  }else{

    $subjectParams['select'] = '*, subject_status.status AS subject_stat';
    $subjectParams['table'] = 'subjects
    LEFT JOIN volunteers ON volunteers.subject_id = subjects.subject_id
    LEFT JOIN projects ON projects.project_id = subjects.project_id
    LEFT JOIN subject_status ON subject_status.status_id = subjects.status_id';

    $subjectParams['where'] = "(subject_status.status = 'NS' OR subject_status.status = 'STP' OR subject_status.status = 'O' OR subject_status.status = 'D') AND volunteers.status = 1";

    if($project_id != 0){
      $subjectParams['where'] .= " AND subjects.project_id = ".$project_id."
      ";
    }else{

      $projectFilter = "";
      if(count($projects) > 0){

        $count = 0;
        foreach ($projects as $proj) {

          if($count == 0)
          $projectFilter .= "subjects.project_id = ".$proj;
          else
          $projectFilter .= " OR subjects.project_id = ".$proj;

          $count++;

        }

        $projectFilter = " AND (".$projectFilter.")";

      }

      $subjectParams['where'] .= $projectFilter;

    }

    if($keyword == '')
    {
      if($gender != '')
        $subjectParams['where'].= ' AND volunteers.gender LIKE "'.$gender.'"';
      
      if($birthday != '' && $birthday != '0000-00-00')
      {
        $birthdate = date('Y-m-d',strtotime($birthday));
        $subjectParams['where'].= " AND volunteers.bday = '".$birthdate."'";
      }

      if($status != '')
        $subjectParams['where'].= ' AND subject_status.status LIKE "'.$status.'"';

      if($random_num != '')
        $subjectParams['where'].= ' AND subjects.random_num = "'.$random_num.'"';
    }
    else
    {
      $subjectParams['where'] .= 'AND
      ( volunteers.gender LIKE "%'.$keyword.'%" 
      OR volunteers.bday LIKE "%'.$keyword.'%" 
      OR subject_status.status LIKE "%'.$keyword.'%" 
      OR subjects.subjects_num LIKE "%'.$keyword.'%"
      OR volunteers.v_name LIKE "%'.$keyword.'%"
      OR subjects.abbrv LIKE "%'.$keyword.'%"
      OR volunteers.id_card_num LIKE "%'.$keyword.'%"
      OR projects.project_name LIKE "%'.$keyword.'%"
      OR projects.project_num LIKE "%'.$keyword.'%"
      OR subjects.random_num LIKE "%'.$keyword.'%"
      OR subject_status.status_date LIKE "%'.$keyword.'%"
      )';
    }

    //$subjectParams['debug'] = 1;
    $getSubjects = jp_get($subjectParams);
    while($subjectRow = mysqli_fetch_assoc($getSubjects)){

      $data[] = $subjectRow;

    }

  }

  return array_reverse($data);

}

function statusChange($subject_id = '', $status = '', $type = ''){

  //echo $subject_id;

  if(isset($_POST['type']) && $_POST['type'] == 'ajax'){

    $_POST["status_date"] = date("Y-m-d");
    $statusParams['data'] = $_POST;

    $subject_id = $_POST["subject_id"];
    $status = $_POST["status"];

  }else if($type != ''){

    $data["status_date"] = date("Y-m-d");
    $data["status"] = $status;
    $data["subject_id"] = $subject_id;
    $statusParams['data'] = $data;

    $subject_id = $subject_id;
    $status = $status;

  }


  $statusParams['table'] = "subject_status";

  $addStatus = jp_add($statusParams);
  $status_id = jp_last_added();

  $i = array();
  if($addStatus){

    // update status_id to subject
    $updateStatData["status_id"] = $status_id;

    $updateStatParam['where'] = "subject_id = ".$subject_id;
    $updateStatParam['data'] = $updateStatData;
    $updateStatParam['table'] = "subjects";
    $updateStatus = jp_update($updateStatParam);

    if($updateStatus){

      // if status is STOP, OUT or DROP update volunteer status to 0 (make available to other project)
      /* if( in_array($status, array('STP', 'O', 'D') ) ){

      // ----------- Start of Get Volunteer ID ----------- //

      $volParam['select'] = "subjects.volunteer_id";
      $volParam['table'] = "subjects";
      $volParam['where'] = "subjects.subject_id = ".$subject_id;

      //$volParam['debug'] = 1;
      $getVol = jp_get($volParam);
      $volRow = mysqli_fetch_assoc($getVol);

      // ----------- End of Get Volunteer ID ----------- //


      // ----------- Start of Update volunteer status to 0 ----------- //

      //updateVolunteerStatus($volRow['volunteer_id']);

      // ----------- End of Update volunteer status to 0 ----------- //

    }*/

    $i['type'] = "success";
    $i['msg'] = "You have successfully updated subject status.";
    if($_SESSION['lang'] == "chinese")
    {
      $i['msg'] = "操作成功";
    }

  }else{

    $i['type'] = "error";
    $i['msg'] = "An error occurred upon updating subject status.";

  }

}else{

  $i['type'] = "error";
  $i['msg'] = "An error occurred upon updating subject status.";

}


if(isset($_POST['type']) && $_POST['type'] == 'ajax')
echo json_encode($i);


}

// general volunteer and subject status update -- called from log-in (fncLogin.php)
function updateStatuses($type = ''){

  // --------- Start of update subject and volunteer status that were (chosen more than 2 weeks and not yet tagged as BEGIN) / (finished after 3 months)   @Rai --------- //
  if($type == "week")
  // DATE(NOW() - INTERVAL 2 WEEK)
  $filter = "subject_status.status_date <= '".date("Y-m-d H:i:s", strtotime("-2 weeks"))."' AND subject_status.status = 'C'";
  else
  // DATE(NOW() - INTERVAL 3 MONTH)
  $filter = "subject_status.status_date <= '".date("Y-m-d H:i:s", strtotime("-3 months"))."' AND subject_status.status = 'F'";

  $lapsedQuery = "
  SELECT subjects.volunteer_id, subject_status.status, subjects.subject_id FROM subject_status

  RIGHT JOIN
  ( SELECT subject_status.subject_id FROM subjects LEFT JOIN subject_status ON subject_status.subject_id = subjects.subject_id
  WHERE ".$filter."
  ) as twoweek ON subject_status.subject_id = twoweek.subject_id

  LEFT JOIN subjects ON subjects.subject_id = twoweek.subject_id
  LEFT JOIN volunteers ON volunteers.volunteer_id = subjects.volunteer_id

  WHERE volunteers.status = 1
  ORDER BY subject_status.status_id DESC
  ";


  $getSubjects = jp_query($lapsedQuery);

  $subjects = array();
  while($subjectRow = mysqli_fetch_assoc($getSubjects)){

    //store latest subject status
    if( !array_key_exists($subjectRow["subject_id"], $subjects) )
    $subjects[$subjectRow["subject_id"]] = $subjectRow['status'];

  }

  // --------- End of update subject and volunteer status that were (chosen more than 2 weeks and not yet tagged as BEGIN) / (finished after 3 months)  @Rai --------- //


  // ----------- Start of Update volunteer status to 0 if status is (not tagged as BEGIN, FOLLOW UP OR FINISH for 2 weeks) / (FINISH after 3 months)  @Rai ----------- //

  foreach ($subjects as $key => $val) {

    if( (!in_array($val, array('B', 'FU', 'F', 'NS')) && $type == "week") OR ( ($val == 'F' || $val != 'NS') && $type == "month") )
    statusChange($key, 'NS', 'function');

  }
  // ----------- End of Update volunteer status to 0 if status is (not tagged as BEGIN, FOLLOW UP OR FINISH for 2 weeks) / (FINISH after 3 months) @Rai ----------- //


  // ----------- Start of Update volunteer status to 0 if status is FINISH after 3 months @Rai ----------- //



  // ----------- End of Update volunteer status to 0 if status is FINISH after 3 months @Rai ----------- //

}

function updateVolunteerStatus(){

  $updateVolData["status"] = 0;
  $updateVolData["subject_id"] = 0;

  $updateVolParam['where'] = "volunteer_id = ".$_POST["volunteer_id"];
  $updateVolParam['data'] = $updateVolData;
  $updateVolParam['table'] = "volunteers";

  //$updateVolParam['debug'] = 1;
  $updateVol = jp_update($updateVolParam);

  $i = array();
  if($updateVol){

    $i['type'] = "success";
    $i['msg'] = "You have successfully updated subject status.";

  }else{

    $i['type'] = "error";
    $i['msg'] = "An error occurred upon updating subject status.";

  }

  echo json_encode($i);

}

function getSubjectLibrary(){



}


function updateSubjectDetails(){

  if($_POST['fr_sbj_lib'] == "" || ! isset($_POST['fr_sbj_lib'])){
    $i['redirect'] = $GLOBALS['base_url'] . "subject-library.php";
  }else{
    $i['redirect'] = $GLOBALS['base_url'] . "pg_audit.php?tab=subjects&project_id=" . $_POST['fr_sbj_lib'];
  }

  $subject_id = $_POST['subject_id'];
  unset($_POST['subject_id']);
  $updateStatParam['where'] = "subject_id = ".$subject_id;
  $_POST["sign_date"] = date("Y-m-d", strtotime($_POST["sign_date"]));
  $_POST["random_date"] = date("Y-m-d", strtotime($_POST["random_date"]));

  $updateStatParam['data'] = $_POST;
  $updateStatParam['table'] = "subjects";
  $updateStatus = jp_update($updateStatParam);
  if($updateStatus){
    $i['type'] = "success";
    $i['msg'] = "You have successfully updated subject status.";
    if($_SESSION['lang'] == "chinese")
    {
      $i['msg'] = "操作成功";
    }
  }else{
    $i['type'] = "error";
    $i['msg'] = "An error occurred upon updating subject status.";
  }

  if(isset($_POST['type']) && $_POST['type'] == 'ajax')
  echo json_encode($i);
}

function getSubjectDetailsById(){
  $params['select'] = "case_num, subjects_num, abbrv, country, address, pi, is_informed_consent, random_num, random_date, sign_date";
  $params['where'] = "subject_id = ". $_POST['subject_id'];
  $params['table'] = "subjects";
  $result = jp_get($params);

  $data =[];

  foreach($result as $key => $val){
    $data[$key] = $val;
  }

  if(isset($_POST['type']) && $_POST['type'] == 'ajax')
  echo json_encode($data);
}

//Desc: Search by type, status, bday and gender
//Developer: Kitcathe
function subjFilters($random_num = '',$status = '',$birthday = '',$gender = '',$project_id)
{  
  
  //$params['select'] = 'v.*, subj.status_id, stat.status as status_letter';
  //$params['table'] = 'volunteers v, subject_status stat, subjects subj';
  //$params['where'] = 'v.volunteer_id = subj.volunteer_id AND subj.status_id = stat.status_id AND v.project_id ='.$project_id;
  $params['select'] = '*, subject_status.status AS subject_stat';
  $params['table'] = 'subjects
    LEFT JOIN volunteers v ON v.subject_id = subjects.subject_id
    LEFT JOIN projects ON projects.project_id = subjects.project_id
    LEFT JOIN subject_status ON subject_status.status_id = subjects.status_id';

  $params['where'] = " subjects.project_id = ".$project_id;

  if($gender != '')
    $params['where'].= ' AND v.gender LIKE "'.$gender.'"';
  
  if($birthday != '' && $birthday != '0000-00-00')
  {
    $birthdate = date('Y-m-d',strtotime($birthday));
    $params['where'].= " AND v.bday = '".$birthdate."'";
  }

  if($status != '')
    $params['where'].= ' AND stat.status LIKE "'.$status.'"';

  if($random_num != '')
    $params['where'].= ' AND subjects.random_num = "'.$random_num.'"';
  
  //$params['debug'] = 1;
  $result = jp_get($params);

  $data = array();
  while($row = mysqli_fetch_assoc($result)){

    $data[] = $row;

  }

  return $data;
}