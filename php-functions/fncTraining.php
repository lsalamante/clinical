<?php

   if (isset($_GET['action']) && $_GET['action'] != '') {
      require("../jp_library/jp_lib.php");

      if(isset($_POST['data']))
      {
        parse_str($_POST['data'], $_POST);
        $_POST['data'] = false;
      }
      $_GET['action'](true);
      if (!isset($_SESSION['user_id'])) {
        session_start();
      }
    }

                                                                        // $projects = all projects assignment - only for list all trainings @Rai
    function getTraining($ajax = false, $project_id = false, $page = "", $projects = array())
    {
        $all_training['table'] = "trainings";
        $all_training['where'] = "is_drafted != 2";
        if($project_id != false)
        {
            $all_training['where'] .= " AND project_id = ".$project_id;
        }

        // ------------------- Start of filter all training per project assignment  @Rai-------------------- //
        if($page == "listAll"){

            $all_training['where'] .= " AND title != ''";

            if(count($projects) > 0){

                $count = 0;
                $projectFilter = "";
                foreach ($projects as $proj) {

                    if($count == 0)
                        $projectFilter .= "project_id = ".$proj;
                    else
                        $projectFilter .= " OR project_id = ".$proj;

                    $count++;

                }

                $projectFilter = "(".$projectFilter.")";

            }

            $all_training['where'] .= " AND ".$projectFilter;

        }
        // ------------------- End of filter all training per project assignment  @Rai-------------------- //

        if(isset($_POST["type"]) && $_POST["type"] != '')
        {
             $all_training['where'] = "training_id = ".$_POST["training_id"];
        }
        $all_training['filters'] = "ORDER BY training_id DESC";
        // $all_training['debug'] = 1;
        $res_training = jp_get($all_training);
        $data = array(); $i = 0;
        while($row_training = mysqli_fetch_assoc($res_training))
        {

            if(isset($_POST["type"]) && $_POST["type"] != ''){

                // -------------- Start of get participants @Rai-------------- //
                    $partsParam['select'] = "role_id";
                    $partsParam['table'] = "trainings_participant";
                    $partsParam['where'] = "training_id = ".$_POST["training_id"];

                    //$partsParam['debug'] = 1;
                    $partsResult = jp_get($partsParam);

                    $participants = array("participants" => "");
                    while($partsRow = mysqli_fetch_assoc($partsResult)){

                        $participants['participants'][] = $partsRow["role_id"];

                    }
                // -------------- End of get participants @Rai-------------- //

                // -------------- Start of get materials @Rai-------------- //
                    $matsParam['table'] = "training_material";
                    $matsParam['where'] = "training_id = ".$_POST["training_id"];

                    // $matsParam['debug'] = 1;
                    $matsParam['filters'] = "ORDER BY material_id DESC";
                    $matsResult = jp_get($matsParam);

                    $materials = array("materials" => "");
                    while($matsRow = mysqli_fetch_assoc($matsResult)){

                        $matsRow['version_date'] = date('m-d-Y', strtotime($matsRow['version_date']));
                        $materials['materials'][] = $matsRow;

                    }


                // -------------- End of get materials @Rai-------------- //

                $data[] = array_merge($row_training, $participants, $materials);

            }else
                $data[$row_training['training_id']] = $row_training;

        }
        if ($ajax == false) {
            return $data;
        } else {
            echo json_encode($data);
        }
    }


    function addTemp($ajax = false)
    {
        $data = 0;
        $add_temp['table'] = "trainings";
        $add_temp['data'] = $_POST;
        if(jp_add($add_temp))
        {
          /*******************
          * LOGS START HERE! *
          *******************/
          $t = new Clinical\Helpers\Translation($_SESSION['lang']);
          $p = new Clinical\Helpers\Project($_POST['project_id']);
          $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

          $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'submit_the_record',
          array(
            $t->tryTranslate('project_name') => "$p->project_name",
            $t->tryTranslate('module') => $t->tryTranslate('training_meeting'),
            $t->tryTranslate('number') => "$p->project_num",
            $t->tryTranslate('submitter') => "$u->fname"
            )
          );
          $l->save();

          /*******************
          * LOGS END HERE!   *
          *******************/
            $data = jp_last_added();
        }
        if ($ajax == false) {
            return $data;
        } else {
            echo $data;
        }
    }

    function getMaterials($ajax = false, $training_id = false)
    {
        $all_material['table'] = "training_material";
        $all_material['where'] = 1;
        if($training_id != false)
        {
            $all_material['where'] = "training_id = ".$training_id;
        }
        $all_material['filters'] = "ORDER BY material_id DESC";
        $res_material = jp_get($all_material);
        $data = array(); $i = 0;
        while($row_material = mysqli_fetch_assoc($res_material))
        {
            $data[$row_material['material_id']] = $row_material;
        }
        if ($ajax == false) {
            return $data;
        } else {
            echo $data;
        }
    }

    function addTraining($ajax = false)
    {
      $t = new Clinical\Helpers\Translation($_SESSION['lang']);
      $p = new Clinical\Helpers\Project($_POST['project_id']);

        if(isset($_POST['participants']))
        {
            if(isset($_POST['training_date']))
            {
                // $_POST['training_date'] = date("Y-m-d H:i:s", strtotime($_POST["training_date"]));
            }
            $p_data = array();
            $p_data['training_id'] = $_POST['training_id'];
            foreach ($_POST['participants'] as $role_id)
            {
                $p_data['role_id'] = "";
                $p_data['role_id'] = $role_id;
                $save_participants['table'] = 'trainings_participant';
                $save_participants['data'] = $p_data;

                if($_POST['is_drafted'] == 0){ # Do not notify if draft
                  $n = new Clinical\Helpers\Notification($role_id, $_POST['project_id']);
                  $n->title = $t->tryTranslate('training_meeting_notif');
                  $n->todo = $t->tryTranslate('training_meeting_datetime') . "..." . $_POST['training_date'];
                  $n->notify();
                }

                jp_add($save_participants);
            }

        }
        $update_training['table'] = 'trainings';
        $update_training['where'] = 'training_id = '.$_POST['training_id'];
        $update_training['data'] = $_POST;
        $data = 0;
        if(jp_update($update_training))
        {
          /*******************
          * LOGS START HERE! *
          *******************/

          $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

          $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'modify',
          array(
            $t->tryTranslate('project_name') => "$p->project_name",
            $t->tryTranslate('module') => $t->tryTranslate('training_meeting'),
            $t->tryTranslate('number') => "$p->project_num",
            $t->tryTranslate('submitter') => "$u->fname"
            )
          );
          $l->save();

          /*******************
          * LOGS END HERE!   *
          *******************/
            $data = 1;
        }
        if ($ajax == false) {
            return $data;
        } else {
            echo $data;
        }
    }
?>
