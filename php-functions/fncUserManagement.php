<?php
/*
Includes: User data management (add, edit, delete, update)
Available for ajax
*/
require("../jp_library/jp_lib.php");
if(!isset($_SESSION['user_id']))
{
	session_start();
}
	if(isset($_GET['action']) && $_GET['action'] != '')
	{
		if(isset($_POST['data']))
		{
			parse_str($_POST['data'], $_POST);
		}
		$_POST['data'] = false;
		$_GET['action'](true);
	}

	function addUser($ajax = false)
	{
		$data = "0";
		
		// required data manipulation before saving to DB
		// if($_POST['pgroup_id'] != '')
		// 	$_POST['pgroup_id'] = implode(",", $_POST['pgroup_id']);
		// else
		// 	$_POST['pgroup_id'] = 0;
		// $_POST['birthdate'] = date("Y-m-d H:i:s", strtotime($_POST['birthdate']));


		$dir_location = "uploads/users/";
	    $traverser = __DIR__ . '/../'; #to go backwards in our file structure

	    $full_path = $traverser . $dir_location;

	    #this crap right here is for creating a folder!!!

	    if (!is_dir($full_path) && !mkdir($full_path, 0777, true)){
	      mkdir($full_path, 0777, true);
	    }
	    
		$_POST['password'] = sha1($_POST['password']);
		if(isset($_FILES['doc_id_card']['tmp_name']))
		{
			$id_card = "id_".time();
			$_POST['doc_id_card'] = jp_upload($_FILES['doc_id_card'],$id_card,"../uploads/users/");
			$_POST['doc_id_card'] = $GLOBALS['base_url'] . "uploads/users/" . $_POST['doc_id_card'];
		}
		if(isset($_FILES['doc_gcp_card']['tmp_name']))
		{
			$gcp_card = "gcp_".time();
			$_POST['doc_gcp_card'] = jp_upload($_FILES['doc_gcp_card'],$gcp_card,"../uploads/users/");
			$_POST['doc_gcp_card'] = $GLOBALS['base_url'] . "uploads/users/" . $_POST['doc_gcp_card'];
		}
		if(isset($_FILES['doc_diploma']['tmp_name']))
		{
			$diploma = "diploma_".time();
			$_POST['doc_diploma'] = jp_upload($_FILES['doc_diploma'],$diploma,"../uploads/users/");
			$_POST['doc_diploma'] = $GLOBALS['base_url'] . "uploads/users/" . $_POST['doc_diploma'];
		}
		if(isset($_FILES['doc_degree']['tmp_name']))
		{
			$degree = "degree_".time();
			$_POST['doc_degree'] = jp_upload($_FILES['doc_degree'],$degree,"../uploads/users/");
			$_POST['doc_degree'] = $GLOBALS['base_url'] . "uploads/users/" . $_POST['doc_degree'];
		}
		if(isset($_FILES['doc_other']['tmp_name']))
		{
			$other = "other_".time();
			$_POST['doc_other'] = jp_upload($_FILES['doc_other'],$other,"../uploads/users/");
			$_POST['doc_other'] = $GLOBALS['base_url'] . "uploads/users/" . $_POST['doc_other'];
		}
		$add_user['table'] = 'users';
		$add_user['data'] = $_POST;
		if(jp_add($add_user))
		{
			if(isset($_POST['position_type']) && $_POST['position_type'] != "")
			{
				# get position id of acronym position type
				$user_id = jp_last_added();
				$get_pos_id['select'] = "position_id, is_professional_group";
				$get_pos_id['table'] = "positions";
				$get_pos_id['where'] = "acronym = '".$_POST['position_type']."'";
				if(strpos($_POST['position_type'], '-') !== false)
				{
					$acronym_sub = explode("-", $_POST['position_type']);
					$get_pos_id['where'] = "acronym = '".current($acronym_sub)."' AND "."acronym_sub = '".end($acronym_sub)."'";
				}
				// $get_pos_id['debug'] = 1;
				$res_pos_id = jp_get($get_pos_id);
				$row_pos_id = mysqli_fetch_assoc($res_pos_id);
				# save role relationship
				$role_data = array();
				$role_data['user_id'] = $user_id;
				$role_data['position_id'] = $row_pos_id['position_id'];
				$role_data['pgroup_id'] = $row_pos_id['is_professional_group'];
				if(isset($_POST['pgroup_id']))
				{
					$role_data['pgroup_id'] = $_POST['pgroup_id'];
				}
				$save_role['table'] = "roles"; 
				$save_role['data'] = $role_data;
				jp_add($save_role);
			}
			$data = jp_last_added();
			if($ajax == false)
			{
				return $data;
			}
			else
			{ 	
				echo $data;
			}
		}
	}

	function addRole($ajax = false)
	{
		$data = "0";
		# get position_id of position type
		$get_pos_id['select'] = "position_id, is_professional_group";
		$get_pos_id['table'] = "positions";
		$get_pos_id['where'] = "acronym = '".$_POST['position_type']."'";
		$res_pos_id = jp_get($get_pos_id);
		$row_pos_id = mysqli_fetch_assoc($res_pos_id);
		# Instantiate user data array and tables
		$user_data = array();
		$user_data['position_id'] = $row_pos_id['position_id'];
		$user_data['pgroup_id'] = 0;
		if(isset($_POST['pgroup_id']))
		{
			$user_data['pgroup_id'] = $_POST['pgroup_id'];
		}
		
		$save_role['table'] = 'roles';
		$data = "1";
		foreach ($_POST['user_id'] as $user_id)
		{
			if($data == "1" && $user_id != 0 && $user_id != null)
			{
				$user_data['user_id'] = "";
				$user_data['user_id'] = $user_id;
				$save_role['data'] = $user_data;
				if(jp_add($save_role))
				{
					$data = "1";
				}
				else
				{
					$data = "0";
				}
	
			}
		}
		if($ajax == false)
		{
			return $data;
		}
		else
		{ 	
			echo $data;
		}

	}

	function addPgroup($ajax = false)
	{
		$data = 0;
		$save_data = array();
		$save_data['pgroup_name'] = $_POST['pgroup_name'];
		$save_data['pgroup_admin_id'] = $_SESSION['user_id'];
		$save_pgroup['table'] = 'professional_group';
		$save_pgroup['data'] = $save_data;
		if(jp_add($save_pgroup))
		{
			$data = 1;
		}

		if($ajax == false)
		{
			return $data;
		}
		else
		{ 	
			echo $data;
		}
	}
	function updateUser($ajax = false)
	{
		$data = "1";
		// required data manipulation before saving to DB
		//$_POST['pgroup_id'] = implode(",", $_POST['pgroup_id']);
		// if($_POST['birthdate'] != '')
		// $_POST['birthdate'] = date("Y-m-d H:i:s", strtotime($_POST['birthdate']));

		
		$dir_location = "uploads/users/";
	    $traverser = __DIR__ . '/../'; #to go backwards in our file structure

	    $full_path = $traverser . $dir_location;

	    #this crap right here is for creating a folder!!!
		
		if($_POST['password'] != '')
			$_POST['password'] = sha1($_POST['password']);
		else
			unset($_POST['password']);

		if(isset($_FILES['doc_id_card']['tmp_name']) && ($_FILES['doc_id_card']['tmp_name'] != ''))
		{
			$id_card = "id_".time();
			$_POST['doc_id_card'] = jp_upload($_FILES['doc_id_card'],$id_card,"../uploads/users/");
			$_POST['doc_id_card'] = $GLOBALS['base_url'] . "uploads/users/" . $_POST['doc_id_card'];
		}
		if(isset($_FILES['doc_gcp_card']['tmp_name']) && ($_FILES['doc_gcp_card']['tmp_name'] != ''))
		{
			$gcp_card = "gcp_".time();
			$_POST['doc_gcp_card'] = jp_upload($_FILES['doc_gcp_card'],$gcp_card,"../uploads/users/");
			$_POST['doc_gcp_card'] = $GLOBALS['base_url'] . "uploads/users/" . $_POST['doc_gcp_card'];
		}
		if(isset($_FILES['doc_diploma']['tmp_name']) && ($_FILES['doc_diploma']['tmp_name'] != ''))
		{
			$diploma = "diploma_".time();
			$_POST['doc_diploma'] = jp_upload($_FILES['doc_diploma'],$diploma,"../uploads/users/");
			$_POST['doc_diploma'] = $GLOBALS['base_url'] . "uploads/users/" . $_POST['doc_diploma'];
		}
		if(isset($_FILES['doc_degree']['tmp_name']) && ($_FILES['doc_degree']['tmp_name'] != ''))
		{
			$degree = "degree_".time();
			$_POST['doc_degree'] = jp_upload($_FILES['doc_degree'],$degree,"../uploads/users/");
			$_POST['doc_degree'] = $GLOBALS['base_url'] . "uploads/users/" . $_POST['doc_degree'];
		}
		if(isset($_FILES['doc_other']['tmp_name']) && ($_FILES['doc_other']['tmp_name'] != ''))
		{
			$other = "other_".time();
			$_POST['doc_other'] = jp_upload($_FILES['doc_other'],$other,"../uploads/users/");
			$_POST['doc_other'] = $GLOBALS['base_url'] . "uploads/users/" . $_POST['doc_other'];
		}

		
		if(isset($_POST['ajax_page']) && $_POST['ajax_page'] == 'm')
		{
			$get_all_roles['select'] = "COUNT(role_id) as role_id";
			$get_all_roles['table'] = "roles";
			$get_all_roles['where'] = "user_id = ".$_POST['user_id'];
			$res_count_roles = jp_get($get_all_roles);
			$count_all_roles = mysqli_fetch_assoc($res_count_roles);
			if($count_all_roles['role_id'] > 0)
			{
				if(!isset($_POST['role_id']))
				{
					$_POST['role_id'] = array("0");
				}
				$delete_roles = array();
				$get_all_roles['select'] = "role_id";
				$get_all_roles['table'] .= " r LEFT JOIN positions p ON r.position_id = p.position_id";
				$get_all_roles['where'] .= " AND p.acronym != 'A'";
				//$get_all_roles['debug'] = 1;
				$res_all_roles = jp_get($get_all_roles);
				while($row_all_roles = mysqli_fetch_assoc($res_all_roles))
				{
					if(!in_array($row_all_roles['role_id'], $_POST['role_id']))
					{
						$delete_roles[] = $row_all_roles['role_id'];
					}
				}
			}
			if(isset($delete_roles) && count($delete_roles) > 0)
			{
				foreach ($delete_roles as $role_id) 
				{
					$delete_role['table'] = 'roles';
					$delete_role['where'] = 'role_id = '.$role_id;
					if(!jp_delete($delete_role))
					{
						$data = "0";
					}
				}
			}
		}
		


		if($data == "1")
		{
			$update_user['table'] = 'users';
			$update_user['data'] = $_POST;
			$update_user['where'] = 'user_id = '.$_POST['user_id'];
			if(jp_update($update_user))
			{
				$data = "1";
			}
		}
		
		if($ajax == false)
		{
			return $data;
		}
		else
		{
			echo $data;
		}
	}

	// initially for admin -> register.php
	function checkUserAvailability($ajax = false)
	{
		$data = array();
		$data['mobile'] = "0";
		$data['email'] = "0";
		if($_POST['mobile_num'] != '')
		{
			$checkUsers['select'] = 'COUNT(user_id) as total';
			$checkUsers['table'] = 'users';
			$checkUsers['where'] = '1';
			if($_POST['user_id'] != 0)
			{
				$checkUsers['where'] .= ' AND user_id != '.$_POST['user_id'];
			}
			$checkUsers['where'] .= ' AND mobile_num = "'.$_POST['mobile_num'].'"';
			$resMobile = jp_get($checkUsers);
			$rowMobile = mysqli_fetch_assoc($resMobile);
			if($rowMobile['total'] > 0)
			{
				$data['mobile'] = "1";
			}	
		}
		if($_POST['email'] != '')
		{
			$checkUsers['select'] = 'COUNT(user_id) as total';
			$checkUsers['table'] = 'users';
			$checkUsers['where'] = '1';
			if($_POST['user_id'] != 0)
			{
				$checkUsers['where'] .= ' AND user_id != '.$_POST['user_id'];
			}
			$checkUsers['where'] .= ' AND email = "'.$_POST['email'].'"';
			$resEmail = jp_get($checkUsers);
			$rowEmail = mysqli_fetch_assoc($resEmail);
			if($rowEmail['total'] > 0)
			{
				$data['email'] = "1";
			}
		}
		if($ajax == false)
		{
			return $data;
		}
		else
		{ 	
			echo json_encode($data);
		}
	}


	function changeStatus($ajax = false)
	{
		$data = '0';
		$set_status['table'] = 'users';
		$set_status['where'] = 'user_id = '.$_POST['user_id'];
		$set_status['data'] = array("is_active" => $_POST['is_active']);
		if(jp_update($set_status))
		{
			$data = '1';
		}
		if($ajax == false)
		{
			return $data;
		}
		else
		{ 	
			echo json_encode($data);
		}
	}

?>