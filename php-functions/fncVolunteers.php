<?php

if (isset($_GET['action']) && $_GET['action'] != '') {
  require("../jp_library/jp_lib.php");
  if(isset($_POST['data'])){
    parse_str($_POST['data'], $_POST);
    $_POST['data'] = false;
  }
  $_GET['action'](true);
  if (!isset($_SESSION['user_id']))
  session_start();
}

function addVolunteer(){


  $params['table'] = "volunteers";

  $_POST["bday"] = date("Y-m-d", strtotime($_POST["bday"]));

  if(isset($_POST["volunteer_id"]) && $_POST["volunteer_id"] != ""){

    $success = ($_SESSION['lang'] == 'chinese') ? "更新" : "updated";
    $error = ($_SESSION['lang'] == 'chinese') ? "更新" : "updating";

    // $_POST["updated_by"] = $_SESSION["role_id"];
    // $_POST["date_update"] = date("Y-m-d");
    //
    // $volunteer_id = $_POST["volunteer_id"];
    // unset($_POST["volunteer_id"]);
    //
    // $params['where'] = "volunteer_id = ".$volunteer_id;
    // $params['data'] = $_POST;
    // $result = jp_update($params);
    ###################################

    if(empty($_FILES['up_file']['name'])){
      $_POST["updated_by"] = $_SESSION["role_id"];
      $_POST["date_update"] = date("Y-m-d");

      $volunteer_id = $_POST["volunteer_id"];
      unset($_POST["volunteer_id"]);

      $params['where'] = "volunteer_id = ".$volunteer_id;

      $params['data'] = $_POST;
      $result = jp_update($params);

    }
    else{
      foreach ($_FILES['up_file']['name'] as $up_fileKey => $up_fileVal )
      {
        $fileExt = explode(".", $up_fileVal);
        $fileName = time() . "." . end($fileExt);

        $dir_location = "uploads/" . 'volunteer_id_cards' . "/";
        $traverser = __DIR__ . '/../'; #to go backwards in our file structure

        $full_path = $traverser . $dir_location;

        #this crap right here is for creating a folder!!!

        if (!is_dir($full_path) && !mkdir($full_path, 0777, true)){
          mkdir($full_path, 0777, true);
        }

        #NOTE: do not delete this scripts. might use this in the future
        #chmod($traverser . "uploads/", 0777);
        #chmod($full_path, 0777);

        if(in_array(end($fileExt), ['jpg', 'txt', 'png', 'xls', 'xlsx', 'jpeg', 'gif', 'doc', 'docx', 'pdf'])){ #these are the allowed files #TODO: add error message reject file type
          if( move_uploaded_file($_FILES['up_file']['tmp_name'][$up_fileKey], $full_path . $fileName) ){

            $_POST['id_card_path'] = $GLOBALS['base_url'] . $dir_location . $fileName;

            $_POST["updated_by"] = $_SESSION["role_id"];
            $_POST["date_update"] = date("Y-m-d");

            $volunteer_id = $_POST["volunteer_id"];
            unset($_POST["volunteer_id"]);

            $params['where'] = "volunteer_id = ".$volunteer_id;

            $params['data'] = $_POST;
            $result = jp_update($params);
            /*******************
            * LOGS START HERE! *
            *******************/
            $t = new Clinical\Helpers\Translation($_SESSION['lang']);
            $p = new Clinical\Helpers\Project($_POST['project_id']);
            $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

            $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'modify',
            array(
              $t->tryTranslate('project_name') => "$p->project_name",
              $t->tryTranslate('module') => $t->tryTranslate('volunteer'),
              $t->tryTranslate('number') => "$p->project_num",
              $t->tryTranslate('submitter') => "$u->fname"
              )
            );
            $l->save();

            /*******************
            * LOGS END HERE!   *
            *******************/
          }
          else{
            $result = 0;
          }
        }else{
          $result = 0;
        }
      }
    }

  }else{

    $success = ($_SESSION['lang'] == 'chinese') ? "添加" : "added";
    $error = ($_SESSION['lang'] == 'chinese') ? "添加" : "adding";
    //
    // $_POST["added_by"] = $_SESSION["role_id"];
    // $_POST["date_added"] = date("Y-m-d");
    //
    // $params['data'] = $_POST;
    // $result = jp_add($params);

    ###################################

    foreach ($_FILES['up_file']['name'] as $up_fileKey => $up_fileVal )
    {
      $fileExt = explode(".", $up_fileVal);
      $fileName = time() . "." . end($fileExt);

      $dir_location = "uploads/" . 'volunteer_id_cards' . "/";
      $traverser = __DIR__ . '/../'; #to go backwards in our file structure

      $full_path = $traverser . $dir_location;

      #this crap right here is for creating a folder!!!

      if (!is_dir($full_path) && !mkdir($full_path, 0777, true)){
        mkdir($full_path, 0777, true);
      }

      #NOTE: do not delete this scripts. might use this in the future
      #chmod($traverser . "uploads/", 0777);
      #chmod($full_path, 0777);

      if(in_array(end($fileExt), ['jpg', 'txt', 'png', 'xls', 'xlsx', 'jpeg', 'gif', 'doc', 'docx', 'pdf'])){ #these are the allowed files #TODO: add error message reject file type
        if( move_uploaded_file($_FILES['up_file']['tmp_name'][$up_fileKey], $full_path . $fileName) ){

          $_POST['id_card_path'] = $GLOBALS['base_url'] . $dir_location . $fileName;

          $_POST["added_by"] = $_SESSION["role_id"];
          $_POST["date_added"] = date("Y-m-d");

          $params['data'] = $_POST;
          $result = jp_add($params);
          /*******************
          * LOGS START HERE! *
          *******************/
          $t = new Clinical\Helpers\Translation($_SESSION['lang']);
          $p = new Clinical\Helpers\Project($_POST['project_id']);
          $u = new Clinical\Helpers\User('role_id', $_SESSION['role_id']);

          $l = new Clinical\Helpers\Log($_SESSION['role_id'], 'submit_the_record',
          array(
            $t->tryTranslate('project_name') => "$p->project_name",
            $t->tryTranslate('module') => $t->tryTranslate('volunteer'),
            $t->tryTranslate('number') => "$p->project_num",
            $t->tryTranslate('submitter') => "$u->fname"
            )
          );
          $l->save();

          /*******************
          * LOGS END HERE!   *
          *******************/
        }
        else{
          $result = 0;
        }
      }else{
        $result = 0;
      }
    }

  }

  $i = array();
  if($result){

    $i['type'] = "success";

    if($_SESSION['lang'] =='chinese'){
      $i['msg'] = "您已成功".$success."了志愿者";
    }else{
      $i['msg'] = "You have successfully ".$success." a Volunteer.";
    }

  }else{

    $i['type'] = "error";

    if($_SESSION['lang'] =='chinese'){
      $i['msg'] = $error ."志愿者时出错";
    }else{
      $i['msg'] = "An error occurred upon ".$error." a volunteer.";
    }


  }

  echo json_encode($i);

}


function chooseToBeSubject(){
  if(!empty($_POST['choose_arr'])){

    $params['table'] = "volunteers";
    $params['where'] = "volunteer_id in (". $_POST['choose_arr'].")";
    $result = (jp_get($params));
    foreach($result as $row){
      $subject_id = $row['subject_id'];
      $volunteer_id = $row['volunteer_id'];
      // TODO: Check if this is needed. TIMEZONE of beta is not asia/pacific -- hotfix
      // $getNow['select'] = "NOW() as now";
      // $getNow['table'] = 'subject_status';
      // $getNow['where'] = "1";
      // $getNow['filters'] = "LIMIT 0,1";
      // $resNow = jp_get($getNow);
      // $rowNow = mysqli_fetch_assoc($resNow);
      $subj_status['table'] = "subject_status";
      $subj_status_arr['subject_id'] = $subject_id;
      $subj_status_arr['status'] = 'C';
      $subj_status_arr['status_date'] = date("Y-m-d H:i:s");
      $subj_status['data'] = $subj_status_arr;
      $subj_status_result = jp_add($subj_status);
      $subj_status_last_added = jp_last_added();

      $update['table'] = 'subjects';
      $update_arr['status_id'] = $subj_status_last_added;
      $update['data'] = $update_arr;
      $update['where'] = "subject_id = '" . $subject_id . "'";
      $update_result = jp_update($update);

      $vol_update['table'] = 'volunteers';
      $vol_update['where'] = "volunteer_id = '" . $volunteer_id . "'";
      $vol_update_arr['subject_id'] = $subject_id;
      $vol_update_arr['status'] = 1; #now unavailable to be subject to other projects
      $vol_update['data'] = $vol_update_arr;
      $vol_update_result = jp_update($vol_update);

    }

    echo 1;
  }else{
    echo 0;
  }

}

// $projects = all projects assignment - only for subject-library @Rai
function getAllVolunteers($project_id = 0, $projects = array(), $is_subjects = false){

  // -------------- Start of WHERE filters for pages -------------- //

  if($project_id != 0 && $project_id != "" && !isset($_POST["volunteer_id"])){ // get all volunteer per project assignment

    $params['select'] = 'v.*, subj.status_id, stat.status as status_letter, stat.*, stat.remarks as stat_remarks';
    $params['where'] = "v.project_id = ".$project_id;

  }else if(isset($projects)){ // For view all volunteers per project assignment @Rai

    $params['select'] = 'volunteer_id, v_name, gender, mobile, status';

    $count = 0;
    $projectFilter = "";
    foreach ($projects as $proj) {

      if($count == 0)
      $projectFilter .= "project_id = ".$proj;
      else
      $projectFilter .= " OR project_id = ".$proj;

      $count++;

    }
    if($projectFilter == "")
    {
      $projectFilter = "false";
    }

    $params['where'] = "(".$projectFilter.") AND status = '1'";

  }

  // -------------- End of WHERE filters for pages -------------- //



  // -------------- Start of WHERE filters for ajax -------------- //

  if(isset($_POST["volunteer_id"]) && $_POST["volunteer_id"] != "") // get volunteer details
  $params['where'] = 'volunteer_id = '.$_POST["volunteer_id"];

  #REVIEW this if we still need this see function `getVolunteerRemarks`
  // if(isset($_POST["volunteer_idR"]) && $_POST["volunteer_idR"] != ""){ // get volunteer remarks
  //   $params['select'] = 'remarks';
  //   $params['where'] = 'volunteer_id = '.$_POST["volunteer_idR"];
  //
  // }

  // -------------- End of WHERE filters for ajax -------------- //

  if($is_subjects && !isset($projects)){
    $params['select'] = '*, stat.remarks as stat_remarks';
    $params['where'] = "v.project_id = '".$project_id . "' AND v.status='1'";
  }

  $params['table'] = 'volunteers as v
  LEFT JOIN subjects as subj ON v.subject_id = subj.subject_id
  LEFT JOIN subject_status as stat on subj.status_id = stat.status_id';
  $params['where'] .= " AND (stat.status != 'NS' OR v.status = 0)";
  // $params['debug'] = 1;
  $params['filters'] = "ORDER BY volunteer_id DESC";
  $result = jp_get($params);

  $data = array();
  while($row = mysqli_fetch_assoc($result)){
    $data[] = $row;

  }

  if(isset($_POST["type"]) && $_POST["type"] == "ajax")
  echo json_encode($data);
  else
  return $data;

}
function getVolunteerRemarks(){
  $params['where'] = 'volunteer_id = '.$_POST["volunteer_id"];
  $params['table'] = 'volunteers';
  // $params['debug'] = 1;
  $result = mysqli_fetch_assoc(jp_get($params));

  if(isset($_POST["type"]) && $_POST["type"] == "ajax")
  echo json_encode($result);
  else
  return $result;

}


function volunteerStatus(){
  $subjects_result = false; #initialize so it doesn't throw an error of not being initialized in case of `else` statement

  if($_POST['status'] == 'S'){
    $success = "enlisted";
    $error = "enlisting";

    $subjects['table'] = "subjects";
    $subjects['data'] = $_POST;
    $subjects_result = jp_add($subjects);

    $subject_id = jp_last_added(); #we add a new row to subjects table
    // TODO: Check if this is needed. TIMEZONE of beta is not asia/pacific -- hotfix
    // $getNow['select'] = "NOW() as now";
    // $getNow['table'] = 'subject_status';
    // $getNow['where'] = "1";
    // $getNow['filters'] = "LIMIT 0,1";
    // $resNow = jp_get($getNow);
    // $rowNow = mysqli_fetch_assoc($resNow);
    $subj_status['table'] = "subject_status";
    $subj_status_arr['subject_id'] = $subject_id;
    $subj_status_arr['status'] = 'S'; #S - for selecting
    $subj_status_arr['status_date'] = date("Y-m-d H:i:s");
    $subj_status['data'] = $subj_status_arr;
    $subj_status_result = jp_add($subj_status); #we add a new row to subject_status table as well and assign that id to subject

    $subj_status_last_added = jp_last_added();
    // UPDATE `subjects` SET `status_id`= '1' WHERE subject_id = 7
    $update['table'] = 'subjects';
    $update_arr['status_id'] = $subj_status_last_added; #we update subjects table with the subject id
    $update['data'] = $update_arr; #any more efficient way to do this? it's hard since both tables rely on ids of each other
    $update['where'] = "subject_id = '" . $subject_id . "'";
    $update_result = jp_update($update);

    // UPDATE `volunteers` SET `subject_id`= '1' WHERE volunteer_id = 6
    $vol_update['table'] = 'volunteers';
    $vol_update['where'] = "volunteer_id = '" . $_POST['volunteer_id'] . "'";
    $vol_update_arr['subject_id'] = $subject_id;
    $vol_update['data'] = $vol_update_arr;
    $vol_update_result = jp_update($vol_update);
  }else{
    #here we can update the record's status without inserting
    if($_POST['status'] == 'P'){
      $success = "passed";
      $error = "passing";
    }else if($_POST['status'] == 'NP'){
      $success = "rejected";
      $error = "rejecting";
    }

    $subject_id = getCurrentSubjectId($_POST['volunteer_id']);
    #refer to previous block for comments. this is just almost identical code
    // echo $subject_id; #for debugging
    // TODO: Check if this is needed. TIMEZONE of beta is not asia/pacific -- hotfix
    // $getNow['select'] = "NOW() as now";
    // $getNow['table'] = 'subject_status';
    // $getNow['where'] = "1";
    // $getNow['filters'] = "LIMIT 0,1";
    // $resNow = jp_get($getNow);
    // $rowNow = mysqli_fetch_assoc($resNow);
    $subj_status['table'] = "subject_status";
    $subj_status_arr['subject_id'] = $subject_id;
    $subj_status_arr['status'] = $_POST['status'];
    $subj_status_arr['status_date'] = date("Y-m-d H:i:s");
    $subj_status['data'] = $subj_status_arr;
    $subj_status_result = jp_add($subj_status);
    $subj_status_last_added = jp_last_added();

    $update['table'] = 'subjects';
    $update_arr['status_id'] = $subj_status_last_added;
    $update['data'] = $update_arr;
    $update['where'] = "subject_id = '" . $subject_id . "'";
    $update_result = jp_update($update);

    $vol_update['table'] = 'volunteers';
    $vol_update['where'] = "volunteer_id = '" . $_POST['volunteer_id'] . "'";
    // if($_POST['status'] == 'NP'){
      // $vol_update_arr['subject_id'] = 0; ## VAN COMMENTED OUT THIS CODE BECAUSE IT SEEMS TO BE PRODUCING A LOT OF ERRORS OMG
    // }else{
      $vol_update_arr['subject_id'] = $subject_id;
    // }
    $vol_update['data'] = $vol_update_arr;
    $vol_update_result = jp_update($vol_update);

  }


  $i = array();
  if(($subjects_result && $subj_status_result && $update_result && $vol_update_result) #for adding
  || ($subj_status_result && $update_result && $vol_update_result)){ #for updating

    $i['type'] = "success";

    switch($success){
      case 'enlisted':
      $i['msg'] = ($_SESSION['lang'] == 'english') ? "You have successfully ".$success." a volunteer." : "操作成功";
      break;
      case 'passed':
      $i['msg'] = ($_SESSION['lang'] == 'english') ? "You have successfully ".$success." a volunteer." : "操作成功";
      break;
      case 'rejected':
      $i['msg'] = ($_SESSION['lang'] == 'english') ? "You have successfully ".$success." a volunteer." : "您已成功拒绝志愿者。";
      break;
    }

  }else{

    $i['type'] = "error";
    switch($error){
      case 'enlisting':
      $i['msg'] = ($_SESSION['lang'] == 'english') ? "An error occurred upon ".$error." a volunteer." : "招募志愿者时发生错误。";
      break;
      case 'passing':
      $i['msg'] = ($_SESSION['lang'] == 'english') ? "An error occurred upon ".$error." a volunteer." : "通过志愿者时出错。";
      break;
      case 'rejecting':
      $i['msg'] = ($_SESSION['lang'] == 'english') ? "An error occurred upon ".$error." a volunteer." : "拒绝志愿者时出错。";
      break;
    }

  }

  echo json_encode($i);


}

function getCurrentSubjectId($volunteer_id) {
  $params['select'] = '*';
  $params['table'] = 'volunteers';
  $params['where'] = "volunteer_id = $volunteer_id";
  $res = jp_get($params);
  $row = mysqli_fetch_assoc($res);
  return $row['subject_id'];
}

function addRemarks(){

  $volunteer_id = $_POST["volunteer_idR"];
  unset($_POST["volunteer_idR"]);
  unset($_POST["project_idR"]);

  $params['table'] = "volunteers";
  $params['data'] = $_POST;
  $params['where'] = "volunteer_id = ".$volunteer_id;
  $result = jp_update($params);

  $i = array();
  if( $result ){

    $i['type'] = "success";
    if($_SESSION['lang'] == "chinese"){
      $i['msg'] = "您已成功提交注释。";
    }else{
      $i['msg'] = "You have successfully submitted remarks.";
    }

  }else{
    $i['type'] = "error";
    if($_SESSION['lang'] == "chinese"){
      $i['msg'] = "提交备注时出错。";
    }else{
      $i['msg'] = "An error occurred upon submitting remarks.";
    }
  }

  echo json_encode($i);

}

function getVolunteerById($ajax = false){

  $params['select'] = '*';
  $params['table'] = 'volunteers';
  $params['where'] = 'volunteer_id = '. $_POST['volunteer_id'];

  $result = mysqli_fetch_assoc(jp_get($params));

  if ($ajax == false) {
    return $result;
  } else {
    echo json_encode($result);
  }

}

//Desc: Search by type, status, bday and gender
//Developer: Kitcathe
function searchFilters($type = '',$status = '',$birthday = '',$gender = '',$project_id)
{  
  
  $params['select'] = 'v.*, subj.status_id, stat.status as status_letter';
  $params['table'] = 'volunteers v, subject_status stat, subjects subj';
  $params['where'] = 'v.volunteer_id = subj.volunteer_id AND subj.status_id = stat.status_id AND v.project_id ='.$project_id;
  if($gender != '')
    $params['where'].= ' AND v.gender LIKE "'.$gender.'"';
  
  if($birthday != '' && $birthday != '0000-00-00')
  {
    $birthdate = date('Y-m-d',strtotime($birthday));
    $params['where'].= " AND v.bday = '".$birthdate."'";
  }

  if($status != '')
    $params['where'].= ' AND stat.status LIKE "'.$status.'"';

  if($type != '')
    $params['where'].= ' AND v.volunteer_type = "'.$type.'"';
  
  $params['where'] .= " AND (stat.status != 'NS' OR v.status = 0)";
  //$params['debug'] = 1;
  $result = jp_get($params);

  $data = array();
  while($row = mysqli_fetch_assoc($result)){

    $data[] = $row;

  }

  return $data;
}