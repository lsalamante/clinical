<?php
include('jp_library/jp_lib.php');
require("php-functions/fncCommon.php");
require("php-functions/fncProjects.php");

if (!isset($_SESSION['user_id'])) {
  header("Location: " . "index.php");
  die();
}

#initial page data
//$user_array = getUserById(0, $_SESSION['user_id']);
$profgroup_array = getProfGroup(0);
$departments = getAllDepartments(0);
$trials = getAllTrials(0);

if(!isset($_GET['project_id'])){
  $_GET['project_id'] = false;
}

$project = getProjectById(0, $_GET['project_id']);

$prof_group_users = getPIByProfGroupId(0, $project['pgroup_id']); #get PI for principal investigator field

$pgroup_admin_id = getProfGroupAdminIdByProfGroupId(0, $project['pgroup_id']);

$pgroup_admin = getUserById(0, $pgroup_admin_id);
$pi = getUserById(0, $project['pi_id']);

?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<?php include('header.php'); ?>
<!--external css-->

<link rel="stylesheet" type="text/css" href="assets/jquery-multi-select/css/multi-select.css" />




<body>
  <section id="container">
    <!--header start-->
    <header class="header white-bg">
      <?php
      if ($LEFT_SIDEBAR) {
        #echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
      }
      ?>
      <!--logo start-->
      <?php if ($LOGO) {
        include('logo.php');
      }
      ?>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <?php if ($NOTIFICATION) {
          include('notification.php');
        } ?>
        <!--  notification end -->
      </div>
      <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
      include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <style>
    span.required
    {
      margin-left: 3px;
      color: red;
    }
    </style>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
          <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
              <li><a href="dashboard.php"><i class="fa fa-home"></i> <?php echo $phrases['project_audit']; ?></a></li>
              <li><?php echo $phrases['project_detail']; ?></li>
            </ul>
            <!--breadcrumbs end -->
          </div>
        </div>
        <?php include('project.php'); ?>
        <section class="panel negative-panel">
          <div class="panel-body">
            <?php include('remarks.php'); ?>
            <?php if($project['status_num'] == 2): ?>
              <div class="row">
                <div class="form-group">
                  <div class="col-sm-4 bigger-word-size">
                    <label class="col-sm-12 control-label"><?php echo $phrases['pi_opinion']; ?><span class="required">*</span></label>
                    <br><br>
                    <form role="form" id="audit_form">
                      <div class="radio">
                        <label>
                          <input type="radio" id="agree_radio" name="status" value="1"> <?php echo $phrases['agree_pi_2']; ?>
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" id="disagree_radio" name="status" value="0"> <?php echo $phrases['disagree_pi_2']; ?>
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-7" id="disagree_remarks" style="display:none;">
                      <label for="comments"><?php echo $phrases['comments_pi_2']; ?></label><span class="required">*</span>
                      <textarea class="form-control v-resize-only" id="comments" name="comments" placeholder="<?php echo $phrases['comments_pi_2']; ?>" ></textarea>
                      <span class="help-block"></span>
                    </div>
                  </div>
                </div>
              <?php endif; ?>
            </div>
          </section>

          <?php if($project['status_num'] == 2): ?>
            <section class="panel negative-panel">
              <div class="panel-body">
                <?php $PRE = getAllPRE(0); ?>
                <a href="#pick_pre" class="btn btn-primary" data-toggle="modal" id="choose_pre" disabled><?php echo $phrases['submit']; ?></a>
                <button type="button" class="btn btn-primary" id="status_submit2" style="display:none;"><?php echo $phrases['submit']; ?></button>
              </div>
            </section>
          <?php endif; ?>
          <!-- page end-->

          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="pick_pre" class="modal fade" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title"><?php echo $phrases['choose_peer_review_expert']; ?></h4>
                </div>
                <div class="modal-body">

                  <div class="form-group last">
                    <label class="control-label col-md-2"></label>
                    <div class="col-md-10">
                    <label style="float: left;">
                    <?php echo $phrases['pre_list']; ?>
                    </label>
                    <label style="float: right;margin-right: 25%;">
                    <?php echo $phrases['choose_pre']; ?>
                    </label>
                      <select name="pre_experts[]" class="multi-select" multiple="" id="my_multi_select3" >
                        <?php

                        foreach ($PRE as $key => $expert) { ?>
                          <option value="<?php echo $expert['role_id']; ?>"><?php echo $expert['fname']." ".$expert['lname']; ?></option>
                          <?php
                        }
                        ?>
                      </select>
                    </div>

                  </div>

                  <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
                  <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
                  <button type="" class="btn btn-primary" id="status_submit"><?php echo $phrases['submit']; ?></button>
                </form>
              </div>
            </div>
          </div>
        </div>

      </section>
    </section>
    <!--main content end-->
    <!-- Right Slidebar start -->
    <?php
    if ($RIGHT_SIDEBAR) {
      include('right-sidebar.php');
    }
    ?>
    <!-- Right Slidebar end -->
    <!--footer start-->
    <?php include('footer.php'); ?>
    <!--footer end-->
  </section>
  <?php include('scripts.php'); ?>

  <script type="text/javascript" src="assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
  <script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script type="text/javascript" src="assets/jquery-multi-select/js/jquery.multi-select.js"></script>
  <script type="text/javascript" src="assets/jquery-multi-select/js/jquery.quicksearch.js"></script>


  <!--summernote-->
  <script src="assets/summernote/dist/summernote.min.js"></script>

  <!--right slidebar-->
  <script src="js/slidebars.min.js"></script>

  <!--common script for all pages-->
  <script src="js/common-scripts.js"></script>
  <!--this page  script only-->
  <script src="js/advanced-form-components.js"></script>
  <script>
  var commentss = "";
  $(document).ready(function () {
    var status_radio = $(".status-radio");
    var agree_radio = $("#agree_radio");
    var disagree_radio = $("#disagree_radio");
    var status_submit = $("#status_submit");
    var status_submit2 = $("#status_submit2");
    var add_documents = $('#add_documents');

    var data = {
      project_id:<?php echo $_GET['project_id'] ?>,
    };
    //                      ###### / INITIALIZE some data used on all pages
    <?php include("initialize.js"); ?>
    var opinion_val = "";
    agree_radio.on('click', function (e) {
      $('#comments').val('');
      $('#disagree_remarks').attr('style', 'display:none');
      $('#choose_pre').removeAttr('disabled');
      $('#choose_pre').attr('style', 'display:inline');
      $('#status_submit2').attr('style', 'display:none');
    });
    disagree_radio.on('click', function (e) {
      $("#disagree_remarks").attr('style', 'display:block; margin-left:250px;');
      $('#choose_pre').attr('disabled', 'disabled');
      $('#status_submit2').attr('style', 'display:block');
      $('#choose_pre').attr('style', 'display:none');
    });
    add_documents.on('click', function (e) {
      $('#add_comments').val("");
      $('#docs_comments_div').attr('style', 'display:none;');
      $('#add_documents:checked').each(function(){
        $('#docs_comments_div').attr('style', 'display:block; margin-left:70px;');
      });
    });


    $('#choose_pre').on('click', function (e) {
      $('#comments').val($('#temp_comments').val());
    });

    status_submit2.on('click', function (e) {
      status_submit2.attr('disabled', 'disabled');
      status_submit2.html("载入中...");
      var form_data = $('#audit_form').serialize();
      $.ajax({
        url:"php-functions/fncProjects.php?action=changeStatus",
        type:"POST",
        data:form_data,
        success:function(msg)
        {
          if(msg != 0)
          {
            show_alert("<?php echo $phrases['operation_successful'] ?>", 'dashboard.php', 1);
            //show_alert(msg, '', 1);
            status_submit.html("<?php echo $phrases['submit']; ?>");

          }
          else
          {
            show_alert("<?php echo $phrases['operation_failed']; ?>", '', 0);
            status_submit2.removeAttr('disabled');
            status_submit2.html("<?php echo $phrases['submit']; ?>");
          }
        }
      });
    });

    <?php include("audit.js"); ?>
    <?php include('disable_project.js') ?>

    /* END DCUMENT RDY*/
  });
  </script>


  </body>
  </html>
