var pp_form = $("#pp_form");
var pp_add = $("#pp_add");
var pp_del = $("#pp_del");
var pp_file = $("#pp_file");
var pp_chk_all = $("#pp_chk_all");

pp_add.on('click', function() {
  pp_form[0].reset();
});

pp_chk_all.change(function(){
  this.checked ? check_all(pp_body) :  uncheck_all(pp_body);
});

pp_form.on('submit', function (e){
  e.preventDefault();
  add_pp_info();
});

pp_del.on('click', function(){
  $("#pp_body input:checkbox:checked").each(function() {
    var _id = $(this).attr('id');
    _id = _id.split('-');
    //                      #push the number only
    del_arr.push(_id[1]);
  });

  var form_data = {
    data: "del_arr=" + del_arr.join(",")
  };

  $.ajax({
    url: "php-functions/fncCommon.php?action=delUpInArray",
    type: "POST",
    data: form_data,
    success: function (msg) {

      if(msg == 1){
        //                                    # Clear our array
        del_arr = [];

        show_alert("<?php echo $phrases['deleted']?>", "", 1);

        //                                  #We reload the table
        ajax_load(data, 'pp_body.php', 'pp_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>.", "", 0);

      }
      pp_chk_all.prop('checked', false);
    }
  });
});

function add_pp_info(){

  var form_data = new FormData(pp_form[0]);
  form_data.append('project_id', "<?php echo $_GET['project_id'] ?>");
  form_data.append('sub_dir', 'project_plan');
  form_data.append('role_id', '<?php echo $_SESSION['role_id'] ?>');
  form_data.append('up_type', '2'); /* up_type 2 for PP */

  $.ajax({
    url: "php-functions/fncCommon.php?action=uploadFile",
    type:'POST',
    processData: false,
    contentType: false,
    data:form_data,
    success:function(msg){
      if(msg == 1){

        if(status_num == 3 || status_num == 10 ){
            $('#status_submit').attr('disabled', false);
        }
        show_alert("<?php echo $phrases['operation_successful']?>", "", 1);


        //                                  #We reload the table
        ajax_load(data, 'pp_body.php', 'pp_body');
      }else{
        show_alert("<?php echo $phrases['operation_failed']?>", "", 0);
      }
    }
  });
}
