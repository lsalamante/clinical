<?php

#initial page data
$user_array = getUserById(0, $_SESSION['user_id']);
$profgroup_array = getProfGroup(0);
$departments = getAllDepartments(0);
$trials = getAllTrials(0);

if(!isset($_GET['project_id'])){
  $_GET['project_id'] = false;
}

$project = getProjectById(0, $_GET['project_id']);

$pgroup_admin_id = getProfGroupAdminIdByProfGroupId(0, $project['pgroup_id']);

$pgroup_admin = getUserById(0, $pgroup_admin_id);
$pi = getUserById(0, $project['pi_id']);

?>

<div class="row <?php echo ($project) ? " " : "hidden "; ?>" id="project_div">
  <div class="col-lg-12">
    <section class="panel negative-panel">
      <form name="project_form" id="project_form">
        <div class="panel-body">
        <style>
          .control-label
          {
            margin-top: 7px;
          }
        </style>
              <div class="form-group">
                <label class="col-sm-2 control-label text-right" style=""><?php echo $phrases['lot_number'] ?></label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="lot_num" name="lot_num"
                  value="<?php echo$project['lot_num'] ?>"
                  >
                  <span class="help-block"></span>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label text-right"><?php echo $phrases['project_number'] ?></label>
                <div class="col-sm-9">
                  <!-- TODO: add number validation -->
                  <input type="text" class="form-control" id="project_num" name="project_num" value="<?php echo$project['project_num'] ?>" maxlength="3">
                  <span class="help-block"></span>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label text-right"><?php echo $phrases['project_name'] ?></label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="project_name" name="project_name" value="<?php echo$project['project_name'] ?>">
                  <span class="help-block"></span>
                </div>
              </div>


              <div class="form-group">
                <label class="col-sm-2 control-label text-right"><?php echo $phrases['trial_category'] ?></label>

                <div class="col-sm-9">
                  <select class="form-control m-bot15" name="trial_id" id="trial_id">
                    <?php foreach ($trials as $trial_id => $text) { ?>
                      <option value="<?php echo $trial_id; ?>"
                        <?php echo($project['trial_id'] == $trial_id) ? "selected='selected'": "" ;?>
                        >
                        <?php echo $text ?>
                      </option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label text-right"><?php echo $phrases['indications'] ?></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="indications" id="indications" value="<?php echo$project['indications'] ?>">
                    <span class="help-block"></span>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label text-right"><?php echo $phrases['risk_category'] ?></label>
                  <!-- #TODO -->
                  <div class="col-sm-9">
                    <select class="form-control m-bot15" id="risk_category" name="risk_category">
                      <option value="0" <?php echo($project['risk_category'] == 0) ? "selected='selected'": "" ;?> ><?php echo $phrases['low'] ?></option>
                      <option value="1" <?php echo($project['risk_category'] == 1) ? "selected='selected'": "" ;?> ><?php echo $phrases['mid'] ?></option>
                      <option value="2" <?php echo($project['risk_category'] == 2) ? "selected='selected'": "" ;?> ><?php echo $phrases['high'] ?></option>
                    </select>
                  </div>
                </div>


                <div class="form-group">
                  <label class="col-sm-2 control-label text-right"><?php echo $phrases['departments'] ?></label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="dept_name" name="dept_name" value="<?php echo $project['dept_name'] ?>">
                    <span class="help-block"></span>
                  </div>
                </div>


                <div class="form-group">
                  <label class="col-sm-2 control-label text-right"><?php echo $phrases['professional_group'] ?></label>

                  <div class="col-sm-9">
                    <select class="form-control m-bot15" name="pgroup_id" id="professional_group">
                      <?php foreach ($profgroup_array as $row) { ?>
                        <option value="<?php echo $row['pgroup_id'] ?>"
                          <?php echo($project['pgroup_id'] == $row['pgroup_id']) ? "selected='selected'": "" ;?>
                          >
                          <?php echo $row['pgroup_name'] ?>
                        </option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label text-right"><?php echo $phrases['principal_investigator'] ?></label>

                    <div class="col-sm-9">
                      <select class="form-control m-bot15" name="pi_id" id="pi_id">

                        <!--                                                                OPTIONS ARE SET BY JQUERY-->
                      </select>
                    </div>
                  </div>

            <div class="form-group">
              <label class="col-sm-2 control-label text-right"><?php echo $phrases['pgroup_contact'] ?></label>

              <div class="col-sm-9">
                <select class="form-control m-bot15" name="pgroup_contact_id" id="pgroup_contact_id">

                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label text-right"><?php echo $phrases['pgroup_mobile'] ?></label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="pgroup_mobile" name="pgroup_mobile" readonly>
                <span class="help-block"></span>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label text-right"><?php echo $phrases['pgroup_email'] ?></label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="pgroup_email" name="pgroup_email" readonly>
                <span class="help-block"></span>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label text-right"><?php echo $phrases['applicant'] ?></label>
              <div class="col-sm-9">
                <?php
                  $proj = new Clinical\Helpers\Project($_GET['project_id']);
                ?>
                <input type="text" class="form-control" readonly name="applicant_name" value="<?= $proj->project_owner ?>">
                <span class="help-block"></span>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label text-right"><?php echo $phrases['emergency_contact'] ?></label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="emergency_contact" value="<?php echo$project['emergency_contact'] ?>">
                <span class="help-block"></span>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label text-right"><?php echo $phrases['emergency_mobile'] ?></label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="emergency_mobile"
                value="<?php echo$project['emergency_mobile'] ?>">
                <span class="help-block"></span>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label text-right"><?php echo $phrases['emergency_email'] ?></label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="emergency_email" value="<?php echo$project['emergency_email'] ?>">
                <span class="help-block"></span>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label text-right"><?php echo $phrases['cro_name'] ?></label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="cro_name" id="cro_name" value="<?php echo$project['cro_name'] ?>">
                <span class="help-block"></span>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label text-right"><?php echo $phrases['cro_contact'] ?></label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="cro_contact" id="cro_contact" value="<?php echo$project['cro_contact'] ?>">
                <span class="help-block"></span>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label text-right"><?php echo $phrases['cro_mobile'] ?></label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="cro_mobile" id="cro_mobile" value="<?php echo$project['cro_mobile'] ?>">
                <span class="help-block"></span>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label text-right">CRO email</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="cro_email" id="cro_email" value="<?php echo$project['cro_email'] ?>">
                <span class="help-block"></span>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label text-right"><?php echo $phrases['research_objectives'] ?></label>
              <div class="col-sm-9">
                <textarea class="form-control v-resize-only" name="research_objectives" ><?php echo$project['research_objectives'] ?></textarea>
                <span class="help-block"></span>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label text-right"><?php echo $phrases['subsidize_type'] ?></label>
              <div class="col-sm-9">

                <div class="radio">
                  <label>
                    <input type="radio" id="fully_funded" value="2" checked="" name="subsidize_type" <?php echo($project['subsidize_type'] == 2) ? "checked='checked'": "" ;?> > <?php echo $phrases['fully_funded'] ?>
                  </label>
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" id="partially_funded" value="1" name="subsidize_type" <?php echo($project['subsidize_type'] == 1) ? "checked='checked'": "" ;?> > <?php echo $phrases['partially_funded'] ?>
                  </label>
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" id="no_subsidize" value="0" name="subsidize_type" <?php echo($project['subsidize_type'] == 0) ? "checked='checked'": "" ;?> > <?php echo $phrases['no_subsidize'] ?>
                  </label>
                </div>

              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label text-right"><?php echo $phrases['remarks'] ?></label>
              <div class="col-sm-9">
                <textarea class="form-control v-resize-only" id="remarks" name="remarks" ><?php echo$project['remarks'] ?></textarea>
                <span class="help-block"></span>
              </div>
            </div>
            <input type="hidden" value="<?php echo $_GET['project_id'] ?>" id="project_id" name="project_id" />

        </div>
    </form>
  </section>
</div>
<div class="col-lg-12 ">
  <section class="panel negative-panel">
    <header class="panel-heading">
      <?php echo $phrases['mc_info'] ?>
      <br>
      <a href="#mc_modal" data-toggle="modal" class="btn btn-xs btn-primary adder-disable" id="mc_add"><?php echo $phrases['add']?></a>
      <button type="" class="btn btn-danger btn-xs adder-disable" id="mc_del"><?php echo $phrases['delete']?></button>
    </header>
    <table class="table table-hover" style="margin-bottom:50px">
      <thead>
        <tr>
          <th>
            <input type="checkbox" id="mc_chk_all" />
          </th>
          <th><?php echo $phrases['number']; ?></th>
          <th><?php echo $phrases['organization'] ?></th>
          <th><?php echo $phrases['person_in_charge'] ?></th>
          <th><?php echo $phrases['lead_participate'] ?></th>
          <th><?php echo $phrases['remarks'] ?></th>
        </tr>
      </thead>
      <tbody id="mc_body">

      </tbody>
    </table>
  </section>
  <section class="panel negative-panel">
    <header class="panel-heading">
      <?php echo $phrases['project_documents'] ?>
      <br>
      <?php if($project['status_num'] == 5 && $_SESSION['user_type'] == 'I'): ?>
        <h6 class="upload-notice">
          <?php echo $phrases['upload_consent_agreement']; ?>
          <i class="fa fa-exclamation-triangle"></i>
        </h6>
        <br>
      <?php elseif($project['status_num'] == 9 && $_SESSION['user_type'] == 'ECS'): ?>
        <h6 class="upload-notice">
          <?php echo $phrases['upload_content_consent']; ?>
          <i class="fa fa-exclamation-triangle"></i>
        </h6>
        <br>
      <?php elseif($project['status_num'] == 12 && $_SESSION['user_type'] == 'ECS'): ?>
        <h6 class="upload-notice">
          <?php echo $phrases['upload_approval']; ?>
          <i class="fa fa-exclamation-triangle"></i>
        </h6>
        <br>
      <?php elseif($project['status_num'] == 14 && $_SESSION['user_type'] == 'I'): ?>
        <h6 class="upload-notice">
          <?php echo $phrases['upload_report_material2']; ?>
          <i class="fa fa-exclamation-triangle"></i>
        </h6>
        <br>
      <?php elseif((in_array($project['status_num'], [14,15]) && $_SESSION['user_type'] == 'A') || $_SESSION['user_type'] == 'I' && $project['status_num'] == 14 ): ?>
        <h6 class="upload-notice">
          <?php echo $phrases['upload_report_material']; ?>
          <i class="fa fa-exclamation-triangle"></i>
        </h6>
        <br>
      <?php endif; ?>
      <a href="#pd_modal" data-toggle="modal" class="btn btn-xs btn-primary adder-disable" id="pd_add"><?php echo $phrases['add']?></a>
      <button type="submit" class="btn btn-danger btn-xs adder-disable" id="pd_del"><?php echo $phrases['delete']?></button>
    </header>
    <table class="table table-hover" style="margin-bottom:50px">
      <thead>
        <tr>
          <th>
            <input type="checkbox" id="pd_chk_all" />
          </th>
          <th><?php echo $phrases['number']; ?></th>
          <th><?php echo $phrases['name'] ?></th>
          <th><?php echo $phrases['version'] ?></th>
          <th><?php echo $phrases['version_date'] ?></th>
          <th><?php echo $phrases['description'] ?></th>
          <th><?php echo $phrases['operating'] ?></th>
        </tr>
      </thead>
      <tbody id="pd_body">

      </tbody>
    </table>
  </section>
  <section class="panel negative-panel">
    <header class="panel-heading">
      <?php echo $phrases['project_plan'] ?>
      <br>
      <?php if($project['status_num'] == 3 && $project['is_rejected'] && $_SESSION['user_type'] == 'I'): ?>
        <h6 class="upload-notice">
          <?php echo $phrases['upload_newer_version']; ?>
          <i class="fa fa-exclamation-triangle"></i>
        </h6>
        <br>
      <?php elseif ($project['status_num'] == 3 && $_SESSION['user_type'] == 'I'): ?>
        <h6 class="upload-notice">
          <?php echo $phrases['upload_newer_version']; ?>
          <i class="fa fa-exclamation-triangle"></i>
        </h6>
        <br>
      <?php elseif ($project['status_num'] == 10 && $_SESSION['user_type'] == 'I'):  ?>
        <h6 class="upload-notice">
          <?php echo $phrases['upload_newer_version']; ?>
          <i class="fa fa-exclamation-triangle"></i>
        </h6>
        <br>
      <?php endif; ?>
      <a href="#pp_modal" data-toggle="modal" class="btn btn-xs btn-primary adder-disable" id="pp_add"><?php echo $phrases['add']?></a>
      <button type="submit" class="btn btn-danger btn-xs adder-disable" id="pp_del"><?php echo $phrases['delete']?></button>
    </header>
    <table class="table table-hover" style="margin-bottom:50px">
      <thead>
        <tr>
          <th>
            <input type="checkbox" id="pp_chk_all"/>
          </th>
          <th><?php echo $phrases['number']; ?></th>
          <th><?php echo $phrases['name'] ?></th>
          <th><?php echo $phrases['version'] ?></th>
          <th><?php echo $phrases['version_date'] ?></th>
          <th><?php echo $phrases['description'] ?></th>
          <th><?php echo $phrases['operating'] ?></th>
        </tr>
      </thead>
      <tbody id="pp_body">

      </tbody>
    </table>
  </section>

  <section class="panel">
    <header class="panel-heading">
      <?php echo $phrases['pre_opinion']; ?>
      <?php if (in_array($project['status_num'], [3,4]) && $_SESSION['user_type'] == 'PRE'): ?>
        <br>
        <a href="#cmt_modal" data-toggle="modal" class="btn btn-xs btn-primary" id="cmt_add"/><?php echo $phrases['add']?></a>
        <button type="" class="btn btn-danger btn-xs adder-disable" id="cmt_del" ><?php echo $phrases['delete']?></button>
        <a href="<?php echo $GLOBALS['base_url'] . 'uploads/templates/研究者发起的项目审议表.docx'?>" class="btn btn-xs btn-primary" id="cmt_pdf" ><?php echo $phrases['download_template']?></a>
      <?php endif; ?>
    </header>
    <table class="table table-hover" style="margin-bottom:50px">
      <thead>
        <tr>
          <th>
            <input type="checkbox" id="cmt_chk_all"/>
          </th>
          <th><?php echo $phrases['number']; ?></th>
          <th><?php echo $phrases['name'] ?></th>
          <th><?php echo $phrases['version'] ?></th>
          <th><?php echo $phrases['version_date'] ?></th>
          <th><?php echo $phrases['description'] ?></th>
          <th><?php echo $phrases['operating'] ?></th>
        </tr>
      </thead>
      <tbody id="cmt_body">

      </tbody>
    </table>
  </section>

  <section class="panel">
    <header class="panel-heading">
      <?php echo $t->tryTranslate('ecm_opinion'); ?>
      <?php if (in_array($project['status_num'], [8]) && $_SESSION['user_type'] == 'ECM'): ?>
        <br>
        <a href="#ecm_modal" data-toggle="modal" class="btn btn-xs btn-primary" id="ecm_add"/><?php echo $phrases['add']?></a>
        <button type="" class="btn btn-danger btn-xs adder-disable" id="ecm_del" ><?php echo $phrases['delete']?></button>
        <a href="<?php echo $GLOBALS['base_url'] . 'uploads/templates/伦理委员会审议表.docx'?>" class="btn btn-xs btn-primary" id="ecm_pdf" ><?php echo $phrases['download_template']?></a>
      <?php endif; ?>
    </header>
    <table class="table table-hover" style="margin-bottom:50px">
      <thead>
        <tr>
          <th>
            <input type="checkbox" id="ecm_chk_all"/>
          </th>
          <th><?php echo $phrases['number']; ?></th>
          <th><?php echo $phrases['name'] ?></th>
          <th><?php echo $phrases['version'] ?></th>
          <th><?php echo $phrases['version_date'] ?></th>
          <th><?php echo $phrases['description'] ?></th>
          <th><?php echo $phrases['operating'] ?></th>
        </tr>
      </thead>
      <tbody id="ecm_body">

      </tbody>
    </table>
  </section>


</div>
</div>

<div class="row <?php echo (!$project) ? " " : "hidden "; ?>">
  <div class="col-lg-12">
    <section class="panel negative-panel">
      <div class="row">
        <div class="col-lg-12 text-center">
          <img src="img/sad.png" class="sad">
          <h1 class="sad">Sorry! That project does not exist.</h1>
        </div>
      </div>
    </section>
  </div>
</div>


<!--        ###################### MODALS ##############-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="mc_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $phrases['mc_info'] ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="mc_form">
          <div class="form-group">
            <label><?php echo $phrases['organization'] ?></label>
            <input type="text" class="form-control" id="mc_org" name="mc_org" placeholder="" required>
          </div>
          <div class="form-group">
            <label><?php echo $phrases['person_in_charge'] ?></label>
            <input type="text" class="form-control" id="mc_person" name="mc_person" placeholder="" required>
          </div>

          <div class="form-group">
            <label><?php echo $phrases['lead_participate'] ?></label>
            <select class="form-control m-bot15" name="mc_lead" id="mc_lead" >
              <option value="" disabled="disabled"><?php echo $phrases['choose'].$phrases['lead']."/".$phrases['participate']; ?></option>
              <option value="<?php echo $phrases['lead']; ?>"><?php echo $phrases['lead']; ?></option>
              <option value="<?php echo $phrases['participate']; ?>"><?php echo $phrases['participate']; ?></option>
            </select>

          </div>
          <div class="form-group">
            <label><?php echo $phrases['remarks'] ?></label>
            <textarea class="form-control v-resize-only" id="mc_remarks" name="mc_remarks" placeholder="" required></textarea>
          </div>
          <button type="" class="btn btn-primary make-loading" id="mc_submit"><?php echo $phrases['submit'] ?></button>
        </form>
      </div>
    </div>
  </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="pd_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $phrases['project_documents'] ?></h4>
      </div>
      <div class="modal-body">

        <form role="form" id="pd_form" enctype="multipart/form-data">
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo $phrases['name'] ?></label>
            <input type="text" class="form-control" id="pd_name" name="up_name" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['version'] ?></label>
            <input type="number" min="0" step="0.1" class="form-control" id="pd_ver" name="up_ver" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['description'] ?></label>
            <textarea class="form-control v-resize-only" id="pd_desc" name="up_desc" placeholder=""></textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputFile"><?php echo $phrases['select_file'] ?></label>
            <input type="file" id="pd_file" name="up_file[]" multiple="" required>
            <!-- <p class="help-block">Example block-level help text here.</p> -->
          </div>
          <!-- databank -->
          <div class="form-group">
            <input type="checkbox" name="databank" value="1" id="pd_databank">
            <label for="pd_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
          </div>
          <button type="" class="btn btn-primary make-loading" id="pd_submit"><?php echo $phrases['submit'] ?></button>
        </form>
      </div>
    </div>
  </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="pp_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $phrases['project_plan'] ?></h4>
      </div>
      <div class="modal-body">

        <form role="form" id="pp_form" enctype="multipart/form-data">
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo $phrases['name'] ?></label>
            <input type="text" class="form-control" id="pp_name" name="up_name" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['version'] ?></label>
            <input type="number" min="0" step="0.1" class="form-control" id="pp_ver" name="up_ver" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['description'] ?></label>
            <textarea class="form-control v-resize-only" id="pp_desc" name="up_desc" placeholder=""></textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputFile"><?php echo $phrases['select_file'] ?></label>
            <input type="file" id="pp_file" name="up_file[]" multiple="" required>
          </div>
          <!-- databank -->
          <div class="form-group">
            <input type="checkbox" name="databank" value="1" id="pp_databank">
            <label for="pp_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
          </div>
          <button type="" class="btn btn-primary make-loading" id="pp_submit"><?php echo $phrases['submit'] ?></button>
        </form>
      </div>
    </div>
  </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="cmt_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $phrases['pre_opinion'] ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="cmt_form" enctype="multipart/form-data">
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo $phrases['name'] ?></label>
            <input type="text" class="form-control" id="cmt_name" name="up_name" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['version'] ?></label>
            <input type="number" min="0" step="0.1" class="form-control" id="cmt_ver" name="up_ver" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['description'] ?></label>
            <textarea class="form-control v-resize-only" id="cmt_desc" name="up_desc" placeholder="" ></textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputFile"><?php echo $phrases['select_file'] ?></label>
            <input type="file" id="cmt_file" name="up_file[]" multiple="" required>
            <!-- <p class="help-block">Example block-level help text here.</p> -->
          </div>
          <!-- databank -->
          <div class="form-group">
            <input type="checkbox" name="databank" value="1" id="cmt_databank">
            <label for="cmt_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
          </div>
          <button type="" class="btn btn-primary" id="cmt_submit"><?php echo $phrases['submit'] ?></button>
        </form>
      </div>
    </div>
  </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="ecm_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $t->tryTranslate('ecm_opinion') ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="ecm_form" enctype="multipart/form-data">
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo $phrases['name'] ?></label>
            <input type="text" class="form-control" id="ecm_name" name="up_name" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['version'] ?></label>
            <input type="number" min="0" step="0.1" class="form-control" id="ecm_ver" name="up_ver" placeholder="">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1"><?php echo $phrases['description'] ?></label>
            <textarea class="form-control v-resize-only" id="ecm_desc" name="up_desc" placeholder="" ></textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputFile"><?php echo $phrases['select_file'] ?></label>
            <input type="file" id="ecm_file" name="up_file[]" multiple="" required>
            <!-- <p class="help-block">Example block-level help text here.</p> -->
          </div>
          <!-- databank -->
          <div class="form-group">
            <input type="checkbox" name="databank" value="1" id="ecm_databank">
            <label for="ecm_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
          </div>
          <button type="" class="btn btn-primary" id="ecm_submit"><?php echo $phrases['submit'] ?></button>
        </form>
      </div>
    </div>
  </div>
</div>
<!--        ###################### / MODALS ##############-->
