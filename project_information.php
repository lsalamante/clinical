<?php

include('jp_library/jp_lib.php');
require("php-functions/fncApplicant.php");
require("php-functions/fncCommon.php");
require("php-functions/fncProjects.php");
require("php-functions/fncStatusLabel.php"); # must always be below fncCommon.php and fncProjects.php

if (!isset($_SESSION['user_id'])) {
  header("Location: " . "index.php");
  die();
}

$projects = getProjectInformation(0);


$get_trial_id = "trial_id=";
$_trial_id = 1;
$get_status_id = 'status_num=';
$_status_id = 1;

?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>

<body class="bggray">
  <section id="container">
    <!--header start-->
    <header class="header white-bg">
      <?php
      if ($LEFT_SIDEBAR) {
        //echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
      }
      ?>
      <!--logo start-->
      <?php if ($LOGO) {
        include('logo.php');
      }
      ?>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <?php if ($NOTIFICATION) {
          include('notification.php');
        } ?>
        <!--  notification end -->
      </div>
      <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
      include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->
        <!-- bread crumbs start -->
        <!--<div class="row">
        <div class="col-lg-12">

        <ul class="breadcrumb">
        <li><i class="fa fa-home"></i> My projects</li>
      </ul>

    </div>
  </div> -->
  <!-- breadcrumbs end -->
  <div class="row">
    <div class="col-lg-12 floating-btn">
      <div class="col-sm-3 input-group fright">
        <input type="text" id="_squery" class="form-control" placeholder="<?php echo $phrases['search'] ?>" value="<?php echo (isset($_GET['squery'])) ? $_GET['squery'] : '' ;?>">
        <span class="input-group-btn">
          <a id="_search" href="project_information.php?
          <?php
          echo (isset($_GET['squery'])) ? $_GET['squery'] . "&": '' ;
          echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ;
          echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ;
          ?>



          ">  <button class="btn btn-white" type="button"><i class="fa fa-search"></i></button> </a>
        </span>
        <!--<div class="row">
        <div class="col-lg-12">
        <div class="form-group" style="margin-top:15px">
        <label class="col-sm-2 control-label">Search</label>

      </div>
    </div>
  </div>-->
</div>
</div>

<div class="col-lg-12">
  <section class="panel">
    <!--<header class="panel-heading">
    My projects
  </header>-->
  <div class="row">
    <div class="col-lg-12">
      <div class="form-group project-filters-row">
        <div class="col-sm-1" style="width:7.6%; padding: 0px 0px;">
          <label class="control-label lightblue" style="width:100%"><?php echo $phrases['research_category'] ?></label>
        </div>
        <div class="col-sm-11" style="width:88%">
          <div class="btn-row">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary <?php echo (isset($_GET['trial_id'])) ? '' : 'btn-active' ; ?>" onclick="window.location.href='project_information.php?<?php echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox" > <?php echo $phrases['all'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 1) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox" > <?php echo $phrases['stage_1'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 2) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['stage_2'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 3) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['stage_3'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 4) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['stage_4'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 5) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['be_trial'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 6) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['clinical_verifications'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 7) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['medical_equipment'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 8) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['food'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['trial_id']) && $_GET['trial_id'] == 9) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_trial_id . $_trial_id++ . "&"; echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['other'] ?>
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="form-group project-filters-row">
        <div class="col-sm-1" style="width:7.6%; padding: 0px 0px;">
          <label class="control-label lightblue" style="width:100%"><?php echo $phrases['state']; ?></label>
        </div>
        <div class="col-sm-11" style="width:88%">
          <div class="btn-row">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary <?php echo (isset($_GET['status_num'])) ? '' : 'btn-active' ; ?>" onclick="window.location.href='project_information.php?<?php echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['all'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 1) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_status_id . 1 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['apply'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 2) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_status_id . 2 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['initiation'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 10) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_status_id . 10 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['ethics_pass'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 14) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_status_id . 14 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['trial_begin'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 14) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_status_id . 14 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['process'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == -1) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_status_id . -1 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['stop'] ?> <!-- TODO: make stopped project disappear!!!!-->
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 15) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_status_id . 15 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['trial_finish'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 16) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_status_id . 16 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['inspection'] ?>
              </label>
              <label class="btn btn-primary <?php if (isset($_GET['status_num']) && $_GET['status_num'] == 17) echo 'btn-active'?>" onclick="window.location.href='project_information.php?<?php echo $get_status_id . 17 . "&"; echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; echo (isset($_GET['squery'])) ? "squery=" . $_GET['squery'] : "" ;  ?>'">
                <input type="checkbox"> <?php echo $phrases['project_complete'] ?>
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row" class="bordtopbot">
    <div class="col-lg-12 tblpad">
      <table class="table table-hover <?php echo (count($projects) > 0) ? " " : "hidden "; ?>">
        <thead>
          <tr>
            <th><?php echo $phrases['number']?></th>
            <th><?php echo $phrases['project_name']?></th>
            <th><?php echo $phrases['date_created']?></th>
            <th><?php echo $phrases['applicant']?></th>
            <th><?php echo $phrases['status']?></th>
            <th><?php echo $phrases['operation']?></th>
          </tr>
        </thead>
        <tbody>

          <?php foreach($projects as $project){
            if($project['status_num'] == 0) continue; #if status_num is draft
            ?>
            <?php if($project['status_num'] >= 13 && $_SESSION['is_professonial_group']) {?>
              <!-- this is the page where multiple users such as the professional group should go to sa tamang panahon-->
              <tr class="clickable-row" onclick="ClickableRow('pg_audit.php?project_id=<?=$project['project_id']?>', event);" >
                <?php }else{ ?>
                  <tr class="clickable-row" onclick="ClickableRow('pg_audit.php?project_id=<?=$project['project_id']?>', event);" >
                    <?php } ?>
                    <td>
                      <?php echo sprintf("%04d", $project['project_id']); ?>
                    </td>
                    <td>
                      <?php echo $project['project_name']; ?>
                    </td>
                    <td>
                      <?php echo $project['date_created']; ?>
                    </td>
                    <td>
                      <?php echo $project['fname'] . " " . $project['lname']; ?>
                    </td>
                    <td>
                      <b><?php echo fncApproved($project['status_num']); ?></b>
                    </td>
                    <td>
                      <!--<button class="btn btn-danger del-btn" title="details" id="del-<?=$project['project_id']?>"><i class="fa fa-trash-o"></i></button>-->
                      <button class="btn btn-success timeline-btn" title="Timeline" id="timeline-<?php echo $project['project_id']; ?>"><?php echo $phrases['progress_icon']; ?></button>
                    </td>
                  </tr>

                  <?php } ?>

                </tbody>
              </table>
            </div>
          </div>

          <div class="row <?php echo (count($projects) <= 0) ? " " : "hidden "; ?>">
            <div class="col-lg-12">
              <section class="panel">
                <div class="row">

                  <div class="col-lg-12 text-center">
                    <img src="img/sad.png" class="sad">
                    <h1 class="sad">It looks lonely here...<br>Let's wait for new projects, shall we?</h1>
                  </div>
                </div>
              </section>
            </div>
          </div>
          <!--timeline start-->

          <div class="row" id="timeline_row" style="display:none">
          </div>
          <!--timeline end-->
        </section>
      </div>



    </section>
  </div>

</div>

</div>

<!-- page end-->
</section>
</section>
<div id="focus_me"></div>
<!--main content end-->
<!-- Right Slidebar start -->
<?php
if ($RIGHT_SIDEBAR) {
  include('right-sidebar.php');
}
?>
<!-- Right Slidebar end -->
<!--footer start-->
<?php include('footer.php'); ?>
<!--footer end-->
</section>
<?php include('scripts.php'); ?>


<script>
function ClickableRow(strlink,event)
{
  if(event.target.nodeName== 'BUTTON')
  return;

  location.assign(strlink);
}
$(document).ready(function () {

  var new_project = $("#new_project");
  var del_btn = $(".del-btn");
  var timeline_btn = $(".timeline-btn");
  var _trial_id = "<?php  echo (isset($_GET['trial_id'])) ? $get_trial_id . $_GET['trial_id'] . "&" : "" ; ?>";
  var _status_num = "<?php echo (isset($_GET['status_num'])) ? $get_status_id . $_GET['status_num'] . "&" : "" ; ?>";
  var _squery = $("#_squery");
  var _search = $("#_search");
  /* underscore prefix for non-clashing variables */

  _search.on('click', function(e){
    e.preventDefault();
    if(_squery.val() == ''){
      _squery = '';
    }else{
      _squery = "squery=" + _squery.val() + "&";
    }

    window.location.href="project_information.php?" + _squery + _status_num + _trial_id;
  });

  del_btn.on('click', function (e) {
    var del_id = this.id;
    var del_arr = del_id.split("-");

    var form_data = {
      project_id: del_arr[1]
    };

    $.ajax({
      url: "php-functions/fncApplicant.php?action=deleteProjectById",
      type: "POST",
      data: form_data,
      success: function (msg) {

        show_alert("Project deleted", "project_information.php", 1);

      }
    });
  });
  var last_timeline = "";
  timeline_btn.on('click', function (e){
    var timeline_id = this.id;
    if(last_timeline == timeline_id)
    {
      $('#timeline_row').fadeOut(1000);
      last_timeline = "none";
    }
    else
    {
      $('#timeline_row').fadeOut(100);
      var timeline_arr = timeline_id.split("-");
      $('#'+timeline_id).attr("data-active", "1");

      var form_data = {
        project_id: timeline_arr[1]
      };
      ajax_load(form_data, "timeline_load.php", "timeline_row"); // PS. ajax_load(form_data, file_url, element_id) in footer.php
      last_timeline = timeline_id;
    }
  });


  new_project.on('click', function (e) {
    e.preventDefault();

    var form_data = {
      user_id: <?= $_SESSION["user_id"]; ?>
    };

    $.ajax({
      url: "php-functions/fncCommon.php?action=addNewProject",
      type: "POST",
      data: form_data,
      success: function (msg) {

        window.location.replace("new_project.php?project_id=" + msg);

      }
    });

  });

});
</script>
</body>

</html>
