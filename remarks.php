<?php $remarks = getAllRemarks(false, $project['project_id']); # php-functions/fncProjects.php
    if(count($remarks) > 0):
        foreach ($remarks as $position_name => $remarks_arr) {  ?>
    <div class="row">
        <div class="form-group">
            <div class="row bigger-word-size">
                <label class="col-sm-1 control-label" style="margin-left: 35px;"><?php echo $phrases[$remarks_arr[0]['lang_phrase']]; ?></label>
                <div class="row ">
                    <div class="col-sm-7">
                        <div class="panel-group m-bot20" id="accordion">
                            <?php foreach($remarks_arr as $remark_id => $remark){ //print_r($remark);?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                <h4 class="panel-title" style="color:#6b6a6a">
                                    <a class="accordion-toggle bigger-word-size" data-toggle="collapse" data-parent="#accordion" href="#remark_<?php echo $remark['acronym']; ?>_<?php echo $remark_id; ?>">
                                    <?php   echo $remark['remark_type'] == 0 ? $phrases['rejected_remarks'] : ($remark['remark_type'] == 1 ? $phrases['add_document_remarks'] : $phrases['ethics_committee_comments']);
                                            echo " : ".date("m/d/Y H:i:s", strtotime($remark['status_date'])); ?>
                                    </a>
                                </h4>
                                </div>
                                <div id="remark_<?php echo $remark['acronym']; ?>_<?php echo $remark_id; ?>" class="panel-collapse collapse <?php echo $remark_id == 0 ? "in" : ""; ?>">
                                    <div class="panel-body">
                                        <i><?php echo $remark['remarks']; ?></i>
                                    </div>
                                </div>
                            </div>
                            <?php } # end foreach of remarks ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }
    endif; ?>
