<!-- js placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<!-- <script src="js/jquery.nicescroll.js" type="text/javascript"></script> -->
<script src="js/jquery.sparkline.js" type="text/javascript"></script>
<script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="js/owl.carousel.js"></script>
<script src="js/jquery.customSelect.min.js"></script>
<script src="js/respond.min.js"></script>
<!--right slidebar-->
<script src="js/slidebars.min.js"></script>
<script src="js/common-scripts.js"></script>

<?php
if($DYNAMIC_TABLE){ ?>
  <!--dynamic table initialization -->
  <script type="text/javascript" language="javascript" src="assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
  <script src="js/dynamic_table_init.js"></script>
  <?php } ?>

  <?php
  if($PICKERS){ ?>
    <script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="assets/bootstrap-daterangepicker/moment.min.js"></script>
    <script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script src="js/advanced-form-components.js"></script>
    <?php
  } ?>
<script type="text/javascript" src="assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script>
  $(document).ready(function(){
    /* notification.php */
    $("#to_do_notif").on("click", function(){
      $.ajax({
        url: "ajax/readNotif.php",
        success: function(msg){
          console.log(msg);
          if (msg) {
            $("#to_do_notif span").html("");
          }
        }
      });
    });

    $('#lang_cn').on('click', function(){
      $.ajax({
        url:"php-functions/fncCommon.php?action=langToCN",
        success:function(msg)
        {
          show_alert("语言已更改", "<?php echo $PAGE_NAME; echo (isset($_GET['project_id'])) ? '?project_id='.$_GET['project_id'] : '' ; ?>", 1);
        }
      });
    });

    $('#lang_en').on('click', function(){
      $.ajax({
        url:"php-functions/fncCommon.php?action=langToEN",
        success:function(msg)
        {
          show_alert("Language changed", "<?php echo $PAGE_NAME; echo (isset($_GET['project_id'])) ? '?project_id='.$_GET['project_id'] : '' ; ?>", 1);
        }
      });
    });

  });
  </script>

  <!--common script for all pages-->
  <!--script for this page-->
  <!--
  <script src="js/sparkline-chart.js"></script>
  <script src="js/easy-pie-chart.js"></script>
  <script src="js/count.js"></script>
-->
<!--
<script>
//owl carousel
$(document).ready(function () {
$("#owl-demo").owlCarousel({
navigation: true
, slideSpeed: 300
, paginationSpeed: 400
, singleItem: true
, autoPlay: true
});
});
//custom select box
$(function () {
$('select.styled').customSelect();
});
$(window).on("resize", function () {
var owl = $("#owl-demo").data("owlCarousel");
owl.reinit();
});
</script>-->
