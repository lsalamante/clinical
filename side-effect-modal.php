<!-- Start of SAE/SIDEEFFECTS modal @Van-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addSideEffectModal" class="modal fade " data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="background #64aaf9">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title" id="modalTitleAddRecord"><?php echo $phrases['side_effects'] ?></h4>
      </div>
      <div class="modal-body nopadL nopadR">

        <form action="#" id="addSideEffectForm" enctype="multipart/form-data" method="POST" class="form-horizontal tasi-form">

          <div class="form-group">
              <label class="col-sm-2" for="lot_num"><?php echo $phrases['lot_number'] ?></label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="lot_num" id="lot_num_s" value="" required>
              </div>

              <label class="col-sm-1"><?php echo $phrases['number']; ?></label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="num" id="num" value="" required>
              </div>
          </div>

          <div class="form-group">
              <label class="col-sm-2"><?php echo $phrases['report_type']; ?></label>
              <div class="col-sm-4">
                <?php foreach($report_t as $id => $text){ ?>
                <div class="checkbox">
                  <input type="checkbox" name="report_type[]" id="report_type_<?php echo $id ?>" value="<?php echo $id ?>"><label for="report_type_<?php echo $id ?>"><?php echo $text; ?></label>
                </div>
                <?php } ?>
              </div>
              <label class="col-sm-2"><?php echo $phrases['report_time']; ?></label>
              <div class="col-sm-4">
                <div class="input-group bootstrap-timepicker">
                    <input type="text" class="form-control" name="report_time" id="report_time" required>
                      <span class="input-group-btn">
                      <!-- <button class="btn btn-default" type="button"><?php echo $phrases['progress_icon']; ?></button> -->
                      </span>
                </div>
              </div>
          </div>

          <div class="form-group">
              <label class="col-sm-2"><?php echo $phrases['hospital']; ?></label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="hospital" id="hospital" required>
              </div>
              <label class="col-sm-2"><?php echo $phrases['hospital_mobile']; ?></label>
              <div class="col-sm-4">
                <input type="number" class="form-control" name="hospital_mobile" id="hospital_mobile" required>
              </div>
          </div>

          <div class="form-group">
              <label class="col-sm-2"><?php echo $phrases['department']; ?></label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="dept" id="dept" required>
              </div>
              <label class="col-sm-2"><?php echo $phrases['department_mobile']?> </label>
              <div class="col-sm-4">
                <input type="number" class="form-control" name="dept_mobile" id="dept_mobile" required>
              </div>
          </div>
          <br>
            <h4 class="modal-title" ><?php echo $phrases['drug_name']?></h4>
          <br>
          <div class="form-group">
              <label class="col-sm-2"><?php echo $phrases['chinese_name']?> </label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="drug_chinese_name" id="drug_chinese_name" required>
              </div>
              <label class="col-sm-2"><?php echo $phrases['english_name']?> </label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="drug_english_name" id="drug_english_name" required>
              </div>
          </div>

          <br>
            <h4 class="modal-title" ><?php echo $phrases['drug_information']?></h4>
          <br>
          <div class="form-group">
            <label class="col-sm-2"><?php echo $phrases['drug_type']?> </label>
            <div class="col-sm-4">
              <?php foreach($drug_t as $id => $text){ ?>
              <div class="radio">
                <input type="radio" required name="drug_type" id="drug_type_<?php echo $id ?>" value="<?php echo $id ?>"><label for="drug_type_<?php echo $id ?>"><?php echo $text; ?></label>
              </div>
              <?php } ?>
              <div id="drug_type_dbl"></div>
            </div>
            <label class="col-sm-2"><?php echo $phrases['form_of_drug']?>  </label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="drug_form" id="drug_form" required>
            </div>
            <br><br><br>
            <label class="col-sm-2"><?php echo $phrases['register_type']?> </label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="drug_register" id="drug_register" required>
            </div>
          </div>

          <br>
            <h4 class="modal-title" ><?php echo $phrases['clinical_trial']?></h4>
          <br>
          <div class="form-group">
            <label class="col-sm-2"><?php echo $phrases['trial_category']?> </label>
            <div class="col-sm-4">
              <?php foreach($trial_c as $id => $text){ ?>
              <div class="radio">
                <input type="radio" required name="clinical_trial" id="clinical_trial_<?php echo $id ?>" value="<?php echo $id ?>"><label for="clinical_trial_<?php echo $id ?>"><?php echo $text; ?></label>
              </div>
              <?php } ?>
              <div id="clinical_trial_dbl"></div>
            </div>
            <label class="col-sm-2"><?php echo $phrases['indications']?> </label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="clinical_indications" id="clinical_indications" required>
            </div>
          </div>

          <br>
            <h4><?php echo $phrases['subjects_situation']?></h4>
          <br>
          <div class="form-group">
            <label class="col-sm-2"><?php echo $phrases['subjects']?> </label>
            <div class="col-sm-4">
              <select class="form-control" name="subject_id" id="se_subj_select" onchange="selectSubject(this.value);">
              <option value=""><?php echo $phrases['select_subject']?></option>
              <?php foreach (getSubjects($_GET['project_id'], array()) as $subject) { ?>
                <option id="se_subj_id_<?php echo $subject['subject_id']; ?>" value="<?php echo $subject['subject_id'] ?>"><?php echo $subject['v_name']; ?></option>
              <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2"><?php echo $phrases['initials']?> </label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="sub_initials" id="sub_initials" required>
            </div>
            <label class="col-sm-2"><?php echo $phrases['gender']?> </label>
            <div class="col-sm-4">
              <div class="radio">
                <input type="radio" name="sub_gender" id="sub_gender_m" value="0"><label for="sub_gender_m"><?php echo $phrases['male']?></label>
              </div>
              <div class="radio">
                <input type="radio" name="sub_gender" id="sub_gender_f" value="1"><label for="sub_gender_f"><?php echo $phrases['female']?></label>
              </div>
            </div>
            <div id="sub_gender_dbl"></div>
          </div>

          <div class="form-group">
            <label class="col-sm-2"><?php echo $phrases['height']?>『cm』</label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="sub_height" id="sub_height" required>
            </div>
            <label class="col-sm-2"><?php echo $phrases['weight']?>『kg』</label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="sub_weight" id="sub_weight" required>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2"><?php echo $phrases['birth_date']?> </label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="sub_birthdate" id="sub_birthdate" required>
              <!-- <input type="text" class="form-control" id="date" name="sub_birthdate" placeholder=""> -->
            </div>
            <label class="col-sm-2"><?php echo $phrases['complications']?> </label>
            <div class="col-sm-4">
              <?php foreach($complications as $id => $text){ ?>
              <div class="radio">
                <input type="radio" required name="sub_complication" id="sub_complications_<?php echo $id ?>" value="<?php echo $id ?>"><label for="sub_complications_<?php echo $id ?>"><?php echo $text; ?></label>
              </div>
              <?php } ?>
              <div id="sub_complication_dbl"></div>
            </div>
          </div>
          <input type="hidden" name="is_sae" id="is_sae">
          <div id="div_SAE">
            <br>
            <h4>SAE</h4>
            <br>

            <div class="form-group">
              <label class="col-sm-2"><?php echo $phrases['sae_diagnosis']?> </label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="sae_diagnosis" id="sae_diagnosis">
              </div>
              <label class="col-sm-2"><?php echo $phrases['drug_measure']?>  </label>
              <div class="col-sm-4">
                <?php foreach($drug_m as $id => $text){ ?>
                <div class="radio">
                  <input type="radio" name="sae_drug_measure" id="sae_drug_measure_<?php echo $id ?>" value="<?php echo $id ?>"><label for="sae_drug_measure_<?php echo $id ?>"><?php echo $text; ?></label>
                </div>
                <?php } ?>
                <div id="sae_drug_measure_dbl"></div>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2"><?php echo $phrases['sae_situation']?> </label>
              <div class="col-sm-4">
                <?php foreach($sae_s as $id => $text){ ?>
                <div class="radio">
                  <input type="radio" name="sae_situation" id="sae_situation_<?php echo $id ?>" value="<?php echo $id ?>"><label for="sae_situation_<?php echo $id ?>"><?php echo $text; ?></label>
                </div>
                <?php } ?>
                <div id="sae_situation_dbl"></div>
              </div>
              <label class="col-sm-2"><?php echo $phrases['sae_drug_relation']?> </label>
              <div class="col-sm-4">
                <?php foreach($relation_sae as $id => $text){ ?>
                <div class="radio">
                  <input type="radio" name="relation_sae_drug" id="relation_sae_drug_<?php echo $id ?>" value="<?php echo $id ?>"><label for="relation_sae_drug_<?php echo $id ?>"><?php echo $text; ?></label>
                </div>
                <?php } ?>
                <div id="relation_sae_drug_dbl"></div>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2"><?php echo $phrases['saevest']?> </label>
              <div class="col-sm-4">
                <?php foreach($sae_v as $id => $text){ ?>
                <div class="radio">
                  <input type="radio" name="sae_vest" id="sae_vest_<?php echo $id ?>" value="<?php echo $id ?>"><label for="sae_vest_<?php echo $id ?>"><?php echo $text; ?></label>
                </div>
                <?php } ?>
                <div id="sae_vest_dbl"></div>
              </div>
              <label class="col-sm-2"><?php echo $phrases['unblinding_situation']?> </label>
              <div class="col-sm-4">
                <?php foreach($unblinding_s as $id => $text){ ?>
                <div class="radio">
                  <input type="radio" name="sae_unbinding" id="sae_unbinding_<?php echo $id ?>" value="<?php echo $id ?>"><label for="sae_unbinding_<?php echo $id ?>"><?php echo $text; ?></label>
                </div>
                <?php } ?>
                <div id="sae_unbinding_dbl"></div>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2"><?php echo $phrases['report_internal']?> </label>
              <div class="col-sm-4">
               <?php foreach($report_i_e as $id => $text){ ?>
                <div class="radio">
                  <input type="radio" name="sae_report_internal" id="sae_report_internal_<?php echo $id ?>" value="<?php echo $id ?>"><label for="sae_report_internal_<?php echo $id ?>"><?php echo $text; ?></label>
                </div>
                <?php } ?>
                <div id="sae_report_internal_dbl"></div>
              </div>
              <label class="col-sm-2"><?php echo $phrases['report_external']?> </label>
              <div class="col-sm-4">
               <?php foreach($report_i_e as $id => $text){ ?>
                <div class="radio">
                  <input type="radio" name="sae_report_external" id="sae_report_external_<?php echo $id ?>" value="<?php echo $id ?>"><label for="sae_report_external_<?php echo $id ?>"><?php echo $text; ?></label>
                </div>
                <?php } ?>
                <div id="sae_report_external_dbl"></div>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-3"><?php echo $phrases['handle_detail']?> </label>
              <div class="col-sm-9">
                <textarea type="text" class="form-control" style="resize:vertical" name="sae_handle_detail" id="sae_handle_detail"></textarea>
              </div>
            </div>
          </div>

          <br>
            <h4><?php echo $phrases['other']; ?></h4>
          <br>
          <div class="form-group">
            <label class="col-sm-2"><?php echo $phrases['reporter_unit']?>  </label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="report_unit" id="report_unit" required>
            </div>
            <label class="col-sm-2"><?php echo $phrases['reporter_position']?> </label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="reporter_position" id="reporter_position" required>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3"><?php echo $phrases['remarks']?> </label>
            <div class="col-sm-9">
              <textarea type="text" class="form-control" style="resize:vertical" name="remark" id="side_remark" ></textarea>
            </div>
          </div>
          <div class="form-group">
            <h5 style="margin-left:15px"><?php echo $phrases['meeting_material']; ?></h5>
            <?php if( isset($_GET["project_id"]) && $_GET["project_id"] != "" && ($_SESSION["user_type"] == "RN" || $_SESSION['user_type'] == "I")){ // make way for training list ?>
                <a href="#se_docs_modal" data-toggle="modal" class="btn btn-xs btn-primary" style="margin-left:15px; margin-bottom:10px;" onclick="$('.modal').modal('hide'); $('#se_docs_form')[0].reset();"><?php echo $phrases['add']; ?></a>
            <?php } ?>
            <br>
              <div class="col-sm-12">
                <table class="table table-hover">
                  <thead>
                    <th><?php echo $phrases['number']?></th>
                    <th><?php echo $phrases['name']?></th>
                    <th><?php echo $phrases['version']?></th>
                    <th><?php echo $phrases['version_date']?></th>
                    <th><?php echo $phrases['description']; ?></th>
                    <th><?php echo $phrases['action']; ?></th>
                  </thead>
                  <tbody id="table_side_effect_upload">
                  </tbody>
                </table>
              </div>
          </div>

          <!-- Start hidden fields @Van-->

            <input type="hidden" name="project_idSE" id="project_idSE" value="<?php echo $_GET["project_id"]; ?>">
            <input type="hidden" name="side_effect_id_update" id="side_effect_id_update" value="">
            <input type="hidden" name="is_drafted" id="is_drafted_se">
            <!-- <input type="hidden" name="side_effect_id_docs" id="side_effect_id_docs" value=""> -->

          <!-- End hidden fields @Van-->

          <?php //if(){ // make way for side-effect-list ?>
              <button type="submit" class="btn btn-primary" id="submitSideEffects" style="<?php echo isset($_GET["project_id"]) && $_GET["project_id"] != "" && ($_SESSION["user_type"] == "RN" || $_SESSION['user_type'] == "I" || $_SESSION["user_type"] == "SC") ? 'display:block;' : 'display:none;';  ?>"><?php echo $phrases['submit']?></button>
              <button type="submit" class="btn btn-primary" id="draftSideEffects" style="<?php echo isset($_GET["project_id"]) && $_GET["project_id"] != "" && ($_SESSION["user_type"] == "RN" || $_SESSION['user_type'] == "I" || $_SESSION["user_type"] == "SC") ? 'display:block;' : 'display:none;';  ?>"><?php echo $phrases['save_as_draft']?></button>
          <?php// } ?>

          <!-- BUTTON FOR NOTIFY FOR DA -->
          <?php if($_SESSION["user_type"] == "DA" ): ?>
            <button class="btn btn-primary" type="button" onclick="notifyOwner('<?php echo $_GET['project_id']?>', 'I', 'side_effects')">
              <?php echo $t->tryTranslate('notify')?>
            </button>
          <?php endif; ?>
          <!-- / BUTTON FOR NOTIFY FOR DA -->
        </form>
      </div>
    </div>
  </div>
</div>

<!--  DOUBLE CHECK SIDE EFFECT  -->
  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="side_effects_dbl_chk_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
          <h4 class="modal-title"><?php echo $t->tryTranslate('add_double_check_opinion') ?></h4>
        </div>
        <div class="modal-body">
          <form role="form" id="side_effects_dbl_chk_form">
            <div class="form-group">
              <div class="form-group <?php echo ($_SESSION['user_type'] == 'DA') ? '' : 'hidden' ; ?>">
                <div class="radio">
                  <input value="1" name="is_checked" id="side_effects_dbl_chk_pass" type="radio" onclick="clearDblCheck('side_effects_comment', this.value);"> <?php echo $t->tryTranslate('no_problem') ?> &nbsp;&nbsp;
                  <input value="-1" name="is_checked" id="side_effects_dbl_chk_nopass" type="radio" onclick="clearDblCheck('side_effects_comment', this.value);"> <?php echo $t->tryTranslate('have_problem') ?> </div>
              </div>
              <label for="exampleInputEmail1" class="dbl_chk_label"><?php echo $t->tryTranslate('comments') ?></label>
              <textarea class="form-control v-resize-only" id="side_effects_comment" name="comments" <?php echo ($_SESSION['user_type'] != 'DA') ? 'disabled' : '' ;?> placeholder=""></textarea>
              <input type='hidden' name="bio_id" id="side_effects_dbl_chk_id" value="">  <!-- unique id of PK-->
              <input type='hidden' name="payload_key" id="side_effects_payload_key" value="">  <!-- key of payload to modify -->
              <input type='hidden' name="table" id="side_effects_dbl_chk_tbl" value="side_effect"> <!-- table name to modify -->
              <input type='hidden' name="pk_col" id="side_effects_pk_col" value="side_effect_id">  <!-- primary key column name of table-->
            </div>
            <?php if($_SESSION['user_type'] == 'DA'):?>
            <button type="" class="btn btn-primary make-loading" id="side_effects_dbl_chk_submit"><?php echo $phrases['submit'] ?></button>
          <?php endif; ?>
          </form>
        </div>
      </div>
    </div>
  </div>
<!-- / DOUBLE CHECK SIDE EFFECTS -->



<!-- End of SAE/SIDEEFFECTS modal @Van-->
