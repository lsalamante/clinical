<!-- Add Side Effect -->

<?php require("php-functions/fncSideEffect.php"); ?>


<?php if(isset($_GET["project_id"]) && $_GET["project_id"] != "" && ($_SESSION["user_type"] == "RN" || $_SESSION["user_type"] == "I")){ ?>

  <a class="btn btn-primary adder" id="addSideEffect" onclick="rmDoubleCheck();"><?php echo $phrases['add']?></a>
  <form action="#" type="POST" id="addNewSE">
    <input type="hidden" name="project_id" id="project_idE" value="<?php echo $_GET["project_id"]; ?>">
    <input type="hidden" name="created_by" id="created_byE" value="<?php echo $_SESSION['role_id']; ?>">
  </form>

<?php } ?>


<?php
#TODO make this external file include();
$report_t = array(
  "0" => $phrases['first_report'],
  "1" => $phrases['follow_up_report'],
  "2" => $phrases['sum_up_report']
  );

$drug_t = array(
  "0" => $phrases['traditional_chinese_medicine'],
  "1" => $phrases['chemistry_drug'],
  "2" => $phrases['cure_medicine'],
  "3" => $phrases['prevent_medicine'],
  "4" => $phrases['other']
  );

$trial_c = array(
  "0" => $phrases['stage_1'],
  "1" => $phrases['stage_2'],
  "2" => $phrases['stage_3'],
  "3" => $phrases['stage_4'],
  "4" => $phrases['be_trial'],
  "5" => $phrases['clinical_verifications']
  );

$complications = array(
  "0" => $phrases['no'],
  "1" => $phrases['yes']
  );

$sae_s = array(
  "0" => $phrases['die'],
  "1" => $phrases['be_in_hospital'],
  "2" => $phrases['disability'],
  "3" => $phrases['dysfunction'],
  "4" => $phrases['deformity'],
  "5" => $phrases['life_threatening'],
  "6" => $phrases['other'],
  "7" => $phrases['severe']
  );

$drug_m = array(
  "0" => $phrases['continuantur_remedia'],
  "1" => $phrases['reduce_the_dosage'],
  "2" => $phrases['stop_then_go'],
  "3" => $phrases['stop']
  );

$sae_v = array(
  "0" => $phrases['transference_cure'],
  "1" => $phrases['symptoms_last']
  );

$relation_sae = array(
  "0" => $phrases['certainty'],
  "1" => $phrases['maybe_concern'] ,
  "2" => $phrases['be_unconcerned'] ,
  "3" => $phrases['certainty_unconcerned'],
  "4" => $phrases['not_determinable']
  );

$unblinding_s = array(
  "0" => $phrases['no_blinding'],
  "1" => $phrases['did_not_unblinding'],
  "2" => $phrases['unblinding']
  );

$report_i_e = array(
  "0" => $phrases['yes'],
  "1" => $phrases['no'],
  "2" => $phrases['no_detail']
  );


?>


    <?php

      $project_id = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? $_GET["project_id"] : 0;

                                                                                                                                                      //$projects - were from side-effect-list.php
      $sideEffects = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? getSideEffects(false, $project_id) : getSideEffects(false, $project_id, $projects);//make way for side effect list


      $count = 0;

      if(count($sideEffects) > 0 ){ // check if found record/s and //make way for inspection list.

  ?>

        <table class="table table-hover">
          <thead>
            <tr>
              <th><?php echo $phrases['number'] ?></th>
              <th><?php echo $phrases['project']; ?></th>
              <th><?php echo $phrases['subjects']; ?></th>
              <th><?php echo $phrases['reporter_unit'] ?></th>
              <th><?php echo $phrases['report_time'] ?></th>
              <th><?php echo $phrases['report_type'] ?></th>
              <th><?php echo $phrases['treatment_situation']; ?></th>
              <th><?php echo $t->tryTranslate('status'); ?></th>
              <th><?php echo $phrases['action'] ?></th>
            </tr>
          </thead>
          <tbody id="se_tbody">

            <?php

                $count = 0;

                foreach ($sideEffects as $effect) {

                  $count++;
            ?>

                <tr onclick="getSideEffectDetails(<?php echo $effect['side_effect_id']; ?>, event, '<?php echo $_SESSION['user_type']; ?>'); return false;" style="cursor:pointer;">
                    <td><?php echo $count; ?></td>
                    <td><?php echo $effect["project_name"]; ?></td>
                    <td><?php echo $effect["sub_initials"]; ?></td>
                    <td><?php echo $effect["report_unit"]; ?></td>
                    <td><?php echo $effect["report_time"] != "0000-00-00 00:00:00.000000" && $effect["report_time"] != "0000-00-00 00:00:00" ? date("Y-m-d", strtotime($effect["report_time"])) : $phrases['not_applicable'] ; ?></td>
                    <td><?php
                    $all_reports = explode(",", $effect['report_type']);
                    foreach ($all_reports as $value) {
                      if($value != '')
                      {
                        echo $report_t[$value]."<br>";
                      }
                      else
                      {
                        echo $phrases['not_applicable'];
                      }
                    }
                    ?></td>
                    <td><?php echo $sae_s[$effect['sae_situation']] ?></td>
                    <td><?php echo $effect['is_drafted'] == 1 ? $t->tryTranslate('drafted') : $t->tryTranslate('submitted'); ?></td>
                    <td onclick="event.cancelBubble = true;">
                      <a  class="btn btn-xs btn-primary" title="add remarks" onclick="$('#side_effect_remark_modal').modal('toggle');
                      $('#side_effect_remark_modal').modal('show'); changeSideEffectRemarkId(<?php echo $effect['side_effect_id']; ?>)" ><?php echo $phrases['remarks']; ?></a>
                    </td>
                    <!--  TODO Add delete here? -->
                </tr>

            <?php } ?>

          </tbody>
        </table>


    <?php }else{ // no record/s found ?>

        <div class="col-lg-12 text-center">
          <img src="img/sad.png" class="sad">
          <h1 class="sad"><?php echo $phrases['empty_table_data'] ?></h1>
        </div>

  <?php } ?>





<!--  REMARK MODAL  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="side_effect_remark_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $phrases['add_remarks'] ?></h4>
      </div>
      <div class="modal-body">
        <form role="form" id="side_effect_remark_form">
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo $phrases['remarks'] ?></label>
            <textarea class="form-control v-resize-only" id="side_effect_remarks" name="clinician_remarks" placeholder="" required></textarea>
            <input type='hidden' name="side_effect_id" id="side_effect_remark_id" value="">
          </div>
          <?php $side_effects_remarks = array("C", "DA"); ?>
          <button type="" class="btn btn-primary make-loading" id="side_effect_remark_submit" <?php if(!in_array($_SESSION['user_type'], $side_effects_remarks)) echo 'disabled'; ?>><?php echo $phrases['submit'] ?></button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- / REMARK MODAL  -->

<!-- add Upload -->

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="se_docs_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $phrases['add']; ?></h4>
      </div>
      <div class="modal-body">

        <form role="form" id="se_docs_form" enctype="multipart/form-data">
          <div class="form-group">
            <label for=""><?php echo $phrases['name'] ?></label>
            <input type="text" class="form-control" name="name" placeholder="">
          </div>
          <div class="form-group">
            <label for=""><?php echo $phrases['version'] ?></label>
            <input type="number" min="0" step="0.1" class="form-control" name="version" placeholder="">
          </div>
          <div class="form-group">
            <label for=""><?php echo $phrases['description'] ?></label>
            <textarea class="form-control v-resize-only" name="description" placeholder=""></textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputFile"><?php echo $phrases['select_file']; ?></label>
            <input type="file" name="up_file[]" multiple="" required>
            <!-- <p class="help-block">Example block-level help text here.</p> -->
          </div>
          <!-- Databank -->
          <div class="form-group">
            <input type="checkbox" name="databank" value="1" id="sideeffects_databank">
            <label for="sideeffects_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
          </div>
          <!-- / Databank -->
          <?php if(isset($_GET['project_id'])): ?>
          <?php if($_GET["project_id"] != "" && ($_SESSION["user_type"] == "RN" || $_SESSION['user_type'] == "I") ){   ?>
            <input type="hidden" name="project_id" id="project_id_upload_se" value="<?php echo $_GET['project_id']; ?>">
            <button type="" class="btn btn-primary make-loading" id="se_docs_submit"><?php echo $phrases['submit']; ?></button>
          <?php } ?>
        <?php endif; ?>

        </form>
      </div>
    </div>
  </div>
</div>

<!-- / add Upload -->
