/* for remarks */
var side_effect_remark_form = $("#side_effect_remark_form");
var side_effect_remark_id = $("#side_effect_remark_id");
var side_effect_remarks = $("#side_effect_remarks");
var side_effect_remark_submit = $("#side_effect_remark_submit");

changeSideEffectRemarkId = function (side_effect_id) {
  side_effect_remark_id.val(side_effect_id);
  $.ajax({
    url:"php-functions/fncSideEffect.php?action=getSideEffectRemarks",
    type: "POST",
    data: { "side_effect_id" : side_effect_id, "type" : "ajax" },
    success:function(data){
      var new_data = JSON.parse(data, true);
      $('#side_effect_remarks').val(new_data[0].clinician_remarks);
    }
  });

}

side_effect_remark_form.on('submit', function(e) {
  e.preventDefault();
  var form_data = side_effect_remark_form.serialize();

  $.ajax({
    url: "php-functions/fncSideEffect.php?action=addSideEffectRemarks",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        show_alert("Operation successful.", "", 1);
      }else{
        show_alert("Operation failed.", "", 0);
      }
    }
  });

});

selectSubject = function(subject_id){
  $.ajax({
    url:"php-functions/fncSideEffect.php?action=getSubjectData",
    type:"POST",
    data : {"subject_id" : subject_id, "type" : "ajax"},
    success:function(data)
    {
      var new_data = JSON.parse(data, true);
      if(new_data[0].gender == "Male")
      {
        $('#sub_gender_m').prop('checked', true);
      }
      else
      {
        $('#sub_gender_f').prop('checked', true);
      }
      $('#sub_height').val(new_data[0].height);
      $('#sub_weight').val(new_data[0].weight);
      $('#sub_birthdate').val(new_data[0].bday);
    }
  })
}
