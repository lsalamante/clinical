-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2017 at 12:05 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clinical`
--

-- --------------------------------------------------------

--
-- Table structure for table `bio_destroy`
--

CREATE TABLE `bio_destroy` (
  `bio_destroy_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `bio_date` datetime NOT NULL,
  `destroy_address` varchar(200) NOT NULL,
  `destroy_operator` varchar(200) NOT NULL,
  `destroy_record` varchar(250) NOT NULL,
  `bio_desc` varchar(300) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `remarks` varchar(250) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0',
  `dbl_chk_payload` varchar(21000) NOT NULL DEFAULT '{      "name":{         "is_checked":0,       "comments":""    },    "quantity":{         "is_checked":0,       "comments":""    },    "bio_date":{         "is_checked":0,       "comments":""    },    "destroy_address":{         "is_checked":0,       "comments":""    },    "destroy_operator":{         "is_checked":0,       "comments":""    },    "destroy_record":{         "is_checked":0,       "comments":""    },    "bio_desc":{         "is_checked":0,       "comments":""    } }' COMMENT '0 - unchecked, -1 - checked/no pass, 1 - checked/pass'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bio_destroy`
--

INSERT INTO `bio_destroy` (`bio_destroy_id`, `name`, `quantity`, `bio_date`, `destroy_address`, `destroy_operator`, `destroy_record`, `bio_desc`, `date_created`, `role_id`, `project_id`, `remarks`, `databank`, `dbl_chk_payload`) VALUES
(1, 'whole_blood', '', '1970-01-01 00:00:00', '', '', '', '', '2016-12-28 01:48:01', 11, 5, '', 0, '{      "name":{         "is_checked":0,       "comments":""    },    "quantity":{         "is_checked":0,       "comments":""    },    "bio_date":{         "is_checked":0,       "comments":""    },    "destroy_address":{         "is_checked":0,       "comments":""    },    "destroy_operator":{         "is_checked":0,       "comments":""    },    "destroy_record":{         "is_checked":0,       "comments":""    },    "bio_desc":{         "is_checked":0,       "comments":""    } }'),
(2, 'whole_blood', '', '1970-01-01 00:00:00', '', '', '', '', '2016-12-28 01:48:05', 11, 5, '', 0, '{      "name":{         "is_checked":0,       "comments":""    },    "quantity":{         "is_checked":0,       "comments":""    },    "bio_date":{         "is_checked":0,       "comments":""    },    "destroy_address":{         "is_checked":0,       "comments":""    },    "destroy_operator":{         "is_checked":0,       "comments":""    },    "destroy_record":{         "is_checked":0,       "comments":""    },    "bio_desc":{         "is_checked":0,       "comments":""    } }'),
(3, 'whole_blood', 'a', '2016-12-28 00:00:00', '', '', '', '', '2016-12-28 09:19:09', 11, 7, '', 0, '{      "name":{         "is_checked":0,       "comments":""    },    "quantity":{         "is_checked":0,       "comments":""    },    "bio_date":{         "is_checked":0,       "comments":""    },    "destroy_address":{         "is_checked":0,       "comments":""    },    "destroy_operator":{         "is_checked":0,       "comments":""    },    "destroy_record":{         "is_checked":0,       "comments":""    },    "bio_desc":{         "is_checked":0,       "comments":""    } }'),
(4, 'whole_blood', 'b', '1970-01-01 00:00:00', '', '', '', '', '2016-12-28 09:19:18', 11, 7, '', 0, '{      "name":{         "is_checked":0,       "comments":""    },    "quantity":{         "is_checked":0,       "comments":""    },    "bio_date":{         "is_checked":0,       "comments":""    },    "destroy_address":{         "is_checked":0,       "comments":""    },    "destroy_operator":{         "is_checked":0,       "comments":""    },    "destroy_record":{         "is_checked":0,       "comments":""    },    "bio_desc":{         "is_checked":0,       "comments":""    } }'),
(5, 'whole_blood', 'df', '2016-12-08 00:00:00', 'adf', 'adf', 'http://betaprojex.com/Clinical2/uploads/bio_destroy/bio_destroy_record_14830785011483078501.docx', '', '2016-12-30 06:15:01', 11, 12, '', 0, '{      "name":{         "is_checked":0,       "comments":""    },    "quantity":{         "is_checked":0,       "comments":""    },    "bio_date":{         "is_checked":0,       "comments":""    },    "destroy_address":{         "is_checked":0,       "comments":""    },    "destroy_operator":{         "is_checked":0,       "comments":""    },    "destroy_record":{         "is_checked":0,       "comments":""    },    "bio_desc":{         "is_checked":0,       "comments":""    } }'),
(6, 'dung', '321', '2017-02-10 00:00:00', '321', '321', 'http://localhost/clinical/uploads/bio_destroy/bio_destroy_record_14867244081486724408.jpg', '321z', '2017-02-10 10:42:56', 11, 27, 'dsa', 0, '{"name":{"is_checked":"-1","comments":"321"},"quantity":{"is_checked":"1","comments":""},"bio_date":{"is_checked":"1","comments":""},"destroy_address":{"is_checked":"-1","comments":""},"destroy_operator":{"is_checked":"1","comments":"11"},"destroy_record":{"is_checked":"-1","comments":"321"},"bio_desc":{"is_checked":"-1","comments":"131"}}');

-- --------------------------------------------------------

--
-- Table structure for table `bio_determine`
--

CREATE TABLE `bio_determine` (
  `determine_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'blood types?',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` varchar(255) NOT NULL COMMENT 'a.k.a. sample size?',
  `determine_start` datetime NOT NULL,
  `determine_end` datetime NOT NULL,
  `determine_record` text NOT NULL COMMENT 'file url',
  `determine_report` text NOT NULL COMMENT 'file url',
  `description` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL COMMENT 'role id of uploader',
  `remarks` text NOT NULL,
  `determine_company` varchar(255) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0',
  `dbl_chk_payload` varchar(21000) NOT NULL DEFAULT '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "determine_start": { "is_checked": 0, "comments": "" }, "determine_end": { "is_checked": 0, "comments": "" }, "determine_record": { "is_checked": 0, "comments": "" }, "determine_report": { "is_checked": 0, "comments": "" },"description" : { "is_checked": 0, "comments": "" }  ,"determine_company" : { "is_checked": 0, "comments": "" } }' COMMENT '0 - unchecked, -1 - checked/no pass, 1 - checked/pass'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bio_determine`
--

INSERT INTO `bio_determine` (`determine_id`, `name`, `date_created`, `quantity`, `determine_start`, `determine_end`, `determine_record`, `determine_report`, `description`, `project_id`, `role_id`, `remarks`, `determine_company`, `databank`, `dbl_chk_payload`) VALUES
(1, 'whole_blood', '2016-12-28 01:47:38', 'cz', '2016-12-28 00:00:00', '2016-12-28 00:00:00', '', '', '', 5, 11, '', 'xz', 0, '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "determine_start": { "is_checked": 0, "comments": "" }, "determine_end": { "is_checked": 0, "comments": "" }, "determine_record": { "is_checked": 0, "comments": "" }, "determine_report": { "is_checked": 0, "comments": "" },"description" : { "is_checked": 0, "comments": "" }  ,"determine_company" : { "is_checked": 0, "comments": "" } }'),
(2, 'whole_blood', '2016-12-28 01:47:43', '', '1970-01-01 00:00:00', '1970-01-01 00:00:00', '', '', '', 5, 11, '', '', 0, '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "determine_start": { "is_checked": 0, "comments": "" }, "determine_end": { "is_checked": 0, "comments": "" }, "determine_record": { "is_checked": 0, "comments": "" }, "determine_report": { "is_checked": 0, "comments": "" },"description" : { "is_checked": 0, "comments": "" }  ,"determine_company" : { "is_checked": 0, "comments": "" } }'),
(3, 'blood_plasma', '2016-12-28 09:18:05', 'a', '1970-01-01 00:00:00', '1970-01-01 00:00:00', '', '', '', 7, 11, '', 'a', 0, '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "determine_start": { "is_checked": 0, "comments": "" }, "determine_end": { "is_checked": 0, "comments": "" }, "determine_record": { "is_checked": 0, "comments": "" }, "determine_report": { "is_checked": 0, "comments": "" },"description" : { "is_checked": 0, "comments": "" }  ,"determine_company" : { "is_checked": 0, "comments": "" } }'),
(4, 'urine', '2016-12-28 09:18:14', 'b', '1970-01-01 00:00:00', '1970-01-01 00:00:00', '', '', '', 7, 11, '', 'b', 0, '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "determine_start": { "is_checked": 0, "comments": "" }, "determine_end": { "is_checked": 0, "comments": "" }, "determine_record": { "is_checked": 0, "comments": "" }, "determine_report": { "is_checked": 0, "comments": "" },"description" : { "is_checked": 0, "comments": "" }  ,"determine_company" : { "is_checked": 0, "comments": "" } }'),
(5, 'whole_blood', '2016-12-30 06:13:48', '23', '2016-12-01 00:00:00', '2016-12-08 00:00:00', '', '', '', 12, 11, '', 'df ', 0, '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "determine_start": { "is_checked": 0, "comments": "" }, "determine_end": { "is_checked": 0, "comments": "" }, "determine_record": { "is_checked": 0, "comments": "" }, "determine_report": { "is_checked": 0, "comments": "" },"description" : { "is_checked": 0, "comments": "" }  ,"determine_company" : { "is_checked": 0, "comments": "" } }'),
(6, 'blood_plasma', '2017-01-12 03:06:25', 'aaaaaaa', '2017-01-05 00:00:00', '2017-01-05 00:00:00', '', '', 'aaaaaaa', 27, 11, '', 'aaaaaaa', 0, '{"name":{"is_checked":"-1","comments":"111"},"quantity":{"is_checked":"1","comments":"1"},"determine_start":{"is_checked":"-1","comments":"ss"},"determine_end":{"is_checked":"1","comments":"1"},"determine_record":{"is_checked":"-1","comments":"c"},"determine_report":{"is_checked":"-1","comments":"c"},"description":{"is_checked":"1","comments":"a"},"determine_company":{"is_checked":"1","comments":""}}');

-- --------------------------------------------------------

--
-- Table structure for table `bio_handles`
--

CREATE TABLE `bio_handles` (
  `bio_handle_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'blood types?',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` varchar(255) NOT NULL COMMENT 'a.k.a. sample size?',
  `handle_start` datetime NOT NULL,
  `handle_end` datetime NOT NULL,
  `handle_record` text NOT NULL COMMENT 'file url',
  `description` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL COMMENT 'role id of uploader',
  `remarks` text NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0',
  `dbl_chk_payload` varchar(21000) NOT NULL DEFAULT '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "handle_start": { "is_checked": 0, "comments": "" }, "handle_end": { "is_checked": 0, "comments": "" }, "handle_record": { "is_checked": 0, "comments": "" }, "description" : { "is_checked": 0, "comments": "" } }' COMMENT '0 - unchecked, -1 - checked/no pass, 1 - checked/pass'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bio_handles`
--

INSERT INTO `bio_handles` (`bio_handle_id`, `name`, `date_created`, `quantity`, `handle_start`, `handle_end`, `handle_record`, `description`, `project_id`, `role_id`, `remarks`, `databank`, `dbl_chk_payload`) VALUES
(10, '', '2017-02-10 03:11:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', 0, 0, '', 0, '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "handle_start": { "is_checked": 0, "comments": "" }, "handle_end": { "is_checked": 0, "comments": "" }, "handle_record": { "is_checked": 0, "comments": "" }, "description" : { "is_checked": 0, "comments": "" } }'),
(11, 'urine', '2017-02-10 03:12:08', 'asd', '2017-02-10 00:00:00', '2017-02-10 00:00:00', 'http://localhost/clinical/uploads/bio_handles/bio_handle_record_14866963381486696338.jpg', 'sdasdaaa', 27, 11, '', 0, '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "handle_start": { "is_checked": 0, "comments": "" }, "handle_end": { "is_checked": 0, "comments": "" }, "handle_record": { "is_checked": 0, "comments": "" }, "description" : { "is_checked": 0, "comments": "" } }'),
(12, 'whole_blood', '2017-02-10 03:19:35', 'ccc', '2017-02-10 00:00:00', '2017-02-10 00:00:00', 'http://localhost/clinical/uploads/bio_handles/bio_handle_record_14866967951486696795.png', 'cc', 27, 11, '', 0, '{"name":{"is_checked":"1","comments":"zzz"},"quantity":{"is_checked":"-1","comments":"ok"},"handle_start":{"is_checked":"-1","comments":"nice"},"handle_end":{"is_checked":"-1","comments":"Wohir ist deine Freund?"},"handle_record":{"is_checked":"-1","comments":"Hallo"},"description":{"is_checked":"1","comments":"Ok"}}'),
(13, 'blood_serum', '2017-02-10 03:20:17', 'e', '2017-02-10 00:00:00', '2017-02-10 00:00:00', 'http://localhost/clinical/uploads/bio_handles/bio_handle_record_14866968371486696837.jpg', 'eeeefff', 27, 11, '', 0, '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "handle_start": { "is_checked": 0, "comments": "" }, "handle_end": { "is_checked": 0, "comments": "" }, "handle_record": { "is_checked": 0, "comments": "" }, "description" : { "is_checked": 0, "comments": "" } }');

-- --------------------------------------------------------

--
-- Table structure for table `bio_recycle`
--

CREATE TABLE `bio_recycle` (
  `bio_recycle_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `sending_party` varchar(200) NOT NULL,
  `sending_operator` varchar(200) NOT NULL,
  `sending_date` datetime NOT NULL,
  `shipper` varchar(200) NOT NULL,
  `shipper_operator` varchar(200) NOT NULL,
  `shipper_date` datetime NOT NULL,
  `recycler` varchar(200) NOT NULL,
  `recycler_operator` varchar(200) NOT NULL,
  `recycler_date` datetime NOT NULL,
  `recycle_record` varchar(220) NOT NULL,
  `bio_desc` varchar(300) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `remarks` varchar(300) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0',
  `dbl_chk_payload` varchar(21000) NOT NULL DEFAULT '{  "name" : { "is_checked": 0, "comments": "" },  "quantity": { "is_checked": 0, "comments": "" },  "sending_party": { "is_checked": 0, "comments": "" },  "sending_operator": { "is_checked": 0, "comments": "" },  "sending_date": { "is_checked": 0, "comments": "" },  "shipper": { "is_checked": 0, "comments": "" }, "shipper_operator" : { "is_checked": 0, "comments": "" }, "shipper_date" : { "is_checked": 0, "comments": "" }, "recycler" : { "is_checked": 0, "comments": "" }, "recycler_operator" : { "is_checked": 0, "comments": "" }, "recycler_date" : { "is_checked": 0, "comments": "" }, "recycle_record" : { "is_checked": 0, "comments": "" }, "bio_desc" : { "is_checked": 0, "comments": "" } }	' COMMENT '0 - unchecked, -1 - checked/no pass, 1 - checked/pass'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bio_recycle`
--

INSERT INTO `bio_recycle` (`bio_recycle_id`, `name`, `quantity`, `sending_party`, `sending_operator`, `sending_date`, `shipper`, `shipper_operator`, `shipper_date`, `recycler`, `recycler_operator`, `recycler_date`, `recycle_record`, `bio_desc`, `date_created`, `role_id`, `project_id`, `remarks`, `databank`, `dbl_chk_payload`) VALUES
(1, 'whole_blood', '', '', '', '1970-01-01 00:00:00', '', '', '1970-01-01 00:00:00', '', '', '1970-01-01 00:00:00', '', '', '2016-12-28 01:47:51', 11, 5, '', 0, '{  "name" : { "is_checked": 0, "comments": "" },  "quantity": { "is_checked": 0, "comments": "" },  "sending_party": { "is_checked": 0, "comments": "" },  "sending_operator": { "is_checked": 0, "comments": "" },  "sending_date": { "is_checked": 0, "comments": "" },  "shipper": { "is_checked": 0, "comments": "" }, "shipper_operator" : { "is_checked": 0, "comments": "" }, "shipper_date" : { "is_checked": 0, "comments": "" }, "recycler" : { "is_checked": 0, "comments": "" }, "recycler_operator" : { "is_checked": 0, "comments": "" }, "recycler_date" : { "is_checked": 0, "comments": "" }, "recycle_record" : { "is_checked": 0, "comments": "" }, "bio_desc" : { "is_checked": 0, "comments": "" } }	'),
(2, 'whole_blood', '', '', '', '1970-01-01 00:00:00', '', '', '1970-01-01 00:00:00', '', '', '1970-01-01 00:00:00', '', '', '2016-12-28 01:47:54', 11, 5, '', 0, '{  "name" : { "is_checked": 0, "comments": "" },  "quantity": { "is_checked": 0, "comments": "" },  "sending_party": { "is_checked": 0, "comments": "" },  "sending_operator": { "is_checked": 0, "comments": "" },  "sending_date": { "is_checked": 0, "comments": "" },  "shipper": { "is_checked": 0, "comments": "" }, "shipper_operator" : { "is_checked": 0, "comments": "" }, "shipper_date" : { "is_checked": 0, "comments": "" }, "recycler" : { "is_checked": 0, "comments": "" }, "recycler_operator" : { "is_checked": 0, "comments": "" }, "recycler_date" : { "is_checked": 0, "comments": "" }, "recycle_record" : { "is_checked": 0, "comments": "" }, "bio_desc" : { "is_checked": 0, "comments": "" } }	'),
(3, 'blood_plasma', 'a', 'a', '', '1970-01-01 00:00:00', '', '', '1970-01-01 00:00:00', '', '', '1970-01-01 00:00:00', '', '', '2016-12-28 09:18:38', 11, 7, '', 0, '{  "name" : { "is_checked": 0, "comments": "" },  "quantity": { "is_checked": 0, "comments": "" },  "sending_party": { "is_checked": 0, "comments": "" },  "sending_operator": { "is_checked": 0, "comments": "" },  "sending_date": { "is_checked": 0, "comments": "" },  "shipper": { "is_checked": 0, "comments": "" }, "shipper_operator" : { "is_checked": 0, "comments": "" }, "shipper_date" : { "is_checked": 0, "comments": "" }, "recycler" : { "is_checked": 0, "comments": "" }, "recycler_operator" : { "is_checked": 0, "comments": "" }, "recycler_date" : { "is_checked": 0, "comments": "" }, "recycle_record" : { "is_checked": 0, "comments": "" }, "bio_desc" : { "is_checked": 0, "comments": "" } }	'),
(4, 'blood_serum', 'b', 'b', '', '1970-01-01 00:00:00', '', '', '1970-01-01 00:00:00', '', '', '1970-01-01 00:00:00', '', '', '2016-12-28 09:18:48', 11, 7, '', 0, '{  "name" : { "is_checked": 0, "comments": "" },  "quantity": { "is_checked": 0, "comments": "" },  "sending_party": { "is_checked": 0, "comments": "" },  "sending_operator": { "is_checked": 0, "comments": "" },  "sending_date": { "is_checked": 0, "comments": "" },  "shipper": { "is_checked": 0, "comments": "" }, "shipper_operator" : { "is_checked": 0, "comments": "" }, "shipper_date" : { "is_checked": 0, "comments": "" }, "recycler" : { "is_checked": 0, "comments": "" }, "recycler_operator" : { "is_checked": 0, "comments": "" }, "recycler_date" : { "is_checked": 0, "comments": "" }, "recycle_record" : { "is_checked": 0, "comments": "" }, "bio_desc" : { "is_checked": 0, "comments": "" } }	'),
(5, 'whole_blood', 'adf', 'adf ', 'adf', '2016-12-08 00:00:00', 'adf', 'adf', '2016-12-29 00:00:00', 'adf', 'adf', '2016-12-29 00:00:00', 'http://betaprojex.com/Clinical2/uploads/bio_recycle/bio_recycle_record_14830784641483078464.docx', '', '2016-12-30 06:14:24', 11, 12, '', 0, '{  "name" : { "is_checked": 0, "comments": "" },  "quantity": { "is_checked": 0, "comments": "" },  "sending_party": { "is_checked": 0, "comments": "" },  "sending_operator": { "is_checked": 0, "comments": "" },  "sending_date": { "is_checked": 0, "comments": "" },  "shipper": { "is_checked": 0, "comments": "" }, "shipper_operator" : { "is_checked": 0, "comments": "" }, "shipper_date" : { "is_checked": 0, "comments": "" }, "recycler" : { "is_checked": 0, "comments": "" }, "recycler_operator" : { "is_checked": 0, "comments": "" }, "recycler_date" : { "is_checked": 0, "comments": "" }, "recycle_record" : { "is_checked": 0, "comments": "" }, "bio_desc" : { "is_checked": 0, "comments": "" } }	'),
(6, 'saliva', 'test', 't', 't', '2017-02-10 00:00:00', 't', 't', '2017-02-10 00:00:00', 't', 't', '2017-02-10 00:00:00', 'http://localhost/clinical/uploads/bio_recycle/bio_recycle_record_14867175911486717591.jpg', 'taest', '2017-02-10 09:06:31', 11, 27, 'zc', 0, '{"name":{"is_checked":"1","comments":"zzz"},"quantity":{"is_checked":"1","comments":""},"sending_party":{"is_checked":"-1","comments":""},"sending_operator":{"is_checked":"-1","comments":"d"},"sending_date":{"is_checked":"-1","comments":"ewq"},"shipper":{"is_checked":"1","comments":"qq"},"shipper_operator":{"is_checked":"-1","comments":"wqe"},"shipper_date":{"is_checked":"1","comments":"1"},"recycler":{"is_checked":"-1","comments":"dsa"},"recycler_operator":{"is_checked":"-1","comments":"qweqwe"},"recycler_date":{"is_checked":"1","comments":"dsa"},"recycle_record":{"is_checked":"-1","comments":"z"},"bio_desc":{"is_checked":"-1","comments":"ee"}}');

-- --------------------------------------------------------

--
-- Table structure for table `bio_samples`
--

CREATE TABLE `bio_samples` (
  `bio_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'blood types?',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` varchar(255) NOT NULL COMMENT 'a.k.a. sample size?',
  `collect_start` datetime NOT NULL,
  `collect_end` datetime NOT NULL,
  `collect_record` text NOT NULL COMMENT 'file url',
  `process_record` text NOT NULL COMMENT 'file url',
  `description` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL COMMENT 'role id of uploader',
  `remarks` text NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0',
  `dbl_chk_payload` varchar(21000) NOT NULL DEFAULT '{   "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "collect_start": { "is_checked": 0, "comments": "" }, "collect_end": { "is_checked": 0, "comments": "" }, "collect_record": { "is_checked": 0, "comments": "" }, "description" : { "is_checked": 0, "comments": "" } }' COMMENT '0 - unchecked, -1 - checked/no pass, 1 - checked/pass'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bio_samples`
--

INSERT INTO `bio_samples` (`bio_id`, `name`, `date_created`, `quantity`, `collect_start`, `collect_end`, `collect_record`, `process_record`, `description`, `project_id`, `role_id`, `remarks`, `databank`, `dbl_chk_payload`) VALUES
(2, 'urine', '2017-02-08 11:50:31', 'test', '2017-02-03 00:00:00', '2017-02-04 00:00:00', 'http://betaprojex.com/clinical/uploads/bio_samples/bio_collect_record_14865546311486554631.jpg', '', 'asd', 27, 11, '', 0, '{   "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "collect_start": { "is_checked": 0, "comments": "" }, "collect_end": { "is_checked": 0, "comments": "" }, "collect_record": { "is_checked": 0, "comments": "" }, "description" : { "is_checked": 0, "comments": "" } }'),
(3, 'blood_serum', '2017-02-08 11:51:44', 'test more', '2017-02-01 00:00:00', '2017-02-02 00:00:00', 'http://localhost/clinical/uploads/bio_samples/bio_collect_record_14866335461486633546.jpg', '', 'not ok', 27, 11, '', 0, '{"name":{"is_checked":"1","comments":""},"quantity":{"is_checked":0,"comments":""},"collect_start":{"is_checked":"-1","comments":"test no"},"collect_end":{"is_checked":0,"comments":""},"collect_record":{"is_checked":0,"comments":""},"description":{"is_checked":0,"comments":""}}'),
(4, 'urine', '2017-02-08 12:33:45', 'Nope', '2017-02-20 00:00:00', '2017-02-14 00:00:00', 'http://betaprojex.com/clinical/uploads/bio_samples/bio_collect_record_14865572251486557225.png', '', 'aksjdhakjsdhasd', 28, 11, '', 0, '{   "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "collect_start": { "is_checked": 0, "comments": "" }, "collect_end": { "is_checked": 0, "comments": "" }, "collect_record": { "is_checked": 0, "comments": "" }, "description" : { "is_checked": 0, "comments": "" } }'),
(5, 'urine', '2017-02-09 10:00:29', 'asd', '2017-02-09 00:00:00', '2017-02-09 00:00:00', '', '', 'asd', 27, 11, '', 0, '{   "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "collect_start": { "is_checked": 0, "comments": "" }, "collect_end": { "is_checked": 0, "comments": "" }, "collect_record": { "is_checked": 0, "comments": "" }, "description" : { "is_checked": 0, "comments": "" } }'),
(6, 'saliva', '2017-02-09 10:01:21', 'asd', '2017-02-09 00:00:00', '2017-02-09 00:00:00', '', '', 'asd', 27, 11, '', 0, '{"name":{"is_checked":0,"comments":""},"quantity":{"is_checked":0,"comments":""},"collect_start":{"is_checked":0,"comments":""},"collect_end":{"is_checked":0,"comments":""},"collect_record":{"is_checked":"-1","comments":"uplaoad sm data!!!! zzzz"},"description":{"is_checked":"-1","comments":"changedesc zzz"}}'),
(7, 'saliva', '2017-02-09 10:15:16', 'z', '2017-02-09 00:00:00', '2017-02-09 00:00:00', 'http://localhost/clinical/uploads/bio_samples/bio_collect_record_14866353161486635316.jpg', '', 'z', 27, 11, '', 0, '{   "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "collect_start": { "is_checked": 0, "comments": "" }, "collect_end": { "is_checked": 0, "comments": "" }, "collect_record": { "is_checked": 0, "comments": "" }, "description" : { "is_checked": 0, "comments": "" } }'),
(8, 'dung', '2017-02-09 10:21:57', 'z', '2017-02-09 00:00:00', '2017-02-09 00:00:00', 'http://localhost/clinical/uploads/bio_samples/bio_collect_record_14866357171486635717.png', '', 'dada', 27, 11, '', 0, '{   "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "collect_start": { "is_checked": 0, "comments": "" }, "collect_end": { "is_checked": 0, "comments": "" }, "collect_record": { "is_checked": 0, "comments": "" }, "description" : { "is_checked": 0, "comments": "" } }'),
(9, 'blood_serum', '2017-02-10 03:21:53', '55', '2017-02-02 00:00:00', '2017-02-10 00:00:00', 'http://localhost/clinical/uploads/bio_samples/bio_collect_record_14866969131486696913.jpg', '', '5511', 27, 11, '', 0, '{   "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "collect_start": { "is_checked": 0, "comments": "" }, "collect_end": { "is_checked": 0, "comments": "" }, "collect_record": { "is_checked": 0, "comments": "" }, "description" : { "is_checked": 0, "comments": "" } }'),
(10, 'saliva', '2017-02-10 03:29:43', '3333333xz', '2017-02-10 00:00:00', '2017-02-10 00:00:00', 'http://localhost/clinical/uploads/bio_samples/bio_collect_record_14867088461486708846.jpg', '', '3333333xz', 27, 11, '', 0, '{"name":{"is_checked":"-1","comments":"fa"},"quantity":{"is_checked":"1","comments":"z"},"collect_start":{"is_checked":"-1","comments":"ye"},"collect_end":{"is_checked":"-1","comments":"c"},"collect_record":{"is_checked":"-1","comments":"hmm"},"description":{"is_checked":"1","comments":"k"}}');

-- --------------------------------------------------------

--
-- Table structure for table `bio_save`
--

CREATE TABLE `bio_save` (
  `bio_save_id` int(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `quantity` varchar(225) NOT NULL,
  `collect_start` datetime NOT NULL,
  `collect_end` datetime NOT NULL,
  `address` varchar(225) NOT NULL,
  `temp` varchar(150) NOT NULL,
  `humidity` varchar(150) NOT NULL,
  `record` varchar(225) NOT NULL,
  `bio_desc` varchar(300) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_id` int(11) NOT NULL,
  `remarks` varchar(200) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0',
  `dbl_chk_payload` varchar(21000) NOT NULL DEFAULT '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "collect_start": { "is_checked": 0, "comments": "" }, "collect_end": { "is_checked": 0, "comments": "" }, "record": { "is_checked": 0, "comments": "" },"bio_desc" : { "is_checked": 0, "comments": "" }  ,"address" : { "is_checked": 0, "comments": "" } ,"temp" : { "is_checked": 0, "comments": "" } ,"humidity" : { "is_checked": 0, "comments": "" }  }	' COMMENT '0 - unchecked, -1 - checked/no pass, 1 - checked/pass',
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bio_save`
--

INSERT INTO `bio_save` (`bio_save_id`, `name`, `quantity`, `collect_start`, `collect_end`, `address`, `temp`, `humidity`, `record`, `bio_desc`, `date_created`, `role_id`, `remarks`, `databank`, `dbl_chk_payload`, `project_id`) VALUES
(8, 'dung', '222', '2017-02-21 00:00:00', '2017-02-27 00:00:00', '2', '2', '2', 'http://localhost/clinical/uploads/bio_save/bio_save_record_14867071111486707111.jpg', '2', '2017-02-10 06:11:51', 11, '', 0, '{"name":{"is_checked":"1","comments":"qweqwe"},"quantity":{"is_checked":"-1","comments":"asdas"},"collect_start":{"is_checked":"1","comments":"z"},"collect_end":{"is_checked":"-1","comments":"c"},"record":{"is_checked":"1","comments":""},"bio_desc":{"is_checked":"1","comments":""},"address":{"is_checked":"-1","comments":"2"},"temp":{"is_checked":"1","comments":""},"humidity":{"is_checked":"1","comments":""}}', 27);

-- --------------------------------------------------------

--
-- Table structure for table `bio_transport`
--

CREATE TABLE `bio_transport` (
  `transport_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'blood types?',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` varchar(255) NOT NULL COMMENT 'a.k.a. sample size?',
  `transport_start` datetime NOT NULL,
  `transport_end` datetime NOT NULL,
  `process_record` text NOT NULL COMMENT 'file url',
  `description` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL COMMENT 'role id of uploader',
  `remarks` text NOT NULL,
  `transport_company` varchar(255) NOT NULL,
  `transport_temp` varchar(255) NOT NULL,
  `transport_humidity` varchar(255) NOT NULL,
  `dbl_chk_payload` varchar(21000) NOT NULL DEFAULT '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "transport_start": { "is_checked": 0, "comments": "" }, "transport_end": { "is_checked": 0, "comments": "" }, "process_record": { "is_checked": 0, "comments": "" },"description" : { "is_checked": 0, "comments": "" }  ,"transport_company" : { "is_checked": 0, "comments": "" } ,"transport_temp" : { "is_checked": 0, "comments": "" } ,"transport_humidity" : { "is_checked": 0, "comments": "" }  }' COMMENT '0 - unchecked, -1 - checked/no pass, 1 - checked/pass',
  `databank` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bio_transport`
--

INSERT INTO `bio_transport` (`transport_id`, `name`, `date_created`, `quantity`, `transport_start`, `transport_end`, `process_record`, `description`, `project_id`, `role_id`, `remarks`, `transport_company`, `transport_temp`, `transport_humidity`, `dbl_chk_payload`, `databank`) VALUES
(1, 'whole_blood', '2016-12-28 01:47:02', '1', '2016-12-28 00:00:00', '2016-12-27 00:00:00', 'http://betaprojex.com/Clinical2/uploads/bio_transport/1482889621.png', '1', 5, 11, '', '1', '1', '1', '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "transport_start": { "is_checked": 0, "comments": "" }, "transport_end": { "is_checked": 0, "comments": "" }, "process_record": { "is_checked": 0, "comments": "" },"description" : { "is_checked": 0, "comments": "" }  ,"transport_company" : { "is_checked": 0, "comments": "" } ,"transport_temp" : { "is_checked": 0, "comments": "" } ,"transport_humidity" : { "is_checked": 0, "comments": "" }  }', 0),
(2, 'whole_blood', '2016-12-28 01:47:26', '1', '2016-12-28 00:00:00', '2016-12-28 00:00:00', 'http://betaprojex.com/Clinical2/uploads/bio_transport/1482889646.png', '1', 5, 11, '', '1', '1', '1', '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "transport_start": { "is_checked": 0, "comments": "" }, "transport_end": { "is_checked": 0, "comments": "" }, "process_record": { "is_checked": 0, "comments": "" },"description" : { "is_checked": 0, "comments": "" }  ,"transport_company" : { "is_checked": 0, "comments": "" } ,"transport_temp" : { "is_checked": 0, "comments": "" } ,"transport_humidity" : { "is_checked": 0, "comments": "" }  }', 0),
(3, 'blood_plasma', '2016-12-28 09:16:48', 'a', '2016-12-01 00:00:00', '2016-12-02 00:00:00', 'http://betaprojex.com/Clinical2/uploads/bio_transport/1482916608.txt', 'a', 7, 11, '', 'a', 'a', 'a', '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "transport_start": { "is_checked": 0, "comments": "" }, "transport_end": { "is_checked": 0, "comments": "" }, "process_record": { "is_checked": 0, "comments": "" },"description" : { "is_checked": 0, "comments": "" }  ,"transport_company" : { "is_checked": 0, "comments": "" } ,"transport_temp" : { "is_checked": 0, "comments": "" } ,"transport_humidity" : { "is_checked": 0, "comments": "" }  }', 0),
(4, 'blood_serum', '2016-12-28 09:17:06', 'b', '2016-12-28 00:00:00', '2016-12-28 00:00:00', 'http://betaprojex.com/Clinical2/uploads/bio_transport/1482916626.txt', 'b', 7, 11, '', 'b', 'b', 'b', '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "transport_start": { "is_checked": 0, "comments": "" }, "transport_end": { "is_checked": 0, "comments": "" }, "process_record": { "is_checked": 0, "comments": "" },"description" : { "is_checked": 0, "comments": "" }  ,"transport_company" : { "is_checked": 0, "comments": "" } ,"transport_temp" : { "is_checked": 0, "comments": "" } ,"transport_humidity" : { "is_checked": 0, "comments": "" }  }', 0),
(5, 'blood_plasma', '2016-12-30 06:13:19', 'adf', '2016-12-07 00:00:00', '2016-12-16 00:00:00', 'http://betaprojex.com/Clinical2/uploads/bio_transport/1483078399.docx', '', 12, 11, '', 'adf', 'adf', 'adf', '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "transport_start": { "is_checked": 0, "comments": "" }, "transport_end": { "is_checked": 0, "comments": "" }, "process_record": { "is_checked": 0, "comments": "" },"description" : { "is_checked": 0, "comments": "" }  ,"transport_company" : { "is_checked": 0, "comments": "" } ,"transport_temp" : { "is_checked": 0, "comments": "" } ,"transport_humidity" : { "is_checked": 0, "comments": "" }  }', 0),
(6, 'blood_plasma', '2017-01-12 03:05:29', 'ttt', '2016-12-28 00:00:00', '2017-01-04 00:00:00', 'http://betaprojex.com/Clinical2/uploads/bio_transport/1484190329.docx', 'ttt', 27, 11, '', 't', '34', '34', '{"name":{"is_checked":"-1","comments":"sda"},"quantity":{"is_checked":"1","comments":"qwe"},"transport_start":{"is_checked":"1","comments":"x"},"transport_end":{"is_checked":"-1","comments":"dwq"},"process_record":{"is_checked":"1","comments":"z"},"description":{"is_checked":"-1","comments":"ewqwe"},"transport_company":{"is_checked":"1","comments":"cccc"},"transport_temp":{"is_checked":"-1","comments":"qweqwe"},"transport_humidity":{"is_checked":"1","comments":"1"}}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`dept_id`, `dept_name`) VALUES
(1, 'Yodo'),
(2, 'Abatz'),
(3, 'Podcat'),
(4, 'Jaxspan'),
(5, 'Mita'),
(6, 'Skynoodle'),
(7, 'Nlounge'),
(8, 'Podcat'),
(9, 'Livepath'),
(10, 'Skibox'),
(11, 'Photobug'),
(12, 'Chatterpoint'),
(13, 'Feedfish'),
(14, 'Zooveo'),
(15, 'Browsetype'),
(16, 'Ozu'),
(17, 'Dynabox'),
(18, 'Eamia'),
(19, 'Skiptube'),
(20, 'Lajo');

-- --------------------------------------------------------

--
-- Table structure for table `drugs`
--

CREATE TABLE `drugs` (
  `drug_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `trade_name` varchar(250) NOT NULL,
  `chemical_name` varchar(250) NOT NULL,
  `english_name` varchar(250) NOT NULL,
  `drug_type` varchar(250) NOT NULL,
  `lot_number` varchar(250) NOT NULL,
  `specification` varchar(250) NOT NULL,
  `validity_from` date NOT NULL,
  `validity_to` date NOT NULL,
  `inspection` varchar(250) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0',
  `factory` varchar(250) NOT NULL,
  `provider` varchar(250) NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drugs`
--

INSERT INTO `drugs` (`drug_id`, `project_id`, `trade_name`, `chemical_name`, `english_name`, `drug_type`, `lot_number`, `specification`, `validity_from`, `validity_to`, `inspection`, `databank`, `factory`, `provider`, `added_by`, `date_added`, `updated_by`, `date_updated`) VALUES
(1, 1, 'aåœ°æ–¹', 'aåœ°æ–¹a', 'daå‘', 'control_drug', 'adfa', 'df', '2016-12-22', '2016-12-22', 'adfa', 0, 'dfa', 'df', 10, '2016-12-22', 0, '0000-00-00'),
(2, 3, 'é˜¿é“å¤«', 'ADf', 'f12', 'drug_combination', 'é˜¿é“å¤«', 'aåœ°æ–¹', '2016-12-21', '2016-12-28', 'é˜¿é“å¤«a', 0, 'é˜¿é“å¤«', 'é˜¿é“å¤«', 10, '2016-12-23', 0, '0000-00-00'),
(3, 5, 'hahaha', 'df', 'adf', 'drug_combination', 'adf', 'adf', '2016-12-14', '2016-12-29', 'adf', 0, 'adf', 'adf', 10, '2016-12-25', 0, '0000-00-00'),
(4, 7, 'a', 'a', 'a', 'tested_drug', 'a', 'a', '2016-12-01', '2016-12-02', '', 0, 'a', 'a', 10, '2016-12-28', 0, '0000-00-00'),
(5, 7, 'b', 'b', 'b', 'drug_combination', 'b', 'b', '2016-12-02', '2016-12-03', '', 0, 'b', 'b', 10, '2016-12-28', 0, '0000-00-00'),
(6, 7, 'c', 'c', 'c', 'control_drug', 'c', 'c', '2016-12-03', '2016-12-04', '', 0, 'c', 'c', 10, '2016-12-28', 0, '0000-00-00'),
(7, 7, '1', '1', '1', 'tested_drug', '1', '1', '2016-12-29', '2016-12-29', '', 0, '1', '1', 10, '2016-12-29', 0, '0000-00-00'),
(8, 12, 'é˜¿é“å¤«', 'é˜¿é“å¤«', 'é˜¿é“å¤«', 'tested_drug', 'é˜¿é“å¤«', 'é˜¿é“å¤«', '2016-12-21', '2017-01-05', 'http://betaprojex.com/Clinical2/uploads/drug_records/drug_record_14830780801483078080.docx', 0, 'é˜¿é“å¤«a', 'adå‘', 10, '2016-12-30', 0, '0000-00-00'),
(9, 13, 'abc', '1', '1', 'drug_combination', '11', '1', '2016-12-21', '2016-12-31', '', 0, '1', '11', 10, '2016-12-30', 0, '0000-00-00'),
(10, 13, 'bcd', '1', '1', 'control_drug', '1', '1', '2016-12-30', '2017-01-12', '', 0, '1', '1', 10, '2016-12-30', 0, '0000-00-00'),
(11, 27, 'æ ¼åŽåœ', 'äºŒç”²åŒèƒ', 'Metformin hydrochloride', 'tested_drug', '15178281393', 'ç‰‡å‰‚', '2017-01-31', '2019-09-17', 'http://betaprojex.com/Clinical2/uploads/drug_records/drug_record_14840461781484046178.docx', 0, 'æ·±åœ³å¸‚ä¸­è”åˆ¶è¯æœ‰é™å…¬å¸', 'æ·±åœ³å¸‚ä¸­è”åˆ¶è¯æœ‰é™å…¬å¸', 10, '2017-01-10', 0, '0000-00-00'),
(12, 27, 'å•Šéƒ½æ”¾åˆ°', 'AD', 'aåœ°æ–¹', 'drug_combination', 'é˜¿é“å¤«', 'é˜¿é“å¤«', '2017-01-25', '2017-01-26', 'http://betaprojex.com/Clinical2/uploads/drug_records/drug_record_14847285071484728507.docx', 0, 'é˜¿é“å¤«', 'aåœ°æ–¹', 10, '2017-01-18', 0, '0000-00-00'),
(13, 27, 'æ˜¯éžè§‚', 'é˜¿é“å¤«a', 'aåœ°æ–¹', 'tested_drug', 'é˜¿é“å¤«a', 'aåœ°æ–¹', '2017-01-26', '2017-01-31', 'http://betaprojex.com/Clinical2/uploads/drug_records/drug_record_14847285761484728576.docx', 0, 'é˜¿é“å¤«', 'é˜¿é“å¤«', 10, '2017-01-18', 0, '0000-00-00'),
(14, 27, 'aåœ°æ–¹', 'aåœ°æ–¹a', 'aåœ°æ–¹', 'tested_drug', 'é˜¿é“å¤«a', 'é˜¿é“å¤«', '2017-01-26', '2017-01-27', 'http://betaprojex.com/Clinical2/uploads/drug_records/drug_record_14847286481484728648.docx', 0, 'é˜¿é“å¤«a', 'aåœ°æ–¹', 10, '2017-01-18', 0, '0000-00-00'),
(15, 27, 'çš„', 'SDs', 'sçš„', 'tested_drug', 'SDs', 'sçš„', '2017-01-19', '2017-01-26', '', 0, 'sçš„', 'sçš„', 10, '2017-01-18', 0, '0000-00-00'),
(16, 27, '11', '21', '2', 'drug_combination', '2', '12', '2017-01-25', '2017-01-31', '', 0, '21', '2', 10, '2017-01-18', 0, '0000-00-00'),
(17, 27, '1231', '123', 'Drug eng', 'control_drug', '321', '321', '2017-01-18', '2017-01-19', 'http://betaprojex.com/Clinical2/uploads/drug_records/drug_record_14847303631484730363.png', 0, '123', '123', 10, '2017-01-18', 0, '0000-00-00'),
(18, 27, '999', '9', '999', 'control_drug', '9', '9', '2017-01-18', '2017-01-18', '', 0, '9', '9', 10, '2017-01-18', 0, '0000-00-00'),
(19, 27, 'test', '1', '1', 'tested_drug', '1', '1', '2017-01-19', '2017-01-19', '', 0, '1', '1', 10, '2017-01-19', 0, '0000-00-00'),
(20, 27, 'a', 'a', 'a', 'control_drug', 'a', 'a', '2017-01-19', '2017-01-19', '', 0, 'a', 'a', 10, '2017-01-19', 0, '0000-00-00'),
(21, 27, 'b', 'b', 'b', 'control_drug', 'b', 'b', '2017-01-19', '2017-01-19', '', 0, 'b', 'b', 10, '2017-01-19', 0, '0000-00-00'),
(22, 28, 'as', 'kjh', 'kjhk', 'drug_combination', 'kh', 'k', '2017-02-08', '2017-02-08', '', 0, 'khk', 'h', 10, '2017-02-08', 0, '0000-00-00'),
(23, 28, 'Databank Test', 'Databank Test', 'Databank Test', 'tested_drug', 'Databank Test', 'Databank Test', '2017-02-28', '2017-07-28', 'http://betaprojex.com/clinical/uploads/drug_records/drug_record_14865553501486555350.png', 1, 'Databank Test', 'Databank Test', 10, '2017-02-08', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `drug_records`
--

CREATE TABLE `drug_records` (
  `drug_record_id` int(11) NOT NULL,
  `drug_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit` varchar(50) NOT NULL,
  `record_type` varchar(15) NOT NULL,
  `date` date NOT NULL,
  `actor1` varchar(150) NOT NULL,
  `position1` varchar(20) NOT NULL,
  `actor2` varchar(150) NOT NULL,
  `position2` varchar(20) NOT NULL,
  `receipt` varchar(350) NOT NULL,
  `description` text NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` date NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drug_records`
--

INSERT INTO `drug_records` (`drug_record_id`, `drug_id`, `quantity`, `unit`, `record_type`, `date`, `actor1`, `position1`, `actor2`, `position2`, `receipt`, `description`, `added_by`, `date_added`, `updated_by`, `date_updated`, `remarks`) VALUES
(1, 4, 0, 'a', 'choose', '2016-12-28', 'a', 'Applicant', 'a', 'Applicant', '', 'a', 10, '2016-12-28', 0, '0000-00-00', ''),
(2, 5, 0, 'b', 'put_in_storage', '2016-12-28', 'b', 'CRO', 'b', 'CRO', '', 'b', 10, '2016-12-28', 0, '0000-00-00', ''),
(3, 6, 0, 'c', 'put_out_storage', '2016-12-28', 'c', 'CRO', 'c', 'CRO', '', 'c', 10, '2016-12-28', 0, '0000-00-00', ''),
(4, 5, 1, '1', 'put_in_storage', '2016-12-10', '1', 'Medicine controller', '1', 'Medicine controller', '', '1', 10, '2016-12-28', 0, '0000-00-00', ''),
(5, 5, 0, 'z', 'waste', '2016-12-09', 'z', 'Research nurse', 'z', 'Research nurse', '', 'z', 10, '2016-12-28', 0, '0000-00-00', ''),
(6, 5, 0, '1', 'put_out_storage', '2016-12-14', '1', 'Applicant', '1', 'Research nurse', '', 'zxczxc', 10, '2016-12-28', 0, '0000-00-00', ''),
(7, 5, 0, 'zxc', 'put_in_storage', '2016-12-01', 'z', 'Applicant', 'z', 'Research nurse', '', 'zxc', 10, '2016-12-28', 0, '0000-00-00', ''),
(8, 4, 0, 'q', 'put_out_storage', '2016-12-28', 'q', 'Medicine controller', 'q', 'Medicine controller', '', 'q', 10, '2016-12-28', 0, '0000-00-00', ''),
(9, 7, 1, '1', 'choose', '2016-12-29', '1', 'Applicant', '1', 'Applicant', '', '1', 10, '2016-12-29', 0, '0000-00-00', ''),
(10, 6, 2, '22', 'put_in_storage', '2016-12-29', '2', 'CRO', '2', 'CRO', '', '2', 10, '2016-12-29', 0, '0000-00-00', ''),
(11, 8, 34, 'å…‹', 'put_in_storage', '2016-12-23', 'è€ƒè™‘åŠ å¯ä¹', 'CRO', 'é˜¿é“å¤«', 'Applicant', '', 'é˜¿é“å¤«', 10, '2016-12-30', 0, '0000-00-00', ''),
(12, 9, 1, '11', 'put_in_storage', '2016-12-30', '1', 'CRO', '1', 'Medicine controller', '', '11', 10, '2016-12-30', 0, '0000-00-00', 'å·²å¤æ ¸'),
(13, 10, 12, 'é¢—', 'put_in_storage', '2017-01-04', 'é˜¿é“å¤«', 'CRO', 'é˜¿é“å¤«', 'Applicant', '', 'é˜¿é“å¤«', 10, '2017-01-07', 0, '0000-00-00', ''),
(14, 11, 100, 'ç›’', 'put_in_storage', '2017-01-12', 'å…³å»ºæ–‡', 'Medicine controller', 'æœ±è‰ºæ€', 'Research nurse', '', 'æ— ', 10, '2017-01-10', 0, '0000-00-00', ''),
(15, 11, 2, 'ç²’', 'put_in_storage', '2017-01-05', 'æŽæ˜Ž', 'Applicant', 'æ¢¨èŠ±', 'CRO', '', 'å‚¨å­˜', 10, '2017-01-12', 0, '0000-00-00', ''),
(16, 22, 123, '123', 'put_in_storage', '2017-02-28', '123', 'CRO', '123', 'CRO', '', '123123123', 10, '2017-02-08', 0, '0000-00-00', '');

-- --------------------------------------------------------

--
-- Table structure for table `drug_record_receipts`
--

CREATE TABLE `drug_record_receipts` (
  `receipt_id` int(11) NOT NULL,
  `drug_record_id` int(11) NOT NULL,
  `receipt_file` text NOT NULL,
  `receipt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drug_record_receipts`
--

INSERT INTO `drug_record_receipts` (`receipt_id`, `drug_record_id`, `receipt_file`, `receipt`) VALUES
(1, 4, '4--&--14829158681797301903.docx', 'PAU.docx'),
(2, 5, '5--&--1482916145304540011.docx', 'PAU.docx'),
(3, 6, '6--&--1482918512631517921.docx', 'PAU.docx'),
(4, 7, '7--&--1482920000572026763.docx', 'PAU.docx'),
(5, 8, '8--&--1482920192420913098.docx', 'PAU.docx'),
(6, 11, '11--&--14830781402089084170.docx', 'Progresstranslation.docx'),
(7, 12, '12--&--14830897621992289131.docx', 'template3.docx'),
(8, 14, '14--&--1484046328820822165.docx', '.docx'),
(9, 15, '15--&--14841908411736391183.docx', '.docx'),
(10, 16, '16--&--1486540615486785082.pdf', 'June10RevisedUXJune6RSLEdits.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `inspections`
--

CREATE TABLE `inspections` (
  `inspection_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `report_date` date NOT NULL,
  `role` varchar(17) NOT NULL,
  `type` varchar(17) NOT NULL,
  `report_description` text NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inspections`
--

INSERT INTO `inspections` (`inspection_id`, `project_id`, `report_date`, `role`, `type`, `report_description`, `added_by`, `date_added`, `updated_by`, `date_updated`) VALUES
(1, 1, '2016-12-22', '6', 'inspector', 'é˜¿é“å¤«', 6, '2016-12-22', 0, '0000-00-00'),
(2, 1, '2016-12-23', '6', 'on_site_inspector', '', 6, '2016-12-23', 0, '0000-00-00'),
(3, 1, '2016-12-23', '6', 'inspector', '', 6, '2016-12-23', 0, '0000-00-00'),
(4, 1, '2016-12-23', '6', 'inspector', '', 6, '2016-12-23', 0, '0000-00-00'),
(5, 1, '2016-12-23', '6', 'inspector', '1', 6, '2016-12-23', 0, '0000-00-00'),
(6, 1, '2016-11-29', '3', 'inspector', 'é˜¿é“å¤«', 3, '2016-12-23', 0, '0000-00-00'),
(7, 1, '2016-11-29', '3', 'inspector', 'é˜¿é“å¤«', 3, '2016-12-23', 0, '0000-00-00'),
(8, 3, '2016-12-07', '6', 'on_site_inspector', 'adf', 6, '2016-12-23', 0, '0000-00-00'),
(9, 5, '2016-12-14', '6', 'inspector', 'adf ', 6, '2016-12-25', 0, '0000-00-00'),
(10, 5, '2016-12-14', '1', 'on_site_inspector', 'kdjaf', 1, '2016-12-27', 0, '0000-00-00'),
(11, 3, '2016-12-01', '6', 'inspector', 'asad', 6, '2016-12-27', 0, '0000-00-00'),
(12, 7, '2016-12-28', '3', 'inspector', 'a', 3, '2016-12-28', 0, '0000-00-00'),
(13, 7, '2016-12-28', '3', 'on_site_inspector', 'b', 3, '2016-12-28', 0, '0000-00-00'),
(14, 7, '2016-12-28', '6', 'on_site_inspector', 'c', 6, '2016-12-28', 0, '0000-00-00'),
(15, 7, '2016-12-28', '6', 'on_site_inspector', 'd', 6, '2016-12-28', 0, '0000-00-00'),
(16, 7, '2016-12-01', '1', 'inspector', 'a', 1, '2016-12-28', 0, '0000-00-00'),
(17, 7, '2016-12-29', '6', 'inspector', 'test', 6, '2016-12-29', 0, '0000-00-00'),
(18, 7, '2016-12-01', '6', 'inspector', 'testing', 6, '2016-12-29', 0, '0000-00-00'),
(19, 7, '2016-12-29', '6', 'inspector', '1', 6, '2016-12-29', 0, '0000-00-00'),
(20, 12, '2016-12-07', '6', 'inspector', 'adf', 6, '2016-12-30', 0, '0000-00-00'),
(21, 13, '2016-12-30', '6', 'inspector', '1', 6, '2016-12-30', 0, '0000-00-00'),
(22, 13, '2016-12-14', '6', 'inspector', '1', 6, '2016-12-30', 0, '0000-00-00'),
(23, 13, '2016-12-30', '3', 'inspector', '11', 3, '2016-12-30', 0, '0000-00-00'),
(24, 13, '2016-12-30', '1', 'inspector', '11', 1, '2016-12-30', 0, '0000-00-00'),
(25, 13, '2016-12-30', '6', 'on_site_inspector', '1', 6, '2016-12-30', 0, '0000-00-00'),
(26, 13, '2017-01-04', '6', 'inspector', 'adf', 6, '2017-01-07', 0, '0000-00-00'),
(27, 13, '2017-01-11', '6', 'inspector', 'fg', 6, '2017-01-10', 0, '0000-00-00'),
(28, 13, '2017-01-11', '6', 'inspector', 'fg', 6, '2017-01-10', 0, '0000-00-00'),
(29, 13, '2017-01-11', '6', 'inspector', 'fg', 6, '2017-01-10', 0, '0000-00-00'),
(30, 27, '2017-01-11', '6', 'inspector', 'SAEæœªä¸ŠæŠ¥', 6, '2017-01-12', 0, '0000-00-00'),
(31, 27, '2017-01-11', '6', 'inspector', 'SAEæœªä¸ŠæŠ¥', 6, '2017-01-12', 0, '0000-00-00'),
(32, 27, '2017-01-06', '6', 'on_site_inspector', 'æ•´ä½“æƒ…å†µæ­£å¸¸', 6, '2017-01-12', 0, '0000-00-00'),
(33, 27, '2017-01-19', '6', 'inspector', '', 6, '2017-01-22', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_uploads`
--

CREATE TABLE `inspection_uploads` (
  `inspection_upload_id` int(11) NOT NULL,
  `inspection_id` int(11) NOT NULL,
  `inspection_file` text NOT NULL,
  `inspection` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inspection_uploads`
--

INSERT INTO `inspection_uploads` (`inspection_upload_id`, `inspection_id`, `inspection_file`, `inspection`) VALUES
(1, 1, '1--&--1482401043605608089.docx', 'Progresstranslation.docx'),
(2, 2, '2--&--1482480434699295768.pdf', '11770881184588101.pdf'),
(3, 3, '3--&--1482480965252434986.docx', '1.docx'),
(4, 4, '4--&--14824809651424762057.docx', '1.docx'),
(5, 5, '5--&--14824811071401953431.docx', '1.docx'),
(6, 6, '6--&--1482481272833532260.docx', 'Progresstranslation.docx'),
(7, 7, '7--&--14824812731687208641.docx', 'Progresstranslation.docx'),
(8, 8, '8--&--14824840001726172567.docx', 'Progresstranslation.docx'),
(9, 9, '9--&--1482680631668067380.docx', 'Progresstranslation.docx'),
(10, 10, '10--&--14828056611244380434.docx', 'Progresstranslation.docx'),
(11, 11, '11--&--14828191751342024444.txt', 'test.txt'),
(12, 12, '12--&--1482917862356531958.txt', 'test.txt'),
(13, 13, '13--&--14829178861141087517.txt', 'test.txt'),
(14, 14, '14--&--14829186422010004822.txt', 'test.txt'),
(15, 15, '15--&--1482918980591685136.txt', 'test.txt'),
(16, 16, '16--&--1482919046624169038.txt', 'test.txt'),
(17, 17, '17--&--1482982948414168267.txt', 'test.txt'),
(18, 18, '18--&--14829880331370900147.txt', 'test.txt'),
(19, 19, '19--&--14829908361729595542.txt', 'test.txt'),
(20, 20, '20--&--14830787943547401.docx', 'Progresstranslation.docx'),
(21, 21, '21--&--1483091645850140868.docx', 'template3.docx'),
(22, 22, '22--&--14830917881719066813.docx', 'template3.docx'),
(23, 23, '23--&--1483091977808936393.docx', 'template3.docx'),
(24, 24, '24--&--148309204651324776.docx', 'template3.docx'),
(25, 25, '25--&--1483092586885685430.docx', 'template3.docx'),
(26, 26, '26--&--14837786212081124461.png', '6.png'),
(27, 27, '27--&--14840202651420429479.docx', '.docx'),
(28, 28, '28--&--1484020267242940778.docx', '.docx'),
(29, 29, '29--&--1484020269404593623.docx', '.docx'),
(30, 30, '30--&--14841879451692634305.docx', '.docx'),
(31, 31, '31--&--14841879561034544476.docx', '.docx'),
(32, 32, '32--&--1484188052428190822.docx', '.docx'),
(33, 33, '33--&--14850598941356047215.docx', '.docx');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `word_id` int(5) NOT NULL,
  `phrase` varchar(150) NOT NULL,
  `english` varchar(150) NOT NULL,
  `chinese` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`word_id`, `phrase`, `english`, `chinese`) VALUES
(1, 'sitename', 'Clinical Trial Management System', '20020,24202,30740,31350,31649,29702,31995,32479'),
(2, 'chinese', 'chinese', '20013,25991'),
(4, 'side_effects', 'Side effects', '19981,33391,20107,20214,47,21453,24212'),
(7, 'search', 'Search', '25628,32034'),
(8, 'research_category', 'Research category', '30740,31350,31867,21035'),
(9, 'status', 'status', '29366,24577'),
(10, 'all', 'All', '20840,37096'),
(11, 'stage_1', 'Stage I', '73,26399'),
(12, 'stage_2', 'Stage II', '73,73,26399'),
(13, 'stage_3', 'Stage III', '73,73,73,26399'),
(14, 'stage_4', 'Stage IV', '73,86,26399'),
(15, 'be_trial', 'BE Trial', '66,69,35797,39564'),
(16, 'clinical_verifications', 'Clinical verifications', '20020,24202,39564,35777'),
(17, 'other', 'Other', '20854,20182'),
(18, 'apply', 'Apply', '31435,39033,30003,35831'),
(19, 'initiation', 'Initiation', '31435,39033,36890,36807'),
(20, 'ethics_pass', 'Ethics pass', '20262,29702,36890,36807'),
(21, 'trial_begin', 'Trial begin', '35797,39564,24320,22987'),
(24, 'number', 'NO.', '24207,21495'),
(25, 'project_number', 'Project number', '39033,30446,32534,21495'),
(26, 'project_name', 'Project name', '39033,30446,21517,31216'),
(27, 'applicant', 'Applicant', '30003,21150,32773'),
(28, 'current_progress', 'Current progress', '24403,21069,20107,39033'),
(32, 'date_created', 'Date created', '24403,21069,20107,39033,26102,38388'),
(33, 'operation', 'Operation', '25805,20316'),
(57, 'new_project', 'New Project', '26032,22686,39033,30446'),
(58, 'project_detail', 'Project detail', '39033,30446,35814,24773'),
(59, 'lot_number', 'Lot number', '20020,24202,35797,39564,25209,25991,25991,20214,21495'),
(62, 'trial_category', 'Trial category', '30740,31350,31867,21035'),
(63, 'indications', 'Indications', '36866,24212,30151'),
(64, 'risk_category', 'Risk category', '39118,38505,31867,21035'),
(65, 'low', 'Low', '20302'),
(66, 'mid', 'Mid', '20013'),
(67, 'high', 'High', '39640'),
(68, 'departments', 'Departments', '31185,23460'),
(69, 'pgroup_details', 'Professional group detials', '19987,19994,32452,35814,24773'),
(70, 'professional_group', 'Professional group', '19987,19994,32452'),
(71, 'principal_investigator', 'Principal investigator', '19987,19994,32452,80,73'),
(72, 'pgroup_contact', 'Professional group contact person', '19987,19994,32452,39033,30446,32852,31995,20154'),
(73, 'pgroup_mobile', 'Professional group mobile', '19987,19994,32452,32852,31995,25163,26426,21495'),
(74, 'pgroup_email', 'Professional group email', '19987,19994,32452,101,109,97,105,108'),
(75, 'applicant_details', 'Applicant details', '30003,21150,32773,35814,24773'),
(76, 'applicant_name', 'Applicant name', '30003,21150,32773'),
(77, 'emergency_contact', 'Emergency contact person', '30003,21150,32773,32852,31995,20154'),
(78, 'emergency_mobile', 'Emergency contact mobile', '30003,21150,32773,32852,31995,25163,26426,21495'),
(79, 'emergency_email', 'Emergency email', '30003,21150,32773,101,109,97,105,108'),
(80, 'cro_details', 'CRO details', '67,82,79,35814,24773'),
(81, 'cro_name', 'CRO', '67,82,79'),
(82, 'cro_contact', 'CRO contact person', '67,82,79,32852,31995,20154'),
(83, 'cro_mobile', 'CRO contact mobile', '67,82,79,32852,31995,25163,26426,21495'),
(84, 'other_details', 'Other details', '20854,20182,35814,24773'),
(85, 'research_objectives', 'Research objectives', '30740,31350,30446,30340'),
(86, 'subsidize_type', 'Type of subsidize', '30740,31350,36164,21161,31867,22411'),
(87, 'fully_funded', 'Fully funded', '33719,20840,39069,36164,21161'),
(88, 'partially_funded', 'Partially funded', '33719,37096,20998,36164,21161'),
(89, 'no_subsidize', 'No subsidize', '26080,36164,21161,26041'),
(90, 'remarks', 'Remarks', '22791,27880'),
(93, 'pi', 'PI', '20027,35201,30740,31350,32773'),
(94, 'mc_info', 'Multi-center information', '22810,20013,24515,20449,24687'),
(95, 'organization', 'Organization', '21333,20301,21517,31216'),
(96, 'person_in_charge', 'Person in charge', '36127,36131,20154'),
(98, 'lead_participate', 'Lead/Participate', '29301,22836,47,21442,19982'),
(99, 'submit', 'Submit', '25552,20132'),
(100, 'project_documents', 'Project documents', '39033,30446,36164,26009'),
(101, 'name', 'Name', '21517,31216'),
(105, 'version', 'Version', '29256,26412'),
(107, 'description', 'Description', '25551,36848'),
(108, 'type', 'Type', '31867,22411'),
(110, 'select_file', 'Select file', '36873,25321,25991,20214'),
(112, 'project_plan', 'Project plan', '39033,30446,26041,26696'),
(113, 'uploader', 'Uploader', '19978,20256,20154'),
(116, 'tfo_opinion', 'Testing Facility opinion', '35797,39564,26426,26500,24847,35265'),
(117, 'pi_opinion', 'Principal Investigator opinion', '20027,35201,30740,31350,32773,24847,35265'),
(119, 'ecc_opinion', 'Ethics Committee Chairman opinion', '20262,29702,22996,21592,20250,24847,35265'),
(120, 'role', 'Role', '35282,33394'),
(123, 'upload_report', 'Upload report', '19978,20256,25253,21578'),
(124, 'file', 'File', '25991,20214'),
(129, 'project_information', 'Project Information', '39033,30446,20449,24687'),
(130, 'pre_opinion', 'Peer review expert opinion', '21516,34892,35780,35758,19987,23478,24847,35265'),
(131, 'gender', 'Gender', '24615,21035'),
(132, 'job_title', 'Job title', '32844,31216'),
(133, 'mobile_num', 'Mobile', '30005,35805,21495,30721'),
(134, 'position', 'position', '32844,20301'),
(136, 'job_spec', 'Job specification', '24037,20316,35268,33539'),
(137, 'title', 'Title', '26631,39064'),
(138, 'add_job_spec', 'Add job pecification', '28155,21152,24037,20316,35268,33539'),
(139, 'add', 'Add', '28155,21152'),
(140, 'delete', 'Delete', '21024,38500'),
(141, 'meeting_type', 'Type of meeting', '20250,35758,31867,22411'),
(142, 'date', 'Date', '26085,26399'),
(143, 'meeting_record', 'Add a meeting', '28155,21152,20250,35758,35760,24405'),
(144, 'participants', 'Participants', '21442,20250,20154,21592'),
(145, 'place', 'Place', '22320,22336'),
(146, 'host', 'Host', '20027,25345,20154'),
(147, 'meeting_material', 'Meeting material', '21407,22987,35760,24405,25991,20214'),
(148, 'add_meeting_material', 'Add meeting material', '28155,21152,20250,35758,36164,26009'),
(149, 'meeting_content', 'Meeting content', '20250,35758,20869,23481'),
(150, 'meeting_summary', 'Meeting summary', '20250,35758,24635,32467'),
(151, 'bio_sample_management', 'Biological sample management', '29983,29289,26679,21697,31649,29702'),
(152, 'quantity', 'Quantity', '26679,26412,37327'),
(153, 'add_bio_record', 'Add biological sample record', '28155,21152,29983,29289,26679,26412,35760,24405'),
(155, 'whole_blood', 'Whole blood', '20840,34880'),
(156, 'blood_plasma', 'Blood plasma', '34880,27974'),
(159, 'blood_serum', 'Blood serum', '34880,28165'),
(160, 'urine', 'Urine', '23615,28082'),
(161, 'dung', 'Dung', '31914,20415'),
(162, 'saliva', 'Saliva', '21822,28082'),
(163, 'milk', 'Milk', '20083,27713'),
(164, 'collect_time', 'Collect time', '26679,26412,37319,38598,26102,38388'),
(165, 'collect_record', 'Collect record', '26679,26412,37319,38598,35760,24405'),
(166, 'process_record', 'Process record', '26679,26412,22788,29702,35760,24405'),
(167, 'state', 'State', '30740,31350,29366,24577'),
(168, 'case_num', 'Case number', '30149,21382,21495'),
(169, 'subjects_num', 'Subjects number', '21463,35797,32773,32534,21495'),
(170, 'abbrv', 'Abbreviation', '22995,21517,32553,20889'),
(171, 'id_card_num', 'I.D. card No.', '36523,20221,35777,21495'),
(172, 'id_card_copy', 'I.D. card Copy', '36523,20221,35777,22797,21360,20214'),
(173, 'birth_date', 'Birth date', '29983,26085'),
(174, 'nation', 'Nation', '27665,26063'),
(175, 'country', 'Country', '22269,23478'),
(176, 'province', 'Province', '30465,20221'),
(177, 'address', 'Address', '20303,22336'),
(178, 'native', 'Native place', '31821,36143'),
(179, 'sign_informed_consent', 'Sign informed consent', '26159,21542,31614,32626,30693,24773,21516,24847,20070'),
(180, 'yes', 'Yes', '26377'),
(181, 'no', 'No', '26080'),
(182, 'screen_date', 'Screen date', '31579,36873,26085,26399'),
(183, 'random_date', 'Random date', '38543,26426,26085,26399'),
(184, 'random_num', 'Random number', '38543,26426,30721'),
(188, 'accept_date', 'Accept date', '25509,21463,26085,26399'),
(189, 'reject_date', 'Reject date', '25298,32477,26085,26399'),
(190, 'drop_out_date', 'Drop out date', '21076,38500,26085,26399'),
(191, 'first_visit_date', 'First visit date', '39318,35775,26085,26399'),
(192, 'male', 'Male', '30007'),
(194, 'female', 'Female', '22899'),
(195, 'stage_summary', 'Stage summary', '38454,27573,23567,32467'),
(196, 'time_slot', 'Time slot', '26102,38388,27573'),
(197, 'enroll_amount', 'Enroll amount', '20837,32452,20363,25968'),
(198, 'quit_amount', 'Quit amount', '36864,20986,20363,25968'),
(199, 'finish_amount', 'Finish amount', '23436,25104,20363,25968'),
(201, 'sae', 'SAE', '19981,33391,21453,24212'),
(202, 'amount', 'Amount', '21457,29983,20363,25968'),
(203, 'degree', 'Degree', '20005,37325,31243,24230'),
(204, 'handling_information', 'Handling information', '22788,29702,24773,20917'),
(205, 'volunteer', 'Volunteer', '24535,24895,32773'),
(206, 'married', 'Married', '23130,21542'),
(207, 'blood_type', 'Blood type', '34880,22411'),
(208, 'had_surgery', 'Had surgery', '26159,21542,20570,36807,25163,26415'),
(209, 'family_history', 'Family history', '23478,26063,21490'),
(210, 'heavy_smoking', 'Heavy smoking', '21980,28895'),
(211, 'heavy_drinking', 'Heavy drinking', '21980,37202'),
(212, 'allergies', 'Allergies', '36807,25935,21490'),
(213, 'height', 'Height', '36523,39640'),
(214, 'weight', 'Weight', '20307,37325'),
(215, 'bmi', 'BMI', '20307,37325,25351,25968'),
(216, 'medical_history', 'Medical history', '26082,24448,30149,21490'),
(217, 'progress', 'Progress', '39033,30446,36827,23637'),
(218, 'material', 'Material', '39033,30446,36164,26009'),
(220, 'timeline', 'Timeline', '26102,38388,32447'),
(221, 'no_data', 'No data available', '26080,27861,33719,21462,25968,25454'),
(223, 'add_remarks', 'Add remarks', '28155,21152,22791,27880'),
(224, 'action', 'Action', '25805,20316'),
(225, 'member', 'Member', '39033,30446,25104,21592'),
(226, 'training_meeting', 'Training/Meeting', '22521,35757,47,20250,35758'),
(227, 'drug_management', 'Drug management', '33647,21697,31649,29702'),
(228, 'subjects', 'Subjects', '21463,35797,32773'),
(229, 'report_type', 'Report type', '25253,21578,31867,22411'),
(230, 'report_time', 'Report time', '25253,21578,26102,38388'),
(231, 'hospital', 'Hospital', '21307,30103,26426,26500,21450,19987,19994,21517,31216'),
(232, 'department', 'Department', '30003,25253,21333,20301,21517,31216'),
(233, 'drug_name', 'Drug name', '35797,39564,29992,33647,21517,31216'),
(234, 'chinese_name', 'Chinese name', '20013,25991,21517,31216'),
(235, 'english_name', 'English name', '33521,25991,21517,31216'),
(237, 'drug_information', 'Drug information', '33647,21697,27880,20876,20998,31867,21450,21058,22411'),
(238, 'drug_type', 'Drug type', '33647,21697,20998,31867'),
(239, 'form_of_drug', 'The form of drug', '21058,22411'),
(240, 'register_type', 'Register type', '27880,20876,20998,31867'),
(241, 'clinical_trial', 'Clinical trial', '20020,24202,30740,31350'),
(242, 'subjects_situation', 'Subjects situation', '21463,35797,32773,22522,26412,24773,20917'),
(243, 'initials', 'Initials', '22995,21517,32553,20889'),
(244, 'complications', 'Complications', '21512,24182,30142,30149,21450,27835,30103'),
(245, 'sae_diagnosis', 'SAE medical diagnosis', '83,65,69,30340,21307,23398,35786,26029'),
(246, 'drug_measure', 'Drug measure', '23545,35797,39564,29992,33647,37319,21462,30340,25514,26045'),
(247, 'sae_situation', 'SAE situation', '83,65,69,24773,20917'),
(248, 'sae_drug_relation', 'Relation of SAE and drug', '83,65,69,19982,35797,39564,33647,30340,20851,31995'),
(249, 'saevest', 'SAEvest', '83,65,69,24402,36716'),
(250, 'unblinding_situation', 'Unblinding situation', '30772,30450,24773,20917'),
(251, 'report_internal', 'Report internal', '22269,20869,83,65,69,25253,36947,24773,20917'),
(252, 'report_external', 'Report external', '22269,22806,83,65,69,25253,36947,24773,20917'),
(253, 'handle_detail', 'Handle detail', '83,65,69,21457,29983,21450,22788,29702,30340,35814,24773'),
(256, 'reporter_unit', 'Reporter unit', '25253,21578,21333,20301,21517,31216'),
(257, 'reporter_position', 'Reporter position', '25253,21578,20154,32844,21153'),
(258, 'die', 'Die', '27515,20129'),
(259, 'be_in_hospital', 'Be in hospital', '23548,33268,20303,38498'),
(260, 'disability', 'Disability', '24310,38271,20303,38498,26102,38388'),
(261, 'dysfunction', 'Dysfunction', '20260,27531'),
(262, 'deformity', 'Deformity', '21151,33021,38556,30861'),
(263, 'life_threatening', 'Life threatening', '21361,21450,29983,21629'),
(264, 'severe', 'Severe', '37325,24230'),
(265, 'continuantur_remedia', 'Continuantur remedia', '32487,32493,29992,33647'),
(266, 'reduce_the_dosage', 'Reduce the dosage', '20943,23569,21058,37327'),
(267, 'stop_then_go', 'Stop then go', '33647,29289,26242,20572,21518,21448,24674,22797'),
(269, 'no_detail', 'No detail', '19981,35814'),
(270, 'no_blinding', 'No blinding', '19981,35774,30450'),
(272, 'did_not_unblinding', 'Did not unblinding', '26410,30772,30450'),
(273, 'unblinding', 'Unblinding', '24050,30772,30450'),
(274, 'transference_cure', 'Transference cure', '30151,29366,28040,22833'),
(275, 'symptoms_last', 'Symptoms last', '30151,29366,25345,32493'),
(276, 'certainty', 'Certainty', '32943,23450,26377,20851'),
(277, 'certainty_unconcerned', 'Certainty unconcerned', '32943,23450,26080,20851'),
(278, 'not_determinable', 'Not determinable', '26080,27861,21028,26029'),
(279, 'maybe_concern', 'Maybe concern', '21487,33021,26377,20851'),
(282, 'be_unconcerned', 'Be unconcerned', '21487,33021,26080,20851'),
(283, 'first_report', 'First report', '39318,27425,25253,21578'),
(284, 'follow_up_report', 'Follow-up report', '38543,35775,25253,21578'),
(285, 'sum_up_report', 'Sum-up report', '24635,32467,25253,21578'),
(286, 'traditional_chinese_medicine', 'Traditional Chinese medicine', '20013,33647'),
(287, 'chemistry_drug', 'Chemistry drug', '21270,23398,33647'),
(288, 'cure_medicine', 'Cure medicine', '27835,30103,29992,29983,29289,21046,21697'),
(290, 'prevent_medicine', 'Prevent medicine', '39044,38450,29992,29983,29289,21046,21697'),
(291, 'project', 'Project', '39033,30446'),
(292, 'hospital_mobile', 'Hospital mobile', '21307,38498,30005,35805'),
(293, 'department_mobile', 'Department mobile', '31185,23460,30005,35805'),
(294, 'subject_library', 'Subject library', '21463,35797,32773,24211'),
(463, 'add_member', ' Add member', '28155,21152,25104,21592'),
(464, 'tfo', 'Testing Facility Office', '35797,39564,26426,26500'),
(465, 'pre', 'Peer review expert', '21516,34892,35780,35758,19987,23478'),
(466, 'ethics_committee', 'Ethics Committee', '20262,29702,22996,21592,20250'),
(467, 'member_list', 'member list', '25104,21592,21015,34920'),
(468, 'tfo_list', 'Testing Facility Office list', '35797,39564,26426,26500,25104,21592,21015,34920'),
(469, 'applicant_list', 'Applicant list', '30003,21150,32773,21015,34920'),
(470, 'company', 'Company', '21333,20301'),
(471, 'email', 'Email', '30005,23376,37038,31665'),
(472, 'password', 'Password', '23494,30721'),
(473, 'con_pass', 'Confirm Password', '30830,35748,23494,30721'),
(474, 'cancel', 'Cancel', '21462,28040'),
(475, 'update', 'Update', '26356,26032'),
(476, 'edit', 'Edit', '32534,36753'),
(478, 'disable_account', 'Are you sure you want to disable account', '20320,30830,35748,35201,20923,32467,35813,24080,21495,21527'),
(480, 'system_message', 'System Message', '31995,32479,28040,24687'),
(481, 'add_group', 'Add Group', '28155,21152,19987,19994,32452'),
(482, 'add_applicant', 'Add Applicant', '28155,21152,25104,21592'),
(483, 'ecm', 'Ethics Committee Member', '20262,29702,22996,21592,20250,25104,21592'),
(484, 'ecc', 'Ethics Committee Chairman', '20262,29702,22996,21592,20250,20027,24109'),
(485, 'blank_update', 'leave blank if not going to update', '22914,27809,26377,21487,19981,22635'),
(486, 'mobile_registered', 'Mobile Number is already registered', '25163,26426,21495,24050,23384,22312'),
(487, 'email_registered', 'Email is already registered', '37038,31665,24050,23384,22312'),
(488, 'password_not_match', 'Password does not match', '23494,30721,19981,27491,30830'),
(489, 'drug_list', 'Drug List', '33647,21697,21015,34920'),
(490, 'record_type', 'Record Type', '20986,20837,31867,21035'),
(491, 'lonely_remark', 'It looks lonely here...<br>Let''s wait for new', '26242,26080,35760,24405'),
(492, 'specification', 'Specification', '35268,26684'),
(493, 'term_validity', 'Term of validity', '26377,25928,26399'),
(494, 'add_drug', 'Add Drug', '28155,21152,33647,21697'),
(495, 'trade_name', 'Trade Name', '21830,21697,21517'),
(496, 'chemical_name', 'Chemical Name', '21270,23398,21517'),
(498, 'tested_drug', 'Tested Drug', '21463,35797,33647'),
(499, 'control_drug', 'Control Drug', '23545,29031,33647'),
(500, 'placebo', 'Placebo', '23545,29031,33647'),
(501, 'adjuvant_drug', 'Adjuvant Drug', '36741,21161,29992,33647'),
(502, 'inspection_report', 'Inspection Report', '33647,21697,26816,39564,25253,21578,20070'),
(503, 'pharmaceutical_factory', 'Pharmaceutical factory', '29983,20135,33647,21378'),
(504, 'provider', 'Provider', '25552,20379,32773'),
(505, 'add_record', 'Add Record', '28155,21152,35760,24405'),
(506, 'id_card', 'ID Card', '36523,20221,35777'),
(507, 'gcp', 'GCP', '71,67,80,22521,35757,35777,20070'),
(508, 'diploma', 'Diploma', '27605,19994,35777'),
(509, 'deg_cert', 'Degree Certificate', '23398,20301,35777'),
(513, 'save_address', 'Save Address', '20445,23384,22320,28857'),
(514, 'save_temp', 'Save Temperature', '20445,23384,28201,24230'),
(515, 'save_humidity', 'Save Humidity', '20445,23384,28287,24230'),
(516, 'project_material', 'Project material', '39033,30446,26448,26009'),
(517, 'project_progress', 'Project progress', '39033,30446,36827,23637'),
(518, 'save_as_draft', 'Save as draft', '20445,23384,20026,33609,31295'),
(520, 'collect', 'Collect', '37319,38598,35760,24405'),
(521, 'handle', 'Handle', '22788,29702,35760,24405'),
(522, 'save', 'Save', '20445,23384,35760,24405'),
(523, 'transport', 'Transport', '36816,36755,35760,24405'),
(524, 'determine', 'Determine', '27979,23450,35760,24405'),
(525, 'recycle', 'Recycle', '22238,25910,35760,24405'),
(526, 'destroy', 'Destroy', '38144,27585,35760,24405'),
(527, 'download_template', 'Download template', '19979,36733,27169,26495'),
(528, 'error_updating_project', 'Error updating project', '26356,26032,39033,30446,26102,20986,38169'),
(529, 'project_updated_successfully', 'Project updated successfully', '39033,30446,24050,25104,21151,26356,26032'),
(530, 'operation_successful', 'Operation Successful', '25552,20132,25104,21151'),
(532, 'confirmation', 'Confirmation', '30830,35748'),
(534, 'add_project_members', 'Add Project Members', '28155,21152,39033,30446,25104,21592'),
(535, 'clinician', 'Clinician', '20020,24202,21307,29983'),
(536, 'research_nurse', 'Research Nurse', '30740,31350,25252,22763'),
(539, 'feedback', 'Feedback', '21453,39304'),
(540, 'add_documents', 'Add Documents', '28155,21152,25991,26723'),
(542, 'agree', 'Agree', '21516,24847'),
(543, 'disagree', 'Disagree', '19981,21516,24847'),
(544, 'comments', 'Comments', '27880,37322'),
(545, 'add_document_remarks', 'Add Document Remarks', '28155,21152,25991,26723,22791,27880'),
(546, 'ethics_committee_comments', 'Ethics Committee Comments', '20262,29702,22996,21592,20250,24847,35265'),
(549, 'pass', 'Pass', '23457,26680,36890,36807'),
(550, 'no_pass', 'No Pass', '23457,26680,19981,36890,36807'),
(552, 'done', 'Done', '23436,25104'),
(553, 'empty_table_data', 'Empty table data', '26242,26080,35760,24405'),
(554, 'logout', 'Log Out', '30331,20986'),
(565, 'approve_reject_investigator_revision', 'Approve or Reject Investigator Opinion', '26159,21542,21516,24847,30740,31350,32773,20462,25913,21518,30340,26041,26696'),
(566, 'birthday', 'Birthday', '20986,29983,26085,26399'),
(567, 'id_num', 'ID Num', '36523,20221,35777,21495,30721'),
(568, 'state_date', 'State Date', '38543,26426,26102,38388'),
(569, 'chosen', 'Chosen', '36873,25321'),
(570, 'begin', 'Begin', '24320,22987'),
(571, 'follow_up', 'Follow Up', '36319,36827'),
(572, 'out', 'Out', '20986,26469'),
(573, 'drop', 'Drop', '19979,38477'),
(574, 'finish', 'Finish', '23436,25104'),
(580, 'participate', 'Participate', '21442,21152'),
(582, 'upload_newer_version', 'Please upload newer version before proceeding', '35831,19978,20256,26032,30340,39033,30446,26041,26696'),
(583, 'reason', 'Reason', '21407,22240'),
(584, 'ethics_committee_secretary', 'Ethics Committee secretary', '20262,29702,22996,21592,20250,31192,20070'),
(585, 'treatment_situation', 'Treatment Situation', '83,65,69,32,24773,20917'),
(587, 'lead', 'Lead', '29301,22836'),
(588, 'investigator', 'Investigator', '30740,31350,32773'),
(589, 'the', 'The', '35813'),
(590, 'are_you_sure', 'Are you sure to', '32,24744,30830,35748'),
(591, 'project_complete', 'Project complete', '39033,30446,23436,25104'),
(592, 'process', 'Process', '35797,39564,36827,34892'),
(593, 'my_projects', 'My Projects', '25105,30340,39033,30446'),
(594, 'medical_equipment', 'Medical Equipment', '21307,30103,22120,26800'),
(595, 'food', 'Food', '39135,21697'),
(596, 'stop_project', 'Are you sure you want to stop project?', '24744,30830,35748,20013,27490,35813,39033,30446,21527'),
(597, 'stop', 'Stop', '35797,39564,20013,27490'),
(598, 'trial_finish', 'Trial Finish', '35797,39564,32467,26463'),
(604, 'add_documents_comments', 'Add Documents Comments', '35831,34917,20840,36164,26009'),
(605, 'check_pending', 'Check Pending', '24453,23457,26680'),
(607, 'project_audit', 'Project Audit', '39033,30446,23457,26680'),
(610, 'choose_peer_review_expert', 'Choose Peer Review Expert', '21516,34892,35780,35758,19987,23478'),
(611, 'pre_list', 'List of Peer Review Expert', '36873,25321,21516,34892,35780,35758,19987,23478'),
(613, 'to_do', 'To Do', '24453,21150'),
(614, 'inspection', 'Inspection', '36136,37327,20445,35777'),
(615, 'inspector', 'Inspector', '30417,26597'),
(616, 'on_site_inspection', 'On-site inspection', '29616,22330,26816,26597'),
(617, 'clinician_selecting', 'Clinician selecting', '21307,29983,23457,26680'),
(618, 'chosen_subject', 'Chosen to be subject', '20837,36873,21463,35797,32773'),
(619, 'before_selecting', 'Before Selecting', '31579,36873,21069'),
(620, 'my_projects_non_a', 'Projects', '39033,30446,23457,26680'),
(622, 'stop_button', 'Stop', '20013,27490'),
(623, 'biological_sample_controller', 'Biological Sample Controller', '29983,29289,26679,21697,31649,29702,21592'),
(624, 'medicine_controller', 'Medicine Controller', '33647,21697,31649,29702,21592'),
(625, 'will_start', 'Will Start', '21551,21160,20250'),
(626, 'training', 'Training', '22521,35757'),
(627, 'agree_tfo_1', 'Agree and Submit to PI', '36164,26009,40784,20840,24050,25552,20132,80,73,23457,26680'),
(628, 'disagree_tfo_1', 'Disagree Return to Applicant', '19981,20104,25509,21463'),
(629, 'agree_pi_2', 'Agree and Submit to Peer', '24314,35758,31435,39033'),
(630, 'disagree_pi_2', 'Disagree and submit back to Applicant', '24314,35758,19981,31435,39033'),
(631, 'comments_pi_2', 'PI Comments', '21407,22240'),
(632, 'comments_tfo_1', 'TFO Comments', '35831,34917,20840,36164,26009'),
(634, 'comments_tfo_2', 'Comments TFO', '21407,22240'),
(635, 'choose_pre', 'Choose Peer Experts', '24050,36873,19987,23478'),
(637, 'upload_content_consent', 'Please upload conference content and consent before proceeding', '28155,21152,20262,29702,22996,21592,20250,20250,35758,35760,24405'),
(638, 'upload_consent_agreement', 'Please upload consent agreement before proceeding', '35831,19978,20256,31614,21452,26041,31614,23383,30422,31456,26041,26696,21450,20854,20182,36164,26009,25195,25551,20214,21040,39033,30446,36164,26009,28165,21333'),
(639, 'submit_materials', 'Submit Materials', '25552,20132,20262,29702,23457,26680'),
(640, 'ethics_pending', 'Ethics Pending', '20262,29702,23457,26680,20013'),
(641, 'agree_ecs_7', 'Agree', '23457,26680,36890,36807'),
(642, 'disagree_ecs_7', 'Disagree', '23457,26680,19981,36890,36807'),
(643, 'ecs_comments_7', 'ECS Comments', '21407,22240'),
(645, 'operation_failed', 'Operation Failed', '25805,20316,22833,36133'),
(646, 'ecm_comments', 'ECM Comments', '24847,35265'),
(647, 'version_date', 'Version date', '25552,20132,26085,26399'),
(648, 'ecc_comments', 'Ecc Comments', '21407,22240'),
(649, 'upload_approval', 'Please upload approval before proceeding', '19978,20256,21508,20010,20262,29702,22996,21592,20250,22996,21592,65292,20027,20219,31614,21517,30340,25209,20934,25991,20214,21040,39033,30446,36164,26009'),
(650, 'select', 'Select', '35831,36873,25321'),
(651, 'choose', 'Choose', '35831,36873,25321'),
(652, 'subject_begin', 'Begin', '24320,22987,35797,39564'),
(653, 'sending_party', 'Sending party', '26679,26412,21457,20986,26041'),
(654, 'operator', 'Operator', '32463,21150,20154'),
(655, 'shipper', 'Shipper', '26679,26412,36816,36865,26041'),
(656, 'recycler', 'Recycler', '26679,26412,22238,25910,26041'),
(657, 'recycle_report', 'Recycle report', '26679,26412,22238,25910,36164,26009'),
(658, 'handle_time', 'Handle time', '22788,29702,26102,38388'),
(659, 'handle_record', 'Handle record', '22788,29702,35760,24405'),
(660, 'transport_company', 'Transport company', '36816,36755,20844,21496'),
(661, 'transport_temp', 'Transport temperature', '36816,36755,28201,24230'),
(662, 'transport_humidity', 'Transport humidity', '36816,36755,28287,24230'),
(663, 'transport_time', 'Transport time', '36816,36755,26102,38388'),
(664, 'determine_record', 'Determine record', '27979,23450,31649,29702,35760,24405'),
(665, 'determine_report', 'Determine report', '26679,26412,27979,23450,25253,21578'),
(666, 'determine_company', 'Determine company', '27979,23450,21333,20301'),
(667, 'report_file', 'Report file', '25253,21578,25991,20214'),
(668, 'comment_app_4', 'Applicant Comments', '21407,22240'),
(669, 'choose_to_be_subject', 'Choose to be subject', '36827,20837,21463,35797,32773,24211'),
(670, 'put_in_storage', 'Put in storage', '20837,24211'),
(671, 'put_out_storage', 'Put out storage', '20986,24211'),
(672, 'waste', 'Waste', '25439,32791'),
(673, 'delivery_receipt', 'Delivery receipt', '20132,25509,21333'),
(674, 'actor_user_two', 'Actor user two', '20132,25509,21592,50'),
(675, 'actor_user_one', 'Transfer manager', '20132,25509,21592,49'),
(676, 'member_name', 'Name', '22995,21517'),
(677, 'data_administrator', 'Data Administrator', '25968,25454,31649,29702,21592'),
(678, 'on_site_inspector', 'On-site inspector', '29616,22330,26816,26597'),
(679, 'drug_combination', 'Drug Combination', '21512,24182,29992,33647'),
(680, 'member_position', 'Position', '36523,20221'),
(681, 'training_modal_pos', 'Position', '36523,20221'),
(682, 'volunteer_name', 'Name', '22995,21517'),
(683, 'inform_clinician', 'Inform Clinician', '36890,30693,20307,26816'),
(684, 'volunteer_pass', 'Pass', '20307,26816,36890,36807'),
(685, 'volunteer_no_pass', 'No Pass', '20307,26816,19981,36890,36807'),
(688, 'subject_name', 'Name', '22995,21517'),
(689, 'subject_follow_up', 'Follow up', '38543,35775'),
(690, 'subject_stop', 'Stop', '20013,27490'),
(691, 'subject_out', 'Out', '36864,20986'),
(692, 'subject_drop', 'Drop', '33073,33853'),
(693, 'light_effect', 'Light Side Effect', '36731,24230,19981,33391,21453,24212'),
(694, 'mid_effect', 'Mid Side Effect', '20013,24230,19981,33391,21453,24212'),
(695, 'unit', 'Unit', '21333,20301'),
(696, 'identity', 'Identity', '36523,20221'),
(698, 'placebo_drug_record', 'Placebo', '23433,24944,21058'),
(699, 'select_subject', 'Select subject', '36873,25321,21463,35797,32773'),
(700, 'upload_report_material', 'Upload statistical analysis, analysis of test reports, sub-center summary', '19978,20256,32479,35745,20998,26512,65292,20998,26512,27979,35797,25253,21578,65292,20998,20013,24515,24635,32467'),
(701, 'upload_report_material2', 'Upload research reports to project information', '19978,20256,30740,31350,25253,21578,21040,39033,30446,36164,26009'),
(703, 'collect_time_bgc', 'Collect Time', '26679,26412,37319,38598,26102,38388'),
(704, 'quantity_bgc', 'Quantity', '26679,26412,37327'),
(705, 'collect_record_bgc', 'Collect Record', '26679,26412,37319,38598,35760,24405'),
(706, 'collect_time_save', 'Collect Time', '20445,23384,26085,26399'),
(707, 'process_record_save', 'Process Record', '26679,26412,22788,29702,35760,24405'),
(708, 'destroy_header', 'Add destroy record sample', '26679,26412,35760,24405,26679,26412,35760,24405'),
(709, 'destroy_address', 'Destruction address', '20303,22336,26679,26412,38144,27585,22320,28857'),
(711, 'destroy_operator', 'Operator Sample Destruction Executives', '25805,20316,21592,26679,26412,38144,27585,23454,26045,20154,21592'),
(712, 'destroy_report_file', 'Report document sample destruction information', '25253,21578,25991,20214,26679,21697,38144,27585,36164,26009'),
(713, 'destroy_quantity', 'Destroy quantity', '26679,26412,37327,26679,21697,37327'),
(714, 'date_determine', 'Date', '27979,23450,26085,26399'),
(715, 'to_date', 'to', '33267'),
(716, 'hard_effect', 'Hard Side Effect', '37325,24230,19981,33391,21453,24212'),
(717, 'important_effect', 'Important Side Effect', '37325,35201,19981,33391,21453,24212'),
(718, 'sae_summary', 'SAE', '20005,37325,19981,33391,21453,24212'),
(719, 'other_situation', 'Other Situation', '20854,20182,24773,20917'),
(720, 'upload_materials', 'Upload Materials', '19978,20256,36164,26009'),
(721, 'return_to_volunteer', 'Return to volunteer', '25104,20026,24535,24895,32773'),
(722, 'drug_production_batch_number', 'Drug production batch number', '33647,21697,29983,20135,25209,21495'),
(723, 'please_input_reason', 'Please input reason', '35831,22635,20889,21407,22240'),
(724, 'audit_status', 'Audit Status', '23457,26680,29366,24577'),
(725, 'content', 'Content', '20107,30001'),
(727, 'execution_date', 'Execution date', '25191,34892,26085,26399'),
(728, 'modification_trace', 'Modification Trace', '31293,26597,36712,36857'),
(729, 'data_bank', 'Data Bank', '36164,26009,24211'),
(730, 'phone_number', 'Phone number', '25163,26426,21495'),
(731, 'date_of_birth', 'Date of birth', '20986,29983,26085,26399'),
(732, 'full_name', 'Full name', '22995,21517'),
(733, 'operating', 'Operating', '25805,20316'),
(734, 'the_drug_name', 'Drug name', '33647,21697,21517,31216'),
(735, 'add_volunteer_information', 'Add volunteer information', '28155,21152,24535,24895,32773,20449,24687'),
(736, 'serious_adverse_reactions', 'Serious adverse reactions', '20005,37325,19981,33391,21453,24212'),
(738, 'sample_volume', 'sample_volume', '26679,26412,37327'),
(739, 'bio_samp_mgt', 'Biological sample management', '29983,29289,26679,26412,31649,29702'),
(740, 'feedback_of_testing', 'Feedback of testing organization', '35797,39564,26426,26500,21453,39304'),
(741, 'participants2', 'Participants', '21442,19982,20154,21592'),
(742, 'untitled_meeting', 'Untitled meeting', '26080,39064,20250,35758'),
(745, 'sign_date', 'Sign date', '31614,32626,26085,26399'),
(746, 'id_card_admin', 'ID Card', '36523,20221,35777'),
(747, 'gcp_card', 'GCP Card', '71,67,80,22521,35757,35777,20070'),
(749, 'degree_certificate', 'Degree certificate', '23398,20301,35777'),
(750, 'new_user_success', 'Successfully registered new user', '24744,24050,25104,21151,28155,21152,26032,24080,21495'),
(751, 'make_sure', 'Please make sure details are correct', '35831,30830,20445,35814,32454,20449,24687,26159,27491,30830,30340,12290'),
(752, 'activated', 'Activated', '24050,28608,27963'),
(753, 'frozen', 'Disabled', '24050,20923,32467'),
(754, 'freeze', 'Freeze', '20923,32467'),
(755, 'activation', 'Activation', '28608,27963'),
(756, 'ok', 'Ok', '22909,30340'),
(757, 'rejected_remarks', 'Rejected remarks', '23457,26680,19981,36890,36807,24847,35265'),
(761, 'applicant_todo_1_1', 'Please check the comments from the Testing Facility Officer and revise', '35797,39564,26426,26500,25298,32477,20102,24744,30340,39033,30446,65292,35831,26597,30475,21407,22240'),
(762, 'applicant_todo_2_1', 'Please check the comments from the Principal Investigator', '20027,35201,30740,31350,32773,25298,32477,20102,24744,30340,39033,30446,65292,35831,26597,30475,21407,22240'),
(763, 'applicant_todo_9_1', 'Rejected by ethics secretary', '20262,29702,22996,21592,20250,25298,32477,20102,24744,30340,39033,30446'),
(764, 'applicant_todo_4_0', 'Version 2 uploaded by Investigator', '30740,31350,32773,24050,19978,20256,26032,30340,39033,30446,26041,26696,65292,35831,26597,30475,30830,35748'),
(765, 'PI_todo_2_0', 'Audit the project accepted by TFO', '35831,23457,26680,39033,30446'),
(766, 'PRE_todo_3_0', ' Offer comments to applicant''s project', '35831,28155,21152,35780,35758,24847,35265'),
(767, 'I_todo_3_0', 'Please Upload version 2', '35831,20462,25913,24182,19978,20256,26032,30340,39033,30446,26041,26696'),
(768, 'I_todo_3_1', ' Please Revise version 2 rejected by applicant', '30003,21150,32773,19981,21516,24847,26032,26041,26696,65292,35831,27807,36890,21518,20462,25913'),
(769, 'I_todo_5_0', 'Please Upload consent agreement', '35831,19978,20256,21452,26041,31614,23383,30422,31456,26041,26696,21450,20854,20182,36164,26009,25195,25551,20214,21040,39033,30446,36164,26009,28165,21333'),
(770, 'I_todo_10_0', 'Please Upload new version', '35831,20462,25913,24182,19978,20256,26032,30340,39033,30446,26041,26696'),
(771, 'ECS_todo_7_0', 'Please audit the project', '35831,23457,26680,39033,30446'),
(772, 'ECS_todo_9_0', ' Please upload conference consent and conference content', '35831,26681,25454,20262,29702,22996,21592,20250,20250,35758,35760,24405,21644,22312,32447,24847,35265,25972,29702,20986,24847,35265,34920,24182,19978,20256,21040,39033,30446,36164,26009'),
(773, 'ECS_todo_9_1', 'Please Re-upload conference consent and conference content', '35831,37325,26032,19978,20256,20462,25913,24847,35265,34920'),
(774, 'ECS_todo_12_0', 'Please approve for trial to begin', '35831,19978,20256,21508,20010,20262,29702,22996,21592,20250,22996,21592,12289,20027,20219,31614,21517,30340,25209,20934,25991,20214,21040,39033,30446,36164,26009'),
(775, 'ECM_todo_8_0', 'Offer Comments', '35831,28155,21152,20462,25913,24847,35265'),
(776, 'ECC_todo_11_0', 'Audit version 2', '35831,23457,26680,39033,30446'),
(777, 'tfo_todo_1_0', 'Please review applicant submission', '35831,23457,26680,39033,30446'),
(778, 'tfo_todo_6_0', 'Please submit materials', '35831,25552,20132,39033,30446,20449,24687,21040,20262,29702,22996,21592,20250'),
(779, 'home', 'Home', '39318,39029'),
(780, 'news', 'News', '28040,24687'),
(781, 'status_0_1', 'Rejected Audit By Principal Investigator or Testing Facility Office', '20027,35201,30740,31350,32773,23457,26680,19981,36890,36807,65292,35797,39564,26426,26500,23457,26680,19981,36890,36807'),
(782, 'status_3_1', 'Returned to Investigator with comments By Applicant', '30003,21150,32773,25552,20132,20462,25913,24314,35758'),
(783, 'status_7_1', 'Returned to Applicant and Investigator for modification by Ethics Committee Secretary', '20262,29702,22996,21592,20250,31192,20070,23457,26680,19981,36890,36807'),
(784, 'status_9_1', 'Rejected new version from Investigator by Ethics Committee Chairman', '20262,29702,22996,21592,20250,19981,21516,24847,26032,26041,26696'),
(785, 'status_1_0', 'Submitted Project', '25552,20132,39033,30446,30003,35831'),
(786, 'status_2_0', 'Approved By Testing Facility Office', '35797,39564,26426,26500,23457,26680,36890,36807'),
(787, 'status_3_0', 'Approved By Principal Investigator', '20027,35201,30740,31350,32773,23457,26680,36890,36807'),
(788, 'status_4_0', 'Project Version 2 uploaded By Investigator', '30740,31350,32773,19978,20256,26032,26041,26696'),
(789, 'status_5_0', 'Applicant Approved version 2 By Investigator', '30003,21150,32773,21516,24847,26032,26041,26696'),
(790, 'status_6_0', 'Consent Agreement Uploaded By Investigator', '30740,31350,32773,19978,20256,21452,26041,31614,23383,30422,31456,26041,26696,21450,20854,20182,36164,26009,25195,25551,20214'),
(791, 'status_7_0', 'Submitted to Ethics Committee Secretary for Auditing', '25552,20132,21040,20262,29702,22996,21592,20250,23457,26680'),
(792, 'status_8_0', 'Submitted to Ethics Committee Members By Ethics Committee Secretary', '20262,29702,22996,21592,20250,31192,20070,23457,26680,36890,36807'),
(793, 'status_9_0', 'Comments Offered By Ethics Committee Members', '20262,29702,22996,21592,20250,25104,21592,25552,20379,24314,35758'),
(794, 'status_10_0', 'Conference Content & Consent Uploaded By Ethics Committee Secretary', '24050,19978,20256,20262,29702,22996,21592,20250,20250,35758,35760,24405,21644,24847,35265'),
(795, 'status_11_0', 'New Version Uploaded By Investigator', '30740,31350,32773,19978,20256,26032,26041,26696'),
(796, 'status_12_0', 'New Version Approved By Ethics Committee Chairman', '20262,29702,22996,21592,20250,21516,24847,26032,26041,26696'),
(797, 'module', 'Module', '27169,22359'),
(798, 'upload_time', 'Upload time', '19978,20256,26102,38388'),
(799, 'view_attachment', 'View attachment', '26597,30475,25991,20214'),
(800, 'no_problem', 'No problem', '22797,26680,36890,36807'),
(801, 'have_problem', 'Have problem', '22797,26680,19981,36890,36807'),
(802, 'add_double_check_opinion', 'Add double check opinion', '28155,21152,22797,26680,24847,35265'),
(803, 'to_do_list', 'To do list', '24453,21150,20107,39033'),
(804, 'user_logs', 'User action log', '31293,26597,36712,36857'),
(805, 'account', 'Account', '36134,25143'),
(806, 'time', 'Time', '26102,38388'),
(807, 'login', 'Login', '30331,24405'),
(808, 'not_applicable', 'N/A', '78,47,65'),
(810, 'submitter', 'Host', '20027,25345,20154'),
(811, 'subjects_controller', 'Subjects controller', '21463,35797,32773,31649,29702,21592'),
(812, 'mobile', 'Mobile', '30005,35805,21495,30721'),
(813, 'native_place', 'Native place', '31821,36143'),
(814, 'save_to_databank', 'Save to data bank', '26159,21542,21516,27493,21040,36164,26009,24211');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `log_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `role_id` int(11) NOT NULL,
  `action` text NOT NULL COMMENT 'phrase key of action',
  `payload` longtext NOT NULL COMMENT 'json string of some data'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`log_id`, `created_at`, `role_id`, `action`, `payload`) VALUES
(1, '2017-02-08 08:54:32', 2, 'logout', ''),
(2, '2017-02-08 08:54:37', 47, 'login', ''),
(3, '2017-02-08 08:54:43', 47, 'login', ''),
(4, '2017-02-09 08:44:57', 1, 'login', ''),
(5, '2017-02-09 08:47:19', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u6d4b\\u8bd5\\u9879\\u76ee5","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"Enzo"}'),
(6, '2017-02-09 08:48:22', 1, 'logout', ''),
(7, '2017-02-09 08:48:30', 3, 'login', ''),
(8, '2017-02-09 08:50:28', 3, 'logout', ''),
(9, '2017-02-09 08:50:49', 21, 'login', ''),
(10, '2017-02-09 08:52:39', 21, 'logout', ''),
(11, '2017-02-09 08:52:56', 2, 'login', ''),
(12, '2017-02-09 08:56:38', 2, 'logout', ''),
(13, '2017-02-09 08:56:47', 1, 'login', ''),
(14, '2017-02-09 08:57:35', 1, 'logout', ''),
(15, '2017-02-09 08:57:53', 21, 'login', ''),
(16, '2017-02-09 09:00:06', 21, 'logout', ''),
(17, '2017-02-09 09:00:19', 47, 'login', ''),
(18, '2017-02-09 09:09:28', 1, 'login', ''),
(19, '2017-02-09 09:09:50', 1, 'logout', ''),
(20, '2017-02-09 09:10:10', 21, 'login', ''),
(21, '2017-02-09 09:10:25', 21, 'logout', ''),
(22, '2017-02-09 09:10:37', 1, 'login', ''),
(23, '2017-02-09 09:11:36', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u6d4b\\u8bd5\\u9879\\u76ee6","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"Enzo"}'),
(24, '2017-02-09 09:12:01', 1, 'logout', ''),
(25, '2017-02-09 09:12:05', 47, 'logout', ''),
(26, '2017-02-09 09:12:15', 3, 'login', ''),
(27, '2017-02-09 09:14:11', 3, 'logout', ''),
(28, '2017-02-09 09:14:24', 1, 'login', ''),
(29, '2017-02-09 09:17:29', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"Enzo"}'),
(30, '2017-02-09 09:21:21', 1, 'logout', ''),
(31, '2017-02-09 09:21:35', 1, 'login', ''),
(32, '2017-02-09 09:27:30', 1, 'logout', ''),
(33, '2017-02-09 09:28:04', 23, 'login', ''),
(34, '2017-02-09 09:29:16', 23, 'logout', ''),
(35, '2017-02-09 09:29:27', 1, 'login', ''),
(36, '2017-02-09 09:29:42', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"Enzo"}'),
(37, '2017-02-09 09:29:49', 1, 'logout', ''),
(38, '2017-02-09 09:29:56', 3, 'login', ''),
(39, '2017-02-09 09:34:20', 3, 'logout', ''),
(40, '2017-02-09 09:34:34', 2, 'login', ''),
(41, '2017-02-09 09:36:46', 2, 'logout', ''),
(42, '2017-02-09 09:36:54', 1, 'login', ''),
(43, '2017-02-09 09:37:03', 1, 'login', ''),
(44, '2017-02-09 09:38:37', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"Enzo"}'),
(45, '2017-02-09 09:39:01', 1, 'logout', ''),
(46, '2017-02-09 09:40:09', 3, 'login', ''),
(47, '2017-02-09 09:41:53', 3, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"TestTFO"}'),
(48, '2017-02-09 09:42:07', 3, 'logout', ''),
(49, '2017-02-09 09:42:25', 23, 'login', ''),
(50, '2017-02-09 09:43:17', 24, 'logout', ''),
(51, '2017-02-09 09:43:39', 2, 'login', ''),
(52, '2017-02-09 09:43:54', 2, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"TestPI"}'),
(53, '2017-02-09 09:44:04', 2, 'logout', ''),
(54, '2017-02-09 09:44:27', 5, 'login', ''),
(55, '2017-02-09 09:46:48', 5, 'logout', ''),
(56, '2017-02-09 09:47:02', 2, 'login', ''),
(57, '2017-02-09 09:47:35', 2, 'logout', ''),
(58, '2017-02-09 09:47:57', 6, 'login', ''),
(59, '2017-02-09 09:49:25', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"TestI"}'),
(60, '2017-02-09 09:50:11', 6, 'logout', ''),
(61, '2017-02-09 09:50:28', 1, 'login', ''),
(62, '2017-02-09 09:50:41', 2, 'login', ''),
(63, '2017-02-09 09:51:13', 1, 'logout', ''),
(64, '2017-02-09 09:51:22', 3, 'login', ''),
(65, '2017-02-09 09:51:36', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"Enzo"}'),
(66, '2017-02-09 09:51:42', 1, 'logout', ''),
(67, '2017-02-09 09:52:25', 6, 'login', ''),
(68, '2017-02-09 09:53:13', 3, 'logout', ''),
(69, '2017-02-09 09:53:20', 1, 'login', ''),
(70, '2017-02-09 09:55:33', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"TestI"}'),
(71, '2017-02-09 09:55:39', 6, 'logout', ''),
(72, '2017-02-09 09:55:50', 3, 'login', ''),
(73, '2017-02-09 09:55:58', 3, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"TestTFO"}'),
(74, '2017-02-09 09:56:04', 3, 'logout', ''),
(75, '2017-02-09 09:56:12', 1, 'login', ''),
(76, '2017-02-09 09:56:15', 20, 'login', ''),
(77, '2017-02-09 09:56:24', 20, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"TestECS"}'),
(78, '2017-02-09 09:56:29', 20, 'logout', ''),
(79, '2017-02-09 09:56:46', 7, 'login', ''),
(80, '2017-02-09 09:56:55', 7, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"TestECM"}'),
(81, '2017-02-09 09:57:00', 7, 'logout', ''),
(82, '2017-02-09 09:57:22', 20, 'login', ''),
(83, '2017-02-09 09:57:46', 20, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"TestECS"}'),
(84, '2017-02-09 09:57:51', 20, 'logout', ''),
(85, '2017-02-09 09:58:09', 6, 'login', ''),
(86, '2017-02-09 09:58:30', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"TestI"}'),
(87, '2017-02-09 09:58:34', 6, 'logout', ''),
(88, '2017-02-09 09:58:41', 4, 'login', ''),
(89, '2017-02-09 09:58:48', 4, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"TestECC"}'),
(90, '2017-02-09 09:58:53', 4, 'logout', ''),
(91, '2017-02-09 09:59:14', 20, 'login', ''),
(92, '2017-02-09 09:59:36', 20, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"","\\u4e3b\\u6301\\u4eba":"TestECS"}'),
(93, '2017-02-09 09:59:41', 20, 'logout', ''),
(94, '2017-02-09 09:59:58', 2, 'login', ''),
(95, '2017-02-09 10:04:49', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test Databank","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"Tes","\\u4e3b\\u6301\\u4eba":"Enzo"}'),
(96, '2017-02-09 10:04:56', 1, 'logout', ''),
(97, '2017-02-09 10:05:04', 3, 'login', ''),
(98, '2017-02-09 10:11:11', 3, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test Databank","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"Tes","\\u4e3b\\u6301\\u4eba":"TestTFO"}'),
(99, '2017-02-09 10:11:19', 3, 'logout', ''),
(100, '2017-02-09 10:11:30', 2, 'login', ''),
(101, '2017-02-09 10:11:44', 2, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test Databank","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"Tes","\\u4e3b\\u6301\\u4eba":"TestPI"}'),
(102, '2017-02-09 10:11:45', 2, 'logout', ''),
(103, '2017-02-09 10:11:50', 2, 'logout', ''),
(104, '2017-02-09 10:11:57', 5, 'login', ''),
(105, '2017-02-09 10:12:01', 2, 'login', ''),
(106, '2017-02-09 10:12:30', 2, 'logout', ''),
(107, '2017-02-09 10:12:34', 5, 'logout', ''),
(108, '2017-02-09 10:12:43', 47, 'login', ''),
(109, '2017-02-09 10:12:53', 6, 'login', ''),
(110, '2017-02-09 10:13:39', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test Databank","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"Tes","\\u4e3b\\u6301\\u4eba":"TestI"}'),
(111, '2017-02-09 10:13:44', 6, 'logout', ''),
(112, '2017-02-09 10:14:21', 1, 'login', ''),
(113, '2017-02-09 10:21:36', 47, 'login', ''),
(114, '2017-02-09 10:21:57', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test Databank","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"Tes","\\u4e3b\\u6301\\u4eba":"Enzo"}'),
(115, '2017-02-09 10:23:46', 1, 'logout', ''),
(116, '2017-02-09 10:23:55', 47, 'login', ''),
(117, '2017-02-09 10:25:11', 47, 'logout', ''),
(118, '2017-02-09 10:25:28', 6, 'login', ''),
(119, '2017-02-09 10:25:54', 6, 'logout', ''),
(120, '2017-02-09 10:26:14', 11, 'login', ''),
(121, '2017-02-09 10:51:53', 47, 'logout', ''),
(122, '2017-02-09 10:52:00', 2, 'login', ''),
(123, '2017-02-09 02:13:55', 1, 'login', ''),
(124, '2017-02-09 02:36:33', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u7532\\u78fa\\u9178\\u666e\\u55b9\\u66ff\\u5c3c\\u7247\\u5728\\u6210\\u4eba\\u590d\\u53d1\\u96be\\u6cbb\\u6027\\u6076\\u6027\\u6dcb\\u5df4\\u7624\\u60a3\\u8005\\u53e3\\u670d\\u7ed9\\u836f\\u7684I\\u671f\\u4e34\\u5e8a\\u7814\\u7a76","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"011","\\u4e3b\\u6301\\u4eba":"Enzo"}'),
(125, '2017-02-09 02:37:19', 3, 'login', ''),
(126, '2017-02-09 02:37:37', 3, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u7532\\u78fa\\u9178\\u666e\\u55b9\\u66ff\\u5c3c\\u7247\\u5728\\u6210\\u4eba\\u590d\\u53d1\\u96be\\u6cbb\\u6027\\u6076\\u6027\\u6dcb\\u5df4\\u7624\\u60a3\\u8005\\u53e3\\u670d\\u7ed9\\u836f\\u7684I\\u671f\\u4e34\\u5e8a\\u7814\\u7a76","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"011","\\u4e3b\\u6301\\u4eba":"TestTFO"}'),
(127, '2017-02-09 02:38:24', 2, 'login', ''),
(128, '2017-02-09 02:38:59', 2, 'logout', ''),
(129, '2017-02-09 02:40:30', 2, 'login', ''),
(130, '2017-02-09 02:47:35', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u7532\\u78fa\\u9178\\u666e\\u55b9\\u66ff\\u5c3c\\u7247\\u5728\\u6210\\u4eba\\u590d\\u53d1\\u96be\\u6cbb\\u6027\\u6076\\u6027\\u6dcb\\u5df4\\u7624\\u60a3\\u8005\\u53e3\\u670d\\u7ed9\\u836f\\u7684I\\u671f\\u4e34\\u5e8a\\u7814\\u7a76","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"012","\\u4e3b\\u6301\\u4eba":"Enzo"}'),
(131, '2017-02-09 02:47:54', 3, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u7532\\u78fa\\u9178\\u666e\\u55b9\\u66ff\\u5c3c\\u7247\\u5728\\u6210\\u4eba\\u590d\\u53d1\\u96be\\u6cbb\\u6027\\u6076\\u6027\\u6dcb\\u5df4\\u7624\\u60a3\\u8005\\u53e3\\u670d\\u7ed9\\u836f\\u7684I\\u671f\\u4e34\\u5e8a\\u7814\\u7a76","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"012","\\u4e3b\\u6301\\u4eba":"TestTFO"}'),
(132, '2017-02-09 02:49:27', 2, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u7532\\u78fa\\u9178\\u666e\\u55b9\\u66ff\\u5c3c\\u7247\\u5728\\u6210\\u4eba\\u590d\\u53d1\\u96be\\u6cbb\\u6027\\u6076\\u6027\\u6dcb\\u5df4\\u7624\\u60a3\\u8005\\u53e3\\u670d\\u7ed9\\u836f\\u7684I\\u671f\\u4e34\\u5e8a\\u7814\\u7a76","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"012","\\u4e3b\\u6301\\u4eba":"TestPI"}'),
(133, '2017-02-09 02:50:58', 5, 'login', ''),
(134, '2017-02-09 02:57:31', 5, 'logout', ''),
(135, '2017-02-09 02:57:42', 6, 'login', ''),
(136, '2017-02-09 02:59:25', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u7532\\u78fa\\u9178\\u666e\\u55b9\\u66ff\\u5c3c\\u7247\\u5728\\u6210\\u4eba\\u590d\\u53d1\\u96be\\u6cbb\\u6027\\u6076\\u6027\\u6dcb\\u5df4\\u7624\\u60a3\\u8005\\u53e3\\u670d\\u7ed9\\u836f\\u7684I\\u671f\\u4e34\\u5e8a\\u7814\\u7a76","\\u6a21\\u5757":"N\\/A","\\u5e8f\\u53f7":"012","\\u4e3b\\u6301\\u4eba":"TestI"}'),
(137, '2017-02-09 03:00:58', 3, 'logout', ''),
(138, '2017-02-09 03:01:11', 20, 'login', ''),
(139, '2017-02-09 03:01:23', 20, 'logout', ''),
(140, '2017-02-09 03:01:35', 20, 'login', ''),
(141, '2017-02-09 03:01:52', 20, 'logout', ''),
(142, '2017-02-09 03:02:01', 3, 'login', ''),
(143, '2017-02-09 03:02:44', 1, 'logout', ''),
(144, '2017-02-09 03:03:29', 20, 'login', ''),
(145, '2017-02-09 04:55:27', 6, 'login', ''),
(146, '2017-02-09 04:56:23', 6, 'logout', ''),
(147, '2017-02-09 04:56:39', 1, 'login', ''),
(148, '2017-02-09 05:24:26', 3, 'logout', ''),
(149, '2017-02-09 05:24:29', 1, 'login', ''),
(150, '2017-02-09 05:25:31', 1, 'logout', ''),
(151, '2017-02-09 05:28:06', 21, 'login', ''),
(152, '2017-02-09 05:38:33', 21, 'logout', ''),
(153, '2017-02-09 05:38:38', 11, 'login', ''),
(154, '2017-02-09 05:39:47', 11, 'logout', ''),
(155, '2017-02-09 05:39:52', 11, 'login', ''),
(156, '2017-02-09 06:03:39', 11, 'logout', ''),
(157, '2017-02-09 06:03:44', 21, 'login', ''),
(158, '2017-02-09 06:07:26', 21, 'logout', ''),
(159, '2017-02-09 06:07:31', 11, 'login', ''),
(160, '2017-02-10 10:30:55', 11, 'logout', ''),
(161, '2017-02-10 10:31:07', 1, 'login', ''),
(162, '2017-02-10 10:33:23', 1, 'logout', ''),
(163, '2017-02-10 10:33:27', 1, 'login', ''),
(164, '2017-02-10 10:34:55', 1, 'logout', ''),
(165, '2017-02-10 10:35:01', 11, 'login', ''),
(166, '2017-02-10 11:39:05', 11, 'logout', ''),
(167, '2017-02-10 11:39:12', 21, 'login', ''),
(168, '2017-02-10 11:54:53', 21, 'logout', ''),
(169, '2017-02-10 11:54:59', 11, 'login', ''),
(170, '2017-02-10 02:12:36', 11, 'logout', ''),
(171, '2017-02-10 02:12:43', 21, 'login', ''),
(172, '2017-02-10 02:39:54', 21, 'logout', ''),
(173, '2017-02-10 02:39:58', 11, 'login', ''),
(174, '2017-02-10 03:06:45', 11, 'logout', ''),
(175, '2017-02-10 03:06:49', 21, 'login', ''),
(176, '2017-02-10 03:13:54', 21, 'logout', ''),
(177, '2017-02-10 03:13:58', 11, 'login', ''),
(178, '2017-02-10 03:14:36', 11, 'logout', ''),
(179, '2017-02-10 03:14:41', 11, 'login', ''),
(180, '2017-02-10 05:03:19', 11, 'logout', ''),
(181, '2017-02-10 05:03:23', 21, 'login', ''),
(182, '2017-02-10 05:05:05', 21, 'logout', ''),
(183, '2017-02-10 05:05:12', 11, 'login', ''),
(184, '2017-02-10 06:37:16', 11, 'logout', ''),
(185, '2017-02-10 06:37:20', 21, 'login', ''),
(186, '2017-02-10 06:42:13', 21, 'logout', ''),
(187, '2017-02-10 06:42:21', 11, 'login', ''),
(188, '2017-02-10 07:01:17', 11, 'logout', ''),
(189, '2017-02-10 07:01:20', 21, 'login', '');

-- --------------------------------------------------------

--
-- Table structure for table `mc_info`
--

CREATE TABLE `mc_info` (
  `mc_org` varchar(255) NOT NULL,
  `mc_person` varchar(255) NOT NULL,
  `mc_lead` varchar(255) NOT NULL,
  `mc_remarks` longtext NOT NULL,
  `mc_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='mc means `multi-center`';

--
-- Dumping data for table `mc_info`
--

INSERT INTO `mc_info` (`mc_org`, `mc_person`, `mc_lead`, `mc_remarks`, `mc_id`, `project_id`) VALUES
('123', '123', 'ç‰µå¤´', '123', 1, 2),
('zxc', 'zxc', 'Participate', 'zxc', 2, 6),
('dsa', 'dsa', 'Lead', 'dsa', 3, 6),
('s', 's', 'Lead', 'las', 4, 6),
('latest', 'latest', 'Lead', 'latest', 5, 6),
('a', 'a', 'Lead', 'a', 6, 7),
('b', 'b', 'Participate', 'b', 7, 7),
('2222', '3', 'ç‰µå¤´', '3', 8, 13),
('Test Databank', 'Test Databank', 'å‚åŠ ', 'Test Databank', 9, 65);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `notification_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `project_name` text NOT NULL,
  `project_owner` text NOT NULL,
  `todo` text NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`notification_id`, `role_id`, `project_id`, `title`, `email`, `project_name`, `project_owner`, `todo`, `created_at`) VALUES
(1, 3, 60, 'æäº¤é¡¹ç›®ç”³è¯·', 'TFO@admin.com', 'æµ‹è¯•é¡¹ç›®5', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 08:47:19'),
(2, 14, 60, 'æäº¤é¡¹ç›®ç”³è¯·', 'all@test.com', 'æµ‹è¯•é¡¹ç›®5', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 08:47:19'),
(3, 27, 60, 'æäº¤é¡¹ç›®ç”³è¯·', 'shiyanjigou', 'æµ‹è¯•é¡¹ç›®5', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 08:47:19'),
(4, 27, 62, 'æäº¤é¡¹ç›®ç”³è¯·', 'shiyanjigou', 'æµ‹è¯•é¡¹ç›®6', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 09:11:36'),
(5, 3, 62, 'æäº¤é¡¹ç›®ç”³è¯·', 'TFO@admin.com', 'æµ‹è¯•é¡¹ç›®6', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 09:11:36'),
(6, 14, 62, 'æäº¤é¡¹ç›®ç”³è¯·', 'all@test.com', 'æµ‹è¯•é¡¹ç›®6', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 09:11:36'),
(7, 14, 63, 'æäº¤é¡¹ç›®ç”³è¯·', 'all@test.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 09:17:29'),
(8, 27, 63, 'æäº¤é¡¹ç›®ç”³è¯·', 'shiyanjigou', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 09:17:29'),
(9, 3, 63, 'æäº¤é¡¹ç›®ç”³è¯·', '857357680@qq.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 09:17:29'),
(10, 3, 64, 'æäº¤é¡¹ç›®ç”³è¯·', '857357680@qq.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 09:29:42'),
(11, 14, 64, 'æäº¤é¡¹ç›®ç”³è¯·', 'all@test.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 09:29:42'),
(12, 27, 64, 'æäº¤é¡¹ç›®ç”³è¯·', 'shiyanjigou', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 09:29:42'),
(13, 3, 61, 'æäº¤é¡¹ç›®ç”³è¯·', '857357680@qq.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 09:38:37'),
(14, 14, 61, 'æäº¤é¡¹ç›®ç”³è¯·', 'all@test.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 09:38:37'),
(15, 27, 61, 'æäº¤é¡¹ç›®ç”³è¯·', 'shiyanjigou', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 09:38:37'),
(16, 2, 64, 'è¯•éªŒæœºæž„å®¡æ ¸é€šè¿‡', 'PI@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 09:41:53'),
(17, 6, 64, 'ä¸»è¦ç ”ç©¶è€…å®¡æ ¸é€šè¿‡', 'I@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·ä¿®æ”¹å¹¶ä¸Šä¼ æ–°çš„é¡¹ç›®æ–¹æ¡ˆ', '2017-02-09 09:43:54'),
(18, 5, 64, 'ä¸»è¦ç ”ç©¶è€…å®¡æ ¸é€šè¿‡', 'PRE@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·æ·»åŠ è¯„è®®æ„è§', '2017-02-09 09:43:54'),
(19, 6, 64, 'ç”³åŠžè€…åŒæ„æ–°æ–¹æ¡ˆ', 'I@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·ä¸Šä¼ åŒæ–¹ç­¾å­—ç›–ç« æ–¹æ¡ˆåŠå…¶ä»–èµ„æ–™æ‰«æä»¶åˆ°é¡¹ç›®èµ„æ–™æ¸…å•', '2017-02-09 09:51:36'),
(20, 14, 64, 'ç ”ç©¶è€…ä¸Šä¼ åŒæ–¹ç­¾å­—ç›–ç« æ–¹æ¡ˆåŠå…¶ä»–èµ„æ–™æ‰«æä»¶', 'all@test.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·æäº¤é¡¹ç›®ä¿¡æ¯åˆ°ä¼¦ç†å§”å‘˜ä¼š', '2017-02-09 09:55:33'),
(21, 27, 64, 'ç ”ç©¶è€…ä¸Šä¼ åŒæ–¹ç­¾å­—ç›–ç« æ–¹æ¡ˆåŠå…¶ä»–èµ„æ–™æ‰«æä»¶', 'shiyanjigou', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·æäº¤é¡¹ç›®ä¿¡æ¯åˆ°ä¼¦ç†å§”å‘˜ä¼š', '2017-02-09 09:55:33'),
(22, 3, 64, 'ç ”ç©¶è€…ä¸Šä¼ åŒæ–¹ç­¾å­—ç›–ç« æ–¹æ¡ˆåŠå…¶ä»–èµ„æ–™æ‰«æä»¶', '857357680@qq.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·æäº¤é¡¹ç›®ä¿¡æ¯åˆ°ä¼¦ç†å§”å‘˜ä¼š', '2017-02-09 09:55:33'),
(23, 20, 64, 'æäº¤åˆ°ä¼¦ç†å§”å‘˜ä¼šå®¡æ ¸', 'ECS@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 09:55:58'),
(24, 44, 64, 'æäº¤åˆ°ä¼¦ç†å§”å‘˜ä¼šå®¡æ ¸', 'lunliweiyuanhuimishu', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 09:55:58'),
(25, 36, 64, 'ä¼¦ç†å§”å‘˜ä¼šç§˜ä¹¦å®¡æ ¸é€šè¿‡', 'lunliweiyuanhuiweiyuan', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·æ·»åŠ ä¿®æ”¹æ„è§', '2017-02-09 09:56:24'),
(26, 7, 64, 'ä¼¦ç†å§”å‘˜ä¼šç§˜ä¹¦å®¡æ ¸é€šè¿‡', 'ECM@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·æ·»åŠ ä¿®æ”¹æ„è§', '2017-02-09 09:56:24'),
(27, 20, 64, 'ä¼¦ç†å§”å‘˜ä¼šæˆå‘˜æä¾›å»ºè®®', 'ECS@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·æ ¹æ®ä¼¦ç†å§”å‘˜ä¼šä¼šè®®è®°å½•å’Œåœ¨çº¿æ„è§æ•´ç†å‡ºæ„è§è¡¨å¹¶ä¸Šä¼ åˆ°é¡¹ç›®èµ„æ–™', '2017-02-09 09:56:55'),
(28, 44, 64, 'ä¼¦ç†å§”å‘˜ä¼šæˆå‘˜æä¾›å»ºè®®', 'lunliweiyuanhuimishu', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·æ ¹æ®ä¼¦ç†å§”å‘˜ä¼šä¼šè®®è®°å½•å’Œåœ¨çº¿æ„è§æ•´ç†å‡ºæ„è§è¡¨å¹¶ä¸Šä¼ åˆ°é¡¹ç›®èµ„æ–™', '2017-02-09 09:56:55'),
(29, 6, 64, 'å·²ä¸Šä¼ ä¼¦ç†å§”å‘˜ä¼šä¼šè®®è®°å½•å’Œæ„è§', 'I@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·ä¿®æ”¹å¹¶ä¸Šä¼ æ–°çš„é¡¹ç›®æ–¹æ¡ˆ', '2017-02-09 09:57:46'),
(30, 4, 64, 'ç ”ç©¶è€…ä¸Šä¼ æ–°æ–¹æ¡ˆ', 'ECC@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'N/A', '2017-02-09 09:58:30'),
(31, 18, 64, 'ç ”ç©¶è€…ä¸Šä¼ æ–°æ–¹æ¡ˆ', 'all@test.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'N/A', '2017-02-09 09:58:30'),
(32, 37, 64, 'ç ”ç©¶è€…ä¸Šä¼ æ–°æ–¹æ¡ˆ', 'lunliweiyuanhuizhuren', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'N/A', '2017-02-09 09:58:30'),
(33, 20, 64, 'ä¼¦ç†å§”å‘˜ä¼šåŒæ„æ–°æ–¹æ¡ˆ', 'ECS@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·ä¸Šä¼ å„ä¸ªä¼¦ç†å§”å‘˜ä¼šå§”å‘˜ã€ä¸»ä»»ç­¾åçš„æ‰¹å‡†æ–‡ä»¶åˆ°é¡¹ç›®èµ„æ–™', '2017-02-09 09:58:48'),
(34, 44, 64, 'ä¼¦ç†å§”å‘˜ä¼šåŒæ„æ–°æ–¹æ¡ˆ', 'lunliweiyuanhuimishu', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'è¯·ä¸Šä¼ å„ä¸ªä¼¦ç†å§”å‘˜ä¼šå§”å‘˜ã€ä¸»ä»»ç­¾åçš„æ‰¹å‡†æ–‡ä»¶åˆ°é¡¹ç›®èµ„æ–™', '2017-02-09 09:58:48'),
(35, 2, 64, '<span style=''color:magenta''>status_13_0</span>', 'PI@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'N/A', '2017-02-09 09:59:35'),
(36, 6, 64, '<span style=''color:magenta''>status_13_0</span>', 'I@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', 'N/A', '2017-02-09 09:59:36'),
(37, 3, 65, 'æäº¤é¡¹ç›®ç”³è¯·', '857357680@qq.com', 'Test Databank', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 10:04:49'),
(38, 14, 65, 'æäº¤é¡¹ç›®ç”³è¯·', 'all@test.com', 'Test Databank', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 10:04:49'),
(39, 27, 65, 'æäº¤é¡¹ç›®ç”³è¯·', 'shiyanjigou', 'Test Databank', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 10:04:49'),
(40, 2, 65, 'è¯•éªŒæœºæž„å®¡æ ¸é€šè¿‡', 'PI@admin.com', 'Test Databank', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 10:11:11'),
(41, 6, 65, 'ä¸»è¦ç ”ç©¶è€…å®¡æ ¸é€šè¿‡', 'I@admin.com', 'Test Databank', 'Enzo', 'è¯·ä¿®æ”¹å¹¶ä¸Šä¼ æ–°çš„é¡¹ç›®æ–¹æ¡ˆ', '2017-02-09 10:11:44'),
(42, 5, 65, 'ä¸»è¦ç ”ç©¶è€…å®¡æ ¸é€šè¿‡', 'PRE@admin.com', 'Test Databank', 'Enzo', 'è¯·æ·»åŠ è¯„è®®æ„è§', '2017-02-09 10:11:44'),
(43, 19, 65, 'ä¸»è¦ç ”ç©¶è€…å®¡æ ¸é€šè¿‡', 'all@test.com', 'Test Databank', 'Enzo', 'è¯·æ·»åŠ è¯„è®®æ„è§', '2017-02-09 10:11:44'),
(44, 6, 65, 'ç”³åŠžè€…åŒæ„æ–°æ–¹æ¡ˆ', 'I@admin.com', 'Test Databank', 'Enzo', 'è¯·ä¸Šä¼ åŒæ–¹ç­¾å­—ç›–ç« æ–¹æ¡ˆåŠå…¶ä»–èµ„æ–™æ‰«æä»¶åˆ°é¡¹ç›®èµ„æ–™æ¸…å•', '2017-02-09 10:21:57'),
(45, 27, 66, 'æäº¤é¡¹ç›®ç”³è¯·', 'shiyanjigou', 'ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„IæœŸä¸´åºŠç ”ç©¶', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 02:36:33'),
(46, 3, 66, 'æäº¤é¡¹ç›®ç”³è¯·', '857357680@qq.com', 'ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„IæœŸä¸´åºŠç ”ç©¶', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 02:36:33'),
(47, 14, 66, 'æäº¤é¡¹ç›®ç”³è¯·', 'all@test.com', 'ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„IæœŸä¸´åºŠç ”ç©¶', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 02:36:33'),
(48, 23, 66, 'è¯•éªŒæœºæž„å®¡æ ¸é€šè¿‡', '123', 'ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„IæœŸä¸´åºŠç ”ç©¶', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 02:37:37'),
(49, 28, 66, 'è¯•éªŒæœºæž„å®¡æ ¸é€šè¿‡', '857357680@qq.com', 'ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„IæœŸä¸´åºŠç ”ç©¶', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 02:37:37'),
(50, 14, 67, 'æäº¤é¡¹ç›®ç”³è¯·', 'all@test.com', 'ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„IæœŸä¸´åºŠç ”ç©¶', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 02:47:35'),
(51, 27, 67, 'æäº¤é¡¹ç›®ç”³è¯·', 'shiyanjigou', 'ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„IæœŸä¸´åºŠç ”ç©¶', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 02:47:35'),
(52, 3, 67, 'æäº¤é¡¹ç›®ç”³è¯·', '857357680@qq.com', 'ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„IæœŸä¸´åºŠç ”ç©¶', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 02:47:35'),
(53, 2, 67, 'è¯•éªŒæœºæž„å®¡æ ¸é€šè¿‡', 'PI@admin.com', 'ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„IæœŸä¸´åºŠç ”ç©¶', 'Enzo', 'è¯·å®¡æ ¸é¡¹ç›®', '2017-02-09 02:47:54'),
(54, 6, 67, 'ä¸»è¦ç ”ç©¶è€…å®¡æ ¸é€šè¿‡', 'I@admin.com', 'ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„IæœŸä¸´åºŠç ”ç©¶', 'Enzo', 'è¯·ä¿®æ”¹å¹¶ä¸Šä¼ æ–°çš„é¡¹ç›®æ–¹æ¡ˆ', '2017-02-09 02:49:26'),
(55, 5, 67, 'ä¸»è¦ç ”ç©¶è€…å®¡æ ¸é€šè¿‡', 'PRE@admin.com', 'ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„IæœŸä¸´åºŠç ”ç©¶', 'Enzo', 'è¯·æ·»åŠ è¯„è®®æ„è§', '2017-02-09 02:49:27');

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `position_id` int(5) NOT NULL,
  `description` varchar(100) NOT NULL,
  `is_professional_group` int(1) NOT NULL COMMENT '0 - not professional group, 1 - professional group role',
  `acronym` varchar(5) NOT NULL COMMENT 'short acronym for session conditions',
  `lang_phrase` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`position_id`, `description`, `is_professional_group`, `acronym`, `lang_phrase`) VALUES
(1, 'Applicant', 0, 'A', 'applicant'),
(2, 'Testing Facility OFfice', 0, 'TFO', 'tfo'),
(3, 'Principal Investigator', 1, 'PI', 'principal_investigator'),
(4, 'Investigator', 1, 'I', 'investigator'),
(5, 'Biological Sample Controller', 1, 'BGC', 'biological_sample_controller'),
(6, 'Medicine Controller', 1, 'MC', 'medicine_controller'),
(7, 'Clinician', 1, 'C', 'clinician'),
(8, 'Subjects Controller', 1, 'SC', 'subjects_controller'),
(9, 'Research Nurse', 1, 'RN', 'research_nurse'),
(10, 'Quality Controller', 1, 'QC', ''),
(11, 'Peer Review Expert', 0, 'PRE', 'pre'),
(12, 'Ethics Committee Chairman', 0, 'ECC', 'ecc'),
(13, 'Ethics Committee Member', 0, 'ECM', 'ecm'),
(14, 'Professional Group Admin', 1, 'PGA', ''),
(15, 'Ethics Committee Secretary', 0, 'ECS', 'ethics_committee_secretary'),
(16, 'Data Administrator', 1, 'DA', 'data_administrator');

-- --------------------------------------------------------

--
-- Table structure for table `professional_group`
--

CREATE TABLE `professional_group` (
  `pgroup_id` int(11) NOT NULL,
  `pgroup_name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL,
  `pgroup_admin_id` tinyint(5) NOT NULL DEFAULT '0' COMMENT 'user_id of the one who administrates this pgroup'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `professional_group`
--

INSERT INTO `professional_group` (`pgroup_id`, `pgroup_name`, `date_created`, `date_modified`, `pgroup_admin_id`) VALUES
(1, 'Professional Group 1', '2016-11-04 23:47:57', '2016-11-05 00:18:29', 1),
(2, 'Professional Group 2', '2016-11-04 23:48:38', '2016-11-17 10:19:29', 12),
(3, 'Professional Group 3', '2016-11-04 23:48:38', '2016-11-10 01:43:33', 2),
(4, 'Professional Group 4', '2016-11-04 23:48:38', '2016-11-17 10:19:32', 14),
(5, 'Professional Group 5', '2016-11-04 23:48:38', '2016-11-17 10:19:33', 18),
(6, 'æŽä¸»ä»»é¡¹ç›®ç»„', '2017-01-12 02:08:38', '0000-00-00 00:00:00', 49);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL,
  `status_id` int(11) DEFAULT NULL,
  `lot_num` varchar(255) DEFAULT NULL,
  `project_num` varchar(255) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT 'è¯·è¾“å…¥é¡¹ç›®åç§°',
  `trial_id` int(11) DEFAULT NULL,
  `indications` varchar(255) DEFAULT NULL,
  `risk_category` int(1) DEFAULT NULL COMMENT '0 - low, 1 - mid, 2 - high',
  `dept_id` int(11) DEFAULT NULL COMMENT 'deprecated, just left it here for backup purposes',
  `dept_name` varchar(255) NOT NULL,
  `pgroup_id` int(11) DEFAULT NULL,
  `pi_id` int(11) DEFAULT NULL COMMENT 'fk from users table',
  `pgroup_contact_id` int(5) NOT NULL DEFAULT '0' COMMENT 'Anyone from the pgroup',
  `user_id` int(11) NOT NULL COMMENT 'id of applicant from users table',
  `emergency_contact` varchar(255) DEFAULT NULL,
  `emergency_mobile` varchar(255) DEFAULT NULL,
  `emergency_email` varchar(255) DEFAULT NULL,
  `cro_name` varchar(255) DEFAULT NULL,
  `cro_contact` varchar(255) DEFAULT NULL,
  `cro_mobile` varchar(255) DEFAULT NULL,
  `cro_email` varchar(255) DEFAULT NULL,
  `research_objectives` mediumtext,
  `subsidize_type` int(1) DEFAULT NULL COMMENT '0 - no subsidize, 1 - partially funded, 2 - Fully funded',
  `remarks` mediumtext,
  `clinician_id` varchar(70) NOT NULL DEFAULT '0',
  `bio_controller_id` varchar(70) NOT NULL DEFAULT '0',
  `nurse_id` varchar(70) NOT NULL DEFAULT '0',
  `med_controller_id` varchar(70) NOT NULL DEFAULT '0',
  `subj_controller_id` varchar(70) NOT NULL DEFAULT '0',
  `project_status` int(11) NOT NULL DEFAULT '0' COMMENT '0 - draft, 1 - submitted/published, 2 - rejected/back to start',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `status_id`, `lot_num`, `project_num`, `project_name`, `trial_id`, `indications`, `risk_category`, `dept_id`, `dept_name`, `pgroup_id`, `pi_id`, `pgroup_contact_id`, `user_id`, `emergency_contact`, `emergency_mobile`, `emergency_email`, `cro_name`, `cro_contact`, `cro_mobile`, `cro_email`, `research_objectives`, `subsidize_type`, `remarks`, `clinician_id`, `bio_controller_id`, `nurse_id`, `med_controller_id`, `subj_controller_id`, `project_status`, `date_created`, `date_modified`) VALUES
(1, 14, '1', '23', 'æµ‹è¯•é¡¹ç›®', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', 'æŽæ˜Ž', '', '', '', '', 0, '', '*8*', '*11*', '*9*', '*10*', '0', 1, '2016-12-22 23:25:57', '0000-00-00 00:00:00'),
(2, 19, 'VAN', 'VAN', 'VAN', 2, '123', 1, NULL, '123123', 1, 48, 48, 2, 'asdasd', 'asdasd', 'asd', 'asd', 'asd', 'asd', 'asd', 'asd', 0, 'asd', '0', '0', '0', '0', '0', 1, '2016-12-23 17:14:46', '0000-00-00 00:00:00'),
(3, 68, '34', '12', 'æµ‹è¯•é¡¹ç›®2', 3, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', 'é˜¿é“å¤«', '', '', '', '', 0, '', '*8*', '*11*', '*9*', '*10*', '0', 1, '2016-12-23 22:38:06', '0000-00-00 00:00:00'),
(4, 232, '123', '123', 'VanProj', 1, '1', 0, NULL, '1', 1, 48, 48, 2, '1', '1', '1', '111', '1', '1', '1', '1', 0, '1', '0', '0', '0', '0', '0', 1, '2016-12-23 23:16:37', '0000-00-00 00:00:00'),
(5, 99, '', '12', 'æµ‹è¯•é¡¹ç›®3', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '111', '', '', '', '', 0, '', '*8*', '*11*', '*9*', '*10*', '0', 1, '2016-12-26 03:11:52', '0000-00-00 00:00:00'),
(6, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2016-12-28 15:26:54', '0000-00-00 00:00:00'),
(7, 66, '', '', 'Project Test', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '*8*', '*11*', '9', '*10*', '0', 1, '2016-12-28 19:15:23', '0000-00-00 00:00:00'),
(8, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2016-12-29 17:42:17', '0000-00-00 00:00:00'),
(9, 103, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°sssss', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2016-12-30 18:42:46', '0000-00-00 00:00:00'),
(10, 69, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2016-12-30 18:49:24', '0000-00-00 00:00:00'),
(11, 70, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2016-12-30 19:00:05', '0000-00-00 00:00:00'),
(12, 84, '', '', 'æµ‹è¯•é¡¹ç›®', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '*8*', '*11*', '9', '*10*', '0', 1, '2016-12-30 19:05:50', '0000-00-00 00:00:00'),
(13, 98, '123', '2', '22', 1, '33', 0, NULL, '3', 1, 48, 48, 2, '3', '3', '3', '5', '55', '5', '5', '5', 0, 'è‹¥5', '*8*', '*11*', '9', '*10*', '0', 1, '2016-12-30 21:10:14', '0000-00-00 00:00:00'),
(14, 102, '', '', 'æµ‹è¯•é¡¹ç›®1', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-07 17:13:17', '0000-00-00 00:00:00'),
(15, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-09 22:15:30', '0000-00-00 00:00:00'),
(16, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-09 22:16:41', '0000-00-00 00:00:00'),
(17, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-09 23:08:36', '0000-00-00 00:00:00'),
(18, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-09 23:09:43', '0000-00-00 00:00:00'),
(19, 167, '', '', 'æµ‹è¯•é¡¹ç›®2', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '2', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-10 15:32:15', '0000-00-00 00:00:00'),
(20, 117, '', '', 'æµ‹è¯•é¡¹ç›®3', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, 'é˜¿é“å¤«', '0', '0', '0', '0', '0', 1, '2017-01-10 15:48:33', '0000-00-00 00:00:00'),
(21, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-10 15:55:48', '0000-00-00 00:00:00'),
(22, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-10 15:57:20', '0000-00-00 00:00:00'),
(23, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-10 18:03:28', '0000-00-00 00:00:00'),
(24, 233, '1', '1', '1', 1, '1', 0, NULL, '11', 1, 48, 48, 2, '1', '1', '11', '1', '1', '1', '1', '1', 0, '1', '0', '0', '0', '0', '0', 1, '2017-01-10 18:11:30', '0000-00-00 00:00:00'),
(25, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-10 22:30:32', '0000-00-00 00:00:00'),
(26, 130, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-10 22:59:39', '0000-00-00 00:00:00'),
(27, 152, '2010L02531', '001', 'ç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡äººä½“ç”Ÿç‰©åˆ©ç”¨åº¦å’Œç”Ÿç‰©ç­‰æ•ˆæ€§ç ”ç©¶', 1, 'ç³–å°¿ç—…IIæœŸ', 0, NULL, 'å†…åˆ†æ³Œç§‘', 1, 48, 48, 2, 'é™ˆå« é»„æ™“ä¼Ÿ', '0755-25700034ï¼Œä¼ çœŸï¼š0755-25708020', 'abdcd-112@163.com', 'æ— ', 'æ— ', 'æ— ', 'æ— ', 'é€šè¿‡ç ”ç©¶å¥åº·ç”·æ€§å—è¯•è€…å•æ¬¡ç©ºè…¹å£æœæ·±åœ³å¸‚ä¸­è”åˆ¶è¯æœ‰é™å…¬å¸ç ”åˆ¶çš„ç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡çš„è¯ä»£åŠ¨åŠ›å­¦ç‰¹å¾ï¼Œç‰¹åˆ«æ˜¯å¯¹è¯ç‰©çš„å¸æ”¶è¿‡ç¨‹çš„å½±å“ï¼Œæ¯”è¾ƒè¿™ç§ç»™è¯æƒ…å†µä¸‹çš„ç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡ä¸Žå‚æ¯”åˆ¶å‰‚æ ¼åŽæ­¢ï¼ˆç›é…¸äºŒç”²åŒèƒç‰‡ï¼‰çš„äººä½“ç”Ÿç‰©åˆ©ç”¨åº¦å’Œç”Ÿç‰©ç­‰æ•ˆæ€§ï¼Œä¸ºå…¶ä¸´åºŠåˆç†ç”¨è¯æä¾›ä¾æ®ã€‚', 0, 'æ— ', '*8*', '*11*', '9', '*10*', '0', 1, '2017-01-10 23:03:31', '0000-00-00 00:00:00'),
(28, 149, '', '001', 'æµ‹è¯•é¡¹ç›®4', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '*8*', '*11*', '9', '*10*', '0', 1, '2017-01-11 21:29:26', '0000-00-00 00:00:00'),
(29, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-12 21:14:42', '0000-00-00 00:00:00'),
(30, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-14 22:55:56', '0000-00-00 00:00:00'),
(31, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-16 17:28:47', '0000-00-00 00:00:00'),
(32, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-18 21:32:25', '0000-00-00 00:00:00'),
(33, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-20 04:34:09', '0000-00-00 00:00:00'),
(34, 154, '2010L02531', '001', 'ç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡äººä½“ç”Ÿç‰©åˆ©ç”¨åº¦å’Œç”Ÿç‰©ç­‰æ•ˆæ€§ç ”ç©¶', 5, 'IIåž‹ç³–å°¿ç—…', 0, NULL, 'å†…åˆ†æ³Œç§‘', 6, 73, 73, 65, 'é™ˆå« é»„æ™“ä¼Ÿ', '0755-25700034ï¼Œä¼ çœŸï¼š0755-25708020', 'live99pei@163.com', 'æ— ', 'æ— ', 'æ— ', 'æ— ', '    é€šè¿‡ç ”ç©¶å¥åº·ç”·æ€§å—è¯•è€…å•æ¬¡ç©ºè…¹å£æœæ·±åœ³å¸‚ä¸­è”åˆ¶è¯æœ‰é™å…¬å¸ç ”åˆ¶çš„ç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡çš„è¯ä»£åŠ¨åŠ›å­¦ç‰¹å¾ï¼Œç‰¹åˆ«æ˜¯å¯¹è¯ç‰©çš„å¸æ”¶è¿‡ç¨‹çš„å½±å“ï¼Œæ¯”è¾ƒè¿™ç§ç»™è¯æƒ…å†µä¸‹çš„ç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡ä¸Žå‚æ¯”åˆ¶å‰‚æ ¼åŽæ­¢ï¼ˆç›é…¸äºŒç”²åŒèƒç‰‡ï¼‰çš„äººä½“ç”Ÿç‰©åˆ©ç”¨åº¦å’Œç”Ÿç‰©ç­‰æ•ˆæ€§ï¼Œä¸ºå…¶ä¸´åºŠåˆç†ç”¨è¯æä¾›ä¾æ®ã€‚', 0, 'æ— ', '0', '0', '0', '0', '0', 1, '2017-01-20 13:42:01', '0000-00-00 00:00:00'),
(35, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-20 17:06:29', '0000-00-00 00:00:00'),
(36, 187, '', '', 'æµ‹è¯•é¡¹ç›®', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-22 15:03:46', '0000-00-00 00:00:00'),
(37, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-22 15:32:40', '0000-00-00 00:00:00'),
(38, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-22 15:36:59', '0000-00-00 00:00:00'),
(39, 169, '', '', 'æµ‹è¯•', 1, '', 0, NULL, '', 1, 48, 48, 65, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-22 15:38:22', '0000-00-00 00:00:00'),
(40, 158, '', '', 'æµ‹è¯•1', 2, '', 0, NULL, '', 1, 48, 48, 65, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-22 15:38:50', '0000-00-00 00:00:00'),
(41, 159, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 3, '', 0, NULL, '', 1, 48, 48, 65, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-22 15:39:16', '0000-00-00 00:00:00'),
(42, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-22 15:39:33', '0000-00-00 00:00:00'),
(43, 160, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 4, '', 0, NULL, '', 1, 48, 48, 65, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-22 15:39:50', '0000-00-00 00:00:00'),
(44, 161, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 5, '', 0, NULL, '', 1, 48, 48, 65, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-22 15:40:11', '0000-00-00 00:00:00'),
(45, 162, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 6, '', 0, NULL, '', 1, 48, 48, 65, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-22 15:41:18', '0000-00-00 00:00:00'),
(46, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-22 15:41:18', '0000-00-00 00:00:00'),
(47, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-22 15:41:18', '0000-00-00 00:00:00'),
(48, 163, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 7, '', 0, NULL, '', 1, 48, 48, 65, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-22 15:42:07', '0000-00-00 00:00:00'),
(49, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-22 16:30:14', '0000-00-00 00:00:00'),
(50, 164, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-25 17:47:24', '0000-00-00 00:00:00'),
(51, NULL, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 0, '2017-01-25 17:52:55', '0000-00-00 00:00:00'),
(52, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-02-04 23:27:05', '0000-00-00 00:00:00'),
(53, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-02-05 22:32:35', '0000-00-00 00:00:00'),
(54, 182, '', '', 'æ˜¯è¿™ä¸ª', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-07 00:25:13', '0000-00-00 00:00:00'),
(55, 183, '', '', 'æ˜¯ä½ äº†', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-07 22:50:25', '0000-00-00 00:00:00'),
(56, 186, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-07 22:55:50', '0000-00-00 00:00:00'),
(57, 200, '', '', 'é¡¹ç›®1', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '*11*', '0', '0', '0', 1, '2017-02-08 16:14:17', '0000-00-00 00:00:00'),
(58, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-02-09 01:48:27', '0000-00-00 00:00:00'),
(59, 201, '', '', 'Test this', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-09 02:18:43', '0000-00-00 00:00:00'),
(60, 202, '', '', 'æµ‹è¯•é¡¹ç›®5', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-09 14:45:08', '0000-00-00 00:00:00'),
(61, 206, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-09 14:56:51', '0000-00-00 00:00:00'),
(62, 203, '', '', 'æµ‹è¯•é¡¹ç›®6', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-09 15:09:34', '0000-00-00 00:00:00'),
(63, 204, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 6, 73, 73, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-09 15:14:28', '0000-00-00 00:00:00'),
(64, 219, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '*48*', 1, '2017-02-09 15:22:00', '0000-00-00 00:00:00'),
(65, 224, 'Test Databank', 'Tes', 'Test Databank', 2, 'Test Databank', 1, NULL, 'Test Databank', 1, 48, 48, 2, 'Test Databank', 'Test Databank', 'Test Databank', 'Test Databank', 'Test Databank', 'Test Databank', 'Test Databank', 'Test Databank', 0, 'Test Databank', '0', '0', '0', '0', '0', 1, '2017-02-09 16:02:33', '0000-00-00 00:00:00'),
(66, 227, '2007L04204', '011', 'ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„IæœŸä¸´åºŠç ”ç©¶', 1, 'è¯¥é¡¹ç›®å°†ä¸»è¦é’ˆå¯¹äºŽæ¶æ€§æ·‹å·´ç˜¤ï¼Œæ­¤é€‚åº”ç—‡ä¸ºä¼´æœ‰é«˜é¢‘çŽ‡çš„PI3KÎ´äºšåž‹ã€‚', 0, NULL, 'è‚¿ç˜¤å†…ç§‘', 6, 73, 73, 2, 'æ¯›ä¼Ÿ', '0575-86021692ï¼Œ13905856345', 'maoweixc@hotmail.com', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-09 20:14:46', '0000-00-00 00:00:00'),
(67, 231, '2007L04204', '012', 'ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„IæœŸä¸´åºŠç ”ç©¶', 1, 'è¯¥é¡¹ç›®å°†ä¸»è¦é’ˆå¯¹äºŽæ¶æ€§æ·‹å·´ç˜¤ï¼Œæ­¤é€‚åº”ç—‡ä¸ºä¼´æœ‰é«˜é¢‘çŽ‡çš„PI3KÎ´äºšåž‹ã€‚', 0, NULL, 'è‚¿ç˜¤å†…ç§‘', 1, 48, 48, 2, 'æ¯›ä¼Ÿ', '0575-86021692ï¼Œ13905856345', 'maoweixc@hotmail.com', '', '', '', '', 'ä¸»è¦ç›®æ ‡ï¼šç¡®å®šç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„RP2D\r\n    æ¬¡è¦ç›®æ ‡ï¼šèŽ·å–ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡æœ‰æ•ˆæ€§çš„åˆæ­¥è¯æ®ã€‚\r\n', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-09 20:44:36', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `projects_pre`
--

CREATE TABLE `projects_pre` (
  `pre_id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL COMMENT 'role id of PRE',
  `reviewer_id` int(5) NOT NULL COMMENT 'one who saved',
  `project_id` int(5) NOT NULL COMMENT 'project_id',
  `already_uploaded` int(1) NOT NULL DEFAULT '0' COMMENT 'if PRE uploaded'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects_pre`
--

INSERT INTO `projects_pre` (`pre_id`, `role_id`, `reviewer_id`, `project_id`, `already_uploaded`) VALUES
(1, 5, 2, 1, 0),
(2, 19, 2, 1, 0),
(3, 5, 2, 2, 0),
(4, 19, 2, 2, 0),
(5, 5, 2, 3, 0),
(6, 19, 2, 3, 0),
(7, 5, 2, 5, 0),
(8, 19, 2, 5, 0),
(9, 5, 2, 7, 0),
(10, 5, 2, 12, 0),
(11, 19, 2, 12, 0),
(12, 5, 2, 13, 0),
(13, 19, 2, 13, 0),
(14, 5, 2, 14, 0),
(15, 19, 2, 14, 0),
(16, 5, 2, 20, 0),
(17, 19, 2, 20, 0),
(18, 5, 2, 19, 0),
(19, 19, 2, 19, 0),
(20, 5, 2, 27, 0),
(21, 19, 2, 27, 0),
(22, 5, 2, 28, 0),
(23, 19, 2, 28, 0),
(24, 24, 2, 39, 0),
(25, 5, 2, 54, 0),
(26, 24, 2, 57, 0),
(27, 35, 2, 57, 0),
(28, 5, 2, 64, 0),
(29, 5, 2, 65, 0),
(30, 19, 2, 65, 0),
(31, 5, 2, 67, 0);

-- --------------------------------------------------------

--
-- Table structure for table `project_remarks`
--

CREATE TABLE `project_remarks` (
  `remarks_id` int(5) NOT NULL,
  `remarks` varchar(300) NOT NULL,
  `status_id` int(5) NOT NULL COMMENT 'correlate with status table to track when and who inputted the comment',
  `project_id` int(5) NOT NULL COMMENT 'project_id from projects table',
  `remark_type` int(11) NOT NULL DEFAULT '0' COMMENT '0 - rejected remarks, 1 - add document remarks, 2 - ECM remarks'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_remarks`
--

INSERT INTO `project_remarks` (`remarks_id`, `remarks`, `status_id`, `project_id`, `remark_type`) VALUES
(1, 'hahaha ', 9, 1, 2),
(2, 'Hello', 16, 2, 1),
(3, 'hahaha', 44, 5, 2),
(4, 'tfo', 51, 7, 1),
(5, 'æ„è§', 79, 12, 2),
(6, 'åŒæ„ï¼Œã€‚ã€‚ã€‚ã€‚ã€‚ã€‚', 93, 13, 2),
(7, 'å˜»å˜»æŒºå¥½çš„', 117, 20, 2),
(8, '1ã€æ²¡æœ‰æŸå®³å—è¯•è€…çš„åˆ©ç›Š\r\n2ã€è¡¥å¿æ–¹æ¡ˆå®Œå–„\r\nåŒæ„å®žæ–½ã€‚', 128, 27, 2),
(9, 'sd', 129, 26, 0),
(10, 'hh', 144, 28, 2),
(11, 'Nope', 165, 36, 0),
(12, 'adf', 178, 54, 2),
(13, 'reject test', 185, 56, 0),
(14, 'hhh', 196, 57, 2),
(15, 'é˜¿é“å¤«', 214, 64, 2);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'can duplicate',
  `position_id` int(11) NOT NULL,
  `pgroup_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `user_id`, `position_id`, `pgroup_id`) VALUES
(1, 2, 1, 0),
(2, 48, 3, 1),
(3, 49, 2, 0),
(4, 50, 12, 0),
(5, 51, 11, 0),
(6, 52, 4, 1),
(7, 55, 13, 0),
(8, 56, 7, 1),
(9, 57, 9, 1),
(10, 58, 6, 1),
(11, 59, 5, 1),
(13, 61, 1, 0),
(14, 62, 2, 0),
(18, 62, 12, 0),
(19, 62, 11, 0),
(20, 63, 15, 0),
(21, 64, 16, 1),
(22, 65, 1, 0),
(23, 73, 3, 6),
(24, 73, 11, 0),
(25, 74, 11, 0),
(26, 75, 1, 0),
(27, 88, 2, 0),
(28, 76, 3, 6),
(29, 76, 5, 6),
(30, 79, 5, 6),
(31, 78, 4, 6),
(32, 80, 6, 6),
(33, 81, 7, 6),
(34, 82, 9, 6),
(35, 84, 11, 0),
(36, 87, 13, 0),
(37, 86, 12, 0),
(38, 89, 1, 0),
(39, 90, 1, 0),
(40, 91, 1, 0),
(41, 92, 1, 0),
(42, 93, 1, 0),
(43, 94, 1, 0),
(44, 85, 15, 0),
(45, 74, 7, 6),
(46, 74, 9, 6),
(47, 95, 8, 1),
(48, 63, 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `side_effect`
--

CREATE TABLE `side_effect` (
  `side_effect_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL COMMENT 'the id of the subject with side effects',
  `lot_num` varchar(50) NOT NULL,
  `num` varchar(50) NOT NULL,
  `report_type` varchar(50) NOT NULL COMMENT '0 - First Report, 1 - Follow-up Report, 2 - Sum Up Report',
  `report_time` datetime NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `hospital_mobile` varchar(50) NOT NULL,
  `dept` varchar(150) NOT NULL,
  `dept_mobile` varchar(150) NOT NULL,
  `drug_chinese_name` varchar(200) NOT NULL,
  `drug_english_name` varchar(200) NOT NULL,
  `drug_type` tinyint(1) NOT NULL COMMENT '0 -Traditional Chinese medicine, 1 - Chemistry Drug, 2 - Cure medicine, 3 - Prevent medicine, 4- Other ',
  `drug_form` varchar(200) NOT NULL,
  `drug_register` varchar(200) NOT NULL,
  `clinical_trial` tinyint(1) NOT NULL COMMENT '0 - I stage,  1 - II stage,  2 - III stage,  3 - IV stage,  4 - BE Trial,  5 - Clinical verifications',
  `clinical_indications` varchar(200) NOT NULL,
  `sub_initials` varchar(100) NOT NULL,
  `sub_gender` tinyint(1) NOT NULL COMMENT '0 - male, 1 - female',
  `sub_height` varchar(50) NOT NULL,
  `sub_weight` varchar(50) NOT NULL,
  `sub_birthdate` datetime NOT NULL,
  `sub_complication` varchar(150) NOT NULL COMMENT '0 - no, 1 - yes',
  `sae_diagnosis` varchar(150) NOT NULL,
  `sae_drug_measure` tinyint(1) NOT NULL COMMENT '0 - Continuantur remedia,  1 - Reduce the dosage,  2 - Stop then go',
  `sae_situation` tinyint(1) NOT NULL COMMENT '0 - Die,  1 - Be in hospital,  2 - Extend hospital,  3 - Disability,  4 - Dysfunction,  5 - Deformity,  6 - Life-threatening,  7 - Other,  8 - Severe',
  `relation_sae_drug` tinyint(1) NOT NULL COMMENT '0 - Certainty ,  1 - Maybe concern ,  2 - Be unconcerned ,  3 - Certainty uncincerned,  4 - Not Determinable',
  `sae_vest` tinyint(1) NOT NULL COMMENT '0 - Transference cure,  1 - Symptoms last',
  `sae_unbinding` tinyint(1) NOT NULL COMMENT '0 - No blinding,  1 - Did not unblinding,  2 - Unblinding',
  `sae_report_internal` tinyint(1) NOT NULL,
  `sae_report_external` tinyint(1) NOT NULL COMMENT '0 - Yes,  1 - No,  2 - No detail',
  `sae_handle_detail` varchar(250) NOT NULL,
  `report_unit` varchar(100) NOT NULL,
  `reporter_position` varchar(150) NOT NULL,
  `remark` varchar(250) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(5) NOT NULL,
  `date_updated` datetime NOT NULL,
  `updated_by` int(5) NOT NULL,
  `clinician_remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `side_effect`
--

INSERT INTO `side_effect` (`side_effect_id`, `project_id`, `subject_id`, `lot_num`, `num`, `report_type`, `report_time`, `hospital`, `hospital_mobile`, `dept`, `dept_mobile`, `drug_chinese_name`, `drug_english_name`, `drug_type`, `drug_form`, `drug_register`, `clinical_trial`, `clinical_indications`, `sub_initials`, `sub_gender`, `sub_height`, `sub_weight`, `sub_birthdate`, `sub_complication`, `sae_diagnosis`, `sae_drug_measure`, `sae_situation`, `relation_sae_drug`, `sae_vest`, `sae_unbinding`, `sae_report_internal`, `sae_report_external`, `sae_handle_detail`, `report_unit`, `reporter_position`, `remark`, `date_created`, `created_by`, `date_updated`, `updated_by`, `clinician_remarks`) VALUES
(1, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-22 09:57:37', 6, '0000-00-00 00:00:00', 0, ''),
(2, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-22 09:59:17', 6, '0000-00-00 00:00:00', 0, ''),
(3, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 07:24:12', 6, '0000-00-00 00:00:00', 0, ''),
(4, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 07:26:50', 6, '0000-00-00 00:00:00', 0, ''),
(5, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 07:27:19', 6, '0000-00-00 00:00:00', 0, ''),
(6, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 07:28:11', 6, '0000-00-00 00:00:00', 0, ''),
(7, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 07:31:43', 6, '0000-00-00 00:00:00', 0, ''),
(8, 3, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 09:16:21', 6, '0000-00-00 00:00:00', 0, ''),
(9, 3, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 09:16:40', 6, '0000-00-00 00:00:00', 0, ''),
(10, 3, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 09:53:18', 6, '0000-00-00 00:00:00', 0, ''),
(11, 3, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-25 13:43:52', 6, '0000-00-00 00:00:00', 0, ''),
(12, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-25 15:14:34', 6, '0000-00-00 00:00:00', 0, ''),
(13, 5, 0, '323', '2323', '0', '2016-12-25 00:00:00', '23234', '234', '2342', '34', '234', '234', 2, '234', '234', 0, '234', '434', 0, '123', '124', '2016-12-28 00:00:00', '0', '', 0, 0, 0, 0, 0, 0, 0, '', '2wdf', 'adf', 'adf', '2016-12-26 05:22:38', 6, '0000-00-00 00:00:00', 0, ''),
(14, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-25 15:24:03', 6, '0000-00-00 00:00:00', 0, ''),
(15, 5, 0, 'æ¸…é“å¤«', '12', '1', '2016-12-25 00:00:00', 'a''d''f', '', 'é˜¿é“å¤«', '', 'é˜¿é“å¤«', 'aæ°´ç”µè´¹', 1, 'é˜¿é“å¤«', 'å•Šæ°´ç”µè´¹', 0, 'é˜¿é“å¤«', 'é˜¿é“å¤«', 0, 'é˜¿é“å¤«1', '2', '2016-12-29 00:00:00', '0', '', 0, 0, 0, 0, 0, 0, 0, '', 'é˜¿é“å¤«', 'é˜¿é“å¤«a', 'aåœ°æ–¹', '2016-12-26 05:25:22', 6, '0000-00-00 00:00:00', 0, ''),
(16, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-25 15:29:31', 6, '0000-00-00 00:00:00', 0, ''),
(17, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-26 07:04:01', 6, '0000-00-00 00:00:00', 0, ''),
(18, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-26 08:24:57', 6, '0000-00-00 00:00:00', 0, ''),
(19, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-26 08:25:09', 6, '0000-00-00 00:00:00', 0, ''),
(20, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-26 09:15:30', 6, '0000-00-00 00:00:00', 0, ''),
(21, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-26 09:32:30', 6, '0000-00-00 00:00:00', 0, ''),
(22, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 02:33:56', 6, '0000-00-00 00:00:00', 0, ''),
(23, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 02:40:58', 6, '0000-00-00 00:00:00', 0, ''),
(24, 3, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:06:10', 6, '0000-00-00 00:00:00', 0, ''),
(25, 3, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:06:46', 6, '0000-00-00 00:00:00', 0, ''),
(26, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:07:11', 6, '0000-00-00 00:00:00', 0, ''),
(27, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:07:36', 6, '0000-00-00 00:00:00', 0, ''),
(28, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:13:21', 6, '0000-00-00 00:00:00', 0, ''),
(29, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:14:10', 6, '0000-00-00 00:00:00', 0, ''),
(30, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:15:27', 6, '0000-00-00 00:00:00', 0, ''),
(31, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:16:19', 6, '0000-00-00 00:00:00', 0, ''),
(32, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:17:01', 6, '0000-00-00 00:00:00', 0, ''),
(33, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:17:21', 6, '0000-00-00 00:00:00', 0, ''),
(34, 5, 4, 'abc', 'ab', '0', '2016-12-27 00:00:00', 'a', '1', 'a', '1', 'a', 'a', 0, 'a', 'a', 0, 'a', 'abc', 0, 'a', 'a', '2016-12-06 00:00:00', '0', 'a', 0, 0, 0, 0, 0, 0, 0, 'a', 'a', 'defg', 'defg', '2016-12-27 19:30:28', 9, '0000-00-00 00:00:00', 0, ''),
(35, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:21:41', 6, '0000-00-00 00:00:00', 0, ''),
(36, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:26:09', 6, '0000-00-00 00:00:00', 0, ''),
(37, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:28:31', 6, '0000-00-00 00:00:00', 0, ''),
(38, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 05:27:32', 6, '0000-00-00 00:00:00', 0, ''),
(39, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 05:28:35', 6, '0000-00-00 00:00:00', 0, ''),
(40, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 05:39:57', 9, '0000-00-00 00:00:00', 0, ''),
(41, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 05:48:40', 6, '0000-00-00 00:00:00', 0, ''),
(42, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 06:43:16', 6, '0000-00-00 00:00:00', 0, ''),
(43, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 06:44:11', 6, '0000-00-00 00:00:00', 0, ''),
(44, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 07:59:39', 6, '0000-00-00 00:00:00', 0, ''),
(45, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 09:18:17', 6, '0000-00-00 00:00:00', 0, ''),
(46, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 09:18:28', 6, '0000-00-00 00:00:00', 0, ''),
(47, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 09:42:43', 6, '0000-00-00 00:00:00', 0, ''),
(48, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-28 01:45:02', 6, '0000-00-00 00:00:00', 0, ''),
(49, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-28 01:45:45', 6, '0000-00-00 00:00:00', 0, ''),
(50, 7, 9, 'c', 'c', '0', '2016-12-28 00:00:00', 'c', '3', 'c', '3', 'c', 'c', 0, 'c', 'c', 0, 'c', 'c', 0, 'c', 'c', '2016-12-28 00:00:00', '0', 'c', 0, 0, 0, 0, 0, 0, 0, 'c', 'c', 'c', 'c', '2016-12-28 22:35:26', 8, '0000-00-00 00:00:00', 0, ''),
(51, 7, 8, '1', '1', '', '2016-12-28 00:00:00', '1', '1', '1', '11', '1', '1', 0, '1', '1', 0, '1', '1', 0, 'a', 'a', '2016-12-28 00:00:00', '0', '', 0, 0, 0, 0, 0, 0, 0, '', '1', '1', '1', '2016-12-28 23:25:21', 6, '0000-00-00 00:00:00', 0, ''),
(52, 7, 0, '2', '2', '', '2016-12-28 00:00:00', '2', '2', '2', '2', '2', '2', 0, '2', '2', 0, '2', '2', 0, '2', '2', '2016-12-28 00:00:00', '1', '2', 0, 0, 0, 0, 0, 0, 0, '2', '2', '2', '2', '2016-12-28 23:28:04', 9, '0000-00-00 00:00:00', 0, ''),
(53, 7, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 03:27:03', 6, '0000-00-00 00:00:00', 0, ''),
(54, 7, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 03:27:37', 6, '0000-00-00 00:00:00', 0, ''),
(55, 7, 10, '1', '1', '0', '2016-12-01 00:00:00', '1', '1', '1', '1', '1', '1', 0, '1', '1', 0, '1', '1', 1, 'D', 'D', '2016-12-28 00:00:00', '0', '1', 0, 0, 0, 0, 0, 0, 0, '1', '1', '1', '1', '2016-12-29 17:29:18', 6, '0000-00-00 00:00:00', 0, ''),
(56, 7, 9, 'yayaya', 'yayaya', '1', '2016-12-01 00:00:00', 'yayaya', '1', '1', '1', '1yayaya', '1yayaya', 3, '1yayaya', '1yayaya', 0, '1yayaya', '1yayaya', 0, 'c', 'c', '2016-12-28 00:00:00', '0', '', 0, 0, 0, 0, 0, 0, 0, '', '1yayaya', '1yayaya', '1yayaya', '2016-12-29 21:38:26', 9, '0000-00-00 00:00:00', 0, ''),
(57, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 03:35:30', 2, '0000-00-00 00:00:00', 0, ''),
(58, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 06:57:27', 6, '0000-00-00 00:00:00', 0, ''),
(59, 5, 6, '1', '1', '0', '2016-12-01 00:00:00', '1', '11', '1', '1', '1', '1', 0, '1', '1', 0, '1', '11', 0, 'a', 'a', '2016-12-08 00:00:00', '0', '1', 0, 3, 0, 1, 1, 0, 1, '1', '1', '11', '1', '2016-12-29 20:59:12', 6, '0000-00-00 00:00:00', 0, ''),
(60, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 06:58:35', 6, '0000-00-00 00:00:00', 0, ''),
(61, 5, 0, '', '', '', '2016-12-29 14:27:41', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 07:02:55', 6, '0000-00-00 00:00:00', 0, ''),
(62, 5, 6, 'OH OH', 'OH OH', '', '2016-12-27 16:43:46', 'OH OH', '1', 'OH OH', '1', 'OH OH', 'OH OH', 0, 'OH OH', 'OH OH', 1, 'OH OH', 'OH OH', 0, '123', '123', '2016-12-30 00:00:00', '0', '', 0, 0, 0, 0, 0, 0, 0, '', 'OH OH', 'OH OH', 'OH OH', '2016-12-29 07:05:11', 6, '0000-00-00 00:00:00', 0, ''),
(63, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 07:21:49', 6, '0000-00-00 00:00:00', 0, ''),
(64, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 07:24:56', 6, '0000-00-00 00:00:00', 0, ''),
(65, 5, 6, '444', '444', '1', '2016-12-05 00:00:00', '4', '44', '44', '4', '4', '4', 1, '4', '4', 2, '4', '4', 0, 'a', 'a', '2016-12-01 00:00:00', '0', '4', 1, 4, 1, 0, 1, 1, 1, '4', '4', '4', '4', '2016-12-29 21:25:54', 6, '0000-00-00 00:00:00', 0, ''),
(66, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 07:26:34', 6, '0000-00-00 00:00:00', 0, ''),
(67, 12, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-30 05:51:47', 6, '0000-00-00 00:00:00', 0, ''),
(68, 12, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-30 05:51:57', 6, '0000-00-00 00:00:00', 0, ''),
(69, 12, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-30 05:52:11', 6, '0000-00-00 00:00:00', 0, ''),
(70, 12, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-30 06:15:31', 6, '0000-00-00 00:00:00', 0, ''),
(71, 12, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-30 06:15:52', 6, '0000-00-00 00:00:00', 0, ''),
(72, 0, 12, '1', '11', '0', '2016-12-06 00:00:00', '1', '1', '1', '1', '1', '11', 0, '1', '1', 0, '1', '11', 0, '1', '1', '2016-12-30 00:00:00', '1', '1', 1, 1, 1, 0, 0, 0, 1, '1', '1', '1', '1', '2016-12-30 22:29:14', 8, '0000-00-00 00:00:00', 0, ''),
(73, 0, 12, '1', '1', '0', '2016-12-30 00:00:00', '1', '1', '1', '11', '1', '1', 0, '1', '11', 1, '1', '1', 0, '1', '1', '2016-12-30 00:00:00', '0', '1', 2, 0, 3, 0, 0, 0, 1, '1', '1', '1', '11', '2016-12-30 22:46:56', 8, '0000-00-00 00:00:00', 0, ''),
(74, 13, 12, '11', '1', '0', '2016-12-21 00:00:00', '1', '1', '1', '1', '1', '1', 1, '1', '11', 0, '1', '11', 1, '1', '1', '2016-12-30 00:00:00', '0', '', 0, 0, 0, 0, 0, 0, 0, '', '1', '1', '1', '2016-12-30 23:30:31', 6, '0000-00-00 00:00:00', 0, ''),
(75, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-07 08:27:27', 6, '0000-00-00 00:00:00', 0, ''),
(76, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-07 08:28:38', 6, '0000-00-00 00:00:00', 0, ''),
(77, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-07 08:31:40', 6, '0000-00-00 00:00:00', 0, ''),
(78, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-10 03:23:56', 6, '0000-00-00 00:00:00', 0, ''),
(79, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-10 03:24:29', 6, '0000-00-00 00:00:00', 0, ''),
(80, 27, 17, '2010L02531', '027', '0', '2017-01-10 00:00:00', 'å¹¿ä¸œçœäººæ°‘åŒ»é™¢å›½å®¶è¯ç‰©ä¸´åºŠè¯•éªŒæœºæž„', '0208352782', 'æ·±åœ³å¸‚ä¸­è”åˆ¶è¯æœ‰é™å…¬å¸', '0208263229', 'äºŒç”²åŒèƒ', 'Metformin hydrochloride', 1, 'ç‰‡å‰‚', 'ä»¿åˆ¶è¯', 4, 'ç³–å°¿ç—…IIåž‹', 'LXE', 1, '162', '53', '1967-04-12 00:00:00', '0', 'è‚¾åŠŸèƒ½ä¸¥é‡æŸä¼¤', 3, 1, 1, 1, 0, 2, 2, 'ç»è¿‡è¡€é€ï¼Œç—‡çŠ¶å¥½è½¬', 'å¹¿ä¸œçœäººæ°‘åŒ»é™¢', 'å‰¯ä¸»ä»»åŒ»å¸ˆ', 'æ— ', '2017-01-11 00:57:52', 8, '0000-00-00 00:00:00', 0, ''),
(81, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 01:57:41', 6, '0000-00-00 00:00:00', 0, ''),
(82, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 01:58:08', 6, '0000-00-00 00:00:00', 0, ''),
(83, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 01:59:38', 6, '0000-00-00 00:00:00', 0, ''),
(84, 13, 0, 'a', 'a', '', '2017-01-11 00:00:00', 'a', '1', 'a', '1', 'a', 'a', 0, 'a', 'a', 0, 'a', 'a', 0, 'a', 'a', '2017-01-11 00:00:00', '0', '', 0, 0, 0, 0, 0, 0, 0, '', 'a', 'a', '', '2017-01-11 16:03:52', 6, '0000-00-00 00:00:00', 0, ''),
(85, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 02:04:16', 6, '0000-00-00 00:00:00', 0, ''),
(86, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 02:07:34', 6, '0000-00-00 00:00:00', 0, ''),
(87, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 02:07:53', 6, '0000-00-00 00:00:00', 0, ''),
(88, 7, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 02:09:02', 6, '0000-00-00 00:00:00', 0, ''),
(89, 7, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 02:09:42', 6, '0000-00-00 00:00:00', 0, ''),
(90, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 02:55:37', 6, '0000-00-00 00:00:00', 0, ''),
(91, 7, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 03:43:35', 6, '0000-00-00 00:00:00', 0, ''),
(92, 7, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 03:43:53', 6, '0000-00-00 00:00:00', 0, ''),
(93, 7, 0, '1', '1', '0', '2017-01-11 00:00:00', '1', '1', '1', '1', '1', '1', 4, '1', '1', 5, '1', '1', 1, '1', '1', '2017-01-11 00:00:00', '1', '', 0, 0, 0, 0, 0, 0, 0, '', '1', '1', '1', '2017-01-11 17:44:24', 6, '0000-00-00 00:00:00', 0, ''),
(94, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-17 10:52:47', 6, '0000-00-00 00:00:00', 0, ''),
(95, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-17 10:53:00', 6, '0000-00-00 00:00:00', 0, ''),
(96, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-18 06:56:05', 6, '0000-00-00 00:00:00', 0, ''),
(97, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-18 06:56:15', 6, '0000-00-00 00:00:00', 0, ''),
(98, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-18 09:10:39', 6, '0000-00-00 00:00:00', 0, ''),
(99, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-18 09:11:59', 6, '0000-00-00 00:00:00', 0, ''),
(100, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-20 08:12:09', 6, '0000-00-00 00:00:00', 0, ''),
(101, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-20 08:12:35', 6, '0000-00-00 00:00:00', 0, ''),
(102, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-22 01:59:21', 6, '0000-00-00 00:00:00', 0, ''),
(103, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-22 01:59:59', 6, '0000-00-00 00:00:00', 0, ''),
(104, 64, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-02-09 06:26:27', 2, '0000-00-00 00:00:00', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `side_effects_docs`
--

CREATE TABLE `side_effects_docs` (
  `side_effect_doc_id` int(5) NOT NULL,
  `side_effect_id` int(5) NOT NULL,
  `name` varchar(150) NOT NULL,
  `version` int(5) NOT NULL,
  `version_date` datetime NOT NULL,
  `description` varchar(100) NOT NULL,
  `up_path` varchar(200) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `side_effects_docs`
--

INSERT INTO `side_effects_docs` (`side_effect_doc_id`, `side_effect_id`, `name`, `version`, `version_date`, `description`, `up_path`, `databank`) VALUES
(1, 27, 'a', 1, '0000-00-00 00:00:00', 'a', 'http://betaprojex.com/Clinical2/uploads/side_effect_documents/1482808370.txt', 0),
(2, 34, 'a', 9, '0000-00-00 00:00:00', 'a', 'http://betaprojex.com/Clinical2/uploads/side_effect_documents/1482808824.txt', 0),
(3, 74, '11', 1, '0000-00-00 00:00:00', '1', 'http://betaprojex.com/Clinical2/uploads/side_effect_documents/1483090228.docx', 0),
(4, 80, 'adf', 1, '0000-00-00 00:00:00', 'adf', 'http://betaprojex.com/Clinical2/uploads/side_effect_documents/1485050277.docx', 0),
(5, 80, 'adf', 1, '0000-00-00 00:00:00', 'adf', 'http://betaprojex.com/Clinical2/uploads/side_effect_documents/1485050283.docx', 0);

-- --------------------------------------------------------

--
-- Table structure for table `side_effect_receipts`
--

CREATE TABLE `side_effect_receipts` (
  `receipt_id` int(11) NOT NULL,
  `side_effect_id` int(11) NOT NULL,
  `receipt_file` text NOT NULL,
  `receipt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stage_summary`
--

CREATE TABLE `stage_summary` (
  `stage_summary_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `time_slot_from` date NOT NULL,
  `time_slot_to` date NOT NULL,
  `enroll_amount` int(11) NOT NULL,
  `quit_amount` int(11) NOT NULL,
  `finish_amount` int(11) NOT NULL,
  `sae` varchar(250) NOT NULL,
  `amount` int(11) NOT NULL,
  `degree` varchar(250) NOT NULL,
  `handling_info` text NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stage_summary`
--

INSERT INTO `stage_summary` (`stage_summary_id`, `project_id`, `time_slot_from`, `time_slot_to`, `enroll_amount`, `quit_amount`, `finish_amount`, `sae`, `amount`, `degree`, `handling_info`, `added_by`, `date_added`, `updated_by`, `date_updated`) VALUES
(1, 7, '2016-12-01', '2016-12-02', 0, 0, 0, 'a', 1, 'a', 'a', 6, '2016-12-28', 0, '0000-00-00'),
(2, 7, '2016-12-02', '2016-12-03', 0, 0, 0, 'b', 0, 'b', 'b', 6, '2016-12-28', 0, '0000-00-00'),
(3, 7, '2016-12-01', '2016-12-02', 1, 1, 1, '1', 1, '1', '1', 6, '2016-12-29', 0, '0000-00-00'),
(4, 7, '2016-12-01', '2016-12-02', 2, 2, 2, '2', 2, '2', '2', 6, '2016-12-29', 0, '0000-00-00'),
(5, 13, '2017-01-04', '2017-01-05', 232, 3, 3, '34', 343, '34', '4', 6, '2017-01-10', 0, '0000-00-00'),
(6, 27, '2017-04-01', '2017-05-01', 33, 4, 4, '7', 56, '7', '5', 6, '2017-01-12', 6, '0000-00-00'),
(7, 27, '2017-01-13', '2017-01-17', 67, 4, 8, '7', 7, '7', '8', 6, '2017-01-12', 0, '0000-00-00'),
(8, 27, '2017-01-04', '2017-01-05', 1, 2, 3, '13', 14, '12', '15', 0, '2017-01-16', 0, '0000-00-00'),
(9, 27, '2016-12-29', '2016-12-30', 1, 2, 3, '13', 14, '12', '15', 6, '2017-01-16', 0, '0000-00-00'),
(10, 27, '2016-12-28', '2017-01-03', 112312312, 21, 123, '123', 123, '123', '123', 6, '2017-01-18', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `status_id` int(5) NOT NULL,
  `status_num` tinyint(3) NOT NULL,
  `reviewer_id` int(5) NOT NULL COMMENT 'FK TABLE: roles role_id -- user who made the action and what position when he did it (i.e. if status = 2, approve by TFO with user_id = 4)',
  `is_rejected` tinyint(1) NOT NULL,
  `status_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `project_id` int(5) NOT NULL COMMENT 'FK in TABLE: projects'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`status_id`, `status_num`, `reviewer_id`, `is_rejected`, `status_date`, `project_id`) VALUES
(1, 1, 1, 0, '0000-00-00 00:00:00', 1),
(2, 2, 3, 0, '0000-00-00 00:00:00', 1),
(3, 3, 2, 0, '0000-00-00 00:00:00', 1),
(4, 4, 6, 0, '0000-00-00 00:00:00', 1),
(5, 5, 1, 0, '0000-00-00 00:00:00', 1),
(6, 6, 6, 0, '0000-00-00 00:00:00', 1),
(7, 7, 3, 0, '0000-00-00 00:00:00', 1),
(8, 8, 20, 0, '0000-00-00 00:00:00', 1),
(9, 9, 7, 0, '0000-00-00 00:00:00', 1),
(10, 10, 20, 0, '0000-00-00 00:00:00', 1),
(11, 11, 6, 0, '0000-00-00 00:00:00', 1),
(12, 12, 4, 0, '0000-00-00 00:00:00', 1),
(13, 13, 20, 0, '0000-00-00 00:00:00', 1),
(14, 14, 6, 0, '0000-00-00 00:00:00', 1),
(15, 1, 1, 0, '2016-12-23 03:17:29', 2),
(16, 2, 3, 0, '2016-12-23 03:19:34', 2),
(17, 3, 2, 0, '2016-12-23 03:22:32', 2),
(18, 4, 6, 0, '2016-12-23 03:39:19', 2),
(19, 5, 1, 0, '2016-12-23 04:10:20', 2),
(20, 1, 1, 0, '2016-12-23 08:38:46', 3),
(21, 2, 3, 0, '2016-12-23 08:39:25', 3),
(22, 3, 2, 0, '2016-12-23 08:39:53', 3),
(23, 4, 6, 0, '2016-12-23 08:41:11', 3),
(24, 5, 1, 0, '2016-12-23 08:41:46', 3),
(25, 6, 6, 0, '2016-12-23 08:42:32', 3),
(26, 7, 3, 0, '2016-12-23 08:42:57', 3),
(27, 8, 20, 0, '2016-12-23 08:43:24', 3),
(28, 9, 7, 0, '2016-12-23 08:46:41', 3),
(29, 10, 20, 0, '2016-12-23 08:47:15', 3),
(30, 11, 6, 0, '2016-12-23 08:48:03', 3),
(31, 12, 4, 0, '2016-12-23 08:53:28', 3),
(32, 13, 20, 0, '2016-12-23 08:54:02', 3),
(33, 14, 6, 0, '2016-12-23 08:54:54', 3),
(34, 15, 6, 0, '2016-12-23 09:02:39', 3),
(35, 1, 1, 0, '2016-12-23 23:17:00', 4),
(36, 1, 1, 0, '2016-12-26 03:12:37', 5),
(37, 2, 3, 0, '2016-12-26 03:52:40', 5),
(38, 3, 2, 0, '2016-12-26 03:53:10', 5),
(39, 4, 6, 0, '2016-12-26 03:59:52', 5),
(40, 5, 1, 0, '2016-12-26 04:00:25', 5),
(41, 6, 6, 0, '2016-12-26 04:01:34', 5),
(42, 7, 3, 0, '2016-12-26 04:02:01', 5),
(43, 8, 20, 0, '2016-12-26 04:02:33', 5),
(44, 9, 7, 0, '2016-12-26 04:04:29', 5),
(45, 10, 20, 0, '2016-12-26 04:08:12', 5),
(46, 11, 6, 0, '2016-12-26 04:10:02', 5),
(47, 12, 4, 0, '2016-12-26 04:11:22', 5),
(48, 13, 20, 0, '2016-12-26 04:12:13', 5),
(49, 14, 6, 0, '2016-12-26 04:12:50', 5),
(50, 1, 1, 0, '2016-12-28 19:50:05', 7),
(51, 2, 3, 0, '2016-12-28 19:51:32', 7),
(52, 3, 2, 0, '2016-12-28 19:53:17', 7),
(53, 4, 6, 0, '2016-12-28 19:54:48', 7),
(54, 5, 1, 0, '2016-12-28 19:55:57', 7),
(55, 6, 6, 0, '2016-12-28 20:07:09', 7),
(56, 7, 3, 0, '2016-12-28 20:31:24', 7),
(57, 8, 20, 0, '2016-12-28 20:46:44', 7),
(58, 9, 7, 0, '2016-12-28 20:49:23', 7),
(59, 10, 20, 0, '2016-12-28 20:50:09', 7),
(60, 11, 6, 0, '2016-12-28 20:50:45', 7),
(61, 12, 4, 0, '2016-12-28 20:51:06', 7),
(62, 13, 20, 0, '2016-12-28 20:51:41', 7),
(63, 14, 6, 0, '2016-12-28 21:13:29', 7),
(64, 15, 6, 0, '2016-12-28 23:45:58', 7),
(65, 16, 6, 0, '2016-12-29 19:54:17', 7),
(66, 17, 1, 0, '2016-12-29 19:55:27', 7),
(67, 16, 6, 0, '2016-12-29 20:01:56', 3),
(68, 17, 1, 0, '2016-12-29 20:03:57', 3),
(69, 1, 1, 0, '2016-12-30 18:59:24', 10),
(70, 1, 1, 0, '2016-12-30 19:01:57', 11),
(71, 1, 1, 0, '2016-12-30 19:06:32', 12),
(72, 2, 3, 0, '2016-12-30 19:07:28', 12),
(73, 3, 2, 0, '2016-12-30 19:09:04', 12),
(74, 4, 6, 0, '2016-12-30 19:16:45', 12),
(75, 5, 1, 0, '2016-12-30 19:17:57', 12),
(76, 6, 6, 0, '2016-12-30 19:21:17', 12),
(77, 7, 3, 0, '2016-12-30 19:21:41', 12),
(78, 8, 20, 0, '2016-12-30 19:25:16', 12),
(79, 9, 7, 0, '2016-12-30 19:33:08', 12),
(80, 10, 20, 0, '2016-12-30 19:34:05', 12),
(81, 11, 6, 0, '2016-12-30 19:34:53', 12),
(82, 12, 4, 0, '2016-12-30 19:38:11', 12),
(83, 13, 20, 0, '2016-12-30 19:41:46', 12),
(84, 14, 6, 0, '2016-12-30 19:43:34', 12),
(85, 1, 1, 0, '2016-12-30 21:13:38', 13),
(86, 2, 3, 0, '2016-12-30 21:15:44', 13),
(87, 3, 2, 0, '2016-12-30 21:16:49', 13),
(88, 4, 6, 0, '2016-12-30 21:36:00', 13),
(89, 5, 1, 0, '2016-12-30 21:36:30', 13),
(90, 6, 6, 0, '2016-12-30 21:37:44', 13),
(91, 7, 3, 0, '2016-12-30 21:39:29', 13),
(92, 8, 20, 0, '2016-12-30 21:40:23', 13),
(93, 9, 7, 0, '2016-12-30 21:41:48', 13),
(94, 10, 20, 0, '2016-12-30 21:57:50', 13),
(95, 11, 6, 0, '2016-12-30 22:02:19', 13),
(96, 12, 4, 0, '2016-12-30 22:03:02', 13),
(97, 13, 20, 0, '2016-12-30 22:12:03', 13),
(98, 14, 6, 0, '2016-12-30 22:13:13', 13),
(99, -1, 1, 0, '2017-01-05 07:54:18', 5),
(100, 1, 1, 0, '2017-01-07 17:13:36', 14),
(101, 2, 3, 0, '2017-01-07 17:35:19', 14),
(102, 3, 2, 0, '2017-01-07 19:51:58', 14),
(103, 1, 1, 0, '2017-01-09 20:51:26', 9),
(104, 1, 1, 0, '2017-01-10 15:32:32', 19),
(105, 2, 3, 0, '2017-01-10 15:34:02', 19),
(106, 1, 1, 0, '2017-01-10 15:55:01', 20),
(107, 2, 3, 0, '2017-01-10 16:07:42', 20),
(108, 3, 2, 0, '2017-01-10 16:09:11', 20),
(109, 3, 2, 0, '2017-01-10 16:11:35', 19),
(110, 1, 1, 0, '2017-01-10 18:11:42', 24),
(111, 2, 3, 0, '2017-01-10 18:14:27', 24),
(112, 4, 6, 0, '2017-01-10 21:23:54', 20),
(113, 5, 1, 0, '2017-01-10 21:24:31', 20),
(114, 6, 6, 0, '2017-01-10 22:06:42', 20),
(115, 7, 3, 0, '2017-01-10 22:07:10', 20),
(116, 8, 20, 0, '2017-01-10 22:07:39', 20),
(117, 9, 7, 0, '2017-01-10 22:08:13', 20),
(118, 1, 1, 0, '2017-01-10 23:23:49', 27),
(119, 2, 3, 0, '2017-01-10 23:25:10', 27),
(120, 3, 2, 0, '2017-01-10 23:26:03', 27),
(121, 4, 6, 0, '2017-01-10 23:32:02', 27),
(122, 5, 1, 0, '2017-01-10 23:32:37', 27),
(123, 6, 6, 0, '2017-01-10 23:37:34', 27),
(124, 1, 1, 0, '2017-01-10 23:39:53', 26),
(125, 7, 3, 0, '2017-01-10 23:46:01', 27),
(126, 8, 20, 0, '2017-01-10 23:46:56', 27),
(127, 2, 3, 0, '2017-01-10 23:48:18', 26),
(128, 9, 7, 0, '2017-01-10 23:48:30', 27),
(129, 0, 2, 1, '2017-01-10 23:49:21', 26),
(130, 1, 1, 0, '2017-01-10 23:49:54', 26),
(131, 10, 20, 0, '2017-01-10 23:52:52', 27),
(132, 11, 6, 0, '2017-01-11 00:04:13', 27),
(133, 12, 4, 0, '2017-01-11 00:09:56', 27),
(134, 13, 20, 0, '2017-01-11 00:11:04', 27),
(135, 14, 6, 0, '2017-01-11 00:12:19', 27),
(136, 1, 1, 0, '2017-01-11 21:31:07', 28),
(137, 2, 3, 0, '2017-01-11 21:36:35', 28),
(138, 3, 2, 0, '2017-01-11 21:37:14', 28),
(139, 4, 6, 0, '2017-01-11 21:42:38', 28),
(140, 5, 1, 0, '2017-01-11 21:43:48', 28),
(141, 6, 6, 0, '2017-01-11 21:44:31', 28),
(142, 7, 3, 0, '2017-01-11 21:45:07', 28),
(143, 8, 20, 0, '2017-01-11 21:46:27', 28),
(144, 9, 7, 0, '2017-01-11 21:50:17', 28),
(145, 10, 20, 0, '2017-01-11 21:54:24', 28),
(146, 11, 6, 0, '2017-01-11 21:55:34', 28),
(147, 12, 4, 0, '2017-01-11 21:56:02', 28),
(148, 13, 20, 0, '2017-01-11 21:57:23', 28),
(149, 14, 6, 0, '2017-01-11 21:58:03', 28),
(150, 15, 6, 0, '2017-01-12 15:59:13', 27),
(151, 16, 6, 0, '2017-01-12 16:26:27', 27),
(152, 17, 1, 0, '2017-01-13 17:42:18', 27),
(153, 1, 22, 0, '2017-01-20 13:52:55', 34),
(154, 2, 27, 0, '2017-01-20 13:55:31', 34),
(155, 1, 1, 0, '2017-01-22 15:04:54', 36),
(156, 2, 3, 0, '2017-01-22 15:05:55', 36),
(157, 1, 22, 0, '2017-01-22 15:38:38', 39),
(158, 1, 22, 0, '2017-01-22 15:39:09', 40),
(159, 1, 22, 0, '2017-01-22 15:39:32', 41),
(160, 1, 22, 0, '2017-01-22 15:39:57', 43),
(161, 1, 22, 0, '2017-01-22 15:40:22', 44),
(162, 1, 22, 0, '2017-01-22 15:41:26', 45),
(163, 1, 22, 0, '2017-01-22 15:42:15', 48),
(164, 1, 1, 0, '2017-01-25 17:52:48', 50),
(165, 0, 2, 1, '2017-01-25 19:54:35', 36),
(166, 2, 3, 0, '2017-02-07 00:15:33', 39),
(167, 4, 6, 0, '2017-02-07 00:16:12', 19),
(168, 3, 2, 0, '2017-02-07 00:17:25', 39),
(169, 4, 6, 0, '2017-02-07 00:20:04', 39),
(170, 1, 1, 0, '2017-02-07 00:25:26', 54),
(171, 2, 3, 0, '2017-02-07 00:25:52', 54),
(172, 3, 2, 0, '2017-02-07 02:03:00', 54),
(173, 4, 6, 0, '2017-02-07 02:04:48', 54),
(174, 5, 1, 0, '2017-02-07 02:05:25', 54),
(175, 6, 6, 0, '2017-02-07 02:07:07', 54),
(176, 7, 3, 0, '2017-02-07 02:07:26', 54),
(177, 8, 20, 0, '2017-02-07 02:13:49', 54),
(178, 9, 7, 0, '2017-02-07 02:16:54', 54),
(179, 10, 20, 0, '2017-02-07 02:17:26', 54),
(180, 11, 6, 0, '2017-02-07 02:18:00', 54),
(181, 12, 4, 0, '2017-02-07 02:22:34', 54),
(182, 13, 20, 0, '2017-02-07 02:23:23', 54),
(183, 1, 1, 0, '2017-02-07 22:50:42', 55),
(184, 1, 1, 0, '2017-02-07 22:55:54', 56),
(185, 0, 3, 1, '2017-02-07 23:04:12', 56),
(186, 1, 1, 0, '2017-02-07 23:33:26', 56),
(187, 1, 1, 0, '2017-02-07 23:47:04', 36),
(188, 1, 1, 0, '2017-02-08 16:14:30', 57),
(189, 2, 3, 0, '2017-02-08 16:22:35', 57),
(190, 3, 2, 0, '2017-02-08 16:25:06', 57),
(191, 4, 6, 0, '2017-02-08 16:32:48', 57),
(192, 5, 1, 0, '2017-02-08 16:44:18', 57),
(193, 6, 6, 0, '2017-02-08 17:30:42', 57),
(194, 7, 3, 0, '2017-02-08 17:31:21', 57),
(195, 8, 20, 0, '2017-02-08 17:31:54', 57),
(196, 9, 7, 0, '2017-02-08 17:32:26', 57),
(197, 10, 20, 0, '2017-02-08 17:43:13', 57),
(198, 11, 6, 0, '2017-02-08 17:43:55', 57),
(199, 12, 4, 0, '2017-02-08 17:48:30', 57),
(200, 13, 20, 0, '2017-02-08 17:49:19', 57),
(201, 1, 1, 0, '2017-02-09 02:18:54', 59),
(202, 1, 1, 0, '2017-02-09 14:47:19', 60),
(203, 1, 1, 0, '2017-02-09 15:11:36', 62),
(204, 1, 1, 0, '2017-02-09 15:17:29', 63),
(205, 1, 1, 0, '2017-02-09 15:29:42', 64),
(206, 1, 1, 0, '2017-02-09 15:38:36', 61),
(207, 2, 3, 0, '2017-02-09 15:41:53', 64),
(208, 3, 2, 0, '2017-02-09 15:43:54', 64),
(209, 4, 6, 0, '2017-02-09 15:49:25', 64),
(210, 5, 1, 0, '2017-02-09 15:51:36', 64),
(211, 6, 6, 0, '2017-02-09 15:55:33', 64),
(212, 7, 3, 0, '2017-02-09 15:55:58', 64),
(213, 8, 20, 0, '2017-02-09 15:56:24', 64),
(214, 9, 7, 0, '2017-02-09 15:56:55', 64),
(215, 10, 20, 0, '2017-02-09 15:57:46', 64),
(216, 11, 6, 0, '2017-02-09 15:58:30', 64),
(217, 12, 4, 0, '2017-02-09 15:58:48', 64),
(218, 13, 20, 0, '2017-02-09 15:59:35', 64),
(219, 14, 2, 0, '2017-02-09 02:02:39', 64),
(220, 1, 1, 0, '2017-02-09 16:04:49', 65),
(221, 2, 3, 0, '2017-02-09 16:11:11', 65),
(222, 3, 2, 0, '2017-02-09 16:11:44', 65),
(223, 4, 6, 0, '2017-02-09 16:13:39', 65),
(224, 5, 1, 0, '2017-02-09 16:21:57', 65),
(225, 1, 1, 0, '2017-02-09 20:36:33', 66),
(226, 2, 3, 0, '2017-02-09 20:37:37', 66),
(227, -1, 1, 0, '2017-02-09 06:44:44', 66),
(228, 1, 1, 0, '2017-02-09 20:47:35', 67),
(229, 2, 3, 0, '2017-02-09 20:47:54', 67),
(230, 3, 2, 0, '2017-02-09 20:49:26', 67),
(231, 4, 6, 0, '2017-02-09 20:59:25', 67),
(232, -1, 1, 0, '2017-02-09 09:25:09', 4),
(233, -1, 1, 0, '2017-02-09 09:25:24', 24);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `subject_id` int(11) NOT NULL,
  `volunteer_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `case_num` varchar(50) NOT NULL,
  `subjects_num` varchar(50) NOT NULL,
  `abbrv` varchar(50) NOT NULL,
  `country` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `pi` varchar(50) NOT NULL,
  `is_informed_consent` tinyint(4) NOT NULL COMMENT '0 - no, 1 - yes',
  `status_id` int(11) NOT NULL COMMENT 'fk from subject_status',
  `sign_date` datetime NOT NULL,
  `random_num` int(11) NOT NULL,
  `random_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subject_id`, `volunteer_id`, `project_id`, `case_num`, `subjects_num`, `abbrv`, `country`, `address`, `pi`, `is_informed_consent`, `status_id`, `sign_date`, `random_num`, `random_date`) VALUES
(1, 1, 1, '', '', '', '', '', '', 0, 4, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(2, 2, 1, '', '', '', '', '', '', 0, 64, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(3, 3, 1, '', '', '', '', '', '', 0, 10, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(4, 4, 5, '', '', '', '', '', '', 0, 14, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(5, 5, 5, '', '', '', '', '', '', 0, 85, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(6, 6, 5, '', '', '', '', '', '', 0, 19, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(7, 8, 7, '', '', '', '', '', '', 0, 75, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(8, 7, 7, '', '', '', '', '', '', 0, 35, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(9, 9, 7, '', '', '', '', '', '', 0, 43, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(10, 10, 7, '', '', '', '', '', '', 0, 76, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(11, 11, 12, '', '', '', '', '', '', 0, 84, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(12, 12, 13, '', '', '', '', '', '', 0, 49, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(13, 13, 13, '', '', '', '', '', '', 0, 55, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(14, 14, 13, '', '', '', '', '', '', 0, 61, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(15, 15, 13, '', '', '', '', '', '', 0, 69, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(16, 17, 27, '', '', '', '', 'fg', '', 0, 82, '0000-00-00 00:00:00', 2334, '0000-00-00 00:00:00'),
(17, 16, 27, '1223', '123', 'ZHG', 'meiguo', 'adf', '', 0, 78, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(18, 15, 28, '', '', '', '', '', '', 0, 89, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(19, 18, 28, '', '', '', '', '', '', 0, 86, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `subject_status`
--

CREATE TABLE `subject_status` (
  `status_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `status` varchar(3) NOT NULL COMMENT 'S - selecting P - pass NP - no pass C - choose  (update status of subject as 1 - means, not available for other projects) B - begin FU - follow up STP - stop O - out D - drop F - Finish (once finish, update volunteer status as 0 - make available to other projects)  ',
  `status_date` datetime NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject_status`
--

INSERT INTO `subject_status` (`status_id`, `subject_id`, `status`, `status_date`, `remarks`) VALUES
(1, 1, 'S', '2016-12-22 18:05:07', ''),
(2, 1, 'P', '2016-12-22 18:05:37', ''),
(3, 1, 'C', '2016-12-22 18:06:50', ''),
(4, 1, 'B', '2016-12-22 00:00:00', ''),
(5, 2, 'S', '2016-12-23 16:20:36', ''),
(6, 2, 'P', '2016-12-23 16:23:27', ''),
(7, 3, 'S', '2016-12-23 17:27:05', ''),
(8, 3, 'P', '2016-12-23 17:27:39', ''),
(9, 3, 'C', '2016-12-23 17:27:45', ''),
(10, 3, 'B', '2016-12-25 00:00:00', ''),
(11, 4, 'S', '2016-12-25 22:20:12', ''),
(12, 4, 'P', '2016-12-25 22:51:59', ''),
(13, 4, 'C', '2016-12-25 22:52:19', ''),
(14, 4, 'B', '2016-12-25 00:00:00', ''),
(15, 5, 'S', '2016-12-27 10:46:36', ''),
(16, 6, 'S', '2016-12-27 12:01:27', ''),
(17, 6, 'P', '2016-12-27 12:02:06', ''),
(18, 6, 'C', '2016-12-27 12:02:16', ''),
(19, 6, 'B', '2016-12-27 00:00:00', ''),
(20, 7, 'S', '2016-12-28 15:28:38', ''),
(21, 8, 'S', '2016-12-28 15:28:45', ''),
(22, 7, 'P', '2016-12-28 15:35:54', ''),
(23, 8, 'P', '2016-12-28 15:35:57', ''),
(24, 8, 'C', '2016-12-28 15:36:03', ''),
(25, 7, 'C', '2016-12-28 15:36:03', ''),
(26, 8, 'B', '2016-12-28 00:00:00', ''),
(27, 7, 'B', '2016-12-28 00:00:00', ''),
(28, 9, 'S', '2016-12-28 15:44:01', ''),
(29, 9, 'P', '2016-12-28 15:44:27', ''),
(30, 9, 'C', '2016-12-28 15:44:32', ''),
(31, 10, 'S', '2016-12-28 16:03:32', ''),
(32, 10, 'P', '2016-12-28 16:04:00', ''),
(33, 10, 'C', '2016-12-28 16:04:13', ''),
(34, 8, 'FU', '2016-12-28 00:00:00', ''),
(35, 8, 'F', '2016-12-28 00:00:00', ''),
(36, 7, 'STP', '2016-12-28 00:00:00', 'stop'),
(37, 9, 'B', '2016-12-28 00:00:00', ''),
(38, 11, 'S', '2016-12-30 13:56:20', ''),
(39, 11, 'P', '2016-12-30 13:58:11', ''),
(40, 10, 'B', '2016-12-30 00:00:00', ''),
(41, 10, 'STP', '2016-12-30 00:00:00', 'é€€å‡ºåŽŸå› '),
(42, 9, 'FU', '2016-12-30 00:00:00', ''),
(43, 9, 'F', '2016-12-30 00:00:00', ''),
(44, 12, 'S', '2016-12-30 16:21:39', ''),
(45, 12, 'P', '2016-12-30 16:24:12', ''),
(46, 12, 'C', '2016-12-30 16:25:38', ''),
(47, 12, 'B', '2016-12-30 00:00:00', ''),
(48, 12, 'FU', '2016-12-30 00:00:00', ''),
(49, 12, 'F', '2016-12-30 00:00:00', ''),
(50, 13, 'S', '2016-12-30 16:37:58', ''),
(51, 13, 'P', '2016-12-30 16:38:56', ''),
(52, 13, 'C', '2016-12-30 16:39:05', ''),
(53, 13, 'B', '2016-12-30 00:00:00', ''),
(54, 13, 'FU', '2016-12-30 00:00:00', ''),
(55, 13, 'F', '2016-12-30 00:00:00', ''),
(56, 14, 'S', '2016-12-30 16:45:18', ''),
(57, 14, 'P', '2016-12-30 16:45:56', ''),
(58, 14, 'C', '2016-12-30 16:46:05', ''),
(59, 14, 'B', '2016-12-30 00:00:00', ''),
(60, 2, 'C', '2017-01-07 14:23:03', ''),
(61, 14, 'FU', '2017-01-07 00:00:00', ''),
(62, 15, 'S', '2017-01-10 10:41:33', ''),
(63, 11, 'C', '2017-01-10 17:56:07', ''),
(64, 2, 'B', '2017-01-10 00:00:00', ''),
(65, 16, 'S', '2017-01-10 18:30:56', ''),
(66, 17, 'S', '2017-01-10 18:31:01', ''),
(67, 16, 'P', '2017-01-10 18:31:46', ''),
(68, 17, 'P', '2017-01-10 18:31:53', ''),
(69, 15, 'NP', '2017-01-10 18:31:58', ''),
(70, 17, 'C', '2017-01-10 18:32:22', ''),
(71, 16, 'C', '2017-01-10 18:32:22', ''),
(72, 16, 'B', '2017-01-10 00:00:00', ''),
(73, 17, 'B', '2017-01-10 00:00:00', ''),
(74, 16, 'FU', '2017-01-10 00:00:00', ''),
(75, 7, 'NS', '2017-01-11 00:00:00', ''),
(76, 10, 'NS', '2017-01-11 00:00:00', ''),
(77, 11, 'B', '2017-01-12 00:00:00', ''),
(78, 17, 'FU', '2017-01-12 00:00:00', ''),
(79, 5, 'P', '2017-01-12 11:45:59', ''),
(80, 5, 'C', '2017-01-12 11:46:07', ''),
(81, 18, 'S', '2017-01-12 11:47:29', ''),
(82, 16, 'F', '2017-01-12 00:00:00', ''),
(83, 11, 'STP', '2017-01-12 00:00:00', 'stop'),
(84, 11, 'NS', '2017-01-25 00:00:00', ''),
(85, 5, 'NS', '2017-01-27 00:00:00', ''),
(86, 19, 'S', '2017-01-27 10:26:10', ''),
(87, 18, 'P', '2017-01-27 10:27:03', ''),
(88, 18, 'C', '2017-01-27 10:27:34', ''),
(89, 18, 'B', '2017-02-07 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

CREATE TABLE `trainings` (
  `training_id` int(10) NOT NULL,
  `project_id` int(10) NOT NULL,
  `title` varchar(200) NOT NULL,
  `type` tinyint(1) DEFAULT NULL COMMENT '0 - Will start, 1 - Training, 2 - Other',
  `place` varchar(300) NOT NULL,
  `host` varchar(200) NOT NULL,
  `meeting_content` varchar(400) NOT NULL,
  `meeting_summary` varchar(400) NOT NULL,
  `is_drafted` tinyint(1) NOT NULL COMMENT '0 - submitted, 1 - draft',
  `created_by` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(5) NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainings`
--

INSERT INTO `trainings` (`training_id`, `project_id`, `title`, `type`, `place`, `host`, `meeting_content`, `meeting_summary`, `is_drafted`, `created_by`, `date_created`, `updated_by`, `date_updated`) VALUES
(1, 1, '', NULL, '', '', '', '', 0, 6, '2016-12-22 09:55:28', 0, '0000-00-00 00:00:00'),
(2, 1, 'adf', 1, 'df', 'adfa', 'adfa', 'df', 0, 6, '2016-12-22 09:56:09', 0, '0000-00-00 00:00:00'),
(3, 3, '', NULL, '', '', '', '', 0, 6, '2016-12-23 09:15:26', 0, '0000-00-00 00:00:00'),
(4, 3, '', NULL, '', '', '', '', 0, 6, '2016-12-25 13:38:36', 0, '0000-00-00 00:00:00'),
(5, 1, '', NULL, '', '', '', '', 0, 6, '2016-12-26 07:00:58', 0, '0000-00-00 00:00:00'),
(6, 1, '', NULL, '', '', '', '', 0, 6, '2016-12-26 07:01:21', 0, '0000-00-00 00:00:00'),
(7, 5, '', NULL, '', '', '', '', 0, 6, '2016-12-27 02:33:30', 0, '0000-00-00 00:00:00'),
(8, 3, '', NULL, '', '', '', '', 0, 6, '2016-12-27 03:03:25', 0, '0000-00-00 00:00:00'),
(9, 3, '', NULL, '', '', '', '', 0, 6, '2016-12-27 03:05:51', 0, '0000-00-00 00:00:00'),
(10, 1, '', NULL, '', '', '', '', 0, 6, '2016-12-27 09:18:08', 0, '0000-00-00 00:00:00'),
(11, 7, 'a', 0, 'a', 'a', 'a', 'a', 0, 6, '2016-12-28 07:09:44', 0, '0000-00-00 00:00:00'),
(12, 7, '', NULL, '', '', '', '', 0, 6, '2016-12-28 07:11:25', 0, '0000-00-00 00:00:00'),
(13, 7, 'b', 1, 'b', 'b', 'b', 'b', 0, 6, '2016-12-28 07:11:35', 0, '0000-00-00 00:00:00'),
(14, 7, '', NULL, '', '', '', '', 0, 6, '2016-12-28 07:11:52', 0, '0000-00-00 00:00:00'),
(15, 12, 'é˜¿é“å¤«', 1, 'é˜¿é“å¤«', 'é˜¿é“å¤«', 'é˜¿é“å¤«', 'é˜¿é“å¤«', 0, 6, '2016-12-30 05:49:26', 0, '0000-00-00 00:00:00'),
(16, 13, '', NULL, '', '', '', '', 0, 6, '2016-12-30 08:14:39', 0, '0000-00-00 00:00:00'),
(17, 13, '1', 0, '1', '11', '11', '11', 0, 6, '2016-12-30 08:16:32', 0, '0000-00-00 00:00:00'),
(18, 13, '', NULL, '', '', '', '', 0, 6, '2017-01-07 07:53:46', 0, '0000-00-00 00:00:00'),
(19, 13, 'kjakl', 2, 'df', 'df', 'adf', 'adf', 0, 6, '2017-01-07 08:13:30', 0, '0000-00-00 00:00:00'),
(20, 13, '', NULL, '', '', '', '', 0, 6, '2017-01-10 03:19:06', 0, '0000-00-00 00:00:00'),
(21, 13, '', NULL, '', '', '', '', 0, 6, '2017-01-10 09:50:01', 0, '0000-00-00 00:00:00'),
(22, 27, 'äºŒç”²åŒèƒBEå®žéªŒçš„å¯åŠ¨ä¼š', 0, 'å¹¿ä¸œçœäººæ°‘åŒ»é™¢ä¼Ÿä¼¦æ¥¼13æ¥¼ä¼šè®®å®¤', 'æœæ™´', 'è¯¦è§é™„ä»¶', 'è¯¦è§é™„ä»¶', 0, 6, '2017-01-10 10:16:00', 0, '0000-00-00 00:00:00'),
(23, 27, 'äºŒç”²åŒèƒBEå®žéªŒç ”ç©¶åŸ¹è®­ä¼š', 1, 'å¹¿ä¸œçœäººæ°‘åŒ»é™¢ä¼Ÿä¼¦æ¥¼13æ¥¼ä¼šè®®å®¤', 'æœæ™´', 'è¯¦è§é™„ä»¶', 'è¯¦è§é™„ä»¶', 0, 6, '2017-01-10 10:19:07', 0, '0000-00-00 00:00:00'),
(24, 13, '', NULL, '', '', '', '', 0, 6, '2017-01-11 01:46:23', 0, '0000-00-00 00:00:00'),
(25, 13, '', NULL, '', '', '', '', 0, 6, '2017-01-12 02:47:33', 0, '0000-00-00 00:00:00'),
(26, 13, '', NULL, '', '', '', '', 0, 6, '2017-01-12 06:30:55', 0, '0000-00-00 00:00:00'),
(27, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-16 07:48:23', 0, '0000-00-00 00:00:00'),
(28, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-16 07:51:30', 0, '0000-00-00 00:00:00'),
(29, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-16 07:51:53', 0, '0000-00-00 00:00:00'),
(30, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-16 07:52:14', 0, '0000-00-00 00:00:00'),
(31, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-16 07:52:40', 0, '0000-00-00 00:00:00'),
(32, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-16 07:53:32', 0, '0000-00-00 00:00:00'),
(33, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-16 07:53:40', 0, '0000-00-00 00:00:00'),
(34, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-16 08:08:34', 0, '0000-00-00 00:00:00'),
(35, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 03:28:37', 0, '0000-00-00 00:00:00'),
(36, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 03:34:17', 0, '0000-00-00 00:00:00'),
(37, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 03:35:39', 0, '0000-00-00 00:00:00'),
(38, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 03:35:44', 0, '0000-00-00 00:00:00'),
(39, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 06:58:55', 0, '0000-00-00 00:00:00'),
(40, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 06:59:18', 0, '0000-00-00 00:00:00'),
(41, 27, 'jdjd', 0, 'a', 'adf', 'adf', 'adf', 0, 6, '2017-01-18 06:59:25', 0, '0000-00-00 00:00:00'),
(42, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 07:02:57', 0, '0000-00-00 00:00:00'),
(43, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 07:04:20', 0, '0000-00-00 00:00:00'),
(44, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 07:05:39', 0, '0000-00-00 00:00:00'),
(45, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 07:07:21', 0, '0000-00-00 00:00:00'),
(46, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 07:07:28', 0, '0000-00-00 00:00:00'),
(47, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 07:07:42', 0, '0000-00-00 00:00:00'),
(48, 27, 'jdjd', 0, 'a', 'adf', 'adf', 'adf', 0, 6, '2017-01-18 07:13:43', 0, '0000-00-00 00:00:00'),
(49, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 08:54:28', 0, '0000-00-00 00:00:00'),
(50, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 08:55:53', 0, '0000-00-00 00:00:00'),
(51, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 08:56:01', 0, '0000-00-00 00:00:00'),
(52, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 09:01:06', 0, '0000-00-00 00:00:00'),
(53, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 09:01:10', 0, '0000-00-00 00:00:00'),
(54, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 09:14:03', 0, '0000-00-00 00:00:00'),
(55, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 09:32:51', 0, '0000-00-00 00:00:00'),
(56, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 09:33:05', 0, '0000-00-00 00:00:00'),
(57, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-18 09:33:13', 0, '0000-00-00 00:00:00'),
(58, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 01:54:48', 0, '0000-00-00 00:00:00'),
(59, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 02:30:41', 0, '0000-00-00 00:00:00'),
(60, 27, '1', 0, '1', '', '1', '1', 0, 6, '2017-01-19 02:30:44', 0, '0000-00-00 00:00:00'),
(61, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 02:31:16', 0, '0000-00-00 00:00:00'),
(62, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 02:32:31', 0, '0000-00-00 00:00:00'),
(63, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 03:48:34', 0, '0000-00-00 00:00:00'),
(64, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 03:50:37', 0, '0000-00-00 00:00:00'),
(65, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 03:51:08', 0, '0000-00-00 00:00:00'),
(66, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 03:51:33', 0, '0000-00-00 00:00:00'),
(67, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 03:52:17', 0, '0000-00-00 00:00:00'),
(68, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 03:52:20', 0, '0000-00-00 00:00:00'),
(69, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 03:52:23', 0, '0000-00-00 00:00:00'),
(70, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 03:52:49', 0, '0000-00-00 00:00:00'),
(71, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 03:52:56', 0, '0000-00-00 00:00:00'),
(72, 27, 'test', 1, 'test', 'test', 'test', 'test', 0, 6, '2017-01-19 03:53:04', 0, '0000-00-00 00:00:00'),
(73, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 03:53:43', 0, '0000-00-00 00:00:00'),
(74, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 08:01:29', 0, '0000-00-00 00:00:00'),
(75, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 08:01:38', 0, '0000-00-00 00:00:00'),
(76, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 08:02:05', 0, '0000-00-00 00:00:00'),
(77, 27, '1', 1, '1', '1', '1', '1', 0, 6, '2017-01-19 08:02:40', 0, '0000-00-00 00:00:00'),
(78, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-19 08:03:04', 0, '0000-00-00 00:00:00'),
(79, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-22 03:24:55', 0, '0000-00-00 00:00:00'),
(80, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-22 03:25:15', 0, '0000-00-00 00:00:00'),
(81, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-23 08:42:53', 0, '0000-00-00 00:00:00'),
(82, 27, '', NULL, '', '', '', '', 0, 6, '2017-01-25 06:15:50', 0, '0000-00-00 00:00:00'),
(83, 54, '', NULL, '', '', '', '', 0, 6, '2017-02-07 01:49:23', 0, '0000-00-00 00:00:00'),
(84, 27, '', NULL, '', '', '', '', 0, 6, '2017-02-07 02:27:19', 0, '0000-00-00 00:00:00'),
(85, 57, '', NULL, '', '', '', '', 0, 6, '2017-02-08 12:13:44', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `trainings_participant`
--

CREATE TABLE `trainings_participant` (
  `participant_id` int(5) NOT NULL,
  `training_id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainings_participant`
--

INSERT INTO `trainings_participant` (`participant_id`, `training_id`, `role_id`) VALUES
(1, 2, 8),
(2, 15, 8),
(3, 15, 9),
(4, 17, 8),
(5, 17, 9),
(6, 17, 10),
(7, 17, 11),
(8, 19, 8),
(9, 22, 8),
(10, 22, 9),
(11, 22, 10),
(12, 22, 11),
(13, 23, 8),
(14, 23, 9),
(15, 23, 10),
(16, 23, 11),
(17, 22, 8),
(18, 22, 9),
(19, 22, 10),
(20, 22, 11),
(21, 23, 8),
(22, 23, 9),
(23, 23, 10),
(24, 23, 11),
(25, 23, 8),
(26, 23, 9),
(27, 23, 10),
(28, 23, 11),
(29, 41, 8),
(30, 41, 8),
(31, 41, 8),
(32, 41, 8),
(33, 41, 8),
(34, 48, 8);

-- --------------------------------------------------------

--
-- Table structure for table `training_material`
--

CREATE TABLE `training_material` (
  `material_id` int(10) NOT NULL,
  `training_id` int(10) NOT NULL,
  `name` varchar(150) NOT NULL,
  `version` int(2) NOT NULL,
  `version_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(300) NOT NULL,
  `up_path` varchar(250) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training_material`
--

INSERT INTO `training_material` (`material_id`, `training_id`, `name`, `version`, `version_date`, `description`, `up_path`, `databank`) VALUES
(1, 15, 'aåœ°æ–¹', 1, '2016-12-30 05:49:55', '', 'http://betaprojex.com/Clinical2/uploads/training_material/1483076995.docx', 0),
(2, 16, '1', 1, '2016-12-30 08:15:16', '1', 'http://betaprojex.com/Clinical2/uploads/training_material/1483085716.jpg', 0),
(3, 22, 'é¡¹ç›®å¯åŠ¨ä¼šä¼šè®®è®°å½•', 1, '2017-01-10 10:18:44', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/training_material/1484043524.docx', 0),
(4, 23, 'åŸ¹è®­ä¼šä¼šè®®è®°å½•', 1, '2017-01-10 10:20:17', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/training_material/1484043617.docx', 0),
(5, 23, 'haha', 1, '2017-01-16 07:48:57', 'enen', 'http://betaprojex.com/Clinical2/uploads/training_material/1484552937.docx', 0),
(6, 41, 'DF', 1, '2017-01-18 07:03:51', '', 'http://betaprojex.com/Clinical2/uploads/training_material/1484723031.docx', 0),
(7, 41, 'adf', 1, '2017-01-18 07:07:04', 'adf', 'http://betaprojex.com/Clinical2/uploads/training_material/1484723224.docx', 0),
(8, 48, 'adf', 1, '2017-01-18 07:14:00', 'adf', 'http://betaprojex.com/Clinical2/uploads/training_material/1484723640.docx', 0),
(9, 58, 'qweqwe', 1, '2017-01-19 01:55:26', '1', 'http://betaprojex.com/Clinical2/uploads/training_material/1484790926.jpg', 0),
(10, 22, 'qweqwe', 1, '2017-01-19 01:59:48', '1', 'http://betaprojex.com/Clinical2/uploads/training_material/1484791188.jpg', 0),
(11, 82, '1', 1, '2017-01-25 06:17:00', 'asd', 'http://betaprojex.com/Clinical2/uploads/training_material/1485325019.jpg', 0),
(12, 22, '1', 1, '2017-01-25 06:18:08', 'asd', 'http://betaprojex.com/Clinical2/uploads/training_material/1485325088.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `trials`
--

CREATE TABLE `trials` (
  `trial_id` int(11) NOT NULL,
  `trial_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trials`
--

INSERT INTO `trials` (`trial_id`, `trial_name`) VALUES
(1, 'I Stage'),
(2, 'II Stage'),
(3, 'III Stage'),
(4, 'IV Stage'),
(5, 'BE Trial'),
(6, 'Clinical verifications');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `up_id` int(11) NOT NULL,
  `up_name` varchar(255) NOT NULL COMMENT 'name of document',
  `role_id` int(11) NOT NULL COMMENT 'role_id of uploader',
  `up_ver` float NOT NULL COMMENT 'version of document',
  `up_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `up_desc` mediumtext NOT NULL,
  `up_path` varchar(255) NOT NULL,
  `up_type` int(11) NOT NULL COMMENT '1 - project document, 2 - project plan',
  `project_id` int(11) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table for `project documents` and `project plan` or more (?)';

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`up_id`, `up_name`, `role_id`, `up_ver`, `up_date`, `up_desc`, `up_path`, `up_type`, `project_id`, `databank`) VALUES
(1, 'é˜¿é“å¤«', 5, 1, '2016-12-22 09:41:14', '', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1482399674.docx', 3, 1, 0),
(2, 'é˜¿é“å¤«', 6, 1, '2016-12-22 09:41:54', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482399714.docx', 2, 1, 0),
(3, 'é˜¿é“å¤«', 6, 12, '2016-12-22 09:46:49', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482400009.docx', 1, 1, 0),
(4, 'adf ', 20, 2, '2016-12-22 09:48:36', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482400116.docx', 1, 1, 0),
(5, 'adf ', 6, 1, '2016-12-22 09:49:17', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482400157.docx', 2, 1, 0),
(6, 'adf ', 20, 1, '2016-12-22 09:50:31', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482400231.docx', 1, 1, 0),
(7, 'adf', 6, 1, '2016-12-22 09:55:15', 'adf', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1482400515.docx', 5, 1, 0),
(8, '1', 1, 1, '2016-12-23 03:16:31', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482462991.pdf', 1, 2, 0),
(9, '1', 1, 1, '2016-12-23 03:17:13', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482463033.png', 2, 2, 0),
(10, '1pre', 5, 1, '2016-12-23 03:26:55', 'pre pre', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1482463615.docx', 3, 2, 0),
(11, '2', 6, 2, '2016-12-23 03:37:03', '2', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482464223.docx', 2, 2, 0),
(12, 'adf', 5, 1, '2016-12-23 08:40:28', '', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1482482428.docx', 3, 3, 0),
(13, 'df', 6, 1, '2016-12-23 08:41:06', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482482466.docx', 2, 3, 0),
(14, 'adf', 6, 1, '2016-12-23 08:42:27', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482482547.docx', 1, 3, 0),
(15, 'daf', 20, 1, '2016-12-23 08:47:09', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482482829.docx', 1, 3, 0),
(16, 'adf', 6, 1, '2016-12-23 08:48:00', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482482880.docx', 2, 3, 0),
(17, 'adf', 20, 1, '2016-12-23 08:53:57', 'adf', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482483237.docx', 1, 3, 0),
(18, 'df', 6, 2, '2016-12-23 08:59:14', '', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1482483554.docx', 5, 3, 0),
(19, 'df', 6, 1, '2016-12-23 09:02:32', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482483752.docx', 1, 3, 0),
(20, 'adf', 1, 1, '2016-12-23 09:05:56', 'adf', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482483956.docx', 1, 3, 0),
(21, 'adf ', 6, 0, '2016-12-26 03:35:40', 'df', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1482672940.docx', 5, 3, 0),
(22, 'adf', 5, 1, '2016-12-26 03:56:18', '', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1482674178.docx', 3, 5, 0),
(23, 'er', 6, 12, '2016-12-26 03:59:48', 'df', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482674387.docx', 2, 5, 0),
(24, '12', 6, 1, '2016-12-26 04:01:29', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482674489.docx', 1, 5, 0),
(25, 'ha', 20, 12, '2016-12-26 04:05:03', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482674703.docx', 1, 5, 0),
(26, '12', 20, 12, '2016-12-26 04:08:02', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482674882.docx', 1, 5, 0),
(27, '12', 6, 12, '2016-12-26 04:09:18', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482674958.docx', 2, 5, 0),
(28, 'adf', 20, 12, '2016-12-26 04:12:08', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482675128.docx', 1, 5, 0),
(29, 'adf', 6, 1, '2016-12-26 04:13:36', '', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1482675216.docx', 5, 5, 0),
(30, 'k1', 1, 1, '2016-12-28 15:37:28', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482889048.png', 1, 6, 0),
(31, 'k2', 1, 1, '2016-12-28 15:37:54', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482889074.png', 1, 6, 0),
(32, 'z1', 1, 1, '2016-12-28 15:38:19', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482889099.png', 2, 6, 0),
(33, 'x2', 1, 1, '2016-12-28 15:38:44', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482889124.png', 2, 6, 0),
(34, 'x2', 1, 1, '2016-12-28 15:38:51', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482889131.png', 2, 6, 0),
(35, 'a', 1, 1, '2016-12-28 19:48:44', 'a', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482904124.txt', 1, 7, 0),
(36, 'b', 1, 2, '2016-12-28 19:49:01', 'b', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482904141.txt', 1, 7, 0),
(38, 'a', 1, 1, '2016-12-28 19:49:46', 'a', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482904186.txt', 2, 7, 0),
(39, 'b', 1, 1, '2016-12-28 19:49:56', 'b', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482904196.txt', 2, 7, 0),
(40, 'a', 5, 1, '2016-12-28 19:54:03', 'a', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1482904443.txt', 3, 7, 0),
(41, 'c', 6, 3, '2016-12-28 19:54:44', 'c', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482904484.txt', 2, 7, 0),
(42, 'c', 6, 3, '2016-12-28 20:07:06', 'c', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482905226.txt', 1, 7, 0),
(43, 'd', 20, 4, '2016-12-28 20:50:00', 'd', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482907800.txt', 1, 7, 0),
(44, 'd', 6, 4, '2016-12-28 20:50:42', 'd', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482907842.txt', 2, 7, 0),
(45, 'e', 20, 5, '2016-12-28 20:51:38', 'e', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482907898.txt', 1, 7, 0),
(46, 'a', 6, 1, '2016-12-28 21:12:52', 'a', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1482909172.txt', 5, 7, 0),
(47, 'b', 6, 2, '2016-12-28 21:13:04', 'b', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1482909184.txt', 5, 7, 0),
(48, 'test', 1, 1, '2016-12-28 22:33:50', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482914030.jpg', 1, 7, 0),
(49, 'adf', 1, 0, '2016-12-28 23:24:02', 'df', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482917042.docx', 1, 5, 0),
(50, '1', 6, 1, '2016-12-28 23:45:19', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482918319.txt', 1, 7, 0),
(55, 'a', 1, 1, '2016-12-29 17:38:25', 'a', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482982705.txt', 1, 7, 0),
(56, 'hjg', 1, 1, '2016-12-29 17:43:26', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482983006.docx', 2, 8, 0),
(57, 'é˜¿é“å¤«', 1, 1, '2016-12-30 19:06:18', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483074378.docx', 1, 12, 0),
(58, 'wrt', 5, 1, '2016-12-30 19:15:04', '', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1483074904.docx', 3, 12, 0),
(59, 'æ–°æ–¹æ¡ˆ', 6, 2, '2016-12-30 19:16:23', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1483074983.docx', 2, 12, 0),
(60, 'qdf', 6, 1, '2016-12-30 19:19:21', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483075161.docx', 1, 12, 0),
(61, 'huiyi jilu', 20, 1, '2016-12-30 19:33:59', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483076039.docx', 1, 12, 0),
(62, 'xifnagan', 6, 2, '2016-12-30 19:34:48', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1483076088.docx', 2, 12, 0),
(63, 'æ‰¹æ–‡', 20, 1, '2016-12-30 19:38:51', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483076331.docx', 1, 12, 0),
(64, 'å•Šå¤§æ¡†æž¶å‘', 6, 1, '2016-12-30 19:49:09', 'é˜¿é“å¤«', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1483076949.docx', 5, 12, 0),
(65, '2', 1, 2, '2016-12-30 21:12:03', '2', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483081923.jpg', 1, 13, 0),
(66, '2', 1, 2, '2016-12-30 21:12:07', '2', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483081927.jpg', 1, 13, 0),
(67, '1', 1, 1, '2016-12-30 21:13:28', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1483082008.jpg', 2, 13, 0),
(68, '1', 5, 1, '2016-12-30 21:31:40', '1', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1483083100.jpg', 3, 13, 0),
(69, '1', 6, 1, '2016-12-30 21:35:53', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1483083353.jpg', 2, 13, 0),
(70, '1', 6, 1, '2016-12-30 21:37:36', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483083456.jpg', 1, 13, 0),
(71, '1', 20, 11, '2016-12-30 21:43:00', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483083780.jpg', 1, 13, 0),
(72, '1', 20, 1, '2016-12-30 21:57:45', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483084665.jpg', 1, 13, 0),
(73, '1', 6, 1, '2016-12-30 21:58:54', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1483084734.jpg', 2, 13, 0),
(74, '1', 6, 1, '2016-12-30 22:02:09', '11', 'http://betaprojex.com/Clinical2/uploads/project_plan/1483084929.jpg', 2, 13, 0),
(75, '1', 6, 1, '2016-12-30 22:02:14', '11', 'http://betaprojex.com/Clinical2/uploads/project_plan/1483084934.jpg', 2, 13, 0),
(76, '1', 20, 11, '2016-12-30 22:04:25', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483085065.jpg', 1, 13, 0),
(77, '1', 20, 1, '2016-12-30 22:11:32', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483085492.jpg', 1, 13, 0),
(78, '1', 6, 1, '2016-12-30 22:14:24', '1', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1483085664.jpg', 5, 13, 0),
(79, '1', 1, 1, '2016-12-31 00:03:47', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483092227.docx', 1, 13, 0),
(80, '1', 1, 11, '2016-12-31 00:04:38', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483092278.docx', 1, 13, 0),
(81, '1', 6, 1, '2016-12-31 00:07:17', '11', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483092437.docx', 1, 13, 0),
(82, '11', 6, 1, '2016-12-31 00:07:52', '11', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483092472.docx', 1, 13, 0),
(83, '1', 1, 1, '2016-12-31 00:18:09', '11', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483093089.docx', 1, 13, 0),
(84, 'æ–¹æ¡ˆ1', 1, 1, '2017-01-10 15:54:54', 'AD', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484013294.png', 2, 20, 0),
(85, 'æ–¹æ¡ˆ1', 1, 1, '2017-01-10 15:54:57', 'AD', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484013297.png', 2, 20, 0),
(86, 'yijian', 5, 1, '2017-01-10 16:13:57', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014437.docx', 3, 20, 0),
(87, 'yijian', 5, 1, '2017-01-10 16:14:00', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014439.png', 3, 20, 0),
(88, 'yijian', 5, 1, '2017-01-10 16:14:00', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014440.docx', 3, 20, 0),
(89, 'yijian', 5, 1, '2017-01-10 16:14:02', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014442.docx', 3, 20, 0),
(90, 'yijian', 5, 1, '2017-01-10 16:14:04', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014444.docx', 3, 20, 0),
(91, 'yijian', 5, 1, '2017-01-10 16:14:05', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014445.docx', 3, 20, 0),
(92, 'yijian', 5, 1, '2017-01-10 16:14:10', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014450.docx', 3, 20, 0),
(93, 'yijian', 5, 1, '2017-01-10 16:14:11', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014450.docx', 3, 20, 0),
(95, 'test', 6, 2, '2017-01-10 21:11:15', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484032275.pdf', 2, 20, 0),
(96, 'æ–°æ–¹æ¡ˆ', 6, 1, '2017-01-10 21:23:49', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484033029.docx', 2, 20, 0),
(97, 'haha', 6, 1, '2017-01-10 21:25:29', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484033129.docx', 1, 20, 0),
(98, '12', 6, 1, '2017-01-10 22:06:35', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484035595.docx', 1, 20, 0),
(99, 'ç—…ä¾‹æŠ¥å‘Šè¡¨', 1, 1, '2017-01-10 23:09:35', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484039375.docx', 1, 27, 0),
(100, 'çŸ¥æƒ…åŒæ„ä¹¦', 1, 1, '2017-01-10 23:10:11', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484039411.docx', 1, 27, 0),
(101, 'ä¸´åºŠè¯•éªŒè®¡åˆ’ä¸Žç ”ç©¶æ–¹æ¡ˆ', 1, 1, '2017-01-10 23:10:41', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484039441.docx', 1, 27, 0),
(102, 'ä¸´åºŠè¯•éªŒè®¡åˆ’ä¸Žç ”ç©¶æ–¹æ¡ˆ', 1, 1, '2017-01-10 23:10:58', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484039458.docx', 2, 27, 0),
(103, 'ç ”ç©¶è€…å‘èµ·çš„é¡¹ç›®å®¡è®®è¡¨', 5, 1, '2017-01-10 23:29:53', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484040593.docx', 3, 27, 0),
(104, 'äºŒç”²åŒèƒç ”ç©¶æ–¹æ¡ˆï¼ˆä¿®æ”¹ç‰ˆï¼‰', 6, 2, '2017-01-10 23:31:56', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484040716.docx', 2, 27, 0),
(105, 'äºŒç”²åŒèƒç ”ç©¶æ–¹æ¡ˆ--ç­¾å­—ç›–ç« è¡¨', 6, 1, '2017-01-10 23:36:09', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484040967.pdf', 1, 27, 0),
(106, 'äºŒç”²åŒèƒç ”ç©¶ç›¸å…³èµ„æ–™1', 6, 1, '2017-01-10 23:36:36', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484040996.pdf', 1, 27, 0),
(107, 'äºŒç”²åŒèƒç ”ç©¶ç›¸å…³èµ„æ–™2', 6, 1, '2017-01-10 23:36:54', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484041014.pdf', 1, 27, 0),
(108, 'äºŒç”²åŒèƒç ”ç©¶ç›¸å…³èµ„æ–™3', 6, 1, '2017-01-10 23:37:16', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484041036.pdf', 1, 27, 0),
(109, 'ä¼¦ç†å§”å‘˜ä¼šä¼šè®®è®°å½•', 20, 1, '2017-01-10 23:51:44', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484041904.pdf', 1, 27, 0),
(110, 'ä¼¦ç†å§”å‘˜ä¼šæ„è§æ•´ç†', 20, 1, '2017-01-10 23:52:21', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484041941.pdf', 1, 27, 0),
(112, 'äºŒç”²åŒèƒç ”ç©¶æ–¹æ¡ˆæœ€ç»ˆç‰ˆ', 6, 3, '2017-01-11 00:03:58', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484042638.docx', 2, 27, 0),
(113, 'ä¼¦ç†å§”å‘˜ä¼šä¸»ä»»ç­¾åçš„æ‰¹å‡†æ–‡ä»¶', 20, 1, '2017-01-11 00:10:55', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484043055.pdf', 1, 27, 0),
(114, 'å·¥ä½œè§„èŒƒåŠè®°å½•è¡¨', 6, 1, '2017-01-11 00:15:45', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1484043345.doc', 5, 27, 0),
(115, 'adf ', 5, 1, '2017-01-11 21:40:26', 'adf', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484120426.docx', 3, 28, 0),
(116, 'kjdf', 6, 1, '2017-01-11 21:42:22', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484120542.docx', 2, 28, 0),
(117, 'kjdf', 6, 1, '2017-01-11 21:42:28', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484120548.docx', 2, 28, 0),
(118, 'kajdf', 6, 1, '2017-01-11 21:44:25', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484120664.docx', 1, 28, 0),
(119, 'hahah', 20, 12, '2017-01-11 21:51:28', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484121088.docx', 1, 28, 0),
(120, 'hahah', 20, 12, '2017-01-11 21:51:33', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484121093.docx', 1, 28, 0),
(121, 'kjdf', 20, 1, '2017-01-11 21:54:18', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484121258.docx', 1, 28, 0),
(122, 'kjdf', 20, 1, '2017-01-11 21:54:26', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484121266.docx', 1, 28, 0),
(123, 'df', 6, 1, '2017-01-11 21:55:26', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484121326.docx', 2, 28, 0),
(124, 'jh', 20, 1, '2017-01-11 21:57:18', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484121438.docx', 1, 28, 0),
(125, '', 6, 0, '2017-01-12 22:01:48', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484208108.png', 1, 1, 0),
(127, 'ã€Šç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡äººä½“ç”Ÿç‰©åˆ©ç”¨åº¦å’Œç”Ÿç‰©ç­‰æ•ˆæ€§ç ”ç©¶ã€‹CRFè¡¨æ ¼', 22, 1, '2017-01-20 13:49:16', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484869756.doc', 1, 34, 0),
(128, 'ã€Šç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡äººä½“ç”Ÿç‰©åˆ©ç”¨åº¦å’Œç”Ÿç‰©ç­‰æ•ˆæ€§ç ”ç©¶ã€‹çŸ¥æƒ…åŒæ„ä¹¦', 22, 1, '2017-01-20 13:49:59', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484869799.docx', 1, 34, 0),
(129, 'ã€Šç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡äººä½“ç”Ÿç‰©åˆ©ç”¨åº¦å’Œç”Ÿç‰©ç­‰æ•ˆæ€§ç ”ç©¶ã€‹ç ”ç©¶æ–¹æ¡ˆ', 22, 1, '2017-01-20 13:50:25', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484869825.docx', 2, 34, 0),
(130, 'æ‚£æ•™', 6, 1, '2017-01-22 17:23:59', '', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1485055439.docx', 5, 27, 0),
(131, 'adf ', 6, 3, '2017-02-07 00:16:09', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1486376169.docx', 2, 19, 0),
(132, 'adf', 6, 1, '2017-02-07 00:19:11', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1486376351.docx', 2, 39, 0),
(133, 'ä¼š', 6, 1, '2017-02-07 02:04:45', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1486382685.docx', 2, 54, 0),
(134, 'jh', 6, 3, '2017-02-07 02:07:04', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1486382824.docx', 1, 54, 0),
(135, 'daf', 20, 12, '2017-02-07 02:17:22', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1486383442.docx', 1, 54, 0),
(136, 'adf', 6, 12, '2017-02-07 02:17:54', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1486383474.docx', 2, 54, 0),
(137, 'é˜¿é“å¤«', 20, 23, '2017-02-07 02:23:19', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1486383799.docx', 1, 54, 0),
(138, 'adf', 6, 12, '2017-02-08 16:26:40', 'adf', 'http://betaprojex.com/clinical/uploads/project_plan/1486520800.docx', 2, 57, 0),
(139, 'adf', 6, 12, '2017-02-08 16:32:44', 'adf', 'http://betaprojex.com/clinical/uploads/project_plan/1486521164.docx', 2, 57, 0),
(140, 'fg', 6, 23, '2017-02-08 17:30:37', 'adf', 'http://betaprojex.com/clinical/uploads/project_documents/1486524637.docx', 1, 57, 0),
(141, 'é‡‘å¡äº¤ç”µè´¹', 20, 1, '2017-02-08 17:38:54', 'é˜¿é“å¤«', 'http://betaprojex.com/clinical/uploads/project_documents/1486525134.docx', 1, 57, 0),
(142, 'adf', 20, 12, '2017-02-08 17:43:08', 'adf', 'http://betaprojex.com/clinical/uploads/project_documents/1486525388.docx', 1, 57, 0),
(143, 'adf1', 6, 2, '2017-02-08 17:43:46', 'adf', 'http://betaprojex.com/clinical/uploads/project_plan/1486525426.docx', 2, 57, 0),
(144, 'é˜¿é“å¤«', 20, 0, '2017-02-08 17:49:08', 'aåœ°æ–¹', 'http://betaprojex.com/clinical/uploads/project_documents/1486525748.docx', 1, 57, 0),
(145, 'å¿«æ·é˜¿é“å¤«', 1, 1, '2017-02-09 14:45:51', 'é˜¿é“å¤«', 'http://betaprojex.com/clinical/uploads/project_documents/1486601151.docx', 1, 60, 0),
(146, 'é˜¿é“å¤«', 1, 23, '2017-02-09 14:47:08', 'é˜¿é“å¤«', 'http://betaprojex.com/clinical/uploads/project_plan/1486601228.docx', 2, 60, 0),
(147, 'é˜¿é“å¤«', 1, 23, '2017-02-09 15:11:38', 'é˜¿é“å¤«', 'http://betaprojex.com/clinical/uploads/project_documents/1486602698.docx', 1, 62, 0),
(148, 'çš„æ–¹æ³•', 1, 12, '2017-02-09 15:23:15', 'é˜¿é“å¤«', 'http://betaprojex.com/clinical/uploads/project_documents/1486603395.docx', 1, 64, 0),
(149, 'kljalkdjf ', 1, 12, '2017-02-09 15:38:19', 'adf', 'http://betaprojex.com/clinical/uploads/project_documents/1486604295.docx', 1, 61, 1),
(150, 'kljalkdjf ', 1, 12, '2017-02-09 15:38:25', 'adf', 'http://betaprojex.com/clinical/uploads/project_documents/1486604305.docx', 1, 61, 1),
(151, 'çœ‹å•¦äº¤ç”µè´¹', 6, 12, '2017-02-09 15:49:04', 'é˜¿é“å¤«', 'http://betaprojex.com/clinical/uploads/project_plan/1486604944.docx', 2, 64, 1),
(152, 'çœ‹å•¦äº¤ç”µè´¹', 6, 12, '2017-02-09 15:49:15', 'é˜¿é“å¤«', 'http://betaprojex.com/clinical/uploads/project_plan/1486604955.docx', 2, 64, 1),
(153, 'AKåŠ³åŠ¨çº çº·', 6, 12, '2017-02-09 15:55:29', 'é˜¿é“å¤«', 'http://betaprojex.com/clinical/uploads/project_documents/1486605329.docx', 1, 64, 1),
(154, 'AKåŠ³åŠ¨çº çº·', 6, 12, '2017-02-09 15:55:37', 'é˜¿é“å¤«', 'http://betaprojex.com/clinical/uploads/project_documents/1486605337.docx', 1, 64, 1),
(155, 'æ˜¯éžè§‚', 20, 12, '2017-02-09 15:57:41', '', 'http://betaprojex.com/clinical/uploads/project_documents/1486605461.jpg', 1, 64, 0),
(156, 'AD', 6, 12, '2017-02-09 15:58:26', '', 'http://betaprojex.com/clinical/uploads/project_plan/1486605506.jpg', 2, 64, 0),
(157, 'adf ', 20, 12, '2017-02-09 15:59:31', '', 'http://betaprojex.com/clinical/uploads/project_documents/1486605571.jpg', 1, 64, 0),
(158, 'Test Databank PD 1', 1, 1, '2017-02-09 16:04:10', 'Checked Upload', 'http://betaprojex.com/clinical/uploads/project_documents/1486605850.png', 1, 65, 1),
(159, 'Test Databank PP', 1, 1, '2017-02-09 16:04:39', 'Checked Upload', 'http://betaprojex.com/clinical/uploads/project_plan/1486605879.png', 2, 65, 1),
(160, 'Test Databank PRE', 5, 1, '2017-02-09 16:12:30', 'Checked Upload', 'http://betaprojex.com/clinical/uploads/pre_comments/1486606350.png', 3, 65, 1),
(161, 'Test Databank I', 6, 1, '2017-02-09 16:13:33', 'Checked Multiple Upload', 'http://betaprojex.com/clinical/uploads/project_plan/1486606413.png', 2, 65, 1),
(162, 'ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„IæœŸä¸´åºŠç ”ç©¶', 1, 1, '2017-02-09 20:29:04', '', 'http://betaprojex.com/clinical/uploads/project_documents/1486621744.doc', 1, 66, 1),
(163, 'ç¾Žè±ç”Ÿç‰©åŒ»ç–—æ ·æœ¬ç®¡ç†ç³»ç»Ÿè®¾è®¡æ–¹æ¡ˆä¹¦', 1, 1, '2017-02-09 20:35:09', '', 'http://betaprojex.com/clinical/uploads/project_plan/1486622109.docx', 2, 66, 1),
(164, 'ç”²ç£ºé…¸æ™®å–¹æ›¿å°¼ç‰‡åœ¨æˆäººå¤å‘éš¾æ²»æ€§æ¶æ€§æ·‹å·´ç˜¤æ‚£è€…å£æœç»™è¯çš„IæœŸä¸´åºŠç ”ç©¶', 1, 1, '2017-02-09 20:46:10', '', 'http://betaprojex.com/clinical/uploads/project_plan/1486622770.doc', 2, 67, 1),
(165, 'ç¾Žè±ç”Ÿç‰©åŒ»ç–—æ ·æœ¬ç®¡ç†ç³»ç»Ÿè®¾è®¡æ–¹æ¡ˆä¹¦', 1, 1, '2017-02-09 20:46:36', '', 'http://betaprojex.com/clinical/uploads/project_documents/1486622796.docx', 1, 67, 1),
(166, 'ä¸‰è¯', 1, 1, '2017-02-09 20:47:28', '', 'http://betaprojex.com/clinical/uploads/project_documents/1486622848.pdf', 1, 67, 1),
(167, 'åŒè¡Œè¯„è®®ä¸“å®¶æ„è§', 5, 1, '2017-02-09 20:53:33', '', 'http://betaprojex.com/clinical/uploads/pre_comments/1486623213.pdf', 3, 67, 1),
(168, 'åŒæ–¹ç­¾å­—ç›–ç« æ–¹æ¡ˆåŠå…¶ä»–èµ„æ–™æ‰«æä»¶', 6, 1, '2017-02-09 20:59:17', '', 'http://betaprojex.com/clinical/uploads/project_plan/1486623557.pdf', 2, 67, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(5) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 - active, 0 - disable',
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `middle_initial` varchar(5) DEFAULT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - male, 1 - female',
  `company` varchar(150) NOT NULL,
  `department` varchar(100) NOT NULL,
  `job_title` varchar(200) DEFAULT NULL,
  `birthdate` datetime NOT NULL,
  `mobile_num` varchar(30) NOT NULL,
  `password` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  `doc_id_card` varchar(100) NOT NULL,
  `doc_gcp_card` varchar(100) NOT NULL,
  `doc_diploma` varchar(100) NOT NULL,
  `doc_degree` varchar(100) NOT NULL,
  `doc_other` varchar(100) NOT NULL,
  `is_admin` tinyint(1) NOT NULL COMMENT '0 - non admin, 1 - admin',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `is_active`, `fname`, `lname`, `middle_initial`, `gender`, `company`, `department`, `job_title`, `birthdate`, `mobile_num`, `password`, `email`, `doc_id_card`, `doc_gcp_card`, `doc_diploma`, `doc_degree`, `doc_other`, `is_admin`, `date_created`, `date_updated`) VALUES
(1, 1, 'Van', 'Vanleor', '', 0, '', '', NULL, '1995-04-06 00:00:00', '12345', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'van@admin.com', '', '', '', '', '', 1, '2016-11-03 21:34:02', '2016-11-10 01:53:52'),
(2, 1, 'Enzo', 'Lorenzo', '', 0, '', '', NULL, '0000-00-00 00:00:00', '123', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'enzo@mail.com', '', '', '', '', '', 0, '2016-11-05 01:40:51', '2016-12-14 01:44:32'),
(48, 1, 'TestPI', 'TESTVAN', NULL, 0, '', '', NULL, '2016-11-09 00:00:00', '12345PI', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'PI@admin.com', '', '', '', '', '', 0, '2016-11-19 04:50:26', '2016-11-21 08:01:12'),
(49, 1, 'TestTFO', 'TESTVAN', NULL, 0, '', '', NULL, '2016-11-11 00:00:00', '12345TFO', 'd033e22ae348aeb5660fc2140aec35850c4da997', '857357680@qq.com', '', '', '', '', '', 0, '2016-11-19 04:52:37', '2016-12-02 18:37:46'),
(50, 1, 'TestECC', 'TESTVAN', NULL, 0, 'company', '', NULL, '2016-10-31 00:00:00', '12345ECC', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'ECC@admin.com', '', '', '', '', '', 0, '2016-11-21 08:02:05', '2016-12-02 04:07:59'),
(51, 1, 'TestPRE', 'TESTVAN', NULL, 0, 'company', '', NULL, '2016-11-15 00:00:00', '12345PRE', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'PRE@admin.com', '', '', '', '', '', 0, '2016-11-21 08:28:41', '2016-12-02 04:07:50'),
(52, 1, 'TestI', 'TESTVAN', NULL, 0, '', '', NULL, '2016-11-22 00:00:00', '12345I', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'I@admin.com', '', '', '', '', '', 0, '2016-11-21 08:33:41', '2016-11-21 10:30:31'),
(55, 1, 'TestECM', 'TESTVAN', NULL, 0, 'comp', '', NULL, '2016-11-15 00:00:00', '12345ECM', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'ECM@admin.com', '', '', '', '', '', 0, '2016-11-21 10:34:06', '2016-12-02 04:08:07'),
(56, 1, 'Clinician', 'One', NULL, 0, '', 'Dept 1', 'jobbb 4', '2016-11-22 00:00:00', '12345C', 'd033e22ae348aeb5660fc2140aec35850c4da997', '12345C@gmail.com', '', '', '', '', '', 0, '2016-11-29 07:25:49', '2016-12-04 07:28:43'),
(57, 1, 'Research nurse', 'One', NULL, 0, '', 'Dept 2', 'jobbb 3', '2016-11-22 00:00:00', '12345RN', 'd033e22ae348aeb5660fc2140aec35850c4da997', '12345N@gmail.com', '', '', '', '', '', 0, '2016-11-29 07:26:41', '2016-12-04 07:28:40'),
(58, 1, 'Medicine controller', 'One', NULL, 0, '', 'Dept 2', 'job 2', '2016-11-16 00:00:00', '12345MC', 'd033e22ae348aeb5660fc2140aec35850c4da997', '12345MC@gmail.com', '', '', '', '', '', 0, '2016-11-29 07:27:26', '2016-12-04 07:28:36'),
(59, 1, 'Biological controller', 'One', NULL, 0, 'Companys', 'Dept 1', 'title job 1', '2016-11-16 00:00:00', '12345BGC', 'd033e22ae348aeb5660fc2140aec35850c4da997', '12345BC@gmail.com', '', '', '', '', '', 0, '2016-11-29 07:27:59', '2016-12-14 01:44:27'),
(61, 0, 'testssss', 'test', NULL, 1, 'cmp', '', NULL, '0000-00-00 00:00:00', '123123', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'mai@mail.comss', '', '', '', '', '', 0, '2016-12-02 03:49:48', '2016-12-02 04:01:07'),
(62, 1, 'TestUser', 'AllPosition', NULL, 1, 'QA!', 'QA!', NULL, '0000-00-00 00:00:00', '12345ALL', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'all@test.com', '', '', '', '', '', 0, '2016-12-02 18:12:44', '2016-12-04 07:49:20'),
(63, 1, 'TestECS', 'TESTVAN', NULL, 0, 'company', '', NULL, '2016-10-31 00:00:00', '12345ECS', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'ECS@admin.com', '', '', '', '', '', 0, '2016-11-21 08:02:05', '2016-12-02 04:07:59'),
(64, 1, 'TestDA', 'TESTVAN', NULL, 0, 'company', 'depart 1', 'Data Admin', '2016-10-31 00:00:00', '12345DA', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'DA@admin.com', '', '', '', '', '', 0, '2016-11-21 08:02:05', '2016-12-02 04:07:59'),
(65, 1, 'ç”³åŠžè€…1', 'æŽæ˜Ž', NULL, 0, 'ä¸­å±±å¤§å­¦', '', NULL, '0000-00-00 00:00:00', 'ç”³åŠžè€…1', 'd033e22ae348aeb5660fc2140aec35850c4da997', '475673@qq.com', 'http://betaprojex.com/Clinical2/uploads/users/id_14841217161484121716.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14841217161484121716.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14841217161484121716.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14841217161484121716.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14841217161484121716.png', 0, '2017-01-11 08:01:56', '0000-00-00 00:00:00'),
(73, 1, 'æŽ', 'ä¸»ä»»', NULL, 0, 'ä¸­å±±å¤§å­¦', '', NULL, '0000-00-00 00:00:00', 'æŽä¸»ä»»PI', 'd033e22ae348aeb5660fc2140aec35850c4da997', '123', 'http://betaprojex.com/Clinical2/uploads/users/id_14841866791484186679.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14841866791484186679.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14841866791484186679.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14841866791484186679.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14841866791484186679.png', 0, '2017-01-12 02:04:39', '0000-00-00 00:00:00'),
(74, 1, 'å¼ ', 'ä¸‰', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'å¼ ä¸‰TFO', 'd033e22ae348aeb5660fc2140aec35850c4da997', '12345', 'http://betaprojex.com/Clinical2/uploads/users/id_14841873531484187353.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14841873531484187353.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14841873531484187353.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14841873531484187353.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14841873531484187353.png', 0, '2017-01-12 02:15:53', '0000-00-00 00:00:00'),
(75, 1, 'æŽ', 'æ˜Ž', NULL, 0, 'åŒ»é™¢', '', NULL, '0000-00-00 00:00:00', '1234567', '3d7df60b781bbe23fcc7e5b60c7adbadd0675e19', '12336452', 'http://betaprojex.com/Clinical2/uploads/users/id_14845686211484568621.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14845686211484568621.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14845686211484568621.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14845686211484568621.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14845686211484568621.png', 0, '2017-01-16 12:10:21', '0000-00-00 00:00:00'),
(76, 0, 'ä¸»è¦ç ”ç©¶è€…', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ä¸»è¦ç ”ç©¶è€…1', 'd033e22ae348aeb5660fc2140aec35850c4da997', '857357680@qq.com', 'http://betaprojex.com/Clinical2/uploads/users/id_14847355521484735552.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847355521484735552.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847355521484735552.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847355521484735552.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847355521484735552.png', 0, '2017-01-18 10:32:32', '0000-00-00 00:00:00'),
(78, 1, 'ç ”ç©¶è€…', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ç ”ç©¶è€…1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'yanjiuzhe', 'http://betaprojex.com/Clinical2/uploads/users/id_14847356381484735638.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847356381484735638.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847356381484735638.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847356381484735638.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847356381484735638.png', 0, '2017-01-18 10:33:58', '0000-00-00 00:00:00'),
(79, 1, 'ç”Ÿç‰©æ ·æœ¬ç®¡ç†å‘˜', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ç”Ÿç‰©æ ·æœ¬ç®¡ç†å‘˜1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'shengwuyangbenguanliyuan', 'http://betaprojex.com/Clinical2/uploads/users/id_14847357291484735729.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847357291484735729.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847357291484735729.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847357291484735729.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847357291484735729.png', 0, '2017-01-18 10:35:29', '0000-00-00 00:00:00'),
(80, 1, 'è¯å“ç®¡ç†å‘˜', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'è¯å“ç®¡ç†å‘˜1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'yaopinguanliyuan', 'http://betaprojex.com/Clinical2/uploads/users/id_14847358121484735812.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847358121484735812.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847358121484735812.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847358121484735812.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847358121484735812.png', 0, '2017-01-18 10:36:52', '0000-00-00 00:00:00'),
(81, 1, 'ä¸´åºŠåŒ»ç”Ÿ', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ä¸´åºŠåŒ»ç”Ÿ1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'linchuangyisheng', 'http://betaprojex.com/Clinical2/uploads/users/id_14847384991484738499.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847384991484738499.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847384991484738499.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847384991484738499.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847384991484738499.png', 0, '2017-01-18 11:21:39', '0000-00-00 00:00:00'),
(82, 1, 'ç ”ç©¶æŠ¤å£«', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ç ”ç©¶æŠ¤å£«1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'yanjiuhushi', 'http://betaprojex.com/Clinical2/uploads/users/id_14847385611484738561.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847385611484738561.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847385611484738561.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847385611484738561.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847385611484738561.png', 0, '2017-01-18 11:22:41', '0000-00-00 00:00:00'),
(84, 1, 'åŒè¡Œè¯„è®®ä¸“å®¶', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'åŒè¡Œè¯„è®®ä¸“å®¶1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'tonghangpingyizhuanjia', 'http://betaprojex.com/Clinical2/uploads/users/id_14847386541484738654.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847386541484738654.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847386541484738654.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847386541484738654.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847386541484738654.png', 0, '2017-01-18 11:24:14', '0000-00-00 00:00:00'),
(85, 1, 'ä¼¦ç†å§”å‘˜ä¼šç§˜ä¹¦', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ä¼¦ç†å§”å‘˜ä¼šç§˜ä¹¦1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'lunliweiyuanhuimishu', 'http://betaprojex.com/Clinical2/uploads/users/id_14847387351484738735.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847387351484738735.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847387351484738735.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847387351484738735.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847387351484738735.png', 0, '2017-01-18 11:25:35', '0000-00-00 00:00:00'),
(86, 1, 'ä¼¦ç†å§”å‘˜ä¼šä¸»ä»»', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ä¼¦ç†å§”å‘˜ä¼šä¸»ä»»1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'lunliweiyuanhuizhuren', 'http://betaprojex.com/Clinical2/uploads/users/id_14847388081484738808.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847388081484738808.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847388081484738808.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847388081484738808.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847388081484738808.png', 0, '2017-01-18 11:26:48', '0000-00-00 00:00:00'),
(87, 1, 'ä¼¦ç†å§”å‘˜ä¼šå§”å‘˜', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ä¼¦ç†å§”å‘˜ä¼šå§”å‘˜1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'lunliweiyuanhuiweiyuan', 'http://betaprojex.com/Clinical2/uploads/users/id_14847388821484738882.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847388821484738882.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847388821484738882.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847388821484738882.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847388821484738882.png', 0, '2017-01-18 11:28:02', '0000-00-00 00:00:00'),
(88, 1, 'è¯•éªŒæœºæž„', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'è¯•éªŒæœºæž„1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'shiyanjigou', 'http://betaprojex.com/Clinical2/uploads/users/id_14847389551484738955.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847389551484738955.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847389551484738955.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847389551484738955.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847389551484738955.png', 0, '2017-01-18 11:29:15', '0000-00-00 00:00:00'),
(89, 1, 'q', '', NULL, 0, 'q', '', NULL, '0000-00-00 00:00:00', 'q', '22ea1c649c82946aa6e479e1ffd321e4a318b1b0', 'q', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 0, '2017-01-19 06:28:09', '0000-00-00 00:00:00'),
(90, 1, 'w', '', NULL, 0, 'w', '', NULL, '0000-00-00 00:00:00', 'w', 'aff024fe4ab0fece4091de044c58c9ae4233383a', 'w', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 0, '2017-01-19 06:28:26', '0000-00-00 00:00:00'),
(91, 1, 'zy', '', NULL, 0, 'zy', '', NULL, '0000-00-00 00:00:00', 'zy', 'dcccef1c61b9892f75822537bc6ad84a598af2e9', 'zy', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 0, '2017-01-19 06:34:26', '0000-00-00 00:00:00'),
(92, 0, 'test', '', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', '1', '356a192b7913b04c54574d18c28d46e6395428ab', '1', 'http://betaprojex.com/Clinical2/uploads/users/id_14848123721484812372.pdf', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14848123721484812372.pdf', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14848123721484812372.pdf', 'http://betaprojex.com/Clinical2/uploads/users/degree_14848123721484812372.pdf', 'http://betaprojex.com/Clinical2/uploads/users/other_14848123721484812372.pdf', 0, '2017-01-19 07:52:52', '0000-00-00 00:00:00'),
(93, 1, 'a', '', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', '2', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8', '2', 'http://betaprojex.com/Clinical2/uploads/users/id_14848124571484812457.pdf', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14848124571484812457.pdf', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14848124571484812457.pdf', 'http://betaprojex.com/Clinical2/uploads/users/degree_14848124571484812457.pdf', 'http://betaprojex.com/Clinical2/uploads/users/other_14848124571484812457.pdf', 0, '2017-01-19 07:54:17', '0000-00-00 00:00:00'),
(94, 1, '9', '', NULL, 0, '9', '', NULL, '0000-00-00 00:00:00', '9', '0ade7c2cf97f75d009975f4d720d1fa6c19f4897', '9', 'http://betaprojex.com/Clinical2/uploads/users/id_14848151181484815118.pdf', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14848151181484815118.pdf', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14848151181484815118.pdf', 'http://betaprojex.com/Clinical2/uploads/users/degree_14848151181484815118.pdf', 'http://betaprojex.com/Clinical2/uploads/users/other_14848151181484815118.pdf', 0, '2017-01-19 08:38:38', '0000-00-00 00:00:00'),
(95, 1, 'å—è¯•è€…ç®¡ç†å‘˜', '', NULL, 0, 'aåœ°æ–¹a', 'aåœ°æ–¹', NULL, '0000-00-00 00:00:00', '12345SC', 'd033e22ae348aeb5660fc2140aec35850c4da997', '3476283', 'http://betaprojex.com/clinical/uploads/users/id_14865263271486526327.docx', 'http://betaprojex.com/clinical/uploads/users/gcp_14865263271486526327.png', 'http://betaprojex.com/clinical/uploads/users/diploma_14865263271486526327.png', 'http://betaprojex.com/clinical/uploads/users/degree_14865263271486526327.png', 'http://betaprojex.com/clinical/uploads/users/other_14865263271486526327.png', 0, '2017-02-08 03:58:47', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `volunteers`
--

CREATE TABLE `volunteers` (
  `volunteer_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `v_name` varchar(150) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `bday` date NOT NULL,
  `nation` varchar(150) NOT NULL,
  `native` varchar(150) NOT NULL,
  `married` varchar(3) NOT NULL,
  `blood_type` varchar(20) NOT NULL,
  `surgery` varchar(3) NOT NULL,
  `fam_history` text NOT NULL,
  `smoker` varchar(3) NOT NULL,
  `drinker` varchar(3) NOT NULL,
  `allergies` text NOT NULL,
  `height` varchar(15) NOT NULL,
  `weight` varchar(15) NOT NULL,
  `bmi` varchar(50) NOT NULL,
  `med_history` text NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `remarks` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '1 - is currently occupied as subject, 0 - available as subject',
  `date_status` date NOT NULL,
  `status_updated_by` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_update` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `id_card_num` varchar(255) NOT NULL,
  `id_card_path` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `volunteers`
--

INSERT INTO `volunteers` (`volunteer_id`, `subject_id`, `v_name`, `gender`, `bday`, `nation`, `native`, `married`, `blood_type`, `surgery`, `fam_history`, `smoker`, `drinker`, `allergies`, `height`, `weight`, `bmi`, `med_history`, `mobile`, `remarks`, `status`, `date_status`, `status_updated_by`, `date_added`, `added_by`, `date_update`, `updated_by`, `id_card_num`, `id_card_path`) VALUES
(1, 1, 'adf', 'Male', '2016-12-22', 'adf', 'adf', 'Yes', 'adf', 'Yes', 'adf', 'Yes', 'Yes', 'adf', '123', '4', '23', 'adf1', 'adf', '', 1, '0000-00-00', 0, '2016-12-22', 9, '0000-00-00', 0, 'adf', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482401097.docx'),
(2, 2, '123', 'Male', '2016-12-23', '123', '123', 'No', '123', 'No', '123', 'No', 'No', '123', '123', '123', '123', '123', '123', '', 1, '0000-00-00', 0, '2016-12-23', 9, '0000-00-00', 0, '123', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482481227.png'),
(3, 3, 'åœ°æ–¹', 'Male', '2016-12-02', 'é˜¿é“å¤«a', 'ADf', 'Yes', 'ADf', 'Yes', 'é˜¿é“å¤«', 'Yes', 'Yes', 'é˜¿é“å¤«', '123', '123', '124', 'é˜¿é“å¤«', '3434', '', 1, '0000-00-00', 0, '2016-12-23', 9, '0000-00-00', 0, '122334', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482485087.docx'),
(4, 4, 'adf', 'Male', '2016-12-06', 'adf', 'adf', 'Yes', 'adf', 'Yes', 'df', 'Yes', 'Yes', 'adf', '12231', '12', '23', 'adf1', '1213', '', 1, '0000-00-00', 0, '2016-12-25', 9, '0000-00-00', 0, '1243', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482675300.docx'),
(5, 5, 'klajdf', 'Male', '2016-12-15', 'adf', 'adf', 'Yes', 'adf', 'Yes', 'sdf', 'Yes', 'Yes', 'adf', '12', '12', '12', '12', '12', '', 1, '0000-00-00', 0, '2016-12-27', 9, '0000-00-00', 0, '12', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482806191.png'),
(6, 6, 'test', 'Male', '2016-12-01', 'aa', 'a', 'Yes', 'a', 'Yes', 'a', 'Yes', 'Yes', 'a', 'a', 'a', 'a', 'a', 'a', '', 1, '0000-00-00', 0, '2016-12-27', 9, '0000-00-00', 0, 'a', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482810873.txt'),
(7, 8, 'a', 'Male', '2016-12-28', 'a', 'a', 'Yes', 'a', 'Yes', 'a', 'Yes', 'Yes', 'a', 'a', 'a', 'NaN', 'a', 'a', '', 1, '0000-00-00', 0, '2016-12-28', 9, '0000-00-00', 0, 'a', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482909682.txt'),
(8, 7, 'b', 'Female', '2016-12-28', 'b', 'b', 'No', 'b', 'No', 'b', 'No', 'No', 'b', 'b', 'b', 'NaN', 'b', 'b', '', 1, '0000-00-00', 0, '2016-12-28', 9, '0000-00-00', 0, 'b', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482909715.txt'),
(9, 9, 'c', 'Male', '2016-12-28', 'c', 'c', 'Yes', 'c', 'Yes', 'c', 'Yes', 'Yes', 'c', 'c', 'c', 'NaN', 'c', 'c', '', 1, '0000-00-00', 0, '2016-12-28', 9, '0000-00-00', 0, 'c', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482911037.txt'),
(10, 10, 'D', 'Female', '2016-12-28', 'D', 'D', 'No', 'D', 'No', 'D', 'No', 'No', 'D', 'D', 'D', 'NaN', 'D', 'D', '', 1, '0000-00-00', 0, '2016-12-28', 9, '0000-00-00', 0, 'D', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482912206.txt'),
(11, 11, 'é˜¿é“å¤«', 'Male', '2016-12-14', 'é˜¿é“å¤«a', 'aåœ°æ–¹', 'Yes', 'å•Š', 'Yes', 'æ‰“åˆ†', 'Yes', 'Yes', 'é˜¿é“å¤«', '170', '56', '19.38', 'åœ°æ–¹', '1244', '', 1, '0000-00-00', 0, '2016-12-30', 9, '0000-00-00', 0, '1245', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1483077328.docx'),
(12, 12, '1', 'Female', '2016-12-30', '1', '1', 'Yes', '1', 'Yes', '1', 'Yes', 'Yes', '1', '1', '1', '10000.00', '1', '11', '', 1, '0000-00-00', 0, '2016-12-30', 9, '0000-00-00', 0, '1', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1483085973.jpg'),
(13, 13, 'å¼ ä¸‰', 'Male', '2016-12-30', '1', '1', 'Yes', '11', 'Yes', '1', 'Yes', 'Yes', '1', '1', '1', '10000.00', '1', '1', '', 1, '0000-00-00', 0, '2016-12-30', 9, '0000-00-00', 0, '1', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1483087072.jpg'),
(14, 14, 'æŽå››', 'Female', '2016-12-30', '11', '1', 'Yes', '1', 'Yes', '1', 'Yes', 'Yes', '1', '1', '1', '10000.00', '1', '11', '', 1, '0000-00-00', 0, '2016-12-30', 9, '0000-00-00', 0, '1', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1483087512.jpg'),
(15, 18, 'liming', 'Male', '2016-12-28', 'han', 'guangdong', 'Yes', 'O', 'Yes', 'dh', 'Yes', 'Yes', 'han', '180', '56', '17.28', 'ganbing', '1234566', '', 1, '0000-00-00', 0, '2017-01-10', 9, '0000-00-00', 0, '346776554', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1484014877.png'),
(16, 17, 'æŽä¸–æ©', 'Female', '1976-04-13', 'æ±‰', 'å¹¿ä¸œ', 'Yes', 'A', 'No', 'æ— ', 'No', 'No', 'æ— ', '162', '53', '20.20', 'æ— ', '13625291913', '', 1, '0000-00-00', 0, '2017-01-10', 9, '0000-00-00', 0, '441522197604130423', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1484044143.jpg'),
(17, 16, 'å¼ ä¸–è£', 'Male', '1965-04-12', 'æ±‰', 'å±±è¥¿', 'Yes', 'B', 'No', 'æ— ', 'Yes', 'Yes', 'æ— ', '175', '65', '21.22', 'æ— ', '13765281627', '', 1, '0000-00-00', 0, '2017-01-10', 9, '0000-00-00', 0, '441826196512042816', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1484044241.jpg'),
(18, 19, 'adf', 'Female', '2017-01-04', 'adf', 'adf', 'Yes', 'df', 'Yes', 'adf', 'Yes', 'Yes', 'adf', '23', '123', '2325.14', 'adf1', '2343431', '', 0, '0000-00-00', 0, '2017-01-18', 9, '0000-00-00', 0, '214343434', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1484731016.png'),
(19, 0, '\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 \0\0\0þÿÿÿþÿÿÿþÿÿÿ$\0\0\0%\0\0\0&\0\0\0''\0\0\0(\0\0\0)\0\0\0*\0\0\0+\0\0\0', '\0\0\0-\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(20, 0, '\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0¼\0\0\0\0†\0‹[SO1\0\0ð\0\0\0	\0\0\0\0\0†ÿ‹[SO1\0\0´\0\0\0ÿ\0\0\0\0†\0‹[SO\0\0\0\0"¥"\\ #', '##0;\\-', '0000-00-00', '##0;[Red]\\-"¥"\\ #', '##0"\0\0\0\0"¥"\\ #', '##0', '##0.00''\0\0"\0\0"¥"\\ ', '##0', '##0.00;\0*\06\0\0_-"¥"\\ * #', '##0', '##0', '\0)\0''\0\0_-* #', '##0_-;\\-* #', '##0_-;_-* "-"_-', '\0>\0\0_-"¥"\\ * #', '##0.00_-;\\-"¥"\\ * #', '##0.00_-;_-"¥"\\ * "-', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(21, 0, '\0\0\0õÿ \0\0´\0\0\0\0\0\0\0ª à\0\0\0\0\0õÿ \0\0Ô\0a\0\0±\0\0À à\0\0\0', '\0õÿ \0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(22, 0, '\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(23, 0, '\0\0\0\0\0ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0;\0\0\0\0\r\0\0\0\0\0???ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0???ÿ        \0\0\0\0\0???ÿ        	\0\0\0\0\0???ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(24, 0, '\0\0\0\0\0???ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\03\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0ÿ        \0\0\0\0\0ÿ        	\0\0\0\0\0ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(25, 0, '\0\0\0\0\0ÿrl€B2——}A\0}\0\0\0\0\0\0\0\0\0\0\0\07\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0ÿ€ÿ??_-;_-@}‘\0}\0\0\0\0\0\0\0\0\0\0\0\04\0\0\0\0\r\0\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0¥¥¥ÿ?', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(26, 0, '\0\0\0\0\0???ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\06\0\0\0\0\r\0\0\0\0\0ÿ\0\0ÿ.00\\)* "\0\0}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0=\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0ÿÿÌÿ??_-;_-@\0\0\0\0\0²²²ÿ ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(27, 0, '\0\0\0\0\0²²²ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\05\0\0\0\0\r\0\0\0\0\0ÿ.00\\)* "\0\0}U\0}\0\0\0\0\0\0\0\0\0\0\0\00\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0\0\0\0??_-;_-@\0\0\0\0\0\0\0\0 ', '\0\0\0\0\r', '0000-00-00', '\0\0*N''`r‚4\0’;\0’\0\0\0\0\0\0\0\0\0\0)ÿ\0*N''`r‚4\0\0\0\0\0\0\0\0ÿÀ\0ÿ\0\0\0\0\0ÿÿÿÿ%\0\0“\r\0-\0\0*N''`r‚5\0’;\0’\0\0\0\0\0\0\0\0\0\0-ÿ\0*N''`r‚5\0\0\0\0\0\0\0\0DrÄÿ\0\0\0\0\0ÿÿÿÿ', 'g’1\0’\0\0\0\0\0\0\0\0\0\05ÿ\0ã‰Ê‘''`‡e', 'g\0\0', 'g’/\0’\0\0\0\0\0\0\0\0\0\0', 'g\0\0', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(28, 0, 'ÿ\0èlÊ‘\0\0\0\0\0ÿ\0\0ÿÿÌÿ\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0	\0\0ÿ\0\0²²²ÿ\0ŽX\0Ž\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0T\0a\0b\0l\0e\0S\0t\0y\0l\0e\0M\0e\0d\0i\0u\0m\09\0P\0i\0v\0o\0t\0S\0t', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(29, 0, '\04dju$i-WÉ~~‚É~~‚ü\0C\0¬0ØY\0ë~t\0àoœtü\0\0@þ€øig\09p5‚Â”\0igÂ”ì—\0cm	\0cm~‚ 2ü\0Â”COÎ˜ \0€ø\0ì—$U‡t€øjuì—$U‡t€ø¶o˜"k\0ju‡tü\0juîoA%\0É', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(30, 0, '\0>þ', 'ƒæ0”mœ', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(31, 0, 'ÉúÆQWéÏÍKv¯Ub OXé#&½®¯¯ÊÍ1`R¢¦Též9<“l#¤Ü$©´>ŽÀr	`·Ð¡¹-Š;c=1g<yèº|Âv«çƒ<ŸHD®Õã©oŠª4„08', ' fªš³º', '0000-00-00', 'CâÏóHF‹ùeæ3Ñ¾mÅÆÛÝ(ëÈgãÅìO\0«ÿ‰þÎ4óßÖ_\0\0\0ÿÿ\0PK\0\0\0\0\0!\0¥Ö§çÀ\0\0\06\0\0\0\0\0_rels/.rels„ÏjÃ0‡ï…½ƒÑ}QÒÃ%v/¥C/£}\0á(h"ÛëÛOÇ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(32, 0, '»„¤ï÷©=þ®‹ùá”ç šªÃâC?Ëháv=¿‚É…¤§%[xp†£{Ûµ_¼PÑ£<Í1¥H¶0•ˆÙO¼R®BdÑÉÒJEÛ4b$§‘q_×˜žà6LÓõR×7`®¨Éÿ³Ã0ÌžOÁ¯', 'åEn7”', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(33, 0, 'Ã @á}¡wÙ7c»(Eb²Ë®»ö\0Cœ\ZAÇ ÒŸÛ×åãƒ7ÎßÕ›K\rY', 'œ\rŠeÍ', '0000-00-00', 'láÇæéxÉ´ßIÈsQ}#Õ…­µÝ Öµ+Õ!ï', 'Ý^¹$j=‹GWèÓ÷)âEë+&', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(34, 0, '8ý\0\0ÿÿ\0PK\0\0\0\0\0!\0w‹}–Â\0\0\Z\0\0\0\0\0theme/theme/theme1.xmlìYÏ‹ÛF¾ú?Ýÿ’d{‰7Ø²m³›„ØIÉql­ÉŽ4F\ZïÆ„@IŽ…BiZr)”^z(m	´Ðô¯Ù4%M!ÿBßŒdyÆw“%‡´d\r‹4úÞ›o', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(35, 0, 'SD<#\Z÷…o¬™Hìø°', 'É"ñil', '0000-00-00', '„µÉ(èÌñ0~3‹A€ˆfðmpÝå¼¼@Ô„kc=x7bðâü–ÆµÄsN3_', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(36, 0, 'B\rxÀm³Ø€Kb.%Âƒy45OÏUÜ5„ŽLsû(ÒRÛÏ@Y‰É¥`æUŠ"Ž¦8ÂÜ×Ø!Æ†ÕÝ$D‹ëÅ', 'anÝ$V', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(37, 0, 'ËÝK¶Sù´¼$ lß„„2™N¢j Q[Š Égsš„\\Ù[aÑ0°¨÷ËTm°\0jyV`ÿdÁ®«i»˜€<T!ŠÇ"Oiª—Ù•É|›™ÞL­`3±¬€U¦‚ëÖå‰Õ¥¥ö\Z™ÖH(å¦“‘‘=', '	ÐgÕ)', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(38, 0, 'ãUi~v°°dsHw?[C:¯!(1·V“^\0•ÓhŽ	¼ÑÌ…lUk)“]õ•¢¬¡tÑY€²Ž¢Šy', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(39, 0, '—RžÓ‘gy”³lÍP%$Y#NEƒUƒªuÓ¼k¤¶vÝÓDäÑ\\õLMUD×4«˜6Ã²\r¬ÅòlM^aµ1hšÚáSé^—ÜÆRëÖö	y—€€çñ3tÝ×h', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(40, 0, 'µÕd\Z5ÁxS†…fg£zïX.ðj¯Ó$Õ÷–n×â–÷ãt0x¦ÎvëUC“å¾RFZ~ûP¿N°á-¼žSžÈTÂ·‡Á†¨/÷$©lÀ-r›g·Yó˜4í;%·åø×/”ên·àTR¡î¶ª…–ëVË]·\\ê´+w¡±ð ', '»éw—¼', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(41, 0, 'Â0„÷‚wooÓº‘&ÝˆÐ­Ô„ä5\r6?$Qìí\r', '.‡a¾™', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(42, 0, '\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(43, 0, '\0\0\0	\0\0\0gfÌ\0€\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ú:\0\0œ<\0\0\r\0\0\0\0\0d\0\0\0\0\0\0\0\0\0\0ü©ñÒMbP?_\0\0\0*\0\0\0\0+\0\0\0\0‚\0\0\0€\0\0\0\0\0\0\0\0\0\0%\0\0\0', '\0\0Á', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(44, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '\0\0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(45, 0, '\0\0\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(46, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(47, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(48, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(49, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(50, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(51, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(52, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(53, 0, '\0\0\0	\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(54, 0, '\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(55, 0, '\0\0	\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(56, 0, '\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(57, 0, '\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(58, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(59, 0, '\0\0\0\r\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(60, 0, '\0\0\0\0\0\r\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(61, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(62, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(63, 0, '\0\0\0\0\0\0\0\0½\0*\0\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0½\0B\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0×\0\0Š\0\0\0î\0>\0¶\0\0\0\0@', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(64, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(65, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ÕÍÕœ.“—\0+', 'ù®0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(66, 0, '\0\0ÿÿÿÿ \0\0\0\0\0À\0\0\0\0\0\0F!\0\0\0Microsoft Excel 97 - 2004 ¹¤×÷±í\0\0\0\0Biff8\0\0\0\0Excel.Sheet.8\0ô9²q\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(67, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0à…ŸòùOh«‘\0+''³Ù0\0\0\0(–\0\0\0\0\0\0\0\0H\0\0\0\0\0\0P\0\0\0\0\0\0p\0\0\0\0\0\0\0\0\0\0\0\0´\0\0\0\r\0\0\0À\0\0\0\0\0\0Ì\0\0\0\0\0\0Ô\0\0\0\0\0\0''\0\0\0\0\0\0\0\0Microsoft Office ÓÃ»', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(68, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0€\0\0\0J\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0Q\0\0\0x”\0\0\0\0\0\0\0\0\0\0\0\0\0I\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0P\0\0\0(\0\0\0x\0\0\0\0”\0\0\0\0\0\0 \0Ì\0€\0\0\0J\0\0\0(\0\0\0€\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(69, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(70, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(71, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(72, 0, '˜Ÿÿ‘‘‘ÿ·˜‰ÿÿÿîÿ‰¥ÒÿÍÒ±ÿ˜p‘ÿÿÿêÿ·Èàÿ½Â·ÿ±«±ÿÍ½·ÿ±½Ðÿ«¥«ÿÿò×ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿçòõÿÒÍ×ÿòîåÿåîòÿÅÍÒÿ±¦¦ÿ‰y‘ÿ«˜˜ÿÒ×ÒÿûîÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(73, 0, '\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 \0\0\0þÿÿÿþÿÿÿþÿÿÿ$\0\0\0%\0\0\0&\0\0\0''\0\0\0(\0\0\0)\0\0\0*\0\0\0+\0\0\0', '\0\0\0-\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(74, 0, '\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0¼\0\0\0\0†\0‹[SO1\0\0ð\0\0\0	\0\0\0\0\0†ÿ‹[SO1\0\0´\0\0\0ÿ\0\0\0\0†\0‹[SO\0\0\0\0"¥"\\ #', '##0;\\-', '0000-00-00', '##0;[Red]\\-"¥"\\ #', '##0"\0\0\0\0"¥"\\ #', '##0', '##0.00''\0\0"\0\0"¥"\\ ', '##0', '##0.00;\0*\06\0\0_-"¥"\\ * #', '##0', '##0', '\0)\0''\0\0_-* #', '##0_-;\\-* #', '##0_-;_-* "-"_-', '\0>\0\0_-"¥"\\ * #', '##0.00_-;\\-"¥"\\ * #', '##0.00_-;_-"¥"\\ * "-', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(75, 0, '\0\0\0õÿ \0\0´\0\0\0\0\0\0\0ª à\0\0\0\0\0õÿ \0\0Ô\0a\0\0±\0\0À à\0\0\0', '\0õÿ \0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(76, 0, '\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(77, 0, '\0\0\0\0\0ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0;\0\0\0\0\r\0\0\0\0\0???ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0???ÿ        \0\0\0\0\0???ÿ        	\0\0\0\0\0???ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(78, 0, '\0\0\0\0\0???ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\03\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0ÿ        \0\0\0\0\0ÿ        	\0\0\0\0\0ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(79, 0, '\0\0\0\0\0ÿrl€B2——}A\0}\0\0\0\0\0\0\0\0\0\0\0\07\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0ÿ€ÿ??_-;_-@}‘\0}\0\0\0\0\0\0\0\0\0\0\0\04\0\0\0\0\r\0\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0¥¥¥ÿ?', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(80, 0, '\0\0\0\0\0???ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\06\0\0\0\0\r\0\0\0\0\0ÿ\0\0ÿ.00\\)* "\0\0}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0=\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0ÿÿÌÿ??_-;_-@\0\0\0\0\0²²²ÿ ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(81, 0, '\0\0\0\0\0²²²ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\05\0\0\0\0\r\0\0\0\0\0ÿ.00\\)* "\0\0}U\0}\0\0\0\0\0\0\0\0\0\0\0\00\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0\0\0\0??_-;_-@\0\0\0\0\0\0\0\0 ', '\0\0\0\0\r', '0000-00-00', '\0\0*N''`r‚4\0’;\0’\0\0\0\0\0\0\0\0\0\0)ÿ\0*N''`r‚4\0\0\0\0\0\0\0\0ÿÀ\0ÿ\0\0\0\0\0ÿÿÿÿ%\0\0“\r\0-\0\0*N''`r‚5\0’;\0’\0\0\0\0\0\0\0\0\0\0-ÿ\0*N''`r‚5\0\0\0\0\0\0\0\0DrÄÿ\0\0\0\0\0ÿÿÿÿ', 'g’1\0’\0\0\0\0\0\0\0\0\0\05ÿ\0ã‰Ê‘''`‡e', 'g\0\0', 'g’/\0’\0\0\0\0\0\0\0\0\0\0', 'g\0\0', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(82, 0, 'ÿ\0èlÊ‘\0\0\0\0\0ÿ\0\0ÿÿÌÿ\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0	\0\0ÿ\0\0²²²ÿ\0ŽX\0Ž\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0T\0a\0b\0l\0e\0S\0t\0y\0l\0e\0M\0e\0d\0i\0u\0m\09\0P\0i\0v\0o\0t\0S\0t', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(83, 0, '\04dju$i-WÉ~~‚É~~‚ü\0C\0¬0ØY\0ë~t\0àoœtü\0\0@þ€øig\09p5‚Â”\0igÂ”ì—\0cm	\0cm~‚ 2ü\0Â”COÎ˜ \0€ø\0ì—$U‡t€øjuì—$U‡t€ø¶o˜"k\0ju‡tü\0juîoA%\0É', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(84, 0, '\0>þ', 'ƒæ0”mœ', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(85, 0, 'ÉúÆQWéÏÍKv¯Ub OXé#&½®¯¯ÊÍ1`R¢¦Též9<“l#¤Ü$©´>ŽÀr	`·Ð¡¹-Š;c=1g<yèº|Âv«çƒ<ŸHD®Õã©oŠª4„08', ' fªš³º', '0000-00-00', 'CâÏóHF‹ùeæ3Ñ¾mÅÆÛÝ(ëÈgãÅìO\0«ÿ‰þÎ4óßÖ_\0\0\0ÿÿ\0PK\0\0\0\0\0!\0¥Ö§çÀ\0\0\06\0\0\0\0\0_rels/.rels„ÏjÃ0‡ï…½ƒÑ}QÒÃ%v/¥C/£}\0á(h"ÛëÛOÇ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(86, 0, '»„¤ï÷©=þ®‹ùá”ç šªÃâC?Ëháv=¿‚É…¤§%[xp†£{Ûµ_¼PÑ£<Í1¥H¶0•ˆÙO¼R®BdÑÉÒJEÛ4b$§‘q_×˜žà6LÓõR×7`®¨Éÿ³Ã0ÌžOÁ¯', 'åEn7”', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(87, 0, 'Ã @á}¡wÙ7c»(Eb²Ë®»ö\0Cœ\ZAÇ ÒŸÛ×åãƒ7ÎßÕ›K\rY', 'œ\rŠeÍ', '0000-00-00', 'láÇæéxÉ´ßIÈsQ}#Õ…­µÝ Öµ+Õ!ï', 'Ý^¹$j=‹GWèÓ÷)âEë+&', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(88, 0, '8ý\0\0ÿÿ\0PK\0\0\0\0\0!\0w‹}–Â\0\0\Z\0\0\0\0\0theme/theme/theme1.xmlìYÏ‹ÛF¾ú?Ýÿ’d{‰7Ø²m³›„ØIÉql­ÉŽ4F\ZïÆ„@IŽ…BiZr)”^z(m	´Ðô¯Ù4%M!ÿBßŒdyÆw“%‡´d\r‹4úÞ›o', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(89, 0, 'SD<#\Z÷…o¬™Hìø°', 'É"ñil', '0000-00-00', '„µÉ(èÌñ0~3‹A€ˆfðmpÝå¼¼@Ô„kc=x7bðâü–ÆµÄsN3_', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(90, 0, 'B\rxÀm³Ø€Kb.%Âƒy45OÏUÜ5„ŽLsû(ÒRÛÏ@Y‰É¥`æUŠ"Ž¦8ÂÜ×Ø!Æ†ÕÝ$D‹ëÅ', 'anÝ$V', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(91, 0, 'ËÝK¶Sù´¼$ lß„„2™N¢j Q[Š Égsš„\\Ù[aÑ0°¨÷ËTm°\0jyV`ÿdÁ®«i»˜€<T!ŠÇ"Oiª—Ù•É|›™ÞL­`3±¬€U¦‚ëÖå‰Õ¥¥ö\Z™ÖH(å¦“‘‘=', '	ÐgÕ)', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(92, 0, 'ãUi~v°°dsHw?[C:¯!(1·V“^\0•ÓhŽ	¼ÑÌ…lUk)“]õ•¢¬¡tÑY€²Ž¢Šy', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(93, 0, '—RžÓ‘gy”³lÍP%$Y#NEƒUƒªuÓ¼k¤¶vÝÓDäÑ\\õLMUD×4«˜6Ã²\r¬ÅòlM^aµ1hšÚáSé^—ÜÆRëÖö	y—€€çñ3tÝ×h', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(94, 0, 'µÕd\Z5ÁxS†…fg£zïX.ðj¯Ó$Õ÷–n×â–÷ãt0x¦ÎvëUC“å¾RFZ~ûP¿N°á-¼žSžÈTÂ·‡Á†¨/÷$©lÀ-r›g·Yó˜4í;%·åø×/”ên·àTR¡î¶ª…–ëVË]·\\ê´+w¡±ð ', '»éw—¼', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(95, 0, 'Â0„÷‚wooÓº‘&ÝˆÐ­Ô„ä5\r6?$Qìí\r', '.‡a¾™', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(96, 0, '\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(97, 0, '\0\0\0	\0\0\0gfÌ\0€\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ú:\0\0œ<\0\0\r\0\0\0\0\0d\0\0\0\0\0\0\0\0\0\0ü©ñÒMbP?_\0\0\0*\0\0\0\0+\0\0\0\0‚\0\0\0€\0\0\0\0\0\0\0\0\0\0%\0\0\0', '\0\0Á', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(98, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '\0\0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(99, 0, '\0\0\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(100, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(101, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(102, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(103, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(104, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(105, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(106, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(107, 0, '\0\0\0	\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(108, 0, '\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(109, 0, '\0\0	\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(110, 0, '\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(111, 0, '\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(112, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(113, 0, '\0\0\0\r\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(114, 0, '\0\0\0\0\0\r\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(115, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(116, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(117, 0, '\0\0\0\0\0\0\0\0½\0*\0\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0½\0B\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0×\0\0Š\0\0\0î\0>\0¶\0\0\0\0@', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(118, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(119, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ÕÍÕœ.“—\0+', 'ù®0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(120, 0, '\0\0ÿÿÿÿ \0\0\0\0\0À\0\0\0\0\0\0F!\0\0\0Microsoft Excel 97 - 2004 ¹¤×÷±í\0\0\0\0Biff8\0\0\0\0Excel.Sheet.8\0ô9²q\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(121, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0à…ŸòùOh«‘\0+''³Ù0\0\0\0(–\0\0\0\0\0\0\0\0H\0\0\0\0\0\0P\0\0\0\0\0\0p\0\0\0\0\0\0\0\0\0\0\0\0´\0\0\0\r\0\0\0À\0\0\0\0\0\0Ì\0\0\0\0\0\0Ô\0\0\0\0\0\0''\0\0\0\0\0\0\0\0Microsoft Office ÓÃ»', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(122, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0€\0\0\0J\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0Q\0\0\0x”\0\0\0\0\0\0\0\0\0\0\0\0\0I\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0P\0\0\0(\0\0\0x\0\0\0\0”\0\0\0\0\0\0 \0Ì\0€\0\0\0J\0\0\0(\0\0\0€\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(123, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(124, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(125, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(126, 0, '˜Ÿÿ‘‘‘ÿ·˜‰ÿÿÿîÿ‰¥ÒÿÍÒ±ÿ˜p‘ÿÿÿêÿ·Èàÿ½Â·ÿ±«±ÿÍ½·ÿ±½Ðÿ«¥«ÿÿò×ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿçòõÿÒÍ×ÿòîåÿåîòÿÅÍÒÿ±¦¦ÿ‰y‘ÿ«˜˜ÿÒ×ÒÿûîÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(127, 0, '\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 \0\0\0þÿÿÿþÿÿÿþÿÿÿ$\0\0\0%\0\0\0&\0\0\0''\0\0\0(\0\0\0)\0\0\0*\0\0\0+\0\0\0', '\0\0\0-\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(128, 0, '\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0¼\0\0\0\0†\0‹[SO1\0\0ð\0\0\0	\0\0\0\0\0†ÿ‹[SO1\0\0´\0\0\0ÿ\0\0\0\0†\0‹[SO\0\0\0\0"¥"\\ #', '##0;\\-', '0000-00-00', '##0;[Red]\\-"¥"\\ #', '##0"\0\0\0\0"¥"\\ #', '##0', '##0.00''\0\0"\0\0"¥"\\ ', '##0', '##0.00;\0*\06\0\0_-"¥"\\ * #', '##0', '##0', '\0)\0''\0\0_-* #', '##0_-;\\-* #', '##0_-;_-* "-"_-', '\0>\0\0_-"¥"\\ * #', '##0.00_-;\\-"¥"\\ * #', '##0.00_-;_-"¥"\\ * "-', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(129, 0, '\0\0\0õÿ \0\0´\0\0\0\0\0\0\0ª à\0\0\0\0\0õÿ \0\0Ô\0a\0\0±\0\0À à\0\0\0', '\0õÿ \0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(130, 0, '\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(131, 0, '\0\0\0\0\0ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0;\0\0\0\0\r\0\0\0\0\0???ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0???ÿ        \0\0\0\0\0???ÿ        	\0\0\0\0\0???ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(132, 0, '\0\0\0\0\0???ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\03\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0ÿ        \0\0\0\0\0ÿ        	\0\0\0\0\0ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(133, 0, '\0\0\0\0\0ÿrl€B2——}A\0}\0\0\0\0\0\0\0\0\0\0\0\07\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0ÿ€ÿ??_-;_-@}‘\0}\0\0\0\0\0\0\0\0\0\0\0\04\0\0\0\0\r\0\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0¥¥¥ÿ?', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(134, 0, '\0\0\0\0\0???ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\06\0\0\0\0\r\0\0\0\0\0ÿ\0\0ÿ.00\\)* "\0\0}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0=\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0ÿÿÌÿ??_-;_-@\0\0\0\0\0²²²ÿ ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(135, 0, '\0\0\0\0\0²²²ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\05\0\0\0\0\r\0\0\0\0\0ÿ.00\\)* "\0\0}U\0}\0\0\0\0\0\0\0\0\0\0\0\00\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0\0\0\0??_-;_-@\0\0\0\0\0\0\0\0 ', '\0\0\0\0\r', '0000-00-00', '\0\0*N''`r‚4\0’;\0’\0\0\0\0\0\0\0\0\0\0)ÿ\0*N''`r‚4\0\0\0\0\0\0\0\0ÿÀ\0ÿ\0\0\0\0\0ÿÿÿÿ%\0\0“\r\0-\0\0*N''`r‚5\0’;\0’\0\0\0\0\0\0\0\0\0\0-ÿ\0*N''`r‚5\0\0\0\0\0\0\0\0DrÄÿ\0\0\0\0\0ÿÿÿÿ', 'g’1\0’\0\0\0\0\0\0\0\0\0\05ÿ\0ã‰Ê‘''`‡e', 'g\0\0', 'g’/\0’\0\0\0\0\0\0\0\0\0\0', 'g\0\0', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(136, 0, 'ÿ\0èlÊ‘\0\0\0\0\0ÿ\0\0ÿÿÌÿ\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0	\0\0ÿ\0\0²²²ÿ\0ŽX\0Ž\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0T\0a\0b\0l\0e\0S\0t\0y\0l\0e\0M\0e\0d\0i\0u\0m\09\0P\0i\0v\0o\0t\0S\0t', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(137, 0, '\04dju$i-WÉ~~‚É~~‚ü\0C\0¬0ØY\0ë~t\0àoœtü\0\0@þ€øig\09p5‚Â”\0igÂ”ì—\0cm	\0cm~‚ 2ü\0Â”COÎ˜ \0€ø\0ì—$U‡t€øjuì—$U‡t€ø¶o˜"k\0ju‡tü\0juîoA%\0É', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(138, 0, '\0>þ', 'ƒæ0”mœ', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(139, 0, 'ÉúÆQWéÏÍKv¯Ub OXé#&½®¯¯ÊÍ1`R¢¦Též9<“l#¤Ü$©´>ŽÀr	`·Ð¡¹-Š;c=1g<yèº|Âv«çƒ<ŸHD®Õã©oŠª4„08', ' fªš³º', '0000-00-00', 'CâÏóHF‹ùeæ3Ñ¾mÅÆÛÝ(ëÈgãÅìO\0«ÿ‰þÎ4óßÖ_\0\0\0ÿÿ\0PK\0\0\0\0\0!\0¥Ö§çÀ\0\0\06\0\0\0\0\0_rels/.rels„ÏjÃ0‡ï…½ƒÑ}QÒÃ%v/¥C/£}\0á(h"ÛëÛOÇ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(140, 0, '»„¤ï÷©=þ®‹ùá”ç šªÃâC?Ëháv=¿‚É…¤§%[xp†£{Ûµ_¼PÑ£<Í1¥H¶0•ˆÙO¼R®BdÑÉÒJEÛ4b$§‘q_×˜žà6LÓõR×7`®¨Éÿ³Ã0ÌžOÁ¯', 'åEn7”', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(141, 0, 'Ã @á}¡wÙ7c»(Eb²Ë®»ö\0Cœ\ZAÇ ÒŸÛ×åãƒ7ÎßÕ›K\rY', 'œ\rŠeÍ', '0000-00-00', 'láÇæéxÉ´ßIÈsQ}#Õ…­µÝ Öµ+Õ!ï', 'Ý^¹$j=‹GWèÓ÷)âEë+&', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(142, 0, '8ý\0\0ÿÿ\0PK\0\0\0\0\0!\0w‹}–Â\0\0\Z\0\0\0\0\0theme/theme/theme1.xmlìYÏ‹ÛF¾ú?Ýÿ’d{‰7Ø²m³›„ØIÉql­ÉŽ4F\ZïÆ„@IŽ…BiZr)”^z(m	´Ðô¯Ù4%M!ÿBßŒdyÆw“%‡´d\r‹4úÞ›o', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(143, 0, 'SD<#\Z÷…o¬™Hìø°', 'É"ñil', '0000-00-00', '„µÉ(èÌñ0~3‹A€ˆfðmpÝå¼¼@Ô„kc=x7bðâü–ÆµÄsN3_', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(144, 0, 'B\rxÀm³Ø€Kb.%Âƒy45OÏUÜ5„ŽLsû(ÒRÛÏ@Y‰É¥`æUŠ"Ž¦8ÂÜ×Ø!Æ†ÕÝ$D‹ëÅ', 'anÝ$V', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(145, 0, 'ËÝK¶Sù´¼$ lß„„2™N¢j Q[Š Égsš„\\Ù[aÑ0°¨÷ËTm°\0jyV`ÿdÁ®«i»˜€<T!ŠÇ"Oiª—Ù•É|›™ÞL­`3±¬€U¦‚ëÖå‰Õ¥¥ö\Z™ÖH(å¦“‘‘=', '	ÐgÕ)', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(146, 0, 'ãUi~v°°dsHw?[C:¯!(1·V“^\0•ÓhŽ	¼ÑÌ…lUk)“]õ•¢¬¡tÑY€²Ž¢Šy', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(147, 0, '—RžÓ‘gy”³lÍP%$Y#NEƒUƒªuÓ¼k¤¶vÝÓDäÑ\\õLMUD×4«˜6Ã²\r¬ÅòlM^aµ1hšÚáSé^—ÜÆRëÖö	y—€€çñ3tÝ×h', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(148, 0, 'µÕd\Z5ÁxS†…fg£zïX.ðj¯Ó$Õ÷–n×â–÷ãt0x¦ÎvëUC“å¾RFZ~ûP¿N°á-¼žSžÈTÂ·‡Á†¨/÷$©lÀ-r›g·Yó˜4í;%·åø×/”ên·àTR¡î¶ª…–ëVË]·\\ê´+w¡±ð ', '»éw—¼', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(149, 0, 'Â0„÷‚wooÓº‘&ÝˆÐ­Ô„ä5\r6?$Qìí\r', '.‡a¾™', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(150, 0, '\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(151, 0, '\0\0\0	\0\0\0gfÌ\0€\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ú:\0\0œ<\0\0\r\0\0\0\0\0d\0\0\0\0\0\0\0\0\0\0ü©ñÒMbP?_\0\0\0*\0\0\0\0+\0\0\0\0‚\0\0\0€\0\0\0\0\0\0\0\0\0\0%\0\0\0', '\0\0Á', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(152, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '\0\0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(153, 0, '\0\0\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(154, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(155, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(156, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(157, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(158, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(159, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(160, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(161, 0, '\0\0\0	\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(162, 0, '\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(163, 0, '\0\0	\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(164, 0, '\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(165, 0, '\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(166, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(167, 0, '\0\0\0\r\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(168, 0, '\0\0\0\0\0\r\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(169, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(170, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(171, 0, '\0\0\0\0\0\0\0\0½\0*\0\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0½\0B\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0×\0\0Š\0\0\0î\0>\0¶\0\0\0\0@', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(172, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(173, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ÕÍÕœ.“—\0+', 'ù®0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(174, 0, '\0\0ÿÿÿÿ \0\0\0\0\0À\0\0\0\0\0\0F!\0\0\0Microsoft Excel 97 - 2004 ¹¤×÷±í\0\0\0\0Biff8\0\0\0\0Excel.Sheet.8\0ô9²q\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(175, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0à…ŸòùOh«‘\0+''³Ù0\0\0\0(–\0\0\0\0\0\0\0\0H\0\0\0\0\0\0P\0\0\0\0\0\0p\0\0\0\0\0\0\0\0\0\0\0\0´\0\0\0\r\0\0\0À\0\0\0\0\0\0Ì\0\0\0\0\0\0Ô\0\0\0\0\0\0''\0\0\0\0\0\0\0\0Microsoft Office ÓÃ»', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(176, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0€\0\0\0J\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0Q\0\0\0x”\0\0\0\0\0\0\0\0\0\0\0\0\0I\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0P\0\0\0(\0\0\0x\0\0\0\0”\0\0\0\0\0\0 \0Ì\0€\0\0\0J\0\0\0(\0\0\0€\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(177, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(178, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(179, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(180, 0, '˜Ÿÿ‘‘‘ÿ·˜‰ÿÿÿîÿ‰¥ÒÿÍÒ±ÿ˜p‘ÿÿÿêÿ·Èàÿ½Â·ÿ±«±ÿÍ½·ÿ±½Ðÿ«¥«ÿÿò×ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿçòõÿÒÍ×ÿòîåÿåîòÿÅÍÒÿ±¦¦ÿ‰y‘ÿ«˜˜ÿÒ×ÒÿûîÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(181, 0, '\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 \0\0\0þÿÿÿþÿÿÿþÿÿÿ$\0\0\0%\0\0\0&\0\0\0''\0\0\0(\0\0\0)\0\0\0*\0\0\0+\0\0\0', '\0\0\0-\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', '');
INSERT INTO `volunteers` (`volunteer_id`, `subject_id`, `v_name`, `gender`, `bday`, `nation`, `native`, `married`, `blood_type`, `surgery`, `fam_history`, `smoker`, `drinker`, `allergies`, `height`, `weight`, `bmi`, `med_history`, `mobile`, `remarks`, `status`, `date_status`, `status_updated_by`, `date_added`, `added_by`, `date_update`, `updated_by`, `id_card_num`, `id_card_path`) VALUES
(182, 0, '\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0¼\0\0\0\0†\0‹[SO1\0\0ð\0\0\0	\0\0\0\0\0†ÿ‹[SO1\0\0´\0\0\0ÿ\0\0\0\0†\0‹[SO\0\0\0\0"¥"\\ #', '##0;\\-', '0000-00-00', '##0;[Red]\\-"¥"\\ #', '##0"\0\0\0\0"¥"\\ #', '##0', '##0.00''\0\0"\0\0"¥"\\ ', '##0', '##0.00;\0*\06\0\0_-"¥"\\ * #', '##0', '##0', '\0)\0''\0\0_-* #', '##0_-;\\-* #', '##0_-;_-* "-"_-', '\0>\0\0_-"¥"\\ * #', '##0.00_-;\\-"¥"\\ * #', '##0.00_-;_-"¥"\\ * "-', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(183, 0, '\0\0\0õÿ \0\0´\0\0\0\0\0\0\0ª à\0\0\0\0\0õÿ \0\0Ô\0a\0\0±\0\0À à\0\0\0', '\0õÿ \0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(184, 0, '\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(185, 0, '\0\0\0\0\0ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0;\0\0\0\0\r\0\0\0\0\0???ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0???ÿ        \0\0\0\0\0???ÿ        	\0\0\0\0\0???ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(186, 0, '\0\0\0\0\0???ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\03\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0ÿ        \0\0\0\0\0ÿ        	\0\0\0\0\0ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(187, 0, '\0\0\0\0\0ÿrl€B2——}A\0}\0\0\0\0\0\0\0\0\0\0\0\07\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0ÿ€ÿ??_-;_-@}‘\0}\0\0\0\0\0\0\0\0\0\0\0\04\0\0\0\0\r\0\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0¥¥¥ÿ?', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(188, 0, '\0\0\0\0\0???ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\06\0\0\0\0\r\0\0\0\0\0ÿ\0\0ÿ.00\\)* "\0\0}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0=\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0ÿÿÌÿ??_-;_-@\0\0\0\0\0²²²ÿ ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(189, 0, '\0\0\0\0\0²²²ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\05\0\0\0\0\r\0\0\0\0\0ÿ.00\\)* "\0\0}U\0}\0\0\0\0\0\0\0\0\0\0\0\00\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0\0\0\0??_-;_-@\0\0\0\0\0\0\0\0 ', '\0\0\0\0\r', '0000-00-00', '\0\0*N''`r‚4\0’;\0’\0\0\0\0\0\0\0\0\0\0)ÿ\0*N''`r‚4\0\0\0\0\0\0\0\0ÿÀ\0ÿ\0\0\0\0\0ÿÿÿÿ%\0\0“\r\0-\0\0*N''`r‚5\0’;\0’\0\0\0\0\0\0\0\0\0\0-ÿ\0*N''`r‚5\0\0\0\0\0\0\0\0DrÄÿ\0\0\0\0\0ÿÿÿÿ', 'g’1\0’\0\0\0\0\0\0\0\0\0\05ÿ\0ã‰Ê‘''`‡e', 'g\0\0', 'g’/\0’\0\0\0\0\0\0\0\0\0\0', 'g\0\0', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(190, 0, 'ÿ\0èlÊ‘\0\0\0\0\0ÿ\0\0ÿÿÌÿ\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0	\0\0ÿ\0\0²²²ÿ\0ŽX\0Ž\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0T\0a\0b\0l\0e\0S\0t\0y\0l\0e\0M\0e\0d\0i\0u\0m\09\0P\0i\0v\0o\0t\0S\0t', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(191, 0, '\04dju$i-WÉ~~‚É~~‚ü\0C\0¬0ØY\0ë~t\0àoœtü\0\0@þ€øig\09p5‚Â”\0igÂ”ì—\0cm	\0cm~‚ 2ü\0Â”COÎ˜ \0€ø\0ì—$U‡t€øjuì—$U‡t€ø¶o˜"k\0ju‡tü\0juîoA%\0É', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(192, 0, '\0>þ', 'ƒæ0”mœ', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(193, 0, 'ÉúÆQWéÏÍKv¯Ub OXé#&½®¯¯ÊÍ1`R¢¦Též9<“l#¤Ü$©´>ŽÀr	`·Ð¡¹-Š;c=1g<yèº|Âv«çƒ<ŸHD®Õã©oŠª4„08', ' fªš³º', '0000-00-00', 'CâÏóHF‹ùeæ3Ñ¾mÅÆÛÝ(ëÈgãÅìO\0«ÿ‰þÎ4óßÖ_\0\0\0ÿÿ\0PK\0\0\0\0\0!\0¥Ö§çÀ\0\0\06\0\0\0\0\0_rels/.rels„ÏjÃ0‡ï…½ƒÑ}QÒÃ%v/¥C/£}\0á(h"ÛëÛOÇ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(194, 0, '»„¤ï÷©=þ®‹ùá”ç šªÃâC?Ëháv=¿‚É…¤§%[xp†£{Ûµ_¼PÑ£<Í1¥H¶0•ˆÙO¼R®BdÑÉÒJEÛ4b$§‘q_×˜žà6LÓõR×7`®¨Éÿ³Ã0ÌžOÁ¯', 'åEn7”', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(195, 0, 'Ã @á}¡wÙ7c»(Eb²Ë®»ö\0Cœ\ZAÇ ÒŸÛ×åãƒ7ÎßÕ›K\rY', 'œ\rŠeÍ', '0000-00-00', 'láÇæéxÉ´ßIÈsQ}#Õ…­µÝ Öµ+Õ!ï', 'Ý^¹$j=‹GWèÓ÷)âEë+&', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(196, 0, '8ý\0\0ÿÿ\0PK\0\0\0\0\0!\0w‹}–Â\0\0\Z\0\0\0\0\0theme/theme/theme1.xmlìYÏ‹ÛF¾ú?Ýÿ’d{‰7Ø²m³›„ØIÉql­ÉŽ4F\ZïÆ„@IŽ…BiZr)”^z(m	´Ðô¯Ù4%M!ÿBßŒdyÆw“%‡´d\r‹4úÞ›o', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(197, 0, 'SD<#\Z÷…o¬™Hìø°', 'É"ñil', '0000-00-00', '„µÉ(èÌñ0~3‹A€ˆfðmpÝå¼¼@Ô„kc=x7bðâü–ÆµÄsN3_', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(198, 0, 'B\rxÀm³Ø€Kb.%Âƒy45OÏUÜ5„ŽLsû(ÒRÛÏ@Y‰É¥`æUŠ"Ž¦8ÂÜ×Ø!Æ†ÕÝ$D‹ëÅ', 'anÝ$V', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(199, 0, 'ËÝK¶Sù´¼$ lß„„2™N¢j Q[Š Égsš„\\Ù[aÑ0°¨÷ËTm°\0jyV`ÿdÁ®«i»˜€<T!ŠÇ"Oiª—Ù•É|›™ÞL­`3±¬€U¦‚ëÖå‰Õ¥¥ö\Z™ÖH(å¦“‘‘=', '	ÐgÕ)', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(200, 0, 'ãUi~v°°dsHw?[C:¯!(1·V“^\0•ÓhŽ	¼ÑÌ…lUk)“]õ•¢¬¡tÑY€²Ž¢Šy', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(201, 0, '—RžÓ‘gy”³lÍP%$Y#NEƒUƒªuÓ¼k¤¶vÝÓDäÑ\\õLMUD×4«˜6Ã²\r¬ÅòlM^aµ1hšÚáSé^—ÜÆRëÖö	y—€€çñ3tÝ×h', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(202, 0, 'µÕd\Z5ÁxS†…fg£zïX.ðj¯Ó$Õ÷–n×â–÷ãt0x¦ÎvëUC“å¾RFZ~ûP¿N°á-¼žSžÈTÂ·‡Á†¨/÷$©lÀ-r›g·Yó˜4í;%·åø×/”ên·àTR¡î¶ª…–ëVË]·\\ê´+w¡±ð ', '»éw—¼', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(203, 0, 'Â0„÷‚wooÓº‘&ÝˆÐ­Ô„ä5\r6?$Qìí\r', '.‡a¾™', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(204, 0, '\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(205, 0, '\0\0\0	\0\0\0gfÌ\0€\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ú:\0\0œ<\0\0\r\0\0\0\0\0d\0\0\0\0\0\0\0\0\0\0ü©ñÒMbP?_\0\0\0*\0\0\0\0+\0\0\0\0‚\0\0\0€\0\0\0\0\0\0\0\0\0\0%\0\0\0', '\0\0Á', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(206, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '\0\0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(207, 0, '\0\0\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(208, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(209, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(210, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(211, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(212, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(213, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(214, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(215, 0, '\0\0\0	\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(216, 0, '\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(217, 0, '\0\0	\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(218, 0, '\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(219, 0, '\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(220, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(221, 0, '\0\0\0\r\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(222, 0, '\0\0\0\0\0\r\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(223, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(224, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(225, 0, '\0\0\0\0\0\0\0\0½\0*\0\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0½\0B\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0×\0\0Š\0\0\0î\0>\0¶\0\0\0\0@', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(226, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(227, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ÕÍÕœ.“—\0+', 'ù®0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(228, 0, '\0\0ÿÿÿÿ \0\0\0\0\0À\0\0\0\0\0\0F!\0\0\0Microsoft Excel 97 - 2004 ¹¤×÷±í\0\0\0\0Biff8\0\0\0\0Excel.Sheet.8\0ô9²q\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(229, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0à…ŸòùOh«‘\0+''³Ù0\0\0\0(–\0\0\0\0\0\0\0\0H\0\0\0\0\0\0P\0\0\0\0\0\0p\0\0\0\0\0\0\0\0\0\0\0\0´\0\0\0\r\0\0\0À\0\0\0\0\0\0Ì\0\0\0\0\0\0Ô\0\0\0\0\0\0''\0\0\0\0\0\0\0\0Microsoft Office ÓÃ»', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(230, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0€\0\0\0J\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0Q\0\0\0x”\0\0\0\0\0\0\0\0\0\0\0\0\0I\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0P\0\0\0(\0\0\0x\0\0\0\0”\0\0\0\0\0\0 \0Ì\0€\0\0\0J\0\0\0(\0\0\0€\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(231, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(232, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(233, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(234, 0, '˜Ÿÿ‘‘‘ÿ·˜‰ÿÿÿîÿ‰¥ÒÿÍÒ±ÿ˜p‘ÿÿÿêÿ·Èàÿ½Â·ÿ±«±ÿÍ½·ÿ±½Ðÿ«¥«ÿÿò×ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿçòõÿÒÍ×ÿòîåÿåîòÿÅÍÒÿ±¦¦ÿ‰y‘ÿ«˜˜ÿÒ×ÒÿûîÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(235, 0, '\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 \0\0\0þÿÿÿþÿÿÿþÿÿÿ$\0\0\0%\0\0\0&\0\0\0''\0\0\0(\0\0\0)\0\0\0*\0\0\0+\0\0\0', '\0\0\0-\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(236, 0, '\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0¼\0\0\0\0†\0‹[SO1\0\0ð\0\0\0	\0\0\0\0\0†ÿ‹[SO1\0\0´\0\0\0ÿ\0\0\0\0†\0‹[SO\0\0\0\0"¥"\\ #', '##0;\\-', '0000-00-00', '##0;[Red]\\-"¥"\\ #', '##0"\0\0\0\0"¥"\\ #', '##0', '##0.00''\0\0"\0\0"¥"\\ ', '##0', '##0.00;\0*\06\0\0_-"¥"\\ * #', '##0', '##0', '\0)\0''\0\0_-* #', '##0_-;\\-* #', '##0_-;_-* "-"_-', '\0>\0\0_-"¥"\\ * #', '##0.00_-;\\-"¥"\\ * #', '##0.00_-;_-"¥"\\ * "-', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(237, 0, '\0\0\0õÿ \0\0´\0\0\0\0\0\0\0ª à\0\0\0\0\0õÿ \0\0Ô\0a\0\0±\0\0À à\0\0\0', '\0õÿ \0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(238, 0, '\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(239, 0, '\0\0\0\0\0ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0;\0\0\0\0\r\0\0\0\0\0???ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0???ÿ        \0\0\0\0\0???ÿ        	\0\0\0\0\0???ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(240, 0, '\0\0\0\0\0???ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\03\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0ÿ        \0\0\0\0\0ÿ        	\0\0\0\0\0ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(241, 0, '\0\0\0\0\0ÿrl€B2——}A\0}\0\0\0\0\0\0\0\0\0\0\0\07\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0ÿ€ÿ??_-;_-@}‘\0}\0\0\0\0\0\0\0\0\0\0\0\04\0\0\0\0\r\0\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0¥¥¥ÿ?', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(242, 0, '\0\0\0\0\0???ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\06\0\0\0\0\r\0\0\0\0\0ÿ\0\0ÿ.00\\)* "\0\0}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0=\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0ÿÿÌÿ??_-;_-@\0\0\0\0\0²²²ÿ ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(243, 0, '\0\0\0\0\0²²²ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\05\0\0\0\0\r\0\0\0\0\0ÿ.00\\)* "\0\0}U\0}\0\0\0\0\0\0\0\0\0\0\0\00\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0\0\0\0??_-;_-@\0\0\0\0\0\0\0\0 ', '\0\0\0\0\r', '0000-00-00', '\0\0*N''`r‚4\0’;\0’\0\0\0\0\0\0\0\0\0\0)ÿ\0*N''`r‚4\0\0\0\0\0\0\0\0ÿÀ\0ÿ\0\0\0\0\0ÿÿÿÿ%\0\0“\r\0-\0\0*N''`r‚5\0’;\0’\0\0\0\0\0\0\0\0\0\0-ÿ\0*N''`r‚5\0\0\0\0\0\0\0\0DrÄÿ\0\0\0\0\0ÿÿÿÿ', 'g’1\0’\0\0\0\0\0\0\0\0\0\05ÿ\0ã‰Ê‘''`‡e', 'g\0\0', 'g’/\0’\0\0\0\0\0\0\0\0\0\0', 'g\0\0', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(244, 0, 'ÿ\0èlÊ‘\0\0\0\0\0ÿ\0\0ÿÿÌÿ\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0	\0\0ÿ\0\0²²²ÿ\0ŽX\0Ž\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0T\0a\0b\0l\0e\0S\0t\0y\0l\0e\0M\0e\0d\0i\0u\0m\09\0P\0i\0v\0o\0t\0S\0t', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(245, 0, '\04dju$i-WÉ~~‚É~~‚ü\0C\0¬0ØY\0ë~t\0àoœtü\0\0@þ€øig\09p5‚Â”\0igÂ”ì—\0cm	\0cm~‚ 2ü\0Â”COÎ˜ \0€ø\0ì—$U‡t€øjuì—$U‡t€ø¶o˜"k\0ju‡tü\0juîoA%\0É', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(246, 0, '\0>þ', 'ƒæ0”mœ', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(247, 0, 'ÉúÆQWéÏÍKv¯Ub OXé#&½®¯¯ÊÍ1`R¢¦Též9<“l#¤Ü$©´>ŽÀr	`·Ð¡¹-Š;c=1g<yèº|Âv«çƒ<ŸHD®Õã©oŠª4„08', ' fªš³º', '0000-00-00', 'CâÏóHF‹ùeæ3Ñ¾mÅÆÛÝ(ëÈgãÅìO\0«ÿ‰þÎ4óßÖ_\0\0\0ÿÿ\0PK\0\0\0\0\0!\0¥Ö§çÀ\0\0\06\0\0\0\0\0_rels/.rels„ÏjÃ0‡ï…½ƒÑ}QÒÃ%v/¥C/£}\0á(h"ÛëÛOÇ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(248, 0, '»„¤ï÷©=þ®‹ùá”ç šªÃâC?Ëháv=¿‚É…¤§%[xp†£{Ûµ_¼PÑ£<Í1¥H¶0•ˆÙO¼R®BdÑÉÒJEÛ4b$§‘q_×˜žà6LÓõR×7`®¨Éÿ³Ã0ÌžOÁ¯', 'åEn7”', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(249, 0, 'Ã @á}¡wÙ7c»(Eb²Ë®»ö\0Cœ\ZAÇ ÒŸÛ×åãƒ7ÎßÕ›K\rY', 'œ\rŠeÍ', '0000-00-00', 'láÇæéxÉ´ßIÈsQ}#Õ…­µÝ Öµ+Õ!ï', 'Ý^¹$j=‹GWèÓ÷)âEë+&', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(250, 0, '8ý\0\0ÿÿ\0PK\0\0\0\0\0!\0w‹}–Â\0\0\Z\0\0\0\0\0theme/theme/theme1.xmlìYÏ‹ÛF¾ú?Ýÿ’d{‰7Ø²m³›„ØIÉql­ÉŽ4F\ZïÆ„@IŽ…BiZr)”^z(m	´Ðô¯Ù4%M!ÿBßŒdyÆw“%‡´d\r‹4úÞ›o', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(251, 0, 'SD<#\Z÷…o¬™Hìø°', 'É"ñil', '0000-00-00', '„µÉ(èÌñ0~3‹A€ˆfðmpÝå¼¼@Ô„kc=x7bðâü–ÆµÄsN3_', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(252, 0, 'B\rxÀm³Ø€Kb.%Âƒy45OÏUÜ5„ŽLsû(ÒRÛÏ@Y‰É¥`æUŠ"Ž¦8ÂÜ×Ø!Æ†ÕÝ$D‹ëÅ', 'anÝ$V', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(253, 0, 'ËÝK¶Sù´¼$ lß„„2™N¢j Q[Š Égsš„\\Ù[aÑ0°¨÷ËTm°\0jyV`ÿdÁ®«i»˜€<T!ŠÇ"Oiª—Ù•É|›™ÞL­`3±¬€U¦‚ëÖå‰Õ¥¥ö\Z™ÖH(å¦“‘‘=', '	ÐgÕ)', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(254, 0, 'ãUi~v°°dsHw?[C:¯!(1·V“^\0•ÓhŽ	¼ÑÌ…lUk)“]õ•¢¬¡tÑY€²Ž¢Šy', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(255, 0, '—RžÓ‘gy”³lÍP%$Y#NEƒUƒªuÓ¼k¤¶vÝÓDäÑ\\õLMUD×4«˜6Ã²\r¬ÅòlM^aµ1hšÚáSé^—ÜÆRëÖö	y—€€çñ3tÝ×h', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(256, 0, 'µÕd\Z5ÁxS†…fg£zïX.ðj¯Ó$Õ÷–n×â–÷ãt0x¦ÎvëUC“å¾RFZ~ûP¿N°á-¼žSžÈTÂ·‡Á†¨/÷$©lÀ-r›g·Yó˜4í;%·åø×/”ên·àTR¡î¶ª…–ëVË]·\\ê´+w¡±ð ', '»éw—¼', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(257, 0, 'Â0„÷‚wooÓº‘&ÝˆÐ­Ô„ä5\r6?$Qìí\r', '.‡a¾™', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(258, 0, '\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(259, 0, '\0\0\0	\0\0\0gfÌ\0€\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ú:\0\0œ<\0\0\r\0\0\0\0\0d\0\0\0\0\0\0\0\0\0\0ü©ñÒMbP?_\0\0\0*\0\0\0\0+\0\0\0\0‚\0\0\0€\0\0\0\0\0\0\0\0\0\0%\0\0\0', '\0\0Á', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(260, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '\0\0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(261, 0, '\0\0\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(262, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(263, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(264, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(265, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(266, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(267, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(268, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(269, 0, '\0\0\0	\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(270, 0, '\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(271, 0, '\0\0	\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(272, 0, '\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(273, 0, '\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(274, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(275, 0, '\0\0\0\r\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(276, 0, '\0\0\0\0\0\r\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(277, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(278, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(279, 0, '\0\0\0\0\0\0\0\0½\0*\0\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0½\0B\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0×\0\0Š\0\0\0î\0>\0¶\0\0\0\0@', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(280, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(281, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ÕÍÕœ.“—\0+', 'ù®0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(282, 0, '\0\0ÿÿÿÿ \0\0\0\0\0À\0\0\0\0\0\0F!\0\0\0Microsoft Excel 97 - 2004 ¹¤×÷±í\0\0\0\0Biff8\0\0\0\0Excel.Sheet.8\0ô9²q\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(283, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0à…ŸòùOh«‘\0+''³Ù0\0\0\0(–\0\0\0\0\0\0\0\0H\0\0\0\0\0\0P\0\0\0\0\0\0p\0\0\0\0\0\0\0\0\0\0\0\0´\0\0\0\r\0\0\0À\0\0\0\0\0\0Ì\0\0\0\0\0\0Ô\0\0\0\0\0\0''\0\0\0\0\0\0\0\0Microsoft Office ÓÃ»', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(284, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0€\0\0\0J\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0Q\0\0\0x”\0\0\0\0\0\0\0\0\0\0\0\0\0I\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0P\0\0\0(\0\0\0x\0\0\0\0”\0\0\0\0\0\0 \0Ì\0€\0\0\0J\0\0\0(\0\0\0€\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(285, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(286, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(287, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(288, 0, '˜Ÿÿ‘‘‘ÿ·˜‰ÿÿÿîÿ‰¥ÒÿÍÒ±ÿ˜p‘ÿÿÿêÿ·Èàÿ½Â·ÿ±«±ÿÍ½·ÿ±½Ðÿ«¥«ÿÿò×ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿçòõÿÒÍ×ÿòîåÿåîòÿÅÍÒÿ±¦¦ÿ‰y‘ÿ«˜˜ÿÒ×ÒÿûîÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(289, 0, '\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 \0\0\0þÿÿÿþÿÿÿþÿÿÿ$\0\0\0%\0\0\0&\0\0\0''\0\0\0(\0\0\0)\0\0\0*\0\0\0+\0\0\0', '\0\0\0-\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(290, 0, '\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0¼\0\0\0\0†\0‹[SO1\0\0ð\0\0\0	\0\0\0\0\0†ÿ‹[SO1\0\0´\0\0\0ÿ\0\0\0\0†\0‹[SO\0\0\0\0"¥"\\ #', '##0;\\-', '0000-00-00', '##0;[Red]\\-"¥"\\ #', '##0"\0\0\0\0"¥"\\ #', '##0', '##0.00''\0\0"\0\0"¥"\\ ', '##0', '##0.00;\0*\06\0\0_-"¥"\\ * #', '##0', '##0', '\0)\0''\0\0_-* #', '##0_-;\\-* #', '##0_-;_-* "-"_-', '\0>\0\0_-"¥"\\ * #', '##0.00_-;\\-"¥"\\ * #', '##0.00_-;_-"¥"\\ * "-', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(291, 0, '\0\0\0õÿ \0\0´\0\0\0\0\0\0\0ª à\0\0\0\0\0õÿ \0\0Ô\0a\0\0±\0\0À à\0\0\0', '\0õÿ \0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(292, 0, '\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(293, 0, '\0\0\0\0\0ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0;\0\0\0\0\r\0\0\0\0\0???ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0???ÿ        \0\0\0\0\0???ÿ        	\0\0\0\0\0???ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(294, 0, '\0\0\0\0\0???ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\03\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0ÿ        \0\0\0\0\0ÿ        	\0\0\0\0\0ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(295, 0, '\0\0\0\0\0ÿrl€B2——}A\0}\0\0\0\0\0\0\0\0\0\0\0\07\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0ÿ€ÿ??_-;_-@}‘\0}\0\0\0\0\0\0\0\0\0\0\0\04\0\0\0\0\r\0\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0¥¥¥ÿ?', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(296, 0, '\0\0\0\0\0???ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\06\0\0\0\0\r\0\0\0\0\0ÿ\0\0ÿ.00\\)* "\0\0}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0=\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0ÿÿÌÿ??_-;_-@\0\0\0\0\0²²²ÿ ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(297, 0, '\0\0\0\0\0²²²ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\05\0\0\0\0\r\0\0\0\0\0ÿ.00\\)* "\0\0}U\0}\0\0\0\0\0\0\0\0\0\0\0\00\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0\0\0\0??_-;_-@\0\0\0\0\0\0\0\0 ', '\0\0\0\0\r', '0000-00-00', '\0\0*N''`r‚4\0’;\0’\0\0\0\0\0\0\0\0\0\0)ÿ\0*N''`r‚4\0\0\0\0\0\0\0\0ÿÀ\0ÿ\0\0\0\0\0ÿÿÿÿ%\0\0“\r\0-\0\0*N''`r‚5\0’;\0’\0\0\0\0\0\0\0\0\0\0-ÿ\0*N''`r‚5\0\0\0\0\0\0\0\0DrÄÿ\0\0\0\0\0ÿÿÿÿ', 'g’1\0’\0\0\0\0\0\0\0\0\0\05ÿ\0ã‰Ê‘''`‡e', 'g\0\0', 'g’/\0’\0\0\0\0\0\0\0\0\0\0', 'g\0\0', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(298, 0, 'ÿ\0èlÊ‘\0\0\0\0\0ÿ\0\0ÿÿÌÿ\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0	\0\0ÿ\0\0²²²ÿ\0ŽX\0Ž\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0T\0a\0b\0l\0e\0S\0t\0y\0l\0e\0M\0e\0d\0i\0u\0m\09\0P\0i\0v\0o\0t\0S\0t', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(299, 0, '\04dju$i-WÉ~~‚É~~‚ü\0C\0¬0ØY\0ë~t\0àoœtü\0\0@þ€øig\09p5‚Â”\0igÂ”ì—\0cm	\0cm~‚ 2ü\0Â”COÎ˜ \0€ø\0ì—$U‡t€øjuì—$U‡t€ø¶o˜"k\0ju‡tü\0juîoA%\0É', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(300, 0, '\0>þ', 'ƒæ0”mœ', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(301, 0, 'ÉúÆQWéÏÍKv¯Ub OXé#&½®¯¯ÊÍ1`R¢¦Též9<“l#¤Ü$©´>ŽÀr	`·Ð¡¹-Š;c=1g<yèº|Âv«çƒ<ŸHD®Õã©oŠª4„08', ' fªš³º', '0000-00-00', 'CâÏóHF‹ùeæ3Ñ¾mÅÆÛÝ(ëÈgãÅìO\0«ÿ‰þÎ4óßÖ_\0\0\0ÿÿ\0PK\0\0\0\0\0!\0¥Ö§çÀ\0\0\06\0\0\0\0\0_rels/.rels„ÏjÃ0‡ï…½ƒÑ}QÒÃ%v/¥C/£}\0á(h"ÛëÛOÇ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(302, 0, '»„¤ï÷©=þ®‹ùá”ç šªÃâC?Ëháv=¿‚É…¤§%[xp†£{Ûµ_¼PÑ£<Í1¥H¶0•ˆÙO¼R®BdÑÉÒJEÛ4b$§‘q_×˜žà6LÓõR×7`®¨Éÿ³Ã0ÌžOÁ¯', 'åEn7”', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(303, 0, 'Ã @á}¡wÙ7c»(Eb²Ë®»ö\0Cœ\ZAÇ ÒŸÛ×åãƒ7ÎßÕ›K\rY', 'œ\rŠeÍ', '0000-00-00', 'láÇæéxÉ´ßIÈsQ}#Õ…­µÝ Öµ+Õ!ï', 'Ý^¹$j=‹GWèÓ÷)âEë+&', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(304, 0, '8ý\0\0ÿÿ\0PK\0\0\0\0\0!\0w‹}–Â\0\0\Z\0\0\0\0\0theme/theme/theme1.xmlìYÏ‹ÛF¾ú?Ýÿ’d{‰7Ø²m³›„ØIÉql­ÉŽ4F\ZïÆ„@IŽ…BiZr)”^z(m	´Ðô¯Ù4%M!ÿBßŒdyÆw“%‡´d\r‹4úÞ›o', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(305, 0, 'SD<#\Z÷…o¬™Hìø°', 'É"ñil', '0000-00-00', '„µÉ(èÌñ0~3‹A€ˆfðmpÝå¼¼@Ô„kc=x7bðâü–ÆµÄsN3_', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(306, 0, 'B\rxÀm³Ø€Kb.%Âƒy45OÏUÜ5„ŽLsû(ÒRÛÏ@Y‰É¥`æUŠ"Ž¦8ÂÜ×Ø!Æ†ÕÝ$D‹ëÅ', 'anÝ$V', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(307, 0, 'ËÝK¶Sù´¼$ lß„„2™N¢j Q[Š Égsš„\\Ù[aÑ0°¨÷ËTm°\0jyV`ÿdÁ®«i»˜€<T!ŠÇ"Oiª—Ù•É|›™ÞL­`3±¬€U¦‚ëÖå‰Õ¥¥ö\Z™ÖH(å¦“‘‘=', '	ÐgÕ)', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(308, 0, 'ãUi~v°°dsHw?[C:¯!(1·V“^\0•ÓhŽ	¼ÑÌ…lUk)“]õ•¢¬¡tÑY€²Ž¢Šy', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(309, 0, '—RžÓ‘gy”³lÍP%$Y#NEƒUƒªuÓ¼k¤¶vÝÓDäÑ\\õLMUD×4«˜6Ã²\r¬ÅòlM^aµ1hšÚáSé^—ÜÆRëÖö	y—€€çñ3tÝ×h', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(310, 0, 'µÕd\Z5ÁxS†…fg£zïX.ðj¯Ó$Õ÷–n×â–÷ãt0x¦ÎvëUC“å¾RFZ~ûP¿N°á-¼žSžÈTÂ·‡Á†¨/÷$©lÀ-r›g·Yó˜4í;%·åø×/”ên·àTR¡î¶ª…–ëVË]·\\ê´+w¡±ð ', '»éw—¼', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(311, 0, 'Â0„÷‚wooÓº‘&ÝˆÐ­Ô„ä5\r6?$Qìí\r', '.‡a¾™', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(312, 0, '\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(313, 0, '\0\0\0	\0\0\0gfÌ\0€\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ú:\0\0œ<\0\0\r\0\0\0\0\0d\0\0\0\0\0\0\0\0\0\0ü©ñÒMbP?_\0\0\0*\0\0\0\0+\0\0\0\0‚\0\0\0€\0\0\0\0\0\0\0\0\0\0%\0\0\0', '\0\0Á', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(314, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '\0\0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(315, 0, '\0\0\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(316, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(317, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(318, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(319, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(320, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(321, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(322, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(323, 0, '\0\0\0	\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(324, 0, '\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(325, 0, '\0\0	\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(326, 0, '\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(327, 0, '\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(328, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(329, 0, '\0\0\0\r\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(330, 0, '\0\0\0\0\0\r\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(331, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(332, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(333, 0, '\0\0\0\0\0\0\0\0½\0*\0\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0½\0B\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0×\0\0Š\0\0\0î\0>\0¶\0\0\0\0@', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(334, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(335, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ÕÍÕœ.“—\0+', 'ù®0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(336, 0, '\0\0ÿÿÿÿ \0\0\0\0\0À\0\0\0\0\0\0F!\0\0\0Microsoft Excel 97 - 2004 ¹¤×÷±í\0\0\0\0Biff8\0\0\0\0Excel.Sheet.8\0ô9²q\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(337, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0à…ŸòùOh«‘\0+''³Ù0\0\0\0(–\0\0\0\0\0\0\0\0H\0\0\0\0\0\0P\0\0\0\0\0\0p\0\0\0\0\0\0\0\0\0\0\0\0´\0\0\0\r\0\0\0À\0\0\0\0\0\0Ì\0\0\0\0\0\0Ô\0\0\0\0\0\0''\0\0\0\0\0\0\0\0Microsoft Office ÓÃ»', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(338, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0€\0\0\0J\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0Q\0\0\0x”\0\0\0\0\0\0\0\0\0\0\0\0\0I\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0P\0\0\0(\0\0\0x\0\0\0\0”\0\0\0\0\0\0 \0Ì\0€\0\0\0J\0\0\0(\0\0\0€\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(339, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(340, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(341, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(342, 0, '˜Ÿÿ‘‘‘ÿ·˜‰ÿÿÿîÿ‰¥ÒÿÍÒ±ÿ˜p‘ÿÿÿêÿ·Èàÿ½Â·ÿ±«±ÿÍ½·ÿ±½Ðÿ«¥«ÿÿò×ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿçòõÿÒÍ×ÿòîåÿåîòÿÅÍÒÿ±¦¦ÿ‰y‘ÿ«˜˜ÿÒ×ÒÿûîÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(343, 0, '\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 \0\0\0þÿÿÿþÿÿÿþÿÿÿ$\0\0\0%\0\0\0&\0\0\0''\0\0\0(\0\0\0)\0\0\0*\0\0\0+\0\0\0', '\0\0\0-\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(344, 0, '\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0¼\0\0\0\0†\0‹[SO1\0\0ð\0\0\0	\0\0\0\0\0†ÿ‹[SO1\0\0´\0\0\0ÿ\0\0\0\0†\0‹[SO\0\0\0\0"¥"\\ #', '##0;\\-', '0000-00-00', '##0;[Red]\\-"¥"\\ #', '##0"\0\0\0\0"¥"\\ #', '##0', '##0.00''\0\0"\0\0"¥"\\ ', '##0', '##0.00;\0*\06\0\0_-"¥"\\ * #', '##0', '##0', '\0)\0''\0\0_-* #', '##0_-;\\-* #', '##0_-;_-* "-"_-', '\0>\0\0_-"¥"\\ * #', '##0.00_-;\\-"¥"\\ * #', '##0.00_-;_-"¥"\\ * "-', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(345, 0, '\0\0\0õÿ \0\0´\0\0\0\0\0\0\0ª à\0\0\0\0\0õÿ \0\0Ô\0a\0\0±\0\0À à\0\0\0', '\0õÿ \0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(346, 0, '\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(347, 0, '\0\0\0\0\0ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0;\0\0\0\0\r\0\0\0\0\0???ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0???ÿ        \0\0\0\0\0???ÿ        	\0\0\0\0\0???ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(348, 0, '\0\0\0\0\0???ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\03\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0ÿ        \0\0\0\0\0ÿ        	\0\0\0\0\0ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(349, 0, '\0\0\0\0\0ÿrl€B2——}A\0}\0\0\0\0\0\0\0\0\0\0\0\07\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0ÿ€ÿ??_-;_-@}‘\0}\0\0\0\0\0\0\0\0\0\0\0\04\0\0\0\0\r\0\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0¥¥¥ÿ?', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(350, 0, '\0\0\0\0\0???ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\06\0\0\0\0\r\0\0\0\0\0ÿ\0\0ÿ.00\\)* "\0\0}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0=\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0ÿÿÌÿ??_-;_-@\0\0\0\0\0²²²ÿ ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(351, 0, '\0\0\0\0\0²²²ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\05\0\0\0\0\r\0\0\0\0\0ÿ.00\\)* "\0\0}U\0}\0\0\0\0\0\0\0\0\0\0\0\00\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0\0\0\0??_-;_-@\0\0\0\0\0\0\0\0 ', '\0\0\0\0\r', '0000-00-00', '\0\0*N''`r‚4\0’;\0’\0\0\0\0\0\0\0\0\0\0)ÿ\0*N''`r‚4\0\0\0\0\0\0\0\0ÿÀ\0ÿ\0\0\0\0\0ÿÿÿÿ%\0\0“\r\0-\0\0*N''`r‚5\0’;\0’\0\0\0\0\0\0\0\0\0\0-ÿ\0*N''`r‚5\0\0\0\0\0\0\0\0DrÄÿ\0\0\0\0\0ÿÿÿÿ', 'g’1\0’\0\0\0\0\0\0\0\0\0\05ÿ\0ã‰Ê‘''`‡e', 'g\0\0', 'g’/\0’\0\0\0\0\0\0\0\0\0\0', 'g\0\0', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(352, 0, 'ÿ\0èlÊ‘\0\0\0\0\0ÿ\0\0ÿÿÌÿ\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0	\0\0ÿ\0\0²²²ÿ\0ŽX\0Ž\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0T\0a\0b\0l\0e\0S\0t\0y\0l\0e\0M\0e\0d\0i\0u\0m\09\0P\0i\0v\0o\0t\0S\0t', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(353, 0, '\04dju$i-WÉ~~‚É~~‚ü\0C\0¬0ØY\0ë~t\0àoœtü\0\0@þ€øig\09p5‚Â”\0igÂ”ì—\0cm	\0cm~‚ 2ü\0Â”COÎ˜ \0€ø\0ì—$U‡t€øjuì—$U‡t€ø¶o˜"k\0ju‡tü\0juîoA%\0É', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(354, 0, '\0>þ', 'ƒæ0”mœ', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(355, 0, 'ÉúÆQWéÏÍKv¯Ub OXé#&½®¯¯ÊÍ1`R¢¦Též9<“l#¤Ü$©´>ŽÀr	`·Ð¡¹-Š;c=1g<yèº|Âv«çƒ<ŸHD®Õã©oŠª4„08', ' fªš³º', '0000-00-00', 'CâÏóHF‹ùeæ3Ñ¾mÅÆÛÝ(ëÈgãÅìO\0«ÿ‰þÎ4óßÖ_\0\0\0ÿÿ\0PK\0\0\0\0\0!\0¥Ö§çÀ\0\0\06\0\0\0\0\0_rels/.rels„ÏjÃ0‡ï…½ƒÑ}QÒÃ%v/¥C/£}\0á(h"ÛëÛOÇ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(356, 0, '»„¤ï÷©=þ®‹ùá”ç šªÃâC?Ëháv=¿‚É…¤§%[xp†£{Ûµ_¼PÑ£<Í1¥H¶0•ˆÙO¼R®BdÑÉÒJEÛ4b$§‘q_×˜žà6LÓõR×7`®¨Éÿ³Ã0ÌžOÁ¯', 'åEn7”', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(357, 0, 'Ã @á}¡wÙ7c»(Eb²Ë®»ö\0Cœ\ZAÇ ÒŸÛ×åãƒ7ÎßÕ›K\rY', 'œ\rŠeÍ', '0000-00-00', 'láÇæéxÉ´ßIÈsQ}#Õ…­µÝ Öµ+Õ!ï', 'Ý^¹$j=‹GWèÓ÷)âEë+&', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', '');
INSERT INTO `volunteers` (`volunteer_id`, `subject_id`, `v_name`, `gender`, `bday`, `nation`, `native`, `married`, `blood_type`, `surgery`, `fam_history`, `smoker`, `drinker`, `allergies`, `height`, `weight`, `bmi`, `med_history`, `mobile`, `remarks`, `status`, `date_status`, `status_updated_by`, `date_added`, `added_by`, `date_update`, `updated_by`, `id_card_num`, `id_card_path`) VALUES
(358, 0, '8ý\0\0ÿÿ\0PK\0\0\0\0\0!\0w‹}–Â\0\0\Z\0\0\0\0\0theme/theme/theme1.xmlìYÏ‹ÛF¾ú?Ýÿ’d{‰7Ø²m³›„ØIÉql­ÉŽ4F\ZïÆ„@IŽ…BiZr)”^z(m	´Ðô¯Ù4%M!ÿBßŒdyÆw“%‡´d\r‹4úÞ›o', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(359, 0, 'SD<#\Z÷…o¬™Hìø°', 'É"ñil', '0000-00-00', '„µÉ(èÌñ0~3‹A€ˆfðmpÝå¼¼@Ô„kc=x7bðâü–ÆµÄsN3_', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(360, 0, 'B\rxÀm³Ø€Kb.%Âƒy45OÏUÜ5„ŽLsû(ÒRÛÏ@Y‰É¥`æUŠ"Ž¦8ÂÜ×Ø!Æ†ÕÝ$D‹ëÅ', 'anÝ$V', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(361, 0, 'ËÝK¶Sù´¼$ lß„„2™N¢j Q[Š Égsš„\\Ù[aÑ0°¨÷ËTm°\0jyV`ÿdÁ®«i»˜€<T!ŠÇ"Oiª—Ù•É|›™ÞL­`3±¬€U¦‚ëÖå‰Õ¥¥ö\Z™ÖH(å¦“‘‘=', '	ÐgÕ)', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(362, 0, 'ãUi~v°°dsHw?[C:¯!(1·V“^\0•ÓhŽ	¼ÑÌ…lUk)“]õ•¢¬¡tÑY€²Ž¢Šy', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(363, 0, '—RžÓ‘gy”³lÍP%$Y#NEƒUƒªuÓ¼k¤¶vÝÓDäÑ\\õLMUD×4«˜6Ã²\r¬ÅòlM^aµ1hšÚáSé^—ÜÆRëÖö	y—€€çñ3tÝ×h', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(364, 0, 'µÕd\Z5ÁxS†…fg£zïX.ðj¯Ó$Õ÷–n×â–÷ãt0x¦ÎvëUC“å¾RFZ~ûP¿N°á-¼žSžÈTÂ·‡Á†¨/÷$©lÀ-r›g·Yó˜4í;%·åø×/”ên·àTR¡î¶ª…–ëVË]·\\ê´+w¡±ð ', '»éw—¼', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(365, 0, 'Â0„÷‚wooÓº‘&ÝˆÐ­Ô„ä5\r6?$Qìí\r', '.‡a¾™', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(366, 0, '\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(367, 0, '\0\0\0	\0\0\0gfÌ\0€\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ú:\0\0œ<\0\0\r\0\0\0\0\0d\0\0\0\0\0\0\0\0\0\0ü©ñÒMbP?_\0\0\0*\0\0\0\0+\0\0\0\0‚\0\0\0€\0\0\0\0\0\0\0\0\0\0%\0\0\0', '\0\0Á', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(368, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '\0\0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(369, 0, '\0\0\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(370, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(371, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(372, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(373, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(374, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(375, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(376, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(377, 0, '\0\0\0	\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(378, 0, '\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(379, 0, '\0\0	\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(380, 0, '\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(381, 0, '\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(382, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(383, 0, '\0\0\0\r\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(384, 0, '\0\0\0\0\0\r\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(385, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(386, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(387, 0, '\0\0\0\0\0\0\0\0½\0*\0\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0½\0B\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0×\0\0Š\0\0\0î\0>\0¶\0\0\0\0@', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(388, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(389, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ÕÍÕœ.“—\0+', 'ù®0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(390, 0, '\0\0ÿÿÿÿ \0\0\0\0\0À\0\0\0\0\0\0F!\0\0\0Microsoft Excel 97 - 2004 ¹¤×÷±í\0\0\0\0Biff8\0\0\0\0Excel.Sheet.8\0ô9²q\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(391, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0à…ŸòùOh«‘\0+''³Ù0\0\0\0(–\0\0\0\0\0\0\0\0H\0\0\0\0\0\0P\0\0\0\0\0\0p\0\0\0\0\0\0\0\0\0\0\0\0´\0\0\0\r\0\0\0À\0\0\0\0\0\0Ì\0\0\0\0\0\0Ô\0\0\0\0\0\0''\0\0\0\0\0\0\0\0Microsoft Office ÓÃ»', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(392, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0€\0\0\0J\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0Q\0\0\0x”\0\0\0\0\0\0\0\0\0\0\0\0\0I\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0P\0\0\0(\0\0\0x\0\0\0\0”\0\0\0\0\0\0 \0Ì\0€\0\0\0J\0\0\0(\0\0\0€\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(393, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(394, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(395, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(396, 0, '˜Ÿÿ‘‘‘ÿ·˜‰ÿÿÿîÿ‰¥ÒÿÍÒ±ÿ˜p‘ÿÿÿêÿ·Èàÿ½Â·ÿ±«±ÿÍ½·ÿ±½Ðÿ«¥«ÿÿò×ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿçòõÿÒÍ×ÿòîåÿåîòÿÅÍÒÿ±¦¦ÿ‰y‘ÿ«˜˜ÿÒ×ÒÿûîÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(397, 0, '\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 \0\0\0þÿÿÿþÿÿÿþÿÿÿ$\0\0\0%\0\0\0&\0\0\0''\0\0\0(\0\0\0)\0\0\0*\0\0\0+\0\0\0', '\0\0\0-\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(398, 0, '\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0¼\0\0\0\0†\0‹[SO1\0\0ð\0\0\0	\0\0\0\0\0†ÿ‹[SO1\0\0´\0\0\0ÿ\0\0\0\0†\0‹[SO\0\0\0\0"¥"\\ #', '##0;\\-', '0000-00-00', '##0;[Red]\\-"¥"\\ #', '##0"\0\0\0\0"¥"\\ #', '##0', '##0.00''\0\0"\0\0"¥"\\ ', '##0', '##0.00;\0*\06\0\0_-"¥"\\ * #', '##0', '##0', '\0)\0''\0\0_-* #', '##0_-;\\-* #', '##0_-;_-* "-"_-', '\0>\0\0_-"¥"\\ * #', '##0.00_-;\\-"¥"\\ * #', '##0.00_-;_-"¥"\\ * "-', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(399, 0, '\0\0\0õÿ \0\0´\0\0\0\0\0\0\0ª à\0\0\0\0\0õÿ \0\0Ô\0a\0\0±\0\0À à\0\0\0', '\0õÿ \0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(400, 0, '\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(401, 0, '\0\0\0\0\0ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0;\0\0\0\0\r\0\0\0\0\0???ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0???ÿ        \0\0\0\0\0???ÿ        	\0\0\0\0\0???ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(402, 0, '\0\0\0\0\0???ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\03\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0ÿ        \0\0\0\0\0ÿ        	\0\0\0\0\0ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(403, 0, '\0\0\0\0\0ÿrl€B2——}A\0}\0\0\0\0\0\0\0\0\0\0\0\07\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0ÿ€ÿ??_-;_-@}‘\0}\0\0\0\0\0\0\0\0\0\0\0\04\0\0\0\0\r\0\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0¥¥¥ÿ?', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(404, 0, '\0\0\0\0\0???ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\06\0\0\0\0\r\0\0\0\0\0ÿ\0\0ÿ.00\\)* "\0\0}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0=\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0ÿÿÌÿ??_-;_-@\0\0\0\0\0²²²ÿ ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(405, 0, '\0\0\0\0\0²²²ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\05\0\0\0\0\r\0\0\0\0\0ÿ.00\\)* "\0\0}U\0}\0\0\0\0\0\0\0\0\0\0\0\00\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0\0\0\0??_-;_-@\0\0\0\0\0\0\0\0 ', '\0\0\0\0\r', '0000-00-00', '\0\0*N''`r‚4\0’;\0’\0\0\0\0\0\0\0\0\0\0)ÿ\0*N''`r‚4\0\0\0\0\0\0\0\0ÿÀ\0ÿ\0\0\0\0\0ÿÿÿÿ%\0\0“\r\0-\0\0*N''`r‚5\0’;\0’\0\0\0\0\0\0\0\0\0\0-ÿ\0*N''`r‚5\0\0\0\0\0\0\0\0DrÄÿ\0\0\0\0\0ÿÿÿÿ', 'g’1\0’\0\0\0\0\0\0\0\0\0\05ÿ\0ã‰Ê‘''`‡e', 'g\0\0', 'g’/\0’\0\0\0\0\0\0\0\0\0\0', 'g\0\0', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(406, 0, 'ÿ\0èlÊ‘\0\0\0\0\0ÿ\0\0ÿÿÌÿ\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0	\0\0ÿ\0\0²²²ÿ\0ŽX\0Ž\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0T\0a\0b\0l\0e\0S\0t\0y\0l\0e\0M\0e\0d\0i\0u\0m\09\0P\0i\0v\0o\0t\0S\0t', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(407, 0, '\04dju$i-WÉ~~‚É~~‚ü\0C\0¬0ØY\0ë~t\0àoœtü\0\0@þ€øig\09p5‚Â”\0igÂ”ì—\0cm	\0cm~‚ 2ü\0Â”COÎ˜ \0€ø\0ì—$U‡t€øjuì—$U‡t€ø¶o˜"k\0ju‡tü\0juîoA%\0É', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(408, 0, '\0>þ', 'ƒæ0”mœ', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(409, 0, 'ÉúÆQWéÏÍKv¯Ub OXé#&½®¯¯ÊÍ1`R¢¦Též9<“l#¤Ü$©´>ŽÀr	`·Ð¡¹-Š;c=1g<yèº|Âv«çƒ<ŸHD®Õã©oŠª4„08', ' fªš³º', '0000-00-00', 'CâÏóHF‹ùeæ3Ñ¾mÅÆÛÝ(ëÈgãÅìO\0«ÿ‰þÎ4óßÖ_\0\0\0ÿÿ\0PK\0\0\0\0\0!\0¥Ö§çÀ\0\0\06\0\0\0\0\0_rels/.rels„ÏjÃ0‡ï…½ƒÑ}QÒÃ%v/¥C/£}\0á(h"ÛëÛOÇ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(410, 0, '»„¤ï÷©=þ®‹ùá”ç šªÃâC?Ëháv=¿‚É…¤§%[xp†£{Ûµ_¼PÑ£<Í1¥H¶0•ˆÙO¼R®BdÑÉÒJEÛ4b$§‘q_×˜žà6LÓõR×7`®¨Éÿ³Ã0ÌžOÁ¯', 'åEn7”', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(411, 0, 'Ã @á}¡wÙ7c»(Eb²Ë®»ö\0Cœ\ZAÇ ÒŸÛ×åãƒ7ÎßÕ›K\rY', 'œ\rŠeÍ', '0000-00-00', 'láÇæéxÉ´ßIÈsQ}#Õ…­µÝ Öµ+Õ!ï', 'Ý^¹$j=‹GWèÓ÷)âEë+&', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(412, 0, '8ý\0\0ÿÿ\0PK\0\0\0\0\0!\0w‹}–Â\0\0\Z\0\0\0\0\0theme/theme/theme1.xmlìYÏ‹ÛF¾ú?Ýÿ’d{‰7Ø²m³›„ØIÉql­ÉŽ4F\ZïÆ„@IŽ…BiZr)”^z(m	´Ðô¯Ù4%M!ÿBßŒdyÆw“%‡´d\r‹4úÞ›o', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(413, 0, 'SD<#\Z÷…o¬™Hìø°', 'É"ñil', '0000-00-00', '„µÉ(èÌñ0~3‹A€ˆfðmpÝå¼¼@Ô„kc=x7bðâü–ÆµÄsN3_', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(414, 0, 'B\rxÀm³Ø€Kb.%Âƒy45OÏUÜ5„ŽLsû(ÒRÛÏ@Y‰É¥`æUŠ"Ž¦8ÂÜ×Ø!Æ†ÕÝ$D‹ëÅ', 'anÝ$V', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(415, 0, 'ËÝK¶Sù´¼$ lß„„2™N¢j Q[Š Égsš„\\Ù[aÑ0°¨÷ËTm°\0jyV`ÿdÁ®«i»˜€<T!ŠÇ"Oiª—Ù•É|›™ÞL­`3±¬€U¦‚ëÖå‰Õ¥¥ö\Z™ÖH(å¦“‘‘=', '	ÐgÕ)', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(416, 0, 'ãUi~v°°dsHw?[C:¯!(1·V“^\0•ÓhŽ	¼ÑÌ…lUk)“]õ•¢¬¡tÑY€²Ž¢Šy', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(417, 0, '—RžÓ‘gy”³lÍP%$Y#NEƒUƒªuÓ¼k¤¶vÝÓDäÑ\\õLMUD×4«˜6Ã²\r¬ÅòlM^aµ1hšÚáSé^—ÜÆRëÖö	y—€€çñ3tÝ×h', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(418, 0, 'µÕd\Z5ÁxS†…fg£zïX.ðj¯Ó$Õ÷–n×â–÷ãt0x¦ÎvëUC“å¾RFZ~ûP¿N°á-¼žSžÈTÂ·‡Á†¨/÷$©lÀ-r›g·Yó˜4í;%·åø×/”ên·àTR¡î¶ª…–ëVË]·\\ê´+w¡±ð ', '»éw—¼', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(419, 0, 'Â0„÷‚wooÓº‘&ÝˆÐ­Ô„ä5\r6?$Qìí\r', '.‡a¾™', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(420, 0, '\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(421, 0, '\0\0\0	\0\0\0gfÌ\0€\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ú:\0\0œ<\0\0\r\0\0\0\0\0d\0\0\0\0\0\0\0\0\0\0ü©ñÒMbP?_\0\0\0*\0\0\0\0+\0\0\0\0‚\0\0\0€\0\0\0\0\0\0\0\0\0\0%\0\0\0', '\0\0Á', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(422, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '\0\0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(423, 0, '\0\0\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(424, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(425, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(426, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(427, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(428, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(429, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(430, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(431, 0, '\0\0\0	\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(432, 0, '\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(433, 0, '\0\0	\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(434, 0, '\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(435, 0, '\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(436, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(437, 0, '\0\0\0\r\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(438, 0, '\0\0\0\0\0\r\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(439, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(440, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(441, 0, '\0\0\0\0\0\0\0\0½\0*\0\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0½\0B\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0×\0\0Š\0\0\0î\0>\0¶\0\0\0\0@', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(442, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(443, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ÕÍÕœ.“—\0+', 'ù®0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(444, 0, '\0\0ÿÿÿÿ \0\0\0\0\0À\0\0\0\0\0\0F!\0\0\0Microsoft Excel 97 - 2004 ¹¤×÷±í\0\0\0\0Biff8\0\0\0\0Excel.Sheet.8\0ô9²q\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(445, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0à…ŸòùOh«‘\0+''³Ù0\0\0\0(–\0\0\0\0\0\0\0\0H\0\0\0\0\0\0P\0\0\0\0\0\0p\0\0\0\0\0\0\0\0\0\0\0\0´\0\0\0\r\0\0\0À\0\0\0\0\0\0Ì\0\0\0\0\0\0Ô\0\0\0\0\0\0''\0\0\0\0\0\0\0\0Microsoft Office ÓÃ»', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(446, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0€\0\0\0J\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0Q\0\0\0x”\0\0\0\0\0\0\0\0\0\0\0\0\0I\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0P\0\0\0(\0\0\0x\0\0\0\0”\0\0\0\0\0\0 \0Ì\0€\0\0\0J\0\0\0(\0\0\0€\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(447, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(448, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(449, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(450, 0, '˜Ÿÿ‘‘‘ÿ·˜‰ÿÿÿîÿ‰¥ÒÿÍÒ±ÿ˜p‘ÿÿÿêÿ·Èàÿ½Â·ÿ±«±ÿÍ½·ÿ±½Ðÿ«¥«ÿÿò×ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿçòõÿÒÍ×ÿòîåÿåîòÿÅÍÒÿ±¦¦ÿ‰y‘ÿ«˜˜ÿÒ×ÒÿûîÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(451, 0, '\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 \0\0\0þÿÿÿþÿÿÿþÿÿÿ$\0\0\0%\0\0\0&\0\0\0''\0\0\0(\0\0\0)\0\0\0*\0\0\0+\0\0\0', '\0\0\0-\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(452, 0, '\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0¼\0\0\0\0†\0‹[SO1\0\0ð\0\0\0	\0\0\0\0\0†ÿ‹[SO1\0\0´\0\0\0ÿ\0\0\0\0†\0‹[SO\0\0\0\0"¥"\\ #', '##0;\\-', '0000-00-00', '##0;[Red]\\-"¥"\\ #', '##0"\0\0\0\0"¥"\\ #', '##0', '##0.00''\0\0"\0\0"¥"\\ ', '##0', '##0.00;\0*\06\0\0_-"¥"\\ * #', '##0', '##0', '\0)\0''\0\0_-* #', '##0_-;\\-* #', '##0_-;_-* "-"_-', '\0>\0\0_-"¥"\\ * #', '##0.00_-;\\-"¥"\\ * #', '##0.00_-;_-"¥"\\ * "-', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(453, 0, '\0\0\0õÿ \0\0´\0\0\0\0\0\0\0ª à\0\0\0\0\0õÿ \0\0Ô\0a\0\0±\0\0À à\0\0\0', '\0õÿ \0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(454, 0, '\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(455, 0, '\0\0\0\0\0ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0;\0\0\0\0\r\0\0\0\0\0???ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0???ÿ        \0\0\0\0\0???ÿ        	\0\0\0\0\0???ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(456, 0, '\0\0\0\0\0???ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\03\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0ÿ        \0\0\0\0\0ÿ        	\0\0\0\0\0ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(457, 0, '\0\0\0\0\0ÿrl€B2——}A\0}\0\0\0\0\0\0\0\0\0\0\0\07\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0ÿ€ÿ??_-;_-@}‘\0}\0\0\0\0\0\0\0\0\0\0\0\04\0\0\0\0\r\0\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0¥¥¥ÿ?', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(458, 0, '\0\0\0\0\0???ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\06\0\0\0\0\r\0\0\0\0\0ÿ\0\0ÿ.00\\)* "\0\0}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0=\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0ÿÿÌÿ??_-;_-@\0\0\0\0\0²²²ÿ ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(459, 0, '\0\0\0\0\0²²²ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\05\0\0\0\0\r\0\0\0\0\0ÿ.00\\)* "\0\0}U\0}\0\0\0\0\0\0\0\0\0\0\0\00\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0\0\0\0??_-;_-@\0\0\0\0\0\0\0\0 ', '\0\0\0\0\r', '0000-00-00', '\0\0*N''`r‚4\0’;\0’\0\0\0\0\0\0\0\0\0\0)ÿ\0*N''`r‚4\0\0\0\0\0\0\0\0ÿÀ\0ÿ\0\0\0\0\0ÿÿÿÿ%\0\0“\r\0-\0\0*N''`r‚5\0’;\0’\0\0\0\0\0\0\0\0\0\0-ÿ\0*N''`r‚5\0\0\0\0\0\0\0\0DrÄÿ\0\0\0\0\0ÿÿÿÿ', 'g’1\0’\0\0\0\0\0\0\0\0\0\05ÿ\0ã‰Ê‘''`‡e', 'g\0\0', 'g’/\0’\0\0\0\0\0\0\0\0\0\0', 'g\0\0', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(460, 0, 'ÿ\0èlÊ‘\0\0\0\0\0ÿ\0\0ÿÿÌÿ\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0	\0\0ÿ\0\0²²²ÿ\0ŽX\0Ž\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0T\0a\0b\0l\0e\0S\0t\0y\0l\0e\0M\0e\0d\0i\0u\0m\09\0P\0i\0v\0o\0t\0S\0t', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(461, 0, '\04dju$i-WÉ~~‚É~~‚ü\0C\0¬0ØY\0ë~t\0àoœtü\0\0@þ€øig\09p5‚Â”\0igÂ”ì—\0cm	\0cm~‚ 2ü\0Â”COÎ˜ \0€ø\0ì—$U‡t€øjuì—$U‡t€ø¶o˜"k\0ju‡tü\0juîoA%\0É', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(462, 0, '\0>þ', 'ƒæ0”mœ', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(463, 0, 'ÉúÆQWéÏÍKv¯Ub OXé#&½®¯¯ÊÍ1`R¢¦Též9<“l#¤Ü$©´>ŽÀr	`·Ð¡¹-Š;c=1g<yèº|Âv«çƒ<ŸHD®Õã©oŠª4„08', ' fªš³º', '0000-00-00', 'CâÏóHF‹ùeæ3Ñ¾mÅÆÛÝ(ëÈgãÅìO\0«ÿ‰þÎ4óßÖ_\0\0\0ÿÿ\0PK\0\0\0\0\0!\0¥Ö§çÀ\0\0\06\0\0\0\0\0_rels/.rels„ÏjÃ0‡ï…½ƒÑ}QÒÃ%v/¥C/£}\0á(h"ÛëÛOÇ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(464, 0, '»„¤ï÷©=þ®‹ùá”ç šªÃâC?Ëháv=¿‚É…¤§%[xp†£{Ûµ_¼PÑ£<Í1¥H¶0•ˆÙO¼R®BdÑÉÒJEÛ4b$§‘q_×˜žà6LÓõR×7`®¨Éÿ³Ã0ÌžOÁ¯', 'åEn7”', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(465, 0, 'Ã @á}¡wÙ7c»(Eb²Ë®»ö\0Cœ\ZAÇ ÒŸÛ×åãƒ7ÎßÕ›K\rY', 'œ\rŠeÍ', '0000-00-00', 'láÇæéxÉ´ßIÈsQ}#Õ…­µÝ Öµ+Õ!ï', 'Ý^¹$j=‹GWèÓ÷)âEë+&', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(466, 0, '8ý\0\0ÿÿ\0PK\0\0\0\0\0!\0w‹}–Â\0\0\Z\0\0\0\0\0theme/theme/theme1.xmlìYÏ‹ÛF¾ú?Ýÿ’d{‰7Ø²m³›„ØIÉql­ÉŽ4F\ZïÆ„@IŽ…BiZr)”^z(m	´Ðô¯Ù4%M!ÿBßŒdyÆw“%‡´d\r‹4úÞ›o', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(467, 0, 'SD<#\Z÷…o¬™Hìø°', 'É"ñil', '0000-00-00', '„µÉ(èÌñ0~3‹A€ˆfðmpÝå¼¼@Ô„kc=x7bðâü–ÆµÄsN3_', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(468, 0, 'B\rxÀm³Ø€Kb.%Âƒy45OÏUÜ5„ŽLsû(ÒRÛÏ@Y‰É¥`æUŠ"Ž¦8ÂÜ×Ø!Æ†ÕÝ$D‹ëÅ', 'anÝ$V', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(469, 0, 'ËÝK¶Sù´¼$ lß„„2™N¢j Q[Š Égsš„\\Ù[aÑ0°¨÷ËTm°\0jyV`ÿdÁ®«i»˜€<T!ŠÇ"Oiª—Ù•É|›™ÞL­`3±¬€U¦‚ëÖå‰Õ¥¥ö\Z™ÖH(å¦“‘‘=', '	ÐgÕ)', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(470, 0, 'ãUi~v°°dsHw?[C:¯!(1·V“^\0•ÓhŽ	¼ÑÌ…lUk)“]õ•¢¬¡tÑY€²Ž¢Šy', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(471, 0, '—RžÓ‘gy”³lÍP%$Y#NEƒUƒªuÓ¼k¤¶vÝÓDäÑ\\õLMUD×4«˜6Ã²\r¬ÅòlM^aµ1hšÚáSé^—ÜÆRëÖö	y—€€çñ3tÝ×h', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(472, 0, 'µÕd\Z5ÁxS†…fg£zïX.ðj¯Ó$Õ÷–n×â–÷ãt0x¦ÎvëUC“å¾RFZ~ûP¿N°á-¼žSžÈTÂ·‡Á†¨/÷$©lÀ-r›g·Yó˜4í;%·åø×/”ên·àTR¡î¶ª…–ëVË]·\\ê´+w¡±ð ', '»éw—¼', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(473, 0, 'Â0„÷‚wooÓº‘&ÝˆÐ­Ô„ä5\r6?$Qìí\r', '.‡a¾™', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(474, 0, '\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(475, 0, '\0\0\0	\0\0\0gfÌ\0€\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ú:\0\0œ<\0\0\r\0\0\0\0\0d\0\0\0\0\0\0\0\0\0\0ü©ñÒMbP?_\0\0\0*\0\0\0\0+\0\0\0\0‚\0\0\0€\0\0\0\0\0\0\0\0\0\0%\0\0\0', '\0\0Á', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(476, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '\0\0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(477, 0, '\0\0\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(478, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(479, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(480, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(481, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(482, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(483, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(484, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(485, 0, '\0\0\0	\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(486, 0, '\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(487, 0, '\0\0	\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(488, 0, '\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(489, 0, '\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(490, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(491, 0, '\0\0\0\r\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(492, 0, '\0\0\0\0\0\r\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(493, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(494, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(495, 0, '\0\0\0\0\0\0\0\0½\0*\0\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0½\0B\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0×\0\0Š\0\0\0î\0>\0¶\0\0\0\0@', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(496, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(497, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ÕÍÕœ.“—\0+', 'ù®0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(498, 0, '\0\0ÿÿÿÿ \0\0\0\0\0À\0\0\0\0\0\0F!\0\0\0Microsoft Excel 97 - 2004 ¹¤×÷±í\0\0\0\0Biff8\0\0\0\0Excel.Sheet.8\0ô9²q\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(499, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0à…ŸòùOh«‘\0+''³Ù0\0\0\0(–\0\0\0\0\0\0\0\0H\0\0\0\0\0\0P\0\0\0\0\0\0p\0\0\0\0\0\0\0\0\0\0\0\0´\0\0\0\r\0\0\0À\0\0\0\0\0\0Ì\0\0\0\0\0\0Ô\0\0\0\0\0\0''\0\0\0\0\0\0\0\0Microsoft Office ÓÃ»', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(500, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0€\0\0\0J\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0Q\0\0\0x”\0\0\0\0\0\0\0\0\0\0\0\0\0I\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0P\0\0\0(\0\0\0x\0\0\0\0”\0\0\0\0\0\0 \0Ì\0€\0\0\0J\0\0\0(\0\0\0€\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(501, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(502, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(503, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(504, 0, '˜Ÿÿ‘‘‘ÿ·˜‰ÿÿÿîÿ‰¥ÒÿÍÒ±ÿ˜p‘ÿÿÿêÿ·Èàÿ½Â·ÿ±«±ÿÍ½·ÿ±½Ðÿ«¥«ÿÿò×ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿçòõÿÒÍ×ÿòîåÿåîòÿÅÍÒÿ±¦¦ÿ‰y‘ÿ«˜˜ÿÒ×ÒÿûîÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(505, 0, '\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 \0\0\0þÿÿÿþÿÿÿþÿÿÿ$\0\0\0%\0\0\0&\0\0\0''\0\0\0(\0\0\0)\0\0\0*\0\0\0+\0\0\0', '\0\0\0-\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(506, 0, '\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0\0\0\0\0†\0‹[SO1\0\0ð\0\0\0¼\0\0\0\0†\0‹[SO1\0\0ð\0\0\0	\0\0\0\0\0†ÿ‹[SO1\0\0´\0\0\0ÿ\0\0\0\0†\0‹[SO\0\0\0\0"¥"\\ #', '##0;\\-', '0000-00-00', '##0;[Red]\\-"¥"\\ #', '##0"\0\0\0\0"¥"\\ #', '##0', '##0.00''\0\0"\0\0"¥"\\ ', '##0', '##0.00;\0*\06\0\0_-"¥"\\ * #', '##0', '##0', '\0)\0''\0\0_-* #', '##0_-;\\-* #', '##0_-;_-* "-"_-', '\0>\0\0_-"¥"\\ * #', '##0.00_-;\\-"¥"\\ * #', '##0.00_-;_-"¥"\\ * "-', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(507, 0, '\0\0\0õÿ \0\0´\0\0\0\0\0\0\0ª à\0\0\0\0\0õÿ \0\0Ô\0a\0\0±\0\0À à\0\0\0', '\0õÿ \0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(508, 0, '\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0}-\0}\0\0\0\0\0\0\0\0\0\0\0\0\r\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(509, 0, '\0\0\0\0\0ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0;\0\0\0\0\r\0\0\0\0\0???ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0???ÿ        \0\0\0\0\0???ÿ        	\0\0\0\0\0???ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(510, 0, '\0\0\0\0\0???ÿrl€B2——}‘\0}\0\0\0\0\0\0\0\0\0\0\0\03\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0òòòÿ??_-;_-@\0\0\0\0\0ÿ        \0\0\0\0\0ÿ        	\0\0\0\0\0ÿË(”@W€', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(511, 0, '\0\0\0\0\0ÿrl€B2——}A\0}\0\0\0\0\0\0\0\0\0\0\0\07\0\0\0\0\r\0\0\0\0\0ú}\0ÿ.00\\)* "\0\0\0\0\0\0\0ÿ€ÿ??_-;_-@}‘\0}\0\0\0\0\0\0\0\0\0\0\0\04\0\0\0\0\r\0\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0¥¥¥ÿ?', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(512, 0, '\0\0\0\0\0???ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\06\0\0\0\0\r\0\0\0\0\0ÿ\0\0ÿ.00\\)* "\0\0}‘\0}\0\0\0\0\0\0\0\0\0\0\0\0=\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0ÿÿÌÿ??_-;_-@\0\0\0\0\0²²²ÿ ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(513, 0, '\0\0\0\0\0²²²ÿrl€B2——}-\0}\0\0\0\0\0\0\0\0\0\0\0\05\0\0\0\0\r\0\0\0\0\0ÿ.00\\)* "\0\0}U\0}\0\0\0\0\0\0\0\0\0\0\0\00\0\0\0\0\r\0\0\0\0\0\0\0\0.00\\)* "\0\0\0\0\0\0\0\0\0\0??_-;_-@\0\0\0\0\0\0\0\0 ', '\0\0\0\0\r', '0000-00-00', '\0\0*N''`r‚4\0’;\0’\0\0\0\0\0\0\0\0\0\0)ÿ\0*N''`r‚4\0\0\0\0\0\0\0\0ÿÀ\0ÿ\0\0\0\0\0ÿÿÿÿ%\0\0“\r\0-\0\0*N''`r‚5\0’;\0’\0\0\0\0\0\0\0\0\0\0-ÿ\0*N''`r‚5\0\0\0\0\0\0\0\0DrÄÿ\0\0\0\0\0ÿÿÿÿ', 'g’1\0’\0\0\0\0\0\0\0\0\0\05ÿ\0ã‰Ê‘''`‡e', 'g\0\0', 'g’/\0’\0\0\0\0\0\0\0\0\0\0', 'g\0\0', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(514, 0, 'ÿ\0èlÊ‘\0\0\0\0\0ÿ\0\0ÿÿÌÿ\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0\0\0ÿ\0\0²²²ÿ\0	\0\0ÿ\0\0²²²ÿ\0ŽX\0Ž\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0T\0a\0b\0l\0e\0S\0t\0y\0l\0e\0M\0e\0d\0i\0u\0m\09\0P\0i\0v\0o\0t\0S\0t', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(515, 0, '\04dju$i-WÉ~~‚É~~‚ü\0C\0¬0ØY\0ë~t\0àoœtü\0\0@þ€øig\09p5‚Â”\0igÂ”ì—\0cm	\0cm~‚ 2ü\0Â”COÎ˜ \0€ø\0ì—$U‡t€øjuì—$U‡t€ø¶o˜"k\0ju‡tü\0juîoA%\0É', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(516, 0, '\0>þ', 'ƒæ0”mœ', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(517, 0, 'ÉúÆQWéÏÍKv¯Ub OXé#&½®¯¯ÊÍ1`R¢¦Též9<“l#¤Ü$©´>ŽÀr	`·Ð¡¹-Š;c=1g<yèº|Âv«çƒ<ŸHD®Õã©oŠª4„08', ' fªš³º', '0000-00-00', 'CâÏóHF‹ùeæ3Ñ¾mÅÆÛÝ(ëÈgãÅìO\0«ÿ‰þÎ4óßÖ_\0\0\0ÿÿ\0PK\0\0\0\0\0!\0¥Ö§çÀ\0\0\06\0\0\0\0\0_rels/.rels„ÏjÃ0‡ï…½ƒÑ}QÒÃ%v/¥C/£}\0á(h"ÛëÛOÇ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(518, 0, '»„¤ï÷©=þ®‹ùá”ç šªÃâC?Ëháv=¿‚É…¤§%[xp†£{Ûµ_¼PÑ£<Í1¥H¶0•ˆÙO¼R®BdÑÉÒJEÛ4b$§‘q_×˜žà6LÓõR×7`®¨Éÿ³Ã0ÌžOÁ¯', 'åEn7”', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(519, 0, 'Ã @á}¡wÙ7c»(Eb²Ë®»ö\0Cœ\ZAÇ ÒŸÛ×åãƒ7ÎßÕ›K\rY', 'œ\rŠeÍ', '0000-00-00', 'láÇæéxÉ´ßIÈsQ}#Õ…­µÝ Öµ+Õ!ï', 'Ý^¹$j=‹GWèÓ÷)âEë+&', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(520, 0, '8ý\0\0ÿÿ\0PK\0\0\0\0\0!\0w‹}–Â\0\0\Z\0\0\0\0\0theme/theme/theme1.xmlìYÏ‹ÛF¾ú?Ýÿ’d{‰7Ø²m³›„ØIÉql­ÉŽ4F\ZïÆ„@IŽ…BiZr)”^z(m	´Ðô¯Ù4%M!ÿBßŒdyÆw“%‡´d\r‹4úÞ›o', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(521, 0, 'SD<#\Z÷…o¬™Hìø°', 'É"ñil', '0000-00-00', '„µÉ(èÌñ0~3‹A€ˆfðmpÝå¼¼@Ô„kc=x7bðâü–ÆµÄsN3_', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(522, 0, 'B\rxÀm³Ø€Kb.%Âƒy45OÏUÜ5„ŽLsû(ÒRÛÏ@Y‰É¥`æUŠ"Ž¦8ÂÜ×Ø!Æ†ÕÝ$D‹ëÅ', 'anÝ$V', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(523, 0, 'ËÝK¶Sù´¼$ lß„„2™N¢j Q[Š Égsš„\\Ù[aÑ0°¨÷ËTm°\0jyV`ÿdÁ®«i»˜€<T!ŠÇ"Oiª—Ù•É|›™ÞL­`3±¬€U¦‚ëÖå‰Õ¥¥ö\Z™ÖH(å¦“‘‘=', '	ÐgÕ)', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(524, 0, 'ãUi~v°°dsHw?[C:¯!(1·V“^\0•ÓhŽ	¼ÑÌ…lUk)“]õ•¢¬¡tÑY€²Ž¢Šy', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(525, 0, '—RžÓ‘gy”³lÍP%$Y#NEƒUƒªuÓ¼k¤¶vÝÓDäÑ\\õLMUD×4«˜6Ã²\r¬ÅòlM^aµ1hšÚáSé^—ÜÆRëÖö	y—€€çñ3tÝ×h', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(526, 0, 'µÕd\Z5ÁxS†…fg£zïX.ðj¯Ó$Õ÷–n×â–÷ãt0x¦ÎvëUC“å¾RFZ~ûP¿N°á-¼žSžÈTÂ·‡Á†¨/÷$©lÀ-r›g·Yó˜4í;%·åø×/”ên·àTR¡î¶ª…–ëVË]·\\ê´+w¡±ð ', '»éw—¼', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(527, 0, 'Â0„÷‚wooÓº‘&ÝˆÐ­Ô„ä5\r6?$Qìí\r', '.‡a¾™', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(528, 0, '\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(529, 0, '\0\0\0	\0\0\0gfÌ\0€\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ú:\0\0œ<\0\0\r\0\0\0\0\0d\0\0\0\0\0\0\0\0\0\0ü©ñÒMbP?_\0\0\0*\0\0\0\0+\0\0\0\0‚\0\0\0€\0\0\0\0\0\0\0\0\0\0%\0\0\0', '\0\0Á', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(530, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '\0\0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(531, 0, '\0\0\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(532, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(533, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(534, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(535, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(536, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(537, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(538, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(539, 0, '\0\0\0	\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(540, 0, '\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(541, 0, '\0\0	\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(542, 0, '\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(543, 0, '\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(544, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(545, 0, '\0\0\0\r\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', '');
INSERT INTO `volunteers` (`volunteer_id`, `subject_id`, `v_name`, `gender`, `bday`, `nation`, `native`, `married`, `blood_type`, `surgery`, `fam_history`, `smoker`, `drinker`, `allergies`, `height`, `weight`, `bmi`, `med_history`, `mobile`, `remarks`, `status`, `date_status`, `status_updated_by`, `date_added`, `added_by`, `date_update`, `updated_by`, `id_card_num`, `id_card_path`) VALUES
(546, 0, '\0\0\0\0\0\r\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(547, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(548, 0, '\0\0\0\0\0\0\0\0ý\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(549, 0, '\0\0\0\0\0\0\0\0½\0*\0\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0½\0B\0\0\0\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0\0\0ð?\0×\0\0Š\0\0\0î\0>\0¶\0\0\0\0@', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(550, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(551, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ÕÍÕœ.“—\0+', 'ù®0\0\0\0', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(552, 0, '\0\0ÿÿÿÿ \0\0\0\0\0À\0\0\0\0\0\0F!\0\0\0Microsoft Excel 97 - 2004 ¹¤×÷±í\0\0\0\0Biff8\0\0\0\0Excel.Sheet.8\0ô9²q\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(553, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0à…ŸòùOh«‘\0+''³Ù0\0\0\0(–\0\0\0\0\0\0\0\0H\0\0\0\0\0\0P\0\0\0\0\0\0p\0\0\0\0\0\0\0\0\0\0\0\0´\0\0\0\r\0\0\0À\0\0\0\0\0\0Ì\0\0\0\0\0\0Ô\0\0\0\0\0\0''\0\0\0\0\0\0\0\0Microsoft Office ÓÃ»', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(554, 0, '\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0€\0\0\0J\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0Q\0\0\0x”\0\0\0\0\0\0\0\0\0\0\0\0\0I\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0€\0\0\0J\0\0\0P\0\0\0(\0\0\0x\0\0\0\0”\0\0\0\0\0\0 \0Ì\0€\0\0\0J\0\0\0(\0\0\0€\0\0', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(555, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(556, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(557, 0, 'ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿõõõÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', ''),
(558, 0, '˜Ÿÿ‘‘‘ÿ·˜‰ÿÿÿîÿ‰¥ÒÿÍÒ±ÿ˜p‘ÿÿÿêÿ·Èàÿ½Â·ÿ±«±ÿÍ½·ÿ±½Ðÿ«¥«ÿÿò×ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿçççÿçòõÿÒÍ×ÿòîåÿåîòÿÅÍÒÿ±¦¦ÿ‰y‘ÿ«˜˜ÿÒ×ÒÿûîÜÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00', 0, '0000-00-00', 0, '0000-00-00', 0, '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bio_destroy`
--
ALTER TABLE `bio_destroy`
  ADD PRIMARY KEY (`bio_destroy_id`);

--
-- Indexes for table `bio_determine`
--
ALTER TABLE `bio_determine`
  ADD PRIMARY KEY (`determine_id`);

--
-- Indexes for table `bio_handles`
--
ALTER TABLE `bio_handles`
  ADD PRIMARY KEY (`bio_handle_id`);

--
-- Indexes for table `bio_recycle`
--
ALTER TABLE `bio_recycle`
  ADD PRIMARY KEY (`bio_recycle_id`);

--
-- Indexes for table `bio_samples`
--
ALTER TABLE `bio_samples`
  ADD PRIMARY KEY (`bio_id`);

--
-- Indexes for table `bio_save`
--
ALTER TABLE `bio_save`
  ADD PRIMARY KEY (`bio_save_id`);

--
-- Indexes for table `bio_transport`
--
ALTER TABLE `bio_transport`
  ADD PRIMARY KEY (`transport_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `drugs`
--
ALTER TABLE `drugs`
  ADD PRIMARY KEY (`drug_id`);

--
-- Indexes for table `drug_records`
--
ALTER TABLE `drug_records`
  ADD PRIMARY KEY (`drug_record_id`);

--
-- Indexes for table `drug_record_receipts`
--
ALTER TABLE `drug_record_receipts`
  ADD PRIMARY KEY (`receipt_id`);

--
-- Indexes for table `inspections`
--
ALTER TABLE `inspections`
  ADD PRIMARY KEY (`inspection_id`);

--
-- Indexes for table `inspection_uploads`
--
ALTER TABLE `inspection_uploads`
  ADD PRIMARY KEY (`inspection_upload_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`word_id`),
  ADD UNIQUE KEY `phrase` (`phrase`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `mc_info`
--
ALTER TABLE `mc_info`
  ADD PRIMARY KEY (`mc_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`position_id`);

--
-- Indexes for table `professional_group`
--
ALTER TABLE `professional_group`
  ADD PRIMARY KEY (`pgroup_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `projects_pre`
--
ALTER TABLE `projects_pre`
  ADD PRIMARY KEY (`pre_id`);

--
-- Indexes for table `project_remarks`
--
ALTER TABLE `project_remarks`
  ADD PRIMARY KEY (`remarks_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `side_effect`
--
ALTER TABLE `side_effect`
  ADD PRIMARY KEY (`side_effect_id`);

--
-- Indexes for table `side_effects_docs`
--
ALTER TABLE `side_effects_docs`
  ADD PRIMARY KEY (`side_effect_doc_id`);

--
-- Indexes for table `side_effect_receipts`
--
ALTER TABLE `side_effect_receipts`
  ADD PRIMARY KEY (`receipt_id`);

--
-- Indexes for table `stage_summary`
--
ALTER TABLE `stage_summary`
  ADD PRIMARY KEY (`stage_summary_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `subject_status`
--
ALTER TABLE `subject_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`training_id`);

--
-- Indexes for table `trainings_participant`
--
ALTER TABLE `trainings_participant`
  ADD PRIMARY KEY (`participant_id`);

--
-- Indexes for table `training_material`
--
ALTER TABLE `training_material`
  ADD PRIMARY KEY (`material_id`);

--
-- Indexes for table `trials`
--
ALTER TABLE `trials`
  ADD PRIMARY KEY (`trial_id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`up_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `mobile_num` (`mobile_num`);

--
-- Indexes for table `volunteers`
--
ALTER TABLE `volunteers`
  ADD PRIMARY KEY (`volunteer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bio_destroy`
--
ALTER TABLE `bio_destroy`
  MODIFY `bio_destroy_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bio_determine`
--
ALTER TABLE `bio_determine`
  MODIFY `determine_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bio_handles`
--
ALTER TABLE `bio_handles`
  MODIFY `bio_handle_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `bio_recycle`
--
ALTER TABLE `bio_recycle`
  MODIFY `bio_recycle_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bio_samples`
--
ALTER TABLE `bio_samples`
  MODIFY `bio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `bio_save`
--
ALTER TABLE `bio_save`
  MODIFY `bio_save_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `bio_transport`
--
ALTER TABLE `bio_transport`
  MODIFY `transport_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `drugs`
--
ALTER TABLE `drugs`
  MODIFY `drug_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `drug_records`
--
ALTER TABLE `drug_records`
  MODIFY `drug_record_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `drug_record_receipts`
--
ALTER TABLE `drug_record_receipts`
  MODIFY `receipt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `inspections`
--
ALTER TABLE `inspections`
  MODIFY `inspection_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `inspection_uploads`
--
ALTER TABLE `inspection_uploads`
  MODIFY `inspection_upload_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `word_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=815;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;
--
-- AUTO_INCREMENT for table `mc_info`
--
ALTER TABLE `mc_info`
  MODIFY `mc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `position_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `professional_group`
--
ALTER TABLE `professional_group`
  MODIFY `pgroup_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `projects_pre`
--
ALTER TABLE `projects_pre`
  MODIFY `pre_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `project_remarks`
--
ALTER TABLE `project_remarks`
  MODIFY `remarks_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `side_effect`
--
ALTER TABLE `side_effect`
  MODIFY `side_effect_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `side_effects_docs`
--
ALTER TABLE `side_effects_docs`
  MODIFY `side_effect_doc_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `side_effect_receipts`
--
ALTER TABLE `side_effect_receipts`
  MODIFY `receipt_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stage_summary`
--
ALTER TABLE `stage_summary`
  MODIFY `stage_summary_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `status_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `subject_status`
--
ALTER TABLE `subject_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `trainings`
--
ALTER TABLE `trainings`
  MODIFY `training_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `trainings_participant`
--
ALTER TABLE `trainings_participant`
  MODIFY `participant_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `training_material`
--
ALTER TABLE `training_material`
  MODIFY `material_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `trials`
--
ALTER TABLE `trials`
  MODIFY `trial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `up_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `volunteers`
--
ALTER TABLE `volunteers`
  MODIFY `volunteer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=559;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
