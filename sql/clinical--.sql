-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2016 at 04:11 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clinical`
--

-- --------------------------------------------------------

--
-- Table structure for table `bio_destroy`
--

CREATE TABLE `bio_destroy` (
  `bio_destroy_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `bio_date` datetime NOT NULL,
  `destroy_address` varchar(200) NOT NULL,
  `destroy_operator` varchar(200) NOT NULL,
  `destroy_record` varchar(250) NOT NULL,
  `bio_desc` varchar(300) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `remarks` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bio_determine`
--

CREATE TABLE `bio_determine` (
  `determine_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'blood types?',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` varchar(255) NOT NULL COMMENT 'a.k.a. sample size?',
  `determine_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `determine_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `determine_record` text NOT NULL COMMENT 'file url',
  `determine_report` text NOT NULL COMMENT 'file url',
  `description` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL COMMENT 'role id of uploader',
  `remarks` text NOT NULL,
  `determine_company` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bio_handles`
--

CREATE TABLE `bio_handles` (
  `bio_handle_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'blood types?',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` varchar(255) NOT NULL COMMENT 'a.k.a. sample size?',
  `handle_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `handle_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `handle_record` text NOT NULL COMMENT 'file url',
  `description` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL COMMENT 'role id of uploader',
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bio_recycle`
--

CREATE TABLE `bio_recycle` (
  `bio_recycle_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `sending_party` varchar(200) NOT NULL,
  `sending_operator` varchar(200) NOT NULL,
  `sending_date` datetime NOT NULL,
  `shipper` varchar(200) NOT NULL,
  `shipper_operator` varchar(200) NOT NULL,
  `shipper_date` datetime NOT NULL,
  `recycler` varchar(200) NOT NULL,
  `recycler_operator` varchar(200) NOT NULL,
  `recycler_date` datetime NOT NULL,
  `recycle_record` varchar(220) NOT NULL,
  `bio_desc` varchar(300) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `remarks` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bio_samples`
--

CREATE TABLE `bio_samples` (
  `bio_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'blood types?',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` varchar(255) NOT NULL COMMENT 'a.k.a. sample size?',
  `collect_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `collect_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `collect_record` text NOT NULL COMMENT 'file url',
  `process_record` text NOT NULL COMMENT 'file url',
  `description` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL COMMENT 'role id of uploader',
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bio_save`
--

CREATE TABLE `bio_save` (
  `bio_save_id` int(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `quantity` varchar(225) NOT NULL,
  `collect_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `collect_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `address` varchar(225) NOT NULL,
  `temp` varchar(150) NOT NULL,
  `humidity` varchar(150) NOT NULL,
  `record` varchar(225) NOT NULL,
  `bio_desc` varchar(300) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_id` int(11) NOT NULL,
  `remarks` varchar(200) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bio_transport`
--

CREATE TABLE `bio_transport` (
  `transport_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'blood types?',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` varchar(255) NOT NULL COMMENT 'a.k.a. sample size?',
  `transport_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `transport_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `process_record` text NOT NULL COMMENT 'file url',
  `description` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL COMMENT 'role id of uploader',
  `remarks` text NOT NULL,
  `transport_company` varchar(255) NOT NULL,
  `transport_temp` varchar(255) NOT NULL,
  `transport_humidity` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `dept_id` int(11) NOT NULL,
  `dept_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`dept_id`, `dept_name`) VALUES
(1, 'Yodo'),
(2, 'Abatz'),
(3, 'Podcat'),
(4, 'Jaxspan'),
(5, 'Mita'),
(6, 'Skynoodle'),
(7, 'Nlounge'),
(8, 'Podcat'),
(9, 'Livepath'),
(10, 'Skibox'),
(11, 'Photobug'),
(12, 'Chatterpoint'),
(13, 'Feedfish'),
(14, 'Zooveo'),
(15, 'Browsetype'),
(16, 'Ozu'),
(17, 'Dynabox'),
(18, 'Eamia'),
(19, 'Skiptube'),
(20, 'Lajo');

-- --------------------------------------------------------

--
-- Table structure for table `drugs`
--

CREATE TABLE `drugs` (
  `drug_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `trade_name` varchar(250) NOT NULL,
  `chemical_name` varchar(250) NOT NULL,
  `english_name` varchar(250) NOT NULL,
  `drug_type` varchar(15) NOT NULL,
  `lot_number` varchar(250) NOT NULL,
  `specification` varchar(250) NOT NULL,
  `validity_from` date NOT NULL,
  `validity_to` date NOT NULL,
  `inspection` varchar(250) NOT NULL,
  `factory` varchar(250) NOT NULL,
  `provider` varchar(250) NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drug_records`
--

CREATE TABLE `drug_records` (
  `drug_record_id` int(11) NOT NULL,
  `drug_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit` varchar(50) NOT NULL,
  `record_type` varchar(15) NOT NULL,
  `date` date NOT NULL,
  `actor1` varchar(150) NOT NULL,
  `position1` varchar(20) NOT NULL,
  `actor2` varchar(150) NOT NULL,
  `position2` varchar(20) NOT NULL,
  `receipt` varchar(350) NOT NULL,
  `description` text NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` date NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drug_record_receipts`
--

CREATE TABLE `drug_record_receipts` (
  `receipt_id` int(11) NOT NULL,
  `drug_record_id` int(11) NOT NULL,
  `receipt_file` text NOT NULL,
  `receipt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inspections`
--

CREATE TABLE `inspections` (
  `inspection_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `report_date` date NOT NULL,
  `role` varchar(17) NOT NULL,
  `type` varchar(17) NOT NULL,
  `report_description` text NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inspection_uploads`
--

CREATE TABLE `inspection_uploads` (
  `inspection_upload_id` int(11) NOT NULL,
  `inspection_id` int(11) NOT NULL,
  `inspection_file` text NOT NULL,
  `inspection` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `word_id` int(5) NOT NULL,
  `phrase` varchar(150) NOT NULL,
  `english` varchar(150) NOT NULL,
  `chinese` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`word_id`, `phrase`, `english`, `chinese`) VALUES
(1, 'sitename', 'Clinical Trial Management System', '20020,24202,30740,31350,31649,29702,31995,32479'),
(2, 'chinese', 'chinese', '20013,25991'),
(4, 'side_effects', 'Side effects', '19981,33391,20107,20214,47,21453,24212'),
(7, 'search', 'Search', '25628,32034'),
(8, 'research_category', 'Research category', '30740,31350,31867,21035'),
(9, 'status', 'status', '29366,24577'),
(10, 'all', 'All', '20840,37096'),
(11, 'stage_1', 'Stage I', '73,26399'),
(12, 'stage_2', 'Stage II', '73,73,26399'),
(13, 'stage_3', 'Stage III', '73,73,73,26399'),
(14, 'stage_4', 'Stage IV', '73,86,26399'),
(15, 'be_trial', 'BE Trial', '66,69,35797,39564'),
(16, 'clinical_verifications', 'Clinical verifications', '20020,24202,39564,35777'),
(17, 'other', 'Other', '20854,20182'),
(18, 'apply', 'Apply', '31435,39033,30003,35831'),
(19, 'initiation', 'Initiation', '31435,39033,36890,36807'),
(20, 'ethics_pass', 'Ethics pass', '20262,29702,36890,36807'),
(21, 'trial_begin', 'Trial begin', '35797,39564,24320,22987'),
(24, 'number', 'NO.', '24207,21495'),
(25, 'project_number', 'Project number', '39033,30446,32534,21495'),
(26, 'project_name', 'Project name', '39033,30446,21517,31216'),
(27, 'applicant', 'Applicant', '30003,21150,32773'),
(28, 'current_progress', 'Current progress', '24403,21069,20107,39033'),
(32, 'date_created', 'Date created', '24403,21069,20107,39033,26102,38388'),
(33, 'operation', 'Operation', '25805,20316'),
(57, 'new_project', 'New Project', '26032,30340,39033,30446'),
(58, 'project_detail', 'Project detail', '39033,30446,35814,24773'),
(59, 'lot_number', 'Lot number', '20020,24202,35797,39564,25209,25991,25991,20214,21495'),
(62, 'trial_category', 'Trial category', '30740,31350,31867,21035'),
(63, 'indications', 'Indications', '36866,24212,30151'),
(64, 'risk_category', 'Risk category', '39118,38505,31867,21035'),
(65, 'low', 'Low', '20302'),
(66, 'mid', 'Mid', '20013'),
(67, 'high', 'High', '39640'),
(68, 'departments', 'Departments', '31185,23460'),
(69, 'pgroup_details', 'Professional group detials', '19987,19994,32452,35814,24773'),
(70, 'professional_group', 'Professional group', '19987,19994,32452'),
(71, 'principal_investigator', 'Principal investigator', '19987,19994,32452,80,73'),
(72, 'pgroup_contact', 'Professional group contact person', '19987,19994,32452,39033,30446,32852,31995,20154'),
(73, 'pgroup_mobile', 'Professional group mobile', '19987,19994,32452,32852,31995,25163,26426,21495'),
(74, 'pgroup_email', 'Professional group email', '19987,19994,32452,101,109,97,105,108'),
(75, 'applicant_details', 'Applicant details', '30003,21150,32773,35814,24773'),
(76, 'applicant_name', 'Applicant name', '30003,21150,32773'),
(77, 'emergency_contact', 'Emergency contact person', '30003,21150,32773,32852,31995,20154'),
(78, 'emergency_mobile', 'Emergency contact mobile', '30003,21150,32773,32852,31995,25163,26426,21495'),
(79, 'emergency_email', 'Emergency email', '30003,21150,32773,101,109,97,105,108'),
(80, 'cro_details', 'CRO details', '67,82,79,35814,24773'),
(81, 'cro_name', 'CRO', '67,82,79'),
(82, 'cro_contact', 'CRO contact person', '67,82,79,32852,31995,20154'),
(83, 'cro_mobile', 'CRO contact mobile', '67,82,79,32852,31995,25163,26426,21495'),
(84, 'other_details', 'Other details', '20854,20182,35814,24773'),
(85, 'research_objectives', 'Research objectives', '30740,31350,30446,30340'),
(86, 'subsidize_type', 'Type of subsidize', '30740,31350,36164,21161,31867,22411'),
(87, 'fully_funded', 'Fully funded', '33719,20840,39069,36164,21161'),
(88, 'partially_funded', 'Partially funded', '33719,37096,20998,36164,21161'),
(89, 'no_subsidize', 'No subsidize', '26080,36164,21161,26041'),
(90, 'remarks', 'Remarks', '22791,27880'),
(93, 'pi', 'PI', '20027,35201,30740,31350,32773'),
(94, 'mc_info', 'Multi-center information', '28155,21152,22810,20013,24515,20449,24687'),
(95, 'organization', 'Organization', '21333,20301,21517,31216'),
(96, 'person_in_charge', 'Person in charge', '36127,36131,20154'),
(98, 'lead_participate', 'Lead/Participate', '29301,22836,47,21442,19982'),
(99, 'submit', 'Submit', '25552,20132'),
(100, 'project_documents', 'Project documents', '28155,21152,39033,30446,36164,26009'),
(101, 'name', 'Name', '21517,31216'),
(105, 'version', 'Version', '29256,26412'),
(107, 'description', 'Description', '25551,36848'),
(108, 'type', 'Type', '31867,22411'),
(110, 'select_file', 'Select file', '36873,25321,25991,20214'),
(112, 'project_plan', 'Project plan', '28155,21152,39033,30446,26041,26696'),
(113, 'uploader', 'Uploader', '19978,20256,20154'),
(116, 'tfo_opinion', 'Testing Facility opinion', '35797,39564,26426,26500,24847,35265'),
(117, 'pi_opinion', 'Principal Investigator opinion', '20027,35201,30740,31350,32773,24847,35265'),
(119, 'ecc_opinion', 'Ethics Committee Chairman opinion', '20262,29702,22996,21592,20250,24847,35265'),
(120, 'role', 'Role', '35282,33394'),
(123, 'upload_report', 'Upload report', '19978,20256,25253,21578'),
(124, 'file', 'File', '25991,20214'),
(129, 'project_information', 'Project Information', '39033,30446,20449,24687'),
(130, 'pre_opinion', 'Peer review expert opinion', '21516,34892,35780,35758,19987,23478,24847,35265'),
(131, 'gender', 'Gender', '24615,21035'),
(132, 'job_title', 'Job title', '32844,31216'),
(133, 'mobile_num', 'Mobile', '30005,35805,21495,30721'),
(134, 'position', 'position', '32844,20301'),
(136, 'job_spec', 'Job specification', '24037,20316,35268,33539'),
(137, 'title', 'Title', '26631,39064'),
(138, 'add_job_spec', 'Add job pecification', '28155,21152,24037,20316,35268,33539'),
(139, 'add', 'Add', '28155,21152'),
(140, 'delete', 'Delete', '21024,38500'),
(141, 'meeting_type', 'Type of meeting', '20250,35758,31867,22411'),
(142, 'date', 'Date', '26085,26399'),
(143, 'meeting_record', 'Add a meeting', '28155,21152,20250,35758,35760,24405'),
(144, 'participants', 'Participants', '21442,20250,20154,21592'),
(145, 'place', 'Place', '22320,22336'),
(146, 'host', 'Host', '20027,25345,20154'),
(147, 'meeting_material', 'Meeting material', '20250,35758,36164,26009'),
(148, 'add_meeting_material', 'Add meeting material', '28155,21152,20250,35758,36164,26009'),
(149, 'meeting_content', 'Meeting content', '20250,35758,20869,23481'),
(150, 'meeting_summary', 'Meeting summary', '20250,35758,24635,32467'),
(151, 'bio_sample_management', 'Biological sample management', '29983,29289,26679,21697,31649,29702'),
(152, 'quantity', 'Quantity', '26679,26412,37327'),
(153, 'add_bio_record', 'Add biological sample record', '28155,21152,29983,29289,26679,26412,35760,24405'),
(155, 'whole_blood', 'Whole blood', '20840,34880'),
(156, 'blood_plasma', 'Blood plasma', '34880,27974'),
(159, 'blood_serum', 'Blood serum', '34880,28165'),
(160, 'urine', 'Urine', '23615,28082'),
(161, 'dung', 'Dung', '31914,20415'),
(162, 'saliva', 'Saliva', '21822,28082'),
(163, 'milk', 'Milk', '20083,27713'),
(164, 'collect_time', 'Collect time', '26679,26412,37319,38598,26102,38388'),
(165, 'collect_record', 'Collect record', '26679,26412,37319,38598,35760,24405'),
(166, 'process_record', 'Process record', '26679,26412,22788,29702,35760,24405'),
(167, 'state', 'State', '29366,24577'),
(168, 'case_num', 'Case number', '30149,21382,21495'),
(169, 'subjects_num', 'Subjects number', '21463,35797,32773,32534,21495'),
(170, 'abbrv', 'Abbreviation', '22995,21517,32553,20889'),
(171, 'id_card_num', 'I.D. card No.', '36523,20221,35777,21495'),
(172, 'id_card_copy', 'I.D. card Copy', '36523,20221,35777,22797,21360,20214'),
(173, 'birth_date', 'Birth date', '29983,26085'),
(174, 'nation', 'Nation', '27665,26063'),
(175, 'country', 'Country', '22269,23478'),
(176, 'province', 'Province', '30465,20221'),
(177, 'address', 'Address', '20303,22336'),
(178, 'native', 'Native place', '31821,36143'),
(179, 'sign_informed_consent', 'Sign informed consent', '26159,21542,31614,32626,30693,24773,21516,24847,20070'),
(180, 'yes', 'Yes', '26377'),
(181, 'no', 'No', '26080'),
(182, 'screen_date', 'Screen date', '31579,36873,26085,26399'),
(183, 'random_date', 'Random date', '38543,26426,26085,26399'),
(184, 'random_num', 'Random number', '38543,26426,30721'),
(188, 'accept_date', 'Accept date', '25509,21463,26085,26399'),
(189, 'reject_date', 'Reject date', '25298,32477,26085,26399'),
(190, 'drop_out_date', 'Drop out date', '21076,38500,26085,26399'),
(191, 'first_visit_date', 'First visit date', '39318,35775,26085,26399'),
(192, 'male', 'Male', '30007'),
(194, 'female', 'Female', '22899'),
(195, 'stage_summary', 'Stage summary', '38454,27573,23567,32467'),
(196, 'time_slot', 'Time slot', '26102,38388,27573'),
(197, 'enroll_amount', 'Enroll amount', '20837,32452,20363,25968'),
(198, 'quit_amount', 'Quit amount', '36864,20986,20363,25968'),
(199, 'finish_amount', 'Finish amount', '23436,25104,20363,25968'),
(201, 'sae', 'SAE', '19981,33391,21453,24212'),
(202, 'amount', 'Amount', '21457,29983,20363,25968'),
(203, 'degree', 'Degree', '20005,37325,31243,24230'),
(204, 'handling_information', 'Handling information', '22788,29702,24773,20917'),
(205, 'volunteer', 'Volunteer', '24535,24895,32773'),
(206, 'married', 'Married', '23130,21542'),
(207, 'blood_type', 'Blood type', '34880,22411'),
(208, 'had_surgery', 'Had surgery', '26159,21542,20570,36807,25163,26415'),
(209, 'family_history', 'Family history', '23478,26063,21490'),
(210, 'heavy_smoking', 'Heavy smoking', '21980,28895'),
(211, 'heavy_drinking', 'Heavy drinking', '21980,37202'),
(212, 'allergies', 'Allergies', '36807,25935,21490'),
(213, 'height', 'Height', '36523,39640'),
(214, 'weight', 'Weight', '20307,37325'),
(215, 'bmi', 'BMI', '20307,37325,25351,25968'),
(216, 'medical_history', 'Medical history', '26082,24448,30149,21490'),
(217, 'progress', 'Progress', '39033,30446,36827,23637'),
(218, 'material', 'Material', '39033,30446,36164,26009'),
(220, 'timeline', 'Timeline', '26102,38388,32447'),
(221, 'no_data', 'No data available', '26080,27861,33719,21462,25968,25454'),
(223, 'add_remarks', 'Add remarks', '28155,21152,22791,27880'),
(224, 'action', 'Action', '25805,20316'),
(225, 'member', 'Member', '39033,30446,25104,21592'),
(226, 'training_meeting', 'Training/Meeting', '22521,35757,47,20250,35758'),
(227, 'drug_management', 'Drug management', '33647,21697,31649,29702'),
(228, 'subjects', 'Subjects', '21463,35797,32773'),
(229, 'report_type', 'Report type', '25253,21578,31867,22411'),
(230, 'report_time', 'Report time', '25253,21578,26102,38388'),
(231, 'hospital', 'Hospital', '21307,30103,26426,26500,21450,19987,19994,21517,31216'),
(232, 'department', 'Department', '30003,25253,21333,20301,21517,31216'),
(233, 'drug_name', 'Drug name', '35797,39564,29992,33647,21517,31216'),
(234, 'chinese_name', 'Chinese name', '20013,25991,21517,31216'),
(235, 'english_name', 'English name', '33521,25991,21517,31216'),
(237, 'drug_information', 'Drug information', '33647,21697,27880,20876,20998,31867,21450,21058,22411'),
(238, 'drug_type', 'Drug type', '33647,21697,20998,31867'),
(239, 'form_of_drug', 'The form of drug', '21058,22411'),
(240, 'register_type', 'Register type', '27880,20876,20998,31867'),
(241, 'clinical_trial', 'Clinical trial', '20020,24202,30740,31350'),
(242, 'subjects_situation', 'Subjects situation', '21463,35797,32773,22522,26412,24773,20917'),
(243, 'initials', 'Initials', '22995,21517,32553,20889'),
(244, 'complications', 'Complications', '21512,24182,30142,30149,21450,27835,30103'),
(245, 'sae_diagnosis', 'SAE medical diagnosis', '83,65,69,30340,21307,23398,35786,26029'),
(246, 'drug_measure', 'Drug measure', '23545,35797,39564,29992,33647,37319,21462,30340,25514,26045'),
(247, 'sae_situation', 'SAE situation', '83,65,69,24773,20917'),
(248, 'sae_drug_relation', 'Relation of SAE and drug', '83,65,69,19982,35797,39564,33647,30340,20851,31995'),
(249, 'saevest', 'SAEvest', '83,65,69,24402,36716'),
(250, 'unblinding_situation', 'Unblinding situation', '30772,30450,24773,20917'),
(251, 'report_internal', 'Report internal', '22269,20869,83,65,69,25253,36947,24773,20917'),
(252, 'report_external', 'Report external', '22269,22806,83,65,69,25253,36947,24773,20917'),
(253, 'handle_detail', 'Handle detail', '83,65,69,21457,29983,21450,22788,29702,30340,35814,24773'),
(256, 'reporter_unit', 'Reporter unit', '25253,21578,21333,20301,21517,31216'),
(257, 'reporter_position', 'Reporter position', '25253,21578,20154,32844,21153'),
(258, 'die', 'Die', '27515,20129'),
(259, 'be_in_hospital', 'Be in hospital', '23548,33268,20303,38498'),
(260, 'disability', 'Disability', '24310,38271,20303,38498,26102,38388'),
(261, 'dysfunction', 'Dysfunction', '20260,27531'),
(262, 'deformity', 'Deformity', '21151,33021,38556,30861'),
(263, 'life_threatening', 'Life threatening', '21361,21450,29983,21629'),
(264, 'severe', 'Severe', '37325,24230'),
(265, 'continuantur_remedia', 'Continuantur remedia', '32487,32493,29992,33647'),
(266, 'reduce_the_dosage', 'Reduce the dosage', '20943,23569,21058,37327'),
(267, 'stop_then_go', 'Stop then go', '33647,29289,26242,20572,21518,21448,24674,22797'),
(269, 'no_detail', 'No detail', '19981,35814'),
(270, 'no_blinding', 'No blinding', '19981,35774,30450'),
(272, 'did_not_unblinding', 'Did not unblinding', '26410,30772,30450'),
(273, 'unblinding', 'Unblinding', '24050,30772,30450'),
(274, 'transference_cure', 'Transference cure', '30151,29366,28040,22833'),
(275, 'symptoms_last', 'Symptoms last', '30151,29366,25345,32493'),
(276, 'certainty', 'Certainty', '32943,23450,26377,20851'),
(277, 'certainty_unconcerned', 'Certainty unconcerned', '32943,23450,26080,20851'),
(278, 'not_determinable', 'Not determinable', '26080,27861,21028,26029'),
(279, 'maybe_concern', 'Maybe concern', '21487,33021,26377,20851'),
(282, 'be_unconcerned', 'Be unconcerned', '21487,33021,26080,20851'),
(283, 'first_report', 'First report', '39318,27425,25253,21578'),
(284, 'follow_up_report', 'Follow-up report', '38543,35775,25253,21578'),
(285, 'sum_up_report', 'Sum-up report', '24635,32467,25253,21578'),
(286, 'traditional_chinese_medicine', 'Traditional Chinese medicine', '20013,33647'),
(287, 'chemistry_drug', 'Chemistry drug', '21270,23398,33647'),
(288, 'cure_medicine', 'Cure medicine', '27835,30103,29992,29983,29289,21046,21697'),
(290, 'prevent_medicine', 'Prevent medicine', '39044,38450,29992,29983,29289,21046,21697'),
(291, 'project', 'Project', '39033,30446'),
(292, 'hospital_mobile', 'Hospital mobile', '21307,38498,30005,35805'),
(293, 'department_mobile', 'Department mobile', '31185,23460,30005,35805'),
(294, 'subject_library', 'Subject library', '21463,35797,32773,24211'),
(463, 'add_member', ' Add member', '28155,21152,25104,21592'),
(464, 'tfo', 'Testing Facility Office', '35797,39564,26426,26500'),
(465, 'pre', 'Peer review expert', '21516,34892,35780,35758,19987,23478'),
(466, 'ethics_committee', 'Ethics Committee', '20262,29702,22996,21592,20250'),
(467, 'member_list', 'member list', '25104,21592,21015,34920'),
(468, 'tfo_list', 'Testing Facility Office list', '35797,39564,26426,26500,25104,21592,21015,34920'),
(469, 'applicant_list', 'Applicant list', '30003,21150,32773,21015,34920'),
(470, 'company', 'Company', '21333,20301'),
(471, 'email', 'Email', '30005,23376,37038,31665'),
(472, 'password', 'Password', '23494,30721'),
(473, 'con_pass', 'Confirm Password', '30830,35748,23494,30721'),
(474, 'cancel', 'Cancel', '21462,28040'),
(475, 'update', 'Update', '26356,26032'),
(476, 'edit', 'Edit', '32534,36753'),
(478, 'disable_account', 'Are you sure you want to disable account', '20320,30830,35748,35201,20923,32467,35813,24080,21495,21527'),
(480, 'system_message', 'System Message', '31995,32479,28040,24687'),
(481, 'add_group', 'Add Group', '28155,21152,19987,19994,32452'),
(482, 'add_applicant', 'Add Applicant', '28155,21152,25104,21592'),
(483, 'ecm', 'Ethics Committee Member', '20262,29702,22996,21592,20250,25104,21592'),
(484, 'ecc', 'Ethics Committee Chairman', '20262,29702,22996,21592,20250,20027,24109'),
(485, 'blank_update', 'leave blank if not going to update', '22914,27809,26377,21487,19981,22635'),
(486, 'mobile_registered', 'Mobile Number is already registered', '25163,26426,21495,24050,23384,22312'),
(487, 'email_registered', 'Email is already registered', '37038,31665,24050,23384,22312'),
(488, 'password_not_match', 'Password does not match', '23494,30721,19981,27491,30830'),
(489, 'drug_list', 'Drug List', '33647,21697,21015,34920'),
(490, 'record_type', 'Record Type', '20986,20837,31867,21035'),
(491, 'lonely_remark', 'It looks lonely here...<br>Let\'s wait for new', '26242,26080,35760,24405'),
(492, 'specification', 'Specification', '35268,26684'),
(493, 'term_validity', 'Term of validity', '26377,25928,26399'),
(494, 'add_drug', 'Add Drug', '28155,21152,33647,21697'),
(495, 'trade_name', 'Trade Name', '21830,21697,21517'),
(496, 'chemical_name', 'Chemical Name', '21270,23398,21517'),
(498, 'tested_drug', 'Tested Drug', '21463,35797,33647'),
(499, 'control_drug', 'Control Drug', '23545,29031,33647'),
(500, 'placebo', 'Placebo', '23545,29031,33647'),
(501, 'adjuvant_drug', 'Adjuvant Drug', '36741,21161,29992,33647'),
(502, 'inspection_report', 'Inspection Report', '33647,21697,26816,39564,25253,21578,20070'),
(503, 'pharmaceutical_factory', 'Pharmaceutical factory', '29983,20135,33647,21378'),
(504, 'provider', 'Provider', '25552,20379,32773'),
(505, 'add_record', 'Add Record', '28155,21152,35760,24405'),
(506, 'id_card', 'ID Card', '36523,20221,35777'),
(507, 'gcp', 'GCP', '71,67,80,22521,35757,35777,20070'),
(508, 'diploma', 'Diploma', '27605,19994,35777'),
(509, 'deg_cert', 'Degree Certificate', '23398,20301,35777'),
(513, 'save_address', 'Save Address', '20445,23384,22320,28857'),
(514, 'save_temp', 'Save Temperature', '20445,23384,28201,24230'),
(515, 'save_humidity', 'Save Humidity', '20445,23384,28287,24230'),
(516, 'project_material', 'Project material', '39033,30446,26448,26009'),
(517, 'project_progress', 'Project progress', '39033,30446,36827,23637'),
(518, 'save_as_draft', 'Save as draft', '20445,23384,20026,33609,31295'),
(520, 'collect', 'Collect', '37319,38598,35760,24405'),
(521, 'handle', 'Handle', '22788,29702,35760,24405'),
(522, 'save', 'Save', '20445,23384,35760,24405'),
(523, 'transport', 'Transport', '36816,36755,35760,24405'),
(524, 'determine', 'Determine', '27979,23450,35760,24405'),
(525, 'recycle', 'Recycle', '22238,25910,35760,24405'),
(526, 'destroy', 'Destroy', '38144,27585,35760,24405'),
(527, 'download_template', 'Download template', '19979,36733,27169,26495'),
(528, 'error_updating_project', 'Error updating project', '26356,26032,39033,30446,26102,20986,38169'),
(529, 'project_updated_successfully', 'Project updated successfully', '39033,30446,24050,25104,21151,26356,26032'),
(530, 'operation_successful', 'Operation Successful', '25552,20132,25104,21151'),
(532, 'confirmation', 'Confirmation', '30830,35748'),
(534, 'add_project_members', 'Add Project Members', '28155,21152,39033,30446,25104,21592'),
(535, 'clinician', 'Clinician', '20020,24202,21307,29983'),
(536, 'research_nurse', 'Research Nurse', '30740,31350,25252,22763'),
(539, 'feedback', 'Feedback', '21453,39304'),
(540, 'add_documents', 'Add Documents', '28155,21152,25991,26723'),
(542, 'agree', 'Agree', '21516,24847'),
(543, 'disagree', 'Disagree', '19981,21516,24847'),
(544, 'comments', 'Comments', '27880,37322'),
(545, 'add_document_remarks', 'Add Document Remarks', '28155,21152,25991,26723,22791,27880'),
(546, 'ethics_committee_comments', 'Ethics Committee Comments', '20262,29702,22996,21592,20250,24847,35265'),
(549, 'pass', 'Pass', '23457,26680,36890,36807'),
(550, 'no_pass', 'No Pass', '23457,26680,19981,36890,36807'),
(552, 'done', 'Done', '23436,25104'),
(553, 'empty_table_data', 'Empty table data', '26242,26080,35760,24405'),
(554, 'logout', 'Log Out', '30331,20986'),
(565, 'approve_reject_investigator_revision', 'Approve or Reject Investigator Opinion', '25209,20934,25110,25298,32477,35843,26597,21592,24847,35265'),
(566, 'birthday', 'Birthday', '20986,29983,26085,26399'),
(567, 'id_num', 'ID Num', '36523,20221,35777,21495,30721'),
(568, 'state_date', 'State Date', '38543,26426,26102,38388'),
(569, 'chosen', 'Chosen', '36873,25321'),
(570, 'begin', 'Begin', '24320,22987'),
(571, 'follow_up', 'Follow Up', '36319,36827'),
(572, 'out', 'Out', '20986,26469'),
(573, 'drop', 'Drop', '19979,38477'),
(574, 'finish', 'Finish', '23436'),
(580, 'participate', 'Participate', '21442,21152'),
(582, 'upload_newer_version', 'Please upload newer version before proceeding', '35831,19978,20256,26032,30340,39033,30446,26041,26696'),
(583, 'reason', 'Reason', '21407,22240'),
(584, 'ethics_committee_secretary', 'Ethics Committee secretary', '20262,29702,22996,21592,20250,31192,20070'),
(585, 'treatment_situation', 'Treatment Situation', '83,65,69,32,24773,20917'),
(586, 'ok', 'Ok', '30830,35748'),
(587, 'lead', 'Lead', '29301,22836'),
(588, 'investigator', 'Investigator', '30740,31350,32773'),
(589, 'the', 'The', '35813'),
(590, 'are_you_sure', 'Are you sure to', '32,24744,30830,35748'),
(591, 'project_complete', 'Project complete', '39033,30446,23436,25104'),
(592, 'process', 'Process', '35797,39564,36827,34892'),
(593, 'my_projects', 'My Projects', '25105,30340,39033,30446'),
(594, 'medical_equipment', 'Medical Equipment', '21307,30103,22120,26800'),
(595, 'food', 'Food', '39135,21697'),
(596, 'stop_project', 'Are you sure you want to stop project?', '24744,30830,35748,20013,27490,35813,39033,30446,21527'),
(597, 'stop', 'Stop', '35797,39564,20013,27490'),
(598, 'trial_finish', 'Trial Finish', '35797,39564,32467,26463'),
(604, 'add_documents_comments', 'Add Documents Comments', '35831,34917,20840,36164,26009'),
(605, 'check_pending', 'Check Pending', '24453,23457,26680'),
(607, 'project_audit', 'Project Audit', '39033,30446,23457,26680'),
(610, 'choose_peer_review_expert', 'Choose Peer Review Expert', '21516,34892,35780,35758,19987,23478'),
(611, 'pre_list', 'List of Peer Review Expert', '36873,25321,21516,34892,35780,35758,19987,23478'),
(613, 'to_do', 'To Do', '24453,21150'),
(614, 'inspection', 'Inspection', '36136,37327,20445,35777'),
(615, 'inspector', 'Inspector', '30417,26597'),
(616, 'on_site_inspection', 'On-site inspection', '29616,22330,26816,26597'),
(617, 'clinician_selecting', 'Clinician selecting', '21307,29983,23457,26680'),
(618, 'chosen_subject', 'Chosen to be subject', '20837,36873,21463,35797,32773'),
(619, 'before_selecting', 'Before Selecting', '31579,36873,21069'),
(620, 'my_projects_non_a', 'Projects', '39033,30446,23457,26680'),
(622, 'stop_button', 'Stop', '20013,27490'),
(623, 'biological_sample_controller', 'Biological Sample Controller', '29983,29289,26679,21697,31649,29702,21592'),
(624, 'medicine_controller', 'Medicine Controller', '33647,21697,31649,29702,21592'),
(625, 'will_start', 'Will Start', '21551,21160,20250'),
(626, 'training', 'Training', '22521,35757'),
(627, 'agree_tfo_1', 'Agree and Submit to PI', '36164,26009,40784,20840,24050,25552,20132,80,73,23457,26680'),
(628, 'disagree_tfo_1', 'Disagree Return to Applicant', '19981,20104,25509,21463'),
(629, 'agree_pi_2', 'Agree and Submit to Peer', '24314,35758,31435,39033'),
(630, 'disagree_pi_2', 'Disagree and submit back to Applicant', '24314,35758,19981,31435,39033'),
(631, 'comments_pi_2', 'PI Comments', '21407,22240'),
(632, 'comments_tfo_1', 'TFO Comments', '35831,34917,20840,36164,26009'),
(634, 'comments_tfo_2', 'Comments TFO', '21407,22240'),
(635, 'choose_pre', 'Choose Peer Experts', '24050,36873,19987,23478'),
(637, 'upload_content_consent', 'Please upload conference content and consent before proceeding', '28155,21152,20262,29702,22996,21592,20250,20250,35758,35760,24405'),
(638, 'upload_consent_agreement', 'Please upload consent agreement before proceeding', '35831,19978,20256,31614,21452,26041,31614,23383,30422,31456,26041,26696,21450,20854,20182,36164,26009,25195,25551,20214,21040,39033,30446,36164,26009,28165,21333'),
(639, 'submit_materials', 'Submit Materials', '25552,20132,20262,29702,23457,26680'),
(640, 'ethics_pending', 'Ethics Pending', '20262,29702,23457,26680,20013'),
(641, 'agree_ecs_7', 'Agree', '23457,26680,36890,36807'),
(642, 'disagree_ecs_7', 'Disagree', '23457,26680,19981,36890,36807'),
(643, 'ecs_comments_7', 'ECS Comments', '21407,22240'),
(645, 'operation_failed', 'Operation Failed', '25805,20316,22833,36133'),
(646, 'ecm_comments', 'ECM Comments', '24847,35265'),
(647, 'version_date', 'Version date', '25552,20132,26085,26399'),
(648, 'ecc_comments', 'Ecc Comments', '21407,22240'),
(649, 'upload_approval', 'Please upload approval before proceeding', '19978,20256,21508,20010,20262,29702,22996,21592,20250,22996,21592,65292,20027,20219,31614,21517,30340,25209,20934,25991,20214,21040,39033,30446,36164,26009'),
(650, 'select', 'Select', '35831,36873,25321'),
(651, 'choose', 'Choose', '36827,20837,21463,35797,32773,24211'),
(652, 'subject_begin', 'Begin', '24320,22987,35797,39564'),
(653, 'sending_party', 'Sending party', '21457,36865,26041'),
(654, 'operator', 'Operator', '25805,20316,21592'),
(655, 'shipper', 'Shipper', '25176,36816,20154'),
(656, 'recycler', 'Recycler', '22238,25910,26426'),
(657, 'recycle_report', 'Recycle report', '22238,25910,25253,21578'),
(658, 'handle_time', 'Handle time', '22788,29702,26102,38388'),
(659, 'handle_record', 'Handle record', '21477,26564,35760,24405'),
(660, 'transport_company', 'Transport company', '36816,36755,20844,21496'),
(661, 'transport_temp', 'Transport temperature', '36816,36755,28201,24230'),
(662, 'transport_humidity', 'Transport humidity', '36816,36755,28287,24230'),
(663, 'transport_time', 'Transport time', '36816,36755,26102,38388'),
(664, 'determine_record', 'Determine record', '30830,23450,35760,24405'),
(665, 'determine_report', 'Determine report', '30830,23450,25253,21578'),
(666, 'determine_company', 'Determine company', '30830,23450,20844,21496'),
(667, 'report_file', 'Report file', '25253,21578,25991,20214');

-- --------------------------------------------------------

--
-- Table structure for table `mc_info`
--

CREATE TABLE `mc_info` (
  `mc_org` varchar(255) NOT NULL,
  `mc_person` varchar(255) NOT NULL,
  `mc_lead` varchar(255) NOT NULL,
  `mc_remarks` longtext NOT NULL,
  `mc_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='mc means `multi-center`';

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `position_id` int(5) NOT NULL,
  `description` varchar(100) NOT NULL,
  `is_professional_group` int(1) NOT NULL COMMENT '0 - not professional group, 1 - professional group role',
  `acronym` varchar(5) NOT NULL COMMENT 'short acronym for session conditions',
  `lang_phrase` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`position_id`, `description`, `is_professional_group`, `acronym`, `lang_phrase`) VALUES
(1, 'Applicant', 0, 'A', 'applicant'),
(2, 'Testing Facility OFfice', 0, 'TFO', 'tfo'),
(3, 'Principal Investigator', 1, 'PI', 'principal_investigator'),
(4, 'Investigator', 1, 'I', 'investigator'),
(5, 'Biological Sample Controller', 1, 'BGC', 'biological_sample_controller'),
(6, 'Medicine Controller', 1, 'MC', 'medicine_controller'),
(7, 'Clinician', 1, 'C', 'clinician'),
(8, 'Subjects Controller', 1, 'SC', ''),
(9, 'Research Nurse', 1, 'RN', 'research_nurse'),
(10, 'Quality Controller', 1, 'QC', ''),
(11, 'Peer Review Expert', 0, 'PRE', 'pre'),
(12, 'Ethics Committee Chairman', 0, 'ECC', 'ecc'),
(13, 'Ethics Committee Member', 0, 'ECM', 'ecm'),
(14, 'Professional Group Admin', 1, 'PGA', ''),
(15, 'Ethics Committee Secretary', 0, 'ECS', 'ethics_committee_secretary');

-- --------------------------------------------------------

--
-- Table structure for table `professional_group`
--

CREATE TABLE `professional_group` (
  `pgroup_id` int(11) NOT NULL,
  `pgroup_name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `pgroup_admin_id` tinyint(5) NOT NULL DEFAULT '0' COMMENT 'user_id of the one who administrates this pgroup'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `professional_group`
--

INSERT INTO `professional_group` (`pgroup_id`, `pgroup_name`, `date_created`, `date_modified`, `pgroup_admin_id`) VALUES
(1, 'Professional Group 1', '2016-11-04 23:47:57', '2016-11-05 00:18:29', 1),
(2, 'Professional Group 2', '2016-11-04 23:48:38', '2016-11-17 10:19:29', 12),
(3, 'Professional Group 3', '2016-11-04 23:48:38', '2016-11-10 01:43:33', 2),
(4, 'Professional Group 4', '2016-11-04 23:48:38', '2016-11-17 10:19:32', 14),
(5, 'Professional Group 5', '2016-11-04 23:48:38', '2016-11-17 10:19:33', 18);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL,
  `status_id` int(11) DEFAULT NULL,
  `lot_num` varchar(255) DEFAULT NULL,
  `project_num` varchar(255) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT 'è¯·è¾“å…¥é¡¹ç›®åç§°',
  `trial_id` int(11) DEFAULT NULL,
  `indications` varchar(255) DEFAULT NULL,
  `risk_category` int(1) DEFAULT NULL COMMENT '0 - low, 1 - mid, 2 - high',
  `dept_id` int(11) DEFAULT NULL COMMENT 'deprecated, just left it here for backup purposes',
  `dept_name` varchar(255) NOT NULL,
  `pgroup_id` int(11) DEFAULT NULL,
  `pi_id` int(11) DEFAULT NULL COMMENT 'fk from users table',
  `pgroup_contact_id` int(5) NOT NULL DEFAULT '0' COMMENT 'Anyone from the pgroup',
  `user_id` int(11) NOT NULL COMMENT 'id of applicant from users table',
  `emergency_contact` varchar(255) DEFAULT NULL,
  `emergency_mobile` varchar(255) DEFAULT NULL,
  `emergency_email` varchar(255) DEFAULT NULL,
  `cro_name` varchar(255) DEFAULT NULL,
  `cro_contact` varchar(255) DEFAULT NULL,
  `cro_mobile` varchar(255) DEFAULT NULL,
  `cro_email` varchar(255) DEFAULT NULL,
  `research_objectives` mediumtext,
  `subsidize_type` int(1) DEFAULT NULL COMMENT '0 - no subsidize, 1 - partially funded, 2 - Fully funded',
  `remarks` mediumtext,
  `clinician_id` int(5) NOT NULL,
  `bio_controller_id` int(5) NOT NULL,
  `nurse_id` int(5) NOT NULL,
  `med_controller_id` int(5) NOT NULL,
  `project_status` int(11) NOT NULL DEFAULT '0' COMMENT '0 - draft, 1 - submitted/published, 2 - rejected/back to start',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `status_id`, `lot_num`, `project_num`, `project_name`, `trial_id`, `indications`, `risk_category`, `dept_id`, `dept_name`, `pgroup_id`, `pi_id`, `pgroup_contact_id`, `user_id`, `emergency_contact`, `emergency_mobile`, `emergency_email`, `cro_name`, `cro_contact`, `cro_mobile`, `cro_email`, `research_objectives`, `subsidize_type`, `remarks`, `clinician_id`, `bio_controller_id`, `nurse_id`, `med_controller_id`, `project_status`, `date_created`, `date_modified`) VALUES
(1, 2, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', 8, 11, 9, 10, 1, '2016-12-23 02:36:26', '2016-12-23 02:47:09');

-- --------------------------------------------------------

--
-- Table structure for table `projects_pre`
--

CREATE TABLE `projects_pre` (
  `pre_id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL COMMENT 'role id of PRE',
  `reviewer_id` int(5) NOT NULL COMMENT 'one who saved',
  `project_id` int(5) NOT NULL COMMENT 'project_id',
  `already_uploaded` int(1) NOT NULL DEFAULT '0' COMMENT 'if PRE uploaded'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `project_remarks`
--

CREATE TABLE `project_remarks` (
  `remarks_id` int(5) NOT NULL,
  `remarks` varchar(300) NOT NULL,
  `status_id` int(5) NOT NULL COMMENT 'correlate with status table to track when and who inputted the comment',
  `project_id` int(5) NOT NULL COMMENT 'project_id from projects table',
  `remark_type` int(11) NOT NULL DEFAULT '0' COMMENT '0 - rejected remarks, 1 - add document remarks, 2 - ECM remarks'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'can duplicate',
  `position_id` int(11) NOT NULL,
  `pgroup_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `user_id`, `position_id`, `pgroup_id`) VALUES
(1, 2, 1, 0),
(2, 48, 3, 1),
(3, 49, 2, 0),
(4, 50, 12, 0),
(5, 51, 11, 0),
(6, 52, 4, 1),
(7, 55, 13, 0),
(8, 56, 7, 1),
(9, 57, 9, 1),
(10, 58, 6, 1),
(11, 59, 5, 1),
(13, 61, 1, 0),
(14, 62, 2, 0),
(18, 62, 12, 0),
(19, 62, 11, 0),
(20, 63, 15, 0);

-- --------------------------------------------------------

--
-- Table structure for table `side_effect`
--

CREATE TABLE `side_effect` (
  `side_effect_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL COMMENT 'the id of the subject with side effects',
  `lot_num` varchar(50) NOT NULL,
  `num` varchar(50) NOT NULL,
  `report_type` varchar(50) NOT NULL COMMENT '0 - First Report, 1 - Follow-up Report, 2 - Sum Up Report',
  `report_time` datetime(6) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `hospital_mobile` varchar(50) NOT NULL,
  `dept` varchar(150) NOT NULL,
  `dept_mobile` varchar(150) NOT NULL,
  `drug_chinese_name` varchar(200) NOT NULL,
  `drug_english_name` varchar(200) NOT NULL,
  `drug_type` tinyint(1) NOT NULL COMMENT '0 -Traditional Chinese medicine, 1 - Chemistry Drug, 2 - Cure medicine, 3 - Prevent medicine, 4- Other ',
  `drug_form` varchar(200) NOT NULL,
  `drug_register` varchar(200) NOT NULL,
  `clinical_trial` tinyint(1) NOT NULL COMMENT '0 - I stage,  1 - II stage,  2 - III stage,  3 - IV stage,  4 - BE Trial,  5 - Clinical verifications',
  `clinical_indications` varchar(200) NOT NULL,
  `sub_initials` varchar(100) NOT NULL,
  `sub_gender` tinyint(1) NOT NULL COMMENT '0 - male, 1 - female',
  `sub_height` varchar(50) NOT NULL,
  `sub_weight` varchar(50) NOT NULL,
  `sub_birthdate` datetime(6) NOT NULL,
  `sub_complication` varchar(150) NOT NULL COMMENT '0 - no, 1 - yes',
  `sae_diagnosis` varchar(150) NOT NULL,
  `sae_drug_measure` tinyint(1) NOT NULL COMMENT '0 - Continuantur remedia,  1 - Reduce the dosage,  2 - Stop then go',
  `sae_situation` tinyint(1) NOT NULL COMMENT '0 - Die,  1 - Be in hospital,  2 - Extend hospital,  3 - Disability,  4 - Dysfunction,  5 - Deformity,  6 - Life-threatening,  7 - Other,  8 - Severe',
  `relation_sae_drug` tinyint(1) NOT NULL COMMENT '0 - Certainty ,  1 - Maybe concern ,  2 - Be unconcerned ,  3 - Certainty uncincerned,  4 - Not Determinable',
  `sae_vest` tinyint(1) NOT NULL COMMENT '0 - Transference cure,  1 - Symptoms last',
  `sae_unbinding` tinyint(1) NOT NULL COMMENT '0 - No blinding,  1 - Did not unblinding,  2 - Unblinding',
  `sae_report_internal` tinyint(1) NOT NULL,
  `sae_report_external` tinyint(1) NOT NULL COMMENT '0 - Yes,  1 - No,  2 - No detail',
  `sae_handle_detail` varchar(250) NOT NULL,
  `report_unit` varchar(100) NOT NULL,
  `reporter_position` varchar(150) NOT NULL,
  `remark` varchar(250) NOT NULL,
  `date_created` datetime(6) NOT NULL,
  `created_by` int(5) NOT NULL,
  `date_updated` datetime(6) NOT NULL,
  `updated_by` int(5) NOT NULL,
  `clinician_remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `side_effects_docs`
--

CREATE TABLE `side_effects_docs` (
  `side_effect_doc_id` int(5) NOT NULL,
  `side_effect_id` int(5) NOT NULL,
  `name` varchar(150) NOT NULL,
  `version` int(5) NOT NULL,
  `version_date` datetime(6) NOT NULL,
  `description` varchar(100) NOT NULL,
  `up_path` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `side_effect_receipts`
--

CREATE TABLE `side_effect_receipts` (
  `receipt_id` int(11) NOT NULL,
  `side_effect_id` int(11) NOT NULL,
  `receipt_file` text NOT NULL,
  `receipt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stage_summary`
--

CREATE TABLE `stage_summary` (
  `stage_summary_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `time_slot_from` date NOT NULL,
  `time_slot_to` date NOT NULL,
  `enroll_amount` int(11) NOT NULL,
  `quit_amount` int(11) NOT NULL,
  `finish_amount` int(11) NOT NULL,
  `sae` varchar(250) NOT NULL,
  `amount` int(11) NOT NULL,
  `degree` varchar(250) NOT NULL,
  `handling_info` text NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `status_id` int(5) NOT NULL,
  `status_num` tinyint(3) NOT NULL,
  `reviewer_id` int(5) NOT NULL COMMENT 'FK TABLE: roles role_id -- user who made the action and what position when he did it (i.e. if status = 2, approve by TFO with user_id = 4)',
  `is_rejected` tinyint(1) NOT NULL,
  `status_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `project_id` int(5) NOT NULL COMMENT 'FK in TABLE: projects'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`status_id`, `status_num`, `reviewer_id`, `is_rejected`, `status_date`, `project_id`) VALUES
(1, 13, 1, 0, '2016-12-23 02:36:37', 1),
(2, 14, 6, 0, '2016-12-23 02:47:09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `subject_id` int(11) NOT NULL,
  `volunteer_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `case_num` varchar(50) NOT NULL,
  `subjects_num` varchar(50) NOT NULL,
  `abbrv` varchar(50) NOT NULL,
  `country` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `pi` varchar(50) NOT NULL,
  `is_informed_consent` tinyint(4) NOT NULL COMMENT '0 - no, 1 - yes',
  `status_id` int(11) NOT NULL COMMENT 'fk from subject_status',
  `sign_date` datetime NOT NULL,
  `random_num` int(11) NOT NULL,
  `random_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subject_status`
--

CREATE TABLE `subject_status` (
  `status_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `status` varchar(3) NOT NULL COMMENT 'S - selecting P - pass NP - no pass C - choose  (update status of subject as 1 - means, not available for other projects) B - begin FU - follow up STP - stop O - out D - drop F - Finish (once finish, update volunteer status as 0 - make available to other projects)  ',
  `status_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

CREATE TABLE `trainings` (
  `training_id` int(10) NOT NULL,
  `project_id` int(10) NOT NULL,
  `title` varchar(200) NOT NULL,
  `type` tinyint(1) DEFAULT NULL COMMENT '0 - Will start, 1 - Training, 2 - Other',
  `place` varchar(300) NOT NULL,
  `host` varchar(200) NOT NULL,
  `meeting_content` varchar(400) NOT NULL,
  `meeting_summary` varchar(400) NOT NULL,
  `is_drafted` tinyint(1) NOT NULL COMMENT '0 - submitted, 1 - draft',
  `created_by` int(5) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(5) NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trainings_participant`
--

CREATE TABLE `trainings_participant` (
  `participant_id` int(5) NOT NULL,
  `training_id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `training_material`
--

CREATE TABLE `training_material` (
  `material_id` int(10) NOT NULL,
  `training_id` int(10) NOT NULL,
  `name` varchar(150) NOT NULL,
  `version` int(2) NOT NULL,
  `version_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(300) NOT NULL,
  `up_path` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trials`
--

CREATE TABLE `trials` (
  `trial_id` int(11) NOT NULL,
  `trial_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trials`
--

INSERT INTO `trials` (`trial_id`, `trial_name`) VALUES
(1, 'I Stage'),
(2, 'II Stage'),
(3, 'III Stage'),
(4, 'IV Stage'),
(5, 'BE Trial'),
(6, 'Clinical verifications');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `up_id` int(11) NOT NULL,
  `up_name` varchar(255) NOT NULL COMMENT 'name of document',
  `role_id` int(11) NOT NULL COMMENT 'role_id of uploader',
  `up_ver` float NOT NULL COMMENT 'version of document',
  `up_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `up_desc` mediumtext NOT NULL,
  `up_path` varchar(255) NOT NULL,
  `up_type` int(11) NOT NULL COMMENT '1 - project document, 2 - project plan',
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='table for `project documents` and `project plan` or more (?)';

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(5) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 - active, 0 - disable',
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `middle_initial` varchar(5) DEFAULT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - male, 1 - female',
  `company` varchar(150) NOT NULL,
  `department` varchar(100) NOT NULL,
  `job_title` varchar(200) DEFAULT NULL,
  `birthdate` datetime NOT NULL,
  `mobile_num` varchar(30) NOT NULL,
  `password` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  `doc_id_card` varchar(100) NOT NULL,
  `doc_gcp_card` varchar(100) NOT NULL,
  `doc_diploma` varchar(100) NOT NULL,
  `doc_degree` varchar(100) NOT NULL,
  `doc_other` varchar(100) NOT NULL,
  `is_admin` tinyint(1) NOT NULL COMMENT '0 - non admin, 1 - admin',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `is_active`, `fname`, `lname`, `middle_initial`, `gender`, `company`, `department`, `job_title`, `birthdate`, `mobile_num`, `password`, `email`, `doc_id_card`, `doc_gcp_card`, `doc_diploma`, `doc_degree`, `doc_other`, `is_admin`, `date_created`, `date_updated`) VALUES
(1, 1, 'Van', 'Vanleor', '', 0, '', '', NULL, '1995-04-06 00:00:00', '12345', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'van@admin.com', '', '', '', '', '', 1, '2016-11-03 21:34:02', '2016-11-10 01:53:52'),
(2, 1, 'Enzo', 'Lorenzo', '', 0, '', '', NULL, '0000-00-00 00:00:00', '123', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'enzo@mail.com', '', '', '', '', '', 0, '2016-11-05 01:40:51', '2016-12-14 01:44:32'),
(48, 1, 'TestPI', 'TESTVAN', NULL, 0, '', '', NULL, '2016-11-09 00:00:00', '12345PI', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'PI@admin.com', '', '', '', '', '', 0, '2016-11-19 04:50:26', '2016-11-21 08:01:12'),
(49, 1, 'TestTFO', 'TESTVAN', NULL, 0, '', '', NULL, '2016-11-11 00:00:00', '12345TFO', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'TFO@admin.com', '', '', '', '', '', 0, '2016-11-19 04:52:37', '2016-12-02 18:37:46'),
(50, 1, 'TestECC', 'TESTVAN', NULL, 0, 'company', '', NULL, '2016-10-31 00:00:00', '12345ECC', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'ECC@admin.com', '', '', '', '', '', 0, '2016-11-21 08:02:05', '2016-12-02 04:07:59'),
(51, 1, 'TestPRE', 'TESTVAN', NULL, 0, 'company', '', NULL, '2016-11-15 00:00:00', '12345PRE', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'PRE@admin.com', '', '', '', '', '', 0, '2016-11-21 08:28:41', '2016-12-02 04:07:50'),
(52, 1, 'TestI', 'TESTVAN', NULL, 0, '', '', NULL, '2016-11-22 00:00:00', '12345I', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'I@admin.com', '', '', '', '', '', 0, '2016-11-21 08:33:41', '2016-11-21 10:30:31'),
(55, 1, 'TestECM', 'TESTVAN', NULL, 0, 'comp', '', NULL, '2016-11-15 00:00:00', '12345ECM', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'ECM@admin.com', '', '', '', '', '', 0, '2016-11-21 10:34:06', '2016-12-02 04:08:07'),
(56, 1, 'Clinician', 'One', NULL, 0, '', 'Dept 1', 'jobbb 4', '2016-11-22 00:00:00', '12345C', 'd033e22ae348aeb5660fc2140aec35850c4da997', '12345C@gmail.com', '', '', '', '', '', 0, '2016-11-29 07:25:49', '2016-12-04 07:28:43'),
(57, 1, 'Research nurse', 'One', NULL, 0, '', 'Dept 2', 'jobbb 3', '2016-11-22 00:00:00', '12345RN', 'd033e22ae348aeb5660fc2140aec35850c4da997', '12345N@gmail.com', '', '', '', '', '', 0, '2016-11-29 07:26:41', '2016-12-04 07:28:40'),
(58, 1, 'Medicine controller', 'One', NULL, 0, '', 'Dept 2', 'job 2', '2016-11-16 00:00:00', '12345MC', 'd033e22ae348aeb5660fc2140aec35850c4da997', '12345MC@gmail.com', '', '', '', '', '', 0, '2016-11-29 07:27:26', '2016-12-04 07:28:36'),
(59, 1, 'Biological controller', 'One', NULL, 0, 'Companys', 'Dept 1', 'title job 1', '2016-11-16 00:00:00', '12345BGC', 'd033e22ae348aeb5660fc2140aec35850c4da997', '12345BC@gmail.com', '', '', '', '', '', 0, '2016-11-29 07:27:59', '2016-12-14 01:44:27'),
(61, 1, 'testssss', 'test', NULL, 1, 'cmp', '', NULL, '0000-00-00 00:00:00', '123123', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'mai@mail.comss', '', '', '', '', '', 0, '2016-12-02 03:49:48', '2016-12-02 04:01:07'),
(62, 1, 'TestUser', 'AllPosition', NULL, 1, 'QA!', 'QA!', NULL, '0000-00-00 00:00:00', '12345ALL', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'all@test.com', '', '', '', '', '', 0, '2016-12-02 18:12:44', '2016-12-04 07:49:20'),
(63, 1, 'TestECS', 'TESTVAN', NULL, 0, 'company', '', NULL, '2016-10-31 00:00:00', '12345ECS', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'ECS@admin.com', '', '', '', '', '', 0, '2016-11-21 08:02:05', '2016-12-02 04:07:59');

-- --------------------------------------------------------

--
-- Table structure for table `volunteers`
--

CREATE TABLE `volunteers` (
  `volunteer_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `v_name` varchar(150) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `bday` date NOT NULL,
  `nation` varchar(150) NOT NULL,
  `native` varchar(150) NOT NULL,
  `married` varchar(3) NOT NULL,
  `blood_type` varchar(20) NOT NULL,
  `surgery` varchar(3) NOT NULL,
  `fam_history` text NOT NULL,
  `smoker` varchar(3) NOT NULL,
  `drinker` varchar(3) NOT NULL,
  `allergies` text NOT NULL,
  `height` varchar(15) NOT NULL,
  `weight` varchar(15) NOT NULL,
  `bmi` varchar(50) NOT NULL,
  `med_history` text NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `remarks` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '1 - is currently occupied as subject, 0 - available as subject',
  `date_status` date NOT NULL,
  `status_updated_by` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_update` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `id_card_num` varchar(255) NOT NULL,
  `id_card_path` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bio_destroy`
--
ALTER TABLE `bio_destroy`
  ADD PRIMARY KEY (`bio_destroy_id`);

--
-- Indexes for table `bio_determine`
--
ALTER TABLE `bio_determine`
  ADD PRIMARY KEY (`determine_id`);

--
-- Indexes for table `bio_handles`
--
ALTER TABLE `bio_handles`
  ADD PRIMARY KEY (`bio_handle_id`);

--
-- Indexes for table `bio_recycle`
--
ALTER TABLE `bio_recycle`
  ADD PRIMARY KEY (`bio_recycle_id`);

--
-- Indexes for table `bio_samples`
--
ALTER TABLE `bio_samples`
  ADD PRIMARY KEY (`bio_id`);

--
-- Indexes for table `bio_save`
--
ALTER TABLE `bio_save`
  ADD PRIMARY KEY (`bio_save_id`);

--
-- Indexes for table `bio_transport`
--
ALTER TABLE `bio_transport`
  ADD PRIMARY KEY (`transport_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `drugs`
--
ALTER TABLE `drugs`
  ADD PRIMARY KEY (`drug_id`);

--
-- Indexes for table `drug_records`
--
ALTER TABLE `drug_records`
  ADD PRIMARY KEY (`drug_record_id`);

--
-- Indexes for table `drug_record_receipts`
--
ALTER TABLE `drug_record_receipts`
  ADD PRIMARY KEY (`receipt_id`);

--
-- Indexes for table `inspections`
--
ALTER TABLE `inspections`
  ADD PRIMARY KEY (`inspection_id`);

--
-- Indexes for table `inspection_uploads`
--
ALTER TABLE `inspection_uploads`
  ADD PRIMARY KEY (`inspection_upload_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`word_id`),
  ADD UNIQUE KEY `phrase` (`phrase`);

--
-- Indexes for table `mc_info`
--
ALTER TABLE `mc_info`
  ADD PRIMARY KEY (`mc_id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`position_id`);

--
-- Indexes for table `professional_group`
--
ALTER TABLE `professional_group`
  ADD PRIMARY KEY (`pgroup_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `projects_pre`
--
ALTER TABLE `projects_pre`
  ADD PRIMARY KEY (`pre_id`);

--
-- Indexes for table `project_remarks`
--
ALTER TABLE `project_remarks`
  ADD PRIMARY KEY (`remarks_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `side_effect`
--
ALTER TABLE `side_effect`
  ADD PRIMARY KEY (`side_effect_id`);

--
-- Indexes for table `side_effects_docs`
--
ALTER TABLE `side_effects_docs`
  ADD PRIMARY KEY (`side_effect_doc_id`);

--
-- Indexes for table `side_effect_receipts`
--
ALTER TABLE `side_effect_receipts`
  ADD PRIMARY KEY (`receipt_id`);

--
-- Indexes for table `stage_summary`
--
ALTER TABLE `stage_summary`
  ADD PRIMARY KEY (`stage_summary_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `subject_status`
--
ALTER TABLE `subject_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`training_id`);

--
-- Indexes for table `trainings_participant`
--
ALTER TABLE `trainings_participant`
  ADD PRIMARY KEY (`participant_id`);

--
-- Indexes for table `training_material`
--
ALTER TABLE `training_material`
  ADD PRIMARY KEY (`material_id`);

--
-- Indexes for table `trials`
--
ALTER TABLE `trials`
  ADD PRIMARY KEY (`trial_id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`up_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `mobile_num` (`mobile_num`);

--
-- Indexes for table `volunteers`
--
ALTER TABLE `volunteers`
  ADD PRIMARY KEY (`volunteer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bio_destroy`
--
ALTER TABLE `bio_destroy`
  MODIFY `bio_destroy_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bio_determine`
--
ALTER TABLE `bio_determine`
  MODIFY `determine_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bio_handles`
--
ALTER TABLE `bio_handles`
  MODIFY `bio_handle_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bio_recycle`
--
ALTER TABLE `bio_recycle`
  MODIFY `bio_recycle_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bio_samples`
--
ALTER TABLE `bio_samples`
  MODIFY `bio_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bio_save`
--
ALTER TABLE `bio_save`
  MODIFY `bio_save_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bio_transport`
--
ALTER TABLE `bio_transport`
  MODIFY `transport_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `drugs`
--
ALTER TABLE `drugs`
  MODIFY `drug_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drug_records`
--
ALTER TABLE `drug_records`
  MODIFY `drug_record_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drug_record_receipts`
--
ALTER TABLE `drug_record_receipts`
  MODIFY `receipt_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inspections`
--
ALTER TABLE `inspections`
  MODIFY `inspection_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inspection_uploads`
--
ALTER TABLE `inspection_uploads`
  MODIFY `inspection_upload_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `word_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=671;
--
-- AUTO_INCREMENT for table `mc_info`
--
ALTER TABLE `mc_info`
  MODIFY `mc_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `position_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `professional_group`
--
ALTER TABLE `professional_group`
  MODIFY `pgroup_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `projects_pre`
--
ALTER TABLE `projects_pre`
  MODIFY `pre_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project_remarks`
--
ALTER TABLE `project_remarks`
  MODIFY `remarks_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `side_effect`
--
ALTER TABLE `side_effect`
  MODIFY `side_effect_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `side_effects_docs`
--
ALTER TABLE `side_effects_docs`
  MODIFY `side_effect_doc_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `side_effect_receipts`
--
ALTER TABLE `side_effect_receipts`
  MODIFY `receipt_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stage_summary`
--
ALTER TABLE `stage_summary`
  MODIFY `stage_summary_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `status_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subject_status`
--
ALTER TABLE `subject_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trainings`
--
ALTER TABLE `trainings`
  MODIFY `training_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trainings_participant`
--
ALTER TABLE `trainings_participant`
  MODIFY `participant_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `training_material`
--
ALTER TABLE `training_material`
  MODIFY `material_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trials`
--
ALTER TABLE `trials`
  MODIFY `trial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `up_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `volunteers`
--
ALTER TABLE `volunteers`
  MODIFY `volunteer_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
