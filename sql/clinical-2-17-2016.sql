-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2017 at 12:09 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `clinical`
--

-- --------------------------------------------------------

--
-- Table structure for table `bio_destroy`
--

CREATE TABLE IF NOT EXISTS `bio_destroy` (
`bio_destroy_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `unit` varchar(100) NOT NULL,
  `bio_date` datetime NOT NULL,
  `destroy_address` varchar(200) NOT NULL,
  `destroy_operator` varchar(200) NOT NULL,
  `destroy_record` varchar(250) NOT NULL,
  `bio_desc` varchar(300) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `remarks` varchar(250) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0',
  `dbl_chk_payload` varchar(21000) NOT NULL DEFAULT '{ "name":{ "is_checked":0, "comments":"" }, "quantity":{ "is_checked":0, "comments":"" }, "bio_date":{ "is_checked":0, "comments":"" }, "destroy_address":{ "is_checked":0, "comments":"" }, "destroy_operator":{ "is_checked":0, "comments":"" }, "destroy_record":{ "is_checked":0, "comments":"" }, "bio_desc":{ "is_checked":0, "comments":"" } }	' COMMENT '0 - unchecked, -1 - checked/no pass, 1 - checked/pass'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `bio_determine`
--

CREATE TABLE IF NOT EXISTS `bio_determine` (
`determine_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'blood types?',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` varchar(255) NOT NULL COMMENT 'a.k.a. sample size?',
  `unit` varchar(100) NOT NULL,
  `determine_start` datetime NOT NULL,
  `determine_end` datetime NOT NULL,
  `determine_record` text NOT NULL COMMENT 'file url',
  `determine_report` text NOT NULL COMMENT 'file url',
  `description` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL COMMENT 'role id of uploader',
  `remarks` text NOT NULL,
  `determine_company` varchar(255) NOT NULL,
  `databank_record` int(2) NOT NULL DEFAULT '0',
  `databank_report` int(2) NOT NULL DEFAULT '0',
  `dbl_chk_payload` varchar(21000) NOT NULL DEFAULT '	{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "determine_start": { "is_checked": 0, "comments": "" }, "determine_end": { "is_checked": 0, "comments": "" }, "determine_record": { "is_checked": 0, "comments": "" }, "determine_report": { "is_checked": 0, "comments": "" },"description" : { "is_checked": 0, "comments": "" } ,"determine_company" : { "is_checked": 0, "comments": "" } }	' COMMENT '0 - unchecked, -1 - checked/no pass, 1 - checked/pass'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `bio_handles`
--

CREATE TABLE IF NOT EXISTS `bio_handles` (
`bio_handle_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'blood types?',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` varchar(255) NOT NULL COMMENT 'a.k.a. sample size?',
  `unit` varchar(100) NOT NULL,
  `handle_start` datetime NOT NULL,
  `handle_end` datetime NOT NULL,
  `handle_record` text NOT NULL COMMENT 'file url',
  `description` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL COMMENT 'role id of uploader',
  `remarks` text NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0',
  `dbl_chk_payload` varchar(21000) NOT NULL DEFAULT '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "handle_start": { "is_checked": 0, "comments": "" }, "handle_end": { "is_checked": 0, "comments": "" }, "handle_record": { "is_checked": 0, "comments": "" }, "description" : { "is_checked": 0, "comments": "" } }' COMMENT '0 - unchecked, -1 - checked/no pass, 1 - checked/pass'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `bio_recycle`
--

CREATE TABLE IF NOT EXISTS `bio_recycle` (
`bio_recycle_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `unit` varchar(100) NOT NULL,
  `sending_party` varchar(200) NOT NULL,
  `sending_operator` varchar(200) NOT NULL,
  `sending_date` datetime NOT NULL,
  `shipper` varchar(200) NOT NULL,
  `shipper_operator` varchar(200) NOT NULL,
  `shipper_date` datetime NOT NULL,
  `recycler` varchar(200) NOT NULL,
  `recycler_operator` varchar(200) NOT NULL,
  `recycler_date` datetime NOT NULL,
  `recycle_record` varchar(220) NOT NULL,
  `bio_desc` varchar(300) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `remarks` varchar(300) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0',
  `dbl_chk_payload` varchar(21000) NOT NULL DEFAULT '	{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "sending_party": { "is_checked": 0, "comments": "" }, "sending_operator": { "is_checked": 0, "comments": "" }, "sending_date": { "is_checked": 0, "comments": "" }, "shipper": { "is_checked": 0, "comments": "" }, "shipper_operator" : { "is_checked": 0, "comments": "" }, "shipper_date" : { "is_checked": 0, "comments": "" }, "recycler" : { "is_checked": 0, "comments": "" }, "recycler_operator" : { "is_checked": 0, "comments": "" }, "recycler_date" : { "is_checked": 0, "comments": "" }, "recycle_record" : { "is_checked": 0, "comments": "" }, "bio_desc" : { "is_checked": 0, "comments": "" } }' COMMENT '0 - unchecked, -1 - checked/no pass, 1 - checked/pass'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `bio_samples`
--

CREATE TABLE IF NOT EXISTS `bio_samples` (
`bio_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'blood types?',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` varchar(255) NOT NULL COMMENT 'a.k.a. sample size?',
  `unit` varchar(100) NOT NULL,
  `collect_start` datetime NOT NULL,
  `collect_end` datetime NOT NULL,
  `collect_record` text NOT NULL COMMENT 'file url',
  `process_record` text NOT NULL COMMENT 'file url',
  `description` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL COMMENT 'role id of uploader',
  `remarks` text NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0',
  `dbl_chk_payload` varchar(21000) NOT NULL DEFAULT '{   "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "collect_start": { "is_checked": 0, "comments": "" }, "collect_end": { "is_checked": 0, "comments": "" }, "collect_record": { "is_checked": 0, "comments": "" }, "description" : { "is_checked": 0, "comments": "" } }' COMMENT '0 - unchecked, -1 - checked/no pass, 1 - checked/pass'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `bio_save`
--

CREATE TABLE IF NOT EXISTS `bio_save` (
`bio_save_id` int(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `quantity` varchar(225) NOT NULL,
  `unit` varchar(100) NOT NULL,
  `collect_start` datetime NOT NULL,
  `collect_end` datetime NOT NULL,
  `address` varchar(225) NOT NULL,
  `temp` varchar(150) NOT NULL,
  `humidity` varchar(150) NOT NULL,
  `record` varchar(225) NOT NULL,
  `bio_desc` varchar(300) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_id` int(11) NOT NULL,
  `remarks` varchar(200) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL,
  `dbl_chk_payload` varchar(21000) NOT NULL DEFAULT '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "collect_start": { "is_checked": 0, "comments": "" }, "collect_end": { "is_checked": 0, "comments": "" }, "record": { "is_checked": 0, "comments": "" },"bio_desc" : { "is_checked": 0, "comments": "" } ,"address" : { "is_checked": 0, "comments": "" } ,"temp" : { "is_checked": 0, "comments": "" } ,"humidity" : { "is_checked": 0, "comments": "" } }' COMMENT '0 - unchecked, -1 - checked/no pass, 1 - checked/pass'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `bio_transport`
--

CREATE TABLE IF NOT EXISTS `bio_transport` (
`transport_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'blood types?',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quantity` varchar(255) NOT NULL COMMENT 'a.k.a. sample size?',
  `unit` varchar(100) NOT NULL,
  `transport_start` datetime NOT NULL,
  `transport_end` datetime NOT NULL,
  `process_record` text NOT NULL COMMENT 'file url',
  `description` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL COMMENT 'role id of uploader',
  `remarks` text NOT NULL,
  `transport_company` varchar(255) NOT NULL,
  `transport_temp` varchar(255) NOT NULL,
  `transport_humidity` varchar(255) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0',
  `dbl_chk_payload` varchar(21000) NOT NULL DEFAULT '{ "name" : { "is_checked": 0, "comments": "" }, "quantity": { "is_checked": 0, "comments": "" }, "transport_start": { "is_checked": 0, "comments": "" }, "transport_end": { "is_checked": 0, "comments": "" }, "process_record": { "is_checked": 0, "comments": "" },"description" : { "is_checked": 0, "comments": "" } ,"transport_company" : { "is_checked": 0, "comments": "" } ,"transport_temp" : { "is_checked": 0, "comments": "" } ,"transport_humidity" : { "is_checked": 0, "comments": "" } }' COMMENT '0 - unchecked, -1 - checked/no pass, 1 - checked/pass'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
`dept_id` int(11) NOT NULL,
  `dept_name` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`dept_id`, `dept_name`) VALUES
(1, 'Yodo'),
(2, 'Abatz'),
(3, 'Podcat'),
(4, 'Jaxspan'),
(5, 'Mita'),
(6, 'Skynoodle'),
(7, 'Nlounge'),
(8, 'Podcat'),
(9, 'Livepath'),
(10, 'Skibox'),
(11, 'Photobug'),
(12, 'Chatterpoint'),
(13, 'Feedfish'),
(14, 'Zooveo'),
(15, 'Browsetype'),
(16, 'Ozu'),
(17, 'Dynabox'),
(18, 'Eamia'),
(19, 'Skiptube'),
(20, 'Lajo');

-- --------------------------------------------------------

--
-- Table structure for table `drugs`
--

CREATE TABLE IF NOT EXISTS `drugs` (
`drug_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `trade_name` varchar(250) NOT NULL,
  `chemical_name` varchar(250) NOT NULL,
  `english_name` varchar(250) NOT NULL,
  `drug_type` varchar(250) NOT NULL,
  `lot_number` int(250) NOT NULL,
  `specification` varchar(250) NOT NULL,
  `validity_from` date NOT NULL,
  `validity_to` date NOT NULL,
  `inspection` varchar(250) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0',
  `factory` varchar(250) NOT NULL,
  `provider` varchar(250) NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `drugs`
--

INSERT INTO `drugs` (`drug_id`, `project_id`, `trade_name`, `chemical_name`, `english_name`, `drug_type`, `lot_number`, `specification`, `validity_from`, `validity_to`, `inspection`, `databank`, `factory`, `provider`, `added_by`, `date_added`, `updated_by`, `date_updated`) VALUES
(1, 1, 'aåœ°æ–¹', 'aåœ°æ–¹a', 'daå‘', 'control_drug', 0, 'df', '2016-12-22', '2016-12-22', 'adfa', 0, 'dfa', 'df', 10, '2016-12-22', 0, '0000-00-00'),
(2, 3, 'é˜¿é“å¤«', 'ADf', 'f12', 'drug_combination', 0, 'aåœ°æ–¹', '2016-12-21', '2016-12-28', 'é˜¿é“å¤«a', 0, 'é˜¿é“å¤«', 'é˜¿é“å¤«', 10, '2016-12-23', 0, '0000-00-00'),
(3, 5, 'hahaha', 'df', 'adf', 'drug_combination', 0, 'adf', '2016-12-14', '2016-12-29', 'adf', 0, 'adf', 'adf', 10, '2016-12-25', 0, '0000-00-00'),
(4, 7, 'a', 'a', 'a', 'tested_drug', 0, 'a', '2016-12-01', '2016-12-02', '', 0, 'a', 'a', 10, '2016-12-28', 0, '0000-00-00'),
(5, 7, 'b', 'b', 'b', 'drug_combination', 0, 'b', '2016-12-02', '2016-12-03', '', 0, 'b', 'b', 10, '2016-12-28', 0, '0000-00-00'),
(6, 7, 'c', 'c', 'c', 'control_drug', 0, 'c', '2016-12-03', '2016-12-04', '', 0, 'c', 'c', 10, '2016-12-28', 0, '0000-00-00'),
(7, 7, '1', '1', '1', 'tested_drug', 1, '1', '2016-12-29', '2016-12-29', '', 0, '1', '1', 10, '2016-12-29', 0, '0000-00-00'),
(8, 12, 'é˜¿é“å¤«', 'é˜¿é“å¤«', 'é˜¿é“å¤«', 'tested_drug', 0, 'é˜¿é“å¤«', '2016-12-21', '2017-01-05', 'http://betaprojex.com/Clinical2/uploads/drug_records/drug_record_14830780801483078080.docx', 0, 'é˜¿é“å¤«a', 'adå‘', 10, '2016-12-30', 0, '0000-00-00'),
(9, 13, 'abc', '1', '1', 'drug_combination', 11, '1', '2016-12-21', '2016-12-31', '', 0, '1', '11', 10, '2016-12-30', 0, '0000-00-00'),
(10, 13, 'bcd', '1', '1', 'control_drug', 1, '1', '2016-12-30', '2017-01-12', '', 0, '1', '1', 10, '2016-12-30', 0, '0000-00-00'),
(11, 27, 'æ ¼åŽåœ', 'äºŒç”²åŒèƒ', 'Metformin hydrochloride', 'tested_drug', 2147483647, 'ç‰‡å‰‚', '2017-01-31', '2019-09-17', 'http://betaprojex.com/Clinical2/uploads/drug_records/drug_record_14840461781484046178.docx', 0, 'æ·±åœ³å¸‚ä¸­è”åˆ¶è¯æœ‰é™å…¬å¸', 'æ·±åœ³å¸‚ä¸­è”åˆ¶è¯æœ‰é™å…¬å¸', 10, '2017-01-10', 0, '0000-00-00'),
(12, 27, 'å•Šéƒ½æ”¾åˆ°', 'AD', 'aåœ°æ–¹', 'drug_combination', 0, 'é˜¿é“å¤«', '2017-01-25', '2017-01-26', 'http://betaprojex.com/Clinical2/uploads/drug_records/drug_record_14847285071484728507.docx', 0, 'é˜¿é“å¤«', 'aåœ°æ–¹', 10, '2017-01-18', 0, '0000-00-00'),
(13, 27, 'æ˜¯éžè§‚', 'é˜¿é“å¤«a', 'aåœ°æ–¹', 'tested_drug', 0, 'aåœ°æ–¹', '2017-01-26', '2017-01-31', 'http://betaprojex.com/Clinical2/uploads/drug_records/drug_record_14847285761484728576.docx', 0, 'é˜¿é“å¤«', 'é˜¿é“å¤«', 10, '2017-01-18', 0, '0000-00-00'),
(14, 27, 'aåœ°æ–¹', 'aåœ°æ–¹a', 'aåœ°æ–¹', 'tested_drug', 0, 'é˜¿é“å¤«', '2017-01-26', '2017-01-27', 'http://betaprojex.com/Clinical2/uploads/drug_records/drug_record_14847286481484728648.docx', 0, 'é˜¿é“å¤«a', 'aåœ°æ–¹', 10, '2017-01-18', 0, '0000-00-00'),
(15, 27, 'çš„', 'SDs', 'sçš„', 'tested_drug', 0, 'sçš„', '2017-01-19', '2017-01-26', '', 0, 'sçš„', 'sçš„', 10, '2017-01-18', 0, '0000-00-00'),
(16, 27, '11', '21', '2', 'drug_combination', 2, '12', '2017-01-25', '2017-01-31', '', 0, '21', '2', 10, '2017-01-18', 0, '0000-00-00'),
(17, 27, '1231', '123', 'Drug eng', 'control_drug', 321, '321', '2017-01-18', '2017-01-19', 'http://betaprojex.com/Clinical2/uploads/drug_records/drug_record_14847303631484730363.png', 0, '123', '123', 10, '2017-01-18', 0, '0000-00-00'),
(18, 27, '999', '9', '999', 'control_drug', 9, '9', '2017-01-18', '2017-01-18', '', 0, '9', '9', 10, '2017-01-18', 0, '0000-00-00'),
(19, 27, 'test', '1', '1', 'tested_drug', 1, '1', '2017-01-19', '2017-01-19', '', 0, '1', '1', 10, '2017-01-19', 0, '0000-00-00'),
(20, 27, 'a', 'a', 'a', 'control_drug', 0, 'a', '2017-01-19', '2017-01-19', '', 0, 'a', 'a', 10, '2017-01-19', 0, '0000-00-00'),
(21, 27, 'b', 'b', 'b', 'control_drug', 0, 'b', '2017-01-19', '2017-01-19', '', 0, 'b', 'b', 10, '2017-01-19', 0, '0000-00-00'),
(22, 111, 'Databank Drug Test', 'Clinicalangitis Databankiosisis Test', 'Clinical Databank Test', 'drug_combination', 100, 'Specsss', '2017-02-28', '2017-09-21', 'http://localhost/clinical/uploads/drug_records/drug_record_14865959191486595919.png', 1, 'Philippines', 'Optimind', 81, '2017-02-09', 79, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `drug_records`
--

CREATE TABLE IF NOT EXISTS `drug_records` (
`drug_record_id` int(11) NOT NULL,
  `drug_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit` varchar(50) NOT NULL,
  `record_type` varchar(15) NOT NULL,
  `date` date NOT NULL,
  `actor1` varchar(150) NOT NULL,
  `position1` varchar(20) NOT NULL,
  `actor2` varchar(150) NOT NULL,
  `position2` varchar(20) NOT NULL,
  `receipt` varchar(350) NOT NULL,
  `description` text NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` date NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `drug_records`
--

INSERT INTO `drug_records` (`drug_record_id`, `drug_id`, `quantity`, `unit`, `record_type`, `date`, `actor1`, `position1`, `actor2`, `position2`, `receipt`, `description`, `added_by`, `date_added`, `updated_by`, `date_updated`, `remarks`) VALUES
(1, 4, 0, 'a', 'choose', '2016-12-28', 'a', 'Applicant', 'a', 'Applicant', '', 'a', 10, '2016-12-28', 0, '0000-00-00', ''),
(2, 5, 0, 'b', 'put_in_storage', '2016-12-28', 'b', 'CRO', 'b', 'CRO', '', 'b', 10, '2016-12-28', 0, '0000-00-00', ''),
(3, 6, 0, 'c', 'put_out_storage', '2016-12-28', 'c', 'CRO', 'c', 'CRO', '', 'c', 10, '2016-12-28', 0, '0000-00-00', ''),
(4, 5, 1, '1', 'put_in_storage', '2016-12-10', '1', 'Medicine controller', '1', 'Medicine controller', '', '1', 10, '2016-12-28', 0, '0000-00-00', ''),
(5, 5, 0, 'z', 'waste', '2016-12-09', 'z', 'Research nurse', 'z', 'Research nurse', '', 'z', 10, '2016-12-28', 0, '0000-00-00', ''),
(6, 5, 0, '1', 'put_out_storage', '2016-12-14', '1', 'Applicant', '1', 'Research nurse', '', 'zxczxc', 10, '2016-12-28', 0, '0000-00-00', ''),
(7, 5, 0, 'zxc', 'put_in_storage', '2016-12-01', 'z', 'Applicant', 'z', 'Research nurse', '', 'zxc', 10, '2016-12-28', 0, '0000-00-00', ''),
(8, 4, 0, 'q', 'put_out_storage', '2016-12-28', 'q', 'Medicine controller', 'q', 'Medicine controller', '', 'q', 10, '2016-12-28', 0, '0000-00-00', ''),
(9, 7, 1, '1', 'choose', '2016-12-29', '1', 'Applicant', '1', 'Applicant', '', '1', 10, '2016-12-29', 0, '0000-00-00', ''),
(10, 6, 2, '22', 'put_in_storage', '2016-12-29', '2', 'CRO', '2', 'CRO', '', '2', 10, '2016-12-29', 0, '0000-00-00', ''),
(11, 8, 34, 'å…‹', 'put_in_storage', '2016-12-23', 'è€ƒè™‘åŠ å¯ä¹', 'CRO', 'é˜¿é“å¤«', 'Applicant', '', 'é˜¿é“å¤«', 10, '2016-12-30', 0, '0000-00-00', ''),
(12, 9, 1, '11', 'put_in_storage', '2016-12-30', '1', 'CRO', '1', 'Medicine controller', '', '11', 10, '2016-12-30', 0, '0000-00-00', 'å·²å¤æ ¸'),
(13, 10, 12, 'é¢—', 'put_in_storage', '2017-01-04', 'é˜¿é“å¤«', 'CRO', 'é˜¿é“å¤«', 'Applicant', '', 'é˜¿é“å¤«', 10, '2017-01-07', 0, '0000-00-00', ''),
(14, 11, 100, 'ç›’', 'put_in_storage', '2017-01-12', 'å…³å»ºæ–‡', 'Medicine controller', 'æœ±è‰ºæ€', 'Research nurse', '', 'æ— ', 10, '2017-01-10', 0, '0000-00-00', ''),
(15, 11, 2, 'ç²’', 'put_in_storage', '2017-01-05', 'æŽæ˜Ž', 'Applicant', 'æ¢¨èŠ±', 'CRO', '', 'å‚¨å­˜', 10, '2017-01-12', 0, '0000-00-00', ''),
(16, 22, 12, '', 'put_out_storage', '2017-02-14', '124', 'Medicine controller', '123', 'Medicine controller', '', 'Test out Reason', 79, '2017-02-17', 79, '0000-00-00', ''),
(17, 22, 25, '22', 'put_out_storage', '2017-02-21', 'me', 'Medicine controller', 'ypu', 'CRO', '', 'hey', 79, '2017-02-17', 0, '0000-00-00', '');

-- --------------------------------------------------------

--
-- Table structure for table `drug_record_receipts`
--

CREATE TABLE IF NOT EXISTS `drug_record_receipts` (
`receipt_id` int(11) NOT NULL,
  `drug_record_id` int(11) NOT NULL,
  `receipt_file` text NOT NULL,
  `receipt` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `drug_record_receipts`
--

INSERT INTO `drug_record_receipts` (`receipt_id`, `drug_record_id`, `receipt_file`, `receipt`) VALUES
(1, 4, '4--&--14829158681797301903.docx', 'PAU.docx'),
(2, 5, '5--&--1482916145304540011.docx', 'PAU.docx'),
(3, 6, '6--&--1482918512631517921.docx', 'PAU.docx'),
(4, 7, '7--&--1482920000572026763.docx', 'PAU.docx'),
(5, 8, '8--&--1482920192420913098.docx', 'PAU.docx'),
(6, 11, '11--&--14830781402089084170.docx', 'Progresstranslation.docx'),
(7, 12, '12--&--14830897621992289131.docx', 'template3.docx'),
(8, 14, '14--&--1484046328820822165.docx', '.docx'),
(9, 15, '15--&--14841908411736391183.docx', '.docx'),
(10, 0, '0--&--148659829129646.pdf', 'June10RevisedUXJune6RSLEdits.pdf'),
(11, 16, '16--&--148727097424790.pdf', 'June10RevisedUXJune6RSLEdits.pdf'),
(12, 17, '17--&--148729911217849.doc', '1487238494.doc');

-- --------------------------------------------------------

--
-- Table structure for table `inspections`
--

CREATE TABLE IF NOT EXISTS `inspections` (
`inspection_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `report_date` date NOT NULL,
  `role` varchar(17) NOT NULL,
  `type` varchar(17) NOT NULL,
  `executor` varchar(225) NOT NULL,
  `report_description` text NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `inspections`
--

INSERT INTO `inspections` (`inspection_id`, `project_id`, `report_date`, `role`, `type`, `executor`, `report_description`, `added_by`, `date_added`, `updated_by`, `date_updated`) VALUES
(1, 111, '2017-02-23', '6', 'on_site_inspector', '', 'test1', 6, '2017-02-14', 0, '0000-00-00'),
(2, 111, '2017-02-26', '6', 'inspector', '', 'test2', 6, '2017-02-14', 0, '0000-00-00'),
(3, 111, '2017-02-28', '6', 'on_site_inspector', 'Executor Test 1', 'Hellowwwwww', 6, '2017-02-17', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_uploads`
--

CREATE TABLE IF NOT EXISTS `inspection_uploads` (
`inspection_upload_id` int(11) NOT NULL,
  `inspection_id` int(11) NOT NULL,
  `inspection_file` text NOT NULL,
  `inspection` text NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `inspection_uploads`
--

INSERT INTO `inspection_uploads` (`inspection_upload_id`, `inspection_id`, `inspection_file`, `inspection`, `databank`) VALUES
(1, 1, 'http://localhost/clinical/inspection/1--&--148702923828279.png', 'genava1478765870611447719.png', 1),
(2, 2, 'http://localhost/clinical/inspection/2--&--148702925219888.png', 'genava1478765870611447719.png', 0),
(3, 3, 'http://localhost/clinical/inspection/3--&--148728186218170.pdf', 'June10RevisedUXJune6RSLEdits.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
`word_id` int(5) NOT NULL,
  `phrase` varchar(150) NOT NULL,
  `english` varchar(150) NOT NULL,
  `chinese` mediumtext NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=766 ;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`word_id`, `phrase`, `english`, `chinese`) VALUES
(1, 'sitename', 'Clinical Trial Management System', '20020,24202,30740,31350,31649,29702,31995,32479'),
(2, 'chinese', 'chinese', '20013,25991'),
(4, 'side_effects', 'Side effects', '19981,33391,20107,20214,47,21453,24212'),
(7, 'search', 'Search', '25628,32034'),
(8, 'research_category', 'Research category', '30740,31350,31867,21035'),
(9, 'status', 'status', '29366,24577'),
(10, 'all', 'All', '20840,37096'),
(11, 'stage_1', 'Stage I', '73,26399'),
(12, 'stage_2', 'Stage II', '73,73,26399'),
(13, 'stage_3', 'Stage III', '73,73,73,26399'),
(14, 'stage_4', 'Stage IV', '73,86,26399'),
(15, 'be_trial', 'BE Trial', '66,69,35797,39564'),
(16, 'clinical_verifications', 'Clinical verifications', '20020,24202,39564,35777'),
(17, 'other', 'Other', '20854,20182'),
(18, 'apply', 'Apply', '31435,39033,30003,35831'),
(19, 'initiation', 'Initiation', '31435,39033,36890,36807'),
(20, 'ethics_pass', 'Ethics pass', '20262,29702,36890,36807'),
(21, 'trial_begin', 'Trial begin', '35797,39564,24320,22987'),
(24, 'number', 'NO.', '24207,21495'),
(25, 'project_number', 'Project number', '39033,30446,32534,21495'),
(26, 'project_name', 'Project name', '39033,30446,21517,31216'),
(27, 'applicant', 'Applicant', '30003,21150,32773'),
(28, 'current_progress', 'Current progress', '24403,21069,20107,39033'),
(32, 'date_created', 'Date created', '24403,21069,20107,39033,26102,38388'),
(33, 'operation', 'Operation', '25805,20316'),
(57, 'new_project', 'New Project', '26032,22686,39033,30446'),
(58, 'project_detail', 'Project detail', '39033,30446,35814,24773'),
(59, 'lot_number', 'Lot number', '20020,24202,35797,39564,25209,25991,25991,20214,21495'),
(62, 'trial_category', 'Trial category', '30740,31350,31867,21035'),
(63, 'indications', 'Indications', '36866,24212,30151'),
(64, 'risk_category', 'Risk category', '39118,38505,31867,21035'),
(65, 'low', 'Low', '20302'),
(66, 'mid', 'Mid', '20013'),
(67, 'high', 'High', '39640'),
(68, 'departments', 'Departments', '31185,23460'),
(69, 'pgroup_details', 'Professional group detials', '19987,19994,32452,35814,24773'),
(70, 'professional_group', 'Professional group', '19987,19994,32452'),
(71, 'principal_investigator', 'Principal investigator', '19987,19994,32452,80,73'),
(72, 'pgroup_contact', 'Professional group contact person', '19987,19994,32452,39033,30446,32852,31995,20154'),
(73, 'pgroup_mobile', 'Professional group mobile', '19987,19994,32452,32852,31995,25163,26426,21495'),
(74, 'pgroup_email', 'Professional group email', '19987,19994,32452,101,109,97,105,108'),
(75, 'applicant_details', 'Applicant details', '30003,21150,32773,35814,24773'),
(76, 'applicant_name', 'Applicant name', '30003,21150,32773'),
(77, 'emergency_contact', 'Emergency contact person', '30003,21150,32773,32852,31995,20154'),
(78, 'emergency_mobile', 'Emergency contact mobile', '30003,21150,32773,32852,31995,25163,26426,21495'),
(79, 'emergency_email', 'Emergency email', '30003,21150,32773,101,109,97,105,108'),
(80, 'cro_details', 'CRO details', '67,82,79,35814,24773'),
(81, 'cro_name', 'CRO', '67,82,79'),
(82, 'cro_contact', 'CRO contact person', '67,82,79,32852,31995,20154'),
(83, 'cro_mobile', 'CRO contact mobile', '67,82,79,32852,31995,25163,26426,21495'),
(84, 'other_details', 'Other details', '20854,20182,35814,24773'),
(85, 'research_objectives', 'Research objectives', '30740,31350,30446,30340'),
(86, 'subsidize_type', 'Type of subsidize', '30740,31350,36164,21161,31867,22411'),
(87, 'fully_funded', 'Fully funded', '33719,20840,39069,36164,21161'),
(88, 'partially_funded', 'Partially funded', '33719,37096,20998,36164,21161'),
(89, 'no_subsidize', 'No subsidize', '26080,36164,21161,26041'),
(90, 'remarks', 'Remarks', '22791,27880'),
(93, 'pi', 'PI', '20027,35201,30740,31350,32773'),
(94, 'mc_info', 'Multi-center information', '22810,20013,24515,20449,24687'),
(95, 'organization', 'Organization', '21333,20301,21517,31216'),
(96, 'person_in_charge', 'Person in charge', '36127,36131,20154'),
(98, 'lead_participate', 'Lead/Participate', '29301,22836,47,21442,19982'),
(99, 'submit', 'Submit', '25552,20132'),
(100, 'project_documents', 'Project documents', '39033,30446,36164,26009'),
(101, 'name', 'Name', '21517,31216'),
(105, 'version', 'Version', '29256,26412'),
(107, 'description', 'Description', '25551,36848'),
(108, 'type', 'Type', '31867,22411'),
(110, 'select_file', 'Select file', '36873,25321,25991,20214'),
(112, 'project_plan', 'Project plan', '39033,30446,26041,26696'),
(113, 'uploader', 'Uploader', '19978,20256,20154'),
(116, 'tfo_opinion', 'Testing Facility opinion', '35797,39564,26426,26500,24847,35265'),
(117, 'pi_opinion', 'Principal Investigator opinion', '20027,35201,30740,31350,32773,24847,35265'),
(119, 'ecc_opinion', 'Ethics Committee Chairman opinion', '20262,29702,22996,21592,20250,24847,35265'),
(120, 'role', 'Role', '35282,33394'),
(123, 'upload_report', 'Upload report', '19978,20256,25253,21578'),
(124, 'file', 'File', '25991,20214'),
(129, 'project_information', 'Project Information', '39033,30446,20449,24687'),
(130, 'pre_opinion', 'Peer review expert opinion', '21516,34892,35780,35758,19987,23478,24847,35265'),
(131, 'gender', 'Gender', '24615,21035'),
(132, 'job_title', 'Job title', '32844,31216'),
(133, 'mobile_num', 'Mobile', '30005,35805,21495,30721'),
(134, 'position', 'position', '32844,20301'),
(136, 'job_spec', 'Job specification', '24037,20316,35268,33539'),
(137, 'title', 'Title', '26631,39064'),
(138, 'add_job_spec', 'Add job pecification', '28155,21152,24037,20316,35268,33539'),
(139, 'add', 'Add', '28155,21152'),
(140, 'delete', 'Delete', '21024,38500'),
(141, 'meeting_type', 'Type of meeting', '20250,35758,31867,22411'),
(142, 'date', 'Date', '26085,26399'),
(143, 'meeting_record', 'Add a meeting', '28155,21152,20250,35758,35760,24405'),
(144, 'participants', 'Participants', '21442,20250,20154,21592'),
(145, 'place', 'Place', '22320,22336'),
(146, 'host', 'Host', '20027,25345,20154'),
(147, 'meeting_material', 'Meeting material', '21407,22987,35760,24405,25991,20214'),
(148, 'add_meeting_material', 'Add meeting material', '28155,21152,20250,35758,36164,26009'),
(149, 'meeting_content', 'Meeting content', '20250,35758,20869,23481'),
(150, 'meeting_summary', 'Meeting summary', '20250,35758,24635,32467'),
(151, 'bio_sample_management', 'Biological sample management', '29983,29289,26679,21697,31649,29702'),
(152, 'quantity', 'Quantity', '26679,26412,37327'),
(153, 'add_bio_record', 'Add biological sample record', '28155,21152,29983,29289,26679,26412,35760,24405'),
(155, 'whole_blood', 'Whole blood', '20840,34880'),
(156, 'blood_plasma', 'Blood plasma', '34880,27974'),
(159, 'blood_serum', 'Blood serum', '34880,28165'),
(160, 'urine', 'Urine', '23615,28082'),
(161, 'dung', 'Dung', '31914,20415'),
(162, 'saliva', 'Saliva', '21822,28082'),
(163, 'milk', 'Milk', '20083,27713'),
(164, 'collect_time', 'Collect time', '26679,26412,37319,38598,26102,38388'),
(165, 'collect_record', 'Collect record', '26679,26412,37319,38598,35760,24405'),
(166, 'process_record', 'Process record', '26679,26412,22788,29702,35760,24405'),
(167, 'state', 'State', '30740,31350,29366,24577'),
(168, 'case_num', 'Case number', '30149,21382,21495'),
(169, 'subjects_num', 'Subjects number', '21463,35797,32773,32534,21495'),
(170, 'abbrv', 'Abbreviation', '22995,21517,32553,20889'),
(171, 'id_card_num', 'I.D. card No.', '36523,20221,35777,21495'),
(172, 'id_card_copy', 'I.D. card Copy', '36523,20221,35777,22797,21360,20214'),
(173, 'birth_date', 'Birth date', '29983,26085'),
(174, 'nation', 'Nation', '27665,26063'),
(175, 'country', 'Country', '22269,23478'),
(176, 'province', 'Province', '30465,20221'),
(177, 'address', 'Address', '20303,22336'),
(178, 'native', 'Native place', '31821,36143'),
(179, 'sign_informed_consent', 'Sign informed consent', '26159,21542,31614,32626,30693,24773,21516,24847,20070'),
(180, 'yes', 'Yes', '26377'),
(181, 'no', 'No', '26080'),
(182, 'screen_date', 'Screen date', '31579,36873,26085,26399'),
(183, 'random_date', 'Random date', '38543,26426,26085,26399'),
(184, 'random_num', 'Random number', '38543,26426,30721'),
(188, 'accept_date', 'Accept date', '25509,21463,26085,26399'),
(189, 'reject_date', 'Reject date', '25298,32477,26085,26399'),
(190, 'drop_out_date', 'Drop out date', '21076,38500,26085,26399'),
(191, 'first_visit_date', 'First visit date', '39318,35775,26085,26399'),
(192, 'male', 'Male', '30007'),
(194, 'female', 'Female', '22899'),
(195, 'stage_summary', 'Stage summary', '38454,27573,23567,32467'),
(196, 'time_slot', 'Time slot', '26102,38388,27573'),
(197, 'enroll_amount', 'Enroll amount', '20837,32452,20363,25968'),
(198, 'quit_amount', 'Quit amount', '36864,20986,20363,25968'),
(199, 'finish_amount', 'Finish amount', '23436,25104,20363,25968'),
(201, 'sae', 'SAE', '19981,33391,21453,24212'),
(202, 'amount', 'Amount', '21457,29983,20363,25968'),
(203, 'degree', 'Degree', '20005,37325,31243,24230'),
(204, 'handling_information', 'Handling information', '22788,29702,24773,20917'),
(205, 'volunteer', 'Volunteer', '24535,24895,32773'),
(206, 'married', 'Married', '23130,21542'),
(207, 'blood_type', 'Blood type', '34880,22411'),
(208, 'had_surgery', 'Had surgery', '26159,21542,20570,36807,25163,26415'),
(209, 'family_history', 'Family history', '23478,26063,21490'),
(210, 'heavy_smoking', 'Heavy smoking', '21980,28895'),
(211, 'heavy_drinking', 'Heavy drinking', '21980,37202'),
(212, 'allergies', 'Allergies', '36807,25935,21490'),
(213, 'height', 'Height', '36523,39640'),
(214, 'weight', 'Weight', '20307,37325'),
(215, 'bmi', 'BMI', '20307,37325,25351,25968'),
(216, 'medical_history', 'Medical history', '26082,24448,30149,21490'),
(217, 'progress', 'Progress', '39033,30446,36827,23637'),
(218, 'material', 'Material', '39033,30446,36164,26009'),
(220, 'timeline', 'Timeline', '26102,38388,32447'),
(221, 'no_data', 'No data available', '26080,27861,33719,21462,25968,25454'),
(223, 'add_remarks', 'Add remarks', '28155,21152,22791,27880'),
(224, 'action', 'Action', '25805,20316'),
(225, 'member', 'Member', '39033,30446,25104,21592'),
(226, 'training_meeting', 'Training/Meeting', '22521,35757,47,20250,35758'),
(227, 'drug_management', 'Drug management', '33647,21697,31649,29702'),
(228, 'subjects', 'Subjects', '21463,35797,32773'),
(229, 'report_type', 'Report type', '25253,21578,31867,22411'),
(230, 'report_time', 'Report time', '25253,21578,26102,38388'),
(231, 'hospital', 'Hospital', '21307,30103,26426,26500,21450,19987,19994,21517,31216'),
(232, 'department', 'Department', '30003,25253,21333,20301,21517,31216'),
(233, 'drug_name', 'Drug name', '35797,39564,29992,33647,21517,31216'),
(234, 'chinese_name', 'Chinese name', '20013,25991,21517,31216'),
(235, 'english_name', 'English name', '33521,25991,21517,31216'),
(237, 'drug_information', 'Drug information', '33647,21697,27880,20876,20998,31867,21450,21058,22411'),
(238, 'drug_type', 'Drug type', '33647,21697,20998,31867'),
(239, 'form_of_drug', 'The form of drug', '21058,22411'),
(240, 'register_type', 'Register type', '27880,20876,20998,31867'),
(241, 'clinical_trial', 'Clinical trial', '20020,24202,30740,31350'),
(242, 'subjects_situation', 'Subjects situation', '21463,35797,32773,22522,26412,24773,20917'),
(243, 'initials', 'Initials', '22995,21517,32553,20889'),
(244, 'complications', 'Complications', '21512,24182,30142,30149,21450,27835,30103'),
(245, 'sae_diagnosis', 'SAE medical diagnosis', '83,65,69,30340,21307,23398,35786,26029'),
(246, 'drug_measure', 'Drug measure', '23545,35797,39564,29992,33647,37319,21462,30340,25514,26045'),
(247, 'sae_situation', 'SAE situation', '83,65,69,24773,20917'),
(248, 'sae_drug_relation', 'Relation of SAE and drug', '83,65,69,19982,35797,39564,33647,30340,20851,31995'),
(249, 'saevest', 'SAEvest', '83,65,69,24402,36716'),
(250, 'unblinding_situation', 'Unblinding situation', '30772,30450,24773,20917'),
(251, 'report_internal', 'Report internal', '22269,20869,83,65,69,25253,36947,24773,20917'),
(252, 'report_external', 'Report external', '22269,22806,83,65,69,25253,36947,24773,20917'),
(253, 'handle_detail', 'Handle detail', '83,65,69,21457,29983,21450,22788,29702,30340,35814,24773'),
(256, 'reporter_unit', 'Reporter unit', '25253,21578,21333,20301,21517,31216'),
(257, 'reporter_position', 'Reporter position', '25253,21578,20154,32844,21153'),
(258, 'die', 'Die', '27515,20129'),
(259, 'be_in_hospital', 'Be in hospital', '23548,33268,20303,38498'),
(260, 'disability', 'Disability', '24310,38271,20303,38498,26102,38388'),
(261, 'dysfunction', 'Dysfunction', '20260,27531'),
(262, 'deformity', 'Deformity', '21151,33021,38556,30861'),
(263, 'life_threatening', 'Life threatening', '21361,21450,29983,21629'),
(264, 'severe', 'Severe', '37325,24230'),
(265, 'continuantur_remedia', 'Continuantur remedia', '32487,32493,29992,33647'),
(266, 'reduce_the_dosage', 'Reduce the dosage', '20943,23569,21058,37327'),
(267, 'stop_then_go', 'Stop then go', '33647,29289,26242,20572,21518,21448,24674,22797'),
(269, 'no_detail', 'No detail', '19981,35814'),
(270, 'no_blinding', 'No blinding', '19981,35774,30450'),
(272, 'did_not_unblinding', 'Did not unblinding', '26410,30772,30450'),
(273, 'unblinding', 'Unblinding', '24050,30772,30450'),
(274, 'transference_cure', 'Transference cure', '30151,29366,28040,22833'),
(275, 'symptoms_last', 'Symptoms last', '30151,29366,25345,32493'),
(276, 'certainty', 'Certainty', '32943,23450,26377,20851'),
(277, 'certainty_unconcerned', 'Certainty unconcerned', '32943,23450,26080,20851'),
(278, 'not_determinable', 'Not determinable', '26080,27861,21028,26029'),
(279, 'maybe_concern', 'Maybe concern', '21487,33021,26377,20851'),
(282, 'be_unconcerned', 'Be unconcerned', '21487,33021,26080,20851'),
(283, 'first_report', 'First report', '39318,27425,25253,21578'),
(284, 'follow_up_report', 'Follow-up report', '38543,35775,25253,21578'),
(285, 'sum_up_report', 'Sum-up report', '24635,32467,25253,21578'),
(286, 'traditional_chinese_medicine', 'Traditional Chinese medicine', '20013,33647'),
(287, 'chemistry_drug', 'Chemistry drug', '21270,23398,33647'),
(288, 'cure_medicine', 'Cure medicine', '27835,30103,29992,29983,29289,21046,21697'),
(290, 'prevent_medicine', 'Prevent medicine', '39044,38450,29992,29983,29289,21046,21697'),
(291, 'project', 'Project', '39033,30446'),
(292, 'hospital_mobile', 'Hospital mobile', '21307,38498,30005,35805'),
(293, 'department_mobile', 'Department mobile', '31185,23460,30005,35805'),
(294, 'subject_library', 'Subject library', '21463,35797,32773,24211'),
(463, 'add_member', ' Add member', '28155,21152,25104,21592'),
(464, 'tfo', 'Testing Facility Office', '35797,39564,26426,26500'),
(465, 'pre', 'Peer review expert', '21516,34892,35780,35758,19987,23478'),
(466, 'ethics_committee', 'Ethics Committee', '20262,29702,22996,21592,20250'),
(467, 'member_list', 'member list', '25104,21592,21015,34920'),
(468, 'tfo_list', 'Testing Facility Office list', '35797,39564,26426,26500,25104,21592,21015,34920'),
(469, 'applicant_list', 'Applicant list', '30003,21150,32773,21015,34920'),
(470, 'company', 'Company', '21333,20301'),
(471, 'email', 'Email', '30005,23376,37038,31665'),
(472, 'password', 'Password', '23494,30721'),
(473, 'con_pass', 'Confirm Password', '30830,35748,23494,30721'),
(474, 'cancel', 'Cancel', '21462,28040'),
(475, 'update', 'Update', '26356,26032'),
(476, 'edit', 'Edit', '32534,36753'),
(478, 'disable_account', 'Are you sure you want to disable account', '20320,30830,35748,35201,20923,32467,35813,24080,21495,21527'),
(480, 'system_message', 'System Message', '31995,32479,28040,24687'),
(481, 'add_group', 'Add Group', '28155,21152,19987,19994,32452'),
(482, 'add_applicant', 'Add Applicant', '28155,21152,25104,21592'),
(483, 'ecm', 'Ethics Committee Member', '20262,29702,22996,21592,20250,25104,21592'),
(484, 'ecc', 'Ethics Committee Chairman', '20262,29702,22996,21592,20250,20027,24109'),
(485, 'blank_update', 'leave blank if not going to update', '22914,27809,26377,21487,19981,22635'),
(486, 'mobile_registered', 'Mobile Number is already registered', '25163,26426,21495,24050,23384,22312'),
(487, 'email_registered', 'Email is already registered', '37038,31665,24050,23384,22312'),
(488, 'password_not_match', 'Password does not match', '23494,30721,19981,27491,30830'),
(489, 'drug_list', 'Drug List', '33647,21697,21015,34920'),
(490, 'record_type', 'Record Type', '20986,20837,31867,21035'),
(491, 'lonely_remark', 'It looks lonely here...<br>Let''s wait for new', '26242,26080,35760,24405'),
(492, 'specification', 'Specification', '35268,26684'),
(493, 'term_validity', 'Term of validity', '26377,25928,26399'),
(494, 'add_drug', 'Add Drug', '28155,21152,33647,21697'),
(495, 'trade_name', 'Trade Name', '21830,21697,21517'),
(496, 'chemical_name', 'Chemical Name', '21270,23398,21517'),
(498, 'tested_drug', 'Tested Drug', '21463,35797,33647'),
(499, 'control_drug', 'Control Drug', '23545,29031,33647'),
(500, 'placebo', 'Placebo', '23545,29031,33647'),
(501, 'adjuvant_drug', 'Adjuvant Drug', '36741,21161,29992,33647'),
(502, 'inspection_report', 'Inspection Report', '33647,21697,26816,39564,25253,21578,20070'),
(503, 'pharmaceutical_factory', 'Pharmaceutical factory', '29983,20135,33647,21378'),
(504, 'provider', 'Provider', '25552,20379,32773'),
(505, 'add_record', 'Add Record', '28155,21152,35760,24405'),
(506, 'id_card', 'ID Card', '36523,20221,35777'),
(507, 'gcp', 'GCP', '71,67,80,22521,35757,35777,20070'),
(508, 'diploma', 'Diploma', '27605,19994,35777'),
(509, 'deg_cert', 'Degree Certificate', '23398,20301,35777'),
(513, 'save_address', 'Save Address', '20445,23384,22320,28857'),
(514, 'save_temp', 'Save Temperature', '20445,23384,28201,24230'),
(515, 'save_humidity', 'Save Humidity', '20445,23384,28287,24230'),
(516, 'project_material', 'Project material', '39033,30446,26448,26009'),
(517, 'project_progress', 'Project progress', '39033,30446,36827,23637'),
(518, 'save_as_draft', 'Save as draft', '20445,23384,20026,33609,31295'),
(520, 'collect', 'Collect', '37319,38598,35760,24405'),
(521, 'handle', 'Handle', '22788,29702,35760,24405'),
(522, 'save', 'Save', '20445,23384,35760,24405'),
(523, 'transport', 'Transport', '36816,36755,35760,24405'),
(524, 'determine', 'Determine', '27979,23450,35760,24405'),
(525, 'recycle', 'Recycle', '22238,25910,35760,24405'),
(526, 'destroy', 'Destroy', '38144,27585,35760,24405'),
(527, 'download_template', 'Download template', '19979,36733,27169,26495'),
(528, 'error_updating_project', 'Error updating project', '26356,26032,39033,30446,26102,20986,38169'),
(529, 'project_updated_successfully', 'Project updated successfully', '39033,30446,24050,25104,21151,26356,26032'),
(530, 'operation_successful', 'Operation Successful', '25552,20132,25104,21151'),
(532, 'confirmation', 'Confirmation', '30830,35748'),
(534, 'add_project_members', 'Add Project Members', '28155,21152,39033,30446,25104,21592'),
(535, 'clinician', 'Clinician', '20020,24202,21307,29983'),
(536, 'research_nurse', 'Research Nurse', '30740,31350,25252,22763'),
(539, 'feedback', 'Feedback', '21453,39304'),
(540, 'add_documents', 'Add Documents', '28155,21152,25991,26723'),
(542, 'agree', 'Agree', '21516,24847'),
(543, 'disagree', 'Disagree', '19981,21516,24847'),
(544, 'comments', 'Comments', '27880,37322'),
(545, 'add_document_remarks', 'Add Document Remarks', '28155,21152,25991,26723,22791,27880'),
(546, 'ethics_committee_comments', 'Ethics Committee Comments', '20262,29702,22996,21592,20250,24847,35265'),
(549, 'pass', 'Pass', '23457,26680,36890,36807'),
(550, 'no_pass', 'No Pass', '23457,26680,19981,36890,36807'),
(552, 'done', 'Done', '23436,25104'),
(553, 'empty_table_data', 'Empty table data', '26242,26080,35760,24405'),
(554, 'logout', 'Log Out', '30331,20986'),
(565, 'approve_reject_investigator_revision', 'Approve or Reject Investigator Opinion', '26159,21542,21516,24847,30740,31350,32773,20462,25913,21518,30340,26041,26696'),
(566, 'birthday', 'Birthday', '20986,29983,26085,26399'),
(567, 'id_num', 'ID Num', '36523,20221,35777,21495,30721'),
(568, 'state_date', 'State Date', '38543,26426,26102,38388'),
(569, 'chosen', 'Chosen', '36873,25321'),
(570, 'begin', 'Begin', '24320,22987'),
(571, 'follow_up', 'Follow Up', '36319,36827'),
(572, 'out', 'Out', '20986,26469'),
(573, 'drop', 'Drop', '19979,38477'),
(574, 'finish', 'Finish', '23436,25104'),
(580, 'participate', 'Participate', '21442,21152'),
(582, 'upload_newer_version', 'Please upload newer version before proceeding', '35831,19978,20256,26032,30340,39033,30446,26041,26696'),
(583, 'reason', 'Reason', '21407,22240'),
(584, 'ethics_committee_secretary', 'Ethics Committee secretary', '20262,29702,22996,21592,20250,31192,20070'),
(585, 'treatment_situation', 'Treatment Situation', '83,65,69,32,24773,20917'),
(587, 'lead', 'Lead', '29301,22836'),
(588, 'investigator', 'Investigator', '30740,31350,32773'),
(589, 'the', 'The', '35813'),
(590, 'are_you_sure', 'Are you sure to', '32,24744,30830,35748'),
(591, 'project_complete', 'Project complete', '39033,30446,23436,25104'),
(592, 'process', 'Process', '35797,39564,36827,34892'),
(593, 'my_projects', 'My Projects', '25105,30340,39033,30446'),
(594, 'medical_equipment', 'Medical Equipment', '21307,30103,22120,26800'),
(595, 'food', 'Food', '39135,21697'),
(596, 'stop_project', 'Are you sure you want to stop project?', '24744,30830,35748,20013,27490,35813,39033,30446,21527'),
(597, 'stop', 'Stop', '35797,39564,20013,27490'),
(598, 'trial_finish', 'Trial Finish', '35797,39564,32467,26463'),
(604, 'add_documents_comments', 'Add Documents Comments', '35831,34917,20840,36164,26009'),
(605, 'check_pending', 'Check Pending', '24453,23457,26680'),
(607, 'project_audit', 'Project Audit', '39033,30446,23457,26680'),
(610, 'choose_peer_review_expert', 'Choose Peer Review Expert', '21516,34892,35780,35758,19987,23478'),
(611, 'pre_list', 'List of Peer Review Expert', '36873,25321,21516,34892,35780,35758,19987,23478'),
(613, 'to_do', 'To Do', '24453,21150'),
(614, 'inspection', 'Inspection', '36136,37327,20445,35777'),
(615, 'inspector', 'Inspector', '30417,26597'),
(616, 'on_site_inspection', 'On-site inspection', '29616,22330,26816,26597'),
(617, 'clinician_selecting', 'Clinician selecting', '21307,29983,23457,26680'),
(618, 'chosen_subject', 'Chosen to be subject', '20837,36873,21463,35797,32773'),
(619, 'before_selecting', 'Before Selecting', '31579,36873,21069'),
(620, 'my_projects_non_a', 'Projects', '39033,30446,23457,26680'),
(622, 'stop_button', 'Stop', '20013,27490'),
(623, 'biological_sample_controller', 'Biological Sample Controller', '29983,29289,26679,21697,31649,29702,21592'),
(624, 'medicine_controller', 'Medicine Controller', '33647,21697,31649,29702,21592'),
(625, 'will_start', 'Will Start', '21551,21160,20250'),
(626, 'training', 'Training', '22521,35757'),
(627, 'agree_tfo_1', 'Agree and Submit to PI', '36164,26009,40784,20840,24050,25552,20132,80,73,23457,26680'),
(628, 'disagree_tfo_1', 'Disagree Return to Applicant', '19981,20104,25509,21463'),
(629, 'agree_pi_2', 'Agree and Submit to Peer', '24314,35758,31435,39033'),
(630, 'disagree_pi_2', 'Disagree and submit back to Applicant', '24314,35758,19981,31435,39033'),
(631, 'comments_pi_2', 'PI Comments', '21407,22240'),
(632, 'comments_tfo_1', 'TFO Comments', '35831,34917,20840,36164,26009'),
(634, 'comments_tfo_2', 'Comments TFO', '21407,22240'),
(635, 'choose_pre', 'Choose Peer Experts', '24050,36873,19987,23478'),
(637, 'upload_content_consent', 'Please upload conference content and consent before proceeding', '28155,21152,20262,29702,22996,21592,20250,20250,35758,35760,24405'),
(638, 'upload_consent_agreement', 'Please upload consent agreement before proceeding', '35831,19978,20256,31614,21452,26041,31614,23383,30422,31456,26041,26696,21450,20854,20182,36164,26009,25195,25551,20214,21040,39033,30446,36164,26009,28165,21333'),
(639, 'submit_materials', 'Submit Materials', '25552,20132,20262,29702,23457,26680'),
(640, 'ethics_pending', 'Ethics Pending', '20262,29702,23457,26680,20013'),
(641, 'agree_ecs_7', 'Agree', '23457,26680,36890,36807'),
(642, 'disagree_ecs_7', 'Disagree', '23457,26680,19981,36890,36807'),
(643, 'ecs_comments_7', 'ECS Comments', '21407,22240'),
(645, 'operation_failed', 'Operation Failed', '25805,20316,22833,36133'),
(646, 'ecm_comments', 'ECM Comments', '24847,35265'),
(647, 'version_date', 'Version date', '25552,20132,26085,26399'),
(648, 'ecc_comments', 'Ecc Comments', '21407,22240'),
(649, 'upload_approval', 'Please upload approval before proceeding', '19978,20256,21508,20010,20262,29702,22996,21592,20250,22996,21592,65292,20027,20219,31614,21517,30340,25209,20934,25991,20214,21040,39033,30446,36164,26009'),
(650, 'select', 'Select', '35831,36873,25321'),
(651, 'choose', 'Choose', '35831,36873,25321'),
(652, 'subject_begin', 'Begin', '24320,22987,35797,39564'),
(653, 'sending_party', 'Sending party', '26679,26412,21457,20986,26041'),
(654, 'operator', 'Operator', '32463,21150,20154'),
(655, 'shipper', 'Shipper', '26679,26412,36816,36865,26041'),
(656, 'recycler', 'Recycler', '26679,26412,22238,25910,26041'),
(657, 'recycle_report', 'Recycle report', '26679,26412,22238,25910,36164,26009'),
(658, 'handle_time', 'Handle time', '22788,29702,26102,38388'),
(659, 'handle_record', 'Handle record', '22788,29702,35760,24405'),
(660, 'transport_company', 'Transport company', '36816,36755,20844,21496'),
(661, 'transport_temp', 'Transport temperature', '36816,36755,28201,24230'),
(662, 'transport_humidity', 'Transport humidity', '36816,36755,28287,24230'),
(663, 'transport_time', 'Transport time', '36816,36755,26102,38388'),
(664, 'determine_record', 'Determine record', '27979,23450,31649,29702,35760,24405'),
(665, 'determine_report', 'Determine report', '26679,26412,27979,23450,25253,21578'),
(666, 'determine_company', 'Determine company', '27979,23450,21333,20301'),
(667, 'report_file', 'Report file', '25253,21578,25991,20214'),
(668, 'comment_app_4', 'Applicant Comments', '21407,22240'),
(669, 'choose_to_be_subject', 'Choose to be subject', '36827,20837,21463,35797,32773,24211'),
(670, 'put_in_storage', 'Put in storage', '20837,24211'),
(671, 'put_out_storage', 'Put out storage', '20986,24211'),
(672, 'waste', 'Waste', '25439,32791'),
(673, 'delivery_receipt', 'Delivery receipt', '20132,25509,21333'),
(674, 'actor_user_two', 'Actor user two', '20132,25509,21592,50'),
(675, 'actor_user_one', 'Transfer manager', '20132,25509,21592,49'),
(676, 'member_name', 'Name', '22995,21517'),
(677, 'data_administrator', 'Data Administrator', '25968,25454,31649,29702,21592'),
(678, 'on_site_inspector', 'On-site inspector', '29616,22330,26816,26597'),
(679, 'drug_combination', 'Drug Combination', '21512,24182,29992,33647'),
(680, 'member_position', 'Position', '36523,20221'),
(681, 'training_modal_pos', 'Position', '36523,20221'),
(682, 'volunteer_name', 'Name', '22995,21517'),
(683, 'inform_clinician', 'Inform Clinician', '36890,30693,20307,26816'),
(684, 'volunteer_pass', 'Pass', '20307,26816,36890,36807'),
(685, 'volunteer_no_pass', 'No Pass', '20307,26816,19981,36890,36807'),
(688, 'subject_name', 'Name', '22995,21517'),
(689, 'subject_follow_up', 'Follow up', '38543,35775'),
(690, 'subject_stop', 'Stop', '20013,27490'),
(691, 'subject_out', 'Out', '36864,20986'),
(692, 'subject_drop', 'Drop', '33073,33853'),
(693, 'light_effect', 'Light Side Effect', '36731,24230,19981,33391,21453,24212'),
(694, 'mid_effect', 'Mid Side Effect', '20013,24230,19981,33391,21453,24212'),
(695, 'unit', 'Unit', '21333,20301'),
(696, 'identity', 'Identity', '36523,20221'),
(698, 'placebo_drug_record', 'Placebo', '23433,24944,21058'),
(699, 'select_subject', 'Select subject', '36873,25321,21463,35797,32773'),
(700, 'upload_report_material', 'Upload statistical analysis, analysis of test reports, sub-center summary', '19978,20256,32479,35745,20998,26512,65292,20998,26512,27979,35797,25253,21578,65292,20998,20013,24515,24635,32467'),
(701, 'upload_report_material2', 'Upload research reports to project information', '19978,20256,30740,31350,25253,21578,21040,39033,30446,36164,26009'),
(703, 'collect_time_bgc', 'Collect Time', '26679,26412,37319,38598,26102,38388'),
(704, 'quantity_bgc', 'Quantity', '26679,26412,37327'),
(705, 'collect_record_bgc', 'Collect Record', '26679,26412,37319,38598,35760,24405'),
(706, 'collect_time_save', 'Collect Time', '20445,23384,26085,26399'),
(707, 'process_record_save', 'Process Record', '26679,26412,22788,29702,35760,24405'),
(708, 'destroy_header', 'Add destroy record sample', '26679,26412,35760,24405,26679,26412,35760,24405'),
(709, 'destroy_address', 'Destruction address', '20303,22336,26679,26412,38144,27585,22320,28857'),
(711, 'destroy_operator', 'Operator Sample Destruction Executives', '25805,20316,21592,26679,26412,38144,27585,23454,26045,20154,21592'),
(712, 'destroy_report_file', 'Report document sample destruction information', '25253,21578,25991,20214,26679,21697,38144,27585,36164,26009'),
(713, 'destroy_quantity', 'Destroy quantity', '26679,26412,37327,26679,21697,37327'),
(714, 'date_determine', 'Date', '27979,23450,26085,26399'),
(715, 'to_date', 'to', '33267'),
(716, 'hard_effect', 'Hard Side Effect', '37325,24230,19981,33391,21453,24212'),
(717, 'important_effect', 'Important Side Effect', '37325,35201,19981,33391,21453,24212'),
(718, 'sae_summary', 'SAE', '20005,37325,19981,33391,21453,24212'),
(719, 'other_situation', 'Other Situation', '20854,20182,24773,20917'),
(720, 'upload_materials', 'Upload Materials', '19978,20256,36164,26009'),
(721, 'return_to_volunteer', 'Return to volunteer', '25104,20026,24535,24895,32773'),
(722, 'drug_production_batch_number', 'Drug production batch number', '33647,21697,29983,20135,25209,21495'),
(723, 'please_input_reason', 'Please input reason', '35831,22635,20889,21407,22240'),
(724, 'audit_status', 'Audit Status', '23457,26680,29366,24577'),
(725, 'content', 'Content', '20107,30001'),
(727, 'execution_date', 'Execution date', '25191,34892,26085,26399'),
(728, 'modification_trace', 'Modification Trace', '31293,26597,36712,36857'),
(729, 'data_bank', 'Data Bank', '36164,26009,24211'),
(730, 'phone_number', 'Phone number', '25163,26426,21495'),
(731, 'date_of_birth', 'Date of birth', '20986,29983,26085,26399'),
(732, 'full_name', 'Full name', '22995,21517'),
(733, 'operating', 'Operating', '25805,20316'),
(734, 'the_drug_name', 'Drug name', '33647,21697,21517,31216'),
(735, 'add_volunteer_information', 'Add volunteer information', '28155,21152,24535,24895,32773,20449,24687'),
(736, 'serious_adverse_reactions', 'Serious adverse reactions', '20005,37325,19981,33391,21453,24212'),
(738, 'sample_volume', 'sample_volume', '26679,26412,37327'),
(739, 'bio_samp_mgt', 'Biological sample management', '29983,29289,26679,26412,31649,29702'),
(740, 'feedback_of_testing', 'Feedback of testing organization', '35797,39564,26426,26500,21453,39304'),
(741, 'participants2', 'Participants', '21442,19982,20154,21592'),
(742, 'untitled_meeting', 'Untitled meeting', '26080,39064,20250,35758'),
(743, 'ttttttttttttt', 'ttttttttt', '28155,21152,29983,29289,26679,26412,35760,24405'),
(744, 'mmmmmmm', 'mmmmmmm', '26679,21697,37319,38598,35760,24405'),
(745, 'sign_date', 'Sign date', '31614,32626,26085,26399'),
(746, 'id_card_admin', 'ID Card', '36523,20221,35777'),
(747, 'gcp_card', 'GCP Card', '71,67,80,22521,35757,35777,20070'),
(749, 'degree_certificate', 'Degree certificate', '23398,20301,35777'),
(750, 'new_user_success', 'Successfully registered new user', '24744,24050,25104,21151,28155,21152,26032,24080,21495'),
(751, 'make_sure', 'Please make sure details are correct', '35831,30830,20445,35814,32454,20449,24687,26159,27491,30830,30340,12290'),
(752, 'activated', 'Activated', '24050,28608,27963'),
(753, 'frozen', 'Disabled', '24050,20923,32467'),
(754, 'freeze', 'Freeze', '20923,32467'),
(755, 'activation', 'Activation', '28608,27963'),
(756, 'ok', 'Ok', '22909,30340'),
(757, 'rejected_remarks', 'Rejected remarks', '23457,26680,19981,36890,36807,24847,35265'),
(758, '_asd_sad', 'asd', '26679,26412'),
(759, 'status_1', 'Success', '113,119,105,113,103,101,105,113,117'),
(760, 'home', 'Home', '36873,25321'),
(761, 'module', 'Module', '27169,22359'),
(762, 'save_to_databank', 'Save to data bank', '26159,21542,21516,27493,21040,36164,26009,24211'),
(763, 'upload_time', 'Upload Time', '19978,20256,26102,38388'),
(765, 'view_attachment', 'View Attachment', '26597,30475,25991,20214');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
`log_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `role_id` int(11) NOT NULL,
  `action` text NOT NULL COMMENT 'phrase key of action',
  `payload` longtext NOT NULL COMMENT 'json string of some data'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=399 ;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`log_id`, `created_at`, `role_id`, `action`, `payload`) VALUES
(1, '2017-02-03 03:23:51', 3, 'login', ''),
(2, '2017-02-03 03:27:02', 3, 'logout', ''),
(3, '2017-02-03 03:27:07', 2, 'login', ''),
(4, '2017-02-03 04:11:45', 2, 'logout', ''),
(5, '2017-02-03 04:27:58', 1, 'login', ''),
(6, '2017-02-03 04:42:55', 1, 'logout', ''),
(7, '2017-02-03 04:43:33', 1, 'login', ''),
(8, '2017-02-03 11:07:03', 1, 'logout', ''),
(9, '2017-02-03 11:07:07', 2, 'login', ''),
(10, '2017-02-03 11:18:43', 2, 'logout', ''),
(11, '2017-02-03 11:18:51', 1, 'login', ''),
(12, '2017-02-04 03:43:29', 1, 'logout', ''),
(13, '2017-02-04 03:43:33', 2, 'login', ''),
(14, '2017-02-04 03:43:50', 2, 'logout', ''),
(15, '2017-02-04 03:43:54', 3, 'login', ''),
(16, '2017-02-04 03:44:06', 3, 'logout', ''),
(17, '2017-02-04 03:44:12', 2, 'login', ''),
(18, '2017-02-04 03:45:25', 2, 'logout', ''),
(19, '2017-02-04 03:45:29', 1, 'login', ''),
(20, '2017-02-04 03:45:37', 1, 'logout', ''),
(21, '2017-02-04 03:45:49', 3, 'login', ''),
(22, '2017-02-04 03:45:58', 3, 'logout', ''),
(23, '2017-02-04 03:46:04', 2, 'login', ''),
(24, '2017-02-04 03:49:11', 2, 'logout', ''),
(25, '2017-02-04 03:49:17', 6, 'login', ''),
(26, '2017-02-04 03:55:17', 6, 'logout', ''),
(27, '2017-02-04 03:55:24', 1, 'login', ''),
(28, '2017-02-04 04:33:22', 1, 'logout', ''),
(29, '2017-02-04 04:33:27', 6, 'login', ''),
(30, '2017-02-04 04:33:49', 6, 'logout', ''),
(31, '2017-02-04 04:33:53', 3, 'login', ''),
(32, '2017-02-04 04:34:10', 3, 'logout', ''),
(33, '2017-02-04 04:34:16', 20, 'login', ''),
(34, '2017-02-04 07:07:11', 20, 'logout', ''),
(35, '2017-02-04 07:07:14', 1, 'login', ''),
(36, '2017-02-04 07:14:51', 1, 'logout', ''),
(37, '2017-02-04 07:14:53', 1, 'login', ''),
(38, '2017-02-04 07:23:43', 1, 'logout', ''),
(39, '2017-02-04 07:23:47', 3, 'login', ''),
(40, '2017-02-04 07:31:53', 3, 'logout', ''),
(41, '2017-02-04 07:31:55', 1, 'login', ''),
(42, '2017-02-07 02:51:59', 2, 'login', ''),
(43, '2017-02-07 03:09:44', 2, 'logout', ''),
(44, '2017-02-07 03:09:53', 1, 'login', ''),
(45, '2017-02-07 03:12:28', 1, 'logout', ''),
(46, '2017-02-07 03:12:33', 3, 'login', ''),
(47, '2017-02-07 03:13:07', 3, 'logout', ''),
(48, '2017-02-07 03:13:11', 1, 'login', ''),
(49, '2017-02-07 03:13:18', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"Enzo"}'),
(50, '2017-02-07 03:13:27', 1, 'logout', ''),
(51, '2017-02-07 03:13:32', 3, 'login', ''),
(52, '2017-02-07 03:15:36', 3, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"TestTFO"}'),
(53, '2017-02-07 03:15:57', 3, 'logout', ''),
(54, '2017-02-07 03:16:28', 2, 'login', ''),
(55, '2017-02-07 03:17:01', 2, 'logout', ''),
(56, '2017-02-07 03:17:05', 1, 'login', ''),
(57, '2017-02-07 03:23:48', 1, 'logout', ''),
(58, '2017-02-07 03:23:52', 2, 'login', ''),
(59, '2017-02-07 03:27:02', 2, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"TestPI"}'),
(60, '2017-02-07 03:27:11', 2, 'logout', ''),
(61, '2017-02-07 03:27:15', 1, 'login', ''),
(62, '2017-02-07 03:28:17', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"Enzo"}'),
(63, '2017-02-07 03:28:58', 1, 'logout', ''),
(64, '2017-02-07 03:29:07', 3, 'login', ''),
(65, '2017-02-07 03:30:23', 3, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"TestTFO"}'),
(66, '2017-02-07 03:30:32', 3, 'logout', ''),
(67, '2017-02-07 03:30:36', 2, 'login', ''),
(68, '2017-02-07 03:30:55', 2, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"TestPI"}'),
(69, '2017-02-07 03:31:04', 2, 'logout', ''),
(70, '2017-02-07 03:31:10', 3, 'login', ''),
(71, '2017-02-07 03:31:30', 3, 'logout', ''),
(72, '2017-02-07 03:31:51', 6, 'login', ''),
(73, '2017-02-07 03:32:22', 6, 'logout', ''),
(74, '2017-02-07 03:32:30', 5, 'login', ''),
(75, '2017-02-07 03:34:10', 5, 'logout', ''),
(76, '2017-02-07 03:35:42', 5, 'login', ''),
(77, '2017-02-07 03:50:56', 5, 'logout', ''),
(78, '2017-02-07 03:53:21', 5, 'login', ''),
(79, '2017-02-07 03:58:20', 47, 'login', ''),
(80, '2017-02-07 03:59:00', 47, 'logout', ''),
(81, '2017-02-07 03:59:05', 5, 'login', ''),
(82, '2017-02-07 04:04:01', 5, 'logout', ''),
(83, '2017-02-07 04:04:09', 6, 'login', ''),
(84, '2017-02-07 04:04:36', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"TestI"}'),
(85, '2017-02-07 04:05:51', 6, 'logout', ''),
(86, '2017-02-07 04:06:08', 1, 'login', ''),
(87, '2017-02-07 04:06:21', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"Enzo"}'),
(88, '2017-02-07 04:06:32', 1, 'logout', ''),
(89, '2017-02-07 04:06:41', 6, 'login', ''),
(90, '2017-02-07 05:21:12', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"TestI"}'),
(91, '2017-02-07 05:21:57', 6, 'logout', ''),
(92, '2017-02-07 05:22:08', 3, 'login', ''),
(93, '2017-02-07 05:22:16', 3, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"TestTFO"}'),
(94, '2017-02-07 05:22:33', 3, 'logout', ''),
(95, '2017-02-07 05:22:40', 20, 'login', ''),
(96, '2017-02-07 05:22:51', 20, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"TestECS"}'),
(97, '2017-02-07 05:22:57', 20, 'logout', ''),
(98, '2017-02-07 05:23:09', 7, 'login', ''),
(99, '2017-02-07 05:23:31', 7, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"TestECM"}'),
(100, '2017-02-07 05:23:36', 7, 'logout', ''),
(101, '2017-02-07 05:23:47', 20, 'login', ''),
(102, '2017-02-07 05:26:05', 20, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"TestECS"}'),
(103, '2017-02-07 05:26:42', 20, 'logout', ''),
(104, '2017-02-07 05:27:00', 6, 'login', ''),
(105, '2017-02-07 05:27:47', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"TestI"}'),
(106, '2017-02-07 05:28:13', 6, 'logout', ''),
(107, '2017-02-07 05:28:22', 20, 'login', ''),
(108, '2017-02-07 05:28:33', 20, 'logout', ''),
(109, '2017-02-07 05:29:13', 7, 'login', ''),
(110, '2017-02-07 05:31:46', 7, 'logout', ''),
(111, '2017-02-07 05:31:53', 7, 'login', ''),
(112, '2017-02-07 05:32:06', 7, 'logout', ''),
(113, '2017-02-07 05:32:10', 20, 'login', ''),
(114, '2017-02-07 05:32:19', 20, 'logout', ''),
(115, '2017-02-07 05:32:24', 20, 'login', ''),
(116, '2017-02-07 05:32:57', 20, 'logout', ''),
(117, '2017-02-07 05:33:01', 7, 'login', ''),
(118, '2017-02-07 05:33:05', 7, 'logout', ''),
(119, '2017-02-07 05:33:13', 4, 'login', ''),
(120, '2017-02-07 05:33:22', 4, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"TestECC"}'),
(121, '2017-02-07 05:33:28', 4, 'logout', ''),
(122, '2017-02-07 05:33:32', 20, 'login', ''),
(123, '2017-02-07 05:34:01', 20, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"TestECS"}'),
(124, '2017-02-07 05:34:07', 20, 'logout', ''),
(125, '2017-02-07 05:36:37', 6, 'login', ''),
(126, '2017-02-07 05:37:49', 6, 'logout', ''),
(127, '2017-02-07 05:37:53', 2, 'login', ''),
(128, '2017-02-07 05:39:02', 2, 'logout', ''),
(129, '2017-02-07 05:39:08', 2, 'login', ''),
(130, '2017-02-07 05:39:29', 2, 'logout', ''),
(131, '2017-02-07 05:39:32', 6, 'login', ''),
(132, '2017-02-07 05:39:47', 6, 'logout', ''),
(133, '2017-02-07 05:39:50', 9, 'login', ''),
(134, '2017-02-07 05:40:01', 9, 'logout', ''),
(135, '2017-02-07 05:41:53', 6, 'login', ''),
(136, '2017-02-07 05:51:02', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"TestI"}'),
(137, '2017-02-07 05:52:33', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Refresher","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Ref","<span style=''color:magenta''>submitter<\\/span>":"TestI"}'),
(138, '2017-02-07 05:53:26', 6, 'logout', ''),
(139, '2017-02-07 06:09:42', 6, 'login', ''),
(140, '2017-02-07 06:55:39', 6, 'logout', ''),
(141, '2017-02-07 06:55:46', 2, 'login', ''),
(142, '2017-02-07 06:56:02', 2, 'logout', ''),
(143, '2017-02-07 06:56:07', 6, 'login', ''),
(144, '2017-02-07 06:56:22', 6, 'logout', ''),
(145, '2017-02-07 06:56:26', 2, 'login', ''),
(146, '2017-02-07 07:24:32', 2, 'logout', ''),
(147, '2017-02-07 07:26:01', 2, 'login', ''),
(148, '2017-02-07 07:26:22', 2, 'logout', ''),
(149, '2017-02-07 07:26:31', 2, 'login', ''),
(150, '2017-02-07 07:32:04', 2, 'logout', ''),
(151, '2017-02-07 07:32:11', 6, 'login', ''),
(152, '2017-02-07 07:52:07', 6, 'logout', ''),
(153, '2017-02-07 07:52:11', 2, 'login', ''),
(154, '2017-02-07 08:10:52', 2, 'logout', ''),
(155, '2017-02-07 08:11:58', 2, 'login', ''),
(156, '2017-02-07 08:42:31', 2, 'logout', ''),
(157, '2017-02-07 08:42:36', 2, 'login', ''),
(158, '2017-02-08 01:38:48', 2, 'login', ''),
(159, '2017-02-08 03:57:03', 2, 'logout', ''),
(160, '2017-02-08 03:57:06', 6, 'login', ''),
(161, '2017-02-08 03:57:31', 6, 'logout', ''),
(162, '2017-02-08 03:57:37', 1, 'login', ''),
(163, '2017-02-08 03:58:18', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test New Function","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Tes","<span style=''color:magenta''>submitter<\\/span>":"Enzo"}'),
(164, '2017-02-08 03:58:45', 1, 'logout', ''),
(165, '2017-02-08 03:58:49', 3, 'login', ''),
(166, '2017-02-08 03:58:57', 3, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test New Function","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Tes","<span style=''color:magenta''>submitter<\\/span>":"TestTFO"}'),
(167, '2017-02-08 03:59:06', 3, 'logout', ''),
(168, '2017-02-08 03:59:11', 2, 'login', ''),
(169, '2017-02-08 04:07:43', 2, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test New Function","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Tes","<span style=''color:magenta''>submitter<\\/span>":"TestPI"}'),
(170, '2017-02-08 04:07:51', 2, 'logout', ''),
(171, '2017-02-08 04:07:54', 5, 'login', ''),
(172, '2017-02-08 05:42:42', 5, 'logout', ''),
(173, '2017-02-08 05:42:46', 6, 'login', ''),
(174, '2017-02-08 05:43:03', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test New Function","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Tes","<span style=''color:magenta''>submitter<\\/span>":"TestI"}'),
(175, '2017-02-08 05:43:08', 6, 'logout', ''),
(176, '2017-02-08 05:43:12', 1, 'login', ''),
(177, '2017-02-08 05:43:24', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test New Function","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Tes","<span style=''color:magenta''>submitter<\\/span>":"Enzo"}'),
(178, '2017-02-08 05:43:28', 1, 'logout', ''),
(179, '2017-02-08 05:43:44', 3, 'login', ''),
(180, '2017-02-08 05:43:50', 3, 'logout', ''),
(181, '2017-02-08 05:43:53', 6, 'login', ''),
(182, '2017-02-08 05:44:08', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test New Function","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Tes","<span style=''color:magenta''>submitter<\\/span>":"TestI"}'),
(183, '2017-02-08 05:44:12', 6, 'logout', ''),
(184, '2017-02-08 05:44:15', 3, 'login', ''),
(185, '2017-02-08 05:44:26', 3, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test New Function","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Tes","<span style=''color:magenta''>submitter<\\/span>":"TestTFO"}'),
(186, '2017-02-08 05:44:34', 3, 'logout', ''),
(187, '2017-02-08 05:44:42', 20, 'login', ''),
(188, '2017-02-08 05:44:51', 20, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test New Function","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Tes","<span style=''color:magenta''>submitter<\\/span>":"TestECS"}'),
(189, '2017-02-08 05:44:55', 20, 'logout', ''),
(190, '2017-02-08 05:45:02', 7, 'login', ''),
(191, '2017-02-08 05:45:13', 7, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test New Function","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Tes","<span style=''color:magenta''>submitter<\\/span>":"TestECM"}'),
(192, '2017-02-08 05:45:35', 7, 'logout', ''),
(193, '2017-02-08 05:45:47', 20, 'login', ''),
(194, '2017-02-08 05:46:15', 20, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test New Function","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Tes","<span style=''color:magenta''>submitter<\\/span>":"TestECS"}'),
(195, '2017-02-08 05:46:26', 20, 'logout', ''),
(196, '2017-02-08 05:46:32', 6, 'login', ''),
(197, '2017-02-08 05:46:54', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test New Function","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Tes","<span style=''color:magenta''>submitter<\\/span>":"TestI"}'),
(198, '2017-02-08 05:47:01', 6, 'logout', ''),
(199, '2017-02-08 05:47:05', 4, 'login', ''),
(200, '2017-02-08 05:47:17', 4, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test New Function","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Tes","<span style=''color:magenta''>submitter<\\/span>":"TestECC"}'),
(201, '2017-02-08 05:47:24', 4, 'logout', ''),
(202, '2017-02-08 05:47:29', 20, 'login', ''),
(203, '2017-02-08 05:47:50', 20, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test New Function","<span style=''color:magenta''>module<\\/span>":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Tes","<span style=''color:magenta''>submitter<\\/span>":"TestECS"}'),
(204, '2017-02-08 05:47:56', 20, 'logout', ''),
(205, '2017-02-08 05:48:00', 6, 'login', ''),
(206, '2017-02-08 05:48:19', 6, 'logout', ''),
(207, '2017-02-08 05:48:28', 2, 'login', ''),
(208, '2017-02-08 05:56:40', 2, 'logout', ''),
(209, '2017-02-08 05:56:44', 3, 'login', ''),
(210, '2017-02-08 05:56:50', 3, 'logout', ''),
(211, '2017-02-08 05:56:53', 1, 'login', ''),
(212, '2017-02-08 05:57:31', 1, 'logout', ''),
(213, '2017-02-08 05:57:39', 6, 'login', ''),
(214, '2017-02-08 05:58:08', 6, 'logout', ''),
(215, '2017-02-08 05:58:15', 9, 'login', ''),
(216, '2017-02-08 05:58:18', 9, 'logout', ''),
(217, '2017-02-08 05:58:22', 6, 'login', ''),
(218, '2017-02-08 05:59:35', 6, 'logout', ''),
(219, '2017-02-08 05:59:40', 2, 'login', ''),
(220, '2017-02-08 06:27:32', 2, 'logout', ''),
(221, '2017-02-08 06:28:11', 10, 'login', ''),
(222, '2017-02-08 06:55:31', 10, 'logout', ''),
(223, '2017-02-08 06:55:35', 2, 'login', ''),
(224, '2017-02-08 07:00:26', 2, 'logout', ''),
(225, '2017-02-08 07:00:29', 10, 'login', ''),
(226, '2017-02-08 08:17:47', 10, 'logout', ''),
(227, '2017-02-08 08:17:52', 10, 'login', ''),
(228, '2017-02-08 08:18:08', 10, 'logout', ''),
(229, '2017-02-08 08:18:14', 10, 'login', ''),
(230, '2017-02-08 08:51:52', 10, 'logout', ''),
(231, '2017-02-08 08:51:56', 10, 'login', ''),
(232, '2017-02-08 08:52:46', 10, 'logout', ''),
(233, '2017-02-08 08:52:52', 2, 'login', ''),
(234, '2017-02-08 09:34:38', 2, 'logout', ''),
(235, '2017-02-08 09:34:43', 10, 'login', ''),
(236, '2017-02-08 09:53:17', 10, 'login', ''),
(237, '2017-02-08 09:53:20', 10, 'logout', ''),
(238, '2017-02-08 09:56:16', 2, 'login', ''),
(239, '2017-02-08 10:00:34', 2, 'logout', ''),
(240, '2017-02-08 10:00:39', 79, 'login', ''),
(241, '2017-02-08 10:01:31', 79, 'logout', ''),
(242, '2017-02-08 10:01:35', 2, 'login', ''),
(243, '2017-02-08 10:04:38', 2, 'logout', ''),
(244, '2017-02-08 10:08:18', 80, 'login', ''),
(245, '2017-02-08 10:08:39', 80, 'logout', ''),
(246, '2017-02-08 10:08:55', 79, 'login', ''),
(247, '2017-02-08 10:22:51', 79, 'logout', ''),
(248, '2017-02-08 10:23:07', 2, 'login', ''),
(249, '2017-02-08 01:32:02', 2, 'logout', ''),
(250, '2017-02-08 01:32:14', 5, 'login', ''),
(251, '2017-02-09 01:17:54', 1, 'login', ''),
(252, '2017-02-09 02:01:15', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"","<span style=''color:magenta''>submitter<\\/span>":"Enzo"}'),
(253, '2017-02-09 02:06:14', 1, 'logout', ''),
(254, '2017-02-09 02:08:07', 3, 'login', ''),
(255, '2017-02-09 02:08:09', 3, 'logout', ''),
(256, '2017-02-09 02:08:21', 3, 'login', ''),
(257, '2017-02-09 02:08:23', 3, 'logout', ''),
(258, '2017-02-09 02:09:00', 3, 'login', ''),
(259, '2017-02-09 02:09:02', 3, 'logout', ''),
(260, '2017-02-09 02:10:02', 3, 'login', ''),
(261, '2017-02-09 02:10:04', 3, 'logout', ''),
(262, '2017-02-09 02:10:08', 3, 'login', ''),
(263, '2017-02-09 02:11:15', 3, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"","<span style=''color:magenta''>submitter<\\/span>":"TestTFO"}'),
(264, '2017-02-09 02:11:19', 3, 'logout', ''),
(265, '2017-02-09 02:11:24', 2, 'login', ''),
(266, '2017-02-09 02:11:39', 2, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"","<span style=''color:magenta''>submitter<\\/span>":"TestPI"}'),
(267, '2017-02-09 02:11:44', 2, 'logout', ''),
(268, '2017-02-09 02:11:49', 5, 'login', ''),
(269, '2017-02-09 02:12:35', 5, 'logout', ''),
(270, '2017-02-09 02:12:39', 6, 'login', ''),
(271, '2017-02-09 02:13:44', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"","<span style=''color:magenta''>submitter<\\/span>":"TestI"}'),
(272, '2017-02-09 02:13:47', 6, 'logout', ''),
(273, '2017-02-09 02:13:51', 1, 'login', ''),
(274, '2017-02-09 02:15:00', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"\\u8bf7\\u8f93\\u5165\\u9879\\u76ee\\u540d\\u79f0","\\u6a21\\u5757":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"","<span style=''color:magenta''>submitter<\\/span>":"Enzo"}'),
(275, '2017-02-09 02:15:40', 1, 'logout', ''),
(276, '2017-02-09 02:15:51', 3, 'login', ''),
(277, '2017-02-09 02:16:01', 3, 'logout', ''),
(278, '2017-02-09 02:16:05', 6, 'login', ''),
(279, '2017-02-09 02:17:08', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test Databank","\\u6a21\\u5757":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"","<span style=''color:magenta''>submitter<\\/span>":"TestI"}'),
(280, '2017-02-09 02:17:30', 6, 'logout', ''),
(281, '2017-02-09 02:17:35', 3, 'login', ''),
(282, '2017-02-09 02:17:43', 3, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test Databank","\\u6a21\\u5757":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"","<span style=''color:magenta''>submitter<\\/span>":"TestTFO"}'),
(283, '2017-02-09 02:17:46', 3, 'logout', ''),
(284, '2017-02-09 02:17:51', 20, 'login', ''),
(285, '2017-02-09 02:18:00', 20, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test Databank","\\u6a21\\u5757":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"","<span style=''color:magenta''>submitter<\\/span>":"TestECS"}'),
(286, '2017-02-09 02:18:24', 20, 'logout', ''),
(287, '2017-02-09 02:18:27', 7, 'login', ''),
(288, '2017-02-09 02:18:44', 7, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test Databank","\\u6a21\\u5757":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"","<span style=''color:magenta''>submitter<\\/span>":"TestECM"}'),
(289, '2017-02-09 02:18:47', 7, 'logout', ''),
(290, '2017-02-09 02:18:53', 20, 'login', ''),
(291, '2017-02-09 02:19:28', 20, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test Databank","\\u6a21\\u5757":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"","<span style=''color:magenta''>submitter<\\/span>":"TestECS"}'),
(292, '2017-02-09 02:19:33', 20, 'logout', ''),
(293, '2017-02-09 02:19:39', 6, 'login', ''),
(294, '2017-02-09 02:20:09', 6, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test Databank","\\u6a21\\u5757":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"","<span style=''color:magenta''>submitter<\\/span>":"TestI"}'),
(295, '2017-02-09 02:20:12', 6, 'logout', ''),
(296, '2017-02-09 02:20:15', 4, 'login', ''),
(297, '2017-02-09 02:20:29', 4, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test Databank","\\u6a21\\u5757":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"","<span style=''color:magenta''>submitter<\\/span>":"TestECC"}'),
(298, '2017-02-09 02:20:40', 4, 'logout', ''),
(299, '2017-02-09 02:20:43', 4, 'login', ''),
(300, '2017-02-09 02:20:51', 4, 'logout', ''),
(301, '2017-02-09 02:20:57', 20, 'login', ''),
(302, '2017-02-09 02:21:35', 20, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test Databank","\\u6a21\\u5757":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"","<span style=''color:magenta''>submitter<\\/span>":"TestECS"}'),
(303, '2017-02-09 02:21:39', 20, 'logout', ''),
(304, '2017-02-09 02:21:54', 6, 'login', ''),
(305, '2017-02-09 07:06:55', 6, 'logout', ''),
(306, '2017-02-09 07:06:59', 2, 'login', ''),
(307, '2017-02-09 07:09:00', 2, 'logout', ''),
(308, '2017-02-09 07:09:18', 81, 'login', ''),
(309, '2017-02-09 07:09:55', 81, 'logout', ''),
(310, '2017-02-09 07:10:04', 81, 'login', ''),
(311, '2017-02-09 07:53:31', 81, 'logout', ''),
(312, '2017-02-09 07:53:35', 1, 'login', ''),
(313, '2017-02-09 07:54:14', 1, 'logout', ''),
(314, '2017-02-09 07:54:17', 2, 'login', ''),
(315, '2017-02-09 07:55:42', 2, 'logout', ''),
(316, '2017-02-09 07:55:51', 79, 'login', ''),
(317, '2017-02-09 07:56:33', 79, 'logout', ''),
(318, '2017-02-09 07:56:37', 6, 'login', ''),
(319, '2017-02-09 07:57:11', 6, 'logout', ''),
(320, '2017-02-09 07:57:17', 14, 'login', ''),
(321, '2017-02-09 07:57:19', 14, 'logout', ''),
(322, '2017-02-09 07:57:24', 81, 'login', ''),
(323, '2017-02-09 08:14:36', 81, 'logout', ''),
(324, '2017-02-09 08:14:41', 2, 'login', ''),
(325, '2017-02-09 08:15:49', 2, 'logout', ''),
(326, '2017-02-09 08:15:56', 82, 'login', ''),
(327, '2017-02-09 08:16:55', 82, 'logout', ''),
(328, '2017-02-09 08:16:58', 6, 'login', ''),
(329, '2017-02-09 11:29:51', 6, 'logout', ''),
(330, '2017-02-09 11:29:56', 11, 'login', ''),
(331, '2017-02-09 11:30:11', 11, 'logout', ''),
(332, '2017-02-09 11:30:14', 79, 'login', ''),
(333, '2017-02-09 11:30:17', 79, 'logout', ''),
(334, '2017-02-09 11:30:22', 11, 'login', ''),
(335, '2017-02-09 12:18:12', 11, 'logout', ''),
(336, '2017-02-09 12:18:18', 79, 'login', ''),
(337, '2017-02-09 12:32:08', 79, 'logout', ''),
(338, '2017-02-09 12:32:12', 11, 'login', ''),
(339, '2017-02-09 12:34:53', 11, 'logout', ''),
(340, '2017-02-09 12:34:58', 21, 'login', ''),
(341, '2017-02-09 12:35:56', 21, 'logout', ''),
(342, '2017-02-09 12:36:01', 11, 'login', ''),
(343, '2017-02-09 12:39:40', 11, 'logout', ''),
(344, '2017-02-09 12:39:49', 21, 'login', ''),
(345, '2017-02-11 03:54:05', 1, 'login', ''),
(346, '2017-02-11 04:04:53', 1, 'logout', ''),
(347, '2017-02-11 04:04:58', 1, 'login', ''),
(348, '2017-02-11 05:37:31', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"Test me","\\u6a21\\u5757":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"Tes","<span style=''color:magenta''>submitter<\\/span>":"Enzo"}'),
(349, '2017-02-11 05:38:44', 1, 'submit', '{"\\u9879\\u76ee\\u540d\\u79f0":"22222","\\u6a21\\u5757":"<span style=''color:magenta''>not_applicable<\\/span>","\\u5e8f\\u53f7":"222","<span style=''color:magenta''>submitter<\\/span>":"Enzo"}'),
(350, '2017-02-11 06:59:37', 1, 'logout', ''),
(351, '2017-02-11 06:59:41', 79, 'login', ''),
(352, '2017-02-11 07:00:12', 79, 'logout', ''),
(353, '2017-02-11 07:00:20', 11, 'login', ''),
(354, '2017-02-11 08:33:37', 79, 'login', ''),
(355, '2017-02-11 08:34:21', 79, 'logout', ''),
(356, '2017-02-11 08:34:27', 11, 'login', ''),
(357, '2017-02-11 09:53:09', 11, 'logout', ''),
(358, '2017-02-11 09:53:28', 6, 'login', ''),
(359, '2017-02-14 01:53:39', 6, 'login', ''),
(360, '2017-02-14 02:24:59', 6, 'login', ''),
(361, '2017-02-14 02:35:39', 6, 'logout', ''),
(362, '2017-02-14 02:35:47', 6, 'login', ''),
(363, '2017-02-14 09:45:17', 6, 'logout', ''),
(364, '2017-02-14 09:45:25', 1, 'login', ''),
(365, '2017-02-16 07:56:05', 1, 'login', ''),
(366, '2017-02-17 02:01:09', 1, 'login', ''),
(367, '2017-02-17 02:07:23', 1, 'logout', ''),
(368, '2017-02-17 02:07:26', 1, 'logout', ''),
(369, '2017-02-17 02:07:32', 79, 'login', ''),
(370, '2017-02-17 02:09:40', 79, 'logout', ''),
(371, '2017-02-17 02:09:45', 6, 'login', ''),
(372, '2017-02-17 02:10:02', 6, 'logout', ''),
(373, '2017-02-17 02:10:06', 2, 'login', ''),
(374, '2017-02-17 02:20:37', 2, 'logout', ''),
(375, '2017-02-17 02:20:42', 79, 'login', ''),
(376, '2017-02-17 02:25:25', 79, 'logout', ''),
(377, '2017-02-17 02:25:30', 79, 'login', ''),
(378, '2017-02-17 02:55:36', 79, 'logout', ''),
(379, '2017-02-17 02:56:11', 6, 'login', ''),
(380, '2017-02-17 05:25:21', 6, 'logout', ''),
(381, '2017-02-17 05:25:26', 6, 'login', ''),
(382, '2017-02-17 05:25:46', 6, 'logout', ''),
(383, '2017-02-17 05:25:52', 6, 'login', ''),
(384, '2017-02-17 05:41:24', 6, 'logout', ''),
(385, '2017-02-17 05:41:31', 6, 'login', ''),
(386, '2017-02-17 05:55:59', 6, 'logout', ''),
(387, '2017-02-17 05:56:04', 11, 'login', ''),
(388, '2017-02-17 08:45:56', 11, 'logout', ''),
(389, '2017-02-17 08:46:03', 79, 'login', ''),
(390, '2017-02-17 10:44:39', 79, 'logout', ''),
(391, '2017-02-17 10:45:34', 47, 'login', ''),
(392, '2017-02-17 10:46:48', 47, 'logout', ''),
(393, '2017-02-17 10:46:56', 79, 'login', ''),
(394, '2017-02-17 10:47:19', 79, 'logout', ''),
(395, '2017-02-17 10:47:24', 80, 'login', ''),
(396, '2017-02-18 02:43:33', 80, 'login', ''),
(397, '2017-02-18 03:34:45', 80, 'logout', ''),
(398, '2017-02-18 03:34:51', 21, 'login', '');

-- --------------------------------------------------------

--
-- Table structure for table `mc_info`
--

CREATE TABLE IF NOT EXISTS `mc_info` (
  `mc_org` varchar(255) NOT NULL,
  `mc_person` varchar(255) NOT NULL,
  `mc_lead` varchar(255) NOT NULL,
  `mc_remarks` longtext NOT NULL,
`mc_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='mc means `multi-center`' AUTO_INCREMENT=10 ;

--
-- Dumping data for table `mc_info`
--

INSERT INTO `mc_info` (`mc_org`, `mc_person`, `mc_lead`, `mc_remarks`, `mc_id`, `project_id`) VALUES
('123', '123', 'ç‰µå¤´', '123', 1, 2),
('zxc', 'zxc', 'Participate', 'zxc', 2, 6),
('dsa', 'dsa', 'Lead', 'dsa', 3, 6),
('s', 's', 'Lead', 'las', 4, 6),
('latest', 'latest', 'Lead', 'latest', 5, 6),
('a', 'a', 'Lead', 'a', 6, 7),
('b', 'b', 'Participate', 'b', 7, 7),
('2222', '3', 'ç‰µå¤´', '3', 8, 13),
('asd', 'asd', 'å‚åŠ ', 'Refresher', 9, 108);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
`notification_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `project_name` text NOT NULL,
  `project_owner` text NOT NULL,
  `todo` text NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=420 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`notification_id`, `role_id`, `project_id`, `title`, `email`, `project_name`, `project_owner`, `todo`, `created_at`) VALUES
(42, 3, 107, 'qwiqgeiqu', 'TFO@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-04 09:29:39'),
(43, 14, 107, 'qwiqgeiqu', 'all@test.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-04 09:29:39'),
(44, 27, 107, 'qwiqgeiqu', 'shiyanjigou', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-04 09:29:39'),
(45, 1, 107, 'qwiqgeiqu', 'enzo@mail.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-04 09:29:39'),
(46, 14, 108, '<span style=''color:magenta''>status_1_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-07 03:13:17'),
(47, 27, 108, '<span style=''color:magenta''>status_1_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-07 03:13:17'),
(48, 3, 108, '<span style=''color:magenta''>status_1_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-07 03:13:17'),
(49, 1, 108, '<span style=''color:magenta''>status_1_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:13:18'),
(50, 2, 108, '<span style=''color:magenta''>status_2_0</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>PI_todo_2_0</span>', '2017-02-07 03:15:35'),
(51, 14, 108, '<span style=''color:magenta''>status_2_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:15:35'),
(52, 27, 108, '<span style=''color:magenta''>status_2_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:15:35'),
(53, 3, 108, '<span style=''color:magenta''>status_2_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:15:36'),
(54, 1, 108, '<span style=''color:magenta''>status_2_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:15:36'),
(55, 2, 108, '<span style=''color:magenta''>not_applicable</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:26:57'),
(56, 6, 108, '<span style=''color:magenta''>not_applicable</span>', 'I@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:26:57'),
(57, 14, 108, '<span style=''color:magenta''>not_applicable</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:26:58'),
(58, 27, 108, '<span style=''color:magenta''>not_applicable</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:26:58'),
(59, 3, 108, '<span style=''color:magenta''>not_applicable</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:26:58'),
(60, 20, 108, '<span style=''color:magenta''>not_applicable</span>', 'ECS@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:26:59'),
(61, 44, 108, '<span style=''color:magenta''>not_applicable</span>', 'lunliweiyuanhuimishu', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:26:59'),
(62, 36, 108, '<span style=''color:magenta''>not_applicable</span>', 'lunliweiyuanhuiweiyuan', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:26:59'),
(63, 7, 108, '<span style=''color:magenta''>not_applicable</span>', 'ECM@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:27:00'),
(64, 18, 108, '<span style=''color:magenta''>not_applicable</span>', 'all@test.com', 'Refresher', 'Enzo', 'N/A', '2017-02-07 03:27:00'),
(65, 37, 108, '<span style=''color:magenta''>not_applicable</span>', 'lunliweiyuanhuizhuren', 'Refresher', 'Enzo', 'N/A', '2017-02-07 03:27:00'),
(66, 4, 108, '<span style=''color:magenta''>not_applicable</span>', 'ECC@admin.com', 'Refresher', 'Enzo', 'N/A', '2017-02-07 03:27:01'),
(67, 1, 108, '<span style=''color:magenta''>not_applicable</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:27:01'),
(68, 14, 108, '<span style=''color:magenta''>status_1_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-07 03:28:15'),
(69, 27, 108, '<span style=''color:magenta''>status_1_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-07 03:28:16'),
(70, 3, 108, '<span style=''color:magenta''>status_1_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-07 03:28:17'),
(71, 1, 108, '<span style=''color:magenta''>status_1_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:28:17'),
(72, 2, 108, '<span style=''color:magenta''>status_2_0</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>PI_todo_2_0</span>', '2017-02-07 03:30:21'),
(73, 14, 108, '<span style=''color:magenta''>status_2_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:30:21'),
(74, 27, 108, '<span style=''color:magenta''>status_2_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:30:22'),
(75, 3, 108, '<span style=''color:magenta''>status_2_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:30:22'),
(76, 1, 108, '<span style=''color:magenta''>status_2_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:30:23'),
(77, 2, 108, '<span style=''color:magenta''>status_3_0</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:30:53'),
(78, 5, 108, '<span style=''color:magenta''>status_3_0</span>', 'PRE@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>PRE_todo_3_0</span>', '2017-02-07 03:30:53'),
(79, 24, 108, '<span style=''color:magenta''>status_3_0</span>', '123', 'Refresher', 'Enzo', '<span style=''color:magenta''>PRE_todo_3_0</span>', '2017-02-07 03:30:54'),
(80, 3, 108, '<span style=''color:magenta''>status_3_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:30:54'),
(81, 14, 108, '<span style=''color:magenta''>status_3_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:30:54'),
(82, 27, 108, '<span style=''color:magenta''>status_3_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:30:55'),
(83, 1, 108, '<span style=''color:magenta''>status_3_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 03:30:55'),
(84, 2, 108, '<span style=''color:magenta''>status_4_0</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 04:04:34'),
(85, 6, 108, '<span style=''color:magenta''>status_4_0</span>', 'I@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 04:04:34'),
(86, 14, 108, '<span style=''color:magenta''>status_4_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 04:04:34'),
(87, 27, 108, '<span style=''color:magenta''>status_4_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 04:04:35'),
(88, 3, 108, '<span style=''color:magenta''>status_4_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 04:04:35'),
(89, 1, 108, '<span style=''color:magenta''>status_4_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>applicant_todo_4_0</span>', '2017-02-07 04:04:35'),
(90, 2, 108, '<span style=''color:magenta''>status_5_0</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 04:06:19'),
(91, 6, 108, '<span style=''color:magenta''>status_5_0</span>', 'I@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>I_todo_5_0</span>', '2017-02-07 04:06:20'),
(92, 3, 108, '<span style=''color:magenta''>status_5_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 04:06:20'),
(93, 14, 108, '<span style=''color:magenta''>status_5_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 04:06:20'),
(94, 27, 108, '<span style=''color:magenta''>status_5_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 04:06:20'),
(95, 1, 108, '<span style=''color:magenta''>status_5_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 04:06:21'),
(96, 2, 108, '<span style=''color:magenta''>status_6_0</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:21:10'),
(97, 6, 108, '<span style=''color:magenta''>status_6_0</span>', 'I@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:21:11'),
(98, 3, 108, '<span style=''color:magenta''>status_6_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>tfo_todo_6_0</span>', '2017-02-07 05:21:11'),
(99, 14, 108, '<span style=''color:magenta''>status_6_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>tfo_todo_6_0</span>', '2017-02-07 05:21:11'),
(100, 27, 108, '<span style=''color:magenta''>status_6_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>tfo_todo_6_0</span>', '2017-02-07 05:21:12'),
(101, 1, 108, '<span style=''color:magenta''>status_6_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:21:12'),
(102, 2, 108, '<span style=''color:magenta''>status_7_0</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:22:15'),
(103, 6, 108, '<span style=''color:magenta''>status_7_0</span>', 'I@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:22:15'),
(104, 3, 108, '<span style=''color:magenta''>status_7_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:22:15'),
(105, 14, 108, '<span style=''color:magenta''>status_7_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:22:16'),
(106, 27, 108, '<span style=''color:magenta''>status_7_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:22:16'),
(107, 1, 108, '<span style=''color:magenta''>status_7_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:22:16'),
(108, 2, 108, '<span style=''color:magenta''>status_8_0</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:22:49'),
(109, 6, 108, '<span style=''color:magenta''>status_8_0</span>', 'I@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:22:49'),
(110, 3, 108, '<span style=''color:magenta''>status_8_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:22:49'),
(111, 14, 108, '<span style=''color:magenta''>status_8_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:22:50'),
(112, 27, 108, '<span style=''color:magenta''>status_8_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:22:50'),
(113, 20, 108, '<span style=''color:magenta''>status_8_0</span>', 'ECS@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:22:50'),
(114, 44, 108, '<span style=''color:magenta''>status_8_0</span>', 'lunliweiyuanhuimishu', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:22:51'),
(115, 1, 108, '<span style=''color:magenta''>status_8_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:22:51'),
(116, 2, 108, '<span style=''color:magenta''>status_9_0</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:23:28'),
(117, 6, 108, '<span style=''color:magenta''>status_9_0</span>', 'I@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:23:28'),
(118, 3, 108, '<span style=''color:magenta''>status_9_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:23:29'),
(119, 14, 108, '<span style=''color:magenta''>status_9_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:23:29'),
(120, 27, 108, '<span style=''color:magenta''>status_9_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:23:29'),
(121, 20, 108, '<span style=''color:magenta''>status_9_0</span>', 'ECS@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>ECS_todo_9_0</span>', '2017-02-07 05:23:29'),
(122, 44, 108, '<span style=''color:magenta''>status_9_0</span>', 'lunliweiyuanhuimishu', 'Refresher', 'Enzo', '<span style=''color:magenta''>ECS_todo_9_0</span>', '2017-02-07 05:23:30'),
(123, 7, 108, '<span style=''color:magenta''>status_9_0</span>', 'ECM@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:23:30'),
(124, 36, 108, '<span style=''color:magenta''>status_9_0</span>', 'lunliweiyuanhuiweiyuan', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:23:30'),
(125, 1, 108, '<span style=''color:magenta''>status_9_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:23:31'),
(126, 2, 108, '<span style=''color:magenta''>status_10_0</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:26:02'),
(127, 6, 108, '<span style=''color:magenta''>status_10_0</span>', 'I@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>I_todo_10_0</span>', '2017-02-07 05:26:03'),
(128, 3, 108, '<span style=''color:magenta''>status_10_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:26:03'),
(129, 14, 108, '<span style=''color:magenta''>status_10_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:26:03'),
(130, 27, 108, '<span style=''color:magenta''>status_10_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:26:04'),
(131, 20, 108, '<span style=''color:magenta''>status_10_0</span>', 'ECS@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:26:04'),
(132, 44, 108, '<span style=''color:magenta''>status_10_0</span>', 'lunliweiyuanhuimishu', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:26:04'),
(133, 7, 108, '<span style=''color:magenta''>status_10_0</span>', 'ECM@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:26:05'),
(134, 36, 108, '<span style=''color:magenta''>status_10_0</span>', 'lunliweiyuanhuiweiyuan', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:26:05'),
(135, 1, 108, '<span style=''color:magenta''>status_10_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:26:05'),
(136, 2, 108, '<span style=''color:magenta''>status_11_0</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:27:44'),
(137, 6, 108, '<span style=''color:magenta''>status_11_0</span>', 'I@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:27:44'),
(138, 14, 108, '<span style=''color:magenta''>status_11_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:27:44'),
(139, 27, 108, '<span style=''color:magenta''>status_11_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:27:45'),
(140, 3, 108, '<span style=''color:magenta''>status_11_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:27:45'),
(141, 20, 108, '<span style=''color:magenta''>status_11_0</span>', 'ECS@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:27:45'),
(142, 44, 108, '<span style=''color:magenta''>status_11_0</span>', 'lunliweiyuanhuimishu', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:27:46'),
(143, 36, 108, '<span style=''color:magenta''>status_11_0</span>', 'lunliweiyuanhuiweiyuan', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:27:46'),
(144, 7, 108, '<span style=''color:magenta''>status_11_0</span>', 'ECM@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:27:46'),
(145, 1, 108, '<span style=''color:magenta''>status_11_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:27:47'),
(146, 2, 108, '<span style=''color:magenta''>status_12_0</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:19'),
(147, 6, 108, '<span style=''color:magenta''>status_12_0</span>', 'I@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:19'),
(148, 3, 108, '<span style=''color:magenta''>status_12_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:19'),
(149, 14, 108, '<span style=''color:magenta''>status_12_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:20'),
(150, 27, 108, '<span style=''color:magenta''>status_12_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:20'),
(151, 20, 108, '<span style=''color:magenta''>status_12_0</span>', 'ECS@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>ECS_todo_12_0</span>', '2017-02-07 05:33:20'),
(152, 44, 108, '<span style=''color:magenta''>status_12_0</span>', 'lunliweiyuanhuimishu', 'Refresher', 'Enzo', '<span style=''color:magenta''>ECS_todo_12_0</span>', '2017-02-07 05:33:21'),
(153, 7, 108, '<span style=''color:magenta''>status_12_0</span>', 'ECM@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:21'),
(154, 36, 108, '<span style=''color:magenta''>status_12_0</span>', 'lunliweiyuanhuiweiyuan', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:21'),
(155, 4, 108, '<span style=''color:magenta''>status_12_0</span>', 'ECC@admin.com', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:33:21'),
(156, 18, 108, '<span style=''color:magenta''>status_12_0</span>', 'all@test.com', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:33:22'),
(157, 37, 108, '<span style=''color:magenta''>status_12_0</span>', 'lunliweiyuanhuizhuren', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:33:22'),
(158, 1, 108, '<span style=''color:magenta''>status_12_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:22'),
(159, 2, 108, '<span style=''color:magenta''>status_13_0</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:56'),
(160, 6, 108, '<span style=''color:magenta''>status_13_0</span>', 'I@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:57'),
(161, 27, 108, '<span style=''color:magenta''>status_13_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:57'),
(162, 3, 108, '<span style=''color:magenta''>status_13_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:57'),
(163, 14, 108, '<span style=''color:magenta''>status_13_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:58'),
(164, 44, 108, '<span style=''color:magenta''>status_13_0</span>', 'lunliweiyuanhuimishu', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:58'),
(165, 20, 108, '<span style=''color:magenta''>status_13_0</span>', 'ECS@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:58'),
(166, 36, 108, '<span style=''color:magenta''>status_13_0</span>', 'lunliweiyuanhuiweiyuan', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:59'),
(167, 7, 108, '<span style=''color:magenta''>status_13_0</span>', 'ECM@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:33:59'),
(168, 37, 108, '<span style=''color:magenta''>status_13_0</span>', 'lunliweiyuanhuizhuren', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:33:59'),
(169, 4, 108, '<span style=''color:magenta''>status_13_0</span>', 'ECC@admin.com', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:34:00'),
(170, 18, 108, '<span style=''color:magenta''>status_13_0</span>', 'all@test.com', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:34:00'),
(171, 1, 108, '<span style=''color:magenta''>status_13_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:34:01'),
(172, 2, 108, '<span style=''color:magenta''>status_14_0</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:50:57'),
(173, 6, 108, '<span style=''color:magenta''>status_14_0</span>', 'I@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:50:57'),
(174, 3, 108, '<span style=''color:magenta''>status_14_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:50:58'),
(175, 14, 108, '<span style=''color:magenta''>status_14_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:50:58'),
(176, 27, 108, '<span style=''color:magenta''>status_14_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:50:58'),
(177, 20, 108, '<span style=''color:magenta''>status_14_0</span>', 'ECS@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:50:59'),
(178, 44, 108, '<span style=''color:magenta''>status_14_0</span>', 'lunliweiyuanhuimishu', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:50:59'),
(179, 7, 108, '<span style=''color:magenta''>status_14_0</span>', 'ECM@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:50:59'),
(180, 36, 108, '<span style=''color:magenta''>status_14_0</span>', 'lunliweiyuanhuiweiyuan', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:51:00'),
(181, 4, 108, '<span style=''color:magenta''>status_14_0</span>', 'ECC@admin.com', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:51:00'),
(182, 18, 108, '<span style=''color:magenta''>status_14_0</span>', 'all@test.com', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:51:00'),
(183, 37, 108, '<span style=''color:magenta''>status_14_0</span>', 'lunliweiyuanhuizhuren', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:51:00'),
(184, 10, 108, '<span style=''color:magenta''>status_14_0</span>', '12345MC@gmail.com', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:51:01'),
(185, 11, 108, '<span style=''color:magenta''>status_14_0</span>', '12345BC@gmail.com', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:51:01'),
(186, 1, 108, '<span style=''color:magenta''>status_14_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>applicant_todo_14_0</span>', '2017-02-07 05:51:02'),
(187, 2, 108, '<span style=''color:magenta''>status_15_0</span>', 'PI@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:52:27'),
(188, 6, 108, '<span style=''color:magenta''>status_15_0</span>', 'I@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:52:27'),
(189, 5, 108, '<span style=''color:magenta''>status_15_0</span>', 'PRE@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:52:28'),
(190, 24, 108, '<span style=''color:magenta''>status_15_0</span>', '123', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:52:28'),
(191, 3, 108, '<span style=''color:magenta''>status_15_0</span>', 'TFO@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:52:29'),
(192, 14, 108, '<span style=''color:magenta''>status_15_0</span>', 'all@test.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:52:29'),
(193, 27, 108, '<span style=''color:magenta''>status_15_0</span>', 'shiyanjigou', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:52:29'),
(194, 20, 108, '<span style=''color:magenta''>status_15_0</span>', 'ECS@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:52:30'),
(195, 44, 108, '<span style=''color:magenta''>status_15_0</span>', 'lunliweiyuanhuimishu', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:52:30'),
(196, 7, 108, '<span style=''color:magenta''>status_15_0</span>', 'ECM@admin.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:52:31'),
(197, 36, 108, '<span style=''color:magenta''>status_15_0</span>', 'lunliweiyuanhuiweiyuan', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:52:31'),
(198, 4, 108, '<span style=''color:magenta''>status_15_0</span>', 'ECC@admin.com', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:52:31'),
(199, 18, 108, '<span style=''color:magenta''>status_15_0</span>', 'all@test.com', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:52:32'),
(200, 37, 108, '<span style=''color:magenta''>status_15_0</span>', 'lunliweiyuanhuizhuren', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:52:32'),
(201, 10, 108, '<span style=''color:magenta''>status_15_0</span>', '12345MC@gmail.com', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:52:33'),
(202, 11, 108, '<span style=''color:magenta''>status_15_0</span>', '12345BC@gmail.com', 'Refresher', 'Enzo', 'N/A', '2017-02-07 05:52:33'),
(203, 1, 108, '<span style=''color:magenta''>status_15_0</span>', 'enzo@mail.com', 'Refresher', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-07 05:52:33'),
(204, 27, 109, '<span style=''color:magenta''>status_1_0</span>', 'shiyanjigou', 'Test New Function', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-08 03:58:17'),
(205, 3, 109, '<span style=''color:magenta''>status_1_0</span>', 'TFO@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-08 03:58:17'),
(206, 14, 109, '<span style=''color:magenta''>status_1_0</span>', 'all@test.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-08 03:58:18'),
(207, 1, 109, '<span style=''color:magenta''>status_1_0</span>', 'enzo@mail.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 03:58:18'),
(208, 2, 109, '<span style=''color:magenta''>status_2_0</span>', 'PI@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>PI_todo_2_0</span>', '2017-02-08 03:58:56'),
(209, 27, 109, '<span style=''color:magenta''>status_2_0</span>', 'shiyanjigou', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 03:58:56'),
(210, 3, 109, '<span style=''color:magenta''>status_2_0</span>', 'TFO@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 03:58:57'),
(211, 14, 109, '<span style=''color:magenta''>status_2_0</span>', 'all@test.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 03:58:57'),
(212, 1, 109, '<span style=''color:magenta''>status_2_0</span>', 'enzo@mail.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 03:58:57'),
(213, 2, 109, '<span style=''color:magenta''>status_3_0</span>', 'PI@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 04:07:41'),
(214, 5, 109, '<span style=''color:magenta''>status_3_0</span>', 'PRE@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>PRE_todo_3_0</span>', '2017-02-08 04:07:41'),
(215, 19, 109, '<span style=''color:magenta''>status_3_0</span>', 'all@test.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>PRE_todo_3_0</span>', '2017-02-08 04:07:41'),
(216, 47, 109, '<span style=''color:magenta''>status_3_0</span>', 'Refresher@Refresher.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>PRE_todo_3_0</span>', '2017-02-08 04:07:42'),
(217, 3, 109, '<span style=''color:magenta''>status_3_0</span>', 'TFO@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 04:07:42'),
(218, 14, 109, '<span style=''color:magenta''>status_3_0</span>', 'all@test.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 04:07:42'),
(219, 27, 109, '<span style=''color:magenta''>status_3_0</span>', 'shiyanjigou', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 04:07:43'),
(220, 1, 109, '<span style=''color:magenta''>status_3_0</span>', 'enzo@mail.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 04:07:43'),
(221, 2, 109, '<span style=''color:magenta''>status_4_0</span>', 'PI@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:43:01'),
(222, 6, 109, '<span style=''color:magenta''>status_4_0</span>', 'I@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:43:01'),
(223, 3, 109, '<span style=''color:magenta''>status_4_0</span>', 'TFO@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:43:02'),
(224, 14, 109, '<span style=''color:magenta''>status_4_0</span>', 'all@test.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:43:02'),
(225, 27, 109, '<span style=''color:magenta''>status_4_0</span>', 'shiyanjigou', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:43:02'),
(226, 1, 109, '<span style=''color:magenta''>status_4_0</span>', 'enzo@mail.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>applicant_todo_4_0</span>', '2017-02-08 05:43:03'),
(227, 2, 109, '<span style=''color:magenta''>status_5_0</span>', 'PI@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:43:23'),
(228, 6, 109, '<span style=''color:magenta''>status_5_0</span>', 'I@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>I_todo_5_0</span>', '2017-02-08 05:43:23'),
(229, 3, 109, '<span style=''color:magenta''>status_5_0</span>', 'TFO@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:43:23'),
(230, 14, 109, '<span style=''color:magenta''>status_5_0</span>', 'all@test.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:43:24'),
(231, 27, 109, '<span style=''color:magenta''>status_5_0</span>', 'shiyanjigou', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:43:24'),
(232, 1, 109, '<span style=''color:magenta''>status_5_0</span>', 'enzo@mail.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:43:24'),
(233, 2, 109, '<span style=''color:magenta''>status_6_0</span>', 'PI@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:06'),
(234, 6, 109, '<span style=''color:magenta''>status_6_0</span>', 'I@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:07'),
(235, 3, 109, '<span style=''color:magenta''>status_6_0</span>', 'TFO@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>tfo_todo_6_0</span>', '2017-02-08 05:44:07'),
(236, 14, 109, '<span style=''color:magenta''>status_6_0</span>', 'all@test.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>tfo_todo_6_0</span>', '2017-02-08 05:44:07'),
(237, 27, 109, '<span style=''color:magenta''>status_6_0</span>', 'shiyanjigou', 'Test New Function', 'Enzo', '<span style=''color:magenta''>tfo_todo_6_0</span>', '2017-02-08 05:44:08'),
(238, 1, 109, '<span style=''color:magenta''>status_6_0</span>', 'enzo@mail.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:08'),
(239, 2, 109, '<span style=''color:magenta''>status_7_0</span>', 'PI@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:24'),
(240, 6, 109, '<span style=''color:magenta''>status_7_0</span>', 'I@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:24'),
(241, 3, 109, '<span style=''color:magenta''>status_7_0</span>', 'TFO@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:25'),
(242, 14, 109, '<span style=''color:magenta''>status_7_0</span>', 'all@test.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:25'),
(243, 27, 109, '<span style=''color:magenta''>status_7_0</span>', 'shiyanjigou', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:26'),
(244, 1, 109, '<span style=''color:magenta''>status_7_0</span>', 'enzo@mail.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:26'),
(245, 2, 109, '<span style=''color:magenta''>status_8_0</span>', 'PI@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:49'),
(246, 6, 109, '<span style=''color:magenta''>status_8_0</span>', 'I@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:50'),
(247, 3, 109, '<span style=''color:magenta''>status_8_0</span>', 'TFO@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:50'),
(248, 14, 109, '<span style=''color:magenta''>status_8_0</span>', 'all@test.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:50'),
(249, 27, 109, '<span style=''color:magenta''>status_8_0</span>', 'shiyanjigou', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:50'),
(250, 20, 109, '<span style=''color:magenta''>status_8_0</span>', 'ECS@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:51'),
(251, 44, 109, '<span style=''color:magenta''>status_8_0</span>', 'lunliweiyuanhuimishu', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:51'),
(252, 1, 109, '<span style=''color:magenta''>status_8_0</span>', 'enzo@mail.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:44:51'),
(253, 2, 109, '<span style=''color:magenta''>status_9_0</span>', 'PI@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:45:10'),
(254, 6, 109, '<span style=''color:magenta''>status_9_0</span>', 'I@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:45:11'),
(255, 3, 109, '<span style=''color:magenta''>status_9_0</span>', 'TFO@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:45:11'),
(256, 14, 109, '<span style=''color:magenta''>status_9_0</span>', 'all@test.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:45:11'),
(257, 27, 109, '<span style=''color:magenta''>status_9_0</span>', 'shiyanjigou', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:45:12'),
(258, 20, 109, '<span style=''color:magenta''>status_9_0</span>', 'ECS@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>ECS_todo_9_0</span>', '2017-02-08 05:45:12'),
(259, 44, 109, '<span style=''color:magenta''>status_9_0</span>', 'lunliweiyuanhuimishu', 'Test New Function', 'Enzo', '<span style=''color:magenta''>ECS_todo_9_0</span>', '2017-02-08 05:45:12'),
(260, 7, 109, '<span style=''color:magenta''>status_9_0</span>', 'ECM@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:45:12'),
(261, 36, 109, '<span style=''color:magenta''>status_9_0</span>', 'lunliweiyuanhuiweiyuan', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:45:13'),
(262, 1, 109, '<span style=''color:magenta''>status_9_0</span>', 'enzo@mail.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:45:13'),
(263, 2, 109, '<span style=''color:magenta''>status_10_0</span>', 'PI@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:12'),
(264, 6, 109, '<span style=''color:magenta''>status_10_0</span>', 'I@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>I_todo_10_0</span>', '2017-02-08 05:46:12'),
(265, 3, 109, '<span style=''color:magenta''>status_10_0</span>', 'TFO@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:13'),
(266, 14, 109, '<span style=''color:magenta''>status_10_0</span>', 'all@test.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:13'),
(267, 27, 109, '<span style=''color:magenta''>status_10_0</span>', 'shiyanjigou', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:13'),
(268, 20, 109, '<span style=''color:magenta''>status_10_0</span>', 'ECS@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:14'),
(269, 44, 109, '<span style=''color:magenta''>status_10_0</span>', 'lunliweiyuanhuimishu', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:14'),
(270, 7, 109, '<span style=''color:magenta''>status_10_0</span>', 'ECM@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:14'),
(271, 36, 109, '<span style=''color:magenta''>status_10_0</span>', 'lunliweiyuanhuiweiyuan', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:15'),
(272, 1, 109, '<span style=''color:magenta''>status_10_0</span>', 'enzo@mail.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:15'),
(273, 2, 109, '<span style=''color:magenta''>status_11_0</span>', 'PI@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:51'),
(274, 6, 109, '<span style=''color:magenta''>status_11_0</span>', 'I@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:51'),
(275, 3, 109, '<span style=''color:magenta''>status_11_0</span>', 'TFO@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:52'),
(276, 14, 109, '<span style=''color:magenta''>status_11_0</span>', 'all@test.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:52'),
(277, 27, 109, '<span style=''color:magenta''>status_11_0</span>', 'shiyanjigou', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:52'),
(278, 20, 109, '<span style=''color:magenta''>status_11_0</span>', 'ECS@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:53'),
(279, 44, 109, '<span style=''color:magenta''>status_11_0</span>', 'lunliweiyuanhuimishu', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:53'),
(280, 7, 109, '<span style=''color:magenta''>status_11_0</span>', 'ECM@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:54'),
(281, 36, 109, '<span style=''color:magenta''>status_11_0</span>', 'lunliweiyuanhuiweiyuan', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:54'),
(282, 1, 109, '<span style=''color:magenta''>status_11_0</span>', 'enzo@mail.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:46:54'),
(283, 2, 109, '<span style=''color:magenta''>status_12_0</span>', 'PI@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:13'),
(284, 6, 109, '<span style=''color:magenta''>status_12_0</span>', 'I@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:13'),
(285, 3, 109, '<span style=''color:magenta''>status_12_0</span>', 'TFO@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:14'),
(286, 14, 109, '<span style=''color:magenta''>status_12_0</span>', 'all@test.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:14'),
(287, 27, 109, '<span style=''color:magenta''>status_12_0</span>', 'shiyanjigou', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:14'),
(288, 20, 109, '<span style=''color:magenta''>status_12_0</span>', 'ECS@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>ECS_todo_12_0</span>', '2017-02-08 05:47:15'),
(289, 44, 109, '<span style=''color:magenta''>status_12_0</span>', 'lunliweiyuanhuimishu', 'Test New Function', 'Enzo', '<span style=''color:magenta''>ECS_todo_12_0</span>', '2017-02-08 05:47:15'),
(290, 7, 109, '<span style=''color:magenta''>status_12_0</span>', 'ECM@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:15'),
(291, 36, 109, '<span style=''color:magenta''>status_12_0</span>', 'lunliweiyuanhuiweiyuan', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:15'),
(292, 4, 109, '<span style=''color:magenta''>status_12_0</span>', 'ECC@admin.com', 'Test New Function', 'Enzo', 'N/A', '2017-02-08 05:47:16'),
(293, 18, 109, '<span style=''color:magenta''>status_12_0</span>', 'all@test.com', 'Test New Function', 'Enzo', 'N/A', '2017-02-08 05:47:16'),
(294, 37, 109, '<span style=''color:magenta''>status_12_0</span>', 'lunliweiyuanhuizhuren', 'Test New Function', 'Enzo', 'N/A', '2017-02-08 05:47:16'),
(295, 1, 109, '<span style=''color:magenta''>status_12_0</span>', 'enzo@mail.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:17'),
(296, 2, 109, '<span style=''color:magenta''>status_13_0</span>', 'PI@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:46'),
(297, 6, 109, '<span style=''color:magenta''>status_13_0</span>', 'I@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:46'),
(298, 3, 109, '<span style=''color:magenta''>status_13_0</span>', 'TFO@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:46'),
(299, 14, 109, '<span style=''color:magenta''>status_13_0</span>', 'all@test.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:47'),
(300, 27, 109, '<span style=''color:magenta''>status_13_0</span>', 'shiyanjigou', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:47'),
(301, 20, 109, '<span style=''color:magenta''>status_13_0</span>', 'ECS@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:48'),
(302, 44, 109, '<span style=''color:magenta''>status_13_0</span>', 'lunliweiyuanhuimishu', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:48'),
(303, 7, 109, '<span style=''color:magenta''>status_13_0</span>', 'ECM@admin.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:48'),
(304, 36, 109, '<span style=''color:magenta''>status_13_0</span>', 'lunliweiyuanhuiweiyuan', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:49'),
(305, 4, 109, '<span style=''color:magenta''>status_13_0</span>', 'ECC@admin.com', 'Test New Function', 'Enzo', 'N/A', '2017-02-08 05:47:49'),
(306, 18, 109, '<span style=''color:magenta''>status_13_0</span>', 'all@test.com', 'Test New Function', 'Enzo', 'N/A', '2017-02-08 05:47:49'),
(307, 37, 109, '<span style=''color:magenta''>status_13_0</span>', 'lunliweiyuanhuizhuren', 'Test New Function', 'Enzo', 'N/A', '2017-02-08 05:47:49'),
(308, 1, 109, '<span style=''color:magenta''>status_13_0</span>', 'enzo@mail.com', 'Test New Function', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-08 05:47:50'),
(309, 3, 111, '<span style=''color:magenta''>status_1_0</span>', 'TFO@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-09 02:01:14'),
(310, 14, 111, '<span style=''color:magenta''>status_1_0</span>', 'all@test.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-09 02:01:15'),
(311, 27, 111, '<span style=''color:magenta''>status_1_0</span>', 'shiyanjigou', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-09 02:01:15'),
(312, 1, 111, '<span style=''color:magenta''>status_1_0</span>', 'enzo@mail.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:01:15'),
(313, 2, 111, '<span style=''color:magenta''>status_2_0</span>', 'PI@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>PI_todo_2_0</span>', '2017-02-09 02:11:14'),
(314, 3, 111, '<span style=''color:magenta''>status_2_0</span>', 'TFO@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:11:14'),
(315, 14, 111, '<span style=''color:magenta''>status_2_0</span>', 'all@test.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:11:15'),
(316, 27, 111, '<span style=''color:magenta''>status_2_0</span>', 'shiyanjigou', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:11:15');
INSERT INTO `notifications` (`notification_id`, `role_id`, `project_id`, `title`, `email`, `project_name`, `project_owner`, `todo`, `created_at`) VALUES
(317, 1, 111, '<span style=''color:magenta''>status_2_0</span>', 'enzo@mail.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:11:15'),
(318, 2, 111, '<span style=''color:magenta''>status_3_0</span>', 'PI@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:11:36'),
(319, 5, 111, '<span style=''color:magenta''>status_3_0</span>', 'PRE@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>PRE_todo_3_0</span>', '2017-02-09 02:11:36'),
(320, 19, 111, '<span style=''color:magenta''>status_3_0</span>', 'all@test.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>PRE_todo_3_0</span>', '2017-02-09 02:11:37'),
(321, 47, 111, '<span style=''color:magenta''>status_3_0</span>', 'Refresher@Refresher.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>PRE_todo_3_0</span>', '2017-02-09 02:11:37'),
(322, 3, 111, '<span style=''color:magenta''>status_3_0</span>', 'TFO@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:11:37'),
(323, 14, 111, '<span style=''color:magenta''>status_3_0</span>', 'all@test.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:11:38'),
(324, 27, 111, '<span style=''color:magenta''>status_3_0</span>', 'shiyanjigou', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:11:38'),
(325, 1, 111, '<span style=''color:magenta''>status_3_0</span>', 'enzo@mail.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:11:38'),
(326, 2, 111, '<span style=''color:magenta''>status_4_0</span>', 'PI@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:13:43'),
(327, 6, 111, '<span style=''color:magenta''>status_4_0</span>', 'I@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:13:43'),
(328, 3, 111, '<span style=''color:magenta''>status_4_0</span>', 'TFO@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:13:43'),
(329, 14, 111, '<span style=''color:magenta''>status_4_0</span>', 'all@test.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:13:44'),
(330, 27, 111, '<span style=''color:magenta''>status_4_0</span>', 'shiyanjigou', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:13:44'),
(331, 1, 111, '<span style=''color:magenta''>status_4_0</span>', 'enzo@mail.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>applicant_todo_4_0</span>', '2017-02-09 02:13:44'),
(332, 2, 111, '<span style=''color:magenta''>status_5_0</span>', 'PI@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:14:58'),
(333, 6, 111, '<span style=''color:magenta''>status_5_0</span>', 'I@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>I_todo_5_0</span>', '2017-02-09 02:14:58'),
(334, 3, 111, '<span style=''color:magenta''>status_5_0</span>', 'TFO@admin.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:14:59'),
(335, 14, 111, '<span style=''color:magenta''>status_5_0</span>', 'all@test.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:14:59'),
(336, 27, 111, '<span style=''color:magenta''>status_5_0</span>', 'shiyanjigou', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:14:59'),
(337, 1, 111, '<span style=''color:magenta''>status_5_0</span>', 'enzo@mail.com', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:15:00'),
(338, 2, 111, '<span style=''color:magenta''>status_6_0</span>', 'PI@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:17:06'),
(339, 6, 111, '<span style=''color:magenta''>status_6_0</span>', 'I@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:17:07'),
(340, 3, 111, '<span style=''color:magenta''>status_6_0</span>', 'TFO@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>tfo_todo_6_0</span>', '2017-02-09 02:17:07'),
(341, 14, 111, '<span style=''color:magenta''>status_6_0</span>', 'all@test.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>tfo_todo_6_0</span>', '2017-02-09 02:17:08'),
(342, 27, 111, '<span style=''color:magenta''>status_6_0</span>', 'shiyanjigou', 'Test Databank', 'Enzo', '<span style=''color:magenta''>tfo_todo_6_0</span>', '2017-02-09 02:17:08'),
(343, 1, 111, '<span style=''color:magenta''>status_6_0</span>', 'enzo@mail.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:17:08'),
(344, 2, 111, '<span style=''color:magenta''>status_7_0</span>', 'PI@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:17:41'),
(345, 6, 111, '<span style=''color:magenta''>status_7_0</span>', 'I@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:17:41'),
(346, 3, 111, '<span style=''color:magenta''>status_7_0</span>', 'TFO@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:17:42'),
(347, 14, 111, '<span style=''color:magenta''>status_7_0</span>', 'all@test.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:17:42'),
(348, 27, 111, '<span style=''color:magenta''>status_7_0</span>', 'shiyanjigou', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:17:42'),
(349, 1, 111, '<span style=''color:magenta''>status_7_0</span>', 'enzo@mail.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:17:43'),
(350, 2, 111, '<span style=''color:magenta''>status_8_0</span>', 'PI@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:17:58'),
(351, 6, 111, '<span style=''color:magenta''>status_8_0</span>', 'I@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:17:58'),
(352, 3, 111, '<span style=''color:magenta''>status_8_0</span>', 'TFO@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:17:59'),
(353, 14, 111, '<span style=''color:magenta''>status_8_0</span>', 'all@test.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:17:59'),
(354, 27, 111, '<span style=''color:magenta''>status_8_0</span>', 'shiyanjigou', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:17:59'),
(355, 20, 111, '<span style=''color:magenta''>status_8_0</span>', 'ECS@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:17:59'),
(356, 44, 111, '<span style=''color:magenta''>status_8_0</span>', 'lunliweiyuanhuimishu', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:18:00'),
(357, 1, 111, '<span style=''color:magenta''>status_8_0</span>', 'enzo@mail.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:18:00'),
(358, 2, 111, '<span style=''color:magenta''>status_9_0</span>', 'PI@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:18:41'),
(359, 6, 111, '<span style=''color:magenta''>status_9_0</span>', 'I@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:18:41'),
(360, 3, 111, '<span style=''color:magenta''>status_9_0</span>', 'TFO@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:18:41'),
(361, 14, 111, '<span style=''color:magenta''>status_9_0</span>', 'all@test.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:18:42'),
(362, 27, 111, '<span style=''color:magenta''>status_9_0</span>', 'shiyanjigou', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:18:42'),
(363, 20, 111, '<span style=''color:magenta''>status_9_0</span>', 'ECS@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>ECS_todo_9_0</span>', '2017-02-09 02:18:42'),
(364, 44, 111, '<span style=''color:magenta''>status_9_0</span>', 'lunliweiyuanhuimishu', 'Test Databank', 'Enzo', '<span style=''color:magenta''>ECS_todo_9_0</span>', '2017-02-09 02:18:43'),
(365, 7, 111, '<span style=''color:magenta''>status_9_0</span>', 'ECM@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:18:43'),
(366, 36, 111, '<span style=''color:magenta''>status_9_0</span>', 'lunliweiyuanhuiweiyuan', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:18:43'),
(367, 1, 111, '<span style=''color:magenta''>status_9_0</span>', 'enzo@mail.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:18:44'),
(368, 2, 111, '<span style=''color:magenta''>status_10_0</span>', 'PI@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:19:25'),
(369, 6, 111, '<span style=''color:magenta''>status_10_0</span>', 'I@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>I_todo_10_0</span>', '2017-02-09 02:19:25'),
(370, 3, 111, '<span style=''color:magenta''>status_10_0</span>', 'TFO@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:19:25'),
(371, 14, 111, '<span style=''color:magenta''>status_10_0</span>', 'all@test.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:19:26'),
(372, 27, 111, '<span style=''color:magenta''>status_10_0</span>', 'shiyanjigou', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:19:26'),
(373, 20, 111, '<span style=''color:magenta''>status_10_0</span>', 'ECS@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:19:27'),
(374, 44, 111, '<span style=''color:magenta''>status_10_0</span>', 'lunliweiyuanhuimishu', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:19:27'),
(375, 7, 111, '<span style=''color:magenta''>status_10_0</span>', 'ECM@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:19:27'),
(376, 36, 111, '<span style=''color:magenta''>status_10_0</span>', 'lunliweiyuanhuiweiyuan', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:19:28'),
(377, 1, 111, '<span style=''color:magenta''>status_10_0</span>', 'enzo@mail.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:19:28'),
(378, 2, 111, '<span style=''color:magenta''>status_11_0</span>', 'PI@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:06'),
(379, 6, 111, '<span style=''color:magenta''>status_11_0</span>', 'I@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:06'),
(380, 27, 111, '<span style=''color:magenta''>status_11_0</span>', 'shiyanjigou', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:07'),
(381, 3, 111, '<span style=''color:magenta''>status_11_0</span>', 'TFO@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:07'),
(382, 14, 111, '<span style=''color:magenta''>status_11_0</span>', 'all@test.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:07'),
(383, 44, 111, '<span style=''color:magenta''>status_11_0</span>', 'lunliweiyuanhuimishu', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:08'),
(384, 20, 111, '<span style=''color:magenta''>status_11_0</span>', 'ECS@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:08'),
(385, 36, 111, '<span style=''color:magenta''>status_11_0</span>', 'lunliweiyuanhuiweiyuan', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:08'),
(386, 7, 111, '<span style=''color:magenta''>status_11_0</span>', 'ECM@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:09'),
(387, 1, 111, '<span style=''color:magenta''>status_11_0</span>', 'enzo@mail.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:09'),
(388, 2, 111, '<span style=''color:magenta''>status_12_0</span>', 'PI@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:25'),
(389, 6, 111, '<span style=''color:magenta''>status_12_0</span>', 'I@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:26'),
(390, 27, 111, '<span style=''color:magenta''>status_12_0</span>', 'shiyanjigou', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:26'),
(391, 3, 111, '<span style=''color:magenta''>status_12_0</span>', 'TFO@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:26'),
(392, 14, 111, '<span style=''color:magenta''>status_12_0</span>', 'all@test.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:27'),
(393, 44, 111, '<span style=''color:magenta''>status_12_0</span>', 'lunliweiyuanhuimishu', 'Test Databank', 'Enzo', '<span style=''color:magenta''>ECS_todo_12_0</span>', '2017-02-09 02:20:27'),
(394, 20, 111, '<span style=''color:magenta''>status_12_0</span>', 'ECS@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>ECS_todo_12_0</span>', '2017-02-09 02:20:27'),
(395, 36, 111, '<span style=''color:magenta''>status_12_0</span>', 'lunliweiyuanhuiweiyuan', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:28'),
(396, 7, 111, '<span style=''color:magenta''>status_12_0</span>', 'ECM@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:28'),
(397, 37, 111, '<span style=''color:magenta''>status_12_0</span>', 'lunliweiyuanhuizhuren', 'Test Databank', 'Enzo', 'N/A', '2017-02-09 02:20:28'),
(398, 4, 111, '<span style=''color:magenta''>status_12_0</span>', 'ECC@admin.com', 'Test Databank', 'Enzo', 'N/A', '2017-02-09 02:20:28'),
(399, 18, 111, '<span style=''color:magenta''>status_12_0</span>', 'all@test.com', 'Test Databank', 'Enzo', 'N/A', '2017-02-09 02:20:29'),
(400, 1, 111, '<span style=''color:magenta''>status_12_0</span>', 'enzo@mail.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:20:29'),
(401, 2, 111, '<span style=''color:magenta''>status_13_0</span>', 'PI@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:21:31'),
(402, 6, 111, '<span style=''color:magenta''>status_13_0</span>', 'I@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:21:31'),
(403, 27, 111, '<span style=''color:magenta''>status_13_0</span>', 'shiyanjigou', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:21:32'),
(404, 3, 111, '<span style=''color:magenta''>status_13_0</span>', 'TFO@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:21:32'),
(405, 14, 111, '<span style=''color:magenta''>status_13_0</span>', 'all@test.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:21:32'),
(406, 44, 111, '<span style=''color:magenta''>status_13_0</span>', 'lunliweiyuanhuimishu', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:21:33'),
(407, 20, 111, '<span style=''color:magenta''>status_13_0</span>', 'ECS@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:21:33'),
(408, 36, 111, '<span style=''color:magenta''>status_13_0</span>', 'lunliweiyuanhuiweiyuan', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:21:33'),
(409, 7, 111, '<span style=''color:magenta''>status_13_0</span>', 'ECM@admin.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:21:34'),
(410, 37, 111, '<span style=''color:magenta''>status_13_0</span>', 'lunliweiyuanhuizhuren', 'Test Databank', 'Enzo', 'N/A', '2017-02-09 02:21:34'),
(411, 4, 111, '<span style=''color:magenta''>status_13_0</span>', 'ECC@admin.com', 'Test Databank', 'Enzo', 'N/A', '2017-02-09 02:21:34'),
(412, 18, 111, '<span style=''color:magenta''>status_13_0</span>', 'all@test.com', 'Test Databank', 'Enzo', 'N/A', '2017-02-09 02:21:35'),
(413, 1, 111, '<span style=''color:magenta''>status_13_0</span>', 'enzo@mail.com', 'Test Databank', 'Enzo', '<span style=''color:magenta''>not_applicable</span>', '2017-02-09 02:21:35'),
(414, 3, 110, '<span style=''color:magenta''>status_1_0</span>', 'TFO@admin.com', 'Test me', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-11 05:37:29'),
(415, 14, 110, '<span style=''color:magenta''>status_1_0</span>', 'all@test.com', 'Test me', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-11 05:37:30'),
(416, 27, 110, '<span style=''color:magenta''>status_1_0</span>', 'shiyanjigou', 'Test me', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-11 05:37:31'),
(417, 3, 112, '<span style=''color:magenta''>status_1_0</span>', 'TFO@admin.com', '22222', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-11 05:38:43'),
(418, 14, 112, '<span style=''color:magenta''>status_1_0</span>', 'all@test.com', '22222', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-11 05:38:43'),
(419, 27, 112, '<span style=''color:magenta''>status_1_0</span>', 'shiyanjigou', '22222', 'Enzo', '<span style=''color:magenta''>tfo_todo_1_0</span>', '2017-02-11 05:38:43');

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE IF NOT EXISTS `positions` (
`position_id` int(5) NOT NULL,
  `description` varchar(100) NOT NULL,
  `is_professional_group` int(1) NOT NULL COMMENT '0 - not professional group, 1 - professional group role',
  `acronym` varchar(5) NOT NULL COMMENT 'short acronym for session conditions',
  `lang_phrase` varchar(250) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`position_id`, `description`, `is_professional_group`, `acronym`, `lang_phrase`) VALUES
(1, 'Applicant', 0, 'A', 'applicant'),
(2, 'Testing Facility OFfice', 0, 'TFO', 'tfo'),
(3, 'Principal Investigator', 1, 'PI', 'principal_investigator'),
(4, 'Investigator', 1, 'I', 'investigator'),
(5, 'Biological Sample Controller', 1, 'BGC', 'biological_sample_controller'),
(6, 'Medicine Controller', 1, 'MC', 'medicine_controller'),
(7, 'Clinician', 1, 'C', 'clinician'),
(8, 'Subjects Controller', 1, 'SC', 'subjects_controller'),
(9, 'Research Nurse', 1, 'RN', 'research_nurse'),
(10, 'Quality Controller', 1, 'QC', ''),
(11, 'Peer Review Expert', 0, 'PRE', 'pre'),
(12, 'Ethics Committee Chairman', 0, 'ECC', 'ecc'),
(13, 'Ethics Committee Member', 0, 'ECM', 'ecm'),
(14, 'Professional Group Admin', 1, 'PGA', ''),
(15, 'Ethics Committee Secretary', 0, 'ECS', 'ethics_committee_secretary'),
(16, 'Data Administrator', 1, 'DA', 'data_administrator');

-- --------------------------------------------------------

--
-- Table structure for table `professional_group`
--

CREATE TABLE IF NOT EXISTS `professional_group` (
`pgroup_id` int(11) NOT NULL,
  `pgroup_name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL,
  `pgroup_admin_id` tinyint(5) NOT NULL DEFAULT '0' COMMENT 'user_id of the one who administrates this pgroup'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `professional_group`
--

INSERT INTO `professional_group` (`pgroup_id`, `pgroup_name`, `date_created`, `date_modified`, `pgroup_admin_id`) VALUES
(1, 'Professional Group 1', '2016-11-04 23:47:57', '2016-11-05 00:18:29', 1),
(2, 'Professional Group 2', '2016-11-04 23:48:38', '2016-11-17 10:19:29', 12),
(3, 'Professional Group 3', '2016-11-04 23:48:38', '2016-11-10 01:43:33', 2),
(4, 'Professional Group 4', '2016-11-04 23:48:38', '2016-11-17 10:19:32', 14),
(5, 'Professional Group 5', '2016-11-04 23:48:38', '2016-11-17 10:19:33', 18),
(6, 'æŽä¸»ä»»é¡¹ç›®ç»„', '2017-01-12 02:08:38', '0000-00-00 00:00:00', 49);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
`project_id` int(11) NOT NULL,
  `status_id` int(11) DEFAULT NULL,
  `lot_num` varchar(255) DEFAULT NULL,
  `project_num` varchar(255) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT 'è¯·è¾“å…¥é¡¹ç›®åç§°',
  `trial_id` int(11) DEFAULT NULL,
  `indications` varchar(255) DEFAULT NULL,
  `risk_category` int(1) DEFAULT NULL COMMENT '0 - low, 1 - mid, 2 - high',
  `dept_id` int(11) DEFAULT NULL COMMENT 'deprecated, just left it here for backup purposes',
  `dept_name` varchar(255) NOT NULL,
  `pgroup_id` int(11) DEFAULT NULL,
  `pi_id` int(11) DEFAULT NULL COMMENT 'fk from users table',
  `pgroup_contact_id` int(5) NOT NULL DEFAULT '0' COMMENT 'Anyone from the pgroup',
  `user_id` int(11) NOT NULL COMMENT 'id of applicant from users table',
  `emergency_contact` varchar(255) DEFAULT NULL,
  `emergency_mobile` varchar(255) DEFAULT NULL,
  `emergency_email` varchar(255) DEFAULT NULL,
  `cro_name` varchar(255) DEFAULT NULL,
  `cro_contact` varchar(255) DEFAULT NULL,
  `cro_mobile` varchar(255) DEFAULT NULL,
  `cro_email` varchar(255) DEFAULT NULL,
  `research_objectives` mediumtext,
  `subsidize_type` int(1) DEFAULT NULL COMMENT '0 - no subsidize, 1 - partially funded, 2 - Fully funded',
  `remarks` mediumtext,
  `clinician_id` varchar(70) NOT NULL DEFAULT '0',
  `bio_controller_id` varchar(70) NOT NULL DEFAULT '0',
  `nurse_id` varchar(70) NOT NULL DEFAULT '0',
  `med_controller_id` varchar(70) NOT NULL DEFAULT '0',
  `subj_controller_id` varchar(70) NOT NULL DEFAULT '0',
  `project_status` int(11) NOT NULL DEFAULT '0' COMMENT '0 - draft, 1 - submitted/published, 2 - rejected/back to start',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=113 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `status_id`, `lot_num`, `project_num`, `project_name`, `trial_id`, `indications`, `risk_category`, `dept_id`, `dept_name`, `pgroup_id`, `pi_id`, `pgroup_contact_id`, `user_id`, `emergency_contact`, `emergency_mobile`, `emergency_email`, `cro_name`, `cro_contact`, `cro_mobile`, `cro_email`, `research_objectives`, `subsidize_type`, `remarks`, `clinician_id`, `bio_controller_id`, `nurse_id`, `med_controller_id`, `subj_controller_id`, `project_status`, `date_created`, `date_modified`) VALUES
(1, 14, '1', '23', 'æµ‹è¯•é¡¹ç›®', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', 'æŽæ˜Ž', '', '', '', '', 0, '', '8', '11', '9', '10', '0', 1, '2016-12-22 23:25:57', '0000-00-00 00:00:00'),
(2, 19, 'VAN', 'VAN', 'VAN', 2, '123', 1, NULL, '123123', 1, 48, 48, 2, 'asdasd', 'asdasd', 'asd', 'asd', 'asd', 'asd', 'asd', 'asd', 0, 'asd', '0', '0', '0', '0', '0', 1, '2016-12-23 17:14:46', '0000-00-00 00:00:00'),
(3, 68, '34', '12', 'æµ‹è¯•é¡¹ç›®2', 3, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', 'é˜¿é“å¤«', '', '', '', '', 0, '', '8', '11', '9', '10', '0', 1, '2016-12-23 22:38:06', '0000-00-00 00:00:00'),
(4, 35, '123', '123', 'VanProj', 1, '1', 0, NULL, '1', 1, 48, 48, 2, '1', '1', '1', '111', '1', '1', '1', '1', 0, '1', '0', '0', '0', '0', '0', 1, '2016-12-23 23:16:37', '0000-00-00 00:00:00'),
(5, 99, '', '12', 'æµ‹è¯•é¡¹ç›®3', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '111', '', '', '', '', 0, '', '8', '11', '9', '10', '0', 1, '2016-12-26 03:11:52', '0000-00-00 00:00:00'),
(6, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2016-12-28 15:26:54', '0000-00-00 00:00:00'),
(7, 66, '', '', 'Project Test', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '8', '11', '9', '10', '0', 1, '2016-12-28 19:15:23', '0000-00-00 00:00:00'),
(8, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2016-12-29 17:42:17', '0000-00-00 00:00:00'),
(9, 103, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°sssss', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2016-12-30 18:42:46', '0000-00-00 00:00:00'),
(10, 69, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2016-12-30 18:49:24', '0000-00-00 00:00:00'),
(11, 70, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2016-12-30 19:00:05', '0000-00-00 00:00:00'),
(12, 84, '', '', 'æµ‹è¯•é¡¹ç›®', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '8', '11', '9', '10', '0', 1, '2016-12-30 19:05:50', '0000-00-00 00:00:00'),
(13, 98, '123', '2', '22', 1, '33', 0, NULL, '3', 1, 48, 48, 2, '3', '3', '3', '5', '55', '5', '5', '5', 0, 'è‹¥5', '8', '11', '9', '10', '0', 1, '2016-12-30 21:10:14', '0000-00-00 00:00:00'),
(14, 102, '', '', 'æµ‹è¯•é¡¹ç›®1', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-07 17:13:17', '0000-00-00 00:00:00'),
(15, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-09 22:15:30', '0000-00-00 00:00:00'),
(16, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-09 22:16:41', '0000-00-00 00:00:00'),
(17, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-09 23:08:36', '0000-00-00 00:00:00'),
(18, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-09 23:09:43', '0000-00-00 00:00:00'),
(19, 109, '', '', 'æµ‹è¯•é¡¹ç›®2', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '2', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-10 15:32:15', '0000-00-00 00:00:00'),
(20, 117, '', '', 'æµ‹è¯•é¡¹ç›®3', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, 'é˜¿é“å¤«', '0', '0', '0', '0', '0', 1, '2017-01-10 15:48:33', '0000-00-00 00:00:00'),
(21, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-10 15:55:48', '0000-00-00 00:00:00'),
(22, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-10 15:57:20', '0000-00-00 00:00:00'),
(23, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-10 18:03:28', '0000-00-00 00:00:00'),
(24, 111, '1', '1', '1', 1, '1', 0, NULL, '11', 1, 48, 48, 2, '1', '1', '11', '1', '1', '1', '1', '1', 0, '1', '0', '0', '0', '0', '0', 1, '2017-01-10 18:11:30', '0000-00-00 00:00:00'),
(25, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-10 22:30:32', '0000-00-00 00:00:00'),
(26, 130, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-10 22:59:39', '0000-00-00 00:00:00'),
(27, 152, '2010L02531', '001', 'ç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡äººä½“ç”Ÿç‰©åˆ©ç”¨åº¦å’Œç”Ÿç‰©ç­‰æ•ˆæ€§ç ”ç©¶', 1, 'ç³–å°¿ç—…IIæœŸ', 0, NULL, 'å†…åˆ†æ³Œç§‘', 1, 48, 48, 2, 'é™ˆå« é»„æ™“ä¼Ÿ', '0755-25700034ï¼Œä¼ çœŸï¼š0755-25708020', 'abdcd-112@163.com', 'æ— ', 'æ— ', 'æ— ', 'æ— ', 'é€šè¿‡ç ”ç©¶å¥åº·ç”·æ€§å—è¯•è€…å•æ¬¡ç©ºè…¹å£æœæ·±åœ³å¸‚ä¸­è”åˆ¶è¯æœ‰é™å…¬å¸ç ”åˆ¶çš„ç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡çš„è¯ä»£åŠ¨åŠ›å­¦ç‰¹å¾ï¼Œç‰¹åˆ«æ˜¯å¯¹è¯ç‰©çš„å¸æ”¶è¿‡ç¨‹çš„å½±å“ï¼Œæ¯”è¾ƒè¿™ç§ç»™è¯æƒ…å†µä¸‹çš„ç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡ä¸Žå‚æ¯”åˆ¶å‰‚æ ¼åŽæ­¢ï¼ˆç›é…¸äºŒç”²åŒèƒç‰‡ï¼‰çš„äººä½“ç”Ÿç‰©åˆ©ç”¨åº¦å’Œç”Ÿç‰©ç­‰æ•ˆæ€§ï¼Œä¸ºå…¶ä¸´åºŠåˆç†ç”¨è¯æä¾›ä¾æ®ã€‚', 0, 'æ— ', '8', '11', '9', '10', '0', 1, '2017-01-10 23:03:31', '0000-00-00 00:00:00'),
(28, 149, '', '001', 'æµ‹è¯•é¡¹ç›®4', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '8', '11', '9', '10', '0', 1, '2017-01-11 21:29:26', '0000-00-00 00:00:00'),
(29, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-12 21:14:42', '0000-00-00 00:00:00'),
(30, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-14 22:55:56', '0000-00-00 00:00:00'),
(31, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-16 17:28:47', '0000-00-00 00:00:00'),
(32, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-18 21:32:25', '0000-00-00 00:00:00'),
(33, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-20 04:34:09', '0000-00-00 00:00:00'),
(34, 154, '2010L02531', '001', 'ç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡äººä½“ç”Ÿç‰©åˆ©ç”¨åº¦å’Œç”Ÿç‰©ç­‰æ•ˆæ€§ç ”ç©¶', 5, 'IIåž‹ç³–å°¿ç—…', 0, NULL, 'å†…åˆ†æ³Œç§‘', 6, 73, 73, 65, 'é™ˆå« é»„æ™“ä¼Ÿ', '0755-25700034ï¼Œä¼ çœŸï¼š0755-25708020', 'live99pei@163.com', 'æ— ', 'æ— ', 'æ— ', 'æ— ', '    é€šè¿‡ç ”ç©¶å¥åº·ç”·æ€§å—è¯•è€…å•æ¬¡ç©ºè…¹å£æœæ·±åœ³å¸‚ä¸­è”åˆ¶è¯æœ‰é™å…¬å¸ç ”åˆ¶çš„ç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡çš„è¯ä»£åŠ¨åŠ›å­¦ç‰¹å¾ï¼Œç‰¹åˆ«æ˜¯å¯¹è¯ç‰©çš„å¸æ”¶è¿‡ç¨‹çš„å½±å“ï¼Œæ¯”è¾ƒè¿™ç§ç»™è¯æƒ…å†µä¸‹çš„ç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡ä¸Žå‚æ¯”åˆ¶å‰‚æ ¼åŽæ­¢ï¼ˆç›é…¸äºŒç”²åŒèƒç‰‡ï¼‰çš„äººä½“ç”Ÿç‰©åˆ©ç”¨åº¦å’Œç”Ÿç‰©ç­‰æ•ˆæ€§ï¼Œä¸ºå…¶ä¸´åºŠåˆç†ç”¨è¯æä¾›ä¾æ®ã€‚', 0, 'æ— ', '0', '0', '0', '0', '0', 1, '2017-01-20 13:42:01', '0000-00-00 00:00:00'),
(35, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-20 17:06:29', '0000-00-00 00:00:00'),
(36, 165, '', '', 'æµ‹è¯•é¡¹ç›®', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 2, '2017-01-22 15:03:46', '0000-00-00 00:00:00'),
(37, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-22 15:32:40', '0000-00-00 00:00:00'),
(38, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-22 15:36:59', '0000-00-00 00:00:00'),
(39, 157, '', '', 'æµ‹è¯•', 1, '', 0, NULL, '', 1, 48, 48, 65, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-22 15:38:22', '0000-00-00 00:00:00'),
(40, 158, '', '', 'æµ‹è¯•1', 2, '', 0, NULL, '', 1, 48, 48, 65, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-22 15:38:50', '0000-00-00 00:00:00'),
(41, 159, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 3, '', 0, NULL, '', 1, 48, 48, 65, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-22 15:39:16', '0000-00-00 00:00:00'),
(42, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-22 15:39:33', '0000-00-00 00:00:00'),
(43, 160, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 4, '', 0, NULL, '', 1, 48, 48, 65, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-22 15:39:50', '0000-00-00 00:00:00'),
(44, 161, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 5, '', 0, NULL, '', 1, 48, 48, 65, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-22 15:40:11', '0000-00-00 00:00:00'),
(45, 162, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 6, '', 0, NULL, '', 1, 48, 48, 65, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-22 15:41:18', '0000-00-00 00:00:00'),
(46, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-22 15:41:18', '0000-00-00 00:00:00'),
(47, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-22 15:41:18', '0000-00-00 00:00:00'),
(48, 163, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 7, '', 0, NULL, '', 1, 48, 48, 65, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-22 15:42:07', '0000-00-00 00:00:00'),
(49, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-01-22 16:30:14', '0000-00-00 00:00:00'),
(50, 164, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-01-25 17:47:24', '0000-00-00 00:00:00'),
(51, NULL, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 0, '2017-01-25 17:52:55', '0000-00-00 00:00:00'),
(52, 166, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 08:28:07', '0000-00-00 00:00:00'),
(53, 167, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 08:41:46', '0000-00-00 00:00:00'),
(54, 168, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 08:43:42', '0000-00-00 00:00:00'),
(55, 169, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 09:43:14', '0000-00-00 00:00:00'),
(56, 170, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 09:44:37', '0000-00-00 00:00:00'),
(57, 171, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 09:45:47', '0000-00-00 00:00:00'),
(58, 172, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 09:46:20', '0000-00-00 00:00:00'),
(59, 173, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 09:54:03', '0000-00-00 00:00:00'),
(60, 174, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 11:16:40', '0000-00-00 00:00:00'),
(61, 175, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 11:22:55', '0000-00-00 00:00:00'),
(62, 176, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 11:32:31', '0000-00-00 00:00:00'),
(63, 177, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 11:36:26', '0000-00-00 00:00:00'),
(64, 178, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 11:37:05', '0000-00-00 00:00:00'),
(65, 179, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 11:37:24', '0000-00-00 00:00:00'),
(66, 180, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 11:50:52', '0000-00-00 00:00:00'),
(67, 181, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 12:00:01', '0000-00-00 00:00:00'),
(68, 182, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 12:01:37', '0000-00-00 00:00:00'),
(69, 183, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 12:04:58', '0000-00-00 00:00:00'),
(70, 184, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 13:26:02', '0000-00-00 00:00:00'),
(71, 185, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 13:26:23', '0000-00-00 00:00:00'),
(72, NULL, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 0, '2017-02-03 13:31:02', '0000-00-00 00:00:00'),
(73, NULL, '', '', 'zaaza', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 0, '2017-02-03 13:31:23', '0000-00-00 00:00:00'),
(74, 186, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 13:57:15', '0000-00-00 00:00:00'),
(75, 187, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 13:59:02', '0000-00-00 00:00:00'),
(76, 188, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 14:09:51', '0000-00-00 00:00:00'),
(77, 189, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 6, 73, 73, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 14:16:12', '0000-00-00 00:00:00'),
(78, 190, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 14:28:58', '0000-00-00 00:00:00'),
(79, 191, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 15:18:54', '0000-00-00 00:00:00'),
(80, 192, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 15:31:16', '0000-00-00 00:00:00'),
(81, 193, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 15:31:42', '0000-00-00 00:00:00'),
(82, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-02-03 15:35:24', '0000-00-00 00:00:00'),
(83, 194, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 15:35:30', '0000-00-00 00:00:00'),
(84, 195, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 15:36:45', '0000-00-00 00:00:00'),
(85, 196, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 15:37:45', '0000-00-00 00:00:00'),
(86, 197, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 15:38:28', '0000-00-00 00:00:00'),
(87, 198, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 15:39:51', '0000-00-00 00:00:00'),
(88, 199, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 15:52:43', '0000-00-00 00:00:00'),
(89, 200, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 15:55:22', '0000-00-00 00:00:00'),
(90, 201, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 15:58:19', '0000-00-00 00:00:00'),
(91, NULL, NULL, NULL, 'è¯·è¾“å…¥é¡¹ç›®åç§°', NULL, NULL, NULL, NULL, '', NULL, NULL, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '0', '0', '0', '0', 0, '2017-02-03 15:59:18', '0000-00-00 00:00:00'),
(92, 202, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-03 16:01:02', '0000-00-00 00:00:00'),
(93, 205, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-04 07:43:22', '0000-00-00 00:00:00'),
(94, 212, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-04 07:45:32', '0000-00-00 00:00:00'),
(95, 213, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-04 11:07:16', '0000-00-00 00:00:00'),
(96, 214, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-04 11:07:37', '0000-00-00 00:00:00'),
(97, 215, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-04 11:14:55', '0000-00-00 00:00:00'),
(98, 216, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-04 11:16:35', '0000-00-00 00:00:00'),
(99, 218, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-04 11:23:15', '0000-00-00 00:00:00'),
(100, 219, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-04 11:31:59', '0000-00-00 00:00:00'),
(101, 220, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-04 11:32:26', '0000-00-00 00:00:00'),
(102, 221, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-04 12:08:00', '0000-00-00 00:00:00'),
(103, 222, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-04 12:08:25', '0000-00-00 00:00:00'),
(104, 223, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-04 12:35:02', '0000-00-00 00:00:00'),
(105, 224, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-04 12:35:24', '0000-00-00 00:00:00'),
(106, 225, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-04 12:36:09', '0000-00-00 00:00:00'),
(107, 226, '', '', 'è¯·è¾“å…¥é¡¹ç›®åç§°', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-04 13:29:35', '0000-00-00 00:00:00'),
(108, 244, 'Refresher', 'Ref', 'Refresher', 2, 'Refresher', 1, NULL, 'Refresher', 1, 48, 48, 2, 'Refresher', 'Refresher', 'mail@refresher.com', 'CRO', 'CO', 'CRO', 'email@refresher.com', 'asd herllloooowwww ', 2, 'descc', '0', '0', '0', '*778*', '0', 1, '2017-02-07 11:10:13', '0000-00-00 00:00:00'),
(109, 259, 'Test New Function', 'Tes', 'Test New Function', 1, 'Test New Function', 0, NULL, 'Test New Function', 1, 48, 48, 2, 'Test New Function', 'Test New Function', 'Test New Function', 'Test New Function', 'Test New Function', 'Test New Function', 'Test New Function', 'Test New Function', 2, 'Test New Function', '0', '0', '0', '0', '0', 1, '2017-02-08 11:57:42', '0000-00-00 00:00:00'),
(110, 274, 'Test me', 'Tes', 'Test me', 1, 'Test me', 0, NULL, 'Test me', 2, 88, 49, 2, 'Test me', 'Test me', 'Test me', 'Test me', 'Test me', 'Test me', 'Test me', 'Test me', 0, 'Test me', '0', '0', '0', '0', '0', 1, '2017-02-09 09:18:39', '0000-00-00 00:00:00'),
(111, 273, '', '', 'Test Databank', 1, '', 0, NULL, '', 1, 48, 48, 2, '', '', '', '', '', '', '', '', 0, '', '0', '*11*', '0', '*79*', '*80*', 1, '2017-02-09 09:56:50', '0000-00-00 00:00:00'),
(112, 275, '22222', '222', '22222', 1, '', 0, NULL, '', 2, 88, 88, 2, '22222', '22222', '22222', '22222', '22222', '', '', '', 0, '', '0', '0', '0', '0', '0', 1, '2017-02-11 13:37:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Stand-in structure for view `projects x status`
--
CREATE TABLE IF NOT EXISTS `projects x status` (
`project_id` int(11)
,`project_name` varchar(255)
,`status_num` tinyint(3)
,`is_rejected` tinyint(1)
);
-- --------------------------------------------------------

--
-- Table structure for table `projects_pre`
--

CREATE TABLE IF NOT EXISTS `projects_pre` (
`pre_id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL COMMENT 'role id of PRE',
  `reviewer_id` int(5) NOT NULL COMMENT 'one who saved',
  `project_id` int(5) NOT NULL COMMENT 'project_id',
  `already_uploaded` int(1) NOT NULL DEFAULT '0' COMMENT 'if PRE uploaded'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `projects_pre`
--

INSERT INTO `projects_pre` (`pre_id`, `role_id`, `reviewer_id`, `project_id`, `already_uploaded`) VALUES
(1, 5, 2, 1, 0),
(2, 19, 2, 1, 0),
(3, 5, 2, 2, 0),
(4, 19, 2, 2, 0),
(5, 5, 2, 3, 0),
(6, 19, 2, 3, 0),
(7, 5, 2, 5, 0),
(8, 19, 2, 5, 0),
(9, 5, 2, 7, 0),
(10, 5, 2, 12, 0),
(11, 19, 2, 12, 0),
(12, 5, 2, 13, 0),
(13, 19, 2, 13, 0),
(14, 5, 2, 14, 0),
(15, 19, 2, 14, 0),
(16, 5, 2, 20, 0),
(17, 19, 2, 20, 0),
(18, 5, 2, 19, 0),
(19, 19, 2, 19, 0),
(20, 5, 2, 27, 0),
(21, 19, 2, 27, 0),
(22, 5, 2, 28, 0),
(23, 19, 2, 28, 0),
(24, 5, 2, 93, 0),
(25, 5, 2, 94, 0),
(26, 19, 2, 94, 0),
(27, 24, 2, 94, 0),
(28, 5, 2, 108, 0),
(29, 24, 2, 108, 0),
(30, 5, 2, 109, 0),
(31, 19, 2, 109, 0),
(32, 47, 2, 109, 0),
(33, 5, 2, 111, 0),
(34, 19, 2, 111, 0),
(35, 47, 2, 111, 0);

-- --------------------------------------------------------

--
-- Table structure for table `project_remarks`
--

CREATE TABLE IF NOT EXISTS `project_remarks` (
`remarks_id` int(5) NOT NULL,
  `remarks` varchar(300) NOT NULL,
  `status_id` int(5) NOT NULL COMMENT 'correlate with status table to track when and who inputted the comment',
  `project_id` int(5) NOT NULL COMMENT 'project_id from projects table',
  `remark_type` int(11) NOT NULL DEFAULT '0' COMMENT '0 - rejected remarks, 1 - add document remarks, 2 - ECM remarks'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `project_remarks`
--

INSERT INTO `project_remarks` (`remarks_id`, `remarks`, `status_id`, `project_id`, `remark_type`) VALUES
(1, 'hahaha ', 9, 1, 2),
(2, 'Hello', 16, 2, 1),
(3, 'hahaha', 44, 5, 2),
(4, 'tfo', 51, 7, 1),
(5, 'æ„è§', 79, 12, 2),
(6, 'åŒæ„ï¼Œã€‚ã€‚ã€‚ã€‚ã€‚ã€‚', 93, 13, 2),
(7, 'å˜»å˜»æŒºå¥½çš„', 117, 20, 2),
(8, '1ã€æ²¡æœ‰æŸå®³å—è¯•è€…çš„åˆ©ç›Š\r\n2ã€è¡¥å¿æ–¹æ¡ˆå®Œå–„\r\nåŒæ„å®žæ–½ã€‚', 128, 27, 2),
(9, 'sd', 129, 26, 0),
(10, 'hh', 144, 28, 2),
(11, 'Nope', 165, 36, 0),
(12, 'Try Reject Refresher', 229, 108, 0),
(13, 'Hellow from ECM', 238, 108, 2),
(14, 'Hey There', 253, 109, 2),
(15, 'Comment Here Hellow FROM ECM', 268, 111, 2);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
`role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'can duplicate',
  `position_id` int(11) NOT NULL,
  `pgroup_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=85 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `user_id`, `position_id`, `pgroup_id`) VALUES
(1, 2, 1, 0),
(2, 48, 3, 1),
(3, 49, 2, 0),
(4, 50, 12, 0),
(5, 51, 11, 0),
(6, 52, 4, 1),
(7, 55, 13, 0),
(8, 56, 7, 1),
(9, 57, 9, 1),
(11, 59, 5, 1),
(13, 61, 1, 0),
(14, 62, 2, 0),
(18, 62, 12, 0),
(19, 62, 11, 0),
(20, 63, 15, 0),
(21, 64, 16, 1),
(22, 65, 1, 0),
(23, 73, 3, 6),
(24, 73, 11, 0),
(25, 74, 11, 0),
(26, 75, 1, 0),
(27, 88, 2, 0),
(28, 76, 3, 6),
(29, 76, 5, 6),
(30, 79, 5, 6),
(31, 78, 4, 6),
(32, 80, 6, 6),
(33, 81, 7, 6),
(34, 82, 9, 6),
(35, 84, 11, 0),
(36, 87, 13, 0),
(37, 86, 12, 0),
(38, 89, 1, 0),
(39, 90, 1, 0),
(40, 91, 1, 0),
(41, 92, 1, 0),
(42, 93, 1, 0),
(43, 94, 1, 0),
(44, 85, 15, 0),
(45, 74, 7, 6),
(46, 74, 9, 6),
(47, 95, 11, 0),
(48, 99, 7, 1),
(49, 101, 6, 1),
(79, 58, 6, 1),
(80, 95, 8, 1),
(81, 62, 6, 1),
(82, 98, 5, 1),
(83, 49, 3, 2),
(84, 88, 3, 2);

-- --------------------------------------------------------

--
-- Stand-in structure for view `roles x users`
--
CREATE TABLE IF NOT EXISTS `roles x users` (
`user_id` int(5)
,`role_id` int(11)
,`fname` varchar(100)
);
-- --------------------------------------------------------

--
-- Table structure for table `side_effect`
--

CREATE TABLE IF NOT EXISTS `side_effect` (
`side_effect_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL COMMENT 'the id of the subject with side effects',
  `lot_num` varchar(50) NOT NULL,
  `num` varchar(50) NOT NULL,
  `report_type` varchar(50) NOT NULL COMMENT '0 - First Report, 1 - Follow-up Report, 2 - Sum Up Report',
  `report_time` datetime NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `hospital_mobile` varchar(50) NOT NULL,
  `dept` varchar(150) NOT NULL,
  `dept_mobile` varchar(150) NOT NULL,
  `drug_chinese_name` varchar(200) NOT NULL,
  `drug_english_name` varchar(200) NOT NULL,
  `drug_type` tinyint(1) NOT NULL COMMENT '0 -Traditional Chinese medicine, 1 - Chemistry Drug, 2 - Cure medicine, 3 - Prevent medicine, 4- Other ',
  `drug_form` varchar(200) NOT NULL,
  `drug_register` varchar(200) NOT NULL,
  `clinical_trial` tinyint(1) NOT NULL COMMENT '0 - I stage,  1 - II stage,  2 - III stage,  3 - IV stage,  4 - BE Trial,  5 - Clinical verifications',
  `clinical_indications` varchar(200) NOT NULL,
  `sub_initials` varchar(100) NOT NULL,
  `sub_gender` tinyint(1) NOT NULL COMMENT '0 - male, 1 - female',
  `sub_height` varchar(50) NOT NULL,
  `sub_weight` varchar(50) NOT NULL,
  `sub_birthdate` datetime NOT NULL,
  `sub_complication` varchar(150) NOT NULL COMMENT '0 - no, 1 - yes',
  `sae_diagnosis` varchar(150) NOT NULL,
  `sae_drug_measure` tinyint(1) NOT NULL COMMENT '0 - Continuantur remedia,  1 - Reduce the dosage,  2 - Stop then go',
  `sae_situation` tinyint(1) NOT NULL COMMENT '0 - Die,  1 - Be in hospital,  2 - Extend hospital,  3 - Disability,  4 - Dysfunction,  5 - Deformity,  6 - Life-threatening,  7 - Other,  8 - Severe',
  `relation_sae_drug` tinyint(1) NOT NULL COMMENT '0 - Certainty ,  1 - Maybe concern ,  2 - Be unconcerned ,  3 - Certainty uncincerned,  4 - Not Determinable',
  `sae_vest` tinyint(1) NOT NULL COMMENT '0 - Transference cure,  1 - Symptoms last',
  `sae_unbinding` tinyint(1) NOT NULL COMMENT '0 - No blinding,  1 - Did not unblinding,  2 - Unblinding',
  `sae_report_internal` tinyint(1) NOT NULL,
  `sae_report_external` tinyint(1) NOT NULL COMMENT '0 - Yes,  1 - No,  2 - No detail',
  `sae_handle_detail` varchar(250) NOT NULL,
  `report_unit` varchar(100) NOT NULL,
  `reporter_position` varchar(150) NOT NULL,
  `remark` varchar(250) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(5) NOT NULL,
  `date_updated` datetime NOT NULL,
  `updated_by` int(5) NOT NULL,
  `clinician_remarks` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=106 ;

--
-- Dumping data for table `side_effect`
--

INSERT INTO `side_effect` (`side_effect_id`, `project_id`, `subject_id`, `lot_num`, `num`, `report_type`, `report_time`, `hospital`, `hospital_mobile`, `dept`, `dept_mobile`, `drug_chinese_name`, `drug_english_name`, `drug_type`, `drug_form`, `drug_register`, `clinical_trial`, `clinical_indications`, `sub_initials`, `sub_gender`, `sub_height`, `sub_weight`, `sub_birthdate`, `sub_complication`, `sae_diagnosis`, `sae_drug_measure`, `sae_situation`, `relation_sae_drug`, `sae_vest`, `sae_unbinding`, `sae_report_internal`, `sae_report_external`, `sae_handle_detail`, `report_unit`, `reporter_position`, `remark`, `date_created`, `created_by`, `date_updated`, `updated_by`, `clinician_remarks`) VALUES
(1, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-22 09:57:37', 6, '0000-00-00 00:00:00', 0, ''),
(2, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-22 09:59:17', 6, '0000-00-00 00:00:00', 0, ''),
(3, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 07:24:12', 6, '0000-00-00 00:00:00', 0, ''),
(4, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 07:26:50', 6, '0000-00-00 00:00:00', 0, ''),
(5, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 07:27:19', 6, '0000-00-00 00:00:00', 0, ''),
(6, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 07:28:11', 6, '0000-00-00 00:00:00', 0, ''),
(7, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 07:31:43', 6, '0000-00-00 00:00:00', 0, ''),
(8, 3, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 09:16:21', 6, '0000-00-00 00:00:00', 0, ''),
(9, 3, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 09:16:40', 6, '0000-00-00 00:00:00', 0, ''),
(10, 3, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-23 09:53:18', 6, '0000-00-00 00:00:00', 0, ''),
(11, 3, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-25 13:43:52', 6, '0000-00-00 00:00:00', 0, ''),
(12, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-25 15:14:34', 6, '0000-00-00 00:00:00', 0, ''),
(13, 5, 0, '323', '2323', '0', '2016-12-25 00:00:00', '23234', '234', '2342', '34', '234', '234', 2, '234', '234', 0, '234', '434', 0, '123', '124', '2016-12-28 00:00:00', '0', '', 0, 0, 0, 0, 0, 0, 0, '', '2wdf', 'adf', 'adf', '2016-12-26 05:22:38', 6, '0000-00-00 00:00:00', 0, ''),
(14, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-25 15:24:03', 6, '0000-00-00 00:00:00', 0, ''),
(15, 5, 0, 'æ¸…é“å¤«', '12', '1', '2016-12-25 00:00:00', 'a''d''f', '', 'é˜¿é“å¤«', '', 'é˜¿é“å¤«', 'aæ°´ç”µè´¹', 1, 'é˜¿é“å¤«', 'å•Šæ°´ç”µè´¹', 0, 'é˜¿é“å¤«', 'é˜¿é“å¤«', 0, 'é˜¿é“å¤«1', '2', '2016-12-29 00:00:00', '0', '', 0, 0, 0, 0, 0, 0, 0, '', 'é˜¿é“å¤«', 'é˜¿é“å¤«a', 'aåœ°æ–¹', '2016-12-26 05:25:22', 6, '0000-00-00 00:00:00', 0, ''),
(16, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-25 15:29:31', 6, '0000-00-00 00:00:00', 0, ''),
(17, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-26 07:04:01', 6, '0000-00-00 00:00:00', 0, ''),
(18, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-26 08:24:57', 6, '0000-00-00 00:00:00', 0, ''),
(19, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-26 08:25:09', 6, '0000-00-00 00:00:00', 0, ''),
(20, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-26 09:15:30', 6, '0000-00-00 00:00:00', 0, ''),
(21, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-26 09:32:30', 6, '0000-00-00 00:00:00', 0, ''),
(22, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 02:33:56', 6, '0000-00-00 00:00:00', 0, ''),
(23, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 02:40:58', 6, '0000-00-00 00:00:00', 0, ''),
(24, 3, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:06:10', 6, '0000-00-00 00:00:00', 0, ''),
(25, 3, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:06:46', 6, '0000-00-00 00:00:00', 0, ''),
(26, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:07:11', 6, '0000-00-00 00:00:00', 0, ''),
(27, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:07:36', 6, '0000-00-00 00:00:00', 0, ''),
(28, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:13:21', 6, '0000-00-00 00:00:00', 0, ''),
(29, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:14:10', 6, '0000-00-00 00:00:00', 0, ''),
(30, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:15:27', 6, '0000-00-00 00:00:00', 0, ''),
(31, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:16:19', 6, '0000-00-00 00:00:00', 0, ''),
(32, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:17:01', 6, '0000-00-00 00:00:00', 0, ''),
(33, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:17:21', 6, '0000-00-00 00:00:00', 0, ''),
(34, 5, 4, 'abc', 'ab', '0', '2016-12-27 00:00:00', 'a', '1', 'a', '1', 'a', 'a', 0, 'a', 'a', 0, 'a', 'abc', 0, 'a', 'a', '2016-12-06 00:00:00', '0', 'a', 0, 0, 0, 0, 0, 0, 0, 'a', 'a', 'defg', 'defg', '2016-12-27 19:30:28', 9, '0000-00-00 00:00:00', 0, ''),
(35, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:21:41', 6, '0000-00-00 00:00:00', 0, ''),
(36, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:26:09', 6, '0000-00-00 00:00:00', 0, ''),
(37, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 03:28:31', 6, '0000-00-00 00:00:00', 0, ''),
(38, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 05:27:32', 6, '0000-00-00 00:00:00', 0, ''),
(39, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 05:28:35', 6, '0000-00-00 00:00:00', 0, ''),
(40, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 05:39:57', 9, '0000-00-00 00:00:00', 0, ''),
(41, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 05:48:40', 6, '0000-00-00 00:00:00', 0, ''),
(42, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 06:43:16', 6, '0000-00-00 00:00:00', 0, ''),
(43, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 06:44:11', 6, '0000-00-00 00:00:00', 0, ''),
(44, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 07:59:39', 6, '0000-00-00 00:00:00', 0, ''),
(45, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 09:18:17', 6, '0000-00-00 00:00:00', 0, ''),
(46, 1, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 09:18:28', 6, '0000-00-00 00:00:00', 0, ''),
(47, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-27 09:42:43', 6, '0000-00-00 00:00:00', 0, ''),
(48, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-28 01:45:02', 6, '0000-00-00 00:00:00', 0, ''),
(49, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-28 01:45:45', 6, '0000-00-00 00:00:00', 0, ''),
(50, 7, 9, 'c', 'c', '0', '2016-12-28 00:00:00', 'c', '3', 'c', '3', 'c', 'c', 0, 'c', 'c', 0, 'c', 'c', 0, 'c', 'c', '2016-12-28 00:00:00', '0', 'c', 0, 0, 0, 0, 0, 0, 0, 'c', 'c', 'c', 'c', '2016-12-28 22:35:26', 8, '0000-00-00 00:00:00', 0, ''),
(51, 7, 8, '1', '1', '', '2016-12-28 00:00:00', '1', '1', '1', '11', '1', '1', 0, '1', '1', 0, '1', '1', 0, 'a', 'a', '2016-12-28 00:00:00', '0', '', 0, 0, 0, 0, 0, 0, 0, '', '1', '1', '1', '2016-12-28 23:25:21', 6, '0000-00-00 00:00:00', 0, ''),
(52, 7, 0, '2', '2', '', '2016-12-28 00:00:00', '2', '2', '2', '2', '2', '2', 0, '2', '2', 0, '2', '2', 0, '2', '2', '2016-12-28 00:00:00', '1', '2', 0, 0, 0, 0, 0, 0, 0, '2', '2', '2', '2', '2016-12-28 23:28:04', 9, '0000-00-00 00:00:00', 0, ''),
(53, 7, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 03:27:03', 6, '0000-00-00 00:00:00', 0, ''),
(54, 7, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 03:27:37', 6, '0000-00-00 00:00:00', 0, ''),
(55, 7, 10, '1', '1', '0', '2016-12-01 00:00:00', '1', '1', '1', '1', '1', '1', 0, '1', '1', 0, '1', '1', 1, 'D', 'D', '2016-12-28 00:00:00', '0', '1', 0, 0, 0, 0, 0, 0, 0, '1', '1', '1', '1', '2016-12-29 17:29:18', 6, '0000-00-00 00:00:00', 0, ''),
(56, 7, 9, 'yayaya', 'yayaya', '1', '2016-12-01 00:00:00', 'yayaya', '1', '1', '1', '1yayaya', '1yayaya', 3, '1yayaya', '1yayaya', 0, '1yayaya', '1yayaya', 0, 'c', 'c', '2016-12-28 00:00:00', '0', '', 0, 0, 0, 0, 0, 0, 0, '', '1yayaya', '1yayaya', '1yayaya', '2016-12-29 21:38:26', 9, '0000-00-00 00:00:00', 0, ''),
(57, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 03:35:30', 2, '0000-00-00 00:00:00', 0, ''),
(58, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 06:57:27', 6, '0000-00-00 00:00:00', 0, ''),
(59, 5, 6, '1', '1', '0', '2016-12-01 00:00:00', '1', '11', '1', '1', '1', '1', 0, '1', '1', 0, '1', '11', 0, 'a', 'a', '2016-12-08 00:00:00', '0', '1', 0, 3, 0, 1, 1, 0, 1, '1', '1', '11', '1', '2016-12-29 20:59:12', 6, '0000-00-00 00:00:00', 0, ''),
(60, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 06:58:35', 6, '0000-00-00 00:00:00', 0, ''),
(61, 5, 0, '', '', '', '2016-12-29 14:27:41', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 07:02:55', 6, '0000-00-00 00:00:00', 0, ''),
(62, 5, 6, 'OH OH', 'OH OH', '', '2016-12-27 16:43:46', 'OH OH', '1', 'OH OH', '1', 'OH OH', 'OH OH', 0, 'OH OH', 'OH OH', 1, 'OH OH', 'OH OH', 0, '123', '123', '2016-12-30 00:00:00', '0', '', 0, 0, 0, 0, 0, 0, 0, '', 'OH OH', 'OH OH', 'OH OH', '2016-12-29 07:05:11', 6, '0000-00-00 00:00:00', 0, ''),
(63, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 07:21:49', 6, '0000-00-00 00:00:00', 0, ''),
(64, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 07:24:56', 6, '0000-00-00 00:00:00', 0, ''),
(65, 5, 6, '444', '444', '1', '2016-12-05 00:00:00', '4', '44', '44', '4', '4', '4', 1, '4', '4', 2, '4', '4', 0, 'a', 'a', '2016-12-01 00:00:00', '0', '4', 1, 4, 1, 0, 1, 1, 1, '4', '4', '4', '4', '2016-12-29 21:25:54', 6, '0000-00-00 00:00:00', 0, ''),
(66, 5, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-29 07:26:34', 6, '0000-00-00 00:00:00', 0, ''),
(67, 12, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-30 05:51:47', 6, '0000-00-00 00:00:00', 0, ''),
(68, 12, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-30 05:51:57', 6, '0000-00-00 00:00:00', 0, ''),
(69, 12, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-30 05:52:11', 6, '0000-00-00 00:00:00', 0, ''),
(70, 12, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-30 06:15:31', 6, '0000-00-00 00:00:00', 0, ''),
(71, 12, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2016-12-30 06:15:52', 6, '0000-00-00 00:00:00', 0, ''),
(72, 0, 12, '1', '11', '0', '2016-12-06 00:00:00', '1', '1', '1', '1', '1', '11', 0, '1', '1', 0, '1', '11', 0, '1', '1', '2016-12-30 00:00:00', '1', '1', 1, 1, 1, 0, 0, 0, 1, '1', '1', '1', '1', '2016-12-30 22:29:14', 8, '0000-00-00 00:00:00', 0, ''),
(73, 0, 12, '1', '1', '0', '2016-12-30 00:00:00', '1', '1', '1', '11', '1', '1', 0, '1', '11', 1, '1', '1', 0, '1', '1', '2016-12-30 00:00:00', '0', '1', 2, 0, 3, 0, 0, 0, 1, '1', '1', '1', '11', '2016-12-30 22:46:56', 8, '0000-00-00 00:00:00', 0, ''),
(74, 13, 12, '11', '1', '0', '2016-12-21 00:00:00', '1', '1', '1', '1', '1', '1', 1, '1', '11', 0, '1', '11', 1, '1', '1', '2016-12-30 00:00:00', '0', '', 0, 0, 0, 0, 0, 0, 0, '', '1', '1', '1', '2016-12-30 23:30:31', 6, '0000-00-00 00:00:00', 0, ''),
(75, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-07 08:27:27', 6, '0000-00-00 00:00:00', 0, ''),
(76, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-07 08:28:38', 6, '0000-00-00 00:00:00', 0, ''),
(77, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-07 08:31:40', 6, '0000-00-00 00:00:00', 0, ''),
(78, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-10 03:23:56', 6, '0000-00-00 00:00:00', 0, ''),
(79, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-10 03:24:29', 6, '0000-00-00 00:00:00', 0, ''),
(80, 27, 17, '2010L02531', '027', '0', '2017-01-10 00:00:00', 'å¹¿ä¸œçœäººæ°‘åŒ»é™¢å›½å®¶è¯ç‰©ä¸´åºŠè¯•éªŒæœºæž„', '0208352782', 'æ·±åœ³å¸‚ä¸­è”åˆ¶è¯æœ‰é™å…¬å¸', '0208263229', 'äºŒç”²åŒèƒ', 'Metformin hydrochloride', 1, 'ç‰‡å‰‚', 'ä»¿åˆ¶è¯', 4, 'ç³–å°¿ç—…IIåž‹', 'LXE', 1, '162', '53', '1967-04-12 00:00:00', '0', 'è‚¾åŠŸèƒ½ä¸¥é‡æŸä¼¤', 3, 1, 1, 1, 0, 2, 2, 'ç»è¿‡è¡€é€ï¼Œç—‡çŠ¶å¥½è½¬', 'å¹¿ä¸œçœäººæ°‘åŒ»é™¢', 'å‰¯ä¸»ä»»åŒ»å¸ˆ', 'æ— ', '2017-01-11 00:57:52', 8, '0000-00-00 00:00:00', 0, ''),
(81, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 01:57:41', 6, '0000-00-00 00:00:00', 0, ''),
(82, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 01:58:08', 6, '0000-00-00 00:00:00', 0, ''),
(83, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 01:59:38', 6, '0000-00-00 00:00:00', 0, ''),
(84, 13, 0, 'a', 'a', '', '2017-01-11 00:00:00', 'a', '1', 'a', '1', 'a', 'a', 0, 'a', 'a', 0, 'a', 'a', 0, 'a', 'a', '2017-01-11 00:00:00', '0', '', 0, 0, 0, 0, 0, 0, 0, '', 'a', 'a', '', '2017-01-11 16:03:52', 6, '0000-00-00 00:00:00', 0, ''),
(85, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 02:04:16', 6, '0000-00-00 00:00:00', 0, ''),
(86, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 02:07:34', 6, '0000-00-00 00:00:00', 0, ''),
(87, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 02:07:53', 6, '0000-00-00 00:00:00', 0, ''),
(88, 7, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 02:09:02', 6, '0000-00-00 00:00:00', 0, ''),
(89, 7, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 02:09:42', 6, '0000-00-00 00:00:00', 0, ''),
(90, 13, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 02:55:37', 6, '0000-00-00 00:00:00', 0, ''),
(91, 7, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 03:43:35', 6, '0000-00-00 00:00:00', 0, ''),
(92, 7, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-11 03:43:53', 6, '0000-00-00 00:00:00', 0, ''),
(93, 7, 0, '1', '1', '0', '2017-01-11 00:00:00', '1', '1', '1', '1', '1', '1', 4, '1', '1', 5, '1', '1', 1, '1', '1', '2017-01-11 00:00:00', '1', '', 0, 0, 0, 0, 0, 0, 0, '', '1', '1', '1', '2017-01-11 17:44:24', 6, '0000-00-00 00:00:00', 0, ''),
(94, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-17 10:52:47', 6, '0000-00-00 00:00:00', 0, ''),
(95, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-17 10:53:00', 6, '0000-00-00 00:00:00', 0, ''),
(96, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-18 06:56:05', 6, '0000-00-00 00:00:00', 0, ''),
(97, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-18 06:56:15', 6, '0000-00-00 00:00:00', 0, ''),
(98, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-18 09:10:39', 6, '0000-00-00 00:00:00', 0, ''),
(99, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-18 09:11:59', 6, '0000-00-00 00:00:00', 0, ''),
(100, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-20 08:12:09', 6, '0000-00-00 00:00:00', 0, ''),
(101, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-20 08:12:35', 6, '0000-00-00 00:00:00', 0, ''),
(102, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-22 01:59:21', 6, '0000-00-00 00:00:00', 0, ''),
(103, 27, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-01-22 01:59:59', 6, '0000-00-00 00:00:00', 0, ''),
(104, 111, 0, '', '', '', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', 0, '', '', 0, '', '', '0000-00-00 00:00:00', '', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', '', '2017-02-09 00:17:29', 6, '0000-00-00 00:00:00', 0, ''),
(105, 111, 0, 'Test Databank Side Effect Main', 'Test Databank Side Effect Main', '0,1', '2017-02-10 00:00:00', 'Test Databank Side Effect Main', '3', 'Test Databank Side Effect Main', '2', 'Test Databank Side Effect Main', 'Test Databank Side Effect Main', 1, 'Test Databank Side Effect Main', 'Test Databank Side Effect Main', 1, 'Test Databank Side Effect Main', 'Test Databank Side Effect Main', 0, '160', '50', '2017-02-24 00:00:00', '0', '', 0, 0, 0, 0, 0, 0, 0, '', 'Test Databank Side Effect Main', 'Test Databank Side Effect Main', 'Test Databank Side Effect Main', '2017-02-09 16:33:33', 6, '0000-00-00 00:00:00', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `side_effects_docs`
--

CREATE TABLE IF NOT EXISTS `side_effects_docs` (
`side_effect_doc_id` int(5) NOT NULL,
  `side_effect_id` int(5) NOT NULL,
  `name` varchar(150) NOT NULL,
  `version` int(5) NOT NULL,
  `version_date` datetime NOT NULL,
  `description` varchar(100) NOT NULL,
  `up_path` varchar(200) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `side_effects_docs`
--

INSERT INTO `side_effects_docs` (`side_effect_doc_id`, `side_effect_id`, `name`, `version`, `version_date`, `description`, `up_path`, `databank`) VALUES
(1, 27, 'a', 1, '0000-00-00 00:00:00', 'a', 'http://betaprojex.com/Clinical2/uploads/side_effect_documents/1482808370.txt', 0),
(2, 34, 'a', 9, '0000-00-00 00:00:00', 'a', 'http://betaprojex.com/Clinical2/uploads/side_effect_documents/1482808824.txt', 0),
(3, 74, '11', 1, '0000-00-00 00:00:00', '1', 'http://betaprojex.com/Clinical2/uploads/side_effect_documents/1483090228.docx', 0),
(4, 80, 'adf', 1, '0000-00-00 00:00:00', 'adf', 'http://betaprojex.com/Clinical2/uploads/side_effect_documents/1485050277.docx', 0),
(5, 80, 'adf', 1, '0000-00-00 00:00:00', 'adf', 'http://betaprojex.com/Clinical2/uploads/side_effect_documents/1485050283.docx', 0),
(6, 105, 'Test Databank Side Effects', 1, '0000-00-00 00:00:00', 'Test Databank Side Effects', 'http://localhost/clinical/uploads/side_effect_documents/1486600369.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `side_effect_receipts`
--

CREATE TABLE IF NOT EXISTS `side_effect_receipts` (
`receipt_id` int(11) NOT NULL,
  `side_effect_id` int(11) NOT NULL,
  `receipt_file` text NOT NULL,
  `receipt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stage_summary`
--

CREATE TABLE IF NOT EXISTS `stage_summary` (
`stage_summary_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `time_slot_from` date NOT NULL,
  `time_slot_to` date NOT NULL,
  `enroll_amount` decimal(11,2) NOT NULL,
  `quit_amount` decimal(11,2) NOT NULL,
  `finish_amount` decimal(11,2) NOT NULL,
  `light_effect` text NOT NULL,
  `light_amount` decimal(10,2) NOT NULL,
  `mid_effect` text NOT NULL,
  `mid_amount` decimal(10,2) NOT NULL,
  `hard_effect` text NOT NULL,
  `hard_amount` decimal(10,2) NOT NULL,
  `important_effect` text NOT NULL,
  `important_amount` decimal(10,2) NOT NULL,
  `sae` varchar(250) NOT NULL,
  `amount` decimal(11,2) NOT NULL,
  `degree` varchar(250) NOT NULL,
  `handling_info` text NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `stage_summary`
--

INSERT INTO `stage_summary` (`stage_summary_id`, `project_id`, `time_slot_from`, `time_slot_to`, `enroll_amount`, `quit_amount`, `finish_amount`, `light_effect`, `light_amount`, `mid_effect`, `mid_amount`, `hard_effect`, `hard_amount`, `important_effect`, `important_amount`, `sae`, `amount`, `degree`, `handling_info`, `added_by`, `date_added`, `updated_by`, `date_updated`) VALUES
(1, 111, '2017-02-09', '2017-02-28', '123123.00', '123123.00', '123123.00', 'stage-summary', '123123.00', 'stage-summary', '123123.00', 'stage-summary', '123123.00', 'stage-summary', '123123.00', '123123', '123.00', '123', '123', 6, '2017-02-14', 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
`status_id` int(5) NOT NULL,
  `status_num` tinyint(3) NOT NULL,
  `reviewer_id` int(5) NOT NULL COMMENT 'FK TABLE: roles role_id -- user who made the action and what position when he did it (i.e. if status = 2, approve by TFO with user_id = 4)',
  `is_rejected` tinyint(1) NOT NULL,
  `status_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `project_id` int(5) NOT NULL COMMENT 'FK in TABLE: projects'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=276 ;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`status_id`, `status_num`, `reviewer_id`, `is_rejected`, `status_date`, `project_id`) VALUES
(1, 1, 1, 0, '0000-00-00 00:00:00', 1),
(2, 2, 3, 0, '0000-00-00 00:00:00', 1),
(3, 3, 2, 0, '0000-00-00 00:00:00', 1),
(4, 4, 6, 0, '0000-00-00 00:00:00', 1),
(5, 5, 1, 0, '0000-00-00 00:00:00', 1),
(6, 6, 6, 0, '0000-00-00 00:00:00', 1),
(7, 7, 3, 0, '0000-00-00 00:00:00', 1),
(8, 8, 20, 0, '0000-00-00 00:00:00', 1),
(9, 9, 7, 0, '0000-00-00 00:00:00', 1),
(10, 10, 20, 0, '0000-00-00 00:00:00', 1),
(11, 11, 6, 0, '0000-00-00 00:00:00', 1),
(12, 12, 4, 0, '0000-00-00 00:00:00', 1),
(13, 13, 20, 0, '0000-00-00 00:00:00', 1),
(14, 14, 6, 0, '0000-00-00 00:00:00', 1),
(15, 1, 1, 0, '2016-12-23 03:17:29', 2),
(16, 2, 3, 0, '2016-12-23 03:19:34', 2),
(17, 3, 2, 0, '2016-12-23 03:22:32', 2),
(18, 4, 6, 0, '2016-12-23 03:39:19', 2),
(19, 5, 1, 0, '2016-12-23 04:10:20', 2),
(20, 1, 1, 0, '2016-12-23 08:38:46', 3),
(21, 2, 3, 0, '2016-12-23 08:39:25', 3),
(22, 3, 2, 0, '2016-12-23 08:39:53', 3),
(23, 4, 6, 0, '2016-12-23 08:41:11', 3),
(24, 5, 1, 0, '2016-12-23 08:41:46', 3),
(25, 6, 6, 0, '2016-12-23 08:42:32', 3),
(26, 7, 3, 0, '2016-12-23 08:42:57', 3),
(27, 8, 20, 0, '2016-12-23 08:43:24', 3),
(28, 9, 7, 0, '2016-12-23 08:46:41', 3),
(29, 10, 20, 0, '2016-12-23 08:47:15', 3),
(30, 11, 6, 0, '2016-12-23 08:48:03', 3),
(31, 12, 4, 0, '2016-12-23 08:53:28', 3),
(32, 13, 20, 0, '2016-12-23 08:54:02', 3),
(33, 14, 6, 0, '2016-12-23 08:54:54', 3),
(34, 15, 6, 0, '2016-12-23 09:02:39', 3),
(35, 1, 1, 0, '2016-12-23 23:17:00', 4),
(36, 1, 1, 0, '2016-12-26 03:12:37', 5),
(37, 2, 3, 0, '2016-12-26 03:52:40', 5),
(38, 3, 2, 0, '2016-12-26 03:53:10', 5),
(39, 4, 6, 0, '2016-12-26 03:59:52', 5),
(40, 5, 1, 0, '2016-12-26 04:00:25', 5),
(41, 6, 6, 0, '2016-12-26 04:01:34', 5),
(42, 7, 3, 0, '2016-12-26 04:02:01', 5),
(43, 8, 20, 0, '2016-12-26 04:02:33', 5),
(44, 9, 7, 0, '2016-12-26 04:04:29', 5),
(45, 10, 20, 0, '2016-12-26 04:08:12', 5),
(46, 11, 6, 0, '2016-12-26 04:10:02', 5),
(47, 12, 4, 0, '2016-12-26 04:11:22', 5),
(48, 13, 20, 0, '2016-12-26 04:12:13', 5),
(49, 14, 6, 0, '2016-12-26 04:12:50', 5),
(50, 1, 1, 0, '2016-12-28 19:50:05', 7),
(51, 2, 3, 0, '2016-12-28 19:51:32', 7),
(52, 3, 2, 0, '2016-12-28 19:53:17', 7),
(53, 4, 6, 0, '2016-12-28 19:54:48', 7),
(54, 5, 1, 0, '2016-12-28 19:55:57', 7),
(55, 6, 6, 0, '2016-12-28 20:07:09', 7),
(56, 7, 3, 0, '2016-12-28 20:31:24', 7),
(57, 8, 20, 0, '2016-12-28 20:46:44', 7),
(58, 9, 7, 0, '2016-12-28 20:49:23', 7),
(59, 10, 20, 0, '2016-12-28 20:50:09', 7),
(60, 11, 6, 0, '2016-12-28 20:50:45', 7),
(61, 12, 4, 0, '2016-12-28 20:51:06', 7),
(62, 13, 20, 0, '2016-12-28 20:51:41', 7),
(63, 14, 6, 0, '2016-12-28 21:13:29', 7),
(64, 15, 6, 0, '2016-12-28 23:45:58', 7),
(65, 16, 6, 0, '2016-12-29 19:54:17', 7),
(66, 17, 1, 0, '2016-12-29 19:55:27', 7),
(67, 16, 6, 0, '2016-12-29 20:01:56', 3),
(68, 17, 1, 0, '2016-12-29 20:03:57', 3),
(69, 1, 1, 0, '2016-12-30 18:59:24', 10),
(70, 1, 1, 0, '2016-12-30 19:01:57', 11),
(71, 1, 1, 0, '2016-12-30 19:06:32', 12),
(72, 2, 3, 0, '2016-12-30 19:07:28', 12),
(73, 3, 2, 0, '2016-12-30 19:09:04', 12),
(74, 4, 6, 0, '2016-12-30 19:16:45', 12),
(75, 5, 1, 0, '2016-12-30 19:17:57', 12),
(76, 6, 6, 0, '2016-12-30 19:21:17', 12),
(77, 7, 3, 0, '2016-12-30 19:21:41', 12),
(78, 8, 20, 0, '2016-12-30 19:25:16', 12),
(79, 9, 7, 0, '2016-12-30 19:33:08', 12),
(80, 10, 20, 0, '2016-12-30 19:34:05', 12),
(81, 11, 6, 0, '2016-12-30 19:34:53', 12),
(82, 12, 4, 0, '2016-12-30 19:38:11', 12),
(83, 13, 20, 0, '2016-12-30 19:41:46', 12),
(84, 14, 6, 0, '2016-12-30 19:43:34', 12),
(85, 1, 1, 0, '2016-12-30 21:13:38', 13),
(86, 2, 3, 0, '2016-12-30 21:15:44', 13),
(87, 3, 2, 0, '2016-12-30 21:16:49', 13),
(88, 4, 6, 0, '2016-12-30 21:36:00', 13),
(89, 5, 1, 0, '2016-12-30 21:36:30', 13),
(90, 6, 6, 0, '2016-12-30 21:37:44', 13),
(91, 7, 3, 0, '2016-12-30 21:39:29', 13),
(92, 8, 20, 0, '2016-12-30 21:40:23', 13),
(93, 9, 7, 0, '2016-12-30 21:41:48', 13),
(94, 10, 20, 0, '2016-12-30 21:57:50', 13),
(95, 11, 6, 0, '2016-12-30 22:02:19', 13),
(96, 12, 4, 0, '2016-12-30 22:03:02', 13),
(97, 13, 20, 0, '2016-12-30 22:12:03', 13),
(98, 14, 6, 0, '2016-12-30 22:13:13', 13),
(99, -1, 1, 0, '2017-01-05 07:54:18', 5),
(100, 1, 1, 0, '2017-01-07 17:13:36', 14),
(101, 2, 3, 0, '2017-01-07 17:35:19', 14),
(102, 3, 2, 0, '2017-01-07 19:51:58', 14),
(103, 1, 1, 0, '2017-01-09 20:51:26', 9),
(104, 1, 1, 0, '2017-01-10 15:32:32', 19),
(105, 2, 3, 0, '2017-01-10 15:34:02', 19),
(106, 1, 1, 0, '2017-01-10 15:55:01', 20),
(107, 2, 3, 0, '2017-01-10 16:07:42', 20),
(108, 3, 2, 0, '2017-01-10 16:09:11', 20),
(109, 3, 2, 0, '2017-01-10 16:11:35', 19),
(110, 1, 1, 0, '2017-01-10 18:11:42', 24),
(111, 2, 3, 0, '2017-01-10 18:14:27', 24),
(112, 4, 6, 0, '2017-01-10 21:23:54', 20),
(113, 5, 1, 0, '2017-01-10 21:24:31', 20),
(114, 6, 6, 0, '2017-01-10 22:06:42', 20),
(115, 7, 3, 0, '2017-01-10 22:07:10', 20),
(116, 8, 20, 0, '2017-01-10 22:07:39', 20),
(117, 9, 7, 0, '2017-01-10 22:08:13', 20),
(118, 1, 1, 0, '2017-01-10 23:23:49', 27),
(119, 2, 3, 0, '2017-01-10 23:25:10', 27),
(120, 3, 2, 0, '2017-01-10 23:26:03', 27),
(121, 4, 6, 0, '2017-01-10 23:32:02', 27),
(122, 5, 1, 0, '2017-01-10 23:32:37', 27),
(123, 6, 6, 0, '2017-01-10 23:37:34', 27),
(124, 1, 1, 0, '2017-01-10 23:39:53', 26),
(125, 7, 3, 0, '2017-01-10 23:46:01', 27),
(126, 8, 20, 0, '2017-01-10 23:46:56', 27),
(127, 2, 3, 0, '2017-01-10 23:48:18', 26),
(128, 9, 7, 0, '2017-01-10 23:48:30', 27),
(129, 0, 2, 1, '2017-01-10 23:49:21', 26),
(130, 1, 1, 0, '2017-01-10 23:49:54', 26),
(131, 10, 20, 0, '2017-01-10 23:52:52', 27),
(132, 11, 6, 0, '2017-01-11 00:04:13', 27),
(133, 12, 4, 0, '2017-01-11 00:09:56', 27),
(134, 13, 20, 0, '2017-01-11 00:11:04', 27),
(135, 14, 6, 0, '2017-01-11 00:12:19', 27),
(136, 1, 1, 0, '2017-01-11 21:31:07', 28),
(137, 2, 3, 0, '2017-01-11 21:36:35', 28),
(138, 3, 2, 0, '2017-01-11 21:37:14', 28),
(139, 4, 6, 0, '2017-01-11 21:42:38', 28),
(140, 5, 1, 0, '2017-01-11 21:43:48', 28),
(141, 6, 6, 0, '2017-01-11 21:44:31', 28),
(142, 7, 3, 0, '2017-01-11 21:45:07', 28),
(143, 8, 20, 0, '2017-01-11 21:46:27', 28),
(144, 9, 7, 0, '2017-01-11 21:50:17', 28),
(145, 10, 20, 0, '2017-01-11 21:54:24', 28),
(146, 11, 6, 0, '2017-01-11 21:55:34', 28),
(147, 12, 4, 0, '2017-01-11 21:56:02', 28),
(148, 13, 20, 0, '2017-01-11 21:57:23', 28),
(149, 14, 6, 0, '2017-01-11 21:58:03', 28),
(150, 15, 6, 0, '2017-01-12 15:59:13', 27),
(151, 16, 6, 0, '2017-01-12 16:26:27', 27),
(152, 17, 1, 0, '2017-01-13 17:42:18', 27),
(153, 1, 22, 0, '2017-01-20 13:52:55', 34),
(154, 2, 27, 0, '2017-01-20 13:55:31', 34),
(155, 1, 1, 0, '2017-01-22 15:04:54', 36),
(156, 2, 3, 0, '2017-01-22 15:05:55', 36),
(157, 1, 22, 0, '2017-01-22 15:38:38', 39),
(158, 1, 22, 0, '2017-01-22 15:39:09', 40),
(159, 1, 22, 0, '2017-01-22 15:39:32', 41),
(160, 1, 22, 0, '2017-01-22 15:39:57', 43),
(161, 1, 22, 0, '2017-01-22 15:40:22', 44),
(162, 1, 22, 0, '2017-01-22 15:41:26', 45),
(163, 1, 22, 0, '2017-01-22 15:42:15', 48),
(164, 1, 1, 0, '2017-01-25 17:52:48', 50),
(165, 0, 2, 1, '2017-01-25 19:54:35', 36),
(166, 1, 1, 0, '2017-02-03 08:38:13', 52),
(167, 1, 1, 0, '2017-02-03 08:41:54', 53),
(168, 1, 1, 0, '2017-02-03 08:43:45', 54),
(169, 1, 1, 0, '2017-02-03 09:43:55', 55),
(170, 1, 1, 0, '2017-02-03 09:44:40', 56),
(171, 1, 1, 0, '2017-02-03 09:45:53', 57),
(172, 1, 1, 0, '2017-02-03 09:46:22', 58),
(173, 1, 1, 0, '2017-02-03 09:54:14', 59),
(174, 1, 1, 0, '2017-02-03 11:16:44', 60),
(175, 1, 1, 0, '2017-02-03 11:22:59', 61),
(176, 1, 1, 0, '2017-02-03 11:32:34', 62),
(177, 1, 1, 0, '2017-02-03 11:36:29', 63),
(178, 1, 1, 0, '2017-02-03 11:37:08', 64),
(179, 1, 1, 0, '2017-02-03 11:37:27', 65),
(180, 1, 1, 0, '2017-02-03 11:50:54', 66),
(181, 1, 1, 0, '2017-02-03 12:00:05', 67),
(182, 1, 1, 0, '2017-02-03 12:01:40', 68),
(183, 1, 1, 0, '2017-02-03 12:06:48', 69),
(184, 1, 1, 0, '2017-02-03 13:26:05', 70),
(185, 1, 1, 0, '2017-02-03 13:28:30', 71),
(186, 1, 1, 0, '2017-02-03 13:58:29', 74),
(187, 1, 1, 0, '2017-02-03 13:59:04', 75),
(188, 1, 1, 0, '2017-02-03 14:09:54', 76),
(189, 1, 1, 0, '2017-02-03 14:16:40', 77),
(190, 1, 1, 0, '2017-02-03 14:29:01', 78),
(191, 1, 1, 0, '2017-02-03 15:18:57', 79),
(192, 1, 1, 0, '2017-02-03 15:31:18', 80),
(193, 1, 1, 0, '2017-02-03 15:31:45', 81),
(194, 1, 1, 0, '2017-02-03 15:35:33', 83),
(195, 1, 1, 0, '2017-02-03 15:36:48', 84),
(196, 1, 1, 0, '2017-02-03 15:37:51', 85),
(197, 1, 1, 0, '2017-02-03 15:38:31', 86),
(198, 1, 1, 0, '2017-02-03 15:39:55', 87),
(199, 1, 1, 0, '2017-02-03 15:52:48', 88),
(200, 1, 1, 0, '2017-02-03 15:55:25', 89),
(201, 1, 1, 0, '2017-02-03 15:58:23', 90),
(202, 1, 1, 0, '2017-02-03 16:01:04', 92),
(203, 1, 1, 0, '2017-02-04 07:43:26', 93),
(204, 2, 3, 0, '2017-02-04 07:44:03', 93),
(205, 3, 2, 0, '2017-02-04 07:44:40', 93),
(206, 1, 1, 0, '2017-02-04 07:45:34', 94),
(207, 2, 3, 0, '2017-02-04 07:45:54', 94),
(208, 3, 2, 0, '2017-02-04 07:46:11', 94),
(209, 4, 6, 0, '2017-02-04 07:50:04', 94),
(210, 5, 1, 0, '2017-02-04 08:33:18', 94),
(211, 6, 6, 0, '2017-02-04 08:33:44', 94),
(212, 7, 3, 0, '2017-02-04 08:34:04', 94),
(213, 1, 1, 0, '2017-02-04 11:07:20', 95),
(214, 1, 1, 0, '2017-02-04 11:07:39', 96),
(215, 1, 1, 0, '2017-02-04 11:14:58', 97),
(216, 1, 1, 0, '2017-02-04 11:16:38', 98),
(217, 1, 1, 0, '2017-02-04 11:23:18', 99),
(218, 2, 3, 0, '2017-02-04 11:23:53', 99),
(219, 1, 1, 0, '2017-02-04 11:32:02', 100),
(220, 1, 1, 0, '2017-02-04 11:32:28', 101),
(221, 1, 1, 0, '2017-02-04 12:08:04', 102),
(222, 1, 1, 0, '2017-02-04 12:08:28', 103),
(223, 1, 1, 0, '2017-02-04 12:35:05', 104),
(224, 1, 1, 0, '2017-02-04 12:35:27', 105),
(225, 1, 1, 0, '2017-02-04 12:36:56', 106),
(226, 1, 1, 0, '2017-02-04 13:29:38', 107),
(227, 1, 1, 0, '2017-02-07 11:13:16', 108),
(228, 2, 3, 0, '2017-02-07 11:15:34', 108),
(229, 0, 2, 1, '2017-02-07 11:26:56', 108),
(230, 1, 1, 0, '2017-02-07 11:28:14', 108),
(231, 2, 3, 0, '2017-02-07 11:30:20', 108),
(232, 3, 2, 0, '2017-02-07 11:30:52', 108),
(233, 4, 6, 0, '2017-02-07 12:04:33', 108),
(234, 5, 1, 0, '2017-02-07 12:06:19', 108),
(235, 6, 6, 0, '2017-02-07 13:21:10', 108),
(236, 7, 3, 0, '2017-02-07 13:22:14', 108),
(237, 8, 20, 0, '2017-02-07 13:22:48', 108),
(238, 9, 7, 0, '2017-02-07 13:23:27', 108),
(239, 10, 20, 0, '2017-02-07 13:26:02', 108),
(240, 11, 6, 0, '2017-02-07 13:27:43', 108),
(241, 12, 4, 0, '2017-02-07 13:33:18', 108),
(242, 13, 20, 0, '2017-02-07 13:33:56', 108),
(243, 14, 6, 0, '2017-02-07 13:50:56', 108),
(244, 15, 6, 0, '2017-02-07 13:52:26', 108),
(245, 1, 1, 0, '2017-02-08 11:58:17', 109),
(246, 2, 3, 0, '2017-02-08 11:58:56', 109),
(247, 3, 2, 0, '2017-02-08 12:07:40', 109),
(248, 4, 6, 0, '2017-02-08 13:43:00', 109),
(249, 5, 1, 0, '2017-02-08 13:43:22', 109),
(250, 6, 6, 0, '2017-02-08 13:44:06', 109),
(251, 7, 3, 0, '2017-02-08 13:44:24', 109),
(252, 8, 20, 0, '2017-02-08 13:44:49', 109),
(253, 9, 7, 0, '2017-02-08 13:45:10', 109),
(254, 10, 20, 0, '2017-02-08 13:46:11', 109),
(255, 11, 6, 0, '2017-02-08 13:46:50', 109),
(256, 12, 4, 0, '2017-02-08 13:47:13', 109),
(257, 13, 20, 0, '2017-02-08 13:47:45', 109),
(259, 14, 2, 0, '2017-02-07 22:59:48', 109),
(260, 1, 1, 0, '2017-02-09 10:01:14', 111),
(261, 2, 3, 0, '2017-02-09 10:11:13', 111),
(262, 3, 2, 0, '2017-02-09 10:11:36', 111),
(263, 4, 6, 0, '2017-02-09 10:13:42', 111),
(264, 5, 1, 0, '2017-02-09 10:14:58', 111),
(265, 6, 6, 0, '2017-02-09 10:17:06', 111),
(266, 7, 3, 0, '2017-02-09 10:17:40', 111),
(267, 8, 20, 0, '2017-02-09 10:17:57', 111),
(268, 9, 7, 0, '2017-02-09 10:18:40', 111),
(269, 10, 20, 0, '2017-02-09 10:19:24', 111),
(270, 11, 6, 0, '2017-02-09 10:20:05', 111),
(271, 12, 4, 0, '2017-02-09 10:20:25', 111),
(272, 13, 20, 0, '2017-02-09 10:21:30', 111),
(273, 14, 2, 0, '2017-02-08 23:08:19', 111),
(274, 1, 1, 0, '2017-02-11 13:37:28', 110),
(275, 1, 1, 0, '2017-02-11 13:38:42', 112);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
`subject_id` int(11) NOT NULL,
  `volunteer_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `case_num` varchar(50) NOT NULL,
  `subjects_num` varchar(50) NOT NULL,
  `abbrv` varchar(50) NOT NULL,
  `country` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `pi` varchar(50) NOT NULL,
  `is_informed_consent` tinyint(4) NOT NULL COMMENT '0 - no, 1 - yes',
  `status_id` int(11) NOT NULL COMMENT 'fk from subject_status',
  `sign_date` datetime NOT NULL,
  `random_num` int(11) NOT NULL,
  `random_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`subject_id`, `volunteer_id`, `project_id`, `case_num`, `subjects_num`, `abbrv`, `country`, `address`, `pi`, `is_informed_consent`, `status_id`, `sign_date`, `random_num`, `random_date`) VALUES
(1, 1, 1, '', '', '', '', '', '', 0, 4, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(2, 2, 1, '', '', '', '', '', '', 0, 64, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(3, 3, 1, '', '', '', '', '', '', 0, 10, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(4, 4, 5, '', '', '', '', '', '', 0, 14, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(5, 5, 5, '', '', '', '', '', '', 0, 85, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(6, 6, 5, '', '', '', '', '', '', 0, 19, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(7, 8, 7, '', '', '', '', '', '', 0, 75, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(8, 7, 7, '', '', '', '', '', '', 0, 35, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(9, 9, 7, '', '', '', '', '', '', 0, 43, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(10, 10, 7, '', '', '', '', '', '', 0, 76, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(11, 11, 12, '', '', '', '', '', '', 0, 84, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(12, 12, 13, '', '', '', '', '', '', 0, 49, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(13, 13, 13, '', '', '', '', '', '', 0, 55, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(14, 14, 13, '', '', '', '', '', '', 0, 61, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(15, 15, 13, '', '', '', '', '', '', 0, 69, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(16, 17, 27, '', '', '', '', 'fg', '', 0, 82, '0000-00-00 00:00:00', 2334, '0000-00-00 00:00:00'),
(17, 16, 27, '1223', '123', 'ZHG', 'meiguo', 'adf', '', 0, 78, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(18, 15, 28, '', '', '', '', '', '', 0, 89, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(19, 18, 28, '', '', '', '', '', '', 0, 86, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `subject_status`
--

CREATE TABLE IF NOT EXISTS `subject_status` (
`status_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `status` varchar(3) NOT NULL COMMENT 'S - selecting P - pass NP - no pass C - choose  (update status of subject as 1 - means, not available for other projects) B - begin FU - follow up STP - stop O - out D - drop F - Finish (once finish, update volunteer status as 0 - make available to other projects)  ',
  `status_date` datetime NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=90 ;

--
-- Dumping data for table `subject_status`
--

INSERT INTO `subject_status` (`status_id`, `subject_id`, `status`, `status_date`, `remarks`) VALUES
(1, 1, 'S', '2016-12-22 18:05:07', ''),
(2, 1, 'P', '2016-12-22 18:05:37', ''),
(3, 1, 'C', '2016-12-22 18:06:50', ''),
(4, 1, 'B', '2016-12-22 00:00:00', ''),
(5, 2, 'S', '2016-12-23 16:20:36', ''),
(6, 2, 'P', '2016-12-23 16:23:27', ''),
(7, 3, 'S', '2016-12-23 17:27:05', ''),
(8, 3, 'P', '2016-12-23 17:27:39', ''),
(9, 3, 'C', '2016-12-23 17:27:45', ''),
(10, 3, 'B', '2016-12-25 00:00:00', ''),
(11, 4, 'S', '2016-12-25 22:20:12', ''),
(12, 4, 'P', '2016-12-25 22:51:59', ''),
(13, 4, 'C', '2016-12-25 22:52:19', ''),
(14, 4, 'B', '2016-12-25 00:00:00', ''),
(15, 5, 'S', '2016-12-27 10:46:36', ''),
(16, 6, 'S', '2016-12-27 12:01:27', ''),
(17, 6, 'P', '2016-12-27 12:02:06', ''),
(18, 6, 'C', '2016-12-27 12:02:16', ''),
(19, 6, 'B', '2016-12-27 00:00:00', ''),
(20, 7, 'S', '2016-12-28 15:28:38', ''),
(21, 8, 'S', '2016-12-28 15:28:45', ''),
(22, 7, 'P', '2016-12-28 15:35:54', ''),
(23, 8, 'P', '2016-12-28 15:35:57', ''),
(24, 8, 'C', '2016-12-28 15:36:03', ''),
(25, 7, 'C', '2016-12-28 15:36:03', ''),
(26, 8, 'B', '2016-12-28 00:00:00', ''),
(27, 7, 'B', '2016-12-28 00:00:00', ''),
(28, 9, 'S', '2016-12-28 15:44:01', ''),
(29, 9, 'P', '2016-12-28 15:44:27', ''),
(30, 9, 'C', '2016-12-28 15:44:32', ''),
(31, 10, 'S', '2016-12-28 16:03:32', ''),
(32, 10, 'P', '2016-12-28 16:04:00', ''),
(33, 10, 'C', '2016-12-28 16:04:13', ''),
(34, 8, 'FU', '2016-12-28 00:00:00', ''),
(35, 8, 'F', '2016-12-28 00:00:00', ''),
(36, 7, 'STP', '2016-12-28 00:00:00', 'stop'),
(37, 9, 'B', '2016-12-28 00:00:00', ''),
(38, 11, 'S', '2016-12-30 13:56:20', ''),
(39, 11, 'P', '2016-12-30 13:58:11', ''),
(40, 10, 'B', '2016-12-30 00:00:00', ''),
(41, 10, 'STP', '2016-12-30 00:00:00', 'é€€å‡ºåŽŸå› '),
(42, 9, 'FU', '2016-12-30 00:00:00', ''),
(43, 9, 'F', '2016-12-30 00:00:00', ''),
(44, 12, 'S', '2016-12-30 16:21:39', ''),
(45, 12, 'P', '2016-12-30 16:24:12', ''),
(46, 12, 'C', '2016-12-30 16:25:38', ''),
(47, 12, 'B', '2016-12-30 00:00:00', ''),
(48, 12, 'FU', '2016-12-30 00:00:00', ''),
(49, 12, 'F', '2016-12-30 00:00:00', ''),
(50, 13, 'S', '2016-12-30 16:37:58', ''),
(51, 13, 'P', '2016-12-30 16:38:56', ''),
(52, 13, 'C', '2016-12-30 16:39:05', ''),
(53, 13, 'B', '2016-12-30 00:00:00', ''),
(54, 13, 'FU', '2016-12-30 00:00:00', ''),
(55, 13, 'F', '2016-12-30 00:00:00', ''),
(56, 14, 'S', '2016-12-30 16:45:18', ''),
(57, 14, 'P', '2016-12-30 16:45:56', ''),
(58, 14, 'C', '2016-12-30 16:46:05', ''),
(59, 14, 'B', '2016-12-30 00:00:00', ''),
(60, 2, 'C', '2017-01-07 14:23:03', ''),
(61, 14, 'FU', '2017-01-07 00:00:00', ''),
(62, 15, 'S', '2017-01-10 10:41:33', ''),
(63, 11, 'C', '2017-01-10 17:56:07', ''),
(64, 2, 'B', '2017-01-10 00:00:00', ''),
(65, 16, 'S', '2017-01-10 18:30:56', ''),
(66, 17, 'S', '2017-01-10 18:31:01', ''),
(67, 16, 'P', '2017-01-10 18:31:46', ''),
(68, 17, 'P', '2017-01-10 18:31:53', ''),
(69, 15, 'NP', '2017-01-10 18:31:58', ''),
(70, 17, 'C', '2017-01-10 18:32:22', ''),
(71, 16, 'C', '2017-01-10 18:32:22', ''),
(72, 16, 'B', '2017-01-10 00:00:00', ''),
(73, 17, 'B', '2017-01-10 00:00:00', ''),
(74, 16, 'FU', '2017-01-10 00:00:00', ''),
(75, 7, 'NS', '2017-01-11 00:00:00', ''),
(76, 10, 'NS', '2017-01-11 00:00:00', ''),
(77, 11, 'B', '2017-01-12 00:00:00', ''),
(78, 17, 'FU', '2017-01-12 00:00:00', ''),
(79, 5, 'P', '2017-01-12 11:45:59', ''),
(80, 5, 'C', '2017-01-12 11:46:07', ''),
(81, 18, 'S', '2017-01-12 11:47:29', ''),
(82, 16, 'F', '2017-01-12 00:00:00', ''),
(83, 11, 'STP', '2017-01-12 00:00:00', 'stop'),
(84, 11, 'NS', '2017-01-25 00:00:00', ''),
(85, 5, 'NS', '2017-01-27 00:00:00', ''),
(86, 19, 'S', '2017-01-27 10:26:10', ''),
(87, 18, 'P', '2017-01-27 10:27:03', ''),
(88, 18, 'C', '2017-01-27 10:27:34', ''),
(89, 18, 'NS', '2017-02-11 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `summary_files`
--

CREATE TABLE IF NOT EXISTS `summary_files` (
`summary_file_id` int(10) NOT NULL,
  `material_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `file` varchar(225) NOT NULL,
  `date_uploaded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `summary_files`
--

INSERT INTO `summary_files` (`summary_file_id`, `material_id`, `filename`, `file`, `date_uploaded`) VALUES
(1, 1, 'June 10 Revised - UX - June 6 RSL Edits.pdf', 'http://localhost/clinical/stage-summary/1--&--148702752525400.pdf', '2017-02-13 23:12:28'),
(2, 2, '11770881184588103.pdf', 'http://localhost/clinical/stage-summary/2--&--148702754132529.pdf', '2017-02-13 23:12:28');

-- --------------------------------------------------------

--
-- Table structure for table `summary_materials`
--

CREATE TABLE IF NOT EXISTS `summary_materials` (
`material_id` int(10) NOT NULL,
  `stage_summary_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `version` varchar(50) NOT NULL,
  `version_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` varchar(255) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `summary_materials`
--

INSERT INTO `summary_materials` (`material_id`, `stage_summary_id`, `name`, `version`, `version_date`, `description`, `databank`) VALUES
(1, 1, 'stage-summary', '3', '2017-02-13 23:12:28', 'stage-summary', 1),
(2, 1, 'stage-summary2', '2', '2017-02-13 23:12:28', '2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

CREATE TABLE IF NOT EXISTS `trainings` (
`training_id` int(10) NOT NULL,
  `project_id` int(10) NOT NULL,
  `title` varchar(200) NOT NULL,
  `type` tinyint(1) DEFAULT NULL COMMENT '0 - Will start, 1 - Training, 2 - Other',
  `training_date` datetime NOT NULL,
  `place` varchar(300) NOT NULL,
  `host` varchar(200) NOT NULL,
  `meeting_content` varchar(400) NOT NULL,
  `meeting_summary` varchar(400) NOT NULL,
  `is_drafted` tinyint(1) NOT NULL COMMENT '0 - submitted, 1 - draft',
  `created_by` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(5) NOT NULL,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=101 ;

--
-- Dumping data for table `trainings`
--

INSERT INTO `trainings` (`training_id`, `project_id`, `title`, `type`, `training_date`, `place`, `host`, `meeting_content`, `meeting_summary`, `is_drafted`, `created_by`, `date_created`, `updated_by`, `date_updated`) VALUES
(1, 1, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2016-12-22 09:55:28', 0, '0000-00-00 00:00:00'),
(2, 1, 'adf', 1, '0000-00-00 00:00:00', 'df', 'adfa', 'adfa', 'df', 0, 6, '2016-12-22 09:56:09', 0, '0000-00-00 00:00:00'),
(3, 3, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2016-12-23 09:15:26', 0, '0000-00-00 00:00:00'),
(4, 3, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2016-12-25 13:38:36', 0, '0000-00-00 00:00:00'),
(5, 1, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2016-12-26 07:00:58', 0, '0000-00-00 00:00:00'),
(6, 1, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2016-12-26 07:01:21', 0, '0000-00-00 00:00:00'),
(7, 5, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2016-12-27 02:33:30', 0, '0000-00-00 00:00:00'),
(8, 3, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2016-12-27 03:03:25', 0, '0000-00-00 00:00:00'),
(9, 3, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2016-12-27 03:05:51', 0, '0000-00-00 00:00:00'),
(10, 1, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2016-12-27 09:18:08', 0, '0000-00-00 00:00:00'),
(11, 7, 'a', 0, '0000-00-00 00:00:00', 'a', 'a', 'a', 'a', 0, 6, '2016-12-28 07:09:44', 0, '0000-00-00 00:00:00'),
(12, 7, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2016-12-28 07:11:25', 0, '0000-00-00 00:00:00'),
(13, 7, 'b', 1, '0000-00-00 00:00:00', 'b', 'b', 'b', 'b', 0, 6, '2016-12-28 07:11:35', 0, '0000-00-00 00:00:00'),
(14, 7, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2016-12-28 07:11:52', 0, '0000-00-00 00:00:00'),
(15, 12, 'é˜¿é“å¤«', 1, '0000-00-00 00:00:00', 'é˜¿é“å¤«', 'é˜¿é“å¤«', 'é˜¿é“å¤«', 'é˜¿é“å¤«', 0, 6, '2016-12-30 05:49:26', 0, '0000-00-00 00:00:00'),
(16, 13, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2016-12-30 08:14:39', 0, '0000-00-00 00:00:00'),
(17, 13, '1', 0, '0000-00-00 00:00:00', '1', '11', '11', '11', 0, 6, '2016-12-30 08:16:32', 0, '0000-00-00 00:00:00'),
(18, 13, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-07 07:53:46', 0, '0000-00-00 00:00:00'),
(19, 13, 'kjakl', 2, '0000-00-00 00:00:00', 'df', 'df', 'adf', 'adf', 0, 6, '2017-01-07 08:13:30', 0, '0000-00-00 00:00:00'),
(20, 13, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-10 03:19:06', 0, '0000-00-00 00:00:00'),
(21, 13, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-10 09:50:01', 0, '0000-00-00 00:00:00'),
(22, 27, 'äºŒç”²åŒèƒBEå®žéªŒçš„å¯åŠ¨ä¼š', 0, '0000-00-00 00:00:00', 'å¹¿ä¸œçœäººæ°‘åŒ»é™¢ä¼Ÿä¼¦æ¥¼13æ¥¼ä¼šè®®å®¤', 'æœæ™´', 'è¯¦è§é™„ä»¶', 'è¯¦è§é™„ä»¶', 0, 6, '2017-01-10 10:16:00', 0, '0000-00-00 00:00:00'),
(23, 27, 'äºŒç”²åŒèƒBEå®žéªŒç ”ç©¶åŸ¹è®­ä¼š', 1, '0000-00-00 00:00:00', 'å¹¿ä¸œçœäººæ°‘åŒ»é™¢ä¼Ÿä¼¦æ¥¼13æ¥¼ä¼šè®®å®¤', 'æœæ™´', 'è¯¦è§é™„ä»¶', 'è¯¦è§é™„ä»¶', 0, 6, '2017-01-10 10:19:07', 0, '0000-00-00 00:00:00'),
(24, 13, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-11 01:46:23', 0, '0000-00-00 00:00:00'),
(25, 13, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-12 02:47:33', 0, '0000-00-00 00:00:00'),
(26, 13, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-12 06:30:55', 0, '0000-00-00 00:00:00'),
(27, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-16 07:48:23', 0, '0000-00-00 00:00:00'),
(28, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-16 07:51:30', 0, '0000-00-00 00:00:00'),
(29, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-16 07:51:53', 0, '0000-00-00 00:00:00'),
(30, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-16 07:52:14', 0, '0000-00-00 00:00:00'),
(31, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-16 07:52:40', 0, '0000-00-00 00:00:00'),
(32, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-16 07:53:32', 0, '0000-00-00 00:00:00'),
(33, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-16 07:53:40', 0, '0000-00-00 00:00:00'),
(34, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-16 08:08:34', 0, '0000-00-00 00:00:00'),
(35, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 03:28:37', 0, '0000-00-00 00:00:00'),
(36, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 03:34:17', 0, '0000-00-00 00:00:00'),
(37, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 03:35:39', 0, '0000-00-00 00:00:00'),
(38, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 03:35:44', 0, '0000-00-00 00:00:00'),
(39, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 06:58:55', 0, '0000-00-00 00:00:00'),
(40, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 06:59:18', 0, '0000-00-00 00:00:00'),
(41, 27, 'jdjd', 0, '0000-00-00 00:00:00', 'a', 'adf', 'adf', 'adf', 0, 6, '2017-01-18 06:59:25', 0, '0000-00-00 00:00:00'),
(42, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 07:02:57', 0, '0000-00-00 00:00:00'),
(43, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 07:04:20', 0, '0000-00-00 00:00:00'),
(44, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 07:05:39', 0, '0000-00-00 00:00:00'),
(45, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 07:07:21', 0, '0000-00-00 00:00:00'),
(46, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 07:07:28', 0, '0000-00-00 00:00:00'),
(47, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 07:07:42', 0, '0000-00-00 00:00:00'),
(48, 27, 'jdjd', 0, '0000-00-00 00:00:00', 'a', 'adf', 'adf', 'adf', 0, 6, '2017-01-18 07:13:43', 0, '0000-00-00 00:00:00'),
(49, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 08:54:28', 0, '0000-00-00 00:00:00'),
(50, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 08:55:53', 0, '0000-00-00 00:00:00'),
(51, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 08:56:01', 0, '0000-00-00 00:00:00'),
(52, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 09:01:06', 0, '0000-00-00 00:00:00'),
(53, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 09:01:10', 0, '0000-00-00 00:00:00'),
(54, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 09:14:03', 0, '0000-00-00 00:00:00'),
(55, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 09:32:51', 0, '0000-00-00 00:00:00'),
(56, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 09:33:05', 0, '0000-00-00 00:00:00'),
(57, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-18 09:33:13', 0, '0000-00-00 00:00:00'),
(58, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 01:54:48', 0, '0000-00-00 00:00:00'),
(59, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 02:30:41', 0, '0000-00-00 00:00:00'),
(60, 27, '1', 0, '0000-00-00 00:00:00', '1', '', '1', '1', 0, 6, '2017-01-19 02:30:44', 0, '0000-00-00 00:00:00'),
(61, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 02:31:16', 0, '0000-00-00 00:00:00'),
(62, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 02:32:31', 0, '0000-00-00 00:00:00'),
(63, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 03:48:34', 0, '0000-00-00 00:00:00'),
(64, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 03:50:37', 0, '0000-00-00 00:00:00'),
(65, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 03:51:08', 0, '0000-00-00 00:00:00'),
(66, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 03:51:33', 0, '0000-00-00 00:00:00'),
(67, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 03:52:17', 0, '0000-00-00 00:00:00'),
(68, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 03:52:20', 0, '0000-00-00 00:00:00'),
(69, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 03:52:23', 0, '0000-00-00 00:00:00'),
(70, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 03:52:49', 0, '0000-00-00 00:00:00'),
(71, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 03:52:56', 0, '0000-00-00 00:00:00'),
(72, 27, 'test', 1, '0000-00-00 00:00:00', 'test', 'test', 'test', 'test', 0, 6, '2017-01-19 03:53:04', 0, '0000-00-00 00:00:00'),
(73, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 03:53:43', 0, '0000-00-00 00:00:00'),
(74, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 08:01:29', 0, '0000-00-00 00:00:00'),
(75, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 08:01:38', 0, '0000-00-00 00:00:00'),
(76, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 08:02:05', 0, '0000-00-00 00:00:00'),
(77, 27, '1', 1, '0000-00-00 00:00:00', '1', '1', '1', '1', 0, 6, '2017-01-19 08:02:40', 0, '0000-00-00 00:00:00'),
(78, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-19 08:03:04', 0, '0000-00-00 00:00:00'),
(79, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-22 03:24:55', 0, '0000-00-00 00:00:00'),
(80, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-22 03:25:15', 0, '0000-00-00 00:00:00'),
(81, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-23 08:42:53', 0, '0000-00-00 00:00:00'),
(82, 27, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-01-25 06:15:50', 0, '0000-00-00 00:00:00'),
(83, 108, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-02-06 21:36:58', 0, '0000-00-00 00:00:00'),
(84, 108, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-02-06 23:32:23', 0, '0000-00-00 00:00:00'),
(90, 111, 'TEst Databank', 2, '0000-00-00 00:00:00', 'Hellow', 'Hellow', 'Hi There', 'Hellowwwww', 0, 6, '2017-02-08 22:17:46', 0, '0000-00-00 00:00:00'),
(91, 111, 'Test Databank 2', 1, '0000-00-00 00:00:00', 'Test Databank 2', 'Test Databank 2', 'Test Databank 2', 'Test Databank 2', 0, 6, '2017-02-08 22:58:00', 0, '0000-00-00 00:00:00'),
(98, 111, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-02-16 19:12:32', 0, '0000-00-00 00:00:00'),
(99, 111, 'Test 28s', 1, '2017-02-28 00:00:00', 'Test', 'hey', 'Hello', 'Hello', 0, 6, '2017-02-16 19:13:23', 0, '0000-00-00 00:00:00'),
(100, 111, '', NULL, '0000-00-00 00:00:00', '', '', '', '', 0, 6, '2017-02-16 19:14:16', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `trainings_participant`
--

CREATE TABLE IF NOT EXISTS `trainings_participant` (
`participant_id` int(5) NOT NULL,
  `training_id` int(5) NOT NULL,
  `role_id` int(5) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `trainings_participant`
--

INSERT INTO `trainings_participant` (`participant_id`, `training_id`, `role_id`) VALUES
(1, 2, 8),
(2, 15, 8),
(3, 15, 9),
(4, 17, 8),
(5, 17, 9),
(6, 17, 10),
(7, 17, 11),
(8, 19, 8),
(9, 22, 8),
(10, 22, 9),
(11, 22, 10),
(12, 22, 11),
(13, 23, 8),
(14, 23, 9),
(15, 23, 10),
(16, 23, 11),
(17, 22, 8),
(18, 22, 9),
(19, 22, 10),
(20, 22, 11),
(21, 23, 8),
(22, 23, 9),
(23, 23, 10),
(24, 23, 11),
(25, 23, 8),
(26, 23, 9),
(27, 23, 10),
(28, 23, 11),
(29, 41, 8),
(30, 41, 8),
(31, 41, 8),
(32, 41, 8),
(33, 41, 8),
(34, 48, 8),
(35, 99, 11),
(36, 99, 79),
(37, 99, 11),
(38, 99, 79),
(39, 99, 11),
(40, 99, 79);

-- --------------------------------------------------------

--
-- Table structure for table `training_material`
--

CREATE TABLE IF NOT EXISTS `training_material` (
`material_id` int(10) NOT NULL,
  `training_id` int(10) NOT NULL,
  `name` varchar(150) NOT NULL,
  `version` int(2) NOT NULL,
  `version_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(300) NOT NULL,
  `up_path` varchar(250) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `training_material`
--

INSERT INTO `training_material` (`material_id`, `training_id`, `name`, `version`, `version_date`, `description`, `up_path`, `databank`) VALUES
(1, 15, 'aåœ°æ–¹', 1, '2016-12-30 05:49:55', '', 'http://betaprojex.com/Clinical2/uploads/training_material/1483076995.docx', 0),
(2, 16, '1', 1, '2016-12-30 08:15:16', '1', 'http://betaprojex.com/Clinical2/uploads/training_material/1483085716.jpg', 0),
(3, 22, 'é¡¹ç›®å¯åŠ¨ä¼šä¼šè®®è®°å½•', 1, '2017-01-10 10:18:44', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/training_material/1484043524.docx', 0),
(4, 23, 'åŸ¹è®­ä¼šä¼šè®®è®°å½•', 1, '2017-01-10 10:20:17', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/training_material/1484043617.docx', 0),
(5, 23, 'haha', 1, '2017-01-16 07:48:57', 'enen', 'http://betaprojex.com/Clinical2/uploads/training_material/1484552937.docx', 0),
(6, 41, 'DF', 1, '2017-01-18 07:03:51', '', 'http://betaprojex.com/Clinical2/uploads/training_material/1484723031.docx', 0),
(7, 41, 'adf', 1, '2017-01-18 07:07:04', 'adf', 'http://betaprojex.com/Clinical2/uploads/training_material/1484723224.docx', 0),
(8, 48, 'adf', 1, '2017-01-18 07:14:00', 'adf', 'http://betaprojex.com/Clinical2/uploads/training_material/1484723640.docx', 0),
(9, 58, 'qweqwe', 1, '2017-01-19 01:55:26', '1', 'http://betaprojex.com/Clinical2/uploads/training_material/1484790926.jpg', 0),
(10, 22, 'qweqwe', 1, '2017-01-19 01:59:48', '1', 'http://betaprojex.com/Clinical2/uploads/training_material/1484791188.jpg', 0),
(11, 82, '1', 1, '2017-01-25 06:17:00', 'asd', 'http://betaprojex.com/Clinical2/uploads/training_material/1485325019.jpg', 0),
(12, 22, '1', 1, '2017-01-25 06:18:08', 'asd', 'http://betaprojex.com/Clinical2/uploads/training_material/1485325088.png', 0),
(13, 90, 'Test Databank Training', 1, '2017-02-08 22:18:10', 'Checked Upload', 'http://localhost/clinical/uploads/training_material/1486592290.png', 0),
(14, 91, 'Test Databank 2', 2, '2017-02-08 22:58:23', 'Test Databank 2', 'http://localhost/clinical/uploads/training_material/1486594703.png', 1),
(15, 99, 'asd', 1, '2017-02-16 19:13:49', 'asd', 'http://localhost/clinical/uploads/training_material/1487272429.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trials`
--

CREATE TABLE IF NOT EXISTS `trials` (
`trial_id` int(11) NOT NULL,
  `trial_name` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `trials`
--

INSERT INTO `trials` (`trial_id`, `trial_name`) VALUES
(1, 'I Stage'),
(2, 'II Stage'),
(3, 'III Stage'),
(4, 'IV Stage'),
(5, 'BE Trial'),
(6, 'Clinical verifications');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE IF NOT EXISTS `uploads` (
`up_id` int(11) NOT NULL,
  `up_name` varchar(255) NOT NULL COMMENT 'name of document',
  `role_id` int(11) NOT NULL COMMENT 'role_id of uploader',
  `up_ver` float NOT NULL COMMENT 'version of document',
  `up_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `up_desc` mediumtext NOT NULL,
  `up_path` varchar(255) NOT NULL,
  `up_type` int(11) NOT NULL COMMENT '1 - project document, 2 - project plan, 3 - PRE Comments, 5 - Job Specification',
  `project_id` int(11) NOT NULL,
  `databank` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='table for `project documents` and `project plan` or more (?)' AUTO_INCREMENT=166 ;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`up_id`, `up_name`, `role_id`, `up_ver`, `up_date`, `up_desc`, `up_path`, `up_type`, `project_id`, `databank`) VALUES
(1, 'é˜¿é“å¤«', 5, 1, '2016-12-22 09:41:14', '', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1482399674.docx', 3, 1, 0),
(2, 'é˜¿é“å¤«', 6, 1, '2016-12-22 09:41:54', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482399714.docx', 2, 1, 0),
(3, 'é˜¿é“å¤«', 6, 12, '2016-12-22 09:46:49', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482400009.docx', 1, 1, 0),
(4, 'adf ', 20, 2, '2016-12-22 09:48:36', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482400116.docx', 1, 1, 0),
(5, 'adf ', 6, 1, '2016-12-22 09:49:17', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482400157.docx', 2, 1, 0),
(6, 'adf ', 20, 1, '2016-12-22 09:50:31', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482400231.docx', 1, 1, 0),
(7, 'adf', 6, 1, '2016-12-22 09:55:15', 'adf', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1482400515.docx', 5, 1, 0),
(8, '1', 1, 1, '2016-12-23 03:16:31', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482462991.pdf', 1, 2, 0),
(9, '1', 1, 1, '2016-12-23 03:17:13', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482463033.png', 2, 2, 0),
(10, '1pre', 5, 1, '2016-12-23 03:26:55', 'pre pre', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1482463615.docx', 3, 2, 0),
(11, '2', 6, 2, '2016-12-23 03:37:03', '2', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482464223.docx', 2, 2, 0),
(12, 'adf', 5, 1, '2016-12-23 08:40:28', '', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1482482428.docx', 3, 3, 0),
(13, 'df', 6, 1, '2016-12-23 08:41:06', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482482466.docx', 2, 3, 0),
(14, 'adf', 6, 1, '2016-12-23 08:42:27', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482482547.docx', 1, 3, 0),
(15, 'daf', 20, 1, '2016-12-23 08:47:09', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482482829.docx', 1, 3, 0),
(16, 'adf', 6, 1, '2016-12-23 08:48:00', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482482880.docx', 2, 3, 0),
(17, 'adf', 20, 1, '2016-12-23 08:53:57', 'adf', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482483237.docx', 1, 3, 0),
(18, 'df', 6, 2, '2016-12-23 08:59:14', '', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1482483554.docx', 5, 3, 0),
(19, 'df', 6, 1, '2016-12-23 09:02:32', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482483752.docx', 1, 3, 0),
(20, 'adf', 1, 1, '2016-12-23 09:05:56', 'adf', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482483956.docx', 1, 3, 0),
(21, 'adf ', 6, 0, '2016-12-26 03:35:40', 'df', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1482672940.docx', 5, 3, 0),
(22, 'adf', 5, 1, '2016-12-26 03:56:18', '', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1482674178.docx', 3, 5, 0),
(23, 'er', 6, 12, '2016-12-26 03:59:48', 'df', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482674387.docx', 2, 5, 0),
(24, '12', 6, 1, '2016-12-26 04:01:29', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482674489.docx', 1, 5, 0),
(25, 'ha', 20, 12, '2016-12-26 04:05:03', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482674703.docx', 1, 5, 0),
(26, '12', 20, 12, '2016-12-26 04:08:02', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482674882.docx', 1, 5, 0),
(27, '12', 6, 12, '2016-12-26 04:09:18', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482674958.docx', 2, 5, 0),
(28, 'adf', 20, 12, '2016-12-26 04:12:08', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482675128.docx', 1, 5, 0),
(29, 'adf', 6, 1, '2016-12-26 04:13:36', '', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1482675216.docx', 5, 5, 0),
(30, 'k1', 1, 1, '2016-12-28 15:37:28', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482889048.png', 1, 6, 0),
(31, 'k2', 1, 1, '2016-12-28 15:37:54', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482889074.png', 1, 6, 0),
(32, 'z1', 1, 1, '2016-12-28 15:38:19', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482889099.png', 2, 6, 0),
(33, 'x2', 1, 1, '2016-12-28 15:38:44', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482889124.png', 2, 6, 0),
(34, 'x2', 1, 1, '2016-12-28 15:38:51', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482889131.png', 2, 6, 0),
(35, 'a', 1, 1, '2016-12-28 19:48:44', 'a', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482904124.txt', 1, 7, 0),
(36, 'b', 1, 2, '2016-12-28 19:49:01', 'b', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482904141.txt', 1, 7, 0),
(38, 'a', 1, 1, '2016-12-28 19:49:46', 'a', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482904186.txt', 2, 7, 0),
(39, 'b', 1, 1, '2016-12-28 19:49:56', 'b', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482904196.txt', 2, 7, 0),
(40, 'a', 5, 1, '2016-12-28 19:54:03', 'a', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1482904443.txt', 3, 7, 0),
(41, 'c', 6, 3, '2016-12-28 19:54:44', 'c', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482904484.txt', 2, 7, 0),
(42, 'c', 6, 3, '2016-12-28 20:07:06', 'c', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482905226.txt', 1, 7, 0),
(43, 'd', 20, 4, '2016-12-28 20:50:00', 'd', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482907800.txt', 1, 7, 0),
(44, 'd', 6, 4, '2016-12-28 20:50:42', 'd', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482907842.txt', 2, 7, 0),
(45, 'e', 20, 5, '2016-12-28 20:51:38', 'e', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482907898.txt', 1, 7, 0),
(46, 'a', 6, 1, '2016-12-28 21:12:52', 'a', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1482909172.txt', 5, 7, 0),
(47, 'b', 6, 2, '2016-12-28 21:13:04', 'b', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1482909184.txt', 5, 7, 0),
(48, 'test', 1, 1, '2016-12-28 22:33:50', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482914030.jpg', 1, 7, 0),
(49, 'adf', 1, 0, '2016-12-28 23:24:02', 'df', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482917042.docx', 1, 5, 0),
(50, '1', 6, 1, '2016-12-28 23:45:19', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482918319.txt', 1, 7, 0),
(55, 'a', 1, 1, '2016-12-29 17:38:25', 'a', 'http://betaprojex.com/Clinical2/uploads/project_documents/1482982705.txt', 1, 7, 0),
(56, 'hjg', 1, 1, '2016-12-29 17:43:26', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1482983006.docx', 2, 8, 0),
(57, 'é˜¿é“å¤«', 1, 1, '2016-12-30 19:06:18', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483074378.docx', 1, 12, 0),
(58, 'wrt', 5, 1, '2016-12-30 19:15:04', '', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1483074904.docx', 3, 12, 0),
(59, 'æ–°æ–¹æ¡ˆ', 6, 2, '2016-12-30 19:16:23', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1483074983.docx', 2, 12, 0),
(60, 'qdf', 6, 1, '2016-12-30 19:19:21', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483075161.docx', 1, 12, 0),
(61, 'huiyi jilu', 20, 1, '2016-12-30 19:33:59', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483076039.docx', 1, 12, 0),
(62, 'xifnagan', 6, 2, '2016-12-30 19:34:48', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1483076088.docx', 2, 12, 0),
(63, 'æ‰¹æ–‡', 20, 1, '2016-12-30 19:38:51', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483076331.docx', 1, 12, 0),
(64, 'å•Šå¤§æ¡†æž¶å‘', 6, 1, '2016-12-30 19:49:09', 'é˜¿é“å¤«', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1483076949.docx', 5, 12, 0),
(65, '2', 1, 2, '2016-12-30 21:12:03', '2', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483081923.jpg', 1, 13, 0),
(66, '2', 1, 2, '2016-12-30 21:12:07', '2', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483081927.jpg', 1, 13, 0),
(67, '1', 1, 1, '2016-12-30 21:13:28', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1483082008.jpg', 2, 13, 0),
(68, '1', 5, 1, '2016-12-30 21:31:40', '1', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1483083100.jpg', 3, 13, 0),
(69, '1', 6, 1, '2016-12-30 21:35:53', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1483083353.jpg', 2, 13, 0),
(70, '1', 6, 1, '2016-12-30 21:37:36', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483083456.jpg', 1, 13, 0),
(71, '1', 20, 11, '2016-12-30 21:43:00', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483083780.jpg', 1, 13, 0),
(72, '1', 20, 1, '2016-12-30 21:57:45', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483084665.jpg', 1, 13, 0),
(73, '1', 6, 1, '2016-12-30 21:58:54', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1483084734.jpg', 2, 13, 0),
(74, '1', 6, 1, '2016-12-30 22:02:09', '11', 'http://betaprojex.com/Clinical2/uploads/project_plan/1483084929.jpg', 2, 13, 0),
(75, '1', 6, 1, '2016-12-30 22:02:14', '11', 'http://betaprojex.com/Clinical2/uploads/project_plan/1483084934.jpg', 2, 13, 0),
(76, '1', 20, 11, '2016-12-30 22:04:25', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483085065.jpg', 1, 13, 0),
(77, '1', 20, 1, '2016-12-30 22:11:32', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483085492.jpg', 1, 13, 0),
(78, '1', 6, 1, '2016-12-30 22:14:24', '1', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1483085664.jpg', 5, 13, 0),
(79, '1', 1, 1, '2016-12-31 00:03:47', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483092227.docx', 1, 13, 0),
(80, '1', 1, 11, '2016-12-31 00:04:38', '1', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483092278.docx', 1, 13, 0),
(81, '1', 6, 1, '2016-12-31 00:07:17', '11', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483092437.docx', 1, 13, 0),
(82, '11', 6, 1, '2016-12-31 00:07:52', '11', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483092472.docx', 1, 13, 0),
(83, '1', 1, 1, '2016-12-31 00:18:09', '11', 'http://betaprojex.com/Clinical2/uploads/project_documents/1483093089.docx', 1, 13, 0),
(84, 'æ–¹æ¡ˆ1', 1, 1, '2017-01-10 15:54:54', 'AD', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484013294.png', 2, 20, 0),
(85, 'æ–¹æ¡ˆ1', 1, 1, '2017-01-10 15:54:57', 'AD', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484013297.png', 2, 20, 0),
(86, 'yijian', 5, 1, '2017-01-10 16:13:57', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014437.docx', 3, 20, 0),
(87, 'yijian', 5, 1, '2017-01-10 16:14:00', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014439.png', 3, 20, 0),
(88, 'yijian', 5, 1, '2017-01-10 16:14:00', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014440.docx', 3, 20, 0),
(89, 'yijian', 5, 1, '2017-01-10 16:14:02', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014442.docx', 3, 20, 0),
(90, 'yijian', 5, 1, '2017-01-10 16:14:04', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014444.docx', 3, 20, 0),
(91, 'yijian', 5, 1, '2017-01-10 16:14:05', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014445.docx', 3, 20, 0),
(92, 'yijian', 5, 1, '2017-01-10 16:14:10', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014450.docx', 3, 20, 0),
(93, 'yijian', 5, 1, '2017-01-10 16:14:11', 'df', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484014450.docx', 3, 20, 0),
(95, 'test', 6, 2, '2017-01-10 21:11:15', '1', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484032275.pdf', 2, 20, 0),
(96, 'æ–°æ–¹æ¡ˆ', 6, 1, '2017-01-10 21:23:49', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484033029.docx', 2, 20, 0),
(97, 'haha', 6, 1, '2017-01-10 21:25:29', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484033129.docx', 1, 20, 0),
(98, '12', 6, 1, '2017-01-10 22:06:35', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484035595.docx', 1, 20, 0),
(99, 'ç—…ä¾‹æŠ¥å‘Šè¡¨', 1, 1, '2017-01-10 23:09:35', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484039375.docx', 1, 27, 0),
(100, 'çŸ¥æƒ…åŒæ„ä¹¦', 1, 1, '2017-01-10 23:10:11', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484039411.docx', 1, 27, 0),
(101, 'ä¸´åºŠè¯•éªŒè®¡åˆ’ä¸Žç ”ç©¶æ–¹æ¡ˆ', 1, 1, '2017-01-10 23:10:41', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484039441.docx', 1, 27, 0),
(102, 'ä¸´åºŠè¯•éªŒè®¡åˆ’ä¸Žç ”ç©¶æ–¹æ¡ˆ', 1, 1, '2017-01-10 23:10:58', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484039458.docx', 2, 27, 0),
(103, 'ç ”ç©¶è€…å‘èµ·çš„é¡¹ç›®å®¡è®®è¡¨', 5, 1, '2017-01-10 23:29:53', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484040593.docx', 3, 27, 0),
(104, 'äºŒç”²åŒèƒç ”ç©¶æ–¹æ¡ˆï¼ˆä¿®æ”¹ç‰ˆï¼‰', 6, 2, '2017-01-10 23:31:56', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484040716.docx', 2, 27, 0),
(105, 'äºŒç”²åŒèƒç ”ç©¶æ–¹æ¡ˆ--ç­¾å­—ç›–ç« è¡¨', 6, 1, '2017-01-10 23:36:09', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484040967.pdf', 1, 27, 0),
(106, 'äºŒç”²åŒèƒç ”ç©¶ç›¸å…³èµ„æ–™1', 6, 1, '2017-01-10 23:36:36', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484040996.pdf', 1, 27, 0),
(107, 'äºŒç”²åŒèƒç ”ç©¶ç›¸å…³èµ„æ–™2', 6, 1, '2017-01-10 23:36:54', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484041014.pdf', 1, 27, 0),
(108, 'äºŒç”²åŒèƒç ”ç©¶ç›¸å…³èµ„æ–™3', 6, 1, '2017-01-10 23:37:16', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484041036.pdf', 1, 27, 0),
(109, 'ä¼¦ç†å§”å‘˜ä¼šä¼šè®®è®°å½•', 20, 1, '2017-01-10 23:51:44', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484041904.pdf', 1, 27, 0),
(110, 'ä¼¦ç†å§”å‘˜ä¼šæ„è§æ•´ç†', 20, 1, '2017-01-10 23:52:21', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484041941.pdf', 1, 27, 0),
(112, 'äºŒç”²åŒèƒç ”ç©¶æ–¹æ¡ˆæœ€ç»ˆç‰ˆ', 6, 3, '2017-01-11 00:03:58', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484042638.docx', 2, 27, 0),
(113, 'ä¼¦ç†å§”å‘˜ä¼šä¸»ä»»ç­¾åçš„æ‰¹å‡†æ–‡ä»¶', 20, 1, '2017-01-11 00:10:55', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484043055.pdf', 1, 27, 0),
(114, 'å·¥ä½œè§„èŒƒåŠè®°å½•è¡¨', 6, 1, '2017-01-11 00:15:45', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1484043345.doc', 5, 27, 0),
(115, 'adf ', 5, 1, '2017-01-11 21:40:26', 'adf', 'http://betaprojex.com/Clinical2/uploads/pre_comments/1484120426.docx', 3, 28, 0),
(116, 'kjdf', 6, 1, '2017-01-11 21:42:22', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484120542.docx', 2, 28, 0),
(117, 'kjdf', 6, 1, '2017-01-11 21:42:28', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484120548.docx', 2, 28, 0),
(118, 'kajdf', 6, 1, '2017-01-11 21:44:25', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484120664.docx', 1, 28, 0),
(119, 'hahah', 20, 12, '2017-01-11 21:51:28', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484121088.docx', 1, 28, 0),
(120, 'hahah', 20, 12, '2017-01-11 21:51:33', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484121093.docx', 1, 28, 0),
(121, 'kjdf', 20, 1, '2017-01-11 21:54:18', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484121258.docx', 1, 28, 0),
(122, 'kjdf', 20, 1, '2017-01-11 21:54:26', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484121266.docx', 1, 28, 0),
(123, 'df', 6, 1, '2017-01-11 21:55:26', '', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484121326.docx', 2, 28, 0),
(124, 'jh', 20, 1, '2017-01-11 21:57:18', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484121438.docx', 1, 28, 0),
(125, '', 6, 0, '2017-01-12 22:01:48', '', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484208108.png', 1, 1, 0),
(127, 'ã€Šç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡äººä½“ç”Ÿç‰©åˆ©ç”¨åº¦å’Œç”Ÿç‰©ç­‰æ•ˆæ€§ç ”ç©¶ã€‹CRFè¡¨æ ¼', 22, 1, '2017-01-20 13:49:16', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484869756.doc', 1, 34, 0),
(128, 'ã€Šç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡äººä½“ç”Ÿç‰©åˆ©ç”¨åº¦å’Œç”Ÿç‰©ç­‰æ•ˆæ€§ç ”ç©¶ã€‹çŸ¥æƒ…åŒæ„ä¹¦', 22, 1, '2017-01-20 13:49:59', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_documents/1484869799.docx', 1, 34, 0),
(129, 'ã€Šç›é…¸äºŒç”²åŒèƒè‚ æº¶ç‰‡äººä½“ç”Ÿç‰©åˆ©ç”¨åº¦å’Œç”Ÿç‰©ç­‰æ•ˆæ€§ç ”ç©¶ã€‹ç ”ç©¶æ–¹æ¡ˆ', 22, 1, '2017-01-20 13:50:25', 'æ— ', 'http://betaprojex.com/Clinical2/uploads/project_plan/1484869825.docx', 2, 34, 0),
(130, 'æ‚£æ•™', 6, 1, '2017-01-22 17:23:59', '', 'http://betaprojex.com/Clinical2/uploads/job_specifications/1485055439.docx', 5, 27, 0),
(131, 'hey', 6, 1, '2017-02-04 07:49:58', '1', 'http://localhost/clinical/uploads/project_plan/1486194598.png', 2, 94, 0),
(132, '1', 6, 1, '2017-02-04 08:33:41', '1', 'http://localhost/clinical/uploads/project_documents/1486197221.jpg', 1, 94, 0),
(133, 'Refresher', 1, 1, '2017-02-07 11:11:41', 'Refresher', 'http://localhost/clinical/uploads/project_documents/1486408301.png', 1, 108, 0),
(134, 'Refresher', 1, 1, '2017-02-07 11:11:54', 'RefresherRefresher', 'http://localhost/clinical/uploads/project_plan/1486408314.png', 2, 108, 0),
(135, 'Refresher', 6, 2, '2017-02-07 11:32:15', 'Refresher', 'http://localhost/clinical/uploads/project_plan/1486409535.png', 2, 108, 0),
(136, 'Refresher', 5, 1, '2017-02-07 12:03:53', 'thissssss', 'http://localhost/clinical/uploads/pre_comments/1486411433.png', 3, 108, 0),
(137, 'Refresher', 6, 3, '2017-02-07 12:04:30', 'thisssss', 'http://localhost/clinical/uploads/project_plan/1486411470.png', 2, 108, 0),
(138, 'Refresher', 6, 2, '2017-02-07 13:21:06', 'Refresher', 'http://localhost/clinical/uploads/project_documents/1486416066.png', 1, 108, 0),
(139, 'Refresher', 20, 3, '2017-02-07 13:25:57', 'RefresherRefresher', 'http://localhost/clinical/uploads/project_documents/1486416357.png', 1, 108, 0),
(140, 'Refresher', 6, 4, '2017-02-07 13:27:32', 'RefresherRefresherRefresher', 'http://localhost/clinical/uploads/project_plan/1486416452.png', 2, 108, 0),
(141, 'RefresherRefresher', 20, 4, '2017-02-07 13:33:52', 'RefresherRefresherRefresher', 'http://localhost/clinical/uploads/project_documents/1486416832.png', 1, 108, 0),
(142, 'RefresherRefresher', 6, 5, '2017-02-07 13:52:24', 'Refresherasdh', 'http://localhost/clinical/uploads/project_documents/1486417944.png', 1, 108, 0),
(143, 'Test Function', 5, 1, '2017-02-08 13:42:39', 'Test Function', 'http://localhost/clinical/uploads/pre_comments/1486503759.png', 3, 109, 0),
(144, 'Test Function', 6, 1, '2017-02-08 13:42:58', 'Test Function', 'http://localhost/clinical/uploads/project_plan/1486503778.png', 2, 109, 0),
(145, 'Test Function', 6, 1, '2017-02-08 13:44:03', 'Test Function', 'http://localhost/clinical/uploads/project_documents/1486503843.png', 1, 109, 0),
(146, 'Test Function', 20, 2, '2017-02-08 13:46:09', 'Test Function', 'http://localhost/clinical/uploads/project_documents/1486503969.png', 1, 109, 0),
(147, 'Test Function', 6, 2, '2017-02-08 13:46:47', 'Test FunctionTest Function', 'http://localhost/clinical/uploads/project_plan/1486504007.png', 2, 109, 0),
(148, 'Test Function2', 20, 3, '2017-02-08 13:47:42', 'Test Function', 'http://localhost/clinical/uploads/project_documents/1486504062.png', 1, 109, 0),
(149, 'Test Databank 1', 1, 1, '2017-02-09 09:57:47', 'Checked PP', 'http://localhost/clinical/uploads/project_plan/1486576667.png', 2, 111, 1),
(150, 'Test Databank 2', 1, 1, '2017-02-09 09:58:30', 'Checked PD', 'http://localhost/clinical/uploads/project_documents/1486576710.png', 1, 111, 1),
(151, 'Test Databank 3', 1, 2, '2017-02-09 09:58:56', 'Unchecked PP', 'http://localhost/clinical/uploads/project_plan/1486576736.png', 2, 111, 0),
(152, 'Test Databank 4', 1, 2, '2017-02-09 09:59:54', 'Checked Multiple Upload PD', 'http://localhost/clinical/uploads/project_documents/1486576794.png', 1, 111, 1),
(153, 'Test Databank 4', 1, 2, '2017-02-09 09:59:54', 'Checked Multiple Upload PD', 'http://localhost/clinical/uploads/project_documents/1486576794.png', 1, 111, 1),
(154, 'Test Databank 4', 1, 2, '2017-02-09 09:59:54', 'Checked Multiple Upload PD', 'http://localhost/clinical/uploads/project_documents/1486576794.png', 1, 111, 1),
(155, 'Test Databank 5', 1, 3, '2017-02-09 10:00:47', 'Unchecked Multiple Upload PP', 'http://localhost/clinical/uploads/project_plan/1486576847.png', 2, 111, 0),
(156, 'Test Databank 5', 1, 3, '2017-02-09 10:00:47', 'Unchecked Multiple Upload PP', 'http://localhost/clinical/uploads/project_plan/1486576847.png', 2, 111, 0),
(157, 'Test Databank 5', 1, 3, '2017-02-09 10:00:47', 'Unchecked Multiple Upload PP', 'http://localhost/clinical/uploads/project_plan/1486576847.png', 2, 111, 0),
(158, 'Test Databank PRE', 5, 1, '2017-02-09 10:12:29', 'Checked Multiselect Uploads PRE', 'http://localhost/clinical/uploads/pre_comments/1486577549.png', 3, 111, 1),
(159, 'Test Databank PRE', 5, 1, '2017-02-09 10:12:29', 'Checked Multiselect Uploads PRE', 'http://localhost/clinical/uploads/pre_comments/1486577549.png', 3, 111, 1),
(160, 'Test Databank I', 6, 4, '2017-02-09 10:13:35', 'Checked Upload', 'http://localhost/clinical/uploads/project_plan/1486577615.png', 2, 111, 1),
(161, 'Test Databank I-2', 6, 3, '2017-02-09 10:17:01', 'Checked Upload', 'http://localhost/clinical/uploads/project_documents/1486577821.png', 1, 111, 1),
(162, 'Test Databank ECS ', 20, 1, '2017-02-09 10:19:20', 'Unchecked Upload', 'http://localhost/clinical/uploads/project_documents/1486577960.png', 1, 111, 0),
(163, 'Test Databank I-3', 6, 1, '2017-02-09 10:20:02', 'Checked Upload', 'http://localhost/clinical/uploads/project_plan/1486578002.png', 2, 111, 1),
(164, 'Test Databank ECS - Approval', 20, 1, '2017-02-09 10:21:27', 'Checked Upload', 'http://localhost/clinical/uploads/project_documents/1486578087.png', 1, 111, 1),
(165, 'Test Databank Job Specification', 6, 1, '2017-02-09 14:03:31', 'Checked Upload', 'http://localhost/clinical/uploads/job_specifications/1486591411.png', 5, 111, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(5) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 - active, 0 - disable',
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `middle_initial` varchar(5) DEFAULT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - male, 1 - female',
  `company` varchar(150) NOT NULL,
  `department` varchar(100) NOT NULL,
  `job_title` varchar(200) DEFAULT NULL,
  `birthdate` datetime NOT NULL,
  `mobile_num` varchar(30) NOT NULL,
  `password` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  `doc_id_card` varchar(100) NOT NULL,
  `doc_gcp_card` varchar(100) NOT NULL,
  `doc_diploma` varchar(100) NOT NULL,
  `doc_degree` varchar(100) NOT NULL,
  `doc_other` varchar(100) NOT NULL,
  `is_admin` tinyint(1) NOT NULL COMMENT '0 - non admin, 1 - admin',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=102 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `is_active`, `fname`, `lname`, `middle_initial`, `gender`, `company`, `department`, `job_title`, `birthdate`, `mobile_num`, `password`, `email`, `doc_id_card`, `doc_gcp_card`, `doc_diploma`, `doc_degree`, `doc_other`, `is_admin`, `date_created`, `date_updated`) VALUES
(1, 1, 'Van', 'Vanleor', '', 0, '', '', NULL, '1995-04-06 00:00:00', '12345', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'van@admin.com', '', '', '', '', '', 1, '2016-11-03 21:34:02', '2016-11-10 01:53:52'),
(2, 1, 'Enzo', 'Lorenzo', '', 0, '', '', NULL, '0000-00-00 00:00:00', '123', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'enzo@mail.com', '', '', '', '', '', 0, '2016-11-05 01:40:51', '2016-12-14 01:44:32'),
(48, 1, 'TestPI', 'TESTVAN', NULL, 0, '', '', NULL, '2016-11-09 00:00:00', '12345PI', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'PI@admin.com', '', '', '', '', '', 0, '2016-11-19 04:50:26', '2016-11-21 08:01:12'),
(49, 1, 'TestTFO', 'TESTVAN', NULL, 0, '', '', NULL, '2016-11-11 00:00:00', '12345TFO', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'TFO@admin.com', '', '', '', '', '', 0, '2016-11-19 04:52:37', '2016-12-02 18:37:46'),
(50, 1, 'TestECC', 'TESTVAN', NULL, 0, 'company', '', NULL, '2016-10-31 00:00:00', '12345ECC', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'ECC@admin.com', '', '', '', '', '', 0, '2016-11-21 08:02:05', '2016-12-02 04:07:59'),
(51, 1, 'TestPRE', 'TESTVAN', NULL, 0, 'company', '', NULL, '2016-11-15 00:00:00', '12345PRE', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'PRE@admin.com', '', '', '', '', '', 0, '2016-11-21 08:28:41', '2016-12-02 04:07:50'),
(52, 1, 'TestI', 'TESTVAN', NULL, 0, '', '', NULL, '2016-11-22 00:00:00', '12345I', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'I@admin.com', '', '', '', '', '', 0, '2016-11-21 08:33:41', '2016-11-21 10:30:31'),
(55, 1, 'TestECM', 'TESTVAN', NULL, 0, 'comp', '', NULL, '2016-11-15 00:00:00', '12345ECM', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'ECM@admin.com', '', '', '', '', '', 0, '2016-11-21 10:34:06', '2016-12-02 04:08:07'),
(56, 1, 'Clinician', 'One', NULL, 0, '', 'Dept 1', 'jobbb 4', '2016-11-22 00:00:00', '12345C', 'd033e22ae348aeb5660fc2140aec35850c4da997', '12345C@gmail.com', '', '', '', '', '', 0, '2016-11-29 07:25:49', '2016-12-04 07:28:43'),
(57, 1, 'Research nurse', 'One', NULL, 0, '', 'Dept 2', 'jobbb 3', '2016-11-22 00:00:00', '12345RN', 'd033e22ae348aeb5660fc2140aec35850c4da997', '12345N@gmail.com', '', '', '', '', '', 0, '2016-11-29 07:26:41', '2016-12-04 07:28:40'),
(58, 1, 'Medicine controller', 'One', NULL, 0, '', 'Dept 2', 'job 2', '2016-11-16 00:00:00', '12345MC', 'd033e22ae348aeb5660fc2140aec35850c4da997', '12345MC@gmail.com', '', '', '', '', '', 0, '2016-11-29 07:27:26', '2016-12-04 07:28:36'),
(59, 1, 'Biological controller', 'One', NULL, 0, 'Companys', 'Dept 1', 'title job 1', '2016-11-16 00:00:00', '12345BGC', 'd033e22ae348aeb5660fc2140aec35850c4da997', '12345BC@gmail.com', '', '', '', '', '', 0, '2016-11-29 07:27:59', '2016-12-14 01:44:27'),
(61, 0, 'testssss', 'test', NULL, 1, 'cmp', '', NULL, '0000-00-00 00:00:00', '123123', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'mai@mail.comss', '', '', '', '', '', 0, '2016-12-02 03:49:48', '2016-12-02 04:01:07'),
(62, 1, 'TestUser', 'AllPosition', NULL, 1, 'QA!', 'QA!', NULL, '0000-00-00 00:00:00', '12345ALL', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'all@test.com', '', '', '', '', '', 0, '2016-12-02 18:12:44', '2016-12-04 07:49:20'),
(63, 1, 'TestECS', 'TESTVAN', NULL, 0, 'company', '', NULL, '2016-10-31 00:00:00', '12345ECS', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'ECS@admin.com', '', '', '', '', '', 0, '2016-11-21 08:02:05', '2016-12-02 04:07:59'),
(64, 1, 'TestDA', 'TESTVAN', NULL, 0, 'company', 'depart 1', 'Data Admin', '2016-10-31 00:00:00', '12345DA', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'DA@admin.com', '', '', '', '', '', 0, '2016-11-21 08:02:05', '2016-12-02 04:07:59'),
(65, 1, 'ç”³åŠžè€…1', 'æŽæ˜Ž', NULL, 0, 'ä¸­å±±å¤§å­¦', '', NULL, '0000-00-00 00:00:00', 'ç”³åŠžè€…1', 'd033e22ae348aeb5660fc2140aec35850c4da997', '475673@qq.com', 'http://betaprojex.com/Clinical2/uploads/users/id_14841217161484121716.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14841217161484121716.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14841217161484121716.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14841217161484121716.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14841217161484121716.png', 0, '2017-01-11 08:01:56', '0000-00-00 00:00:00'),
(73, 1, 'æŽ', 'ä¸»ä»»', NULL, 0, 'ä¸­å±±å¤§å­¦', '', NULL, '0000-00-00 00:00:00', 'æŽä¸»ä»»PI', 'd033e22ae348aeb5660fc2140aec35850c4da997', '123', 'http://betaprojex.com/Clinical2/uploads/users/id_14841866791484186679.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14841866791484186679.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14841866791484186679.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14841866791484186679.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14841866791484186679.png', 0, '2017-01-12 02:04:39', '0000-00-00 00:00:00'),
(74, 1, 'å¼ ', 'ä¸‰', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'å¼ ä¸‰TFO', 'd033e22ae348aeb5660fc2140aec35850c4da997', '12345', 'http://betaprojex.com/Clinical2/uploads/users/id_14841873531484187353.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14841873531484187353.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14841873531484187353.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14841873531484187353.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14841873531484187353.png', 0, '2017-01-12 02:15:53', '0000-00-00 00:00:00'),
(75, 1, 'æŽ', 'æ˜Ž', NULL, 0, 'åŒ»é™¢', '', NULL, '0000-00-00 00:00:00', '1234567', '3d7df60b781bbe23fcc7e5b60c7adbadd0675e19', '12336452', 'http://betaprojex.com/Clinical2/uploads/users/id_14845686211484568621.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14845686211484568621.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14845686211484568621.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14845686211484568621.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14845686211484568621.png', 0, '2017-01-16 12:10:21', '0000-00-00 00:00:00'),
(76, 0, 'ä¸»è¦ç ”ç©¶è€…', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ä¸»è¦ç ”ç©¶è€…1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'zhuyaoyanjiuzhe', 'http://betaprojex.com/Clinical2/uploads/users/id_14847355521484735552.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847355521484735552.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847355521484735552.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847355521484735552.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847355521484735552.png', 0, '2017-01-18 10:32:32', '0000-00-00 00:00:00'),
(78, 1, 'ç ”ç©¶è€…', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ç ”ç©¶è€…1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'yanjiuzhe', 'http://betaprojex.com/Clinical2/uploads/users/id_14847356381484735638.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847356381484735638.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847356381484735638.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847356381484735638.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847356381484735638.png', 0, '2017-01-18 10:33:58', '0000-00-00 00:00:00'),
(79, 1, 'ç”Ÿç‰©æ ·æœ¬ç®¡ç†å‘˜', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ç”Ÿç‰©æ ·æœ¬ç®¡ç†å‘˜1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'shengwuyangbenguanliyuan', 'http://betaprojex.com/Clinical2/uploads/users/id_14847357291484735729.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847357291484735729.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847357291484735729.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847357291484735729.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847357291484735729.png', 0, '2017-01-18 10:35:29', '0000-00-00 00:00:00'),
(80, 1, 'è¯å“ç®¡ç†å‘˜', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'è¯å“ç®¡ç†å‘˜1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'yaopinguanliyuan', 'http://betaprojex.com/Clinical2/uploads/users/id_14847358121484735812.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847358121484735812.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847358121484735812.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847358121484735812.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847358121484735812.png', 0, '2017-01-18 10:36:52', '0000-00-00 00:00:00'),
(81, 1, 'ä¸´åºŠåŒ»ç”Ÿ', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ä¸´åºŠåŒ»ç”Ÿ1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'linchuangyisheng', 'http://betaprojex.com/Clinical2/uploads/users/id_14847384991484738499.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847384991484738499.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847384991484738499.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847384991484738499.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847384991484738499.png', 0, '2017-01-18 11:21:39', '0000-00-00 00:00:00'),
(82, 1, 'ç ”ç©¶æŠ¤å£«', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ç ”ç©¶æŠ¤å£«1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'yanjiuhushi', 'http://betaprojex.com/Clinical2/uploads/users/id_14847385611484738561.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847385611484738561.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847385611484738561.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847385611484738561.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847385611484738561.png', 0, '2017-01-18 11:22:41', '0000-00-00 00:00:00'),
(84, 1, 'åŒè¡Œè¯„è®®ä¸“å®¶', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'åŒè¡Œè¯„è®®ä¸“å®¶1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'tonghangpingyizhuanjia', 'http://betaprojex.com/Clinical2/uploads/users/id_14847386541484738654.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847386541484738654.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847386541484738654.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847386541484738654.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847386541484738654.png', 0, '2017-01-18 11:24:14', '0000-00-00 00:00:00'),
(85, 1, 'ä¼¦ç†å§”å‘˜ä¼šç§˜ä¹¦', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ä¼¦ç†å§”å‘˜ä¼šç§˜ä¹¦1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'lunliweiyuanhuimishu', 'http://betaprojex.com/Clinical2/uploads/users/id_14847387351484738735.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847387351484738735.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847387351484738735.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847387351484738735.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847387351484738735.png', 0, '2017-01-18 11:25:35', '0000-00-00 00:00:00'),
(86, 1, 'ä¼¦ç†å§”å‘˜ä¼šä¸»ä»»', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ä¼¦ç†å§”å‘˜ä¼šä¸»ä»»1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'lunliweiyuanhuizhuren', 'http://betaprojex.com/Clinical2/uploads/users/id_14847388081484738808.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847388081484738808.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847388081484738808.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847388081484738808.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847388081484738808.png', 0, '2017-01-18 11:26:48', '0000-00-00 00:00:00'),
(87, 1, 'ä¼¦ç†å§”å‘˜ä¼šå§”å‘˜', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'ä¼¦ç†å§”å‘˜ä¼šå§”å‘˜1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'lunliweiyuanhuiweiyuan', 'http://betaprojex.com/Clinical2/uploads/users/id_14847388821484738882.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847388821484738882.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847388821484738882.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847388821484738882.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847388821484738882.png', 0, '2017-01-18 11:28:02', '0000-00-00 00:00:00'),
(88, 1, 'è¯•éªŒæœºæž„', '1', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', 'è¯•éªŒæœºæž„1', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'shiyanjigou', 'http://betaprojex.com/Clinical2/uploads/users/id_14847389551484738955.png', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14847389551484738955.png', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14847389551484738955.png', 'http://betaprojex.com/Clinical2/uploads/users/degree_14847389551484738955.png', 'http://betaprojex.com/Clinical2/uploads/users/other_14847389551484738955.png', 0, '2017-01-18 11:29:15', '0000-00-00 00:00:00'),
(89, 1, 'q', '', NULL, 0, 'q', '', NULL, '0000-00-00 00:00:00', 'q', '22ea1c649c82946aa6e479e1ffd321e4a318b1b0', 'q', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 0, '2017-01-19 06:28:09', '0000-00-00 00:00:00'),
(90, 1, 'w', '', NULL, 0, 'w', '', NULL, '0000-00-00 00:00:00', 'w', 'aff024fe4ab0fece4091de044c58c9ae4233383a', 'w', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 0, '2017-01-19 06:28:26', '0000-00-00 00:00:00'),
(91, 1, 'zy', '', NULL, 0, 'zy', '', NULL, '0000-00-00 00:00:00', 'zy', 'dcccef1c61b9892f75822537bc6ad84a598af2e9', 'zy', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 'http://betaprojex.com/Clinical2/uploads/users/', 0, '2017-01-19 06:34:26', '0000-00-00 00:00:00'),
(92, 0, 'test', '', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', '1', '356a192b7913b04c54574d18c28d46e6395428ab', '1', 'http://betaprojex.com/Clinical2/uploads/users/id_14848123721484812372.pdf', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14848123721484812372.pdf', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14848123721484812372.pdf', 'http://betaprojex.com/Clinical2/uploads/users/degree_14848123721484812372.pdf', 'http://betaprojex.com/Clinical2/uploads/users/other_14848123721484812372.pdf', 0, '2017-01-19 07:52:52', '0000-00-00 00:00:00'),
(93, 1, 'a', '', NULL, 0, '', '', NULL, '0000-00-00 00:00:00', '2', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8', '2', 'http://betaprojex.com/Clinical2/uploads/users/id_14848124571484812457.pdf', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14848124571484812457.pdf', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14848124571484812457.pdf', 'http://betaprojex.com/Clinical2/uploads/users/degree_14848124571484812457.pdf', 'http://betaprojex.com/Clinical2/uploads/users/other_14848124571484812457.pdf', 0, '2017-01-19 07:54:17', '0000-00-00 00:00:00'),
(94, 1, '9', '', NULL, 0, '9', '', NULL, '0000-00-00 00:00:00', '9', '0ade7c2cf97f75d009975f4d720d1fa6c19f4897', '9', 'http://betaprojex.com/Clinical2/uploads/users/id_14848151181484815118.pdf', 'http://betaprojex.com/Clinical2/uploads/users/gcp_14848151181484815118.pdf', 'http://betaprojex.com/Clinical2/uploads/users/diploma_14848151181484815118.pdf', 'http://betaprojex.com/Clinical2/uploads/users/degree_14848151181484815118.pdf', 'http://betaprojex.com/Clinical2/uploads/users/other_14848151181484815118.pdf', 0, '2017-01-19 08:38:38', '0000-00-00 00:00:00'),
(95, 1, 'Refresher', '', NULL, 0, 'Refresher', 'Refresher', NULL, '0000-00-00 00:00:00', '12345REF', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Refresher@Refresher.com', 'http://localhost/clinical/uploads/users/id_14864110451486411045.png', 'http://localhost/clinical/uploads/users/gcp_14864110451486411045.png', 'http://localhost/clinical/uploads/users/diploma_14864110451486411045.png', 'http://localhost/clinical/uploads/users/degree_14864110451486411045.png', 'http://localhost/clinical/uploads/users/other_14864110451486411045.png', 0, '2017-02-06 19:57:25', '0000-00-00 00:00:00'),
(98, 1, 'refffffff', '', NULL, 0, 'asd', 'asdewq', NULL, '0000-00-00 00:00:00', 'assss', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'sssasd', 'http://localhost/clinical/uploads/users/id_14864255591486425559.png', 'http://localhost/clinical/uploads/users/gcp_14864255591486425559.png', 'http://localhost/clinical/uploads/users/diploma_14864255591486425559.png', 'http://localhost/clinical/uploads/users/degree_14864255591486425559.png', 'http://localhost/clinical/uploads/users/other_14864255591486425559.png', 0, '2017-02-06 23:59:19', '0000-00-00 00:00:00'),
(99, 1, 'refssereffsers', '', NULL, 0, 'asd', 'asd', NULL, '0000-00-00 00:00:00', 'asd', 'b4914600112ba18af7798b6c1a1363728ae1d96f', 'asd', 'http://localhost/clinical/uploads/users/id_14864258671486425867.png', 'http://localhost/clinical/uploads/users/gcp_14864258671486425867.png', 'http://localhost/clinical/uploads/users/diploma_14864258671486425867.png', 'http://localhost/clinical/uploads/users/degree_14864258671486425867.png', 'http://localhost/clinical/uploads/users/other_14864258671486425867.png', 0, '2017-02-07 00:04:27', '0000-00-00 00:00:00'),
(101, 1, '123334444444', '', NULL, 0, '12332312312', '123', NULL, '0000-00-00 00:00:00', '4', 'd033e22ae348aeb5660fc2140aec35850c4da997', '4', 'http://localhost/clinical/uploads/users/id_14864268621486426862.png', 'http://localhost/clinical/uploads/users/gcp_14864268621486426862.png', 'http://localhost/clinical/uploads/users/diploma_14864268621486426862.png', 'http://localhost/clinical/uploads/users/degree_14864268621486426862.png', 'http://localhost/clinical/uploads/users/other_14864268621486426862.png', 0, '2017-02-07 00:21:02', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `volunteers`
--

CREATE TABLE IF NOT EXISTS `volunteers` (
`volunteer_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `v_name` varchar(150) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `bday` date NOT NULL,
  `nation` varchar(150) NOT NULL,
  `native` varchar(150) NOT NULL,
  `married` varchar(3) NOT NULL,
  `blood_type` varchar(20) NOT NULL,
  `surgery` varchar(3) NOT NULL,
  `fam_history` text NOT NULL,
  `smoker` varchar(3) NOT NULL,
  `drinker` varchar(3) NOT NULL,
  `allergies` text NOT NULL,
  `height` varchar(15) NOT NULL,
  `weight` varchar(15) NOT NULL,
  `bmi` varchar(50) NOT NULL,
  `med_history` text NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `remarks` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '1 - is currently occupied as subject, 0 - available as subject',
  `date_status` date NOT NULL,
  `status_updated_by` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_update` date NOT NULL,
  `updated_by` int(11) NOT NULL,
  `id_card_num` varchar(255) NOT NULL,
  `id_card_path` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `volunteers`
--

INSERT INTO `volunteers` (`volunteer_id`, `subject_id`, `v_name`, `gender`, `bday`, `nation`, `native`, `married`, `blood_type`, `surgery`, `fam_history`, `smoker`, `drinker`, `allergies`, `height`, `weight`, `bmi`, `med_history`, `mobile`, `remarks`, `status`, `date_status`, `status_updated_by`, `date_added`, `added_by`, `date_update`, `updated_by`, `id_card_num`, `id_card_path`) VALUES
(1, 1, 'adf', 'Male', '2016-12-22', 'adf', 'adf', 'Yes', 'adf', 'Yes', 'adf', 'Yes', 'Yes', 'adf', '123', '4', '23', 'adf1', 'adf', '', 1, '0000-00-00', 0, '2016-12-22', 9, '0000-00-00', 0, 'adf', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482401097.docx'),
(2, 2, '123', 'Male', '2016-12-23', '123', '123', 'No', '123', 'No', '123', 'No', 'No', '123', '123', '123', '123', '123', '123', '', 1, '0000-00-00', 0, '2016-12-23', 9, '0000-00-00', 0, '123', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482481227.png'),
(3, 3, 'åœ°æ–¹', 'Male', '2016-12-02', 'é˜¿é“å¤«a', 'ADf', 'Yes', 'ADf', 'Yes', 'é˜¿é“å¤«', 'Yes', 'Yes', 'é˜¿é“å¤«', '123', '123', '124', 'é˜¿é“å¤«', '3434', '', 1, '0000-00-00', 0, '2016-12-23', 9, '0000-00-00', 0, '122334', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482485087.docx'),
(4, 4, 'adf', 'Male', '2016-12-06', 'adf', 'adf', 'Yes', 'adf', 'Yes', 'df', 'Yes', 'Yes', 'adf', '12231', '12', '23', 'adf1', '1213', '', 1, '0000-00-00', 0, '2016-12-25', 9, '0000-00-00', 0, '1243', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482675300.docx'),
(5, 5, 'klajdf', 'Male', '2016-12-15', 'adf', 'adf', 'Yes', 'adf', 'Yes', 'sdf', 'Yes', 'Yes', 'adf', '12', '12', '12', '12', '12', '', 1, '0000-00-00', 0, '2016-12-27', 9, '0000-00-00', 0, '12', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482806191.png'),
(6, 6, 'test', 'Male', '2016-12-01', 'aa', 'a', 'Yes', 'a', 'Yes', 'a', 'Yes', 'Yes', 'a', 'a', 'a', 'a', 'a', 'a', '', 1, '0000-00-00', 0, '2016-12-27', 9, '0000-00-00', 0, 'a', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482810873.txt'),
(7, 8, 'a', 'Male', '2016-12-28', 'a', 'a', 'Yes', 'a', 'Yes', 'a', 'Yes', 'Yes', 'a', 'a', 'a', 'NaN', 'a', 'a', '', 1, '0000-00-00', 0, '2016-12-28', 9, '0000-00-00', 0, 'a', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482909682.txt'),
(8, 7, 'b', 'Female', '2016-12-28', 'b', 'b', 'No', 'b', 'No', 'b', 'No', 'No', 'b', 'b', 'b', 'NaN', 'b', 'b', '', 1, '0000-00-00', 0, '2016-12-28', 9, '0000-00-00', 0, 'b', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482909715.txt'),
(9, 9, 'c', 'Male', '2016-12-28', 'c', 'c', 'Yes', 'c', 'Yes', 'c', 'Yes', 'Yes', 'c', 'c', 'c', 'NaN', 'c', 'c', '', 1, '0000-00-00', 0, '2016-12-28', 9, '0000-00-00', 0, 'c', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482911037.txt'),
(10, 10, 'D', 'Female', '2016-12-28', 'D', 'D', 'No', 'D', 'No', 'D', 'No', 'No', 'D', 'D', 'D', 'NaN', 'D', 'D', '', 1, '0000-00-00', 0, '2016-12-28', 9, '0000-00-00', 0, 'D', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1482912206.txt'),
(11, 11, 'é˜¿é“å¤«', 'Male', '2016-12-14', 'é˜¿é“å¤«a', 'aåœ°æ–¹', 'Yes', 'å•Š', 'Yes', 'æ‰“åˆ†', 'Yes', 'Yes', 'é˜¿é“å¤«', '170', '56', '19.38', 'åœ°æ–¹', '1244', '', 1, '0000-00-00', 0, '2016-12-30', 9, '0000-00-00', 0, '1245', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1483077328.docx'),
(12, 12, '1', 'Female', '2016-12-30', '1', '1', 'Yes', '1', 'Yes', '1', 'Yes', 'Yes', '1', '1', '1', '10000.00', '1', '11', '', 1, '0000-00-00', 0, '2016-12-30', 9, '0000-00-00', 0, '1', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1483085973.jpg'),
(13, 13, 'å¼ ä¸‰', 'Male', '2016-12-30', '1', '1', 'Yes', '11', 'Yes', '1', 'Yes', 'Yes', '1', '1', '1', '10000.00', '1', '1', '', 1, '0000-00-00', 0, '2016-12-30', 9, '0000-00-00', 0, '1', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1483087072.jpg'),
(14, 14, 'æŽå››', 'Female', '2016-12-30', '11', '1', 'Yes', '1', 'Yes', '1', 'Yes', 'Yes', '1', '1', '1', '10000.00', '1', '11', '', 1, '0000-00-00', 0, '2016-12-30', 9, '0000-00-00', 0, '1', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1483087512.jpg'),
(15, 18, 'liming', 'Male', '2016-12-28', 'han', 'guangdong', 'Yes', 'O', 'Yes', 'dh', 'Yes', 'Yes', 'han', '180', '56', '17.28', 'ganbing', '1234566', '', 1, '0000-00-00', 0, '2017-01-10', 9, '0000-00-00', 0, '346776554', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1484014877.png'),
(16, 17, 'æŽä¸–æ©', 'Female', '1976-04-13', 'æ±‰', 'å¹¿ä¸œ', 'Yes', 'A', 'No', 'æ— ', 'No', 'No', 'æ— ', '162', '53', '20.20', 'æ— ', '13625291913', '', 1, '0000-00-00', 0, '2017-01-10', 9, '0000-00-00', 0, '441522197604130423', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1484044143.jpg'),
(17, 16, 'å¼ ä¸–è£', 'Male', '1965-04-12', 'æ±‰', 'å±±è¥¿', 'Yes', 'B', 'No', 'æ— ', 'Yes', 'Yes', 'æ— ', '175', '65', '21.22', 'æ— ', '13765281627', '', 1, '0000-00-00', 0, '2017-01-10', 9, '0000-00-00', 0, '441826196512042816', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1484044241.jpg'),
(18, 19, 'adf', 'Female', '2017-01-04', 'adf', 'adf', 'Yes', 'df', 'Yes', 'adf', 'Yes', 'Yes', 'adf', '23', '123', '2325.14', 'adf1', '2343431', '', 0, '0000-00-00', 0, '2017-01-18', 9, '0000-00-00', 0, '214343434', 'http://betaprojex.com/Clinical2/uploads/volunteer_id_cards/1484731016.png');

-- --------------------------------------------------------

--
-- Structure for view `projects x status`
--
DROP TABLE IF EXISTS `projects x status`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `projects x status` AS select `projects`.`project_id` AS `project_id`,`projects`.`project_name` AS `project_name`,`status`.`status_num` AS `status_num`,`status`.`is_rejected` AS `is_rejected` from (`projects` join `status`) where (`projects`.`status_id` = `status`.`status_id`);

-- --------------------------------------------------------

--
-- Structure for view `roles x users`
--
DROP TABLE IF EXISTS `roles x users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `roles x users` AS select `users`.`user_id` AS `user_id`,`roles`.`role_id` AS `role_id`,`users`.`fname` AS `fname` from (`users` join `roles`) where (`users`.`user_id` = `roles`.`user_id`);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bio_destroy`
--
ALTER TABLE `bio_destroy`
 ADD PRIMARY KEY (`bio_destroy_id`);

--
-- Indexes for table `bio_determine`
--
ALTER TABLE `bio_determine`
 ADD PRIMARY KEY (`determine_id`);

--
-- Indexes for table `bio_handles`
--
ALTER TABLE `bio_handles`
 ADD PRIMARY KEY (`bio_handle_id`);

--
-- Indexes for table `bio_recycle`
--
ALTER TABLE `bio_recycle`
 ADD PRIMARY KEY (`bio_recycle_id`);

--
-- Indexes for table `bio_samples`
--
ALTER TABLE `bio_samples`
 ADD PRIMARY KEY (`bio_id`);

--
-- Indexes for table `bio_save`
--
ALTER TABLE `bio_save`
 ADD PRIMARY KEY (`bio_save_id`);

--
-- Indexes for table `bio_transport`
--
ALTER TABLE `bio_transport`
 ADD PRIMARY KEY (`transport_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
 ADD PRIMARY KEY (`dept_id`);

--
-- Indexes for table `drugs`
--
ALTER TABLE `drugs`
 ADD PRIMARY KEY (`drug_id`);

--
-- Indexes for table `drug_records`
--
ALTER TABLE `drug_records`
 ADD PRIMARY KEY (`drug_record_id`);

--
-- Indexes for table `drug_record_receipts`
--
ALTER TABLE `drug_record_receipts`
 ADD PRIMARY KEY (`receipt_id`);

--
-- Indexes for table `inspections`
--
ALTER TABLE `inspections`
 ADD PRIMARY KEY (`inspection_id`);

--
-- Indexes for table `inspection_uploads`
--
ALTER TABLE `inspection_uploads`
 ADD PRIMARY KEY (`inspection_upload_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
 ADD PRIMARY KEY (`word_id`), ADD UNIQUE KEY `phrase` (`phrase`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
 ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `mc_info`
--
ALTER TABLE `mc_info`
 ADD PRIMARY KEY (`mc_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
 ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
 ADD PRIMARY KEY (`position_id`);

--
-- Indexes for table `professional_group`
--
ALTER TABLE `professional_group`
 ADD PRIMARY KEY (`pgroup_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
 ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `projects_pre`
--
ALTER TABLE `projects_pre`
 ADD PRIMARY KEY (`pre_id`);

--
-- Indexes for table `project_remarks`
--
ALTER TABLE `project_remarks`
 ADD PRIMARY KEY (`remarks_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
 ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `side_effect`
--
ALTER TABLE `side_effect`
 ADD PRIMARY KEY (`side_effect_id`);

--
-- Indexes for table `side_effects_docs`
--
ALTER TABLE `side_effects_docs`
 ADD PRIMARY KEY (`side_effect_doc_id`);

--
-- Indexes for table `side_effect_receipts`
--
ALTER TABLE `side_effect_receipts`
 ADD PRIMARY KEY (`receipt_id`);

--
-- Indexes for table `stage_summary`
--
ALTER TABLE `stage_summary`
 ADD PRIMARY KEY (`stage_summary_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
 ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
 ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `subject_status`
--
ALTER TABLE `subject_status`
 ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `summary_files`
--
ALTER TABLE `summary_files`
 ADD PRIMARY KEY (`summary_file_id`);

--
-- Indexes for table `summary_materials`
--
ALTER TABLE `summary_materials`
 ADD PRIMARY KEY (`material_id`);

--
-- Indexes for table `trainings`
--
ALTER TABLE `trainings`
 ADD PRIMARY KEY (`training_id`);

--
-- Indexes for table `trainings_participant`
--
ALTER TABLE `trainings_participant`
 ADD PRIMARY KEY (`participant_id`);

--
-- Indexes for table `training_material`
--
ALTER TABLE `training_material`
 ADD PRIMARY KEY (`material_id`);

--
-- Indexes for table `trials`
--
ALTER TABLE `trials`
 ADD PRIMARY KEY (`trial_id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
 ADD PRIMARY KEY (`up_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `mobile_num` (`mobile_num`);

--
-- Indexes for table `volunteers`
--
ALTER TABLE `volunteers`
 ADD PRIMARY KEY (`volunteer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bio_destroy`
--
ALTER TABLE `bio_destroy`
MODIFY `bio_destroy_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `bio_determine`
--
ALTER TABLE `bio_determine`
MODIFY `determine_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bio_handles`
--
ALTER TABLE `bio_handles`
MODIFY `bio_handle_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `bio_recycle`
--
ALTER TABLE `bio_recycle`
MODIFY `bio_recycle_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `bio_samples`
--
ALTER TABLE `bio_samples`
MODIFY `bio_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `bio_save`
--
ALTER TABLE `bio_save`
MODIFY `bio_save_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `bio_transport`
--
ALTER TABLE `bio_transport`
MODIFY `transport_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
MODIFY `dept_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `drugs`
--
ALTER TABLE `drugs`
MODIFY `drug_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `drug_records`
--
ALTER TABLE `drug_records`
MODIFY `drug_record_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `drug_record_receipts`
--
ALTER TABLE `drug_record_receipts`
MODIFY `receipt_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `inspections`
--
ALTER TABLE `inspections`
MODIFY `inspection_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `inspection_uploads`
--
ALTER TABLE `inspection_uploads`
MODIFY `inspection_upload_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
MODIFY `word_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=766;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=399;
--
-- AUTO_INCREMENT for table `mc_info`
--
ALTER TABLE `mc_info`
MODIFY `mc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=420;
--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
MODIFY `position_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `professional_group`
--
ALTER TABLE `professional_group`
MODIFY `pgroup_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT for table `projects_pre`
--
ALTER TABLE `projects_pre`
MODIFY `pre_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `project_remarks`
--
ALTER TABLE `project_remarks`
MODIFY `remarks_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `side_effect`
--
ALTER TABLE `side_effect`
MODIFY `side_effect_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `side_effects_docs`
--
ALTER TABLE `side_effects_docs`
MODIFY `side_effect_doc_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `side_effect_receipts`
--
ALTER TABLE `side_effect_receipts`
MODIFY `receipt_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stage_summary`
--
ALTER TABLE `stage_summary`
MODIFY `stage_summary_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
MODIFY `status_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=276;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `subject_status`
--
ALTER TABLE `subject_status`
MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `summary_files`
--
ALTER TABLE `summary_files`
MODIFY `summary_file_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `summary_materials`
--
ALTER TABLE `summary_materials`
MODIFY `material_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `trainings`
--
ALTER TABLE `trainings`
MODIFY `training_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `trainings_participant`
--
ALTER TABLE `trainings_participant`
MODIFY `participant_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `training_material`
--
ALTER TABLE `training_material`
MODIFY `material_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `trials`
--
ALTER TABLE `trials`
MODIFY `trial_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
MODIFY `up_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=166;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `volunteers`
--
ALTER TABLE `volunteers`
MODIFY `volunteer_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
