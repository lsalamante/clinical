<!-- Add Side Effect per Summary per Project @Rai-->

<?php require("php-functions/fncStageSummary.php"); ?>

<?php if(isset($_GET["project_id"]) && $_GET["project_id"] != "" && $_SESSION["user_type"] == "I" ){ ?>

  <a href="#" data-toggle="modal" class="btn btn-primary adder" id="addSummary"><?php echo $phrases['add']?></a>

  <?php } ?>


  <!-- </style> -->

  <?php

  $project_id = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? $_GET["project_id"] : 0;
  $stageSummary = getStageSummary("list", $project_id);


  $count = 0;

  if(count($stageSummary) > 0 && $project_id != 0){ // check if found record/s and if id is provided.
    ?>

    <table class="table table-hover">
      <thead>
        <tr>
          <th><?php echo $phrases['number']?></th>
          <th><?php echo $phrases['time_slot']?></th>
          <th><?php echo $phrases['enroll_amount']?></th>
          <th><?php echo $phrases['quit_amount']?></th>
          <th><?php echo $phrases['finish_amount']?></th>
          <!-- <th><?php // echo $phrases['serious_adverse_reactions']?></th> -->
          <th><?php echo $t->tryTranslate('sae_amount'); ?></th>

        </tr>
      </thead>

      <tbody id="pp_body">
        <?php

        $count = 0;

        foreach ($stageSummary as $summary) {

          $count++;

          ?>
          <tr onclick="getStageSummary(<?php echo $summary['stage_summary_id']; ?>); return false;" style="cursor:pointer;">
            <td><?php echo $count ?></td>
            <td><?php echo $summary["time_slot_from"]." - ".$summary["time_slot_to"]; ?></td>
            <td><?php echo $summary["enroll_amount"] ?></td>
            <td><?php echo $summary["quit_amount"] ?></td>
            <td><?php echo $summary["finish_amount"] ?></td>
            <!-- <td><?php // echo $summary["serious_adverse_reactions"] ?></td> -->
            <td><?php echo $summary["amount"] ?></td>
          </tr>

          <?php } ?>

        </tbody>
      </table>

      <?php }else{ // no record/s found ?>

        <div class="col-lg-12 text-center">
          <img src="img/sad.png" class="sad">
          <h1 class="sad"><?php echo $phrases['empty_table_data']?></h1>
        </div>

        <?php } ?>


        <!-- Start of Add Stage Summary Modal  @Rai-->

        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addSummaryModal" class="modal fade " data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title" id="modalTitleAddSummary"><?php echo $phrases["add_stage_summary"] ?></h4>
              </div>
              <div class="modal-body">

                <form action="#" id="addSummaryForm">

                  <div class="form-group" id="time_slot_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["time_slot"] ?></label>
                    <div class="row">
                      <div class="col-lg-5">
                        <div class="iconic-input right">
                          <i class="fa fa-calendar"></i>
                          <input type="text" class="form-control" id="time_slot_from" name="time_slot_from">
                        </div>
                      </div>
                      <div class="col-lg-1">
                        <center><?php echo $phrases['to_date']; ?></center>
                      </div>
                      <div class="col-lg-5">
                        <div class="iconic-input right">
                          <i class="fa fa-calendar"></i>
                          <input type="text" class="form-control" id="time_slot_to" name="time_slot_to" placeholder="">
                        </div>
                      </div>
                    </div>
                    <p class="help-block" style="display:none;" id="time_slot_msg"></p>
                  </div>

                  <div class="form-group" id="enroll_amount_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["enroll_amount"] ?></label>
                    <input type="text" class="form-control" id="enroll_amount" name="enroll_amount"  placeholder="">
                    <p class="help-block" style="display:none;" id="enroll_amount_msg"></p>
                  </div>

                  <div class="form-group" id="quit_amount_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["quit_amount"] ?></label>
                    <input type="text" class="form-control" id="quit_amount" name="quit_amount"  placeholder="">
                    <p class="help-block" style="display:none;" id="quit_amount_msg"></p>
                  </div>

                  <div class="form-group" id="finish_amount_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["finish_amount"] ?></label>
                    <input type="text" class="form-control" id="finish_amount" name="finish_amount"  placeholder="">
                    <p class="help-block" style="display:none;" id="finish_amount_msg"></p>
                  </div>

                  <div class="form-group" id="light_effect_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["light_effect"] ?></label>
                    <input type="text" class="form-control" id="light_effect" name="light_effect" placeholder="">
                    <p class="help-block" style="display:none;" id="light_effect_msg"></p>
                  </div>

                  <div class="form-group" id="light_amount_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["amount"] ?></label>
                    <input type="text" class="form-control" id="light_amount" name="light_amount"  placeholder="">
                    <p class="help-block" style="display:none;" id="light_amount_msg"></p>
                  </div>

                  <div class="form-group" id="mid_effect_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["mid_effect"] ?></label>
                    <input type="text" class="form-control" id="mid_effect" name="mid_effect" placeholder="">
                    <p class="help-block" style="display:none;" id="mid_effect_msg"></p>
                  </div>

                  <div class="form-group" id="mid_amount_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["amount"] ?></label>
                    <input type="text" class="form-control" id="mid_amount" name="mid_amount"  placeholder="">
                    <p class="help-block" style="display:none;" id="mid_amount_msg"></p>
                  </div>

                  <div class="form-group" id="hard_effect_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["hard_effect"] ?></label>
                    <input type="text" class="form-control" id="hard_effect" name="hard_effect" placeholder="">
                    <p class="help-block" style="display:none;" id="hard_effect_msg"></p>
                  </div>

                  <div class="form-group" id="hard_amount_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["amount"] ?></label>
                    <input type="text" class="form-control" id="hard_amount" name="hard_amount"  placeholder="">
                    <p class="help-block" style="display:none;" id="hard_amount_msg"></p>
                  </div>

                  <div class="form-group" id="important_effect_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["important_effect"] ?></label>
                    <input type="text" class="form-control" id="important_effect" name="important_effect" placeholder="">
                    <p class="help-block" style="display:none;" id="important_effect_msg"></p>
                  </div>

                  <div class="form-group" id="important_amount_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["amount"] ?></label>
                    <input type="text" class="form-control" id="important_amount" name="important_amount"  placeholder="">
                    <p class="help-block" style="display:none;" id="important_amount_msg"></p>
                  </div>

                  <div class="form-group" id="degree_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["degree"] ?></label>
                    <input type="text" class="form-control" id="degree" name="degree" placeholder="">
                    <p class="help-block" style="display:none;" id="degree_msg"></p>
                  </div>

                  <div class="form-group" id="sae_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["sae_summary"] ?></label>
                    <input type="text" class="form-control" id="sae" name="sae" placeholder="">
                    <p class="help-block" style="display:none;" id="sae_msg"></p>
                  </div>

                  <div class="form-group" id="amount_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["amount"] ?></label>
                    <input type="text" class="form-control" id="amount" name="amount"  placeholder="">
                    <p class="help-block" style="display:none;" id="amount_msg"></p>
                  </div>

                  <div class="form-group" id="handling_info_div">
                    <label for="exampleInputEmail1"><?php echo $phrases["other_situation"] ?></label>
                    <input type="text" class="form-control" id="handling_info" name="handling_info" placeholder="">
                    <p class="help-block" style="display:none;" id="handling_info_msg"></p>
                  </div>

                  <div class="form-group" id="handling_info_div">
                    <h5><?php echo $phrases["upload_materials"] ?></h5>

                    <?php if( isset($_GET["project_id"]) && $_GET["project_id"] != "" && $_SESSION["user_type"] == "I" ){ // make way for training list ?>
                      <a href="#" data-toggle="modal" class="btn btn-primary adder" id="addUploadS"><?php echo $phrases['add']?></a>
                    <?php } ?>

                    <table class="table table-hover">
                      <thead>
                        <th><?php echo $phrases['number']?></th>
                        <th><?php echo $phrases['name']?></th>
                        <th><?php echo $phrases['version']?></th>
                        <th><?php echo $phrases['version_date']?></th>
                        <th><?php echo $phrases['description']; ?></th>
                        <th><?php echo $phrases['file']; ?></th>
                      </thead>
                      <tbody id="stageSummaryUpload">
                      </tbody>
                    </table>

                  </div>


                  <!-- Start hidden fields @Rai-->

                  <input type="hidden" name="stage_summary_id" id="stage_summary_id" value="">
                  <input type="hidden" name="project_idS" id="project_idS" value="<?php echo $_GET["project_id"]; ?>">

                  <!-- End hidden fields @Rai-->

                  <?php if($_SESSION["user_type"] == "I"){ ?>
                    <button type="button" class="btn btn-primary" id="submitSummary"><?php echo $phrases["submit"] ?></button>
                    <?php } ?>

                  </form>

                  <!-- BUTTON FOR NOTIFY FOR DA -->
                  <?php if($_SESSION["user_type"] == "DA" ): ?>
                    <button class="btn btn-primary" type="button" onclick="notifyOwner('<?php echo $_GET['project_id']?>', 'I', 'stage_summary')">
                      <?php echo $t->tryTranslate('notify')?>
                    </button>
                  <?php endif; ?>
                  <!-- / BUTTON FOR NOTIFY FOR DA -->

                </div>
              </div>
            </div>
            </div>

            <!-- End of Add Stage Summary Modal  @Rai-->

            <!-- add Upload -->

            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="summaryUploadModal" class="modal fade" data-backdrop="static" data-keyboard="false">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title"><?php echo $phrases['add']; ?></h4>
                  </div>
                  <div class="modal-body">

                    <form role="form" id="summaryUploadForm" enctype="multipart/form-data">
                      <div class="form-group" id="nameS_div">
                        <label for=""><?php echo $phrases['name'] ?></label>
                        <input type="text" class="form-control" name="name" id="nameS" placeholder="">
                        <p class="help-block" style="display:none;" id="nameS_msg"></p>
                      </div>
                      <div class="form-group" id="versionS_div">
                        <label for=""><?php echo $phrases['version'] ?></label>
                        <input type="number" min="0" step="0.1" class="form-control" name="version" id="versionS" placeholder="">
                        <p class="help-block" style="display:none;" id="versionS_msg"></p>
                      </div>
                      <div class="form-group" id="descriptionS_div">
                        <label for=""><?php echo $phrases['description'] ?></label>
                        <textarea class="form-control v-resize-only" name="description" id="descriptionS" placeholder=""></textarea>
                        <p class="help-block" style="display:none;" id="descriptionS_msg"></p>
                      </div>
                      <div class="form-group" id="up_fileS_div">
                        <label for="exampleInputFile"><?php echo $phrases['select_file']; ?></label>
                        <input type="file" name="up_file[]" id="up_fileS" multiple="" required>
                        <p class="help-block" style="display:none; color: red" id="up_fileS_msg"></p>
                      </div>

                      <div class="form-group">
                        <input type="checkbox" name="databank" value="1" id="stage_databank">
                        <label for="stage_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
                      </div>
                       <!-- Start hidden fields @Rai-->

                        <input type="hidden" name="count" id="countS" value="0">
                        <input type="hidden" name="count" id="countData" value="0">

                        <!-- End hidden fields @Rai-->

                      <?php if( isset($_GET["project_id"]) && $_GET["project_id"] != "" && $_SESSION["user_type"] == "I" ){ // make way for training list  ?>
                        <button type="button" class="btn btn-primary make-loading" id="submitMaterialS"><?php echo $phrases['submit']; ?></button>
                      <?php } ?>

                    </form>
                  </div>
                </div>
              </div>
            </div>

            <!-- / add Upload -->

            <!--  DOUBLE CHECK Stage summary
            -->
                      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="stagesummary_dbl_chk_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                              <h4 class="modal-title"><?php echo $t->tryTranslate('add_double_check_opinion') ?></h4>
                            </div>
                            <div class="modal-body">
                              <form role="form" id="stagesummary_dbl_chk_form">
                                <div class="form-group">
                                  <div class="form-group <?php echo ($_SESSION['user_type'] == 'DA') ? '' : 'hidden' ; ?>">
                                    <div class="radio">
                                      <input value="1" name="is_checked" id="stagesummary_dbl_chk_pass" type="radio" onClick="clearDblCheck('stagesummary_comment', this.value);"> <?php echo $t->tryTranslate('no_problem') ?> &nbsp;&nbsp;
                                      <input value="-1" name="is_checked" id="stagesummary_dbl_chk_nopass" type="radio" onClick="clearDblCheck('stagesummary_comment', this.value);"> <?php echo $t->tryTranslate('have_problem') ?> </div>
                                  </div>
                                  <label for="exampleInputEmail1" class="dbl_chk_label"><?php echo $t->tryTranslate('comments') ?></label>
                                  <textarea class="form-control v-resize-only" id="stagesummary_comment" name="comments" <?php echo ($_SESSION['user_type'] != 'DA') ? 'disabled' : '' ;?> placeholder=""></textarea>
                                  <input type='hidden' name="bio_id" id="stagesummary_dbl_chk_id" value="">  <!-- unique id of PK-->
                                  <input type='hidden' name="payload_key" id="stagesummary_payload_key" value="">  <!-- key of payload to modify -->
                                  <input type='hidden' name="table" id="stagesummary_dbl_chk_tbl" value="stage_summary"> <!-- table name to modify -->
                                  <input type='hidden' name="pk_col" id="stagesummary_pk_col" value="stage_summary_id">  <!-- primary key column name of table-->
                                </div>
                                <?php if($_SESSION['user_type'] == 'DA'):?>
                                <button type="" class="btn btn-primary make-loading" id="stagesummary_dbl_chk_submit"><?php echo $phrases['submit'] ?></button>
                              <?php endif; ?>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- / DOUBLE CHECK DRUGS  -->
