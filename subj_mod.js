
$("#random_date").datepicker();
$("#sign_date").datepicker();

function set_subject_id(volunteer_id, param_subject_id, the_pi){
  // if(status == 1)
  // $("#submitVolunteer").hide(); // hide submit button when volunteer is already approved

  $("#v_status").val(status);
  $("#modalTitle").html("<?php echo $phrases['edit'] . $phrases['subjects'];  ?>"); // initial Add Volunteer Modal Title
  $("#update_subject_id").val(param_subject_id);
  $("#the_pi").val(the_pi);

  $.ajax({
    url:"php-functions/fncVolunteers.php?action=getVolunteerById",
    type: "POST",
    dataType: "json",
    data: { "volunteer_id" : volunteer_id, "type" : "ajax" },
    success:function(data){
      $("#submitVolunteer").html('更新');
      $("#subjectModal").modal("show");

      $('#sv_name').val(data.v_name).attr('disabled', true);
      $('#sgender').val(data.gender).attr('disabled', true);;
      $('#sbday').val(data.bday).attr('disabled', true);
      $('#snation').val(data.nation).attr('disabled', true);
      $('#snative').val(data.native).attr('disabled', true);
      $('#sgender').val(data.gender).attr('disabled', true);
      $('.smarried').val([data.married]).attr('disabled', true);
      $('#sblood_type').val(data.blood_type).attr('disabled', true);
      $('.ssurgery').val([data.surgery]).attr('disabled', true);
      $('#sfam_history').val(data.fam_history).attr('disabled', true);
      $('#smed_history').val(data.med_history).attr('disabled', true);
      $('.ssmoker').val([data.smoker]).attr('disabled', true);
      $('.sdrinker').val([data.drinker]).attr('disabled', true);
      $('#sallergies').val(data.allergies).attr('disabled', true);
      $('#sheight').val(data.height).attr('disabled', true);
      $('#sweight').val(data.weight).attr('disabled', true);
      $('#sbmi').val(data.bmi).attr('disabled', true);
      $('#smobile').val(data.mobile).attr('disabled', true);
      $('#sid_card_num').val(data.id_card_num).attr('disabled', true);
      if(data.id_card_path != ''){
        $('#sid_card_copy_div').removeClass('hidden');
        $('#sid_card_copy_file').attr('href', data.id_card_path);
      }
    }
  });
  $.ajax({
    url:"php-functions/fncSubjects.php?actionS=getSubjectDetailsById",
    type: "POST",
    data: { "subject_id" : param_subject_id, "type" : "ajax" },
    success:function(data){
      data = JSON.parse(data);

      $('#case_num').val(data[0].case_num);
      $('#subjects_num').val(data[0].subjects_num);
      $('#abbrv').val(data[0].abbrv);
      $('#country').val(data[0].country);
      $('#address').val(data[0].address);
      if(data[0].is_informed_consent == 1){
        $('#iifc_yes').prop('checked', 'checked');
      }else{
        $('#iifc_no').prop('checked', 'checked');
      }
      $('#random_num').val(data[0].random_num);
      $('#random_date').val(data[0].random_date);
      $('#sign_date').val(data[0].sign_date);
    }
  });


}

$("#subjectsForm").on('submit', function(e){
  e.preventDefault();

  var form_data = $('#subjectsForm').serialize();

  $.ajax({
    url:"php-functions/fncSubjects.php?actionS=updateSubjectDetails",
    type: "POST",
    data: form_data,
    success:function(msg){
      msg = JSON.parse(msg);
      if(msg.type == 'success'){
        show_alert(msg.msg, msg.redirect, 1);
      }
      else{
        show_alert(msg.msg, "", 0);
      }
    }
  });
});
