<?php
include('jp_library/jp_lib.php');
require("php-functions/fncCommon.php");
require("php-functions/fncProjects.php");
require("php-functions/fncVolunteers.php");
require("php-functions/fncStatusLabel.php");

if (!isset($_SESSION['user_id'])) {
  header("Location: " . "index.php");
  die();
}

// ------------- Start get all projects assignment (fncCommon.php, fncProjects.php, fncStatusLabel.php) ------------- //
$projects =array();
foreach (getAllProjects(0) as $proj) {

	array_push($projects, $proj["project_id"]);

}
// ------------- End get all projects assignment ------------- //

?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<?php include('header.php'); ?>

<body>
  <section id="container">
    <!--header start-->
    <header class="header white-bg">
      <?php
      if ($LEFT_SIDEBAR) {
        #echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
      }
      ?>
      <!--logo start-->
      <?php if ($LOGO) {
        include('logo.php');
      }
      ?>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <?php if ($NOTIFICATION) {
          include('notification.php');
        } ?>
        <!--  notification end -->
      </div>
      <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
      include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <style>
    span.required
    {
      margin-left: 3px;
      color: red;
    }
    </style>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
          <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
              <li><a href="training-list"><i class="fa fa-home"></i> <?php echo $phrases['subject_library']; ?></a></li>
            </ul>
            <!--breadcrumbs end -->
          </div>
        </div>


        <section class="panel pad18">
          <?php include("subjects.php"); ?>
      	</section>
        <div class="hidden" id="hide-side-effect">
        <?php include("side-effect.php"); ?>
      </div>
        <?php include("side-effect-modal.php"); ?>

      <!-- page end-->

  </section>
  <!--main content end-->
  <!-- Right Slidebar start -->
  <?php
  if ($RIGHT_SIDEBAR) {
    include('right-sidebar.php');
  }
  ?>
  <!-- Right Slidebar end -->
  <!--footer start-->
  <?php include('footer.php'); echo "Rai"; ?>
  <!--footer end-->
</section>
<?php $PICKERS = TRUE; ?>
<?php include('scripts.php'); ?>





<!-- Start of Add Volunteer Scripts  @Rai-->

<script type="text/javascript" src="js-functions/subjects.js"></script>

<!-- End of Add Volunteer Scripts  @Rai-->

<script type="text/javascript">
<?php require 'subj_mod.js' ?>

$(document).ready(function () {

	$("#sub_birthdate").datepicker();
	$("#report_time").datepicker();
	$('#addSideEffect').on('click', function (event) {

		$.ajax({
			url:"php-functions/fncSideEffect.php?action=addSideEffectBlank",
			type: "POST",
			dataType: "json",
			data: $('#addNewSE').serialize(),
			success:function(id){
				$('#side_effect_id_update').val(id);
				var data = {
					project_id:$('#project_idE').val()
				};
				ajax_load(data, "sideeffects_table.php", "se_tbody");
				$('#addSideEffectModal').modal('show');
			}
		});

		$('#addSideEffectForm')[0].reset();
		$('#submitSideEffects').html("提交");
		// $('#addSideEffectModal').modal('show');
	});
	$("#addSideEffectForm").on('submit', function (event) { // Submit Side Effect Form

		$("#submitSideEffects").prop('disabled', true).html('载入中...');
		$.ajax({
				url:"php-functions/fncSideEffect.php?action=addSideEffect",
				type: "POST",
	    		dataType: "json",
	        	data: $("#addSideEffectForm").serialize(),
				success:function(data){

					$("#submitSideEffects").prop('disabled', false).html('提交');

					if(data == "1")
						// You have successfully added a Side Effect
            <?php if($PAGE_NAME == 'pg_audit.php'):?>
						show_alert("您已成功添加不良事件/反应记录", 'pg_audit.php?tab=sideeffect&project_id='+$("#project_idSE").val(), 1);
            <?php elseif ($PAGE_NAME == 'subject-library.php'):?>
            show_alert("您已成功添加不良事件/反应记录", 'subject-library.php', 1);
            <?php endif; ?>
					else if(data == "2")
						// You have successfully udpated a Side Effect
            <?php if($PAGE_NAME == 'pg_audit.php'):?>
            show_alert("您已成功添加不良反应记录", 'pg_audit.php?tab=sideeffect&project_id='+$("#project_idSE").val(), 1);
            <?php elseif ($PAGE_NAME == 'subject-library.php'):?>
            show_alert("您已成功添加不良反应记录", 'subject-library.php', 1);
            <?php endif; ?>
					else
						show_alert(data, '', 0);

				}
			});
		event.preventDefault();

	});

});

$('#se_docs_modal').on('hidden.bs.modal', function () { // Show side effects main modal when add document is closed
	$('#addSideEffectModal').modal('show');
});

$('#se_docs_form').on('submit', function (e){
  e.preventDefault();
  add_se_document();
});

function add_se_document()
{
  var form_data = new FormData($('#se_docs_form')[0]);
  form_data.append('side_effect_id', $('#side_effect_id_update').val());
  form_data.append('sub_dir', 'side_effect_documents');
  // form_data.append('role_id', '<?php echo $_SESSION['role_id'] ?>');
  // form_data.append('up_type', '1'); /* up_type 1 for PD */

  $.ajax({
    url: "php-functions/fncCommon.php?action=uploadFile",
    type:'POST',
    processData: false,
    contentType: false,
    data:form_data,
    success:function(msg){
      if(msg == 1){
        // show_alert("Operation successful.", "", 1);
        // #We reload the table
        var side_effect_id_ajax = {
        	"side_effect_id":$('#side_effect_id_update').val()
        }
        $('#se_docs_modal').modal('hide');
        ajax_load(side_effect_id_ajax, 'table_side_effect_upload.php', 'table_side_effect_upload');
      }else{
        show_alert(msg, "", 0);
      }
    }
  });
}

function getSideEffectDetails(sideeffect_id, event, user)
{
	if(user != 'RN')
	{
		$('#submitSideEffects').attr('style', 'display:none');
	}
	if(event.target.nodeName != "A" && event.target.nodeName != "I")
	{
		$('#side_effect_id_update').val(sideeffect_id);
	    $.ajax({
	      url:"php-functions/fncSideEffect.php?action=getSideEffectDetails",
	      type: "POST",
	      dataType: "json",
	      data: { "side_effect_id" : sideeffect_id, "type" : "ajax" },
	      success:function(data){
	      	// inputs
        $('#lot_num_s').val(data[0].lot_num);
			$('#num').val(data[0].num);
			$('#report_time').val(data[0].report_time);
			$('#hospital').val(data[0].hospital);
			$('#hospital_mobile').val(data[0].hospital_mobile);
			$('#dept').val(data[0].dept);
			$('#dept_mobile').val(data[0].dept_mobile);
			$('#drug_chinese_name').val(data[0].drug_chinese_name);
			$('#drug_english_name').val(data[0].drug_english_name);
			$('#drug_form').val(data[0].drug_form);
			$('#drug_register').val(data[0].drug_register);
			$('#clinical_indications').val(data[0].clinical_indications);
			$('#sub_initials').val(data[0].sub_initials);
			$('#sub_height').val(data[0].sub_height);
			$('#sub_weight').val(data[0].sub_weight);
			$('#sub_birthdate').val(data[0].sub_birthdate);
			$('#sae_diagnosis').val(data[0].sae_diagnosis);
			$('#sae_handle_detail').val(data[0].sae_handle_detail);
			$('#report_unit').val(data[0].report_unit);
			$('#reporter_position').val(data[0].reporter_position);
			$('#remark').val(data[0].remark);

			// radio
			$('#drug_type_'+data[0].drug_type).prop("checked", true);
			$('#clinical_trial_'+data[0].clinical_trial).prop("checked", true);
			$('#sub_complications_'+data[0].sub_complication).prop("checked", true);
			$('#sae_drug_measure_'+data[0].sae_drug_measure).prop("checked", true);
			$('#sae_situation_'+data[0].sae_situation).prop("checked", true);
			$('#relation_sae_drug_'+data[0].relation_sae_drug).prop("checked", true);
			$('#sae_vest_'+data[0].sae_vest).prop("checked", true);
			$('#sae_unbinding_'+data[0].sae_unbinding).prop("checked", true);
			$('#sae_report_internal_'+data[0].sae_report_internal).prop("checked", true);
			$('#sae_report_external_'+data[0].sae_report_external).prop("checked", true);
			if(data[0].sub_gender == 0)
			{
				$('#sub_gender_m').prop("checked", true);
			}
			else
			{
				$('#sub_gender_f').prop("checked", true);
			}
			// checkbox
			var x = 0;
			for(x=0; x<data[0].report_type.length; x++)
			{
				$('#report_type_'+data[0].report_type[x]).prop('checked', true);
			}

			// select
			$('#se_subj_select').val(data[0].subject_id);
			// $('#se_subj_id_'+data[0].subject_id).attr('selected', 'selected');

			$('#side_effect_id_update').val(sideeffect_id);
			$('#submitSideEffects').html('更新');
			$('#addSideEffectModal').modal('show');
			var se_data = { "side_effect_id" : sideeffect_id };
			ajax_load(se_data, 'table_side_effect_upload.php', 'table_side_effect_upload');
	      }
	    });
	}
}

</script>
</body>
</html>
