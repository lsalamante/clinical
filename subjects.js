var subjects_add = $('#subjects_add');
var subjects_add_form = $('#subjects_add_form');

var subjects_update_form = $('#subjects_update_form'); // form name
var update_volunteer_id = $('#update_volunteer_id'); //hidden volunter_id field
var subjects_update = $('#subjects_update'); //submit button

var subject_remark_form = $("#subject_remark_form");
var subject_remark_id = $("#subject_remark_id");
var subject_remarks = $("#subject_remarks");
var subject_remark_submit = $("#subject_remark_submit");

var dropout_remark_form = $("#dropout_remark_form");
var dropout_remark_id = $("#dropout_remark_id");
var dropout_remarks = $("#dropout_remarks");
var dropout_remark_submit = $("#dropout_remark_submit");

$('.first_visit_date').datepicker();
$('.dropout_date').datepicker();
$('.reject_date').datepicker();
$('.accept_date').datepicker();
$('.random_date').datepicker();
$('.screen_date').datepicker();
$('.bday').datepicker();


subjects_update_form.on('submit', function(e){
  e.preventDefault();
  var form_data = new FormData(subjects_update_form[0]);

  $.ajax({
    url: "php-functions/fncSubjects.php?action=updateSubject",
    type:'POST',
    processData: false,
    contentType: false,
    data:form_data,
    success:function(msg){
      if(msg == 1){
        show_alert("Operation successful.", "pg_audit.php?project_id=<?php echo $_GET['project_id']; ?>&tab=subjects", 1);
      }else{
        show_alert("Operation failed.", "", 0);
      }
    }
  });
});


subjects_add_form.on('submit', function(e){
  e.preventDefault();
  var form_data = new FormData(subjects_add_form[0]);

  $.ajax({
    url: "php-functions/fncSubjects.php?action=addSubject",
    type:'POST',
    processData: false,
    contentType: false,
    data:form_data,
    success:function(msg){
      if(msg == 1){
        show_alert("Operation successful.", "pg_audit.php?project_id=<?php echo $_GET['project_id']; ?>&tab=subjects", 1);
      }else{
        show_alert("Operation failed.", "", 0);
      }
    }
  });
});

changeSubjectRemarkId = function (volunteer_id) {
  subject_remark_id.val(volunteer_id);
  $.ajax({
    url:"php-functions/fncSubjects.php?action=getSubjectRemarks",
    type: "POST",
    data: { "volunteer_id" : volunteer_id, "type" : "ajax" },
    success:function(data){
      var new_data = JSON.parse(data, true);
      $('#subject_remarks').val(new_data[0].subject_remark);
    }
  });
}

subject_remark_form.on('submit', function(e) {
  e.preventDefault();
  var form_data = subject_remark_form.serialize();

  $.ajax({
    url: "php-functions/fncSubjects.php?action=addSubjectRemarks",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        show_alert("Operation successful.", "", 1);
      }else{
        show_alert("Operation failed.", "", 0);
      }
    }
  });
});

changeDropoutRemarkId = function (volunteer_id) {
  dropout_remark_id.val(volunteer_id);
  $.ajax({
    url:"php-functions/fncSubjects.php?action=getDropoutRemarks",
    type: "POST",
    data: { "volunteer_id" : volunteer_id, "type" : "ajax" },
    success:function(data){
      var new_data = JSON.parse(data, true);
      $('#dropout_remarks').val(new_data[0].dropout_remark);
    }
  });
}

dropout_remark_form.on('submit', function(e) {
  e.preventDefault();
  var form_data = dropout_remark_form.serialize();

  $.ajax({
    url: "php-functions/fncSubjects.php?action=addDropoutRemarks",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        show_alert("Operation successful.", "", 1);
      }else{
        show_alert("Operation failed.", "", 0);
      }
    }
  });
});

