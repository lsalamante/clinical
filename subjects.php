<!-- Add Volunteer per Project @Rai-->
<?php require("php-functions/fncSubjects.php"); ?>

<?php if(isset($_GET["project_id"]) && $_GET["project_id"] != "" && ($_SESSION["user_type"] == "RN" || $_SESSION['user_type'] == "SC")){ ?>

  <!-- <a href="#add_subject_modal" data-toggle="modal" class="btn btn-primary" id="addSubject" ><?php echo $phrases['add']?></a> -->

  <?php } ?>

  <?php

  $project_id = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? $_GET["project_id"] : 0;
  $projects = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? array() : $projects; // $projects from subject-library.php

  $subjects = getSubjects($project_id, $projects);

  if(count($subjects) > 0){ // check if found record/s and make way to subject list.

?>
  <?php if($_SESSION['user_type'] == "SC") { ?>

  <a class="btn btn-primary adder" href="export-subjects.php?project_id=<?php echo $_GET['project_id']; ?>"><?php echo $t->tryTranslate("export"); ?></a>

  <?php } ?>
  <!-- SUBJECTS SEARCH -->
      <div class="col-lg-12">
        <form role="form" id="" action="#" enctype="multipart/form-data">
        <!-- Patient Type -->
          <div class="form-group">
            <!-- <label class="col-lg-1"><?php echo $t->tryTranslate("type"); ?></label> -->
            <div class="col-lg-3">
              <input type="text" name="vsearch_keyword" id="vsearch_keyword" class="form-control" placeholder="搜索" onkeyup="keywordSearch(this.value,<?php echo $_GET['project_id']; ?>,'subjects','subject_body.php','subj_body');">
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-1">
              <input type='hidden' name="project_id" id="project_id" value="<?php echo $_GET['project_id']; ?>">
              <!-- <input class="btn btn-primary" type="button" id="vsearch_submit" value="<?php echo $t->tryTranslate('search'); ?>"> -->
            </div>
          </div>
        </form>
      </div>

  <!-- SUBJECTS FILTERS -->
      <div class="col-lg-12">
        <form role="form" id="" action="#" enctype="multipart/form-data">
        <!-- Patient Type -->
          <div class="form-group">
            <!-- <label class="col-lg-1"><?php echo $t->tryTranslate("type"); ?></label> -->
            <div class="col-lg-3">
              <input type="text" id="ssearch_random_no" placeholder="随机号" class="form-control">
            </div>
          </div>
        <!-- Patient Status -->
          <div class="form-group">
            <!-- <label class="col-lg-1"><?php echo $t->tryTranslate("status"); ?></label> -->
            <div class="col-lg-3">
              <select name="vsearch_status" id="ssearch_status" class="form-control">
                <option value=""><?php echo $t->tryTranslate("status"); ?></option>
                <option value="FU"><?php echo $t->tryTranslate("follow_up_subjects"); ?></option>
                <option value="STP"><?php echo $t->tryTranslate("stop"); ?></option>
                <option value="O"><?php echo $t->tryTranslate("out_subjects"); ?></option>
                <option value="D"><?php echo $t->tryTranslate("drop_subjects"); ?></option>
                <!-- <option value="C"><?php echo $t->tryTranslate("before_selecting"); ?></option>
                <option value="S"><?php echo $t->tryTranslate("clinician_selecting"); ?></option>
                <option value="P"><?php echo $t->tryTranslate("pass"); ?></option>
                <option value="NP"><?php echo $t->tryTranslate("no_pass"); ?></option>
                <option value="B"><?php echo $t->tryTranslate("begin"); ?></option>
                <option value="F"><?php echo $t->tryTranslate("finish"); ?></option> -->
              </select>
            </div>
          </div>
        <!-- Patient Birthday -->
          <div class="form-group">
            <!-- <label class="col-lg-1"><?php echo $t->tryTranslate("date_of_birth"); ?></label> -->
            <div class="col-lg-3">
              <input name="vsearch_birthday" id="ssearch_birthday" class="form-control" placeholder="<?php echo $t->tryTranslate('date_of_birth'); ?>">
            </div>
          </div>
        <!-- Patient Gender -->
          <div class="form-group">
            <!-- <label class="col-lg-1"><?php echo $t->tryTranslate("gender"); ?></label> -->
            <div class="col-lg-2">
              <select name="vsearch_gender" id="ssearch_gender" class="form-control">
                <option value=""><?php echo $t->tryTranslate("gender"); ?></option>
                <option value="Male"><?php echo $t->tryTranslate("male"); ?></option>
                <option value="Female"><?php echo $t->tryTranslate("female"); ?></option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-1">
              <input type='hidden' name="project_id" id="project_id" value="<?php echo $_GET['project_id']; ?>">
              <input class="btn btn-primary" type="button" id="vfilter_subj" value="<?php echo $t->tryTranslate('search'); ?>">
            </div>
          </div>
        </form>
      </div>

    <table class="table table-hover">
      <thead>
        <tr>
          <th><?php echo $phrases['number']?></th>
          <th><?php echo $phrases['project_name']?></th>
          <th><?php echo $phrases['project_number']?></th>
          <th><?php echo $phrases['subjects_num']?></th>
          <th><?php echo $phrases['subject_name']?></th>
          <th><?php echo $phrases['abbrv']?></th>
          <th><?php echo $phrases['birthday']; ?></th>
          <th><?php echo $phrases['id_num']; ?></th>
          <!-- <th><?php echo $phrases['random_date']?></th> -->
          <th><?php echo $phrases['random_num']?></th>
          <th><?php echo $phrases['state']?></th>
          <th><?php echo $phrases['state_date']; ?></th>
          <th><?php echo $phrases['operation']?></th>
          </tr>
        </thead>
        <tbody id="subj_body">

          <?php

          $state = array(
              "C" => $phrases['chosen'],
              "B" => $phrases['begin'],
              "FU" => $phrases['follow_up'],
              "STP" => $phrases['stop'],
              "O" => $phrases['out'],
              "NS" => $phrases['next_select'],
              "D" => $phrases['drop'],
              "F" => $phrases['finish']);

          $count = 0;

          foreach ($subjects as $subject) {

            $pi_id = (getProjectById(0,$subject["project_id"])['pi_id']);
            $pi = (getUserById(0,$pi_id));
            $pi = $pi['lname'] ." ". $pi['fname']; #Get the PI name

            $count++;
            ?>
            <!-- set_subject_id is on the bottom of this page -->
            <tr href="#update_subject_modal" onclick="set_subject_id(<?php echo $subject['volunteer_id'] ?>, <?php echo $subject['subject_id'] ?>, '<?php echo $pi ?>')" data-toggle="modal" style="cursor:pointer;">
              <td><?php echo $count; ?></td>
              <td><?php echo $subject["project_name"]; ?></td>
              <td><?php echo $subject["project_num"]; ?></td>
              <td><?php echo $subject["subjects_num"]; ?></td>
              <td><?php echo strtoupper($subject["v_name"]); ?></td>
              <td><?php echo $subject["abbrv"]; ?></td>
              <td><?php echo date('m-d-Y', strtotime($subject["bday"])); ?></td>
              <td><?php echo $subject["id_card_num"]; ?></td>
              <!-- <td><?php echo $subject["random_date"]; ?></td> -->
              <td><?php echo $subject["random_num"]; ?></td>
              <td><?php echo $state[$subject["subject_stat"]]; ?> </td>
              <td><?php echo date('m-d-Y', strtotime($subject["status_date"])); ?> </td>

              <td onclick="event.cancelBubble = true;">

                <?php if($_SESSION["user_type"] == 'C' || $_SESSION['user_type'] == 'SC'){ ?>

                    <!-- Start of buttons for CLinician @Rai-->
                    <?php if($subject['subject_stat'] == "C"){ ?>

                      <a class="btn btn-sm btn-primary" title="Begin" onclick="statusChange(<?php echo $subject["subject_id"] ?>, 'B', '', '<?php echo $_SESSION['lang']?>'); return false;"><?php echo $phrases['subject_begin']; ?></a>

                    <?php }else if($subject['subject_stat'] == 'B'){ ?>

                      <a class="btn btn-sm btn-primary" title="Follow Up" onclick="statusChange(<?php echo $subject["subject_id"] ?>, 'FU', '', '<?php echo $_SESSION['lang']?>'); return false;"><!-- <i class="fa   fa-map-marker"></i> --><?php echo $phrases['subject_follow_up']; ?></a>
                      <a class="btn btn-sm btn-primary" title="Stop" onclick="statusWithRemarks(<?php echo $subject["subject_id"] ?>, 'STP'); return false;"><!-- <i class="fa   fa-exclamation-triangle"></i> --><?php echo $phrases['subject_stop']; ?></a>
                      <a class="btn btn-sm btn-primary" title="Out" onclick="statusWithRemarks(<?php echo $subject["subject_id"] ?>, 'O'); return false;"><!-- <i class="fa  fa-ban"></i> --><?php echo $phrases['subject_out']; ?></a>
                      <a class="btn btn-sm btn-primary" title="Drop" onclick="statusWithRemarks(<?php echo $subject["subject_id"] ?>, 'D'); return false;"><!-- <i class="fa   fa-minus-circle"></i> --><?php echo $phrases['subject_drop']; ?></a>

                      <?php if($_SESSION['user_type'] == 'I'): ?>
                      <a class="btn btn-sm btn-primary" title="SAE" onclick="show_sub_sae('<?php echo $subject['subject_id']; ?>')">SAE</a>
                    <?php endif; ?>

                    <?php }else if($subject['subject_stat'] == "FU"){ ?>

                      <a class="btn btn-sm btn-primary" title="Finish" onclick="statusChange(<?php echo $subject["subject_id"] ?>, 'F', '', '<?php echo $_SESSION['lang']?>'); return false;"><?php echo $phrases['finish']?></a>

                    <?php } ?>
                    <!-- End of buttons for CLinician @Rai-->

                <?php }else if($_SESSION["user_type"] == 'I' && in_array($subject['subject_stat'], array('NS', 'STP', 'O', 'D') ) ){ ?>

                    <!-- Start of button for Inspector @Rai-->
                      <a class="btn btn-sm btn-primary" title="Return to Volunteer" onclick="statusInspectorChange(<?php echo $subject["volunteer_id"] ?>); return false;"><?= $phrases['return_to_volunteer'] ?></a>
                    <!-- End of button for Inspector @Rai-->

                <?php } ?>

              </td>

            </tr>

    <?php } ?>

        </tbody>
    </table>

  <?php }else{ // no record/s found ?>

        <div class="col-lg-12 text-center">
          <img src="img/sad.png" class="sad">
          <h1 class="sad"><?php echo $phrases['empty_table_data']; ?></h1>
        </div>

  <?php } ?>
<!-- Start of Add Stage Inspection Modal  @Rai-->

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addRemarkSModal" class="modal fade " data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title" id="modalTitleAddInspection"><?php echo $phrases['add_remarks']; ?></h4>
      </div>
      <div class="modal-body">

        <form action="#" id="addRemarkSForm">

          <div class="form-group" id="remarkS_div">
            <center>
              <textarea name="remarks" id="remarkS" class="form-group" cols="60" rows="10" placeholder="<?php echo $phrases['please_input_reason']?>"></textarea>
              <p class="help-block" style="display:none;" id="remarkS_msg"></p>
            </center>
          </div>

          <!-- Start hidden fields @Rai-->

            <input type="hidden" value="" id="statusRemark">
            <input type="hidden" value="" id="subject_id">

          <!-- End hidden fields @Rai-->

          <button type="button" class="btn btn-primary" id="submitInspection" onclick="statusChange('', '', 'remarks', '<?php echo $_SESSION['lang']?>'); return false;"><?php echo $phrases['submit']; ?></button>


        </form>

      </div>
    </div>
  </div>
  </div>

<!-- End of Add Stage Inspection Modal  @Rai-->



<input type="hidden" value="<?php echo $_GET['project_id']; ?>" id="project_idS">





<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="subjectModal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title" id="modalTitle">受试者信息</h4>
      </div>
      <div class="modal-body">

          <div class="form-group" id="v_name_div">
            <label for="exampleInputEmail1"><?php echo $phrases['name']?></label>
            <input type="text" class="form-control" id="sv_name" name="v_name" placeholder="">
            <p class="help-block" style="display:none;" id="v_name_msg"></p>
          </div>

          <div class="form-group" id="gender_div">
            <label for="exampleInputEmail1"><?php echo $phrases['gender']?></label>
            <select name="gender" id="sgender" class="form-control">
              <option value=""><?php echo $phrases['select']?></option>
              <option value="Male"><?php echo $phrases['male']?></option>
              <option value="Female"><?php echo $phrases['female']?></option>
            </select>
            <p class="help-block" style="display:none;" id="sgender_msg"></p>
          </div>

          <div class="form-group" id="bday_div">
            <label for="exampleInputEmail1"><?php echo $phrases['birth_date']?></label>
            <input type="text" class="form-control" id="sbday" name="bday" placeholder="">
            <p class="help-block" style="display:none;" id="bday_msg"></p>
          </div>

          <div class="form-group" id="nation_div">
            <label for="exampleInputEmail1"><?php echo $phrases['nation']?></label>
            <input type="text" class="form-control" id="snation" name="nation" placeholder="">
            <p class="help-block" style="display:none;" id="nation_msg"></p>
          </div>

          <div class="form-group" id="native_div">
            <label for="exampleInputEmail1"><?php echo $phrases['native']?></label>
            <input type="text" class="form-control" id="snative" name="native" placeholder="">
            <p class="help-block" style="display:none;" id="native_msg"></p>
          </div>

          <div class="form-group" id="married_div">
            <label for="exampleInputEmail1"><?php echo $phrases['married']?></label>
            <div class="radio">
              <input value="Yes" name="married" type="radio" class="smarried"> <?php echo $phrases['yes']?> &nbsp;&nbsp;
              <input value="No" name="married" type="radio" class="smarried"> <?php echo $phrases['no']?>
            </div>
            <p class="help-block" style="display:none;" id="married_msg"></p>
          </div>

          <div class="form-group" id="blood_type_div">
            <label for="exampleInputEmail1"><?php echo $phrases['blood_type']?></label>
            <input type="text" class="form-control" id="sblood_type" name="blood_type" placeholder="">
            <p class="help-block" style="display:none;" id="blood_type_msg"></p>
          </div>

          <div class="form-group" id="surgery_div">
            <label for="exampleInputEmail1"><?php echo $phrases['had_surgery']?></label>
            <div class="radio">
              <input value="Yes" name="surgery" type="radio" class="ssurgery"> <?php echo $phrases['yes']?> &nbsp;&nbsp;
              <input value="No" name="surgery" type="radio" class="ssurgery"> <?php echo $phrases['no']?>
            </div>
            <p class="help-block" style="display:none;" id="surgery_msg"></p>
          </div>

          <div class="form-group" id="fam_history_div">
            <label for="exampleInputEmail1"><?php echo $phrases['family_history']?></label>
            <input type="text" class="form-control" id="sfam_history" name="fam_history" placeholder="">
            <p class="help-block" style="display:none;" id="fam_history_msg"></p>
          </div>

          <div class="form-group" id="smoker_div">
            <label for="exampleInputEmail1"><?php echo $phrases['heavy_smoking']?></label>
            <div class="radio">
              <input value="Yes" name="smoker" type="radio" class="ssmoker"> <?php echo $phrases['yes']?> &nbsp;&nbsp;
              <input value="No" name="smoker" type="radio" class="ssmoker"> <?php echo $phrases['no']?>
            </div>
            <p class="help-block" style="display:none;" id="smoker_msg"></p>
          </div>

          <div class="form-group" id="drinker_div">
            <label for="exampleInputEmail1"><?php echo $phrases['heavy_drinking']?> </label>
            <div class="radio">
              <input value="Yes" name="drinker" type="radio" class="sdrinker"> <?php echo $phrases['yes']?> &nbsp;&nbsp;
              <input value="No" name="drinker" type="radio" class="sdrinker"> <?php echo $phrases['no']?>
            </div>
            <p class="help-block" style="display:none;" id="drinker_msg"></p>
          </div>

          <div class="form-group" id="allergies_div">
            <label for="exampleInputEmail1"><?php echo $phrases['allergies']?></label>
            <input type="text" class="form-control" id="sallergies" name="allergies" placeholder="">
            <p class="help-block" style="display:none;" id="allergies_msg"></p>
          </div>

          <div class="form-group" id="height_div">
            <label for="exampleInputEmail1"><?php echo $phrases['height']?> 『cm』</label>
            <input type="text" class="form-control" id="sheight" name="height" placeholder="" onchange="computeBMI();">
            <p class="help-block" style="display:none;" id="height_msg"></p>
          </div>

          <div class="form-group" id="weight_div">
            <label for="exampleInputEmail1"><?php echo $phrases['weight']?> 『kg』</label>
            <input type="text" class="form-control" id="sweight" name="weight" placeholder="" onchange="computeBMI();">
            <p class="help-block" style="display:none;" id="weight_msg"></p>
          </div>

          <div class="form-group" id="bmi_div">
            <label for="exampleInputEmail1"><?php echo $phrases['bmi']?></label>
            <input type="text" class="form-control" id="sbmi" name="bmi" placeholder="" readonly="">
            <p class="help-block" style="display:none;" id="bmi_msg"></p>
          </div>

          <div class="form-group" id="med_history_div">
            <label for="exampleInputEmail1"><?php echo $phrases['medical_history']?></label>
            <input type="text" class="form-control" id="smed_history" name="med_history" placeholder="">
            <p class="help-block" style="display:none;" id="med_history_msg"></p>
          </div>

          <div class="form-group" id="mobile_div">
            <label for="exampleInputEmail1"><?php echo $phrases['mobile_num']?></label>
            <input type="text" class="form-control" id="smobile" name="mobile" placeholder="">
            <p class="help-block" style="display:none;" id="mobile_msg"></p>
          </div>

          <div class="form-group" id="">
            <label for=""><?php echo $phrases['id_card_num'] ?></label>
            <input type="text" class="form-control" id="sid_card_num" name="id_card_num" placeholder="" required>
          </div>

          <div class="form-group hidden" id="sid_card_copy_div">
            <label for=""><?php echo $phrases['id_card_copy'] ?> <?php echo $phrases['file']?></label><br>
            <a href="" class="btn btn-sm btn-primary" target="_blank" id="sid_card_copy_file" title="view file"><?php echo $phrases['view_attachments']; ?></a>
          </div>


          <!-- Start hidden fields @Rai-->

          <input type="hidden" name="project_id" id="project_id" value="<?php echo $_GET["project_id"]; ?>">
          <input type="hidden" name="v_status" id="v_status" >

          <!-- End hidden fields @Rai-->


          <hr>
          <form role="form" id="subjectsForm" action="#" method="post">

          <div class="form-group">
            <label for=""><?php echo $phrases['case_num'] ?></label>
            <input type="text" class="form-control" id="case_num" name="case_num" placeholder="" required>
          </div>

          <div class="form-group">
            <label for=""><?php echo $phrases['subjects_num'] ?></label>
            <input type="text" class="form-control" id="subjects_num" name="subjects_num" placeholder="" required>
          </div>

          <div class="form-group">
            <label for=""><?php echo $phrases['abbrv'] ?></label>
            <input type="text" class="form-control" id="abbrv" name="abbrv" placeholder="" required>
          </div>

          <div class="form-group">
            <label for=""><?php echo $phrases['country'] ?></label>
            <input type="text" class="form-control" id="country" name="country" placeholder="" required>
          </div>

          <div class="form-group">
            <label for=""><?php echo $phrases['address'] ?></label>
            <input type="text" class="form-control" id="address" name="address" placeholder="" required>
          </div>

          <div class="form-group">
            <label for=""><?php echo $phrases['principal_investigator'] ?></label>
            <input type="text" class="form-control" id="the_pi" required disabled>
          </div>

          <div class="form-group">
            <label for=""><?php echo $phrases['random_num'] ?></label>
            <input type="text" class="form-control" id="random_num" name="random_num" placeholder="" required>
          </div>

          <div class="form-group">
            <label for=""><?php echo $phrases['random_date'] ?></label>
            <input type="text" class="form-control" id="random_date" name="random_date" placeholder="" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo $phrases['sign_informed_consent']?> </label>
            <div class="radio">
              <input value="1" name="is_informed_consent" id="iifc_yes" type="radio" required=""> <?php echo $phrases['yes']?> &nbsp;&nbsp;
              <input value="0" name="is_informed_consent" id="iifc_no" type="radio"> <?php echo $phrases['no']?>
            </div>
          </div>

          <div class="form-group">
            <label for=""><?php echo $phrases['sign_date'] ?></label>
            <input type="text" class="form-control" id="sign_date" name="sign_date" placeholder="" required>
          </div>

          <input type="hidden" name="subject_id" id="update_subject_id" value="">
          <input type="hidden" name="type" value="ajax">
          <input type="hidden" name="fr_sbj_lib" value="<?php echo (isset($_GET['project_id']) && $_GET['project_id'] != '')  ? $_GET['project_id'] : '' ;?>"> <!-- from subject library -->


          <button type="submit" class="btn btn-primary" id="submitSubject"><?php echo $phrases['update']?></button>
        </form>
      </div>
    </div>
  </div>
</div>
