<section class="panel negative-panel">
  <div class="panel-body">
    <?php if($project['status_num'] == null){ ?>
      <!--        ###################### ONLY SHOWS WHEN THE PROJECT HAS NEVER BEEN SUBMITTED YET!!! ##############-->
      <button type="" class="btn btn-default" id="ap_save"><?php echo $phrases['save_as_draft']?></button><br>
      <!--        ###################### / ONLY SHOWS WHEN THE PROJECT HAS NEVER BEEN SUBMITTED YET!!! ##############-->
      <?php } ?>
    <?php if(in_array($project['status_num'], [0,null])){   #pag 0, null, pwede siya mag submit ?>
      <form name="ap_submit_form" id="ap_submit_form">
        <input hidden id="status" name="status"  value="1"> <!-- # For changing status, set POST['status'] for non-reject status change ref. fncProjects function: changeStatus  -->
        <input hidden id="project_status" name="project_status" value="0"> <!-- # to be incremented in PHP function  -->
        <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
      </form>
      <button type="" class="btn btn-primary" id="ap_submit"><?php echo $phrases['submit']?></button>
      <?php } ?>

      <?php if(in_array($project['status_num'], [4])){ ?>

        <form id="audit_form">
          <div class="row">
            <label class="col-sm-2 control-label"><?php echo $phrases['approve_reject_investigator_revision']?><span class="required">*</span></label><br>
            <label class="col-sm-2 control-label"></label><br>
            <div class="form-group">
              <div class="col-sm-2"></div>
              <div class="col-sm-10">
                <div class="radio">
                  <label>
                    <input type="radio" id="agree_radio" name="status" value="1" /> <?php echo $phrases['agree']?>
                  </label>
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" id="disagree_radio" name="status" value="0" /> <?php echo $phrases['disagree']?>
                  </label>
                </div>
              </div>
              <div class="col-sm-6" id="disagree_remarks" style="display:none;">
                <label for="comments"><?php echo $phrases['comment_app_4']; ?></label><span class="required">*</span>
                <textarea class="form-control v-resize-only" id="comments" name="comments" placeholder="<?php echo $phrases['comment_app_4']; ?>"></textarea>
                <span class="help-block"></span>
              </div>
              <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
              <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
            </div>
          </div>
          <section class="panel">
            <div class="panel-body">
              <button type="" class="btn btn-primary" id="status_submit" disabled ><?php echo $phrases['submit']?></button>
            </div>
          </section>
        </form>
        <?php } ?>

        <?php if($project['status_num'] == 7 && $project['is_rejected']){ ?>
          <form id="audit_form">
            <div class="row">
              <input hidden id="status" name="status" value="1">
              <input hidden id="project_status" name="project_status" value="<?php echo ($project['status_num'] - 1); ?>">
              <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
            </div>
          <section class="panel negative-panel">
            <div class="panel-body">
              <button type="" class="btn btn-primary" id="status_submit" ><?php echo $phrases['submit']?></button>
            </div>
          </section>
        </form>
        <?php }elseif($project['status_num'] == 16){ ?>
          <form id="audit_form">
            <div class="row">
              <input hidden id="status" name="status" value="1">
              <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
              <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
            </div>
          <section class="panel negative-panel">
            <div class="panel-body">
              <button type="" class="btn btn-primary" id="status_submit" ><?php echo $phrases['project_complete']?></button>
            </div>
          </section>
          </form>
        <?php }?>
        </div>
      </section>
