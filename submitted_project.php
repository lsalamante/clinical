<?php
include('jp_library/jp_lib.php');
require("php-functions/fncCommon.php");

if (!isset($_SESSION['user_id'])) {
    header("Location: " . "index.php");
    die();
}

#initial page data
//$user_array = getUserById(0, $_SESSION['user_id']);
$profgroup_array = getProfGroup(0);
$departments = getAllDepartments(0);
$trials = getAllTrials(0);

if(!isset($_GET['project_id'])){
    $_GET['project_id'] = false;
}

$project = getProjectById(0, $_GET['project_id']);

$prof_group_users = getPIByProfGroupId(0, $project['pgroup_id']); #get PI for principal investigator field

$pgroup_admin_id = getProfGroupAdminIdByProfGroupId(0, $project['pgroup_id']);

$pgroup_admin = getUserById(0, $pgroup_admin_id);
$pi = getUserById(0, $project['pi_id']);

?>
    <!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/html">
    <?php include('header.php'); ?>

        <body>
            <section id="container">
                <!--header start-->
                <header class="header white-bg">
                    <?php
        if ($LEFT_SIDEBAR) {
            echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
        }
        ?>
                        <!--logo start-->
                        <?php if ($LOGO) {
            include('logo.php');
        }
        ?>
                            <!--logo end-->
                            <div class="nav notify-row" id="top_menu">
                                <!--  notification start -->
                                <?php if ($NOTIFICATION) {
                include('notification.php');
            } ?>
                                    <!--  notification end -->
                            </div>
                            <?php include('top-nav.php'); ?>
                </header>
                <!--header end-->
                <!--sidebar start-->
                <?php
    if ($LEFT_SIDEBAR) {
        include('left-sidebar.php');
    }
    ?>
                    <!--sidebar end-->
                    <!--main content start-->
                    <style>
                        span.required
                        {
                            margin-left: 3px;
                            color: red;
                        }
                    </style>
                    <section id="main-content">
                        <section class="wrapper site-min-height">
                            <!-- page start-->
                            <div class="row">
                                <div class="col-lg-12">
                                    <!--breadcrumbs start -->
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Project audit</a></li>
                                        <li>Project details</li>
                                    </ul>
                                    <!--breadcrumbs end -->
                                </div>
                            </div>
                            <div class="row <?php echo ($project) ? " " : "hidden "; ?>">
                                <div class="col-lg-12">
                                    <section class="panel negative-panel">
                                        <form name="project_form" id="project_form">
                                            <header class="panel-heading tab-bg-dark-navy-blue ">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a data-toggle="tab" href="#project">Project details</a>
                                                    </li>
                                                    <li class="">
                                                        <a data-toggle="tab" href="#professional-group">Professional group details</a>
                                                    </li>
                                                    <li class="">
                                                        <a data-toggle="tab" href="#applicant">Applicant details</a>
                                                    </li>
                                                    <li class="">
                                                        <a data-toggle="tab" href="#cro">CRO details</a>
                                                    </li>
                                                    <li class="">
                                                        <a data-toggle="tab" href="#other">Other details</a>
                                                    </li>
                                                </ul>
                                            </header>
                                            <div class="panel-body">
                                                <div class="tab-content">
                                                    <div id="project" class="tab-pane active">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Lot number</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" id="lot_num" name="lot_num"
                                                                value="<?=$project['lot_num'] ?>" disabled>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Project number</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" id="project_num" name="project_num" value="<?=$project['project_num'] ?>" disabled>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Project name</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" id="project_name" name="project_name" value="<?=$project['project_name'] ?>" disabled>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Trial category</label>

                                                            <div class="col-sm-10">
                                                                <select class="form-control m-bot15" name="trial_id" id="trial_id" disabled>
                                                                    <?php foreach ($trials as $row) { ?>
                                                                        <option value="<?= $row['trial_id'] ?>"
                                                                           <?=($project['trial_id'] == $row['trial_id']) ? "selected='selected'": "" ;?>
                                                                           >
                                                                            <?= $row['trial_name'] ?>
                                                                        </option>
                                                                        <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Indications</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" name="indications" id="indications" value="<?=$project['indications'] ?>" disabled>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Risk category</label>
                                                            <!-- #TODO -->
                                                            <div class="col-sm-10">
                                                                <select class="form-control m-bot15" id="risk_category" name="risk_category" disabled>
                                                                    <option value="2" <?=($project['risk_category'] == 2) ? "selected='selected'": "" ;?> >High</option>
                                                                    <option value="1" <?=($project['risk_category'] == 1) ? "selected='selected'": "" ;?> >Mid</option>
                                                                    <option value="0" <?=($project['risk_category'] == 0) ? "selected='selected'": "" ;?> >Low</option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Departments</label>

                                                            <div class="col-sm-10">
                                                                <select class="form-control m-bot15" name="dept_id" id="departments" disabled>
                                                                    <?php foreach ($departments as $row) { ?>
                                                                        <option value="<?= $row['dept_id'] ?>"
                                                                           <?=($project['dept_id'] == $row['dept_id']) ? "selected='selected'": "" ;?>
                                                                           >
                                                                            <?= $row['dept_name'] ?>
                                                                        </option>
                                                                        <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div id="professional-group" class="tab-pane">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Professional group</label>

                                                            <div class="col-sm-10">
                                                                <select class="form-control m-bot15" name="pgroup_id" id="professional_group" disabled>
                                                                    <?php foreach ($profgroup_array as $row) { ?>
                                                                        <option value="<?= $row['pgroup_id'] ?>"
                                                                         <?=($project['pgroup_id'] == $row['pgroup_id']) ? "selected='selected'": "" ;?>
                                                                           >
                                                                            <?= $row['pgroup_name'] ?>
                                                                        </option>
                                                                        <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Principal investigator</label>

                                                            <div class="col-sm-10">
                                                                <select class="form-control m-bot15" name="pi_id" id="pi_id" disabled>
                                                                       <?php foreach ($prof_group_users as $row) { ?>
                                                                        <option value="<?= $row['user_id'] ?>"
                                                                         <?=($project['pi_id'] == $row['user_id']) ? "selected='selected'": "" ;?>
                                                                           >
                                                                            <?= $row['fname'] . " " . $row['lname'] ?>
                                                                        </option>
                                                                        <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Professional group contact person
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" id="pgroup_contact" name="pgroup_contact" readonly value="<?= $pgroup_admin['fname'] . " " . $pgroup_admin['lname'] ?>" disabled>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Professional group mobile</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" id="pgroup_mobile" name="pgroup_mobile" readonly value="<?= $pi['mobile_num'] ?>" disabled>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Professional group email</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" id="pgroup_email" name="pgroup_email" readonly value="<?= $pi['email'] ?>" disabled>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div id="applicant" class="tab-pane">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Applicant name</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" readonly name="applicant_name" value="<?= $_SESSION['full_name'] ?>" disabled>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Emergency contact person</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" name="emergency_contact" value="<?=$project['emergency_contact'] ?>" disabled>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Emergency contact mobile</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" name="emergency_mobile" value="<?=$project['emergency_mobile'] ?>" disabled>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Emergency email</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" name="emergency_email" value="<?=$project['emergency_email'] ?>" disabled>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div id="cro" class="tab-pane">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">CRO name</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" name="cro_name" id="cro_name" value="<?=$project['cro_name'] ?>" disabled>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">CRO contact person</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" name="cro_contact" id="cro_contact" value="<?=$project['cro_contact'] ?>" disabled>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">CRO contact mobile</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" name="cro_mobile" id="cro_mobile" value="<?=$project['cro_mobile'] ?>" disabled>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">CRO email</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control" name="cro_email" id="cro_email" value="<?=$project['cro_email'] ?>" disabled>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div id="other" class="tab-pane">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Research objectives</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control v-resize-only" name="research_objectives" disabled><?=$project['research_objectives'] ?></textarea>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Subsidize type</label>
                                                            <div class="col-sm-10">
                                                            <?php
                                                            /*$subsidize_value = 'Fully funded';
                                                            if($project['subsidize_type'] == 1)
                                                                $subsidize_value = 'Partially funded';
                                                            elseif($project['subsidize_type'] == 0)
                                                                $subsidize_value = 'No subsidize';*/
                                                            ?>
                                                                <!--<input type="text" name="subsidize_type" value="<?php echo $subsidize_value; ?>" disabled>-->

                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" id="fully_funded" value="2" checked="" name="subsidize_type" <?=($project['subsidize_type'] == 2) ? "checked='checked'": "" ;?> disabled> Fully funded
                                                                    </label>
                                                                </div>
                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" id="partially_funded" value="1" name="subsidize_type" <?=($project['subsidize_type'] == 1) ? "checked='checked'": "" ;?> disabled> Partially funded
                                                                    </label>
                                                                </div>
                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" id="no_subsidize" value="0" name="subsidize_type" <?=($project['subsidize_type'] == 0) ? "checked='checked'": "" ;?> disabled> No subsidize
                                                                    </label>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Remarks</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control v-resize-only" id="remarks" name="remarks" disabled><?=$project['remarks'] ?></textarea>
                                                                <span class="help-block"></span>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" value="<?= $_GET['project_id'] ?>" id="project_id" name="project_id" />

                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </section>
                                </div>
                                <div class="col-lg-12 ">
                                    <section class="panel negative-panel">
                                        <header class="panel-heading">
                                            Multi-center information
                                        </header>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <input type="checkbox" id="mc_chk_all" />
                                                    </th>
                                                    <th>Organization</th>
                                                    <th>Person in charge</th>
                                                    <th>Lead/Participate</th>
                                                    <th>Remarks</th>
                                                </tr>
                                            </thead>
                                            <tbody id="mc_body">

                                            </tbody>
                                        </table>
                                    </section>
                                    <section class="panel negative-panel">
                                        <header class="panel-heading">
                                            Project documents list
                                        </header>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <input type="checkbox" id="pd_chk_all" />
                                                    </th>
                                                    <th>Name</th>
                                                    <th>Version</th>
                                                    <th>Version Date</th>
                                                    <th>Description</th>
                                                    <th>File</th>
                                                </tr>
                                            </thead>
                                            <tbody id="pd_body">

                                            </tbody>
                                        </table>
                                    </section>
                                    <section class="panel negative-panel">
                                        <header class="panel-heading">
                                            Project plan
                                        </header>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <input type="checkbox" id="pp_chk_all"/>
                                                    </th>
                                                    <th>Name</th>
                                                    <th>Version</th>
                                                    <th>Version Date</th>
                                                    <th>Description</th>
                                                    <th>File</th>
                                                </tr>
                                            </thead>
                                            <tbody id="pp_body">

                                            </tbody>
                                        </table>
                                    </section>

                                    <!-- feedback row START -->
                                    <section class="panel negative-panel">
                                        <header class="panel-heading">
                                            Feedback
                                        </header>
                                        <form id="audit_form">
                                            <div class="panel-body">
                                            <?php if($project['status_num'] > 1): ?>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Testing Facility opinion</label>
                                                        <div class="col-sm-6">
                                                            <textarea class="form-control v-resize-only" id="tfo_opinion" name="tfo_opinion" disabled>SQL QUERY GET TFO COMMENTS HERE</textarea>
                                                            <span class="help-block"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <?php if($project['status_num'] > 3 || $project['status_num'] == 0): ?>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Principal Investigator</label>
                                                        <div class="col-sm-6">
                                                            <textarea class="form-control v-resize-only" id="pi_opinion" name="pi_opinion" disabled>SQL QUERY GET TFO COMMENTS HERE</textarea>
                                                            <span class="help-block"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                                <?php
                                                    $allowed_submit =  # for submit button visibility
                                                    array(
                                                        "A" => array("5", "8"), # 8 rejected cause needs modification by ethics chairman, is_rejected from TABLE:`projects` MUST be == 1
                                                        "TFO" => array("1","7"),
                                                        "PI" => array("2"),
                                                        "I" => array("4", "6","11", "13"),
                                                        "ECC" => array("8", "10", "12"), # 8 where is_rejected in TABLE:`projects` MUST be == 0
                                                        "PRE" => array("3"),
                                                        );
                                                ?>

                                                <?php if(in_array($project['status_num'], $allowed_submit[$_SESSION['user_type']])):  ?>
                                                    <?php if($project['status_num'] == '1'): # TFO APPROVAL FROM APPLICANT ?>
                                                    <div class="row">

                                                    </div>
                                                    <div class="row">
                                                        <label class="col-sm-2 control-label">Testing Facility Office<span class="required">*</span></label><br>
                                                        <div class="form-group">
                                                            <div class="col-sm-10">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox" id="add_documents" name="add_documents" value="1"> Add Documents
                                                                    </label>
                                                                </div>
                                                                <div class="col-sm-6" id="docs_comments_div" style="display:none;">
                                                                    <label for="add_comments">Add Documents Comments</label><span class="required">*</span>
                                                                    <textarea class="form-control v-resize-only" id="add_comments" name="add_comments" placeholder="Add Documents Comment Here"></textarea>
                                                                    <span class="help-block"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <label class="col-sm-2 control-label"></label><br>
                                                        <div class="form-group">
                                                            <div class="col-sm-10">
                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" id="agree_radio" name="status" value="1"> Agree
                                                                    </label>
                                                                </div>
                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" id="disagree_radio" name="status" value="0"> Disagree
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6" id="disagree_remarks" style="display:none;">
                                                                <label for="comments">Comments</label><span class="required">*</span>
                                                                <textarea class="form-control v-resize-only" id="comments" name="comments" placeholder="Disagree Comments Here"></textarea>
                                                                <span class="help-block"></span>
                                                            </div>
                                                            <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
                                                            <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                                    <?php if($project['status_num'] == '7'): # TFO SUBMIT MATERIALS TO ETHICS COMMITTEE ?>
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Submit to Ethics Committee Chairman</label>
                                                            <input hidden id="status" name="status" value="1">
                                                            <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
                                                            <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                                    <?php if($project['status_num'] == '2'): ?>
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Principal Investigator Opinion<span class="required">*</span></label>
                                                            <div class="col-sm-4">
                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" id="agree_radio" name="status" value="1"> Agree
                                                                    </label>
                                                                </div>
                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" id="disagree_radio" name="status" value="0"> Disagree
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-7" id="disagree_remarks" style="display:none;">
                                                                <label for="comments">Comments</label><span class="required">*</span>
                                                                <textarea class="form-control v-resize-only" id="comments" name="comments" placeholder="Disagree Comments Here"></textarea>
                                                                <span class="help-block"></span>
                                                            </div>
                                                            <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
                                                            <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
                                                        </div>
                                                    </div>
                                                    <?php endif; # ENDIF for status_num == 2 ?>
                                                    <?php if(($project['status_num'] == '8' || $project['status_num'] == '12') && $project['is_rejected'] == 0): ?>
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Ethics Committee Opinion<span class="required">*</span></label>
                                                            <div class="col-sm-6">
                                                                <?php if($project['status_num'] == "8"): ?>
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox" id="add_documents" name="add_documents" value="1"> Add Documents
                                                                    </label>
                                                                </div>
                                                                <div id="docs_comments_div" style="display:none">
                                                                    <label for="comments">Add Documents Comments</label><span class="required">*</span>
                                                                    <textarea class="form-control v-resize-only" id="add_comments" name="add_comments" placeholder="Documents Comment Here"></textarea>
                                                                </div>
                                                                <?php endif; ?>
                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" id="agree_radio" name="status" value="1"> Agree
                                                                    </label>
                                                                </div>
                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" id="disagree_radio" name="status" value="0"> Disagree
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6" id="disagree_remarks" style="display:none;">
                                                                <label for="comments">Comments</label><span class="required">*</span>
                                                                <textarea class="form-control v-resize-only" id="comments" name="comments" placeholder="Disagree Comments Here"></textarea>
                                                                <span class="help-block"></span>
                                                            </div>
                                                            <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
                                                            <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
                                                        </div>
                                                    </div>
                                                    <?php endif; # ENDIF for status_num == 8 || status_num == 12 && is_rejected == 0 ?>
                                                    <?php if($project['status_num'] == '10'): ?>
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">Ethics Committee Upload Conference Content and Consent<span class="required">*</span></label>
                                                            <!--
                                                            TODO: delete if confirmed that this is not needed
                                                            <div class="col-sm-6">
                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" id="agree_radio" name="status" value="1"> Agree
                                                                    </label>
                                                                </div>
                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" id="disagree_radio" name="status" value="0"> Disagree
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6" id="disagree_remarks" style="display:none;">
                                                                <label for="comments">Comments</label><span class="required">*</span>
                                                                <textarea class="form-control v-resize-only" id="comments" name="comments" placeholder="Disagree Comments Here"></textarea>
                                                                <span class="help-block"></span>
                                                            </div> -->
                                                            <input hidden id="status" name="status" value="1">
                                                            <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
                                                            <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
                                                        </div>
                                                    </div>
                                                    <?php endif; # ENDIF for status_num == 10 ?>
                                                    <?php if($project['status_num'] == '3'): ?>

                                                    <div class="row">
                                                        <section class="panel">
                                                        <header class="panel-heading">
                                                            Peer review expert opinion<span class="required">*</span>
                                                            <br>
                                                            <a href="#cmt_modal" data-toggle="modal" class="btn btn-xs btn-primary adder" id="cmt_add">Add</a>
                                                            <button type="" class="btn btn-danger btn-xs" id="cmt_del">Delete</button>
                                                            <a href="#" class="btn btn-xs btn-primary adder" id="cmt_pdf">Download template</a>
                                                        </header>
                                                        <table class="table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>
                                                                        <input type="checkbox" id="cmt_chk_all"/>
                                                                    </th>
                                                                    <th>Name</th>
                                                                    <th>Version</th>
                                                                    <th>Version Date</th>
                                                                    <th>Description</th>
                                                                    <th>File</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="cmt_body">

                                                            </tbody>
                                                        </table>
                                                        </section>
                                                        <input hidden id="status" name="status" value="1">
                                                        <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
                                                        <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
                                                    </div>
                                                    <?php endif; ?>
                                                    <?php if($project['status_num'] == '4'): ?>

                                                    <div class="row">
                                                        <section class="panel">
                                                        <header class="panel-heading">
                                                            Upload newer version<span class="required">*</span>
                                                            <br>
                                                            <a href="#i_modal" data-toggle="modal" class="btn btn-xs btn-primary adder" id="i_add">Upload</a>
<!--             <button type="" class="btn btn-danger btn-xs" id="i_del">Delete</button>-->
                                                        </header>
                                                        <table class="table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>
                                                                        <input type="checkbox" id="i_chk_all"/>
                                                                    </th>
                                                                    <th>Name</th>
                                                                    <th>Version</th>
                                                                    <th>Version Date</th>
                                                                    <th>Description</th>
                                                                    <th>File</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="i_body">

                                                            </tbody>
                                                        </table>
                                                        </section>
                                                    </div>
                                                    <input hidden id="status" name="status" value="1">
                                                    <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
                                                    <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
                                                    <?php endif; ?>

                                            <?php endif; # ENDIF for in_array($project['status_num'], $allowed_submit[$_SESSION['user_type']]) ?>
                                            </div>
                                        </form>
                                    </section>
                                    <!-- feedback row END -->


                                    <?php
                                    $status_disabled_not_needed = array("10", "7");
                                    if(in_array($project['status_num'], $allowed_submit[$_SESSION['user_type']])): ?>
                                    <section class="panel negative-panel">
                                        <div class="panel-body">
                                            <button type="" class="btn btn-primary" id="status_submit" <?php echo in_array($project['status_num'], $status_disabled_not_needed) ? "" : 'disabled="disabled"'; ?>>Submit</button>
                                        </div>
                                    </section>
                                <?php endif; ?>
                                </div>
                            </div>

                            <div class="row <?php echo (!$project) ? " " : "hidden "; ?>">
                                <div class="col-lg-12">
                                    <section class="panel negative-panel">
                                        <div class="row">

                                            <div class="col-lg-12 text-center">
                                                <img src="img/sad.png" class="sad">
                                                <h1 class="sad">Sorry! That project does not exist.</h1>
                                            </div>
                                        </div>
                                </div>
                                </section>
                            </div>
                            </div>

                            <!-- page end-->
                        </section>
                    </section>



                              <!-- find your modals 'round here boy!!! -->

                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="mc_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Add multi-center information</h4>
                                          </div>
                                          <div class="modal-body">
                                              <form role="form" id="mc_form">
                                                  <div class="form-group">
                                                      <label for="exampleInputEmail1">Organization</label>
                                                      <input type="text" class="form-control" id="mc_org" name="mc_org" placeholder="" required>
                                                  </div>
                                                  <div class="form-group">
                                                      <label for="exampleInputPassword1">Person in charge</label>
                                                      <input type="text" class="form-control" id="mc_person" name="mc_person" placeholder="" required>
                                                  </div>

                                                  <div class="form-group">
                                                      <label for="exampleInputFile">Lead/Participate</label>
                                                      <select class="form-control m-bot15" name="mc_lead" id="mc_lead" >
                                                          <option value="" disabled="disabled">Choose a lead</option>
                                                          <option value="Squall">Squall</option>
                                                          <option value="Cloud">Cloud</option>
                                                          <option value="Sephiroth">Sephiroth</option>
                                                      </select>

                                                  </div>
                                                  <div class="form-group">
                                                      <label for="exampleInputEmail1">Remarks</label>
                                                      <textarea class="form-control v-resize-only" id="mc_remarks" name="mc_remarks" placeholder="" required></textarea>
                                                  </div>
                                                  <button type="" class="btn btn-primary" id="mc_submit">Submit</button>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="pd_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Add project documents</h4>
                                          </div>
                                          <div class="modal-body">

                                              <form role="form" id="pd_form" enctype="multipart/form-data">
                                                  <div class="form-group">
                                                      <label for="exampleInputEmail1">Name</label>
                                                      <input type="text" class="form-control" id="pd_name" name="up_name" placeholder="">
                                                  </div>
                                                  <div class="form-group">
                                                      <label for="exampleInputPassword1">Version</label>
                                                      <input type="number" min="0" step="0.1" class="form-control" id="pd_ver" name="up_ver" placeholder="">
                                                  </div>
                                                  <div class="form-group">
                                                      <label for="exampleInputPassword1">Description</label>
                                                      <textarea class="form-control v-resize-only" id="pd_desc" name="up_desc" placeholder=""></textarea>
                                                  </div>
                                                  <div class="form-group">
                                                      <label for="exampleInputFile">Select file</label>
                                                      <input type="file" id="pd_file" name="up_file[]" multiple="">
                                                      <p class="help-block">Example block-level help text here.</p>
                                                  </div>
                                                  <button type="" class="btn btn-primary" id="pd_submit">Submit</button>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                               <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="pp_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Add project plan</h4>
                                          </div>
                                          <div class="modal-body">

                                              <form role="form" id="pp_form" enctype="multipart/form-data">
                                                  <div class="form-group">
                                                      <label for="exampleInputEmail1">Name</label>
                                                      <input type="text" class="form-control" id="pp_name" name="up_name" placeholder="">
                                                  </div>
                                                  <div class="form-group">
                                                      <label for="exampleInputPassword1">Version</label>
                                                      <input type="number" min="0" step="0.1" class="form-control" id="pp_ver" name="up_ver" placeholder="">
                                                  </div>
                                                  <div class="form-group">
                                                      <label for="exampleInputPassword1">Description</label>
                                                      <textarea class="form-control v-resize-only" id="pp_desc" name="up_desc" placeholder=""></textarea>
                                                  </div>
                                                  <div class="form-group">
                                                      <label for="exampleInputFile">Select file</label>
                                                      <input type="file" id="pp_file" name="up_file[]" multiple="">
                                                      <p class="help-block">Example block-level help text here.</p>
                                                  </div>
                                                  <button type="" class="btn btn-primary" id="pp_submit">Submit</button>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="i_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Add project version 2.x</h4>
                                          </div>
                                          <div class="modal-body">

                                              <form role="form" id="i_form" enctype="multipart/form-data">
                                                  <div class="form-group">
                                                      <label for="exampleInputEmail1">Name</label>
                                                      <input type="text" class="form-control" id="i_name" name="up_name" placeholder="">
                                                  </div>
                                                  <div class="form-group">
                                                      <label for="exampleInputPassword1">Version</label>
                                                      <input type="number" min="0" step="0.1" class="form-control" id="i_ver" name="up_ver" placeholder="">
                                                  </div>
                                                  <div class="form-group">
                                                      <label for="exampleInputPassword1">Description</label>
                                                      <textarea class="form-control v-resize-only" id="i_desc" name="up_desc" placeholder=""></textarea>
                                                  </div>
                                                  <div class="form-group">
                                                      <label for="exampleInputFile">Select file</label>
                                                      <input type="file" id="i_file" name="up_file[]" multiple="">
                                                      <p class="help-block">Example block-level help text here.</p>
                                                  </div>
                                                  <button type="" class="btn btn-primary" id="i_submit">Submit</button>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="cmt_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                              <h4 class="modal-title">Add P.R.E comment</h4>
                                          </div>
                                          <div class="modal-body">

                                              <form role="form" id="cmt_form" enctype="multipart/form-data">
                                                  <div class="form-group">
                                                      <label for="exampleInputEmail1">Name</label>
                                                      <input type="text" class="form-control" id="cmt_name" name="up_name" placeholder="">
                                                  </div>
                                                  <div class="form-group">
                                                      <label for="exampleInputPassword1">Version</label>
                                                      <input type="number" min="0" step="0.1" class="form-control" id="cmt_ver" name="up_ver" placeholder="">
                                                  </div>
                                                  <div class="form-group">
                                                      <label for="exampleInputPassword1">Description</label>
                                                      <textarea class="form-control v-resize-only" id="cmt_desc" name="up_desc" placeholder=""></textarea>
                                                  </div>
                                                  <div class="form-group">
                                                      <label for="exampleInputFile">Select file</label>
                                                      <input type="file" id="cmt_file" name="up_file[]" multiple="">
                                                      <p class="help-block">Example block-level help text here.</p>
                                                  </div>
                                                  <button type="" class="btn btn-primary" id="cmt_submit">Submit</button>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <!-- MODALS END -->
                    <!--main content end-->
                    <!-- Right Slidebar start -->
                    <?php
    if ($RIGHT_SIDEBAR) {
        include('right-sidebar.php');
    }
    ?>
                        <!-- Right Slidebar end -->
                        <!--footer start-->
                        <?php include('footer.php'); ?>
                        <!--footer end-->
            </section>
            <?php include('scripts.php'); ?>
            <script>
                $(document).ready(function () {
                    var status_radio = $(".status-radio");
                    var agree_radio = $("#agree_radio");
                    var disagree_radio = $("#disagree_radio");
                    var status_submit = $("#status_submit");
                    var add_documents = $('#add_documents');
                    var cmt_form = $('#cmt_form');
                    var i_form = $('#i_form');
                    var cmt_count = 0;

                    var data = {
                            project_id:<?php echo $_GET['project_id'] ?>,
                        };

                    ajax_load(data, 'mc_body.php', 'mc_body');
                    ajax_load(data, 'pd_body.php', 'pd_body');
                    ajax_load(data, 'pp_body.php', 'pp_body');
                    ajax_load(data, 'cmt_body.php', 'cmt_body');
                    ajax_load(data, 'i_body.php', 'i_body');

                    agree_radio.on('click', function (e) {
                        $('#comments').val('');
                        $('#disagree_remarks').attr('style', 'display:none');
                        status_submit.removeAttr('disabled');
                    });
                    disagree_radio.on('click', function (e) {
                        $("#disagree_remarks").attr('style', 'display:block; margin-left:250px;');
                        status_submit.attr('disabled', 'disabled');
                    });
                    $('#comments').on('keyup', function (e) {
                        if($('#comments').val() != '')
                        {
                            status_submit.removeAttr('disabled');
                        }
                        else
                        {
                            status_submit.attr('disabled', 'disabled');
                        }
                    });
                    add_documents.on('click', function (e) {
                        $('#add_comments').val("");
                        $('#docs_comments_div').attr('style', 'display:none;');
                        $('#add_documents:checked').each(function(){
                            $('#docs_comments_div').attr('style', 'display:block; margin-left:70px;');
                       });
                    });

                    cmt_form.on('submit', function (e){
                            e.preventDefault();
                            add_cmt_info();
                    });
                    i_form.on('submit', function (e){
                            e.preventDefault();
                            add_i_info();
                    });

                    status_submit.on('click', function (e) {
                        status_submit.attr('disabled', 'disabled');
                        status_submit.html("载入中...");
                        var form_data = $('#audit_form').serialize();
                        $.ajax({
                          url:"php-functions/fncProjects.php?action=changeStatus",
                          type:"POST",
                          data:form_data,
                          success:function(msg)
                          {
                            if(msg != 0)
                            {
                              show_alert("Successfully Submitted", 'dashboard.php', 1);
                              status_submit.html("Submit");
                              /** PS: function show_alert(msg, redirect, type) --> footer.php **/
                            }
                            else
                            {
                              //show_alert("Unsuccessful Submission. Please Try Again. ", '', 0);
                              show_alert(msg, '', 0);
                              status_submit.removeAttr('disabled');
                              status_submit.html("Submit");
                            }
                          }
                        });
                    });

                    function add_cmt_info(){
                            var form_data = new FormData(cmt_form[0]);
                            form_data.append('project_id', "<?php echo $_GET['project_id'] ?>");
                            form_data.append('sub_dir', 'pre_comments');
                            form_data.append('user_id', '<?php echo $_SESSION['user_id'] ?>');
                            form_data.append('up_type', '3'); /* up_type 3 for P.R.E. comment */

                            $.ajax({
                                url: "php-functions/fncCommon.php?action=uploadFile",
                                type:'POST',
                                processData: false,
                                contentType: false,
                                data:form_data,
                                success:function(msg){
                                  if(msg == 1){
                                        show_alert("Operation successful.", "", 1);
//                                  #We reload the table
                                        ajax_load(data, 'cmt_body.php', 'cmt_body');
                                    }else{
                                        show_alert("Operation failed.", "", 0);
                                    }
                                }
                            });
                    }

                    function add_i_info(){
                            var form_data = new FormData(i_form[0]);
                            form_data.append('project_id', "<?php echo $_GET['project_id'] ?>");
                            form_data.append('sub_dir', 'project_documents');
                            form_data.append('user_id', '<?php echo $_SESSION['user_id'] ?>');
                            form_data.append('up_type', '4'); /* up_type 3 for project document version 2.x */

                            $.ajax({
                                url: "php-functions/fncCommon.php?action=uploadFile",
                                type:'POST',
                                processData: false,
                                contentType: false,
                                data:form_data,
                                success:function(msg){
                                  if(msg == 1){
                                        show_alert("Operation successful.", "", 1);
//                                  #We reload the table
                                        ajax_load(data, 'i_body.php', 'i_body');
                                    }else{
                                        show_alert("Operation failed.", "", 0);
                                    }
                                }
                            });
                    }

                    /* END DCUMENT RDY*/
                });
            </script>
        </body>
    </html>
