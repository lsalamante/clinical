<?php
include('jp_library/jp_lib.php');
require("php-functions/fncCommon.php");
require("php-functions/fncProjects.php");

if (!isset($_SESSION['user_id'])) {
  header("Location: " . "index.php");
  die();
}

#initial page data
//$user_array = getUserById(0, $_SESSION['user_id']);
$profgroup_array = getProfGroup(0);
$departments = getAllDepartments(0);
$trials = getAllTrials(0);

if(!isset($_GET['project_id'])){
  $_GET['project_id'] = false;
}

$project = getProjectById(0, $_GET['project_id']);

$prof_group_users = getPIByProfGroupId(0, $project['pgroup_id']); #get PI for principal investigator field

$pgroup_admin_id = getProfGroupAdminIdByProfGroupId(0, $project['pgroup_id']);

$pgroup_admin = getUserById(0, $pgroup_admin_id);
$pi = getUserById(0, $project['pi_id']);

$allowed_arr = [1,6,8];
$submit_only_arr = [6];

if(in_array($project['status_num'], $allowed_arr)){
  $disabled = '';
}else{
  $disabled = 'disabled';
}

if(in_array($project['status_num'], $submit_only_arr)){
  $submit_only = 1;
}else{
  $submit_only = 0;
}

// if(in_array($project['status_num'], $submit_only_arr)){
//   $submit_only = true;
// }else{
//   $submit_only = false;
// }

?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<?php include('header.php'); ?>

<body>
  <section id="container">
    <!--header start-->
    <header class="header white-bg">
      <?php
      if ($LEFT_SIDEBAR) {
        #  echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
      }
      ?>
      <!--logo start-->
      <?php if ($LOGO) {
        include('logo.php');
      }
      ?>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <?php if ($NOTIFICATION) {
          include('notification.php');
        } ?>
        <!--  notification end -->
      </div>
      <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
      include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <style>
    span.required
    {
      margin-left: 3px;
      color: red;
    }
    </style>
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->
        <div class="row">
          <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
              <li><a href="dashboard.php"><i class="fa fa-home"></i> <?php echo $phrases['project_audit']; ?></a></li>
              <li><?php echo $phrases['project_detail']; ?></li>
            </ul>
            <!--breadcrumbs end -->
          </div>
        </div>

        <?php include('project.php') ?>
        <!-- feedback row START -->
        <section class="panel negative-panel">
          <form id="audit_form">
            <div class="panel-body">
              <?php include('remarks.php'); ?>
              <?php if($project['status_num'] == 6): ?>
                <header class="panel-heading">
                  <?php echo $phrases['feedback_of_testing']; ?>
                </header>
                <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
                <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
                <input hidden id="status" name="status" value="1">
                <section class="panel negative-panel">
                  <div class="panel-body">
                    <button type="" class="btn btn-primary" id="status_submit" ><?php echo $phrases['submit_materials']; ?></button>
                  </div>
                </section>
              <?php elseif ($project['status_num'] == 1): ?>
                <header class="panel-heading">
                  <?php echo $phrases['feedback_of_testing']; ?>
                </header>
                <div class="row">
                  <!-- <label class="col-sm-2 control-label"><span class="required">*</span></label><br> -->
                  <!-- <div class="form-group">
                    <div class="col-sm-10">
                      <div class="checkbox" style="margin-bottom:0px">
                        <label>
                          <input type="checkbox" id="add_documents" name="add_documents" value="1" <?php echo $disabled ?>/> <?php echo $phrases['comments_tfo_1']; ?>
                        </label>
                      </div>
                      <div class="col-sm-6" id="docs_comments_div" style="display:none;">
                        <label for="add_comments"><?php echo $phrases['add_documents_comments']; ?></label><span class="required">*</span>
                        <textarea class="form-control v-resize-only" id="add_comments" name="add_comments"></textarea>
                        <span class="help-block"></span>
                      </div>
                    </div>
                  </div> -->
                  <label class="col-sm-2 control-label"></label><br>
                  <div class="form-group">
                    <div class="col-sm-10">
                      <div class="radio">
                        <label>
                          <input type="radio" id="agree_radio" name="status" value="1" <?php echo $disabled ?> /> <?php echo $phrases['agree_tfo_1']; ?>
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" id="disagree_radio" name="status" value="0" <?php echo $disabled ?> /> <?php echo $phrases['disagree_tfo_1']; ?>
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-6" id="disagree_remarks" style="display:none;">
                      <label for="comments"><?php echo $phrases['comments_tfo_2']; ?></label><span class="required">*</span>
                      <textarea class="form-control v-resize-only" id="comments" name="comments" placeholder="<?php echo $phrases['disagree'].$phrases['comments_tfo_2']; ?>"></textarea>
                      <span class="help-block"></span>
                    </div>
                    <input hidden id="project_status" name="project_status" value="<?php echo $project['status_num']; ?>">
                    <input hidden id="project_id" name="project_id" value="<?php echo $project['project_id']; ?>">
                  </div>
                </div>
            </div>
            <section class="panel negative-panel">
              <div class="panel-body">
                <button type="" class="btn btn-primary" id="status_submit" disabled ><?php echo $phrases['submit']; ?></button>
              </div>
            </section>
          <?php endif; ?>

          </form>
        </section>
        <!-- feedback row END -->

        <!-- page end-->
      </section>
    </section>


    <!--main content end-->
    <!-- Right Slidebar start -->
    <?php
    if ($RIGHT_SIDEBAR) {
      include('right-sidebar.php');
    }
    ?>
    <!-- Right Slidebar end -->
    <!--footer start-->
    <?php include('footer.php'); ?>
    <!--footer end-->
  </section>
  <?php include('scripts.php'); ?>
  <script>
  $(document).ready(function () {
    // INITIALIZE some data used on all pages
    <?php include("initialize.js"); ?>
    <?php include("audit.js"); ?>
    <?php include('disable_project.js') ?>
    /* END DCUMENT RDY*/
  });
  </script>
  </body>
  </html>
