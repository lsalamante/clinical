                <?php if(!isset($proj_info)): ?>
                <div class="text-center mbot30">
                    <h3><?php echo $phrases['timeline']?></h3>
                    <p>项目时间表: <?php echo $res['project_name']; ?></p>
                </div>
              <?php else: ?>
                <!-- <h3><b><?php echo $res['project_name']; ?></b></h3><br><br> -->
              <?php endif; ?>
                <?php foreach ($res['project_status'] as $odd => $arr) { ?>
                <div class="timeline">
                    <!-- <article class="timeline-item <?php echo ($odd%2) != 0 ? "alt" : ""; ?>"> LEFT AND RIGHT DESIGN-->
                    <article class="timeline-item <?php echo $arr['is_rejected'] != 1 ? "alt" : ""; ?>">
                        <div class="timeline-desk">
                            <div class="panel">
                                <div class="panel-body">
                                    <!--<span class="arrow<?php echo ($odd%2) != 0 ? "-alt" : ""; ?>"></span> LEFT AND RIGHT DESIGN-->
                                    <span class="arrow<?php echo $arr['is_rejected'] != 1 ? "-alt" : ""; ?>"></span>
                                    <span class="timeline-icon <?php echo $arr['is_rejected'] == "1" ? "red" : "blue" ?>"></span>
                                    <span class="timeline-date"><?php echo date("m/d/Y H:i:s", strtotime($arr['status_date'])); ?></span>
                                    <h1 class="<?php echo $arr['is_rejected'] == "1" ? "red" : "blue" ?>">
                                    <?php
                                    if($arr['status_num'] != 8)
                                    {
                                      echo $arr['is_rejected'] != "1" ? fncApproved($arr['status_num']) : fncRejected($arr['status_num'], $arr['reviewer_id']);
                                    }
                                    else
                                    {
                                      $forStatus8 = getUserById(false, $arr['reviewer_id'], true);
                                      if($forStatus8['acronym'] == 'A')
                                      {
                                        echo fncApproved("8.1");
                                      }
                                      else
                                      {
                                        echo $arr['is_rejected'] != "1" ? fncApproved($arr['status_num']) : fncRejected($arr['status_num'], $arr['reviewer_id']);
                                      }
                                    }
                                    ?>
                                    </h1>
                                    <p><?php $user = getUserByRoleId(false, $arr['reviewer_id']); echo "<b>".$user['fname']." ".$user['lname']."</b>"; ?></p>
                                    <?php if($arr['remarks_id'] != null && $arr['remarks'] != ''): ?>
                                      <p>Remarks:&nbsp;
                                        <i>
                                          <?php echo $arr['remarks']; ?>
                                        </i>
                                      </p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <?php } ?>

                <div class="clearfix">&nbsp;</div>
