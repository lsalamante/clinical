<div class="top-nav ">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">

                   <!-- Role -->
                    <li class="role hidden">
                        <h5><?php echo $phrases['role'] ?></h5>
                        <select style="font-size: small; width:9%!important;" id="top_nav_role" onchange="topRoleChange(this.value);">
                          <option value="<?php echo $_SESSION['role_id']; ?>"><?php echo $_SESSION['pos_desc']; ?></option>
                        <?php
                        $getRolesUser['table'] = "users u LEFT JOIN
                                      roles r ON u.user_id = r.user_id LEFT JOIN
                                      positions p ON r.position_id = p.position_id";
                        $getRolesUser['where'] = "u.user_id = ".$_SESSION['user_id']." AND p.description != '".$_SESSION['pos_desc']."' ";
                        $resRolesUser = jp_get($getRolesUser);
                        while($rowRolesUser = mysqli_fetch_assoc($resRolesUser))
                        {
                          echo '<option value="'.$rowRolesUser["role_id"].'">'.$rowRolesUser["description"].'</option>';
                        }
                        ?>
                        </select>
                    </li>

                    <!-- user login dropdown start-->
                        <li class="dropdown" style="margin-right: 100px;">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" style="border:none !important">
                <?php if ($AVATAR) echo '<img alt="" src="img/admin.png">'; ?>
                <span class="username">
<!--                               Welcome back, -->
                    <?php
                    if (isset($_SESSION['full_name']))
                        echo $_SESSION['full_name'];
                    ?>
                                </span> <b class="caret"></b> </a>
                            <ul class="dropdown-menu extended logout">
                                <div class="log-arrow-up"></div>
                                <!--                <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li>-->
                                <!--                <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>-->
                                <!--                <li><a href="#"><i class="fa fa-bell-o"></i> Notification</a></li>-->
                                <li><a href="logout.php"><i class="fa fa-key"></i> <?php echo $phrases['logout']; ?></a></li>
                            </ul>
                        </li>
                        <?php if ($RIGHT_SIDEBAR) {
            echo '<li class="sb-toggle-right"> <i class="fa  fa-align-right"></i> </li>';
        }
        ?>
                            <!-- user login dropdown end -->
        <?php if($LANGUAGE){ ?>
        <!-- <li class="dropdown language">
          <a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="#">
              <?php if($_SESSION['lang'] == 'chinese'){?>
                <img src="img/flags/ch.png" alt="">
                <span class="username">中文</span>
              <?php }elseif($_SESSION['lang'] == 'english'){ ?>
                <img src="img/flags/us.png" alt="">
                <span class="username">English</span>
              <?php } ?>
              <b class="caret"></b>
          </a>
          <ul class="dropdown-menu">
              <li><a href="#" id="lang_cn"><img src="img/flags/ch.png" alt=""> 中文</a></li>
              <li><a href="#" id="lang_en"><img src="img/flags/us.png" alt=""> English</a></li>
          </ul>
        </li> -->

                    <?php } ?>

    </ul>
    <!--search & user info end-->
</div>
