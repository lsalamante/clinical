<?php
require("php-functions/fncTraining.php");
if(isset($_GET["project_id"]) && $_GET["project_id"] != "" && ($_SESSION["user_type"] == "I" || $_SESSION["user_type"] == "CRC") ){ ?>
	<a data-toggle="modal" class="btn btn-primary adder" href="#addTraining" id="addTrainingButton"><?php echo $phrases['add']; ?></a>
	<form action="#" type="POST" id="addNewTraining">
		<input type="hidden" name="project_id" id="project_idT" value="<?php echo $_GET["project_id"]; ?>">
		<input type="hidden" name="created_by" id="created_by" value="<?php echo $_SESSION['role_id']; ?>">
	</form>
<?php
}

$t_type = array(
	"0" => $phrases['will_start'],
	"1" => $phrases['training'],
	"2" => $phrases['other']
	);

$project_id = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? $_GET["project_id"] : 0;
                                                                                                                                                // $projects - were from training-list.php
$trainings = isset($_GET["project_id"]) && $_GET["project_id"] != "" ?  getTraining(false, $project_id) : getTraining(false, $project_id, "listAll", $projects); //make way for training list

$count = 0;
if(count($trainings) > 0) // check if found record/s and make way for training list
{  ?>
		<table class="table table-hover">
		  <thead>
		    <tr>
		      <th><?php echo $phrases['number']; ?></th>
		      <th><?php echo $phrases['project_name']; ?></th>
		      <th><?php echo $phrases['title']; ?></th>
		      <th><?php echo $phrases['meeting_type']; ?></th>
		      <th><?php echo $phrases['date']; ?></th>
          <th><?php echo $t->tryTranslate('status'); ?></th>
		      <!-- <th><?php echo $phrases['action']; ?></th> -->
		    </tr>
		  </thead>
		  <tbody id="training_table_body">

		  <?php

                $count = 0;

                foreach ($trainings as $training) {

                  $count++;
									$training_proj = getProjectById(0, $training['project_id']);
									$training_proj_name = $training_proj['project_name'];
            ?>

                <tr onclick="getTrainingDetails(<?php echo $training['training_id']; ?>); return false; " style="cursor:pointer;">
                    <td><?php echo $count; ?></td>
                    <td><?php echo $training_proj_name; ?></td>
                    <td><?php echo $training['title'] == '' ? "无题会议" : $training["title"]; ?></td> <!-- 无题会议 means Untitled meeting-->
                    <td><?php echo $training['type'] == null ? $phrases["not_applicable"] : $t_type[$training["type"]]; ?></td>
                    <td><?php echo $training['training_date'] != '0000-00-00 00:00:00' ? date("Y-m-d H:i:s", strtotime($training['training_date'])) : $phrases['not_applicable']; ?></td>
                    <td><?php echo $training['is_drafted'] == 1 ? $t->tryTranslate('drafted') : $t->tryTranslate('submitted'); ?></td>
                    <!-- <td onclick="event.cancelBubble = true;"></td> -->
                </tr>

            <?php } ?>

		  </tbody>
		</table>
<?php
}
else
{ ?>
	<div class="col-lg-12 text-center">
      <img src="img/sad.png" class="sad">
      <h1 class="sad"><?php echo $phrases['empty_table_data']; ?></h1>
    </div>

<?php
}
?>

<!-- MODALSSSSSSSS -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addTraining" class="modal fade " data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" style="background #64aaf9">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title" id="modalTitleTrainingModal"><?php echo $phrases['add_record']; ?></h4>
      </div>
      <div class="modal-body nopadL nopadR">

        <form action="#" id="addTrainingForm" enctype="multipart/form-data" method="POST" class="form-horizontal tasi-form">

          <div class="form-group">
              <label class="col-sm-1" for=""><?php echo $phrases['title']; ?></label>
              <div class="col-sm-11">
                <textarea class="form-control" name="title" id="title" style="resize:vertical" required></textarea>
              </div>
          </div>
          <div class="form-group">
              <label class="col-sm-1" for=""><?php echo $phrases['type']; ?></label>
              <div class="col-sm-5">
              	<select name="type" id="type" class="form-control" required>
                <?php foreach ($t_type as $key => $type) { ?>
                	<option value="<?php echo $key; ?>"><?php echo $type; ?></option>
                <?php }?>
                </select>
              </div>
              <label class="col-sm-1" for=""><?php echo $phrases['host']; ?> </label>
              <div class="col-sm-5">
                <input type="text" class="form-control" name="host" id="host">
              </div>
          </div>
          <div class="form-group">
            <label class="col-sm-1"><?php echo $t->tryTranslate("date_training"); ?></label>
            <div class="col-lg-5">
              <div class="iconic-input right">
                <i class="fa fa-calendar"></i>
                <input type="text" class="form-control" id="training_date" name="training_date" placeholder="">
              </div>
            </div>
            <label class="col-sm-1" for=""><?php echo $phrases['place']; ?> </label>
              <div class="col-sm-5">
                <textarea class="form-control" name="place" id="place" style="resize:vertical" rows="3" required></textarea>
              </div>
          </div>
          <div class="form-group">
         	  <h5 style="margin-left:15px"><?php echo $phrases['participants2']; ?></h5> <br>
              <div class="col-sm-12">
                <table class="table table-hover">
                	<thead>
                		<th> </th>
                		<th><?php echo $phrases['number']?></th>
                		<th><?php echo $phrases['name']?></th>
                		<th><?php echo $phrases['gender']?></th>
                		<th><?php echo $phrases['departments']?></th>
                		<th><?php echo $phrases['training_modal_pos']?></th>
                		<th><?php echo $phrases['action']; ?></th>

                	</thead>
                	<tbody>
                		<?php $ctr = 0; foreach (getMembers(false, $project_id) as $user) { $ctr++; ?>
                			<tr>
                				<td><input type="checkbox" name="participants[]" id="participant_<?php echo $user['role_id']; ?>" value="<?php echo $user['role_id']; ?>"></td>
                				<td><?php echo $ctr; ?></td>
                				<td><?php echo $user['fname']." ".$user['lname']; ?></td>
                				<td><?php echo $user['gender'] == 0 ? $phrases['male'] :  $phrases['female']; ?></td>
                				<td><?php echo $user['department']; ?></td>
                				<td><?php echo $phrases[$user['lang_phrase']]; ?></td>
                				<td></td>
                			</tr>
                		<?php } ?>
                	</tbody>
                </table>
              </div>
		  </div>
		  <div class="form-group">
			  <label class="col-sm-2" for=""><?php echo $phrases['meeting_content']; ?> </label>
              <div class="col-sm-4">
                <textarea class="form-control" name="meeting_content" id="meeting_content" style="resize:vertical" required></textarea>
              </div>
              <label class="col-sm-2" for=""><?php echo $phrases['meeting_summary']; ?> </label>
              <div class="col-sm-4">
                <textarea class="form-control" name="meeting_summary" id="meeting_summary" style="resize:vertical" required></textarea>
              </div>
		  </div>
		  <div class="form-group">
         	  <h5 style="margin-left:15px"><?php echo $phrases['meeting_material']; ?></h5>
            <?php if( isset($_GET["project_id"]) && $_GET["project_id"] != "" && ($_SESSION["user_type"] == "RN" || $_SESSION["user_type"] == "I") ){ // make way for training list ?>
                <a href="#t_material_modal" data-toggle="modal" class="btn btn-xs btn-primary" id="add_training" style="margin-left:15px; margin-bottom:10px;" onclick="$('.modal').modal('hide');$('#t_material_form')[0].reset();"><?php echo $phrases['add']; ?></a>
         	  <?php } ?>

            <br>
              <div class="col-sm-12">
                <table class="table table-hover">
                	<thead>
                		<th><?php echo $phrases['number']?></th>
                		<th><?php echo $phrases['name']?></th>
                		<th><?php echo $phrases['version']?></th>
                		<th><?php echo $phrases['version_date']?></th>
                		<th><?php echo $phrases['description']; ?></th>
                		<th><?php echo $phrases['action']; ?></th>
                	</thead>
                	<tbody id="table_material_upload">
                	</tbody>
                </table>
              </div>
		  </div>

          <!-- Start hidden fields @Van-->

            <input type="hidden" name="training_id" id="training_id" value="">

          <!-- End hidden fields @Van-->

          <?php if( isset($_GET["project_id"]) && $_GET["project_id"] != "" && ($_SESSION["user_type"] == "RN" || $_SESSION["user_type"] == "I")){ // make way for training list  ?>
              <input type="hidden" id="is_drafted" name="is_drafted">
              <button type="submit" class="btn btn-primary" id="submitTraining"><?php echo $t->tryTranslate('submit'); ?></button>
              <button type="submit" class="btn btn-primary" id="draftTraining"><?php echo $t->tryTranslate('save_as_draft'); ?></button>
          <?php } ?>
        </form>
      </div>
    </div>
  </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="t_material_modal" class="modal fade" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 class="modal-title"><?php echo $phrases['add_meeting_material']; ?></h4>
      </div>
      <div class="modal-body">

        <form role="form" id="t_material_form" enctype="multipart/form-data">
          <div class="form-group">
            <label for=""><?php echo $phrases['name'] ?></label>
            <input type="text" class="form-control" id="material_name" name="name" placeholder="">
          </div>
          <div class="form-group">
            <label for=""><?php echo $phrases['version'] ?></label>
            <input type="number" min="0" step="0.1" class="form-control" id="material_ver" name="version" placeholder="">
          </div>
          <div class="form-group">
            <label for=""><?php echo $phrases['description'] ?></label>
            <textarea class="form-control v-resize-only" id="material_desc" name="description" placeholder=""></textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputFile"><?php echo $phrases['select_file']; ?></label>
            <input type="file" id="up_file" name="up_file[]" multiple="" required>
            <!-- <p class="help-block">Example block-level help text here.</p> -->
          </div>
          <div class="form-group">
            <input type="checkbox" name="databank" value="1" id="training_databank">
            <label for="training_databank"><?php echo $t->tryTranslate("save_to_databank"); ?></label>
          </div>

          <?php if( isset($_GET["project_id"]) && $_GET["project_id"] != "" && ($_SESSION["user_type"] == "RN" || $_SESSION["user_type"] == "I")){ // make way for training list  ?>
            <input type="hidden" name="project_id" id="project_id_training" value="<?php echo $_GET['project_id']; ?>">
            <button type="" class="btn btn-primary make-loading" id="t_material_submit"><?php echo $phrases['submit']; ?></button>
          <?php } ?>

        </form>
      </div>
    </div>
  </div>
</div>
