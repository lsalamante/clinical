<?php
namespace Clinical\Helpers;
include('jp_library/jp_lib.php');
require("php-functions/fncApplicant.php");
require("php-functions/fncCommon.php");
require("php-functions/fncProjects.php");
require("php-functions/fncStatusLabel.php"); # must always be below fncCommon.php and fncProjects.php

$DYNAMIC_TABLE = true;

if (!isset($_SESSION['user_id'])) {
  header("Location: " . "index.php");
  die();
}


$t = new Translation($_SESSION['lang']);
$l = new Log();
$l->fetchLogs();
?>
<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>

<style>
.odd .sorting_1 {
  background: #fff!important;
}
.even {
  background: #d8eafe!important;
}
</style>
<body class="bggray">
  <section id="container" style="background: white!important;">
    <!--header start-->
    <header class="header white-bg">
      <?php
      if ($LEFT_SIDEBAR) {
        //echo '<div class="sidebar-toggle-box"> <i class="fa fa-bars"></i> </div>';
      }
      ?>
      <!--logo start-->
      <?php if ($LOGO) {
        include('logo.php');
      }
      ?>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <?php if ($NOTIFICATION) {
          include('notification.php');
        } ?>
        <!--  notification end -->
      </div>
      <?php include('top-nav.php'); ?>
    </header>
    <!--header end-->
    <!--sidebar start-->
    <?php
    if ($LEFT_SIDEBAR) {
      include('left-sidebar.php');
    }
    ?>
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper site-min-height">
        <!-- page start-->

        <div class="row">
          <div class="col-lg-12">
            <!--breadcrumbs start -->
            <ul class="breadcrumb">
              <li><a href="training-list"><i class="fa fa-home"></i> <?php echo $t->tryTranslate('user_logs'); ?></a></li>
            </ul>
            <!--breadcrumbs end -->
          </div>
        </div>

        <section class="panel pad18">
          <table class="table table-hover" id="dynamic-table">
            <thead>
              <tr>
                <th><?php echo $t->tryTranslate("number"); ?></th>
                <th><?php echo $t->tryTranslate("time"); ?></th>
                <th><?php echo $t->tryTranslate("account"); ?></th>
                <th><?php echo $t->tryTranslate("role"); ?></th>
                <th><?php echo $t->tryTranslate("action"); ?></th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach (array_reverse($l->logs) as $logs) {
               ?>
              <tr>
                <td><?php echo $logs['log_id']; ?></td>
                <td><?php echo $logs['created_at']; ?></td>
                <?php
                $u = new User('role_id', $logs['role_id'], $_SESSION['lang']);

                 ?>
                <td><?php echo $u->fname; ?></td>
                <td><?php echo $u->role; ?></td>
                <td>
                  <?php echo $t->tryTranslate($logs['action']); ?>
                  <?php
                  if ($logs['payload'] != "") {
                    foreach (json_decode($logs['payload'], true) as $key => $value) {
                      echo "<br>$key: $value";
                    }
                  }
                   ?>
                </td>
              </tr>
              <?php
              }
               ?>
            </tbody>
          </table>
        </section>

<!-- page end-->
</section>
</section>
<div id="focus_me"></div>
<!--main content end-->
<!-- Right Slidebar start -->
<?php
if ($RIGHT_SIDEBAR) {
  include('right-sidebar.php');
}
?>
<!-- Right Slidebar end -->
<!--footer start-->
<?php include('footer.php'); ?>
<!--footer end-->
</section>
<?php include('scripts.php'); ?>

</body>

</html>
