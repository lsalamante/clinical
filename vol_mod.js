var vol_chk_all = $("#vol_chk_all");
var choose_arr = [];
vol_chk_all.change(function(){
  this.checked ? check_all(vol_body) :  uncheck_all(vol_body);
});

$('#choose_to_be_subject').on('click', function(){
  $("#vol_body input:checkbox:checked").each(function() {
    var _id = $(this).attr('id');
    _id = _id.split('-');
    //                      #push the number only
    choose_arr.push(_id[1]);
  });

  var form_data = {
    data: "choose_arr=" + choose_arr.join(",")
  };

  $.ajax({
    url: "php-functions/fncVolunteers.php?action=chooseToBeSubject",
    type: "POST",
    data: form_data,
    success: function (msg) {
      if(msg == 1){
        choose_arr = [];

        show_alert("<?php echo $phrases['operation_successful']?>", "pg_audit.php?tab=volunteer&project_id=<?php echo $_GET['project_id']?>", 1);
      }else{
        show_alert("<?php $phrases['operation_failed']?>", "", 0);
      }
      vol_chk_all.prop('checked', false);
    }
  });
});
