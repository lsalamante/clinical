<!-- Add Volunteer per Project @Rai-->

<?php
require("php-functions/fncVolunteers.php");


?>


<?php if(isset($_GET["project_id"]) && $_GET["project_id"] != "" && ($_SESSION["user_type"] == "RN" || $_SESSION['user_type'] == "SC")){ ?>

  <a class="btn btn-primary adder" id="addVolunteer"><?php echo $phrases['add']?></a>

  <?php } ?>

  <?php

  $project_id = isset($_GET["project_id"]) && $_GET["project_id"] != "" ? $_GET["project_id"] : 0;
  $volunteers = getAllVolunteers($project_id);

  $count = 0;
  ?>
  <?php if ($_SESSION['user_type'] == 'SC' && $project['status_num'] > 13): ?>
    <div class="pull-right" style="margin-top: -60px;">
      <a href="download-csv-template.php" class="btn btn-primary"><?php echo $t->tryTranslate('download_template'); ?></a>
    </div>

    <form class="pull-right" action="import-volunteer.php?project_id=<?php echo $_GET['project_id']; ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
      <!-- <span class="btn btn-success fileinput-button"> -->
      <!-- <i class="glyphicon glyphicon-plus"></i> -->
      <!-- <span>Add file...</span> -->
      <!-- <input type="file" name="file"> -->
      <!-- </span> -->

      <div class="fileupload fileupload-new" data-provides="fileupload" style="display: inline-block;">
        <span class="btn btn-white btn-file">
          <span class="fileupload-new"><i class="fa fa-paper-clip"></i> <?php echo $t->tryTranslate('select_file'); ?></span>
          <span class="fileupload-exists"><i class="fa fa-undo"></i> <?php echo $t->tryTranslate('change'); ?></span>
          <input type="file" name="file" class="default" />
        </span>
        <span class="fileupload-preview"></span>
        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
      </div>

      <input type="Submit" value="<?php echo $t->tryTranslate('import'); ?>" class="btn btn-primary" style="margin-left: 10px; ">
    </form>
  <?php endif; ?>
  <?php

  if(count($volunteers) > 0 && $project_id != 0){ // check if found record/s and if id is provided.

    ?>

    <?php if(($_SESSION['user_type'] == 'C' || $_SESSION['user_type'] == 'SC') && $project['status_num'] == 14) {?>
      <button type="" class="btn btn-primary" id="choose_to_be_subject" ><?php echo $phrases['choose_to_be_subject']?></button>
      <?php } ?>
      <!-- VOLUNTEER SEARCH -->
      <div class="col-lg-12">
        <form role="form" id="" action="#" enctype="multipart/form-data">
          <div class="form-group">
            <!-- <label class="col-lg-1"><?php echo $t->tryTranslate("type"); ?></label> -->
            <div class="col-lg-3">
              <input type="text" name="vsearch_keyword" id="vsearch_keyword" class="form-control" placeholder="搜索" onkeyup="keywordSearch(this.value,<?php echo $_GET['project_id']; ?>,'volunteers','volunteer_body.php','vol_body');">
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-1">
              <input type='hidden' name="project_id" id="project_id" value="<?php echo $_GET['project_id']; ?>">
              <!-- <input class="btn btn-primary" type="button" id="vsearch_submit" value="<?php echo $t->tryTranslate('search'); ?>"> -->
            </div>
          </div>
        </form>
      </div>

      <!-- VOLUNTEER FILTERS -->
      <div class="col-lg-12">
        <form role="form" id="" action="#" enctype="multipart/form-data">
        <!-- Patient Type -->
          <div class="form-group">
            <!-- <label class="col-lg-1"><?php echo $t->tryTranslate("type"); ?></label> -->
            <div class="col-lg-3">
              <select name="vsearch_type" id="vsearch_type" class="form-control">
                <option value=""><?php echo $t->tryTranslate("type"); ?></option>
                <option value="1"><?php echo $t->tryTranslate("volunteer_patient"); ?></option>
                <option value="2"><?php echo $t->tryTranslate("volunteer_healthy_people"); ?></option>
              </select>
            </div>
          </div>
        <!-- Patient Status -->
          <div class="form-group">
            <!-- <label class="col-lg-1"><?php echo $t->tryTranslate("status"); ?></label> -->
            <div class="col-lg-3">
              <select name="vsearch_status" id="vsearch_status" class="form-control">
                <option value=""><?php echo $t->tryTranslate("status"); ?></option>
                <option value="C"><?php echo $t->tryTranslate("before_selecting"); ?></option>
                <option value="S"><?php echo $t->tryTranslate("clinician_selecting"); ?></option>
                <option value="P"><?php echo $t->tryTranslate("pass"); ?></option>
                <option value="NP"><?php echo $t->tryTranslate("no_pass"); ?></option>
                <option value="B"><?php echo $t->tryTranslate("patient_status_begin"); ?></option>
                <option value="FU"><?php echo $t->tryTranslate("patient_status_follow_up"); ?></option>
                <option value="STP"><?php echo $t->tryTranslate("stop"); ?></option>
                <option value="O"><?php echo $t->tryTranslate("patient_status_out"); ?></option>
                <option value="D"><?php echo $t->tryTranslate("patient_status_drop"); ?></option>
                <option value="F"><?php echo $t->tryTranslate("patient_status_finish"); ?></option>
              </select>
            </div>
          </div>
        <!-- Patient Birthday -->
          <div class="form-group">
            <!-- <label class="col-lg-1"><?php echo $t->tryTranslate("date_of_birth"); ?></label> -->
            <div class="col-lg-3">
              <input name="vsearch_birthday" id="vsearch_birthday" class="form-control" placeholder="<?php echo $t->tryTranslate('date_of_birth'); ?>">
            </div>
          </div>
        <!-- Patient Gender -->
          <div class="form-group">
            <!-- <label class="col-lg-1"><?php echo $t->tryTranslate("gender"); ?></label> -->
            <div class="col-lg-2">
              <select name="vsearch_gender" id="vsearch_gender" class="form-control">
                <option value=""><?php echo $t->tryTranslate("gender"); ?></option>
                <option value="Male"><?php echo $t->tryTranslate("male"); ?></option>
                <option value="Female"><?php echo $t->tryTranslate("female"); ?></option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-1">
              <input type='hidden' name="project_id" id="project_id" value="<?php echo $_GET['project_id']; ?>">
              <input class="btn btn-primary" type="button" id="vfilter_submit" value="<?php echo $t->tryTranslate('search'); ?>">
            </div>
          </div>
        </form>
      </div>

      <table class="table table-hover">
        <thead>
          <tr>
            <th onclick="event.cancelBubble = true;">
              <?php # if ($_SESSION['user_type'] == 'C'){?>
                <input type="checkbox" id="vol_chk_all" />
                <?php # } ?>
              </th>
              <th><?php echo $phrases['number']?></th>
              <th><?php echo $phrases['volunteer_name']?></th>
              <th><?php echo $phrases['gender']?></th>
              <th><?php echo $phrases['date_of_birth']?></th>
              <th><?php echo $phrases['native']?></th>
              <th><?php echo $phrases['married']?></th>
              <th><?php echo $phrases['id_card_num']?></th>
              <th><?php echo $phrases['phone_number']?></th>
              <th><?php echo $phrases['state']?></th>
              <th><?php echo $phrases['remarks']; ?></th>
              <th><?php echo $phrases['volunteer_type']?></th>
              <th><?php echo $phrases['operation']?></th>
            </tr>
          </thead>
          <tbody id="vol_body">

            <?php

            $count = 0;

            foreach ($volunteers as $vol) {

              $count++;
              ?>

              <tr onclick="getVolunteerDetails(<?php echo $vol['volunteer_id']; ?>, '<?php echo $vol['status']; ?>', '<?php echo $_SESSION['user_type']; ?>'); return false;" style="cursor:pointer;">
                <td onclick="event.cancelBubble = true;">
                  <?php if (($_SESSION['user_type'] == 'C' || $_SESSION['user_type'] == 'SC') && in_array($vol['status_letter'], ['P'])){?>
                    <input type="checkbox" id="vol_chk-<?php echo $vol['volunteer_id'] ?>" />
                    <?php } ?>
                  </td>
                  <td><?php echo $count; ?></td>
                  <td><?php echo strtoupper($vol["v_name"]); ?></td>
                  <td><?php echo $vol["gender"] == "Male" ? $phrases['male'] : $phrases['female']; ?></td>
                  <td><?php echo $vol["bday"]; ?></td>
                  <td><?php echo $vol["native"]; ?></td>
                  <td><?php echo $vol["married"] == "Yes" ? $phrases['yes'] : $phrases['no']; ?></td>
                  <td><?php echo $vol["id_card_num"]; ?></td>
                  <td><?php echo $vol["mobile"]; ?></td>
                  <td>
                    <?php

                    if($vol['status_letter'] == 'P') #NOTE: this is important!!! `status_letter` is used instead of `status` for using the subject_status status column!
                    $status = $phrases['pass'];
                    else if ($vol['status_letter'] == 'NP')
                    $status = $phrases['no_pass'];
                    else if ($vol['status_letter'] == 'S')
                    $status = $phrases['clinician_selecting'];
                    else if ($vol['status_letter'] == 'C')
                    $status = $phrases['chosen_subject'];
                    else if ($vol['status_letter'] == 'B')
                    $status = $phrases['begin'];
                    else if ($vol['status_letter'] == 'FU')
                    $status = $phrases['follow_up'];
                    else if ($vol['status_letter'] == 'STP')
                    $status = $phrases['stop'];
                    else if ($vol['status_letter'] == 'O')
                    $status = $phrases['out'];
                    else if ($vol['status_letter'] == 'F')
                    $status = $phrases['finish'];
                    else if ($vol['status_letter'] == 'D')
                    $status = $phrases['subject_drop'];
                    else if ($vol['status'] == 0)
                    $status = $phrases['before_selecting'];

                    // if($vol["status"] == 0)
                    // $status = "Before selecting"; #PHRASE: add before_selecting phrase
                    // else if($vol["status"] == 1)
                    // $status = "Clinician selecting";

                    echo $status;

                    ?>
                  </td>
                  <td>
                    <?php echo $vol['stat_remarks']; ?>
                  </td>
                  <td>
                    <?php 
                    if($vol["volunteer_type"] == 1)
                      echo $t->tryTranslate("patient");
                    else if($vol["volunteer_type"] == 2)
                      echo $t->tryTranslate("healthy_people");
                    ?>
                 </td>
                  <td onclick="event.cancelBubble = true;">

                    <?php if($project['status_num'] == 14 && ($_SESSION['user_type'] == 'RN' || $_SESSION['user_type'] == 'SC') && $vol['status_letter'] == ''){ ?>

                      <a href="#" onclick="volunteerStatus(<?php echo $vol['volunteer_id']; ?>, <?php echo $_GET['project_id']; ?>, 'S', '<?php echo $_SESSION['lang']; ?>'); return false;">
                        <button class="btn btn-primary" > <!-- title="Inform clinician" -->
                          <?php echo $phrases['inform_clinician']; ?>
                        </button>
                      </a>

                      <?php } ?>

                      <?php if($project['status_num'] == 14 && ($_SESSION['user_type'] == 'C' || $_SESSION['user_type'] == 'SC') && $vol['status_letter'] == 'S'){ ?>

                        <a href="#" onclick="volunteerStatus(<?php echo $vol['volunteer_id']; ?>, <?php echo $_GET['project_id']; ?>, 'P', '<?php echo $_SESSION['lang']; ?>'); return false;">
                          <button class="btn btn-primary" title="Pass">
                            <!-- <i class="fa fa-check" aria-hidden="true"></i> -->
                            <?php echo $phrases['volunteer_pass']; ?>
                          </button>
                        </a>

                        <?php } ?>

                        <?php if($project['status_num'] == 14 && ($_SESSION['user_type'] == 'C' || $_SESSION['user_type'] == 'SC') && $vol['status_letter'] == 'S'){ ?>

                          <a href="#" onclick="volunteerStatus(<?php echo $vol['volunteer_id']; ?>, <?php echo $_GET['project_id']; ?>, 'NP', '<?php echo $_SESSION['lang']; ?>'); return false;">
                            <button class="btn btn-primary" title="No pass">
                              <!-- <i class="fa fa-times" aria-hidden="true"></i> -->
                              <?php echo $phrases['volunteer_no_pass']; ?>
                            </button>
                          </a>

                          <?php } ?>


                          <!-- Show only when pending -->
                          <?php if(false){ ?>

                            <a href="#" onclick="volunteerStatus(<?php echo $vol['volunteer_id']; ?>, <?php echo $_GET['project_id']; ?>, 2); return false;">
                              <button class="btn btn-primary" title="Reject">
                                <i class="fa fa-times" aria-hidden="true"></i>
                              </button>
                            </a>

                            <?php } ?>


                            <a href="#" onclick="remarks(<?php echo $vol['volunteer_id']; ?>); return false;">
                              <button class="btn btn-primary" title="Remarks">
                                <?php echo $phrases['remarks']; ?>
                              </button>
                            </a>


                            <!-- Show only when pending and rejected -->
                            <?php if(false){ ?>

                              <a href="#" onclick="volunteerStatus(<?php echo $vol['volunteer_id']; ?>, <?php echo $_GET['project_id']; ?>, 3); return false;">
                                <button class="btn btn-primary" title="Delete">
                                  <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                              </a>

                              <?php } ?>

                            </td>

                          </tr>

                          <?php } ?>

                        </tbody>
                      </table>
                      <?php if(count($volunteers) > 0 && $project_id != 0){ ?>
                        <!-- REVIEW: -->
                        <section class="panel negative-panel">
                          <div class="panel-body">

                          </div>
                        </section>
                        <?php }else{ /*do nothing*/ }?>

                        <?php }else{ // no record/s found ?>

                          <div class="col-lg-12 text-center">
                            <img src="img/sad.png" class="sad">
                            <h1 class="sad"><?php echo $phrases['empty_table_data']?></h1>
                          </div>

                          <?php } ?>



                          <!-- Start of Add Volunteer Modal  @Rai-->

                          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="volunteerModal" class="modal fade" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                  <h4 class="modal-title" id="modalTitle"><?php echo $phrases['add_volunteer_information']?></h4>
                                </div>
                                <div class="modal-body">

                                  <form role="form" id="volunteerForm" action="#" enctype="multipart/form-data">

                                    <div class="form-group" id="v_name_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['full_name']?></label>
                                      <input type="text" class="form-control" id="v_name" name="v_name" placeholder="">
                                      <p class="help-block" style="display:none;" id="v_name_msg"></p>
                                    </div>

                                    <div class="form-group" id="v_volunteer_type_div">
                                      <label><?php echo $t->tryTranslate('volunteer_type'); ?></label>
                                      <select class="form-control" id="volunteer_type" name="volunteer_type">
                                        <option value=""><?php echo $t->tryTranslate('choose'); ?></option>
                                        <option value="1"><?php echo $t->tryTranslate('patient_volunteer'); ?></option>
                                        <option value="2"><?php echo $t->tryTranslate('healthy_volunteer'); ?></option>
                                      </select>
                                      <p class="help-block" style="display:none;" id="v_name_msg"></p>
                                    </div>

                                    <div class="form-group" id="gender_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['gender']?></label>
                                      <select name="gender" id="gender" class="form-control">
                                        <option value=""><?php echo $phrases['select']?></option>
                                        <option value="Male"><?php echo $phrases['male']?></option>
                                        <option value="Female"><?php echo $phrases['female']?></option>
                                      </select>
                                      <p class="help-block" style="display:none;" id="gender_msg"></p>
                                    </div>

                                    <div class="form-group" id="bday_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['birth_date']?></label>
                                      <input type="text" class="form-control" id="bday" name="bday" placeholder="">
                                      <p class="help-block" style="display:none;" id="bday_msg"></p>
                                    </div>

                                    <div class="form-group" id="nation_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['nation']?></label>
                                      <input type="text" class="form-control" id="nation" name="nation" placeholder="">
                                      <p class="help-block" style="display:none;" id="nation_msg"></p>
                                    </div>

                                    <div class="form-group" id="native_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['native']?></label>
                                      <input type="text" class="form-control" id="native" name="native" placeholder="">
                                      <p class="help-block" style="display:none;" id="native_msg"></p>
                                    </div>

                                    <div class="form-group" id="married_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['married']?></label>
                                      <div class="radio">
                                        <input value="Yes" name="married" type="radio"> <?php echo $phrases['yes']?> &nbsp;&nbsp;
                                        <input value="No" name="married" type="radio"> <?php echo $phrases['no']?>
                                      </div>
                                      <p class="help-block" style="display:none;" id="married_msg"></p>
                                    </div>

                                    <div class="form-group" id="blood_type_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['blood_type']?></label>
                                      <input type="text" class="form-control" id="blood_type" name="blood_type" placeholder="">
                                      <p class="help-block" style="display:none;" id="blood_type_msg"></p>
                                    </div>

                                    <div class="form-group" id="surgery_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['had_surgery']?></label>
                                      <div class="radio">
                                        <input value="Yes" name="surgery" type="radio"> <?php echo $phrases['yes']?> &nbsp;&nbsp;
                                        <input value="No" name="surgery" type="radio"> <?php echo $phrases['no']?>
                                      </div>
                                      <p class="help-block" style="display:none;" id="surgery_msg"></p>
                                    </div>

                                    <div class="form-group" id="fam_history_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['family_history']?></label>
                                      <input type="text" class="form-control" id="fam_history" name="fam_history" placeholder="">
                                      <p class="help-block" style="display:none;" id="fam_history_msg"></p>
                                    </div>

                                    <div class="form-group" id="smoker_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['heavy_smoking']?></label>
                                      <div class="radio">
                                        <input value="Yes" name="smoker" type="radio"> <?php echo $phrases['yes']?> &nbsp;&nbsp;
                                        <input value="No" name="smoker" type="radio"> <?php echo $phrases['no']?>
                                      </div>
                                      <p class="help-block" style="display:none;" id="smoker_msg"></p>
                                    </div>

                                    <div class="form-group" id="drinker_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['heavy_drinking']?> </label>
                                      <div class="radio">
                                        <input value="Yes" name="drinker" type="radio"> <?php echo $phrases['yes']?> &nbsp;&nbsp;
                                        <input value="No" name="drinker" type="radio"> <?php echo $phrases['no']?>
                                      </div>
                                      <p class="help-block" style="display:none;" id="drinker_msg"></p>
                                    </div>

                                    <div class="form-group" id="allergies_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['allergies']?></label>
                                      <input type="text" class="form-control" id="allergies" name="allergies" placeholder="">
                                      <p class="help-block" style="display:none;" id="allergies_msg"></p>
                                    </div>

                                    <div class="form-group" id="height_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['height']?> 『cm』</label>
                                      <input type="text" class="form-control" id="height" name="height" placeholder="" onchange="computeBMI();">
                                      <p class="help-block" style="display:none;" id="height_msg"></p>
                                    </div>

                                    <div class="form-group" id="weight_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['weight']?> 『kg』</label>
                                      <input type="text" class="form-control" id="weight" name="weight" placeholder="" onchange="computeBMI();">
                                      <p class="help-block" style="display:none;" id="weight_msg"></p>
                                    </div>

                                    <div class="form-group" id="bmi_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['bmi']?></label>
                                      <input type="text" class="form-control" id="bmi" name="bmi" placeholder="" readonly="">
                                      <p class="help-block" style="display:none;" id="bmi_msg"></p>
                                    </div>

                                    <div class="form-group" id="med_history_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['medical_history']?></label>
                                      <input type="text" class="form-control" id="med_history" name="med_history" placeholder="">
                                      <p class="help-block" style="display:none;" id="med_history_msg"></p>
                                    </div>

                                    <div class="form-group" id="mobile_div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['mobile_num']?></label>
                                      <input type="text" class="form-control" id="mobile" name="mobile" placeholder="">
                                      <p class="help-block" style="display:none;" id="mobile_msg"></p>
                                    </div>

                                    <div class="form-group" id="">
                                      <label for=""><?php echo $phrases['id_card_num'] ?></label>
                                      <input type="text" class="form-control" id="id_card_num" name="id_card_num" placeholder="" required>
                                    </div>

                                    <div class="form-group" id="">
                                      <label for=""><?php echo $phrases['id_card_copy'] ?></label>
                                      <input type="file" id="" name="up_file[]" multiple="" required/>
                                    </div>

                                    <div class="form-group hidden" id="id_card_copy_div">
                                      <label for=""><?php echo $phrases['id_card_copy'] ?> <?php echo $phrases['file']?></label>
                                      <a href="" class="btn btn-sm btn-primary" target="_blank" id="id_card_copy_file" title="view file"><?php echo $phrases['view_attachments']; ?></a>
                                    </div>


                                    <!-- Start hidden fields @Rai-->

                                    <input type="hidden" name="volunteer_id" id="volunteer_id" value="">
                                    <input type="hidden" name="project_id" id="project_id" value="<?php echo $_GET["project_id"]; ?>">
                                    <input type="hidden" name="v_status" id="v_status" >

                                    <!-- End hidden fields @Rai-->

                                    <button type="submit" class="btn btn-primary" id="submitVolunteer"></button>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>

                          <!-- End of Add Volunteer Modal  @Rai-->

                          <!-- Start of Add Remarks Modal  @Rai-->
                          <a href="#volunteerRemarks" data-toggle="modal" class="btn btn-primary adder"  id="addRemarks" style="display:none;"></a>
                          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="volunteerRemarks" class="modal fade" data-backdrop="static" data-keyboard="false">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                  <h4 class="modal-title" id="modalTitle"><?php echo $phrases['add_remarks']?></h4>
                                </div>
                                <div class="modal-body">

                                  <form role="form" id="remarksForm">

                                    <div class="form-group" id="remarks__div">
                                      <label for="exampleInputEmail1"><?php echo $phrases['remarks']?></label>
                                      <textarea name="remarks" id="remarks_" class="form-control" required="required"></textarea>
                                      <p class="help-block" style="display:none;" id="remarks__msg"></p>
                                    </div>

                                    <!-- Start hidden fields @Rai-->

                                    <input type="hidden" name="volunteer_idR" id="volunteer_idR" value="">
                                    <input type="hidden" name="project_idR" id="project_idR" value="<?php echo $_GET["project_id"]; ?>">

                                    <!-- End hidden fields @Rai-->

                                    <button type="submit" class="btn btn-primary" id="submitRemarks"><?php echo $phrases['submit']?></button>

                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>

                          <!-- End of Add Remarks Modal  @Rai-->
